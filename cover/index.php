<?php 
session_start();
 //unset($_SESSION['img']);


  
  
 
?>
<!DOCTYPE html>
<html>
  <head>
    <title>cropit</title>

   <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script src="dist/jquery.cropit.js"></script>
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js" ></script>

    <style>
      .cropit-preview {
        background-color: #f8f8f8;
        background-size: cover;
        border: 1px solid #ccc;
        border-radius: 3px;
        height: 250px;
        margin-top: 7px;
        position: relative;
        width: 700px;
      } 
      .cropit-preview > img {
          height: 248px;
          width: 698px;
      }
      .change_photo {
          position: absolute;
          right: 7px;
          top: 34px;
      }
      .remove_photo {
          position: absolute;
          right: 7px;
          top: 82px;
      }

      .cropit-preview-image-container {
        cursor: move;
      }

      .image-size-label {
        margin-top: 10px;
      }

      input {
        display: block;
      }

      button[type="submit"] {
        /*margin-top: 10px;*/
      }

      #result {
        margin-top: 10px;
        width: 900px;
      }

      #result-data {
        display: block;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        word-wrap: break-word;
      }
	  .image-editor {
    display: inline-block;
    position: relative;
}
.add_photo {
    background: #007aea none repeat scroll 0 0;
    color: #fff;
    display: inline-block;
    font-weight: bold;
    padding: 1px 6px;
    position: absolute;
    right: 0;
    top: 21px;
    z-index: 1;
}
.remove_photo {
    background: #007aea none repeat scroll 0 0;
    color: #fff;
    display: inline-block;
    font-weight: bold;
    padding: 1px 6px;
    position: absolute;
    right: 0;
    top: 54px;
    z-index: 1;
}
#loading-image {
    background: transparent url("loading.gif") no-repeat scroll 0 0;
    height: 135px;
    left: 294px;
    position: absolute;
    top: 63px;
    width: 133px;
    z-index: 99;
	display:none;
}
.add_photo:hover, .remove_photo:hover {
    cursor: pointer;
}
    </style>
  </head>
  <body>

        

              

    


    

    <!--<div id="result">
      <code>$form.serialize() =</code>
      <code id="result-data"></code>
    </div>-->

    <script>
      $(function() {
        $('.image-editor').cropit();

        $('#saveImage').click(function() {
          // Move cropped image data to hidden input
          var imageData = $('.image-editor').cropit('export');  
          //console.log(imageData);
          $('.hidden-image-data').val(imageData);
		  $('#loading-image').show();
		  $.ajax({
			  url: "upload.php",
			  method:"POST",
			  data: {"image":imageData}
			}).done(function(data) {
				$(".cropit-preview img").attr("src","images/"+data);
				//alert(data);
				 $(".modal-footer").hide();
	   $(".cropit-image-zoom-input").hide();
	   $(".image-size-label").hide();
	   
	   $(".cropit-preview-image").attr("src","");
				//alert("Done");
			  //$( this ).addClass( "done" );
			   $('#loading-image').hide();
			});

          // Print HTTP request params
          var formValue = $(this).serialize();
          $('#result-data').text(formValue);
            
        });
		
		$('.remove_photo').click(function() {
          // Move cropped image data to hidden input
          var imageData = $('.image-editor').cropit('export');  
          //console.log(imageData);
          $('.hidden-image-data').val(imageData);
		   $('#loading-image').show();
		  $.ajax({
			  url: "upload.php?del=yes",
			  method:"POST",
			}).done(function() {
				//alert("Done");
			  //$( this ).addClass( "done" );
			  $(".cropit-preview img").attr("src","images/banner.jpg");
			  $(".cropit-preview-image-container").remove();
			  $('#loading-image').hide();
			});

          // Print HTTP request params
          var formValue = $(this).serialize();
          $('#result-data').text(formValue);
            
        });
        /*$('.cancel').click(function(){
            $('.cropit-preview').empty();
            $('.cropit-image-input').val('');
        });*/
      });
    </script>
    

			<div class="image-editor">
			<div id="loading-image"></div>
				<input type="file" id="fileInput" name="fileInput" style="display:none;" class="cropit-image-input"/>
				<p onclick="chooseFile();" class="add_photo"><i class="fa fa-camera"></i><span>Add cover photo</span></p>
				<p class="remove_photo"><span>Remove cover photo</span></p>
					<div class="cropit-preview"><img src="images/<?php echo isset($_SESSION["img"]) ? $_SESSION["img"] : 'banner.jpg'?>">
          </div>
					<div class="image-size-label" style="display:none;">
						Resize image
					</div>
					<input type="range" class="cropit-image-zoom-input" style="display:none;">
					<input type="hidden" name="image-data" class="hidden-image-data" />
					<div class="modal-footer" style="display:none;">
						 <button type="submit" class="btn btn-primary" name="send" id="saveImage">Save</button>
						 <button type="button" class="btn btn-default cancel" onclick="clickcancel();">Cancel</button>
					</div>
			</div>


  </body>
</html>

<script>
   function chooseFile() {
	   //alert("choose file clicked");
      $("#fileInput").click();
   }
   function clickcancel() {
		$.ajax({
			  url: "upload.php?can=yes",
			  method:"POST",
			}).done(function(data) {
				$(".cropit-preview img").attr("src","images/"+data);
				$(".modal-footer").hide();
			   $(".cropit-image-zoom-input").hide();
			   $(".image-size-label").hide();
			   $(".cropit-preview-image").attr("src","");
			});
   
	   $(".modal-footer").hide();
	   $(".cropit-image-zoom-input").hide();
	   $(".image-size-label").hide();
	   $(".cropit-preview-image").attr("src","");
   }
   

</script>	
