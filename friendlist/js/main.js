$(document).ready(function () {

    $('#sidebarCollapse').on('click', function () {
        // open or close navbar
        $('#sidebar').toggleClass('active');
    });


    /* ---------------------------------------------
        WOW js Animation 
     --------------------------------------------- */
    var wow = new WOW(
      {
        mobile:false // trigger animations on mobile devices (true is default)
      }
    );
    wow.init();




   $('.active-carusel').owlCarousel({
        loop: true,
        nav: true,
        navText: ["<i class='fa fa-caret-left' aria-hidden='true'></i>","<i class='fa fa-caret-right' aria-hidden='true'></i>"],          
        items: 2,
        margin: 10,
        responsive: {
            0: {
                items: 1
            },
            552: {
                items: 2,
            }
        }        
    })


});