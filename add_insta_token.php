 <?php

require_once "bootstrap.php";

require_once 'model/Signup.php';

require_once 'model/Instagram.php';

require_once 'classes/Session.class.php';

include_once("config.php");

include_once("instagramoauth/instagram.class.php");

$session = new Session();

$instagram = new Instagram(array(
    'apiKey' => IG_CONSUMER_KEY,
    'apiSecret' => IG_CONSUMER_SECRET,
    'apiCallback' => 'http://whatsdadilly.com/betaPhaseProjectWDD/add_insta_token.php'
));

if ($_GET['code']) {
    
    $code = $_GET['code'];
    
    $data = $instagram->getOAuthToken($code);
    
    $session->setSession("screen_name_instagram", $data->user->username);
    $session->setSession("screen_id_instagram", $data->user->id);
    $session->setSession("auth_token_instagram", $data->access_token);
    $session->setSession("auth_secret_instagram", $code);
    
    //$instagram->setAccessToken($data);
    
    //$media = $instagram->getUserMedia();
    
    // foreach($media->data as $values){
    
    // $med['ig_p_id']  = $values->id;
    
    // $med['auth_id']  = base64_decode($_GET['profileid']);
    
    // $med['own_id']   = base64_decode($_GET['profileid']);
    
    // $med['link']     = $values->link;
    
    // $med['img_link'] = $values->images->standard_resolution->url;
    
    // $med['datetime'] = date('Y-m-d H:i:s');
    
    // $med['posttype'] = 2;
    
    // $last_id = Insta::addFeedPost($entityManager,$med);
    // }
    
    $arr['networkname'] = 'instagram';
    
    $arr['scr_id'] = $data->user->id;
    
    $arr['scr_name'] = $data->user->username;
    
    $arr['img_link'] = $data->user->profile_picture;
    
    $arr['oauth_token'] = $data->access_token;
    
    $arr['oauth_secret'] = $code;
    
    $arr['user_id'] = $session->getSession("userid");
    
    $isExist = Insta::checkAlreadyExist($entityManager, $arr);
    
    if (empty($isExist)) {
        
        $last_id = Insta::addNetworkToken($entityManager, $arr);
        
        header('location:home.php?msg=success');
        
    } else {
        
        header('location:profile.php?error=error');
    }
    
}
Download F