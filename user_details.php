 <?php
error_reporting(-1);
require_once "bootstrap.php";
require_once 'classes/Session.class.php';
require_once "model/Signup.php";

$session = new Session();
$Profile = new Signup();

$userId = "";

if ($session->getSession("userid") != "" || $session->getSession("userid") != null) {
    
    
    if (!empty($_GET['profileid'])) {
        
        $data['userid'] = base64_decode($_GET['profileid']);
        
        $user_info = $Profile->profileView($data, $entityManager);
        
        $date = explode('-', date('d-m-Y', strtotime($user_info[0]["dob"])));
        
        $work_edu = $Profile->workEducations($data, $entityManager);
        
        $contact_info = $Profile->contactInfo($data, $entityManager);
        
        if ($contact_info['email'] != '') {
            $email = explode(',', $contact_info['email']);
        } else {
            $email = array(
                '',
                ''
            );
        }
        
        if ($contact_info['phone'] != '') {
            $phone = explode(',', $contact_info['phone']);
        } else {
            $phone = array(
                '',
                ''
            );
        }
        
        
        if ($contact_info['website'] != '') {
            $web = explode(',', $contact_info['website']);
        } else {
            $web = array(
                '',
                ''
            );
        }
        
        $cityName    = $Profile->getCityName($user_info[0]['current_city'], $entityManager);
        $stateName   = $Profile->getStateName($user_info[0]['current_state'], $entityManager);
        $countryName = $Profile->getCountryName($user_info[0]['current_country'], $entityManager);
        
        
        $country = $Profile->allCountry($entityManager);
        
        $state = $Profile->getState($user_info[0]['current_country'], $entityManager);
        
        $city = $Profile->getCity($user_info[0]['current_state'], $entityManager);
        
        //echo '<pre>'; print_r($contact_info);die;
        
    }
    //include 'html/user_details.php';
    
} else {
    header("Location:index.php");
} 