 <?php
error_reporting(-1);
require 'getid3/getid3.php';
require 'bootstrap.php';
require_once 'entities/Album.php';
require_once 'model/Albums.php';
require_once 'model/Wall.php';
require_once 'classes/Albums.class.php';
require_once 'classes/Session.class.php';

if ($session->getSession("userid") != "" || $session->getSession("userid") != null) {
    
    $userid = $session->getSession("userid");
    
    $output_dir = __DIR__ . "/uploads/temp/";
    
    $output_dir_con = __DIR__ . "/uploads/temp/thumb/";
    
    $Albums = new Albums();
    
    $imageName = $_FILES["file"]["name"];
    
    $uploadedfile = $_FILES["file"]["tmp_name"];
    
    $getID3 = new getID3;
    
    $fileinfo = $getID3->analyze($uploadedfile);
    
    $width = $fileinfo['video']['resolution_x'];
    
    $height = $fileinfo['video']['resolution_y'];
    
    $extension = $Albums->getExtension($imageName);
    
    $extension = strtolower($extension);
    
    if ($extension == "jpg" || $extension == "jpeg") {
        $src = imagecreatefromstring(file_get_contents($uploadedfile));
        
    } else if ($extension == "png") {
        $src = imagecreatefromstring(file_get_contents($uploadedfile));
        
    } else {
        $src = imagecreatefromstring(file_get_contents($uploadedfile));
    }
    
    switch ($width && $height) {
        
        case $width > 250 && $height > 260:
            $newwidth3  = 250;
            $newheight3 = 260;
            break;
        case $width > 250 && $height < 260:
            $newwidth3  = 250;
            $newheight3 = $height;
            break;
        case $width < 250 && $height > 260:
            $newwidth3  = $width;
            $newheight3 = 260;
            break;
        default:
            $newwidth3  = $width;
            $newheight3 = $height;
            break;
    }
    
    $original_aspect = $width / $height;
    
    $thumb_aspect = $newwidth3 / $newheight3;
    
    if ($original_aspect >= $thumb_aspect) {
        // If image is wider than thumbnail (in aspect ratio sense)
        $new_height = $newheight3;
        
        $new_width = $width / ($height / $newheight3);
    } else {
        // If the thumbnail is wider than the image
        $new_width  = $newwidth3;
        $new_height = $height / ($width / $newwidth3);
    }
    
    $thumb = imagecreatetruecolor($newwidth3, $newheight3);
    
    // Resize and crop
    imagecopyresampled($thumb, $src, 0 - ($new_width - $newwidth3) / 2, // Center the image horizontally
        0 - ($new_height - $newheight3) / 2, // Center the image vertically
        0, 0, $new_width, $new_height, $width, $height);
    
    $imgNm = uniqid();
    
    $image = $imgNm . "." . $extension;
    
    $filename = $output_dir_con . $image;
    
    if (move_uploaded_file($uploadedfile, $output_dir . $image)) {
        
        imagejpeg($thumb, $filename, 80);
        
        header("Content-type:application/json");
        
        echo json_encode(array(
            'image' => $image
        ));
        exit();
    }
    
} else {
    
    header("Location:index.php");
    
} 