 <?php
error_reporting(-1);
require 'bootstrap.php';
include_once("config.php");
require_once 'model/Audio.php';
require_once 'classes/Session.class.php';
require_once "entities/Videos.php";
require_once "model/VideosModel.php";
require_once "model/Friends.php";
require_once 'model/Wall.php';
require_once 'model/Comments.php';
include 'html/wall/functions.php';
require_once 'model/PhotosModel.php';
$session    = new Session();
$AudioModel = new AudioModel();
$messages   = new Friends();
//echo'<pre>';print_r($_POST);die;
if (!empty($_POST)) {
    if ($_POST['profile_id'] == '') {
        $data['owner_id'] = $session->getSession('userid');
    } else {
        $data['owner_id'] = $_POST['profile_id'];
    }
    $data['userid']        = $session->getSession('userid');
    $data['unique_file']   = 'uploads/tmp_audio/' . $_POST['unique_file'];
    $data['file_thumb']    = $_POST['file_thumb'];
    $data['full_name']     = addslashes($_POST['full_name']);
    $data['desc_playlist'] = addslashes($_POST['street1']);
    $data['playlist_type'] = $_POST['playlist_type'];
    $data['rel_date']      = $_POST['bday'];
    $data['genre']         = $_POST['genre'];
    $data['tags']          = $_POST['zip'];
    $data['descript']      = addslashes($_POST['message']);
    if ($session->getSession('userid') == $_POST['profile_id'] || $_POST['profile_id'] == '') {
        $data['post_type'] = 0;
    } else {
        $data['post_type'] = 1;
    }
    $data['date'] = date('Y-m-d H:i:s');
    
    //echo "<pre>"; print_r($data); die;
    
    //echo $get_last_id ;die;
    $get_last_id = $AudioModel->store_wall_post_audio($data, $entityManager);
    if ($session->getSession('userid') == $_POST['profile_id'] || $_POST['profile_id'] == '') {
        
        
        $friends = Friends::getFriendsList($entityManager, $session->getSession('userid'), 5000);
        
        if ($get_last_id != "") {
            $data['wall_id'] = $get_last_id;
            $last_id         = $AudioModel->store_audio_file($data, $entityManager);
            
            if ($last_id) {
                
                $previous_post = WallModel::getPrviousPost($entityManager, $data['wall_id']);
                
                foreach ($previous_post as $key => $value) {
                    
                    $comments = CommentsModel::getComments($previous_post[$key]['id'], $entityManager);
                    
                    $photos = PhotosModel::getPhotos($previous_post[$key]['id'], $entityManager);
                    
                    $previous_post[$key]['comments'] = $comments;
                    $previous_post[$key]['photos']   = $photos;
                    $previous_post[$key]['audios']   = $AudioModel->getAudios($previous_post[$key]['id'], $entityManager);
                    
                }
                
            }
            
        }
        
        foreach ($friends as $friend) {
            $params = array(
                'id_friend' => $session->getSession('userid'),
                'id_user' => $friend['user_id'],
                'message' => addslashes("<span>" . $session->getSession('firstname') . " " . $session->getSession('lastname') . " </span>publish new audio . You can comment it and share. "),
                'thumb' => $data['file_thumb'],
                'type' => '1',
                'read' => 0
            );
            NotificationModel::addNotificationForAudio($entityManager, $params);
            
            
        }
    } else {
        
        //$friends = Friends::getFriendsList($entityManager, $session->getSession('userid'), 5000);
        
        
        $data['post_type'] = 1;
        $get_user_info     = $messages->getUserInfoById($entityManager, $session->getSession("userid"));
        
        $fullname = $get_user_info['firstname'] . ' ' . $get_user_info['lastname'];
        
        $frnds_info = $messages->getFriendsEmail($entityManager, $session->getSession("userid"));
        
        
        $newfriendarray = array();
        foreach ($frnds_info as $key => $friendss) {
            if ($friendss['user_id'] != $_POST['profile_id']) {
                $newfriendarray[] = $friendss;
                
            }
            
        }
        $frnds_info = $newfriendarray;
        
        
        $get_user_profile    = $messages->getUserInfoByOwnerId($entityManager, $_POST['profile_id']);
        $get_user_profile_id = $get_user_profile[0]['user_id'];
        //echo'<pre>';print_r($get_user_profile);
        
        if ($_POST['profile_id'] != $session->getSession("userid")) {
            
            
            
            $frnds_info = array_merge($frnds_info, $get_user_profile);
        }
        //$frnds_info = array_merge($frnds_info,$get_user_profile);
        
        $noti_type = 8;
        $noti_read = 0;
        
        $noti_id_friend = $session->getSession("userid");
        
        $get_user_profile1    = $messages->getUserInfoByOwnerId($entityManager, $_POST['profile_id']);
        //echo'<pre>';print_r($get_user_profile1);
        $profileownerfullname = $get_user_profile1[0]['firstname'] . ' ' . $get_user_profile1[0]['lastname'];
        
        foreach ($frnds_info as $key => $value) {
            
            $noti_id_user = $value['user_id'];
            
            if ($value['user_id'] == $_POST['profile_id']) {
                $msg = addslashes("<span>" . $fullname . "</span> posted new audio on your Whiteboard");
            } else {
                $msg = addslashes("<span>" . $fullname . "</span> posted new audio on " . $profileownerfullname . " Whiteboard");
            }
            
            $sql = "INSERT INTO `notifications` (`id_user`,`id_friend`,`type`,`message`,`thumb`,`readed`) values ('" . $noti_id_user . "','" . $noti_id_friend . "','" . $noti_type . "','" . $msg . "','" . $_POST['file_thumb'] . "','" . $noti_read . "')";
            
            $notifications->addNotificationNEW($entityManager, $sql);
            
            
        }
        if ($get_last_id != "") {
            $data['wall_id'] = $get_last_id;
            $last_id         = $AudioModel->store_audio_file($data, $entityManager);
            
            if ($last_id) {
                
                $previous_post = WallModel::getPrviousPost($entityManager, $data['wall_id']);
                
                foreach ($previous_post as $key => $value) {
                    
                    $comments = CommentsModel::getComments($previous_post[$key]['id'], $entityManager);
                    
                    $photos = PhotosModel::getPhotos($previous_post[$key]['id'], $entityManager);
                    
                    $previous_post[$key]['comments'] = $comments;
                    $previous_post[$key]['photos']   = $photos;
                    $previous_post[$key]['audios']   = $AudioModel->getAudios($previous_post[$key]['id'], $entityManager);
                    
                }
                
            }
            
        }
        
    }
    
}






$postids = 0;
foreach ($previous_post as $entry) {
    
    $userdetails = WallModel::getUserDetails($entityManager, $entry['author_id']);
    
    if ($entry['type'] == 1) {
        $infodetails = WallModel::getUserDetails($entityManager, $entry['owner_id']);
    }
    
?>
       
            
    <div class="middle_area_four crispbx crispbxmain" id="wall<?php
    echo $entry['id'];
?>" style="margin-bottom:5px;" data="<?php
    echo $entry['id'];
?>" data-count="<?php
    echo $postids;
?>"> 
  
        <input type="hidden" id="wall_entry_id" value="<?php
    echo $entry['id'];
?>" />

            <div class="slider_two_title">


                <img class="image_border_style" src="uploads/<?php
    echo $_SESSION['profile_pic'];
?>" alt="" style="width:49px;height:49px;"/>

                        <div class="slider_title_style2">

                            <span class="author_slide_top author_upload_name Font18">

                                <a style="font-size:16px;" href="./profile.php?profileid=<?php
    echo base64_encode($userdetails[0]['user_id']);
?> ">
                                <?php
    echo $userdetails[0]['firstname'];
?> <?php
    echo $userdetails[0]['lastname'];
?>
                               </a>
                                <?php
    if ($entry['type'] == 1) {
        $text = ' <span style="font-size:14px;">posted on </span>';
        if (trim($entry['link']) != '') {
            $text = ' <span style="font-size:14px;">shared a <a href="' . $entry['link'] . '" target="_blank">link</a> on</span> ';
        }
        $username = ' <a style="font-size:16px;" href="./profile.php?profileid=' . base64_encode($infodetails[0]['user_id']) . '">' . $infodetails[0]['firstname'] . ' ' . $infodetails[0]['lastname'] . '</a> ';
        if ($_SESSION['userid'] == $infodetails[0]['user_id']) {
            $username = ' <span style="font-size:14px;">Your</span> ';
        }
        echo $text . $username . '<span style="font-size:14px;">Whiteboard</span>';
    }
    echo '<br /><span style="font-size:12px;font-weight:normal;">' . WallModel::time_elapsed_string($entry['date']) . '</span>';
?>

                            </span>

                            <p class="update_profile_date"></p>
                        </div>

            </div>
            
            <!-- Processing -->
            
                <?php
    if (!empty($entry['audios'])) {
?>
               <div style="width: 98%;margin-left: 5px;">
                <div class="progress"  style="background:none;background-color:#f5f5f5;padding-right:0px;">
                      <div class="progress-bar" id="progress<?php
        echo $entry['id'];
?>" role="progressbar" aria-valuenow="70"
                      aria-valuemin="0" aria-valuemax="100" style="width:0%">
                        <span>Processing...</span>
                      </div>
                </div>
                </div>
                <?php
    }
?>
               
        <div style="padding:10px;width:100%;"><p><?php
    echo Functions::addLink($entry['text']);
?></p></div>

<?php
    if (strlen($entry['link']) > 0) {
?>
   <div class="display_profile_pic">
<?php
        if (strlen($entry['link_photo']) > 0):
?>
       <img src="<?php
            echo $entry['link_photo'];
?>" alt="" />
        <?php
        endif;
?>
           <div style="padding:10px;">

            <p><a target="_blank" href="<?php
        echo $entry['link'];
?>" style="color: #000;font-size: 18pt;line-height: 1.2em;"><?php
        echo $entry['link_title'];
?></a></p>
            <p style="font-size:12pt;"><?php
        echo $entry['link_description'];
?></p>
           </div>
        <div class="clear"></div>
    </div>
<?php
    }
?>

            <div class="player_body"  id="player_body" style='background-image: url("<?php
    echo $entry['audios']['thumb'];
?>");opacity:0.4;'><!-- Start Player Body -->

                

                <div class="player_content">

                    <div class="column add-bottom">

                        <div id="mainwrap">

                            <div id="nowPlay">

                                <h3 class="left" id="songTitle_<?php
    echo $entry['audios']['id'];
?>"><?php
    echo $entry['audios']['title'];
?></h3>

                                <p  class="right" id="songAuther_<?php
    echo $entry['audios']['id'];
?>"><?php
    echo $entry['audios']['playlist_desc'];
?></p>

                            </div>

                            <div id="audiowrap">

                                <div id="waveform_<?php
    echo $entry['audios']['id'];
?>" data='<?php
    echo $entry['audios']['file'];
?>' class='waveform' ></div>


                               <!-- <div id="time-total"></div> -->

                                <span class="waveform__counter" id='counter_<?php
    echo $entry['audios']['id'];
?>'></span>

                                <span class="waveform__duration" id='duration_<?php
    echo $entry['audios']['id'];
?>'></span>
                                <script>
                                                                    
                                        var wavesurfer_<?php
    echo $entry['audios']['id'];
?> = WaveSurfer.create({
                                                    container        :     '#waveform_<?php
    echo $entry['audios']['id'];
?>',
                                                    waveColor        :     '#B2B2B2',
                                                    cursorColor        :     'transparent',
                                                    progressColor    :     '#fd8331',
                                                    barWidth        :    '3',
                                                    height            :    '180',
                                                    renderer        :     'MultiCanvas',
                                                    pixelRatio        :       5,
                                                    timeInterval    :      100,
                                                    backend            :     'MediaElement',
                                                });
                                                
                                            //wavesurfer_<?php
    echo $entry['audios']['id'];
?>.load("<?php
    echo $entry['audios']['file'];
?>");
                                            
                                            
                                            
                                            // Change Volume  
                                            wavesurfer_<?php
    echo $entry['audios']['id'];
?>.setVolume(0.4);
                                            
                                            document.querySelector('#volume_<?php
    echo $entry['audios']['id'];
?>').value = wavesurfer_<?php
    echo $entry['audios']['id'];
?>.backend.getVolume();
                                    
                                    </script>    
                                <div class="custom_controls"> 

                                    <div class="volbox">

                                        <i id="toggleMuteBtn" class="fas fa-volume-up"></i>

                                        <input class="volume" id="volume_<?php
    echo $entry['audios']['id'];
?>" type="range" min="0" max="1" value="0.1" step="0.1">

                                    </div>

                                   <span class='mp3_menu_right'>

                                    <a class="backward" onclick="backward(wavesurfer_<?php
    echo $entry['audios']['id'];
?>,<?php
    echo $entry['audios']['id'];
?>)"><i class="fa fa-backward"></i></a>
                    
                                    <span id="playPause_<?php
    echo $entry['audios']['id'];
?>">

                                        <button id='ply_<?php
    echo $entry['audios']['id'];
?>' onclick="play_pause(wavesurfer_<?php
    echo $entry['audios']['id'];
?>,<?php
    echo $entry['audios']['id'];
?>,'<?php
    echo $entry['audios']['file'];
?>')">

                                            <i class="fas fa-play"></i>

                                        </button>
                                        
                                        <button style='display:none;' id='pause_<?php
    echo $entry['audios']['id'];
?>' onclick="pause(wavesurfer_<?php
    echo $entry['audios']['id'];
?>,<?php
    echo $entry['audios']['id'];
?>)">

                                            <i class="fas fa-pause"></i>

                                        </button>
                                        
                                        <button style='display:none;' id='plya_<?php
    echo $entry['audios']['id'];
?>' onclick="playAgain(wavesurfer_<?php
    echo $entry['audios']['id'];
?>,<?php
    echo $entry['audios']['id'];
?>)">

                                            <i class="fas fa-play"></i>

                                        </button>

                                    </span>
                    
                                    <a class="forward" onclick="forward(wavesurfer_<?php
    echo $entry['audios']['id'];
?>,<?php
    echo $entry['audios']['id'];
?>)" ><i class="fa fa-forward"></i></a>

                                    <a onClick="play_pause(wavesurfer_<?php
    echo $entry['audios']['id'];
?>,<?php
    echo $entry['audios']['id'];
?>,'<?php
    echo $entry['audios']['file'];
?>')" id="repeateFun"><i class="fas fa-redo-alt"></i></a>

                                    <!--<a onClick="shuffleOnOff()" id="shuffle"><i class="fa fa-random"></i></a>-->
                                    
                                    </span>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div><!-- End Player Body -->

            <script>
                
            </script>

            <br>

            <br>

                <div class="like_comment_share">
                    <a href="#"><span><i class="fa fa-thumbs-up"></i>Like</span></a>
                    <a href="#"><span><i class="fa fa-heart"></i>Love it</span></a>
                   <a href="#" class="add-comment-link" rel="<?php
    echo $entry['id'];
?>" ><span><i class="fa fa-commenting"></i>Comment</span></a>
                    <a href="#"><span><i class="fa fa-share-square-o"></i>Share</span></a>
                </div>
                <div data="<?php
    echo $entry['id'];
?>" id="post_comment_<?php
    echo $entry['id'];
?>"> </div>
                <div class="comment_box" rel="<?php
    echo $entry['id'];
?>">
                    <form rel="<?php
    echo $entry['id'];
?>" id="commform_<?php
    echo $entry['id'];
?>" action="" method="post" enctype="multipart/form-data" onsubmit="return false;">
                        <img src="uploads/<?php
    echo $_SESSION['profile_pic'];
?>" alt="" style="width:37px;height:37px;">
                        <span>
                        <input class="comment_box_inline grybord comment_input" placeholder="Write your comment here" id="postid_<?php
    echo $entry['id'];
?>" name="comment" type="text" autocomplete="off"></span>
                        <div class="comment_box_icon comment_box_inline"><a href="#"><span><i class="comment_box_left fa fa-camera"></i></span></a>
                        <a href="#"><span><i class="comment_box_left2 fa fa-smile-o"></i></span></a></div>
                        <h5 class="press_enter_post">Press enter to post</h5>
                    </form>
                </div>


                <div data="<?php
    echo $entry['id'];
?>" id="comment_<?php
    echo $entry['id'];
?>"> 
                    <?php
    foreach ($entry['comments'] as $comment):
        $cuserdetails = WallModel::getUserDetails($entityManager, $comment['author_id']);
?>
                       <div class="slider_two_title">
                            <img src="uploads/<?php
        echo $cuserdetails[0]['profile_pic'];
?>" alt="<?php
        echo $comment['1firstname'];
?> <?php
        echo $comment['1lastname'];
?>" style="width:37px;height:37px;">
                            <div class="slider_title_style 1single-comment">
                                <a href="#"><span class="author_slide_top"><?php
        echo $comment['firstname'];
?> <?php
        echo $comment['lastname'];
?></span></a>
                                <span class="comment_box_bottom_text"><?php
        echo Functions::addLink($comment['text']);
?></span>
                                <a href="#"><p class="like_and_reply"><span>Like</span><span class="comment_reply">Reply</span> 路 <?php
        echo date('m/d/Y H:i:s', strtotime($comment['date']));
?></p></a>                                    
                            </div>
                        </div>
                        <div class="line"></div>
                <?php
    endforeach;
?>
               </div>


</div>
<?php
    $postids++;
}
die();
?> 