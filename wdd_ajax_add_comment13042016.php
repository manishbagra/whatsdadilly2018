 <?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once "bootstrap.php";

require_once 'model/Comments.php';
include 'html/wall/functions.php';
include_once("config.php");

require_once 'classes/Session.class.php';

$session = new Session();
if ($session->getSession("userid") != "" || $session->getSession("userid") != null) {
    $img_p = $session->getSession("profile_pic");
    $data  = array(
        "userid" => $session->getSession("userid")
    );
    
    //    $messages = new Friends();
    $notifications = new NotificationModel();
    
    function slugify($text)
    {
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        $text = trim($text, '-');
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = strtolower($text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        
        if (empty($text)) {
            return 'n-a';
        }
        
        return $text;
    }
    
    if (isset($_POST['post_id'])) {
        //$comment = $_POST['comment-' . $_POST['post_id']];
        $post_id = $_POST['post_id'];
        
        
        $params              = array();
        $params['author_id'] = $_SESSION['userid'];
        $params['post_id']   = $post_id;
        $params['text']      = $_POST["text"];
        $params['date']      = date('Y-m-d H:i:s');
        CommentsModel::addComment($entityManager, $params);
    }
}
?> 