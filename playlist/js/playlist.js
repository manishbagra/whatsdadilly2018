var formatTime = function (time) {

			return [

				Math.floor((time % 3600) / 60), // minutes

				('00' + Math.floor(time % 60)).slice(-2) // seconds

			].join('.');

	};
function playSingleTrack(file,thumb,title,playlist_desc) {
	
	var modelId = $("#singleTrack");
	
	modelId.show();
	
	modelId.addClass("singleTrack");
	
	$(".player_body").css('background-image' , 'url('+thumb+')');
	
	if($(".modal").hasClass("fade")){
		
		$(".modal").removeClass("fade").addClass("fade in");
	}
	
	setTimeout(function(){
		
		var wavesurfer = WaveSurfer.create({
		container: '#waveform',
		waveColor: '#B2B2B2',
		cursorColor: 'transparent',
		progressColor: '#fd8331',
		barWidth:'3',
		height:'180',
		renderer: 'MultiCanvas',
		pixelRatio:  5,
		timeInterval: 100,
		backend: 'MediaElement',
		
		});	
		
		$("#songTitle").text(title);
		$("#songAuther").text(playlist_desc);
		
	
		//alert(JSON.stringify(wavesurfer));
		
		wavesurfer.load(file);
		
		wavesurfer.on('ready', function () {
																	
			$('#duration').text( formatTime(wavesurfer.getDuration()));
		
		});
		
		wavesurfer.on('audioprocess', function() {
								
			$('#counter').text( formatTime(wavesurfer.getCurrentTime()));
			
			if(wavesurfer.isPlaying()) { }
			
		});
		
		wavesurfer.play();
		
		
	}, 1000);
	
}