<?php
use Doctrine\ORM\Tools\Setup;

require_once "Doctrine/ORM/Tools/Setup.php";
Setup::registerAutoloadPEAR();

$isDevMode = true;
$config    = Setup::createXMLMetadataConfiguration(array(
    __DIR__ . "/config/xml"
), $isDevMode);


$conn = array(
    'driver' => 'pdo_mysql',
    'host' => '127.0.0.1',
    'dbname' => 'whatsdadilly',
    'user' => 'root',
    'password' => ''
);


$entityManager = Doctrine\ORM\EntityManager::create($conn, $config);