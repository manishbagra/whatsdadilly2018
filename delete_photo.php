 <?php
error_reporting(-1);
require 'bootstrap.php';
require_once 'model/Albums.php';
require_once 'classes/Session.class.php';

if ($_GET != '') {
    
    $Albums = new Albums();
    
    $id    = $_GET['id'];
    $albId = $_GET['albId'];
    $path  = $_GET['path'];
    
    
    $session = new Session();
    
    $userid = $session->getSession('userid');
    
    $checkCoverPic = $Albums->checkPhotoCover($entityManager, $id, $albId);
    
    
    
    if (count($checkCoverPic) > 0) {
        
        $result = $Albums->getNewPhotoIdCover($entityManager, $albId, $id);
        
        $Albums->setCoverPhotoOnly($entityManager, $result, $albId);
        
    }
    
    unlink($path);
    
    $delRespo = $Albums->deletePhoto($entityManager, $id);
    
    if ($delRespo == true) {
        $jsonArray = array(
            'status' => true
        );
        echo json_encode($jsonArray);
        exit();
    }
} 