 <?php
error_reporting(E_ALL & ~E_NOTICE);
clearstatcache();
require_once "bootstrap.php";
require_once 'model/Twitter.php';
require_once 'classes/Session.class.php';
include_once("config.php");
include_once("twitteroauth/twitteroauth.php");
include_once("twitter_ajax/codebird.php");
require_once 'model/Instagram.php';

$session = new Session();

$insta = new Insta();

if ($session->getSession("userid") != "" || $session->getSession("userid") != null) {
    
    if ($_GET['prof_id'] == "undefined") {
        
        $data = array(
            "userid" => $session->getSession("userid")
        );
        
    } else {
        
        $data = array(
            "userid" => base64_decode($_GET['prof_id'])
        );
        
    }
    
    
    $twitterInfo = Twitter::getAllTwitterAccounts($data, $entityManager);
    
    // echo '<pre>';
    // print_r($twitterInfo);die;
    
    $screenname         = $twitterInfo[0]['screen_name'];
    $twitterid          = $twitterInfo[0]['screen_id'];
    $oauth_token        = $twitterInfo[0]['auth_token'];
    $oauth_token_secret = $twitterInfo[0]['auth_secret'];
    
    
    $url      = $_SERVER['REQUEST_URI'];
    $str      = explode("/", $url);
    $cur_url  = $str[count($str) - 1] . '?';
    $pcur_url = $str[count($str) - 1];
    
    include 'html/twitter_feeds.php';
    
} else {
    header("Location:index.php");
}
?> 