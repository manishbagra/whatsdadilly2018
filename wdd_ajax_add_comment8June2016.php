 <?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once "bootstrap.php";
ini_set('display_errors', 1);

require_once 'model/Comments.php';
require_once 'model/Wall.php';
include 'html/wall/functions.php';
include_once("config.php");

require_once 'classes/Session.class.php';

$session = new Session();
if ($session->getSession("userid") != "" || $session->getSession("userid") != null) {
    $img_p         = $session->getSession("profile_pic");
    $data          = array(
        "userid" => $session->getSession("userid")
    );
    //echo $_SESSION['userid']."USERID".$session->getSession("userid")."::".$session->getSession("profile_pic");
    //    $messages = new Friends();
    $notifications = new NotificationModel();
    
    function slugify($text)
    {
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        $text = trim($text, '-');
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = strtolower($text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        
        if (empty($text)) {
            return 'n-a';
        }
        
        return $text;
    }
    
    if (isset($_POST['post_id'])) {
        //$comment = $_POST['comment-' . $_POST['post_id']];
        $post_id = $_POST['post_id'];
        if ($_POST['type'] == "reply") {
            $parent_id = $_POST['comment_id'];
        } else {
            $parent_id = 0;
        }
        $params              = array();
        $params['author_id'] = $_SESSION['userid'];
        $params['post_id']   = $post_id;
        $params['text']      = $_POST["text"];
        $params['date']      = date('Y-m-d H:i:s');
        $params['parent_id'] = $parent_id;
        CommentsModel::addComment($entityManager, $params); //
        $allWallPhotos = WallModel::getPhotoAlbum($entityManager, $post_id);
        $postInfo      = WallModel::getPrviousPost($entityManager, $post_id);
        $postAuthorId  = $postInfo[0]['author_id'];
        $postText      = $postInfo[0]['text'];
        //print_r($postInfo);
        //Add Into NotificationModel
        //If Comment then update to Author Only
        
        $notificationParams['id_user']   = $_SESSION['userid'];
        $notificationParams['id_friend'] = $postAuthorId;
        
        
        //print_r($allWallPhotos);echo count($allWallPhotos)."K";
        if (count($allWallPhotos) > 0) {
            $photoPath = $allWallPhotos[0]['file'];
            $message   = "Commented On a Post $postText <img src='uploads/" . $photoPath . "' width='50px' height='50px'/>";
        } else {
            $message = "Commented On a Post $postText";
        }
        //$message = "Commented On a Postaaa";
        //$message = "Commented On <img src='".$photoPath."'/>";
        $notificationParams['message'] = $message;
        $notificationParams['type']    = 6;
        
        //If Comment then update to Author and Comment Creator
        NotificationModel::addNotification($entityManager, $notificationParams);
    }
    
}
?> 