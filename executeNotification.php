<?php
require_once "bootstrap.php";
require_once 'classes/Session.class.php';
require_once 'model/Notification.php';

$session = new Session();

$iduser = $session->getSession('userid');

NotificationModel::readNotifications($entityManager, $iduser);