 <?php
//error_reporting(-1);

require 'bootstrap.php';
require_once 'entities/Album.php';
require_once 'model/Albums.php';
require_once 'model/Wall.php';
require_once 'classes/Albums.class.php';
require_once 'classes/Session.class.php';

$album_desc    = "";
$picDesc       = array();
$albumLocation = "";

$last_photo_id = 0;

$Albums = new Albums();

if (!empty($_POST)) {
    
    $picDesc = explode('@DadillYWhats@', $_POST['picDesc']);
    
    $image64 = explode('@DadillY@', $_POST['image64']);
    
    $albumLocation = $_POST['album_location'];
    
    $album_desc = $_POST["description"];
    
    $output_dir = __DIR__ . "/uploads";
    
    $session = new Session();
    
    $userid = $session->getSession('userid');
    
    $params = array();
    
    $params['userid'] = $userid;
    
    $params['id_album'] = $session->getSession('id_album');
    
    $params['title'] = $_POST['title'];
    
    $photo = AlbumUtil::getAlbum($entityManager, $params);
    
    if ($photo == false) {
        die('trying to acess an album that isnt yours');
    }
    
    AlbumUtil::userHaveFolder();
    
    $output_dir .= "/" . $userid . "/";
    if (!file_exists($output_dir)) {
        $mask = umask(0);
        mkdir($output_dir, 0777);
        umask($mask);
    }
    
    $output_dir .= "albums/";
    
    if (!file_exists($output_dir)) {
        $mask = umask(0);
        mkdir($output_dir, 0777);
        umask($mask);
    }
    
    
    $output_dir .= $params['id_album'] . "/";
    
    if (!file_exists($output_dir)) {
        $mask = umask(0);
        mkdir($output_dir, 0777);
        umask($mask);
    }
    
    
    $thumbDir = $output_dir . "thumb/";
    
    if (!file_exists($thumbDir)) {
        
        $mask = umask(0);
        
        mkdir($thumbDir, 0777);
        
        umask($mask);
    }
    
    
    
    $resp = array();
    
    $total_photos = count($image64);
    
    foreach ($image64 as $key => $value) {
        
        $params = array(
            'id_owner' => $userid,
            'id_album' => $params['id_album'],
            'title' => $params['title'],
            'pic_desc' => addslashes($picDesc[$key])
        );
        
        $last_id[$key] = $Albums->createPhoto($entityManager, $params);
        
        if ($key == 0) {
            
            $params1 = array(
                'id_album' => $params['id_album'],
                'title' => addslashes($params['title']),
                'id_photo' => $last_id[0],
                'about_album' => addslashes($album_desc),
                'location' => addslashes($albumLocation)
            );
            
            $Albums->setCover($entityManager, $params1);
            
        }
        
        
        $dir = __DIR__ . "/uploads/temp/";
        
        $dir_thumb = __DIR__ . "/uploads/temp/thumb/";
        
        $exp = explode(".", $value);
        
        $extVal = $exp[1];
        
        if ($extVal == 'jpeg' || $extVal == 'JPEG') {
            $extNM = -5;
        } else {
            $extNM = -4;
        }
        
        
        if (@copy($dir . $value, $output_dir . str_replace(substr($value, 0, $extNM), $last_id[$key], $value)) && @copy($dir_thumb . $value, $thumbDir . str_replace(substr($value, 0, $extNM), $last_id[$key], $value))) {
            
            if (@unlink($dir . $value) && @unlink($dir_thumb . $value)) {
                
                continue;
                
            } else {
                
                echo "Del Failed";
                
                exit;
            }
            
        }
        
        
    }
    
    $photo_details = $Albums->photosByaId($entityManager, $params['id_album']);
    
    $image_path = 'uploads/' . $photo_details['id_owner'] . '/albums/' . $photo_details['id_album'] . '/thumb/' . $photo_details['id_photo'];
    
    $images = glob($image_path . "*.{jpg,jpeg,png,gif}", GLOB_BRACE);
    
    $file = basename($images[0]);
    
    $explode = explode(".", $file);
    
    if ($explode[0] == $photo_details['id_photo']) {
        
        $ext = $explode[1];
        
    }
    
    $cover = $image_path . '.' . $ext;
    
    $countPhoto = $Albums->countPhotosByaAlbumId($entityManager, $photo_details['id_album']);
    
    
    $message = array(
        "flag" => "1",
        "album" => $photo_details,
        'cover' => $cover,
        'countPhoto' => $countPhoto,
        'album_id' => $params['id_album'],
        'total_photos' => $total_photos,
        'photos_array' => $last_id
    );
    
    
    //echo "<pre>"; print_r($message);die;
    
    echo json_encode($message);
    exit();
    
    
} else {
    echo "Failed";
    exit;
} 