 <?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once "bootstrap.php";

require_once 'model/Comments.php';
require_once 'model/Wall.php';
include 'html/wall/functions.php';
include_once("config.php");

require_once 'classes/Session.class.php';

$session = new Session();
if ($session->getSession("userid") != "" || $session->getSession("userid") != null) {
    $img_p         = $session->getSession("profile_pic");
    $data          = array(
        "userid" => $session->getSession("userid")
    );
    //echo $_SESSION['userid']."USERID".$session->getSession("userid")."::".$session->getSession("profile_pic");
    //    $messages = new Friends();
    $notifications = new NotificationModel();
    
    function slugify($text)
    {
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        $text = trim($text, '-');
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = strtolower($text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        
        if (empty($text)) {
            return 'n-a';
        }
        
        return $text;
    }
    
    if (isset($_POST['post_id'])) {
        //$comment = $_POST['comment-' . $_POST['post_id']];
        $post_id = $_POST['post_id'];
        if ($_POST['type'] == "reply") {
            $parent_id = $_POST['comment_id'];
        } else {
            $parent_id = 0;
        }
        $params              = array();
        $params['author_id'] = $_SESSION['userid'];
        $params['post_id']   = $post_id;
        $params['text']      = $_POST["text"];
        $params['date']      = date('Y-m-d H:i:s');
        $params['parent_id'] = $parent_id;
        CommentsModel::addComment($entityManager, $params); //
        //Add Into NotificationModel
        //If Comment then update to Author Only
        
        $notificationParams['id_user']   = $_SESSION['userid'];
        $notificationParams['id_friend'] = $_SESSION['userid'];
        
        $allWallPhotos = WallModel::getPhotoAlbum($entityManager, $post_id);
        //print_r($allWallPhotos);echo count($allWallPhotos)."K";
        if (count($allWallPhotos) > 0) {
            $photoPath = $allWallPhotos[0]['file'];
            $message   = "Commented On";
        } else {
            $message = "Commented On a Post11";
        }
        //$message = "Commented On <img src='".$photoPath."'/>";
        $notificationParams['message'] = $message;
        $notificationParams['type']    = 6;
        
        //If Comment then update to Author and Comment Creator
        NotificationModel::addNotification($entityManager, $notificationParams);
    }
    if ($_POST['type'] == "reply") {
        $replies            = CommentsModel::getCommentOfReply($_POST['comment_id'], $entityManager);
        $comment['replies'] = $replies;
        
        if (count($comment['replies'])):
            $allReplies = $comment['replies'];
        //print('<pre>');
            
        //print_r($allReplies);
            foreach ($allReplies as $replyKey => $replyValue):
?>
<div class="line"></div>
<div class="slider_two_title" id="reply_<?= $replyValue['id'] ?>">
    <img src="uploads/<?php
                echo $replyValue['profile_pic'];
?>" alt="<?php
                echo $replyValue['firstname'];
?> <?php
                echo $replyValue['lastname'];
?>" style="width:37px;height:37px;">
    <div class="slider_title_style 1single-comment">
        <a href="#"><span class="author_slide_top"><?php
                echo $replyValue['firstname'];
?> <?php
                echo $replyValue['lastname'];
?></span></a>
        <span class="comment_box_bottom_text"><?php
                echo $replyValue['text'];
?></span>
        <a href="#"><p class="like_and_reply"><?php
                echo date('m/d/Y H:i:s', strtotime($replyValue['date']));
?></p></a>                                    
    </div>
</div>

<?php
            endforeach;
        endif;
    }
    if ($_POST['type'] == "comment"):
        if ($_POST['comment_id']) {
            $lastComment = CommentsModel::getCommentAfterCommentId($_POST['post_id'], $_POST['comment_id'], $entityManager);
            foreach ($lastComment as $comment):
                $replies            = CommentsModel::getCommentOfReply($comment['id'], $entityManager);
                $comment['replies'] = $replies;
            //$cuserdetails = WallModel::getUserDetails($entityManager, $comment['author_id']);
                
            //    echo '<pre>'; print_r($comment);
?>                    
            <div class="slider_two_title postcomment" id="comment_<?= $comment['id'] ?>">
                <img src="uploads/<?php
                echo $comment['profile_pic'];
?>" alt="<?php
                echo $comment['firstname'];
?> <?php
                echo $comment['lastname'];
?>" style="width:37px;height:37px;">
                <div class="slider_title_style 1single-comment">
                    <a href="#"><span class="author_slide_top"><?php
                echo $comment['firstname'];
?> <?php
                echo $comment['lastname'];
?></span></a>
                    <span class="comment_box_bottom_text"><?php
                echo $comment['text'];
?></span>
                    <a href="javascript:void(0)" ><p class="like_and_reply"><span>Like</span><span class="comment_reply">Reply</span>  <?php
                echo date('m/d/Y H:i:s', strtotime($comment['date']));
?></p></a>
                                                        
                    <!-- Reply Code -->
                    <div class="comment_box comment_box_reply" >                
                        <img src="uploads/<?php
                echo $comment['profile_pic'];
?>" alt="" style="width:37px;height:37px;" >
                        <span><input id="commentid_<?= $entry['id'] ?>_<?= $comment['id'] ?>" class="comment_box_inline comment_input " placeholder="Write your comment here" type="text"></span>
                        <div class="comment_box_icon comment_box_inline"><a href="#"><span><i class="comment_box_left fa fa-camera"></i></span></a>
                        <a href="#"><span><i class="comment_box_left2 fa fa-smile-o"></i></span></a></div>
                        <h5 class="press_enter_post">Press enter to post or press this button: </h5>
                    </div>
                    <div id="allreply_<?= $comment['id'] ?>">
                    <?php
                if (count($comment['replies'])):
                    $allReplies = $comment['replies'];
                    foreach ($allReplies as $replyKey => $replyValue):
?>
                   <div class="line"></div>
                    <div class="slider_two_title" id="reply_<?= $replyValue['id'] ?>">
                        <img src="uploads/<?php
                        echo $replyValue['profile_pic'];
?>" alt="<?php
                        echo $replyValue['firstname'];
?> <?php
                        echo $replyValue['lastname'];
?>" style="width:37px;height:37px;">
                        <div class="slider_title_style 1single-comment">
                            <a href="#"><span class="author_slide_top"><?php
                        echo $replyValue['firstname'];
?> <?php
                        echo $replyValue['lastname'];
?></span></a>
                            <span class="comment_box_bottom_text"><?php
                        echo Functions::addLink($replyValue['text']);
?></span>
                            <a href="#"><p class="like_and_reply"><?php
                        echo date('m/d/Y H:i:s', strtotime($replyValue['date']));
?></p></a>                                    
                        </div>
                    </div>
                    <div class="line"></div>
                    <?php
                    endforeach;
                endif;
?>
                   </div>
                </div>
                
            </div>
            <div class="line"></div>
        <?php
            endforeach;
        }
    endif;
    
    
    
}
?> 