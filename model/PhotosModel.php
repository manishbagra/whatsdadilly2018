<?php

require_once "bootstrap.php";

class PhotosModel {
     public function __construct() {

    }
    
     public static function addPhoto($entityManager,$params) {
        $entry = new Photos();
        $entry->setWallId($params['wall_id']);
        $entry->setFile($params['file']);
        $entry->setDate($params['date']);
		$entry->setPostId($params['posted_by']);
		
        $entityManager->getConnection()->beginTransaction();
		
        try {
            $res = $entityManager->persist($entry);
            $entityManager->flush();
            $entityManager->getConnection()->commit();
            return $entry->getId();
            
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }
	
	public static function addPhotoNew($entityManager,$data) {
	
	$sql= "INSERT INTO `photos` (`wall_id`,`file`,`date`,`posted_by`) values ('".$data['wall_id']."','".$data['file']."','".$data['date']."','".$data['posted_by']."')";
	$entityManager->getConnection()->executeQuery($sql);
	return $entityManager->getConnection()->lastInsertId();
		
	}	
	
	public static function addPhotoNewByAnotherUser($entityManager,$params) {
	
	$sql= "INSERT INTO `photos` (`wall_id`,`file`,`date`,`posted_by`) values ('".$params['wall_id']."','".$params['file']."','".$params['date']."','".$params['author_id']."')";
	$entityManager->getConnection()->executeQuery($sql);
	return $entityManager->getConnection()->lastInsertId();
		
	}	
    
    public static function getPhotos($post_id,$entityManager)
    {
       
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "SELECT p
                FROM Wall w,Photos p
                WHERE w.id=p.wall_id and  p.wall_id =:post order by p.date desc";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'post' => $post_id
            ));
        
             return $query->getArrayResult();
			 
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
	 public static function getIsCover($post_id,$entityManager)
    {
       
       $sql = "SELECT is_cover FROM `photos` WHERE wall_id = '".$post_id."' ";
				
		$query = $entityManager->getConnection()->executeQuery($sql);
		
		$result = $query->fetchAll();
		
		return $result;
		
    }
	
	
	 public static function getPhoto($post_id,$entityManager)
    {
      
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "SELECT p,w.author_id
                FROM Wall w,Photos p
                WHERE w.id=p.wall_id and  p.wall_id =:post order by p.date desc";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'post' => $post_id
            ));
        
             return $query->getArrayResult();
			 
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
	
	
	
	
	
	public function getPhotosByPostedId($posted_id,$entityManager) {
		
		$sql = "SELECT * FROM `photos` WHERE posted_by = '".$posted_id."' AND is_cover = 0 AND author_id = 0 ";
				
		$query = $entityManager->getConnection()->executeQuery($sql);
		
		$result = $query->fetchAll();
		
		return $result;
		
	}
    
}