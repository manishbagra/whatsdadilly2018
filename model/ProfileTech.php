<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 ----------------------------------------------------------- ( model )
 */
require_once "bootstrap.php";

class ProfileTech {

    protected $res;

    //put yor code here
    public function __construct() {
        
    } 

    static public function getCitiesForBasic($data, $entityManager) {
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "SELECT l FROM Location l WHERE l.city LIKE :search";
            $query = $entityManager->createQuery($dql);
            $query->setMaxResults(10);
            $query->setParameter('search', $data['text'] . '%');
            return $query->getArrayResult();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }
    static public function setUserActivity($data, $entityManager){
        $lastLogin = new LastLogin();
        $lastLogin->setIPAddress($data['ipaddress']);
        $lastLogin->setLoginDate($data['login_date']);
        $lastLogin->setUserId($data['user_id']);
        $lastLogin->setDevice($data['device']);
        $entityManager->getConnection()->beginTransaction();
        try {
            $res = $entityManager->persist($lastLogin);
            $entityManager->flush();
            $entityManager->getConnection()->commit();
            return $lastLogin->getLoginId();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    // added by Tech 

        public static function addCoverPhoto($entityManager, $params) {

        // $friend = new Photos();
        // $friend->setWallId($params['wall_id']);
        // $friend->setFile($params['file']);
        // $friend->setCoverFlag($params['is_cover']);
        // $params['date'] = $friend->getDatetime(true);
//        $entityManager->persist($friend);
//        $entityManager->flush();


        $entityManager->getConnection()->executeQuery('DELETE FROM `photos` WHERE is_cover = 1 AND author_id=' . $params['author_id'] );

        // $sql = "insert into photos (`wall_id`, `file`, `date`,`is_cover`,`author_id`) values ( '". $params['wall_id']. "','" . $params['file'] ."','"  . Date('Y-m-d h:m ') . "','" . $params['is_cover'] ."','100')";

         $sql = "INSERT INTO `photos` (`wall_id`, `file`, `date`, `is_cover`, `author_id`) VALUES ( '". $params['wall_id']. "', '" . $params['file'] ."', '" . Date('Y-m-d h:m ') . "', '" . $params['is_cover'] ."', '" . $params['author_id'] ."')";

        $entityManager->getConnection()->executeQuery($sql);


//        $photo = $entityManager->getRepository('Photos')->findOneBy($params);
//        return $photo;
    }

    public static function getCoverPhoto($entityManager,$user_id) {
        $query = $entityManager->getConnection()->executeQuery('SELECT file FROM `photos` WHERE is_cover = 1 AND author_id = ' . $user_id);
        $result = $query->fetchAll();
        //$result2 = array();
        $coverPicName = '';
        foreach ($result as $line) {
            $coverPicName = $line['file'];
        }
        return $coverPicName;
    }

    public static function removeCoverPhoto($entityManager,$params) {

        $query = $entityManager->getConnection()->executeQuery('DELETE FROM `photos` WHERE is_cover = 1');

    }

    // end added by tech. 

}
?>