<?php
require_once "bootstrap.php";

class Insta {

    public function __construct() {

    }
	
	public static function addNetworkToken($entityManager,$arr)
	{
		$instaobj = new Insta();
		
		$sql = "INSERT INTO `sh_network_token` (`networkname`,`auth_token`,`auth_secret`,`user_id`,`screen_name`, `screen_id`,`img_link`) values ('".$arr['networkname']."','".$arr['oauth_token']."','".$arr['oauth_secret']."','".$arr['user_id']."','".$arr['scr_name']."','".$arr['scr_id']."','".$arr['img_link']."')";
		
		$query = $entityManager->getConnection()->executeQuery($sql);
	    
		return $entityManager->getConnection()->lastInsertId();
	}
	
	public function time_elapsed_string($datetime, $full = false) {
		
			$now = new DateTime;
			$ago = new DateTime($datetime);
			 
			$diff = $now->diff($ago);
            
			$diff->w = floor($diff->d / 7);
			$diff->d -= $diff->w * 7;
    
			$string = array(
				'y' => 'year',
				'm' => 'month',
				'w' => 'week',
				'd' => 'day',
				'h' => 'hour',
				'i' => 'minute',
				's' => 'second',
			);
			
			foreach ($string as $k => &$v) {
				
				if ($diff->$k) {
					$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
					
				} else {
					unset($string[$k]);
				}
			}
             
			if (!$full) $string = array_slice($string, 0, 1);
			return $string ? implode(', ', $string) . ' ago' : 'just now';
		}
	
	
	
	public  function addNetworkTokenYoutube($entityManager,$arr)
	{
		$sql = "INSERT INTO `sh_youtube_network` (`channel_id`,`likes`,`favorites`,`uploads`,`watchHistory`, `watchLater`,`oauth_token`,`oauth_secret`,`user_id`) values ('".$arr['channel_id']."','".$arr['likes']."','".$arr['favorites']."','".$arr['uploads']."','".$arr['watchHistory']."','".$arr['watchLater']."','".$arr['oauth_token']."','".$arr['oauth_secret']."','".$arr['user_id']."')";
		
		$query = mysql_query($sql);
		
	}
	
	public  function getLastUser($entityManager)
	{
		$sql= "SELECT * FROM `sh_users` ORDER BY `user_id` DESC  LIMIT 0,1";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetch();
		return $result;
	}
	
	public function getInvitedEmail($entityManager,$user_id)
	{
		$sql= "SELECT * FROM `invited_contact` WHERE `user_id` ='".$user_id."'";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetchAll();
		return $result;
	}
	
	public function deleteInviteId($entityManager,$id)
	{
		$sql = "DELETE from `invited_contact`  WHERE `id` = '".$id."' ";
     
        $query = $entityManager->getConnection()->executeQuery($sql);
		
		return true;
	}
	
	public  function updateNetworkTokenYoutube($entityManager,$username,$image)
	{
		$sql = "UPDATE `sh_youtube_network` SET `given_name` = '".$username."' , `picture` = '".$image."' ";
		
		$query = mysql_query($sql);
		
	}
	
	public function curl_output($url)
	{
		$ch =  curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_TIMEOUT, 3);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
		$curlResult = curl_exec($ch);
		$finalResult = json_decode($curlResult);
		return $finalResult;
		
	}
	
	
	public function checkAlreadyExist($entityManager,$arr)
	{
		$entry = new Insta();
		$sql= "SELECT count(*) c FROM `sh_network_token` WHERE `screen_id` = '".$arr['scr_id']."' AND  `user_id` =  '".$arr['user_id']."' ";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetchAll();
		return $result[0]['c'];
	}
	
	public function checkAlreadyExistYoutube($entityManager,$arr)
	{
		$entry = new Insta();
		$sql= "SELECT count(*) c FROM `sh_youtube_network` WHERE  `user_id` =  '".$arr['user_id']."' ";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetchAll();
		return $result[0]['c'];
	}
	
	
	public function checkExistYoutube($entityManager,$user_id)
	{
		$sql= "SELECT * FROM `sh_youtube_network` WHERE  `user_id` =  '".$user_id."' ";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetchAll();
		return $result;
	}
	
	public function addFeedPost($entityManager,$med)
	{
		$instaobj = new Insta();
		$sql= "INSERT INTO `wall` (`insta_post_id`,`author_id`,`owner_id`,`link`,`link_photo`,`date`,`post_type`) values ('".$med['ig_p_id']."','".$med['auth_id']."','".$med['own_id']."','".$med['link']."','".$med['img_link']."','".$med['datetime']."','".$med['posttype']."')";
		$query = $entityManager->getConnection()->executeQuery($sql);
	    return $entityManager->getConnection()->lastInsertId();
	}
	
	public function getInstaAccount($entityManager,$uId)
	{
		$entry = new Insta();
		$sql= "SELECT * FROM `sh_network_token` WHERE `user_id` = '".$uId."' AND  `networkname` = 'instagram'  ";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetchAll();
		return $result;
	}
	
	public function getYouTubeAccount($entityManager,$uId)
	{
		$entry = new Insta();
		$sql= "SELECT * FROM `sh_youtube_network` WHERE `user_id` = '".$uId."' ";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetch();
		return $result;
	}
	
	public function getInstaUsername($entityManager,$uId)
	{
		$entry = new Insta();
		$sql= "SELECT * FROM `sh_network_token` WHERE `user_id` = '".$uId."' AND  `networkname` = 'instagram'  ";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetchAll();
		return $result;
	}
	
	public function getInstaInfoByScreenId($entityManager,$screen_id)
	{
		$entry = new Insta();
		$sql= "SELECT * FROM `sh_network_token` WHERE `screen_id` = '".$screen_id."' AND  `networkname` = 'instagram'  ";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetchAll();
		return $result;
	}
	
	public function getInstaPostIds($entityManager)
	{
		$entry = new Insta();
		$sql= "SELECT *  FROM `wall` WHERE `post_type` = 2 ";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetchAll();
		foreach($result as $ids){
			$idss[] = $ids['insta_post_id'];
		}
		
		return $idss;
	}
	
	public function getUserSocialAccount($entityManager,$user_id,$netw_name)
	{
		$sql= "SELECT *  FROM `sh_network_token` WHERE `user_id` = '".$user_id."' AND `networkname` = '".$netw_name."'  ";
		
		$query = $entityManager->getConnection()->executeQuery($sql);
		
		$result = $query->fetchAll();
		
		return $result ;
	}
	
	
}	