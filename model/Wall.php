<?php

require_once "bootstrap.php";
require_once "mysqli_connection.php";
class WallModel
{
    
    public static function addOtherWallEntry($entityManager, $params)
    {
        $entry = new Wall();
        $entry->setAuthorId($params['author_id']);
        $entry->setOwnerId($params['owner_id']);
        $entry->setText($params['text']);
        
        $entry->setLink($params['link']);
        $entry->setLinkDescription($params['link_description']);
        $entry->setLinkPhoto($params['link_photo']);
        $entry->setLinkTitle($params['link_title']);
        $entry->setDate($params['date']);
        if ($params['author_id'] != $params['owner_id']) {
            $entry->setPostType(1);
        } else {
            $entry->setPostType(0);
        }
        
        
        $entityManager->getConnection()->beginTransaction();
        try {
            $res = $entityManager->persist($entry);
            $entityManager->flush();
            $entityManager->getConnection()->commit();
            return $entry->getId();
        }
        catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }
    public static function getSingleWallId($entityManager, $wall_id)
    {
        
        $sql = "SELECT * FROM wall w , sh_users u WHERE w.id = '" . $wall_id . "' AND u.user_id=w.author_id ";
        
        $query = $entityManager->getConnection()->executeQuery($sql);
        
        $result = $query->fetch();
        
        return $result;
        
    }
    
    public static function getAutherId($entityManager, $wall_id)
    {
        
        $sql = "SELECT author_id,text FROM wall  WHERE id = '" . $wall_id . "' ";
        
        $query = $entityManager->getConnection()->executeQuery($sql);
        
        $result = $query->fetch();
        
        return $result['author_id'];
        
    }
    public static function getAutherIdForReply($entityManager, $wall_id)
    {
        
        $sql = "SELECT author_id,text FROM wall  WHERE id = '" . $wall_id . "' ";
        
        $query = $entityManager->getConnection()->executeQuery($sql);
        
        $result = $query->fetch();
        
        return $result;
        
    }
    
    public static function getOneWallRow($wall_id, $entityManager)
    {
        
        $sql = "SELECT  distinct(w.id),w.text,w.link,w.count_photos,w.link_title,
				w.link_description,w.link_photo,w.date,w.post_type,u.user_id,u.firstname, 
				u.lastname,u.orginalpic,u.email,w.author_id as user_id,w.author_id,w.owner_id,w.post_type as type
				FROM wall w 
				join sh_users u WHERE u.user_id = w.author_id and w.id = '" . $wall_id . "' 
				group by w.id order by w.id desc";
        
        $query = $entityManager->getConnection()->executeQuery($sql);
        
        $result = $query->fetchAll();
        
        return $result;
    }
    
    public static function addWallEntry($entityManager, $params)
    {
		
        $entry = new Wall();
        $entry->setAuthorId($params['author_id']);
        $entry->setOwnerId($params['owner_id']);
        $entry->setText($params['text']);
        if (isset($params['link'])) {
            $entry->setLink($params['link']);
        }
        if (isset($params['link_description'])) {
            $entry->setLinkDescription($params['link_description']);
        }
        if (isset($params['link_photo'])) {
            $entry->setLinkPhoto($params['link_photo']);
        }
        if (isset($params['link_title'])) {
            $entry->setLinkTitle($params['link_title']);
        }
        $entry->setDate($params['date']);
        if ($params['author_id'] != $params['owner_id']) {
            $entry->setPostType(1);
        } else {
            $entry->setPostType(0);
        }
        
        $entityManager->getConnection()->beginTransaction();
        try {
            $res = $entityManager->persist($entry);
            $entityManager->flush();
            $entityManager->getConnection()->commit();
            return $entry->getId();
        }
        catch (Exception $e) {
            echo "Ki Hoya 22";
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }
    
    public function updateCount($entityManager, $id, $countPhoto)
    {
        
        $sql = "UPDATE `wall` SET `count_photos` = '" . $countPhoto . "' WHERE `id` = '" . $id . "' ";
        
        $entityManager->getConnection()->executeQuery($sql);
        
        return 1;
        
    }
    
    public static function getPhotoAlbum($entityManager, $wall_id)
    {
        //$entityManager->getConnection()->beginTransaction();
        try {
            $query  = $entityManager->getConnection()->executeQuery('select file,id from photos where wall_id=' . $wall_id);
            $result = $query->fetchAll();
            
            return $result;
        }
        catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
    
    public static function getEntries($entityManager, $user_id = false)
    {
        if (!$user_id) {
            $user_id = $_SESSION['userid'];
        }
        
        $entityManager->getConnection()->beginTransaction();
        
        try {
            
             $SQL = 'SELECT distinct(w.id),w.text,w.link,w.count_photos,w.link_title,w.link_description,w.link_photo,w.date,
				w.author_id as user_id,w.author_id,w.owner_id,
				w.post_type as type
				FROM friends f
				RIGHT JOIN wall as w on (w.author_id = f.id_owner or w.author_id=f.id_friend) 
				WHERE w.post_type != 2
				AND  (f.id_friend=' . $user_id . '
				OR f.id_owner=' . $user_id . '
				 
				OR w.owner_id=' . $user_id . ')
				
				 order by w.id desc'; 
            
            $query = $entityManager->getConnection()->executeQuery($SQL);
            
            
            
            $result = $query->fetchAll();
            
            return $result;
            
        }
        catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
    
    
    public static function getEntriesInfinit($entityManager, $limit1, $limit2)
    {
        $user_id = $_SESSION['userid'];
        
        $entityManager->getConnection()->beginTransaction();
        
        try {
            
            $query = $entityManager->getConnection()->executeQuery('SELECT distinct(w.id),w.text,w.link,w.count_photos,w.link_title,w.link_description,w.link_photo,w.date,
			w.author_id as user_id,w.author_id,w.owner_id,
			w.post_type as type
			FROM friends f
			RIGHT JOIN wall as w on (w.author_id = f.id_owner or w.author_id=f.id_friend) 
			WHERE f.status= 1
			AND  (f.id_friend = ' . $user_id . ' OR f.id_owner = ' . $user_id . ' OR w.author_id=' . $user_id . ' )
			
			AND w.post_type != 2 
			
			order by id desc LIMIT ' . $limit1 . ' , ' . $limit2 . ' ');
            
            $result = $query->fetchAll();
            
            return $result;
            
        }
        catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
    
    public static function getTweetsEntries($entityManager, $user_id = false, $curTime)
    {
		
        if (!$user_id) {
            $user_id = $_SESSION['userid'];
        }
        $entityManager->getConnection()->beginTransaction();
        try {
			 $sql ='SELECT distinct(w.id),w.text,w.link,w.link_title,w.link_description,w.link_photo,w.date,w.author_id as user_id,w.author_id,w.owner_id,w.post_type as type FROM friends f
            RIGHT JOIN wall as w on w.author_id = f.id_owner or w.author_id=f.id_friend
            WHERE (f.id_friend=' . $user_id . ' OR f.id_owner=' . $user_id . ' OR w.owner_id='. $user_id .') order by w.id desc LIMIT 1'; 
            $query  = $entityManager->getConnection()->executeQuery($sql);
            $result = $query->fetchAll();
            /* $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
            'owner' => $user_id
            ));
            * */
            return $result;
        }
        catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
    
    public static function getUserWall($entityManager, $data)
    {
        $entityManager->getConnection()->beginTransaction();
        try {
                $query = "SELECT  distinct(w.id),w.text,w.link,w.count_photos,w.link_title,
				w.link_description,w.link_photo,w.date,w.post_type,u.user_id,u.firstname, 
				u.lastname,u.orginalpic,u.email,w.author_id as user_id,w.author_id,w.owner_id,
				w.post_type as type
				FROM wall w 
				join sh_users u WHERE 
				((u.user_id = w.author_id and u.user_id = '" . $data['userid'] . "' and w.post_type = 0) or
				
				((w.owner_id = '" . $data['userid'] . "' or w.author_id = '" . $data['userid'] . "') and 
				w.post_type=1))  
				
				
				group by w.id order by w.id desc"; 
            
            
            $SQL = $entityManager->getConnection()->executeQuery($query);
            
            $result = $SQL->fetchAll();
            
            return $result;
            
        }
        catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
        
    }
    
    public static function getUserWallNew($entityManager, $data, $wall_id)
    {
        
        $entityManager->getConnection()->beginTransaction();
        
        try {
            // /*$dql = "SELECT distinct(w.id),w.text,w.link,w.link_title,w.link_description,w.link_photo,w.date,u.firstname, u.lastname,w.author_id as user_id,w.author_id,w.owner_id,w.post_type as type FROM Wall w join UserRegister u WHERE ((u.user_id=w.author_id and u.user_id =:owner) or ((w.author_id=:owner or w.owner_id =:owner) and w.post_type=1))  group by w.id order by w.date desc";*/
            
             $query = "SELECT  distinct(w.id),w.text,w.link,w.count_photos,w.link_title,
				w.link_description,w.link_photo,w.date,w.post_type,u.user_id,u.firstname, 
				u.lastname,u.orginalpic,u.email,w.author_id as user_id,w.author_id,w.owner_id,w.post_type as type
				FROM wall w 
				join sh_users u WHERE 
				((u.user_id = w.author_id and u.user_id = '" . $data['userid'] . "' and w.post_type = 0 and w.id <> '" . $wall_id . "') or
				
				((w.author_id = '" . $data['userid'] . "' or w.owner_id = '" . $data['userid'] . "') and w.post_type=1 and w.id <> '" . $wall_id . "' ))  
				
				group by w.id order by w.id desc";
            
            $SQL = $entityManager->getConnection()->executeQuery($query);
            
            $result = $SQL->fetchAll();
            
            return $result;
            
        }
        catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
        
    }
    
    public static function getProfileSummeryWall($entityManager, $data)
    {
        
        
        $sql = "SELECT distinct(w.id),w.text,w.link,w.link_title,
			   w.link_description,w.link_photo,w.date,w.post_type,u.user_id,u.firstname, 
			   u.lastname,u.orginalpic,u.email,w.author_id as user_id,w.author_id,w.owner_id,w.post_type
			   FROM sh_users u, wall w
			   WHERE 
			   (u.user_id = w.author_id and u.user_id = '" . $data['userid'] . "'and w.post_type = 0)
			  group by w.id order by w.date desc";
        
        $query = $entityManager->getConnection()->executeQuery($sql);
        
        $result = $query->fetchAll();
        
        return $result;
    }
    
    
    public static function getUserInstaWall($entityManager, $data)
    {
        $entityManager->getConnection()->beginTransaction();
        
        try {
            // $dql = "SELECT distinct(w.id),w.text,w.link,w.link_title,w.link_description,w.link_photo,w.date,u.firstname, u.lastname,w.author_id as user_id,w.author_id,w.owner_id,w.post_type as type FROM Wall w join UserRegister u WHERE ((u.user_id=w.author_id and u.user_id =:owner) or ((w.author_id=:owner or w.owner_id =:owner) and w.post_type=1))  group by w.id order by w.date desc";
            $dql = "SELECT distinct(w.id),w.text,w.link,w.link_title,
				w.link_description,w.link_photo,w.date,w.post_type,u.user_id,u.firstname, 
				u.lastname,u.orginalpic,u.email,w.author_id as user_id,w.author_id,w.owner_id,w.post_type as type
				FROM Wall w 
				 
				join UserRegister u WHERE 
				u.user_id =:owner and w.post_type=2
				group by w.id order by w.date desc";
            
            
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'owner' => $data['userid']
            ));
            
            return $query->getArrayResult();
        }
        catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
        
    }
    
    
    
    public static function getUserInfiniteWall($entityManager, $limit1, $limit2, $data = null)
    {
        
        $entityManager->getConnection()->beginTransaction();
        
        try {
            // /*$dql = "SELECT distinct(w.id),w.text,w.link,w.link_title,w.link_description,w.link_photo,w.date,u.firstname, u.lastname,w.author_id as user_id,w.author_id,w.owner_id,w.post_type as type FROM Wall w join UserRegister u WHERE ((u.user_id=w.author_id and u.user_id =:owner) or ((w.author_id=:owner or w.owner_id =:owner) and w.post_type=1))  group by w.id order by w.date desc";*/
            
            $query = "SELECT  distinct(w.id),w.text,w.link,w.count_photos,w.link_title,
				w.link_description,w.link_photo,w.date,w.post_type,u.user_id,u.firstname, 
				u.lastname,u.orginalpic,u.email,w.author_id as user_id,w.author_id,w.owner_id,w.post_type as type
				FROM wall w 
				join sh_users u WHERE 
				((u.user_id = w.author_id and u.user_id = '" . $data['userid'] . "' and w.post_type = 0) or
				
				((w.author_id = '" . $data['userid'] . "' or w.owner_id = '" . $data['userid'] . "') and w.post_type=1))  
				
				group by w.id order by w.date desc LIMIT $limit1,$limit2 ";
            
            $SQL = $entityManager->getConnection()->executeQuery($query);
            
            $result = $SQL->fetchAll();
            
            return $result;
            
        }
        catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
        
    }
    
    public static function getInfiniteWall($entityManager, $limit1, $limit2, $data = null)
    {
        
        $user_id = $_SESSION['userid'];
        
        $entityManager->getConnection()->beginTransaction();
        
        try {
            
            $dql = $entityManager->getConnection()->executeQuery('SELECT distinct(w.id),w.text,w.link,w.link_title,w.link_description,w.link_photo,w.post_type,w.date,w.author_id as user_id FROM friends f
            INNER JOIN wall as w on w.author_id = f.id_owner or w.author_id = f.id_friend
             WHERE 
			 f.id_friend=' . $user_id . ' OR f.id_owner=' . $user_id . ' order by w.date desc LIMIT ' . $limit1 . ',' . $limit2);
            
            $result = $dql->fetchAll();
            
            $query = $entityManager->createQuery($dql);
            
            $query->setParameters(array(
                'owner' => $user_id
            ));
            
            
            return $result;
            
        }
        catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
    
    public static function getUserDetails($entityManager, $user_id)
    {
        $entityManager->getConnection()->beginTransaction();
        try {
            $query  = $entityManager->getConnection()->executeQuery("select user_id,profile_pic,firstname,lastname,email,gender from sh_users where `user_id` = '" . $user_id . "' ");
            $result = $query->fetchAll();
            return $result;
        }
        catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
    
    public static function getUserCoverPic($entityManager, $user_id)
    {
        $entityManager->getConnection()->beginTransaction();
        try {
            $query  = $entityManager->getConnection()->executeQuery('select file from photos where author_id=' . $user_id);
            $result = $query->fetchAll();
            return $result;
        }
        catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
    
    public static function getPrviousPost($entityManager, $post_id)
    {
        //  $entityManager->getConnection()->beginTransaction();
        try {
            $dql   = "SELECT w.id,w.author_id,w.owner_id,w.post_type as type,w.text,w.link,w.link_title,w.link_description,w.link_photo,w.date,u.firstname, u.lastname FROM Wall w inner join UserRegister u WHERE  u.user_id=w.author_id and w.id =:wall order by w.date desc";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'wall' => $post_id
            ));
            //            echo '<pre>';
            //            print_r($query->getArrayResult()); die();
            return $query->getArrayResult();
        }
        catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
    
    public static function getLatestPost($entityManager, $post_id)
    {
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql   = "SELECT w.id,w.text,w.link,w.link_title,w.link_description,w.link_photo,w.date,u.firstname, u.lastname FROM Wall w inner join UserRegister u WHERE  u.user_id=w.author_id and w.id >:wall order by w.date desc";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'wall' => $post_id
            ));
            //            echo '<pre>';
            //            print_r($query->getArrayResult()); die();
            return $query->getArrayResult();
        }
        catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
    
    
    public static function time_elapsed_string($ptime)
    {
        $etime = time() - strtotime($ptime);
        
        if ($etime < 1) {
            return '0 seconds';
        }
        
        $a        = array(
            365 * 24 * 60 * 60 => 'year',
            30 * 24 * 60 * 60 => 'month',
            24 * 60 * 60 => 'day',
            60 * 60 => 'hour',
            60 => 'minute',
            1 => 'second'
        );
        $a_plural = array(
            'year' => 'years',
            'month' => 'months',
            'day' => 'days',
            'hour' => 'hours',
            'minute' => 'minutes',
            'second' => 'seconds'
        );
        
        foreach ($a as $secs => $str) {
            $d = $etime / $secs;
            if ($d >= 1) {
                $r = round($d);
                return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
            }
        }
    }
    
    public function time_string($datetime, $full = false)
    {
        
        $now  = new DateTime;
        $ago  = new DateTime($datetime);
        $diff = $now->diff($ago);
        
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
        
        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second'
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }
        
        if (!$full)
            $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
    
    public static function getWallPostOwnerId($entityManager, $params)
    {
        
        
        $sql = "SELECT  photos.file FROM wall, photos 
               WHERE wall.id=photos.wall_id and wall.id = '" . $params['post_id'] . "' ";
        
        
        $query = $entityManager->getConnection()->executeQuery($sql);
        
        $result = $query->fetchAll();
        
        
        return $result;
    }
    
    public static function getUserCommentedIds($entityManager, $postid, $comented_id)
    {
        
        $sql    = "SELECT DISTINCT author_id FROM comments WHERE post_id = '" . $postid . "' AND author_id <> '" . $comented_id . "'  ";
        $query  = $entityManager->getConnection()->executeQuery($sql);
        $result = $query->fetchAll();
        return $result;
    }
    
    public static function addVideosTitle($userid,$owner_id,$post_type, $title, $time, $entityManager)
    {
        $sql = "INSERT INTO `wall` (`author_id`,`owner_id`,`text`,`date`,`post_type`) values ('" . $userid . "','" . $owner_id . "','" . $title . "','" . $time . "','".$post_type."')";
        $entityManager->getConnection()->executeQuery($sql);
        return $entityManager->getConnection()->lastInsertId();
    }
    
   public function create_movie_thumb($src_file,$mediapath,$output_dir)
	{
		global $CONFIG, $ERROR;

		$CONFIG['ffmpeg_path'] = $output_dir;
		
		$dir_img = $output_dir;
		
		$CONFIG['fullpath'] = $dir_img."thumbnails/";

		$src_file = $src_file;
		
		$name_file = explode(".",$mediapath);
		
		$imgname1 = $name_file[0]."_01.jpg";
		$imgname2 = $name_file[0]."_02.jpg";
		$imgname3 = $name_file[0]."_03.jpg";
		
		$dest_file1 = $CONFIG['fullpath'].$imgname1;
		
		$dest_file2 = $CONFIG['fullpath'].$imgname2;
		
		$dest_file3 = $CONFIG['fullpath'].$imgname3;
		

		$output = array();

		if(PHP_OS==='WINNT'){
			
			$sec1 = 05; $sec2 = 10 ; $sec3 = 15;
			
			$cmd1 = 'ffmpeg -i ' . str_replace("\\", "/", $src_file) . ' -an -ss 00:00:'.$sec1.'  -s 375x220 -r 1 -vframes 1 -y ' . str_replace("\\", "/", $dest_file1);
			
			exec ($cmd1, $output1, $retval1);
			
			$cmd2 = 'ffmpeg -i ' . str_replace("\\", "/", $src_file) . ' -an -ss 00:00:'.$sec2.'  -s 375x220 -r 1 -vframes 1 -y ' . str_replace("\\", "/", $dest_file2);
			
			exec ($cmd2, $output2, $retval2);
			
			$cmd3 = 'ffmpeg -i ' . str_replace("\\", "/", $src_file) . ' -an -ss 00:00:'.$sec3.'  -s 375x220 -r 1 -vframes 1 -y ' . str_replace("\\", "/", $dest_file3);
			
			exec ($cmd3, $output3, $retval3);
			
		}else{
			
			$sec1 = 05; $sec2 = 10 ; $sec3 = 15;
			
			$cmd1 = 'ffmpeg -i ' . str_replace("\\", "/", $src_file) . ' -an -ss 00:00:'.$sec1.'  -s 375x220 -r 1 -vframes 1 -y ' . str_replace("\\", "/", $dest_file1);
			
			exec ($cmd1, $output1, $retval1);
			
			$cmd2 = 'ffmpeg -i ' . str_replace("\\", "/", $src_file) . ' -an -ss 00:00:'.$sec2.'  -s 375x220 -r 1 -vframes 1 -y ' . str_replace("\\", "/", $dest_file2);
			
			exec ($cmd2, $output2, $retval2);
			
			$cmd3 = 'ffmpeg -i ' . str_replace("\\", "/", $src_file) . ' -an -ss 00:00:'.$sec3.'  -s 375x220 -r 1 -vframes 1 -y ' . str_replace("\\", "/", $dest_file3);
			
			exec ($cmd3, $output3, $retval3);
			
		}
		
		if(file_exists($dest_file1)){
			$thumb[] = $dest_file1;
		}
		if(file_exists($dest_file2)){
			$thumb[] = $dest_file2;
		}
		if(file_exists($dest_file3)){
			$thumb[] = $dest_file3;
		}

		$return = $thumb;
		
		return $return;
	}
    
    function mp4toflv($src_file, $mediapath, $output_dir)
    {
        global $CONFIG, $ERROR;
        
        $CONFIG['fullpath'] = $output_dir . "videomp4/";
        
        $name_file = explode(".", $mediapath);
        
        $imgname = $name_file[0] . ".mp4";
        
        $dest_file = $CONFIG['fullpath'] . $imgname;
        
        if (preg_match("#[A-Z]:|\\\\#Ai", __FILE__)) {
            
            $cur_dir = substr(dirname(__FILE__), 0, -6);
            
            $src_file = strtr($src_file, '/', '\\');
            
            $ff_dest_file = strtr($dest_file, '/', '\\');
            
        } else {
            
            $src_file = escapeshellarg($src_file);
            
            $ff_dest_file = escapeshellarg($dest_file);
        }
        
        $output = array();
        
        if (PHP_OS === 'WINNT') {
            
            //$cmd ="ffmpeg -i $src_file -f mp4 -vcodec libx264 -preset fast -profile:v main -acodec aac $ff_dest_file"; 
            
            $cmd = "ffmpeg -i '" . $src_file . "' -c:a copy -c:v libx264 -preset superfast -profile:v baseline '" . $ff_dest_file . "'";
            
            exec($cmd . " 2>&1", $out, $ret);
            
        } else {
            
            $cmd = "ffmpeg -i '" . $src_file . "' -c:a copy -c:v libx264 -preset superfast -profile:v baseline '" . $ff_dest_file . "'";
            
            exec($cmd . " 2>&1", $out, $ret);
            
        }
        
        if ($ret) {
            
            echo "There was a problem!\n";
            echo "<pre>";
            print_r($out);
            die;
            
        } else {
            
            if (file_exists($src_file)) {
                @unlink($src_file);
            }
            
            $return = $dest_file;
        }
        
        return $return;
        
    }
    
    public function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }
        
        return $bytes;
    }
    
    public function getWallPostByUserId($entityManager, $userId)
    {
        
        $sql = "SELECT id FROM `wall` WHERE `author_id` = '" . $userId . "' ORDER BY id DESC ";
        
        $query = $entityManager->getConnection()->executeQuery($sql);
        
        $result = $query->fetchAll();
        
        return $result;
    }
    public static function getAuthorId($entityManager, $post_id)
    {
        $sql = "SELECT author_id FROM `wall` WHERE `id` = '" . $post_id . "' ";
        
        $query = $entityManager->getConnection()->executeQuery($sql);
        
        $result = $query->fetch();
        
        return $result['author_id'];
    }
	 public static function getAuthorOwnerId($entityManager, $post_id)
    {
        $sql = "SELECT author_id,owner_id FROM `wall` WHERE `id` = '" . $post_id . "' ";
        
        $query = $entityManager->getConnection()->executeQuery($sql);
        
        $result = $query->fetch();
        
        return $result;
    }
	 public static function getOwnerId($entityManager, $post_id)
    {
        $sql = "SELECT author_id FROM `wall` WHERE `id` = '" . $post_id . "' ";
        
        $query = $entityManager->getConnection()->executeQuery($sql);
        
        $result = $query->fetchAll();
        
        return $result;
    }
}
