<?php

require_once "bootstrap.php";

class CommentsModel {
    
     public static function addComment($entityManager,$params) {
        $entry = new Comments();
        $entry->setAuthorId($params['author_id']);
        $entry->setPostId($params['post_id']);
        $entry->setText($params['text']);
        $entry->setDate($params['date']);
		$entry->setParentId($params['parent_id']);
		$entry->setPostType($params['post_type']);
		//echo'<pre>';print_r($entry);die;
        $entityManager->getConnection()->beginTransaction();
        try {
            $res = $entityManager->persist($entry);
            $entityManager->flush();
            $last_id = $entityManager->getConnection()->commit();
            return $entry->getId();
            
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }
	
	public static function addNewComment($entityManager, $sql_cmt)
	{
		$entityManager->getConnection()->executeQuery($sql_cmt);
	    
		return $entityManager->getConnection()->lastInsertId();
	}
	
	public static function getPostCommentType($entityManager,$comment_id)
	{
		$sql = "SELECT * FROM `comments` WHERE `id` = '".$comment_id."' ";
				
		$query = $entityManager->getConnection()->executeQuery($sql);
		
		$result = $query->fetch();
		
		return $result;
	}
	public static function getPostType($entityManager,$post_id)
	{
		$sql = "SELECT post_type FROM `comments` WHERE `post_id` = '".$post_id."' ";
				
		$query = $entityManager->getConnection()->executeQuery($sql);
		
		$result = $query->fetch();
		
		return $result['post_type'];
	}
	
    public static function getLastComments($entityManager,$post_id)
    {

        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "SELECT c.text, c.date, c.author_id
                FROM Comments c
                WHERE c.id =:post order by c.date desc";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'post' => $post_id
            ));
             return $query->getArrayResult();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
    public static function getComments($post_id,$entityManager)
    {
       
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "SELECT c.text, c.date, c.author_id, u.firstname, u.lastname
                FROM Wall w,Comments c,UserRegister u
                WHERE u.user_id=w.author_id and w.id=c.post_id and c.post_id =:post order by c.date desc";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'post' => $post_id
            ));
             return $query->getArrayResult();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
	
	public static function getCommentsMainPost($post_id,$entityManager)
    {
       
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "SELECT c.id,c.text, c.date, c.author_id, u.firstname, u.lastname,c.parent_id,u.profile_pic
                FROM Wall w,Comments c,UserRegister u
                WHERE u.user_id=w.author_id and w.id=c.post_id and c.parent_id=0 and c.post_id =:post order by c.date desc";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'post' => $post_id
            ));
             return $query->getArrayResult();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
	
	public static function getCommentsMainPostWithLimit($post_id,$start,$length,$entityManager)
    {
       
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "SELECT c.id,c.text, c.date, c.author_id, u.firstname, u.lastname,c.parent_id,u.profile_pic
                FROM Comments c,UserRegister u
                WHERE u.user_id=c.author_id  and c.parent_id=0 and c.post_id =:post  order by c.id desc ";
            $query = $entityManager->createQuery($dql)->setFirstResult($start)
                       ->setMaxResults($length);
            $query->setParameters(array(
                'post' => $post_id
            ));
             return $query->getArrayResult();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
	
	public static function getCommentsSinglePostWithLimit($post_id,$start,$length,$entityManager)
    {
       
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "SELECT c.id,c.text, c.date, c.author_id, u.firstname, u.lastname,c.parent_id,u.profile_pic
                FROM Comments c,UserRegister u
                WHERE u.user_id=c.author_id  and c.parent_id=0 and c.post_id =:post  order by c.id ASC ";
            $query = $entityManager->createQuery($dql)->setFirstResult($start)
                       ->setMaxResults($length);
            $query->setParameters(array(
                'post' => $post_id
            ));
             return $query->getArrayResult();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
	
	public static function getCommentAfterCommentId($post_id,$comment_id,$entityManager)
    {
       
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "SELECT c.id,c.text, c.date, c.author_id, u.firstname, u.lastname,c.parent_id,u.profile_pic
                FROM Wall w,Comments c,UserRegister u
                WHERE u.user_id=c.author_id and w.id=c.post_id and c.parent_id=0 and c.post_id =:post  and c.id>:commentId order by c.date desc ";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'post' => $post_id,
				'commentId' => $comment_id
            ));
			
             return $query->getArrayResult();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
	
	public static function getCountCommentsMainPost($post_id,$entityManager)
    {
       
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "SELECT c.post_id
                FROM Comments c
                WHERE c.parent_id=0 and c.post_id =:post order by c.date desc";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'post' => $post_id
            ));
             return $query->getArrayResult();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
	
	public static function getCommentOfReply($comment_id,$entityManager)
    {
       
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "SELECT c.text,c.id, c.date, c.author_id, u.firstname, u.lastname,c.parent_id,u.profile_pic
                FROM Comments c,UserRegister u
                WHERE u.user_id=c.author_id  and c.parent_id= :parent order by c.date desc";
				
            $query = $entityManager->createQuery($dql);            
			$query->setParameters(array(
                'parent' => $comment_id
            ));
             return $query->getArrayResult();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
	
	 public static function getLastcommentID($user_id, $entityManager) {
        $entityManager->getConnection()->beginTransaction();
        try {

            $query = $entityManager->getConnection()->executeQuery('SELECT * FROM wall as w
                    inner JOIN comments as c on c.post_id=w.id
                    where w.author_id=' . $user_id . ' ORDER BY c.id DESC Limit 0,1');
            $result = $query->fetch();
            return $result;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
    public static function checkNewComment($data,$entityManager){
        $entityManager->getConnection()->beginTransaction();
        $user_id = $_SESSION['userid'];
        $last_cmtid = $data['last_cmtid'];
     
        try {

            $query = $entityManager->getConnection()->executeQuery('SELECT * FROM wall as w
                    inner JOIN comments as c on c.post_id=w.id
                    where w.author_id=' . $user_id . ' and c.id > '.$last_cmtid.' ORDER BY c.id DESC');
            $result = $query->fetchAll();
            return $result;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }

    }
	public static function getAllCommentTime($postArray,$afterTime,$entityManager){
			$postIds = implode(",",$postArray);
        try {
            $query = $entityManager->getConnection()->executeQuery('SELECT c.*,u.firstname,u.lastname,u.profile_pic FROM comments as c join sh_users  as u
                WHERE u.user_id=c.author_id and post_id in ('.$postIds.') and parent_id=0 and date > "'.$afterTime.'" ORDER BY date DESC');
            $result = $query->fetchAll();
            return $result;
        } catch (Exception $e) {           
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }

    }
	public static function getAllReplyTime($postArray,$afterTime,$entityManager){
			$postIds = implode(",",$postArray);
        try {
            $query = $entityManager->getConnection()->executeQuery('SELECT c.*,u.firstname,u.lastname,u.profile_pic FROM comments as c join sh_users  as u
                WHERE u.user_id=c.author_id and post_id in ('.$postIds.') and parent_id!=0 and date > "'.$afterTime.'" ORDER BY date DESC');
            $result = $query->fetchAll();
            return $result;
        } catch (Exception $e) {           
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }

    }
	
	
	public function  checkwallnewcomment($postid,$commentid,$user_id,$entityManager)
	{
		
        try {
			
		 $SQL="SELECT *  FROM comments where post_id = '".$postid."' AND `parent_id` =0 AND  id >'".$commentid."' AND  `author_id`  <> '".$user_id."'  ORDER BY date DESC";
	
            $query = $entityManager->getConnection()->executeQuery($SQL);
            $result = $query->fetchAll();
            return $result;
        } catch (Exception $e) {           
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }

		
		
	}
	public function  checkwallnewreply($commentid,$entityManager)
	{
		
        try {
			
		 $SQL="SELECT * FROM comments where parent_id = '".$commentid."' ORDER BY date DESC";
	
            $query = $entityManager->getConnection()->executeQuery($SQL);
            $result = $query->fetchAll();
            return $result;
        } catch (Exception $e) {           
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }

		
		
	}
	
	
	
	
	public function getCommentUserDetails($entityManager,$params)
	{	
		$SQL="SELECT DISTINCT author_id  FROM comments where author_id <> '".$params['author_id']."' AND post_id='".$params['post_id']."' AND parent_id = 0  ";
	 
		$query = $entityManager->getConnection()->executeQuery($SQL);
		
		$result = $query->fetchAll();
		
		return $result;
	}
	
    
}