<?php
//error_reporting(E_ALL);
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//namespace Model;
//require 'bootstrap.php';
//use Entities\Mail;

/**
 * Description of Message
 *
 * @author Marcel
 */
require_once "mysqli_connection.php"; 
class Albums
{
    /**
     *
     * @param Doctrine\ORM\EntityManager $entityManager
     * @param type $id_owner
     * @param type $id_friend
     */
    public static function removeIfEmpty($entityManager, $params)
    {
        $session = new Session();
        if (count($session->getSession('send_photos_path')) == 0) {
            Albums::removeAlbum($entityManager, $params);
            unset($_SESSION['open_album']);
            unset($_SESSION['send_photos']);
            return false;
        } else
            return $entityManager->getRepository('Album')->find($params['id_album']);
    }

    public static function albumsWithCover($entityManager, $params)
    {
        $repo = $entityManager->getRepository('Album');

        $album = new Album();
		
        $list = [];
		
		$count = 0;
		
        foreach ($repo->findBy($params,array('id_album' => 'DESC')) as $album) {
			
            if (!empty($album->getCover()) && $album->getCover() != "" && $album->getCover() != null) {
               
                $listAlbums = new stdClass();
                $listAlbums->albumId = $album->getIdAlbum();
                $listAlbums->albumTitle = $album->getTitle();
				
                $listAlbums->albumCover = Albums::getCover($entityManager, $album->getCover());
				
				
                $listAlbums->photoCount = Albums::getPhotoCount($entityManager, $album->getIdAlbum());
				if ($count++ > 5) { 
					break;
				}
                $list[] = $listAlbums;
				
            }   
			
        }
		
        return $list;
    }

    /**
     *
     * @param Doctrine\ORM\EntityManager $entityManager
     * @param type $id_owner
     * @param type $id_friend
     */
	 
	 
    public function getExtension($str){
         $i = strrpos($str,".");
         if (!$i) { return ""; }
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
    } 

	public function getAlbumsByUserId($entityManager,$id){
		
		$sql = "SELECT * FROM `albums` WHERE id_owner = '".$id."' AND id_wall != 'NULL' ORDER BY id_album DESC ";
				
		$query = $entityManager->getConnection()->executeQuery($sql);
		
		$result = $query->fetchAll();
		
		return $result;
		
	}	
	
	public function countPhotosByaAlbumId($entityManager,$album_id){
		
		$sql = "SELECT COUNT(*) as count FROM `photo_album` WHERE id_album = '".$album_id."' ";
				
		$query = $entityManager->getConnection()->executeQuery($sql);
		
		$result = $query->fetch();
		
		return $result['count'];
		
	}
	
	public function PhotosByaAlbumId($entityManager,$album_id){
		
		$sql = "SELECT * FROM `photo_album` WHERE id_album = '".$album_id."' ";
				
		$query = $entityManager->getConnection()->executeQuery($sql);
		
		$result = $query->fetchAll();
		
		return $result;
		
	}
	
	public function photosByaId($entityManager,$album_id){
		
		$sql = "SELECT * FROM `photo_album` WHERE id_album = '".$album_id."' ";
				
		$query = $entityManager->getConnection()->executeQuery($sql);
		
		$result = $query->fetch();
		
		return $result;
		
	}
	 
    public static function listAlbum($entityManager, $params)
    {
		
		$data = array();
        $repo = $entityManager->getRepository('Album');
        
        $album = new Album();
        $i = 1;
		
		
		foreach ($repo->findBy($params,array('id_album' => 'DESC')) as $album) {
			 if (!empty($album->getCover()) && $album->getCover() != "" && $album->getCover() != null) {
			$data[] = $album;
			 }
		}
		
		// echo '<pre>';
		// print_r($data);die;
	
        $div = '<div class="topic">Topic</div>';
		
		$div .= '<div class="GalleryGrid">';
		
        foreach ($data as $keies => $album) {
			
            if (!empty($album->getCover()) && $album->getCover() != "" && $album->getCover() != null) {
			
                $div .='<div class="item newsClassShd_'.$keies.' galleryShadow"  style="width:16%;float:left">
                            <a href="album_detail.php?id=' . $album->getIdAlbum() . '"  >
                            <img class="img-class" src="' . Albums::getCover($entityManager, $album->getCover()) . '" >
								<div class="title">' . $album->getTitle() . '</div> 
								<div class="view">' . Albums::getPhotoCount($entityManager, $album->getIdAlbum()) . ' <span style="font-size: 10px">Photo</span></div>
                            </a>    
                        </div>';
            }

	   
	   }
		
        $div .= '</div>';
		
		
		
        return $div;
    }

    public static function getPhotos($entityManager, $params)
    {
		$data = array();
	
        $other = @$params['other'];
        unset($params['other']);
		
        $album = $entityManager->getRepository('Album')->find($params['id_album']);
          
		
        $result =  Albums::getUserProfile($entityManager,$album->getIdOwner());	
	   
        $album_info =  Albums::getAlbumInfoByAlbumId($entityManager,$params['id_album']);
	   
	    $location = trim($album_info['location']);
	   
        $pos = '';

        $i = 1;
		$key = -1;
        $photo = new PhotoAlbum();
        $div = '';
		date_default_timezone_set("Asia/Calcutta");
		
		$photoDetails ="
		
		<div class='proImg' style='width:25%;float:left;margin-right:2%;'>
		<img style='border-radius:100%;'  src='uploads/".$result['orginalpic']."'>
		</div>
		<div id='pro_name'>
		<span><a href='#'>".$result['firstname']."&nbsp;".$result['lastname']."</a></span>
		<br/>
		<span style='color:gray;'>".@date('j F', @$album->getDatetime())."</span></div>
		<br/><br/><br/><div id='photo-desc'></div>";
		
		if(trim($location) != ''){
		$photoDetails .= "<span style='color:gray;'> — at  </span><span id='locat'><i class='fa fa-map-marker'></i>&nbsp;<a href='javascript:void(0)'><span>".$location."</span></a></span><br/><br/>";
		}
		
		$photoDetails .= "
		<div class='imge_left top_cmnt'>
			<img src='img/social_img/Social_Media_Feed_03.jpg' alt=''>
		</div>
		<div class='text_box top_cmnt'>
		<form action='javascript:void(0)'>
			<input style='height:47px;' id='post_photo_comment' name='comment_post'  placeholder='Write your comment here' class='form-control post_photo_comment' type='text'>
		    <input type='hidden' id='postid'>
		</form>
		</div>";
		
	   foreach($entityManager->getRepository('PhotoAlbum')->findBy($params,array('id_photo' => 'DESC')) as $keies => $photo) {
		   
		   $data[] = $photo;
	   }
						 
		$addMoreArray = array(
		'id_photo' => -1,
        'id_album' => 0,
        'id_wall' => 0,
        'id_owner' => 0,
        'title' => 'Empty',
        'datetime' => '0000-00-00 00:00:00'
		);				 
	   
	   
		array_unshift($data,$addMoreArray);
            
		$div .= '<div class="GalleryGrid" >';
	   
		foreach($data as $keies => $photos) {
		   
		 
		   if($keies == 0){
			   
			 $div .=  '<a class="item  newsClassShd_'.$keies.' " style="width:16%;"  href="javascript:void(0)" onclick="addMorePhotoInAlbum()">
							<img  class="img-class" src="images/addnew.png" >
						 </a>';
						 
			   
		   }else{
				
         
            $div .= '<div class="item newsClassShd_'.$keies.' gallery-img"  style="width:16%;" id="rem_'.$photos->getIdPhoto().'">';
            
           	if($_SESSION['userid'] == $photos->getIdOwner()){
				
			$div .=	'<i class="fa fa-trash remove-album-image" style="color:red;float:right;margin-right:5px;cursor:pointer;" id="rem_'.$photos->getIdPhoto().'_'.$photos->getIdAlbum().'_'.$photos->getPath().'"  aria-hidden="true"></i>';
           					
			}
			
			$div .=	'<a class="shadowCss" href="javascript:void(0)"  title="'. $photos->getTitle() . '" style="float:left;">
							<img class="img-class" src="'.$photos->getPath().'" >
						 </a>
						 
						<div data-desc="'.$photoDetails.'"></div> 
						
			        </div>';
					
		   		
		   }
			
               
         
        }
		
		 $div .= '</div>';
		
        $div .= "<script>$(document).ready(function(){ 
             $(\".group1\").colorbox({  rel:'group1',iframe:true, width:\"85%\", height:\"85%\"});
             })             
        ;</script>";
        return $div;
		
		
    }


    // public static function getPhotos($entityManager, $params)
    // {

    //     //  $entityManager->getRepository('Photo')->findBy($params);
    //     // $photo = new Photo();
    //     $other = $params['other'];
    //     unset($params['other']);
    //     $album = $entityManager->getRepository('Album')->find($params['id_album']);


    //     $pos = '';

    //     $i = 1;
    //     $photo = new PhotoAlbum();
    //     foreach ($entityManager->getRepository('PhotoAlbum')->findBy($params) as $photo) {
    //         if ($i == 1)
    //             $div .= '<div class="lines">';
    //         $div .= '       
    //     <div class="favicon" onclick="sendCover(\'' . $photo->getIdAlbum() . '\',\'' . $photo->getIdPhoto() . '\'); " >                            </div>        
    //                         <a class="group1" href="photo_detail.php?id=' . $photo->getIdPhoto() . '" title="' . $photo->getTitle() . '" style="float:left;">                            
    //     <img src="' . $photo->getPath() . '" >                            
        
    //                         </a>                            
    //                 ';
    //         if ($i == 5) {
    //             $div .= '</div>';
    //             $i = 1;
    //         } else
    //             $i++;
    //     }
    //     $div .= "<script>$(document).ready(function(){ 
    //          $(\".group1\").colorbox({  rel:'group1',iframe:true, width:\"85%\", height:\"85%\"});
    //          })             
    //     ;</script>";
    //     return $div;
    // }    

   


    public static function delTree($dir)
    {
        $files = glob($dir . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (substr($file, -1) == '/')
                delTree($file);
            else
                unlink($file);
        }

        if (is_dir($dir))
            rmdir($dir);
    }

    public static function getPhotoArray($entityManager, $id_album)
    {
        $query = $entityManager->getConnection()->executeQuery('SELECT id_photo FROM `photo_album` WHERE id_album =' . $id_album);
        $result = $query->fetchAll();
        $result2 = array();
        foreach ($result as $line) {
            $result2[] = $line['id_photo'];
        }
        return $result2;
    }

    /**
     *
     * @param Doctrine\ORM\EntityManager $entityManager
     * @param type $id_owner
     * @param type $id_friend
     */
    public static function removeAlbum($entityManager, $params)
    {
        $session = new Session();
        $album = $entityManager->getRepository('Album')->find($params['id_album']);
        $photoArray = Albums::getPhotoArray($entityManager, $params['id_album']);

        /* clean all photos related to the album */
        $query = $entityManager->getConnection()->executeQuery('delete FROM `photo_album` WHERE id_album =' . $params['id_album']);

        /**
         * Clean all files related to the album
         */
        foreach ($photoArray as $photo) {
            $path = 'uploads/' . $session->getSession('userid') . '/' . $params['id_album'] . '/' . $photo . '.jpg';
            unlink($path);
        }
//


        $photocomments = '(';
        $count = count($photoArray);
        if ($count > 0) {
            $i = 0;
            foreach ($photoArray as $photo) {
                $photocomments .= $photo;
                if ($i != $count - 1) {
                    $photocomments .= ',';
                    $i++;
                }
            }
            $photocomments .= ')';

            echo $photocomments;
            $query = $entityManager->getConnection()->executeQuery('delete FROM `photo_comments` WHERE id_photo IN' . $photocomments);
        }

        if ($album->getIdOwner() == $session->getSession('userid')) {
            $query = $entityManager->getConnection()->executeQuery('delete FROM `albums` WHERE id_album =' . $params['id_album']);
        }

        return false;
    }

    /**
     *
     * @param Doctrine\ORM\EntityManager $entityManager
     * @param type $id_owner
     * @param type $id_friend
     */
    public static function removePhoto($entityManager, $params)
    {
        $session = new Session();
        $photo = new PhotoAlbum();
        $photo = $entityManager->getRepository('PhotoAlbum')->findOneBy($params);
        if ($photo->getIdOwner() == $session->getSession('userid')) {
            unlink($photo->getPath());
            $entityManager->remove($photo);
            $comments = $entityManager->getRepository('PhotoComment')->findBy($params);
            foreach ($comments as $comment) {
                if ($comment->getIdUser() == $_SESSION['userid'] || $photo->getIdOwner() == $_SESSION['userid']) {
                    $id_photo = $comment->getIdPhoto();
                    $entityManager->remove($comment);
                }
            }
            $entityManager->flush();
            unset($params);
            $params = array();
            $params['id_album'] = $photo->getIdAlbum();
            return $params;
        }
        return false;
    }

    /**
     *
     * @param Doctrine\ORM\EntityManager $entityManager
     * @param type $id_owner
     * @param type $id_friend
     */
	 
    // public static function createPhoto($entityManager, $params)
    // {
        // $friend = new PhotoAlbum();
        // $friend->setIdAlbum($params['id_album']);
        // $friend->setIdOwner($params['id_owner']);
        // $friend->setTitle($params['title']);
		// $friend->setPicDesc($params['pic_desc']);
        // $params['datetime'] = $friend->getDatetime(true);
        // $entityManager->persist($friend);
        // $entityManager->flush();

        // $photo = $entityManager->getRepository('PhotoAlbum')->findOneBy($params);
        // return $photo;
    // }
	
	public static function createPhoto($entityManager,$params)
	{
		
		$friend = new PhotoAlbum();
		
		$params['datetime'] = $friend->getDatetime(true);
		
		//$sql= mysql_query("INSERT INTO `photo_album` (`id_album`,`id_owner`,`id_wall`,`title`,`pic_desc`,`datetime`) values ('".$params['id_album']."','".$params['id_owner']."','Null','".$params['title']."','".$params['pic_desc']."','".$params['datetime']."')");
		
		//unset($params['pic_desc']);
		
		//$photo = $entityManager->getRepository('PhotoAlbum')->findOneBy($params);
		
        //return mysql_insert_id();
		
		$sql= "INSERT INTO `photo_album` (`id_album`,`id_owner`,`id_wall`,`title`,`pic_desc`,`datetime`) values ('".$params['id_album']."','".$params['id_owner']."',NULL,'".$params['title']."','".$params['pic_desc']."','".$params['datetime']."')";
		
		//echo $sql;die;
		
		$entityManager->getConnection()->executeQuery($sql);
		
		unset($params['pic_desc']);
		
	    return $entityManager->getConnection()->lastInsertId();
		
	}

    /**
     *
     * @param Doctrine\ORM\EntityManager $entityManager
     */
    public static function createAlbum($entityManager, $params)
    {
        $id_owner = $params['id_owner'];
		
        $own = $id_owner > 0 ? $id_owner : 0;

        if ($own > 0) {
			
            $friend = new Album();
			
            $friend->setIdOwner($id_owner);
			
            $friend->setPrivate($params['priv']);
			
            $friend->setTitle($params['title']);
			
            $entityManager->persist($friend);
			
            $entityManager->flush();
			
			
			
            AlbumUtil::createAlbumFolder($friend->getIdAlbum());
			
            return $friend;
        }
    }

    /**
     *
     * @param Doctrine\ORM\EntityManager $entityManager
     */
    public static function getPhotoCount($entityManager, $id_album)
    {
       $sql = 'SELECT count( id_album ) AS count
FROM `photo_album`
WHERE id_album =' . $id_album;

        $query = $entityManager->getConnection()->executeQuery($sql);
        $result = $query->fetchAll();
        $result = $result[0];
        $result = $result['count'];
        return $result;
    }

    /**
     *
     * @param Doctrine\ORM\EntityManager $entityManager
     */
    public static function getPhotoDescription($entityManager, $params)
    {
        $div = '';
        $photo = new PhotoAlbum();
        $photo = $params['photo'];
        $other = $params['other'];
        unset($params['other']);
        $user = $entityManager->getRepository('UserRegister')->find($photo->getIdOwner());
        $div .= '<div id="info"><img src="uploads/' . $user->getProfilePic() . '" class="imgowner"> ' . ucfirst($user->getFirstName()) . ' ' . ucfirst($user->getLastName()) . ': <p>' . $photo->getDatetime() . '</p> ';
        $div .= '<h2 id="titlePhoto" ';
        if ($other)
            $div .= 'onclick="showTitlePhotoForm()">';
        $div .= '>' . $photo->getTitle() . '<h2></div>';

        return $div;
    }

    /**
     *
     * @param Doctrine\ORM\EntityManager $entityManager
     */
	 
    // public static function setCover($entityManager, $params)
    // {
        // $album = new Album();
        // $session = new Session();
        // $album = $entityManager->getRepository('Album')->find($params['id_album']);
        // if ($album->getIdOwner() == $session->getSession('userid')) {
            // $photo = new PhotoAlbum();
            // $photo = $entityManager->getRepository("PhotoAlbum")->find($params['id_photo']);
            // if ($photo->getIdOwner() == $session->getSession('userid')){
				
                // $album->setCover($params['id_photo']);
				// $album->setTitle($params['title']);
				// $album->setDescription($params['about_album']);
				
                // $entityManager->persist($album);
                // $entityManager->flush();
            // }
        // }

    // }
	
	public function setCover($entityManager,$params)
	{	
	
		$album_new  = str_replace("'", '', $params['about_album']);
		
		$album_new1 = str_replace('"', '', $album_new);
		
		$sql= "UPDATE `albums` SET `title` = '".$params['title']."', `about_album` = '".$album_new1."', `location` = '".$params['location']."', `cover` = '".$params['id_photo']."' WHERE `id_album` = '".$params['id_album']."' ";
			
		//echo $sql;die;
			
		$entityManager->getConnection()->executeQuery($sql);
			
		return  1 ;
	
	}
	
	
	public function updateWallId($entityManager,$id_wall,$id_album) {
		
		$sql= "UPDATE `albums` SET   `id_wall` = '".$id_wall."' WHERE `id_album` = '".$id_album."' ";
			
		$entityManager->getConnection()->executeQuery($sql);
			
		return  true ;
		
	}

    /**
     *
     * @param Doctrine\ORM\EntityManager $entityManager
     */
    public static function getCover($entityManager, $id)
    {
        $photo = new PhotoAlbum();
        $photo = $entityManager->getRepository('PhotoAlbum')->find($id);
        
        if(isset($photo))
            return $photo->getPath();

        return null;
    }
	
	public static function getLastId($entityManager,$id_album)
	{
		$query = $entityManager->getConnection()->executeQuery("SELECT `id_photo` FROM `photo_album` WHERE `id_album` = '".$id_album."' ORDER BY `id_photo` DESC LIMIT 1");
        $result = $query->fetch();
		return $result ;
	}
	
	public static function getPhotosByAlbumId($entityManager,$id_album)
	{
		$query = $entityManager->getConnection()->executeQuery("SELECT * FROM `photo_album` WHERE `id_album` = '".$id_album."' ORDER BY `id_photo` DESC ");
        $result = $query->fetchAll();
		return $result ;
	}
	
	public static function getAlbumInfoByAlbumId($entityManager,$id_album)
	{
		$query = $entityManager->getConnection()->executeQuery("SELECT * FROM `albums` WHERE `id_album` = '".$id_album."' ");
        $result = $query->fetch();
		return $result;
	}
	
	public static function deletePhoto($entityManager,$id)
	{
		$query = $entityManager->getConnection()->executeQuery("DELETE  FROM `photo_album` WHERE `id_photo` = '".$id."' ");
		
		return true ;
	}
	
	public static function getUserProfile($entityManager,$uId)
	{
		$query = $entityManager->getConnection()->executeQuery("SELECT user_id,firstname,lastname,orginalpic FROM `sh_users` WHERE `user_id` = '".$uId."' ");
        $result = $query->fetch();
		return $result;
	}
	
	public static function photoDesc($entityManager,$photoId)
	{
		$query = $entityManager->getConnection()->executeQuery("SELECT pic_desc FROM `photo_album` WHERE `id_photo` = '".$photoId."' ");
        $result = $query->fetch();
		//print_r($result);die;
		//echo $result['pic_desc'] ;die;
		return $result['pic_desc'];
	}
	
	public static function checkPhotoCover($entityManager,$photoId,$albId)
	{
		$query = $entityManager->getConnection()->executeQuery("SELECT * FROM `albums` WHERE `id_album` = '".$albId."' AND `cover` = '".$photoId."' ");
        
		$result = $query->fetch();
		
		return $result;
	}
	
	public function checkAlbum($wall_id, $entityManager){
		
		$query = $entityManager->getConnection()->executeQuery("SELECT id_album FROM `albums` WHERE `id_wall` = '".$wall_id."' ");
        
		$result = $query->fetch();
		
		return $result['id_album'];
	}
	
	public function checkAlbumIsExist($wall_id, $entityManager){
		
		$query = $entityManager->getConnection()->executeQuery("SELECT * FROM `albums` WHERE `id_wall` = '".$wall_id."' ");
        
		$result = $query->fetch();
		
		return $result;
	}
	
	public function setCoverPhotoOnly($entityManager,$id_photo,$id_album)
	{	
	$sql= "UPDATE `albums` SET `cover` = '".$id_photo."' WHERE `id_album` = '".$id_album."' ";
		
	$query = mysql_query($sql);
	
	return 1 ;
	}
	
	public function getNewPhotoIdCover($entityManager,$albId,$photoId)
	{
		$query = $entityManager->getConnection()->executeQuery("SELECT * FROM `photo_album` WHERE `id_album` = '".$albId."' AND `id_photo` <> '".$photoId."' ORDER BY `id_photo` DESC LIMIT 1  ");
        
		$result = $query->fetch();
		
		return $result['id_photo'];
	}
	
	public function getLastPhotoIdOnly($entityManager)
	{
		$query = $entityManager->getConnection()->executeQuery("SELECT id_photo FROM `photo_album`  ORDER BY `id_photo` DESC LIMIT 1  ");
        
		$result = $query->fetch();
		
		return $result['id_photo'];
		
	}

    /**
     *
     * @param Doctrine\ORM\EntityManager $entityManager
     */

}

?>