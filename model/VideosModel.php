<?php

require_once "bootstrap.php";

class VideosModel {

    public function __construct() {
        
    }

    public static function addVideo($entityManager, $params) {
        $entry = new Videos();
        $entry->setWallId($params['wall_id']);
        $entry->setFile($params['file']);
        $entry->setDate($params['date']);
        $entityManager->getConnection()->beginTransaction();
        try {
            $res = $entityManager->persist($entry);
            $entityManager->flush();
            $entityManager->getConnection()->commit();
            return $entry->getId();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }
	
	public static function add_video_view($entityManager,$data)
	{
		$entry = new Videos();
		$sql= "INSERT INTO `video_views` (`user_id`,`video_id`,`ip`) values ('".$data['uId']."','".$data['video_id']."','".$data['ip']."')";
		$entityManager->getConnection()->executeQuery($sql);
	    return $entityManager->getConnection()->lastInsertId();
	}
	public static function addVideoDetails($lastId,$params,$entityManager)
	{
		$entry = new Videos();
		
		//echo '<pre>'; print_r($params);die;
		
		$sql= "INSERT INTO `videos` (`wall_id`,`file`,`tags`,`cat`,`date`,`title`,`description`,`privacy`,`thumbnail`) values ('".$lastId."','".$params['file']."','".$params['tags']."','".$params['cat']."','".$params['date']."','".$params['title']."','".$params['description']."','".$params['privacy']."','".$params['thumbnail']."')";
		
		$entityManager->getConnection()->executeQuery($sql);
		
	    return $entityManager->getConnection()->lastInsertId();
	}
	
	public static function substrword($text, $maxchar, $end='.') {
		if (strlen($text) > $maxchar || $text == '') {
			$words = preg_split('/\s/', $text);      
			$output = '';
			$i      = 0;
			while (1) {
				$length = strlen($output)+strlen(@$words[$i]);
				if ($length > $maxchar) {
					break;
				} 
				else {
					$output .= " " .@$words[$i];
					++$i;
				}
			}
			$output .= $end;
		} 
		else {
			$output = $text;
		}
		return $output;
	}		
	
	public static function allUploadedVideoByUserId($userid,$entityManager,$start = NULL ,$limit = NULL)
	{
		$entry = new Videos();
		
		$sql= "SELECT v.id,v.wall_id,v.file,v.tags,v.date,v.title,v.description,v.privacy,v.thumbnail 
				FROM videos v LEFT JOIN wall w ON w.id = v.wall_id 
				WHERE author_id = '".$userid."' and owner_id = '".$userid."' order by v.id desc limit $start , $limit ";
		
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetchAll();
		return $result;
	}
	
	public function checkAlreadyView($entityManager,$data)
	{
		$entry = new Videos();
		$sql= "SELECT count(*) c FROM `video_views` WHERE `user_id` = '".$data['uId']."' AND  `video_id` =  '".$data['video_id']."' ";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetchAll();
		return $result[0]['c'];
	}
	
	public function countVideoViews($entityManager,$vid_id)
	{
		$entry = new Videos();
		$sql= "SELECT count(*) c FROM `video_views` WHERE   `video_id` =  '".$vid_id."' ";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetchAll();
		return $result[0]['c'];
	}

    public function updateVideoDetials($entityManager, $params, $id) {
        if ($id) {
            $query = $entityManager->getConnection()->executeQuery('UPDATE '
                    . '`videos` SET '
                    . 'status=-1 '
                    . 'status=-1 '
                    . 'status=-1 '
                    . 'status=-1 '
                    . 'status=-1 '
                    . 'WHERE (id =' . $id . ')');
            $result = $query->fetchAll();

            return $result;
        }
    }

    public static function saveVideoDetails($entityManager, $params, $entity) {
//        if ($entity)
//            return false;

//        $entry->setNotify($params['notify']);
//        $entry->setTumbnails($params['tumbnails']);
        
        $entity->setWallId($params['wall_id']);
        
        $entity->setCat($params['category']);

       /* if (isset($params['status']))
            $entity->setStatus($params['status']);
        else
            $entity->setStatus(1); */

        $entity->setTags_more($params['moretags']);
        $entity->setTags($params['tags']);
        $entity->setDescription($params['description']);
        $entity->setTitle($params['title']);
		$entity->setPrivacy($params['showTo']);
		$entity->setThumbnail($params['selectedThumbnail']);

        $entityManager->getConnection()->beginTransaction();

        try {
            $res = $entityManager->persist($entity);
            $entityManager->flush();
            $entityManager->getConnection()->commit();
            return $entity->getId();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    public static function getVedio($entityManager,$vid) {

        return $videos = $entityManager->getRepository('Videos')->find($vid);
    }

    public static function getEntries($entityManager, $vid = false) {

        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "SELECT *
                FROM videos  WHERE id = " . $vid . " LIMIT 0,1";

            $query = $entityManager->getConnection()->executeQuery($dql);
            return $result = $query->fetch();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
	
	
	public static function getVideos($post_id,$entityManager)
    {
       
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "SELECT v,w
                FROM Wall w,Videos v
                WHERE w.id=v.wall_id and  v.wall_id =:post order by v.date desc";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'post' => $post_id
            ));
           // echo '<pre>';
          // print_r($query->getArrayResult());
             return $query->getArrayResult();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }
    

}
