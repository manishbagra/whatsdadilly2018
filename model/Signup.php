<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Signup
 *
 * @author sathish
 */
require_once "bootstrap.php";

class Signup {

    protected $res;

    //put yor code here
    public function __construct() {
        
    }

    static public function generateRandomString() {
        $length = 20;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $characters = str_shuffle($characters);
        return substr($characters, 0, $length);
    }

    static public function generateRandomStringForForgotpwd() {
        $length = 20;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $characters = str_shuffle($characters);
        return substr($characters, 0, $length);
    }

    static public function save($data, $entityManager) {
        $register = new UserRegister();
        $activation = Signup::generateRandomString();
        $dob = $data['year'] . '-' . $data['month'] . '-' . $data['day'];
        $register->setFirstName($data['firstname']);
        $register->setLastName($data['lastname']);
        $register->setEmail($data['email']);
        $register->setPassword(md5($data['password']));
        //$register->setPhoto($photo);
        $register->setActivation($activation);
        $register->setDOB(new DateTime($dob));
        $register->setRegStatus("PENDING");
        $register->setSignupDate(new DateTime());
        $register->setSessionId(session_id());
        $register->setGender($data['gender']);
        $entityManager->getConnection()->beginTransaction();
		
        try {
			
            $res = $entityManager->persist($register);
            
			//SendMail::sendRegisterationMail($register->getEmail(), $register->getActivation(), $register->getFirstName(), $register->getLastName());
			
			$entityManager->flush();
            $entityManager->getConnection()->commit();
            return $register->getUser_id();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }
	
	

    static public function signup2($data, $entityManager) {
		
            $dql = "UPDATE sh_users r set 
			
			r.current_country= '".$data['country']."', 
			r.current_state = '".$data['state']."',  
			r.current_city  = '".$data['city']."' 
			
			WHERE r.user_id = '".$data['sign_uid']."' ";
			
			$entityManager->getConnection()->executeQuery($dql);
			
            return true;
    }

    static public function profileEdit($data, $entityManager) {
        $register = new UserRegister();
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "UPDATE UserRegister r set r." . $data['id'] . " = :iValue WHERE r.user_id =:sUid";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'iValue' => $data['value'],
                'sUid' => $data['userid']
            ));
            $query->getResult();
            $entityManager->getConnection()->commit();
            return true;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    static public function profileEditBasicinfo($data, $entityManager) {
        $register = new UserRegister();
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "UPDATE UserRegister r set "
                    . "r.current_city = :current_city_id,r.home_city = :home_city_id,r.interested = :interested"
                    . ",r.relationship = :relationship,r.politicalview = :politicalview,r.religion = :religion,r.language =:language"
                    . " WHERE r.user_id =:sUid";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                "current_city_id" => $data['current_city_id'],
                "home_city_id" => $data['home_city_id'],
                "interested" => $data['interested'],
                "relationship" => $data['relationship'],
                "politicalview" => $data['politicalview'],
                "religion" => $data['religion'],
                "language" => $data['language'],
                "sUid" => $data['userid']
            ));
            $query->getResult();
            $entityManager->getConnection()->commit();
            return true;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    static public function profileEditAboutyou($data, $entityManager) {
        $register = new UserRegister();
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "UPDATE UserRegister r set r.aboutyou = :aboutyou,r.listinterest =:listinterest WHERE r.user_id =:sUid";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'aboutyou' => $data['aboutyou_p'],
                'listinterest' => $data['listinterest'],
                'sUid' => $data['userid']
            ));
            $query->getResult();
            $entityManager->getConnection()->commit();
            return true;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    static public function profileEditContact($data, $entityManager) {
        $register = new UserRegister();
        $register->sethome($data['home']);
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "UPDATE UserRegister r set r.phonenumber = :phonenumber, r.home= :home WHERE r.user_id =:sUid";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'phonenumber' => $data['phonenumber'],
                'home' => $data['home'],
                'sUid' => $data['userid']
            ));
            $query->getResult();
            $entityManager->getConnection()->commit();
            return "yip";
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            return $e;
        }
    }

    // static public function profileView($data, $entityManager) {
        // $entityManager->getConnection()->beginTransaction();
        // try {
            // $dql = "SELECT b FROM UserRegister b WHERE b.user_id = ?1";
            // $query = $entityManager->createQuery($dql);
            // $query->setParameter(1, $data['userid']);
            // return $query->getArrayResult();
        // } catch (Exception $e) {
            // $entityManager->getConnection()->rollback();
            // $entityManager->close();
            // throw $e;
        // }
    // }
	
	static public function profileView($data, $entityManager) {
	
		$sql= "SELECT * FROM `sh_users` where `user_id`= '".$data['userid']."' ";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetchAll();
		return $result;
	}	

    static public function signup3($data, $entityManager) {
        session_start();
        $register = new UserRegister();
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "UPDATE UserRegister r set r.profile_pic = :iImage WHERE r.user_id =:sUid";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'iImage' => $data['image'],
                'sUid' => $data['sign_uid']
            ));
            $query->getResult();
            $entityManager->getConnection()->commit();
            return $data['sign_uid'];
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    static public function addCities($data, $entityManager) {
        session_start();
        $location = new Location();
        $location->setCountry($data['country']);
        $location->setCity($data['city']);
        $entityManager->getConnection()->beginTransaction();
        try {
            $res = $entityManager->persist($location);
            $entityManager->flush();
            $entityManager->getConnection()->commit();
            return $location->getId();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    // static public function allCountry($entityManager) {
        // $entityManager->getConnection()->beginTransaction();
        // try {
            // $dql = "SELECT l FROM Location l group by l.country";
            // $query = $entityManager->createQuery($dql);
            // return $query->getArrayResult();
        // } catch (Exception $e) {
            // $entityManager->getConnection()->rollback();
            // $entityManager->close();
            // throw $e;
        // }
    // }
	
	public function allCountry($entityManager) {
		$sql= "SELECT * FROM `countries`";
        $query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetchAll();
		return $result;
    }
	
	public function getAllStateByCountryId($entityManager,$country_id)
	{
		$sql= "SELECT * FROM `states` WHERE `country_id` = '".$country_id."' ";
        $query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetchAll();
		return $result;
	}
	
	public function getAllCityByStateId($entityManager,$state_id)
	{
		$sql= "SELECT * FROM `cities` WHERE `state_id` = '".$state_id."' ";
        $query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetchAll();
		return $result;
	}

    static public function getCities($data, $entityManager) {
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "SELECT DISTINCT l.city FROM Location l WHERE l.city LIKE :search AND l.country = ?2";
            $query = $entityManager->createQuery($dql);
            $query->setParameter('search', $data['text'] . '%');
            $query->setParameter(2, $data['country']);
            return $query->getArrayResult();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    static public function validate($email, $entityManager) {
			
            $dql = "SELECT * FROM sh_users WHERE email = '".$email."' ";
			
            $query = $entityManager->getConnection()->executeQuery($dql);
			
            return $query->fetchAll();
       
    }

    static public function login($data, $entityManager) {
		
        $entityManager->getConnection()->beginTransaction();
		
        try {
            $dql = "SELECT * FROM sh_users b WHERE b.email = '".$data['login_username']."' AND b.password = '".$data['login_password']."' ";
			
           // $query = $entityManager->createQuery($dql);
			
			$query = $entityManager->getConnection()->executeQuery($dql);
			

           // $query->setParameter(1, $data['login_username']);
			
            //$query->setParameter(2, $data['login_password']);
			
			$result = $query->fetchAll();
			
           // $user_id = $query->getArrayResult();
			
			//echo"<pre>"; print_r($result);die;
			
           return $result;
			
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    static public function firstlogin($data, $entityManager) {
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "SELECT b FROM UserRegister b WHERE b.user_id = ?1";
            $query = $entityManager->createQuery($dql);
            $query->setParameter(1, $data['user_id']);
            $user_details = $query->getArrayResult();
            return $user_details;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    static public function lastLogin($user_id, $entityManager) {
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "UPDATE sh_users r set r.last_login = '".date('l, F j, Y, g:i:s A')."' WHERE r.user_id = '".$user_id."' ";
			
			
            $query = $entityManager->createQuery($dql);
			
            // $query->setParameters(array(
                // 'sUid' => $user_id,
                // 'iLastLogin' => date("l, F j, Y, g:i:s A")
            // ));
			
            //$query->getResult();
			
			
			
           // $entityManager->getConnection()->commit();
			
            return true;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    static public function addToken($data, $entityManager) {
        $addToken = new AddToken();
        $addToken->setNetworkName($data['networkname']);
        $addToken->setAuthToken($data['auth_token']);
        $addToken->setAuthSecret($data['auth_secret']);
        $addToken->setUserId($data['user_id']);
        $addToken->setScreenName($data['screen_name']);
		$addToken->setScreenImg($data['img_link']);
        $addToken->setScreenID($data['screen_id']);
        $entityManager->getConnection()->beginTransaction();
        try {
            $res = $entityManager->persist($addToken);
            $entityManager->flush();
            $entityManager->getConnection()->commit();
            return $addToken->getTokenId();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    static public function getTokens($data, $entityManager) {
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "SELECT a FROM AddToken a WHERE a.user_id = ?1";
            $query = $entityManager->createQuery($dql);
            $query->setParameter(1, $data['userid']);
            return $query->getArrayResult();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    static public function getTown($data, $entityManager) {
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "SELECT l FROM Location l where l.id =?1";
            $query = $entityManager->createQuery($dql);
            $query->setParameter(1, $data);
            return $query->getArrayResult();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    static public function profileEditwork($data, $entityManager) {
        $register = new UserRegister();
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "UPDATE UserRegister r set r.work = :work,r.education = :education WHERE r.user_id =:sUid";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'work' => $data['work'],
                'education' => $data['education'],
                'sUid' => $data['userid']
            ));
            $query->getResult();
            $entityManager->getConnection()->commit();
            return true;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    static public function profileEditcover($data, $entityManager) {
        $register = new UserRegister();
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "UPDATE UserRegister r set r.cover_photo = :cover_photo WHERE r.user_id =:sUid";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'cover_photo' => $data['cover_photo'],
                'sUid' => $data['userid']
            ));
            $query->getResult();
            $entityManager->getConnection()->commit();
            return true;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    static public function removeProfilePic($data, $entityManager) {
        $register = new UserRegister();
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "UPDATE UserRegister r set r.profile_pic = :profile_pic WHERE r.user_id =:sUid";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'profile_pic' => "",
                'sUid' => $data['sign_uid']
            ));
            $query->getResult();
            $entityManager->getConnection()->commit();
            return true;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    static public function getActivationLink($data, $entityManager) {
        $entityManager->getConnection()->beginTransaction();
        try {
            $query = $entityManager->getConnection()->executeQuery("select * from sh_users where registeration_status='PENDING' and user_id=" . $data['sign_uid']);
            $result = $query->fetchAll();
            return $result;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }

    static public function getActivated($activation_code, $entityManager) {
        $entityManager->getConnection()->beginTransaction();

        try {
            $dql = "UPDATE UserRegister r set r.registeration_status = :status WHERE r.activation =:activation_token";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'activation_token' => $activation_code,
                'status' => 'COMPLETED'
            ));
            $query->getResult();
            $entityManager->getConnection()->commit();
            return true;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    static public function checkusername($data1, $entityManager) {
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "SELECT b FROM UserRegister b WHERE b.email = ?1";
            $query = $entityManager->createQuery($dql);
            $query->setParameter(1, $data1['login_username']);
            $user_id = $query->getArrayResult();
            return $user_id;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    static public function sendForgotPasswordLink($data, $entityManager) {
        $register = new UserRegister();
        $token = '';
        $entityManager->getConnection()->beginTransaction();

        try {
            $token = Signup::generateRandomStringForForgotpwd();

            $email = $data['email'];
            $dql = "UPDATE UserRegister r set r.forgotpwd = :forgotpwd WHERE r.email =:email";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'forgotpwd' => $token,
                'email' => $email
            ));
            SendMail::sendForgotPasswordMail($email, $token);
            $query->getResult();
            $entityManager->getConnection()->commit();
            return true;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    static public function UpdatePassword($data, $entityManager) {
        $register = new UserRegister();
        $entityManager->getConnection()->beginTransaction();

        try {
            $dql = "UPDATE UserRegister r set r.password = :password WHERE r.forgotpwd =:forgotpwd";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'forgotpwd' => $data['token'],
                'password' => md5($data['log_password'])
            ));
            //SendMail::sendForgotPasswordMail($email, $token);
            $query->getResult();
            $entityManager->getConnection()->commit();
            return true;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    static public function updateOriginalProfilePic($data, $entityManager) {
        $register = new UserRegister();
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "UPDATE UserRegister r set r.orginalpic = :orginalpic WHERE r.user_id =:user_id";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'orginalpic' => $data['filename'],
                'user_id' => $data['userid']
            ));
            //SendMail::sendForgotPasswordMail($email, $token);
            $query->getResult();
            $entityManager->getConnection()->commit();
            return true;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    static public function updateProfilePic($data, $entityManager) {
        session_start();
        $register = new UserRegister();
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "UPDATE UserRegister r set r.profile_pic = :iImage,r.orginalpic = :iImage WHERE r.user_id =:sUid";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'iImage' => $data['image'],
                'sUid' => $data['sign_uid']
            ));
            $query->getResult();
            $entityManager->getConnection()->commit();
            return $data['sign_uid'];
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    static public function getUserGender($data, $entityManager) {
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "SELECT b FROM UserRegister b WHERE b.user_id = ?1";
            $query = $entityManager->createQuery($dql);
            $query->setParameter(1, $data['sign_uid']);
            $user_id = $query->getArrayResult();
            return $user_id;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }
	
	public function getCity($id, $entityManager) {
        $sql= "SELECT * FROM `cities` where `state_id`= '".$id."' ";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetchAll();
		return $result;
    }
	
	public function getState($country_id, $entityManager) {
        $sql= "SELECT * FROM `states` where `country_id`= '".$country_id."' ";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetchAll();
		return $result;
    }
	
	public function getHomeCity($id, $entityManager){
		$sql= "SELECT * FROM `location` where `id`= '".$id."' ";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetch();
		return $result;
	}
	
	public function workEducations($data, $entityManager){
		$sql= "SELECT * FROM `work_educations` where `userid`= '".$data['userid']."' ";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetch();
		return $result;
	}
	
	public function contactInfo($data, $entityManager)
	{
		$sql= "SELECT * FROM `contact_info` where `userid`= '".$data['userid']."' ";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetch();
		return $result;
	}
	
	public function checkIndex($user_id,$field,$entityManager)
	{
		$sql= "SELECT `".$field."` FROM `contact_info` where `userid`= '".$user_id."' ";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetch();
		return $result[$field];
	}
	
	static public function updateUserDetails($data,$entityManager)
	{	
		$sql= "SELECT count(id) AS count FROM `contact_info` where `userid`= '".$data['userid']."' ";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$count = $query->fetch();
	
		$sql = "UPDATE  `sh_users`  SET 
		`dob`				=	'".$data['date']."',
		`gender`			=	'".$data['gender']."',
		`interested`		=	'".$data['interested']."',
		`aboutyou`			=	'".$data['about_you']."' ,
		`current_country` 	= 	'".$data['country']."',
		`current_state` 	= 	'".$data['state']."',
		`current_city` 		= 	'".$data['city']."',
		`relationship` 		= 	'".$data['relationship']."',
		`interested`		= 	'".$data['interested']."',
		`politicalview` 	= 	'".$data['political']."',
		`religion` 			= 	'".$data['religion']."',
		`language` 			= 	'".$data['language']."'
		
		WHERE `user_id`='".$data['userid']."' ";
		
		
		$entityManager->getConnection()->executeQuery($sql);
		
		if($count['count']>0){
			
			$sql1 = "UPDATE  `contact_info`  SET `email`='".$data['email']."',`phone`='".$data['phone']."',`website`='".$data['website']."' WHERE `userid` = '".$data['userid']."' ";
		
		}else{
			$sql1 = "INSERT INTO `contact_info` (`userid`,`email`,`phone`,`website`) VALUES ('".$data['userid']."','".$data['email']."','".$data['phone']."','".$data['website']."') ";
		}
		
		
		$entityManager->getConnection()->executeQuery($sql1);
	   
	    return 1 ;
		
	}
	
	static public function removeIndexElement($data,$field,$entityManager)
	{
		$sql1 = "UPDATE  `contact_info`  SET `".$field."` = '".$data[$field]."' ";
		
		$entityManager->getConnection()->executeQuery($sql1);
	   
	    return 1 ;
	}
	
	static public function getCityName($cityId, $entityManager)
	{
		$sql= "SELECT * FROM `cities` where `id`= '".$cityId."' ";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetch();
		return $result;
	}
	
	static public function getStateName($stateId, $entityManager)
	{
		$sql= "SELECT * FROM `states` where `id`= '".$stateId."' ";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetch();
		return $result;
	}
	static public function getCountryName($countryId, $entityManager)
	{
		$sql= "SELECT * FROM `countries` where `id`= '".$countryId."' ";
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetch();
		return $result;
	}
	
	

}
?>