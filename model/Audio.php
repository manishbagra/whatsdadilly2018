<?php
require_once "bootstrap.php";

class AudioModel
{
	public function store_wall_post_audio($data,$entityManager)
	{
		$sql= "INSERT INTO `wall` (`author_id`,`owner_id`,`text`,`date`,`post_type`) values ('".$data['userid']."','".$data['owner_id']."','".$data['full_name']."','".$data['date']."','".$data['post_type']."')";
		$entityManager->getConnection()->executeQuery($sql);
	    return $entityManager->getConnection()->lastInsertId();
	}
	
	public function store_audio_file($data,$entityManager)
	{
		$sql= "INSERT INTO `audios`
		(`wall_id`,
		`file`,
		`thumb`,
		`tags`,
		`title`,
		`playlist_desc`,
		`type`,
		`release_date`,
		`genre`,
		`descrioption`,
		`post_type`,
		`uploaded_on`) VALUES 
		('".$data['wall_id']."',
		'".$data['unique_file']."',
		'".$data['file_thumb']."',
		'".$data['tags']."',
		'".$data['full_name']."',
		'".$data['desc_playlist']."',
		'".$data['playlist_type']."',
		'".$data['rel_date']."',
		'".$data['genre']."',
		'".$data['descript']."',
		'".$data['post_type']."',
		'".$data['date']."')" ;
		
		$entityManager->getConnection()->executeQuery($sql);
	    
		return $entityManager->getConnection()->lastInsertId();
	}
	
	public function getAudios($wall_id, $entityManager)
	{
		
		
		$sql = "SELECT * FROM `audios` WHERE wall_id = '".$wall_id."' ";
				
		$query = $entityManager->getConnection()->executeQuery($sql);
		
		$result = $query->fetch();
		
		return $result;
	}
	
	public static function allUploadedAudioByUserId($userid,$entityManager,$start = NULL ,$limit = NULL)
	{
		//echo $userid;die;
		
		
		$sql= "SELECT a.id,a.wall_id,a.file,a.tags,a.uploaded_on,a.title,a.playlist_desc,a.descrioption,a.post_type,a.thumb 
				FROM audios a LEFT JOIN wall w ON w.id = a.wall_id 
				WHERE author_id = '".$userid."' and owner_id = '".$userid."' order by a.id desc limit $start , $limit ";
		
		
		$query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetchAll();
		//print_r($result);die;
		return $result;
	}
	
	
}