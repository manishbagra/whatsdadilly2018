<?php

require_once "bootstrap.php";

class WallModel
{

    public static function addOtherWallEntry($entityManager, $params)
    {
        $entry = new Wall();
        $entry->setAuthorId($params['author_id']);
        $entry->setOwnerId($params['owner_id']);
        $entry->setText($params['text']);

        $entry->setLink($params['link']);
        $entry->setLinkDescription($params['link_description']);
        $entry->setLinkPhoto($params['link_photo']);
        $entry->setLinkTitle($params['link_title']);
        $entry->setDate($params['date']);
        if ($params['author_id'] != $params['owner_id']) {
            $entry->setPostType(1);
        } else {
            $entry->setPostType(0);
        }


        $entityManager->getConnection()->beginTransaction();
        try {
            $res = $entityManager->persist($entry);
            $entityManager->flush();
            $entityManager->getConnection()->commit();
            return $entry->getId();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    public static function addWallEntry($entityManager, $params)
    {
        $entry = new Wall();
        $entry->setAuthorId($params['author_id']);
        $entry->setOwnerId($params['owner_id']);
        $entry->setText($params['text']);

        $entry->setLink($params['link']);
        $entry->setLinkDescription($params['link_description']);
        $entry->setLinkPhoto($params['link_photo']);
        $entry->setLinkTitle($params['link_title']);
        $entry->setDate($params['date']);
        if ($params['author_id'] != $params['owner_id']) {
            $entry->setPostType(1);
        } else {
            $entry->setPostType(0);
        }

        $entityManager->getConnection()->beginTransaction();
        try {
            $res = $entityManager->persist($entry);
            $entityManager->flush();
            $entityManager->getConnection()->commit();
            return $entry->getId();
        } catch (Exception $e) {
            echo "Ki Hoya 22";
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    public static function getPhotoAlbum($entityManager, $wall_id)
    {
        //$entityManager->getConnection()->beginTransaction();
        try {
            $query = $entityManager->getConnection()->executeQuery('select file,id from photos where wall_id=' . $wall_id);
            $result = $query->fetchAll();

            return $result;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }

    public static function getEntries($entityManager, $user_id = false)
    {
        if (!$user_id) {
            $user_id = $_SESSION['userid'];
        }
        $entityManager->getConnection()->beginTransaction();
        try {
            $query = $entityManager->getConnection()->executeQuery('SELECT distinct(w.id),w.text,w.link,w.link_title,w.link_description,w.link_photo,w.date,w.author_id as user_id,w.author_id,w.owner_id,w.post_type as type FROM friends f
                                                                    RIGHT JOIN wall as w on w.author_id = f.id_owner or w.author_id=f.id_friend
                                                                    WHERE f.id_friend=' . $user_id . ' OR f.id_owner=' . $user_id . ' OR w.author_id=' . $user_id . ' order by w.date desc');
            $result = $query->fetchAll();
            /* $query = $entityManager->createQuery($dql);
             $query->setParameters(array(
                 'owner' => $user_id
             ));
             * */
            return $result;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }

    public static function getTweetsEntries($entityManager, $user_id = false, $curTime)
    {
        if (!$user_id) {
            $user_id = $_SESSION['userid'];
        }
        $entityManager->getConnection()->beginTransaction();
        try {
            $query = $entityManager->getConnection()->executeQuery('SELECT distinct(w.id),w.text,w.link,w.link_title,w.link_description,w.link_photo,w.date,w.author_id as user_id,w.author_id,w.owner_id,w.post_type as type FROM friends f
                                                                    RIGHT JOIN wall as w on w.author_id = f.id_owner or w.author_id=f.id_friend
                                                                    WHERE (f.id_friend=' . $user_id . ' OR f.id_owner=' . $user_id . ' OR w.author_id=' . $user_id . ') and  w.date >"' . $curTime . '"');
            $result = $query->fetchAll();
            /* $query = $entityManager->createQuery($dql);
             $query->setParameters(array(
                 'owner' => $user_id
             ));
             * */
            return $result;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }

    public static function getUserWall($entityManager, $data)
    {
        $entityManager->getConnection()->beginTransaction();

        try {
            // $dql = "SELECT distinct(w.id),w.text,w.link,w.link_title,w.link_description,w.link_photo,w.date,u.firstname, u.lastname,w.author_id as user_id,w.author_id,w.owner_id,w.post_type as type FROM Wall w join UserRegister u WHERE ((u.user_id=w.author_id and u.user_id =:owner) or ((w.author_id=:owner or w.owner_id =:owner) and w.post_type=1))  group by w.id order by w.date desc";
            $dql = "SELECT distinct(w.id),w.text,w.link,w.link_title,w.link_description,w.link_photo,w.date,u.firstname, u.lastname,w.author_id as user_id,w.author_id,w.owner_id,w.post_type as type FROM Wall w join UserRegister u WHERE ((u.user_id=w.author_id and u.user_id =:owner) or ((w.author_id=:owner or w.owner_id =:owner) and w.post_type=1))  group by w.id order by w.date desc";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'owner' => $data['userid']
            ));
            return $query->getArrayResult();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }

    }

    public static function getUserInfiniteWall($entityManager, $limit1, $limit2,$data = null)
    {
//        return $limit2;

        $entityManager->getConnection()->beginTransaction();

        try {
            $dql = "SELECT distinct(w.id),w.text,w.link,w.link_title,w.link_description,w.link_photo,w.date,u.firstname, u.lastname,w.author_id as user_id,w.author_id,w.owner_id,w.post_type as type FROM Wall w join UserRegister u WHERE ((u.user_id=w.author_id and u.user_id =:owner) or ((w.author_id=:owner or w.owner_id =:owner) and w.post_type=1))  group by w.id order by w.date desc";
//            $t = "LIMIT " . $limit1 . " , " . $limit2;
            $query = $entityManager->createQuery($dql)
                ->setMaxResults(10)
                ->setFirstResult($limit1);
            $query->setParameters(array(
                'owner' => $data['userid']
            ));
            return $query->getArrayResult();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }

    }

    public static function getInfiniteWall($entityManager, $limit1, $limit2,$data = null)
    {
//        return $limit2;
        echo $_SESSION['userid'];

        if (!$user_id) {
            $user_id = $_SESSION['userid'];
        }

        else if (!$user_id) {
            $user_id = $_SESSION['userid'];
        }
        $entityManager->getConnection()->beginTransaction();
        try {
            
            $query = $entityManager->getConnection()->executeQuery('SELECT distinct(w.id),w.text,w.link,w.link_title,w.link_description,w.link_photo,w.date,w.author_id as user_id FROM friends f
                                                                    INNER JOIN wall as w on w.author_id = f.id_owner or w.author_id=f.id_friend
                                                                    WHERE f.id_friend=' . $user_id . ' OR f.id_owner=' . $user_id . ' order by w.date desc LIMIT ' . $limit1 . ',' . $limit2);
            $result = $query->fetchAll();
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'owner' => $user_id
            ));

//            $query = $entityManager->getConnection()->executeQuery('SELECT distinct(w.id),w.text,w.link,w.link_title,w.link_description,w.link_photo,w.date,w.author_id as user_id,w.author_id,w.owner_id,w.post_type as type FROM friends f INNER JOIN wall as w on w.author_id = f.id_owner or w.author_id=f.id_friend WHERE f.id_friend=' . $user_id . ' OR f.id_owner=' . $user_id . ' order by w.date desc')
//                ->setMaxResults($limit2)
//                ->setFirstResult($limit1);;
//            $result = $query->fetchAll();
//            $query = $entityManager->createQuery($dql);
//            $query->setParameters(array(
//                'owner' => $user_id
//            ));
            return $result;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }

    public static function getUserDetails($entityManager, $user_id)
    {
        $entityManager->getConnection()->beginTransaction();
        try {
            $query = $entityManager->getConnection()->executeQuery('select user_id,profile_pic,firstname,lastname from sh_users where user_id=' . $user_id);
            $result = $query->fetchAll();
            return $result;
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }

    public static function getPrviousPost($entityManager, $post_id)
    {
        //  $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "SELECT w.id,w.author_id,w.owner_id,w.post_type as type,w.text,w.link,w.link_title,w.link_description,w.link_photo,w.date,u.firstname, u.lastname FROM Wall w inner join UserRegister u WHERE  u.user_id=w.author_id and w.id =:wall order by w.date desc";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'wall' => $post_id
            ));
//            echo '<pre>';
//            print_r($query->getArrayResult()); die();
            return $query->getArrayResult();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }

    public static function getLatestPost($entityManager, $post_id)
    {
        $entityManager->getConnection()->beginTransaction();
        try {
            $dql = "SELECT w.id,w.text,w.link,w.link_title,w.link_description,w.link_photo,w.date,u.firstname, u.lastname FROM Wall w inner join UserRegister u WHERE  u.user_id=w.author_id and w.id >:wall order by w.date desc";
            $query = $entityManager->createQuery($dql);
            $query->setParameters(array(
                'wall' => $post_id
            ));
//            echo '<pre>';
//            print_r($query->getArrayResult()); die();
            return $query->getArrayResult();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            var_dump($e->getMessage());
            throw $e;
        }
    }


    public static function time_elapsed_string($ptime)
    {
        $etime = time() - strtotime($ptime);

        if ($etime < 1) {
            return '0 seconds';
        }

        $a = array(365 * 24 * 60 * 60 => 'year',
            30 * 24 * 60 * 60 => 'month',
            24 * 60 * 60 => 'day',
            60 * 60 => 'hour',
            60 => 'minute',
            1 => 'second'
        );
        $a_plural = array('year' => 'years',
            'month' => 'months',
            'day' => 'days',
            'hour' => 'hours',
            'minute' => 'minutes',
            'second' => 'seconds'
        );

        foreach ($a as $secs => $str) {
            $d = $etime / $secs;
            if ($d >= 1) {
                $r = round($d);
                return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
            }
        }
    }

}
