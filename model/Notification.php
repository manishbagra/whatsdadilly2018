<?php
require_once "bootstrap.php";

class NotificationModel {

    /**
     * 
     * @param Doctrine\ORM\EntityManager $entityManager
     * @param type $params
     * @throws Exception
     */
    // public static function addNotification($entityManager, $params) {
       
        // $entry = new Notification();
        // $entry->setIdFriend($params['id_user']);
        // $entry->setIdUser($params['id_friend']);
        // $entry->setMessage($params['message']);
        // $entry->setType($params['type']);
		// $entry->setRead(0);

        // $entityManager->getConnection()->beginTransaction();
        // try {
            // $res = $entityManager->persist($entry);
            // $entityManager->flush();
			
            // $entityManager->getConnection()->commit();
        // } catch (Exception $e) {
			
            // $entityManager->getConnection()->rollback();
            // $entityManager->close();
            // throw $e;
        // }
    // }
	
	public static function addNotification($entityManager, $notifi)
	{
		
		
		$sql = "INSERT INTO `notifications` (`id_user`,`id_friend`,`type`,`message`,`readed`) values ('".$notifi['id_user']."','".$notifi['id_friend']."','".$notifi['type']."','".$notifi['message']."','".$notifi['read']."')";
		
		//echo $sql;die;
		
		$query = $entityManager->getConnection()->executeQuery($sql);
	    
		return $entityManager->getConnection()->lastInsertId();
	
	}
		public static function addNotificationForAudio($entityManager, $notifi)
	{
		
		
		  $sql = "INSERT INTO `notifications` (`id_user`,`id_friend`,`type`,`message`,`thumb`,`readed`) values ('".$notifi['id_user']."','".$notifi['id_friend']."','".$notifi['type']."','".$notifi['message']."','".$notifi['thumb']."','".$notifi['read']."')";
		
		
		$query = $entityManager->getConnection()->executeQuery($sql);
	    
		return $entityManager->getConnection()->lastInsertId();
	
	}
	
	public static function addNotificationNEW($entityManager, $sql){
		
		$query = $entityManager->getConnection()->executeQuery($sql);
	    
		return $entityManager->getConnection()->lastInsertId();
		
	}
	
    public static function addFriendNotification($entityManager, $params) {

        $sql = 'select firstname,lastname from sh_users where user_id = ' . $params['id_user'];

        $query = $entityManager->getConnection()->executeQuery($sql);
        $result = $query->fetchAll();
        $result = $result[0];

        $params['message'] = ucfirst($result['firstname']) .' ' . $params['message'];

        $entry = new Notification();
        $entry->setIdFriend($params['id_user']);
        $entry->setIdUser($params['id_friend']);
        $entry->setMessage($params['message']);
        $entry->setType($params['type']);
//        $entry->setDate('CURRENT_TIMESTAMP');


        $entityManager->getConnection()->beginTransaction();
        try {
            $res = $entityManager->persist($entry);
            $entityManager->flush();
            $entityManager->getConnection()->commit();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }

    /**
     * 
     * @param Doctrine\ORM\EntityManager $entityManager
     * @param type $params
     */
    
	public static function readNotification($entityManager, $id_notification) {
        $sql = 'UPDATE notifications SET readed= 1 WHERE id_notifications =' . $id_notification . ' ;';

        $session = new Session();
        $query = $entityManager->getConnection()->executeQuery($sql);

        return NotificationModel::getNotifications($entityManager, $session->getSession('userid'));
    }

    public static function readNotifications($entityManager, $user_id) {
        $own = $user_id > 0 ? $user_id : 0;
        $sql = 'UPDATE notifications SET readed= 1 WHERE id_user =' . $own . ' ;';

        $query = $entityManager->getConnection()->executeQuery($sql);

        return NotificationModel::getNotifications($entityManager, $user_id);
    }

    /**
     * 
     * @param Doctrine\ORM\EntityManager $entityManager
     * @param type $params
     */
    public static function getAllNotifications($entityManager) {
        $q = $entityManager->createQuery("select u from Notification u where u.id_user = ". $_SESSION['userid']."");
        $div = '';
        $repoUser = $entityManager->getRepository('UserRegister');
        $notifications = $q->getResult();
        foreach ($notifications as $notification) {        
            $user = $repoUser->find($notification->getIdFriend());
			
									$profile_pic='uploads/'.$user->getProfilePic();
										if($user->getProfilePic()==''){
												$profile_pic="uploads/default/Maledefault.png";
												
										}
			
			
            $div.= '<div class="lines">';
            if ($notification->getRead() == 0) {
                $div .= '<img src="images/read.png" class="readed"  onclick="readNotification(\'' . $notification->getId() . '\');$(this).hide();" title="Mark as Read"> </p>';
            }
            $div.='<img class="notif1" src="' . $profile_pic . '"></img> <a href="profile.php?profileid=' . base64_encode($user->getUser_id()) . '  ">  ' . ucfirst($user->getFirstName()). '  ' . ucfirst($user->getLastName()). ' </a>
                                        <br>
                                        <h4>' . $notification->getType() . ' - ' . $notification->getDate() . '</h4>
                                        <p>' . $notification->getMessage() . '';

            $div .='</div>';
        }
		$div .="<script>$(document).ready(function(){ 
             $(\".group1\").colorbox({  rel:'group1',iframe:true, width:\"85%\", height:\"85%\"});
             })             
;</script>";
        return $div;
    }
	

    /**
     * 
     * @param Doctrine\ORM\EntityManager $entityManager
     * @param type $params
     */
    public static function getNotifications($entityManager, $sh_user_receiver) 
	{
		
		$read = 1 ;

        $receiver = $sh_user_receiver > 0 ? $sh_user_receiver : 0;
        $sql = 'SELECT distinct(`id_notifications`), `id_friend` , profile_pic, `type` , `message` , `readed` , `date`
			FROM notifications, sh_users
			WHERE id_user = '.$receiver.' 
			AND readed = '.$read.'
			AND sh_users.user_id = id_friend  ORDER BY `notifications`.`readed` ASC,`notifications`.`date` desc  ';
      //echo $sql;
        $query = $entityManager->getConnection()->executeQuery($sql);
        $result = $query->fetchAll();
       
        return $result;
        
    }
	/**
     * 
     * @param Doctrine\ORM\EntityManager $entityManager
     * @param type $params
     */
    public static function getNotificationsWithTime($entityManager, $sh_user_receiver,$curDateTime) {

        $receiver = $sh_user_receiver > 0 ? $sh_user_receiver : 0;
        $sql = 'SELECT distinct(`id_notifications`), `id_friend` , profile_pic, `type` , `message` , `readed` , `date`
			FROM notifications, sh_users
			WHERE id_user = ' . $receiver . ' 
			AND sh_users.user_id = id_friend ORDER BY `notifications`.`readed` ASC,`notifications`.`date` ASC ';
      //echo $sql;
        $query = $entityManager->getConnection()->executeQuery($sql);
        $result = $query->fetchAll();

        return $result;
        
    }
	
    public static function getNotifications2($entityManager, $sh_user_receiver) {

        $receiver = $sh_user_receiver > 0 ? $sh_user_receiver : 0;
        $sql = 'SELECT `id_notifications`, `id_friend` , profile_pic, `type` , `message` , `readed` , `date`
				FROM notifications, sh_users
				WHERE id_user = ' . $receiver . '
				AND sh_users.user_id = id_friend ORDER BY `notifications`.`date` DESC LIMIT 1';
//        echo $sql;
        $query = $entityManager->getConnection()->executeQuery($sql);
        $result = $query->fetchAll();

        $unreaded = 0;
        if (count($result) > 0) {
            $var = '';
            $i = 0;
			$bg = '';
            foreach ($result as $item) {
                $bg = $i % 2 == 0 ? " nobg" : "";
//                <h4>' . NotificationModel::verifyType($item['type']) . '</h4>
                if ($item['readed'] == 0) {
                    $var .= '<div class="notificationwrap notification_content' . $bg . '" >
        <img src="uploads/' . $item['profile_pic'] . '" alt="Image Friend" class="dimg friendrequestimg" onclick="var url = \'profile.php?profileid=' . base64_encode($item['id_friend'] ). '\';$(location).attr(\'href\',url); "style="width:50px;height:50px;"/>  
<div class="notification_person">
<img class="n_image_right" src="img/notification/notificatin2.png" alt="">

                   <p onclick="var url = \'profile.php?profileid=' . base64_encode($item['id_friend']) . '\';$(location).attr(\'href\',url); ">' . $item['message'] . '      </p>
                    <div id="read_' . $item['id_notifications'] . '"><img src="images/read.png" style="float:right;cursor:pointer;display:block;"  onclick="readNotification(\'' . $item['id_notifications'] . '\');" title="Mark as Read"><p class="notificationtime">' . NotificationModel::getDiffDate($item['date']) . '</p></div> </div> </div>';
                    $unreaded++;
					
                }

                $i++;
               //if($limit == $i) break;
            }
			if(count($result)!= $i){
            $var .= '<div class="notificationwrap " onmouseover="getMoreNotificaitons()" >            
                        <img src="images/add.png" >
                        <img src="images/ajax-loader.gif" onclick="getMoreNotificaitons()" style="margin-bottom:10px; width: 30px; height:15px;" >                        
                     </div> ';
            }
            
            $number = $unreaded > 0 ? $unreaded : '';
 unset($_SESSION['notifications_number']);
            $_SESSION['notifications_number'] = $number;
            return $var;
        }
    }	
 private static function verifyType($type) {

        switch ($type) {
            case 1:
                return "Friendship Request";
                break;
            case 2:
                return "Friendship Accept";
                break;
            default:
                break;
        }
        return;
    }
	
 public static function getDiffDate($date) {

        $inicio = strtotime($date);
        $fim = strtotime('' . date('Y-m-d H:i:s', time()));


        $intervalo = $fim - $inicio;


        define('FATOR_ANO', (365 * 60 * 60 * 24));
        define('FATOR_MES', (30 * 60 * 60 * 24));
        define('FATOR_DIA', (60 * 60 * 24));
        define('FATOR_HORA', (60 * 60));
        define('FATOR_MINUTO', 60);
        $retorno = '';


        $anos = floor($intervalo / FATOR_ANO);
        if($anos>0){
            $retorno = $anos.' year ago';
            return $retorno;
        }

        $meses = floor(($intervalo / FATOR_MES));
        if ($meses > 0) {
            $retorno = $meses . ' month ago';
            return $retorno;
        }

        $dias = floor($intervalo / FATOR_DIA);
        if ($dias > 0) {

            $semanas = floor($dias / 7);

            if ($semanas >= 1) {
                $retorno = $semanas . 'week';
                if ($semanas == 1)
                    $retorno .= ' ago';
                else
                    $retorno .= 's ago';
                return $retorno;
            }
            $retorno = $dias . ' day';            
            return $retorno;
        }
        

        $horas = floor($intervalo / FATOR_HORA);
        if ($horas < 24 && $horas > 0) {
            $retorno = $horas . ' hr';
            if ($horas > 1)
                $retorno .= 's';
            
            return $retorno;
        }

        $minutos = floor($intervalo / FATOR_MINUTO);
        if ($minutos < 60 && $minutos > 0) {
            $retorno = $minutos . ' min';
            if ($minutos > 1)
                $retorno .= 's';
            
            return $retorno;
        }

        return $intervalo . ' s ago';


    }
	
	/*  public static function getNotification_update($entityManager, $sh_user_receiver) {

        $receiver = $sh_user_receiver > 0 ? $sh_user_receiver : 0;
        $sql = 'UPDATE notifications set `readed`=1 where `readed`=0 and `id_user`= ' . $receiver . '';
//        echo $sql;
        $query = $entityManager->getConnection()->executeQuery($sql);
        $result = $query->fetchAll();

        
//                <h4>' . NotificationModel::verifyType($item['type']) . '</h4>
           
    } */
	
	
	
public static function getNumber2() {
        $session = new Session();
        if (isset($_SESSION['notifications_number'])) {

            return 'block';
        }else
            return 'none';
    }
	
	
    public static function getNumber() {
        $session = new Session();
        if (isset($_SESSION['notifications_number'])) {

            $number = $session->getSession('notifications_number');
            unset($_SESSION['notifications_number']);
            return $number;
        }else
            return '';
    }
	public static function getCount($entityManager, $user_id){
	
	$sql = "SELECT * FROM `notifications` WHERE `id_user` = '$user_id' Order by `date` desc Limit 1";
//        echo $sql;
        $query = $entityManager->getConnection()->executeQuery($sql);
        $result = $query->fetchAll();

        if (count($result) > 0) {
            $var = '';
            $i = 0;
			$bg = '';
            foreach ($result as $item) {
            $readed=$item['readed'];}
			}
	return $readed;		
	}
	
	public static function countNotificationByUser($entityManager, $user_id)
	{
		
		$sql = "SELECT * FROM `notifications` WHERE `id_user` = '".$user_id."' AND `readed` = 0 ORDER BY id_notifications DESC ";
	    $query = $entityManager->getConnection()->executeQuery($sql);
		$result = $query->fetchAll();
		return $result;
	}
	
	public function getFriendProfile($entityManager, $friend_id)
	{
		$getuserprofile = "SELECT * FROM `sh_users` WHERE `user_id` = '".$friend_id."' " ;
		
		$query = $entityManager->getConnection()->executeQuery($getuserprofile);
				
		$result = $query->fetch();
		
		return $result;
	}
    public static function updateNotificationClick($entityManager,$comment_id){
	
	 $sql = "UPDATE notifications SET notification_click  = 1 WHERE comment_id = '".$comment_id."'";
	 $query = $entityManager->getConnection()->executeQuery($sql);      
	}
	public static function getNotificationClick($entityManager)
	{
		
		$getnotificationclick = "SELECT * FROM `notifications` WHERE `notification_click` = 1  ORDER BY id_notifications DESC" ;
		
		$query = $entityManager->getConnection()->executeQuery($getnotificationclick);
				
		$result = $query->fetchAll();
		
		return $result;
	}
	
}
