<?php
/*
 * jQuery File Upload Plugin PHP Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

error_reporting(E_ALL | E_STRICT);
ini_set('memory_limit','100M');
ini_set('post_max_size','50M');
ini_set('upload_max_filesize','50M');
require('UploadHandler.php');
$upload_handler = new UploadHandler();
