 /*jslint unparam: true, regexp: true */
            /*global window, $ */
            $(function () {
                'use strict';
                // Change this to the location of your server-side upload handler:
               var ispop = 0;
			   var jqXHR='';
				var url =  'uploadpl/server/php/',
                        uploadButton = $('<button/>')
                        .addClass('btn btn-primary upload-video-button')
                        .prop('disabled', true)
                        .text('Processing...')
                        .on('click', function () {
                           var $this = $(this),
                                    data = $this.data();
                            $this
                                    .off('click')
                                    .text('Abort')
                                    .on('click', function () {
                                        $this.remove();
                                        data.abort();
                                    });
                            data.submit().always(function () {
                                $this.remove();
                            });
                        });
                $('#fileupload').fileupload({
                    url: url,
                    dataType: 'json',
                    acceptFileTypes: /(\.|\/)(mp4|avi|flv)$/i,
                    maxFileSize: 99900000,
                    // Enable image resizing, except for Android and Opera,
                    // which actually support image resizing, but fail to
                    // send Blob objects via XHR requests:
                    disableImageResize: /Android(?!.*Chrome)|Opera/
                            .test(window.navigator.userAgent),
                    previewMaxWidth: 100,
                    previewMaxHeight: 100,
                    previewCrop: false
                }).on('fileuploadadd', function (e, data) {
                    $('.video-area').html('');
					$('.video-thumbnail-section').hide();
					$('#files').html('');
					
					data.context = $('<div/>').appendTo('#files');
					//jqXHR = data.submit(); // Catching the upload process of every file
                    $.each(data.files, function (index, file) {
                        var node = $('<p/>').addClass('upload-video-button-container')
                                .append($('<span/>').text(file.name));
                        if (!index) {
                            node
                                    .append('<br>')
                                    .append(uploadButton.clone(true).data(data));
                        }
                        node.appendTo(data.context);
					});
                }).on('fileuploadprocessalways', function (e, data) {
                    var index = data.index,
                            file = data.files[index],
                            node = $(data.context.children()[index]);
                    if (file.preview) {
                        node
                                .prepend('<br>')
                                .prepend(file.preview);
                    }
					
                    if (file.error) {
						alert('Invalid file type');
						node
                                .append('<br>')
                                .append($('<span class="text-danger"/>').text(file.error));
                    }else{
						ispop = 0;
						//$('.upload-video-button').click();
						$('#vtitle').val(data.files[0].name.replace(/\.[^/.]+$/, ""));
						jqXHR = data.submit(); 
					}
                    if (index + 1 === data.files.length) {
                        data.context.find('button')
                                .text('Upload')
                                .prop('disabled', !!data.files.error);
                    }
                }).on('fileuploadprogressall', function (e, data) {
					if(!ispop){
						$('#custom-bootstrap').prop('disabled',false);
						$('#modelid').modal({
							backdrop: 'static',
							keyboard: false
						});
						$('.upload-status-label').html('Your video is still <span class="video-upload-status">Uploading</span>. Please keep this page opne until it\'s done.');
						$('#video-post-button').prop('disabled', true);
						$('#video-post-button').addClass("disabled");
					}
					ispop = 1;
                    var progress = parseInt(data.loaded / data.total * 100, 10);
					$('.video-upload-progress').html( progress + '%');
					$('#progress .progress-bar').css(
                            'width',
                            progress + '%'
                            );
                }).on('fileuploaddone', function (e, data) {
                    $('.video-upload-status').html('Processing');
					$('.video-area').html("<img src='uploadpl/img/loading.gif' alt='Processing' style='margin:10px 0 0 50px;'/>");
                    
					$.ajax({
						url: 'processVideo.php',
						method: "post",
						data: data.result,
						type: "post",
						dataType:'json',
						success: function(responce){
							if (responce != '0') {
								if(responce.video.responce.length>0){	
									var	mainImg = 'uploads/videos/thumbnails/'+responce.video.folder+'/'+responce.video.responce[0];
									$('.video-area').html("<img src='"+mainImg+"' alt='Video Thumbnail' />");
									$('#selectedThumbnail').val(mainImg);
									var thumbnail = '';	
									var checked = '';
									$.each(responce.video.responce, function(i, item) {
										var checked = '';
										if(i<3){
										 if(!i){
										 checked = 'checked="checked"'
										 }
										 var	Img = 'uploads/videos/thumbnails/'+responce.video.folder+'/'+responce.video.responce[i];
										 thumbnail += "<li><img src='"+Img+"' width='131' height='76' /><input type='checkbox' class='videothumb' value='"+Img+"' "+checked+" /></li>";
										}
									});
									if(thumbnail!=''){
										$('.video-thumbnail-section').show();
										$('.video-thumbnail-section').html(thumbnail);
										$('.upload-status-label').html('Your video has been proccessed successfully.You may publish it now.');
									}
								}
								//imgSrc
								$('#vid').val(responce.video.vid);
								$('#video-post-button').prop('disabled', false).removeClass('disabled');
								 
								}
						},
						error: function(){
							alert('There are some errors');
						}
					});
					
					

//                    $.each(data.result.files, function (index, file) {
//                        if (file.url) {
//                            var link = $('<a>')
//                                    .attr('target', '_blank')
//                                    .prop('href', file.url);
//                            $(data.context.children()[index])
//                                    .wrap(link);
//                        } else if (file.error) {
//                            var error = $('<span class="text-danger"/>').text(file.error);
//                            $(data.context.children()[index])
//                                    .append('<br>')
//                                    .append(error);
//                        }
//                    });
                }).on('fileuploadfail', function (e, data) {
                    $.each(data.files, function (index) {
                        var error = $('<span class="text-danger"/>').text('File upload failed.');
                        $(data.context.children()[index])
                                .append('<br>')
                                .append(error);
                    });
                }).prop('disabled', !$.support.fileInput)
                        .parent().addClass($.support.fileInput ? undefined : 'disabled');
            
			$('#videoPostfrm').submit(function(){
				$('#vtitle').css({'border-color':'#c3c3c3'});
				$('.title-error').hide();
				if($.trim($('#vtitle').val()) == ''){
					$('.title-error').show();
					$('#vtitle').css({'border-color':'red'})
					return false;
				}
				
				$.ajax({
						url: 'postvideo.php',
						method: "post",
						data: $( this ).serialize(),
						type: "post",
						success: function(responce){
							$('#custom-bootstrap').prop('disabled',true);
							document.getElementById("videoPostfrm").reset();
							$('.upload-status-label').html('Your video is still <span class="video-upload-status">Uploading</span>. Please keep this page opne until it\'s done.');
						    $('.video-upload-status').html('Uploading');
							$('.video-area').html('');
							$('#video-colse-button').click();
							$(".wallEntries").prepend(responce);
							videojs('#my-video'+$('#wall_entry_id').val(), {}, function(){
							  // Player (this) is initialized and ready.
							});
							setTimeout(function(){
								
								$('#progress'+$('#wall_entry_id').val()).css({'-webkit-transition':'none !important',' transition':'none !important'});
								
								$('#progress'+$('#wall_entry_id').val()).animate({
								 width: "100%",
								}, 8000);
								
								var myVar =setInterval(function(){
								var w = $('#progress'+$('#wall_entry_id').val()).width();
								var wp = (w/parseInt($('#progress'+$('#wall_entry_id').val()).parent().width()))*100;
								wp = wp.toFixed(2)
							if(wp>=100){
									
									wp = 100;
									clearInterval(myVar);
									$('#progress'+$('#wall_entry_id').val()).parent().hide();
									$('#my-video'+$('#wall_entry_id').val()).css({'opacity':'1'});
									$('#my-video'+$('#wall_entry_id').val()+'_html5_api').css({'opacity':'1'});
									$('#testy').toastee({
										type: 'success',
										message: 'Your Video is ready to view.!!!!!',
										width: 300,
										height: 150
									});
									postIds.push(parseInt($('#wall_entry_id').val()));
								}
								$('#progress'+$('#wall_entry_id').val()).html(wp+'% Perparing...');
								}, 100);
								$('#wall'+$('#wall_entry_id').val()).fadeIn(300);
							},100);
							
						},
						error: function(){
							alert('There are some errors in posting this video.');
						}
					});
				return false;
			});
			
			$('#video-cancel-button').on('click',function(){
			 if(confirm('Are you sure you want to be discard this video post?')){
				//window.location.href='home.php';
				//window.location.reload();
				jqXHR.abort();
				$('.video-area').html('');
				document.getElementById("videoPostfrm").reset();
				$('.upload-status-label').html('Your video is still <span class="video-upload-status">Uploading</span>. Please keep this page opne until it\'s done.');
				$('.video-upload-status').html('Uploading');
				$('#custom-bootstrap').prop('disabled',true);
				$('#video-colse-button').click();
			}
			});
			
			$('body').on('click','.videothumb',function(){
				$('.videothumb').prop( "checked", false );
				$(this).prop("checked", true );
				$('.video-area').html("<img src='"+$(this).val()+"' alt='Video Thumbnail' />");
				$('#selectedThumbnail').val($(this).val());
			})
			
});