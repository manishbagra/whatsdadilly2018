 <?php

$curl = "https://social.yahooapis.com/v1/user/me/contacts?format=json&count=1000&access_token=" . $_REQUEST['access_token'];
// create curl resource
$ch   = curl_init();
// set url
curl_setopt($ch, CURLOPT_URL, $curl);

//return the transfer as a string
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

// $output contains the output string
$output = curl_exec($ch);
$output = json_decode($output);
$data   = array();
foreach ($output->contacts->contact as $contact) {
    
    $name = '---';
    foreach ($contact->fields as $key => $value) {
        
        if (isset($value->type) && $value->type == 'name') {
            $name = $value->value->givenName . ' ' . $value->value->middleName;
        }
        if (isset($value->type) && $value->type == 'email') {
            $data[] = array(
                'name' => $name,
                'email' => $value->value
            );
        }
    }
}
//print_r($data);
echo $data = json_encode(array(
    'data' => $data
));
// close curl resource to free up system resources
curl_close($ch);
?> 