 <?php
ini_set("display_errors", "1");

error_reporting(E_ALL);

require_once "bootstrap.php";
require_once 'classes/Session.class.php';
require_once 'model/Instagram.php';
require_once 'youtube/Google/autoload.php';
require_once 'youtube/Google/Client.php';
require_once 'youtube/Google/Service/YouTube.php';

include_once("config.php");

$session = new Session();

$instaObj = new Insta();

if ($session->getSession("userid") != "" || $session->getSession("userid") != null) {
    
    $VIDEO_ID = $_POST['vidId'];
    
    $TEXT = $_POST['text'];
    
    $client = new Google_Client();
    $client->setClientId(YOUTUBE_CONSUMER_KEY);
    $client->setClientSecret(YOUTUBE_CONSUMER_SECRET);
    $client->setScopes(array(
        'https://www.googleapis.com/auth/youtube',
        'https://www.googleapis.com/auth/userinfo.email',
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/youtube.force-ssl'
    ));
    $client->setRedirectUri(YOUTUBE_CONSUMER_CALLBACK);
    
    $youtube = new Google_Service_YouTube($client);
    
    $commentSnippet = new Google_Service_YouTube_CommentSnippet();
    
    $commentSnippet->setTextOriginal($TEXT);
    
    $topLevelComment = new Google_Service_YouTube_Comment();
    
    $topLevelComment->setSnippet($commentSnippet);
    
    $commentThreadSnippet = new Google_Service_YouTube_CommentThreadSnippet();
    
    $commentThreadSnippet->setTopLevelComment($topLevelComment);
    
    $commentThread = new Google_Service_YouTube_CommentThread();
    
    $commentThread->setSnippet($commentThreadSnippet);
    
    $commentThreadSnippet->setVideoId($VIDEO_ID);
    
    $videoCommentInsertResponse = $youtube->commentThreads->insert('snippet', $commentThread);
    
    print_r($videoCommentInsertResponse);
    die;
    
} 