 <?php
error_reporting(0);
session_start();
require_once "bootstrap.php";
require_once 'model/Albums.php';
// require_once 'model/ProfileTech.php';
require_once 'model/Profile.php';
require_once 'model/Wall.php';
require_once 'entities/Album.php';
require_once 'classes/Session.class.php';
require_once 'classes/Albums.class.php';
require_once 'model/Friends.php';
require_once 'model/Comments.php';
require_once 'model/PhotosModel.php';
include 'html/wall/functions.php';

$frd_obj = new Friends();
//require_once 'model/Albums.php';

$albums = new Albums();

$photoPath = array();

$session = new Session();

$_SESSION['debug'] = 0;

$userid = $session->getSession('userid');



if ($_POST['func'] == "listPhotos") {
    $params = array(
        'id_album' => $_POST['qtd']
    );
    
    
    
    
    
    $phId_array = $_POST['pid_array'];
    
    $explode_array = explode(',', $phId_array);
    
    //echo '<pre>'; print_r($explode_array);die;
    
    $countPhoto = $_POST['count'];
    
    //echo $countPhoto ;die;
    
    $friends = Friends::getFriendsList($entityManager, $session->getSession('userid'), 5000);
    
    $text = '';
    
    $resultsPhoto = $albums->getPhotosByAlbumId($entityManager, $_POST['qtd']);
    
    foreach ($explode_array as $photos) {
        $image_path = "uploads/" . $userid . "/albums/" . $_POST['qtd'] . "/" . $photos;
        
        $images = glob($image_path . "*.{jpg,png,gif}", GLOB_BRACE);
        
        foreach ($images as $filepath) {
            $file    = basename($filepath);
            $explode = explode(".", $file);
            if ($explode[0] == $photos) {
                $ext = $explode[1];
            }
        }
        
        $photoPath[] = "uploads/" . $userid . "/albums/" . $_POST['qtd'] . "/" . $photos . "." . $ext;
        
    }
    
    
    $paths = $session->getSession('send_photos_path');
    
    $album = $session->getSession('album_title'); //    
    
    foreach ($friends as $friend) {
        $params = array(
            'id_friend' => $session->getSession('userid'),
            'id_user' => $friend['user_id'],
            'message' => addslashes("<span>" . $session->getSession('firstname') . " " . $session->getSession('lastname') . " </span>publish new album . You can comment it and share. "),
            'thumb' => $photoPath[0],
            'type' => '3',
            'read' => 0
        );
        $sql    = "INSERT INTO `notifications` (`id_user`,`id_friend`,`type`,`message`,`thumb`,`readed`) values ('" . $params['id_user'] . "','" . $params['id_friend'] . "','" . $params['type'] . "','" . $params['message'] . "','" . $params['thumb'] . "','" . $params['read'] . "')";
        
        //echo $sql;die;
        
        $query = $entityManager->getConnection()->executeQuery($sql);
        
        
    }
    
    if (count($photoPath) > 1)
        $i = 0;
    if (count($photoPath) <= 9) {
        $linhas = floor(count($photoPath) / 3);
        $resto  = count($photoPath) % 3;
        $tam1   = '182px';
        $tam2   = '276.5px;';
        $tam3   = '558px';
    }
    $text .= '<div class="wall_photoboard gallery-img">';
    $i = 1;
    foreach ($photoPath as $path) {
        $index3 = explode('/', $path);
        
        $index0 = explode('.', $index3[3]);
        
        if ($i <= ($linhas * 3)) {
            $tamAtual = $tam1;
        } else {
            if ($resto == 2) {
                $tamAtual = $tam2;
            } else
                $tamAtual = $tam3;
        }
        
        if ($tamAtual == '558px') {
            
            $text .= '<a class="mid2wall" href="javascript:void(0)" >
        <img  style="width: ' . $tamAtual . '; height: 400px; border-radius:px; margin: 2px 0 0 2px;border:1px solid #ccc !important;"  src="' . $path . '"/>
        </a>';
            
        } elseif ($tamAtual == "") {
            $text .= '<a class="mid2wall" href="javascript:void(0)" >
        <img  style="width: 182px; height: 182px; border-radius:px; margin: 2px 0 0 2px;border:1px solid #ccc !important;"  src="' . $path . '"/>
        </a>';
        } else {
            $text .= '<a class="mid2wall" href="javascript:void(0)" >
        <img  style="width: ' . $tamAtual . '; height: ' . $tamAtual . '; border-radius:px; margin: 2px 0 0 2px;border:1px solid #ccc !important;"  src="' . $path . '"/>
        </a>';
        }
        
        $i++;
        
        if ($i > 9) {
            break;
        }
        
    }
    $text .= '</div>';
    
    $params = array(
        'owner_id' => '' . $session->getSession('userid'),
        'author_id' => '' . $session->getSession('userid'),
        'text' => '',
        'link' => './album_detail.php?id=' . $_POST['qtd'],
        'date' => date('Y-m-d H:i:s')
    );
    
    $id = WallModel::addWallEntry($entityManager, $params);
    
    if ($id) {
        
        WallModel::updateCount($entityManager, $id, $countPhoto);
        
        //$albums->updateWallId($entityManager,$id,$params['id_album']);
        
    }
    
    unset($_SESSION['send_photos']);
    
    unset($_SESSION['send_photos_path']);
    
    $album = new Album();
    $album = $entityManager->getRepository('Album')->find($_POST['qtd']);
    $album->setIdWall($id);
    $entityManager->persist($album);
    $entityManager->flush();
    
    unset($params);
    
    $params = array(
        'id_album' => $_POST['qtd'],
        'other' => true
    );
    
    $result = Albums::getPhotos($entityManager, $params);
    
    echo $result;
    
}

if ($_POST['func'] == "setCover") {
    
    $params = array(
        'id_album' => $_POST['id_album'],
        'id_photo' => $_POST['id_photo']
    );
    
    Albums::setCover($entityManager, $params);
    
}

if ($_POST['func'] == "listAlbum") {
    $params = array(
        'id_owner' => $_SESSION['userid']
    );
    $result = Albums::listAlbum($entityManager, $params);
    echo $result;
}


if ($_POST['func'] == "remove_album") {
    $params             = array();
    $params['id_album'] = $_POST['id_album'];
    Albums::removeAlbum($entityManager, $params);
    unset($params);
    $params = array(
        'id_owner' => $_SESSION['userid']
    );
}

//this if will check if when the popup were open he send some photo
//if not he removes the album.
// the album must have at leat one picture
if ($_POST['func'] == "remove_open") {
    $params = array();
    //id_album its the album thats were uploading or seeing the photos.
    
    $params['id_album'] = isset($_SESSION['open_album']) ? $_SESSION['open_album'] : $_SESSION['id_album'];
    
    //$album = Albums::removeIfEmpty($entityManager, $params);
    
    
    //if were uploaded some photo the function will return a Album instance else he remove that entry
    
    if ($album instanceof Album) {
        $friends = Friends::getFriendsList($entityManager, $session->getSession('userid'), 5000);
        
        $text  = '';
        $paths = $session->getSession('send_photos_path');
        
        
        foreach ($friends as $friend) {
            $params = array(
                'id_friend' => $friend['user_id'],
                'id_user' => '' . $session->getSession('userid'),
                //'message' => $text . '<a href="album_detail.php?id=' . $album->getIdAlbum() . '"><img src="images/camera-image-uplaod.png"> </a>Created an album ' . $album->getTitle(),
                'message' => '<a href="album_detail.php?id=' . $album->getIdAlbum() . '"><img src="images/camera-image-uplaod.png"> </a>Created an album ' . $album->getTitle(),
                'type' => '5'
            );
            
            NotificationModel::addNotification($entityManager, $params);
        }
        //tech // $text = 'Created an album ' . $album->getTitle() . ' ' . $text; 
        
        
        $i = 0;
        if (count($paths) <= 9) {
            $linhas = floor(count($paths) / 3);
            $resto  = count($paths) % 3;
            $tam1   = '143px';
            $tam2   = '216.5px;';
            $tam3   = '436px';
        }
        $text .= '<div class="wall_photoboard">';
        $i = 1;
        foreach ($paths as $path) {
            
            if ($i <= ($linhas * 3)) {
                $tamAtual = $tam1;
            } else {
                if ($resto == 2) {
                    $tamAtual = $tam2;
                } else
                    $tamAtual = $tam3;
            }
            
            $text .= '<a class="mid2wall" href="photo_detail.php?id=' . $path['id'] . '" ><img  style="width: ' . $tamAtual . '; height: ' . $tamAtual . '; border-radius:px; margin: 2px 0 0 2px;border:1px solid #ccc !important;"  src="' . $path['path'] . '"/></a>';
            $i++;
            if ($i >= 9 && count($paths) > 9) {
                //tech // $text.= ' and ' . (count($paths) - 9).' more photos';
                break;
            }
            
        }
        $text .= '</div>';
        
        $album->setCover($paths[0]['id']);
        $entityManager->persist($album);
        $entityManager->flush();
        
        $params = array(
            'owner_id' => '' . $session->getSession('userid'),
            'author_id' => '' . $session->getSession('userid'),
            'text' => $text,
            'link' => './album_detail.php?id=' . $album->getIdAlbum(),
            //            'link_photo' => str_replace('.jpg', '', AlbumUtil::getFirstPhotoAlbum($album->getIdAlbum())),
            'date' => date('Y-m-d H:i:s')
        );
        
        $id = WallModel::addWallEntry($entityManager, $params);
        $album->setIdWall($id);
        $entityManager->persist($album);
        $entityManager->flush();
        
        //        $entityManager = new Doctrine\ORM\EntityManager;
        $sql = 'update photo_album set id_wall =' . $id . ' where id_album =' . $album->getIdAlbum();
        
        $entityManager->getConnection()->executeQuery($sql);
    }
    unset($_SESSION['send_photos_path']);
    unset($_SESSION['send_photos']);
    unset($params);
    $params = array(
        'id_owner' => $_SESSION['userid']
    );
    $result = Albums::listAlbum($entityManager, $params);
    echo $result;
}

if ($_POST['func'] == "alter_title") {
    $album = new Album();
    $album = $entityManager->getRepository('Album')->find($_SESSION['id_album']);
    $album->setTitle($_POST['title']);
    $entityManager->persist($album);
    $entityManager->flush();
    
}

if ($_POST['func'] == "alter_title_photo") {
    $album = new Album();
    $album = $entityManager->getRepository('PhotoAlbum')->find($_SESSION['photo_open']);
    $album->setTitle($_POST['title']);
    $entityManager->persist($album);
    $entityManager->flush();
}



if ($_POST['func'] == "remove_photo") {
    
    $params             = array();
    $params['id_photo'] = $_POST['id_photo'];
    $params             = Albums::removePhoto($entityManager, $params);
    unset($params['id_photo']);
    $params['other'] = true;
    $result          = Albums::getPhotos($entityManager, $params);
    echo $result;
}

// added by tech 

if ($_POST['func'] == "add_cover_photo") {
    $wall              = array();
    $wall['author_id'] = $_SESSION['userid'];
    $wall['owner_id']  = $_SESSION['userid'];
    $wall['text']      = '';
    $wall['date']      = date('Y-m-d H:i:s');
    $id_wall           = WallModel::addWallEntry($entityManager, $wall);
    $params            = array();
    
    $params['wall_id']   = $id_wall;
    $params['file']      = $_POST['imgPath'];
    $params['is_cover']  = 1;
    $params['author_id'] = '' . $session->getSession('userid');
    
    $params = Profile::addCoverPhoto($entityManager, $params);
    
    unset($params);
    $thumb = 'coverpic/images/' . $_POST['imgPath'];
    
    $user_id       = $session->getSession('userid');
    $get_user_info = $frd_obj->getUserInfoById($entityManager, $user_id);
    
    $fullname = $get_user_info['firstname'] . ' ' . $get_user_info['lastname'];
    
    $frnds_info = $frd_obj->getFriendsEmail($entityManager, $user_id);
    
    
    $data            = array();
    $data['user_id'] = $user_id;
    $data['thumb']   = $thumb;
    $data['type']    = 10; //for cover photo
    if ($get_user_info['gender'] == "Male") {
        $data['msg'] = addslashes("<span>" . $fullname . "</span> updated his billboard photo.");
    } else {
        $data['msg'] = addslashes("<span>" . $fullname . "</span> updated her billboard photo.");
    }
    $data['reded'] = 0;
    if ($get_user_info['gender'] == "Male") {
        $msg = " updated his billboard photo";
    } else {
        $msg = " updated her billboard photo";
    }
    foreach ($frnds_info as $key => $value) {
        
        $noti_id_user = $value['user_id'];
        
        $sql = "INSERT INTO `notifications` (`id_user`,`id_friend`,`type`,`message`,`thumb`,`readed`) values ('" . $noti_id_user . "','" . $data['user_id'] . "','" . $data['type'] . "','" . $data['msg'] . "','" . $data['thumb'] . "','" . $data['reded'] . "')";
        
        $notifications->addNotificationNEW($entityManager, $sql);
        
        
    }
    $previous_post = WallModel::getPrviousPost($entityManager, $id_wall);
    $comments      = CommentsModel::getComments($previous_post[0]['id'], $entityManager);
    $photos        = PhotosModel::getPhotos($previous_post[0]['id'], $entityManager);
    
    $previous_post[0]['comments'] = $comments;
    $previous_post[0]['photos']   = $photos;
    $postids                      = 0;
    
    foreach ($previous_post as $entry) {
?>

        <div class="middle_area_four crispbx crispbxmain" id="wall<?php
        echo $entry['id'];
?>"
             style="margin-bottom:5px;" data="<?php
        echo $entry['id'];
?>" data-count="<?php
        echo $postids;
?>">

            <div class="slider_two_title">


                <img class="image_border_style" src="uploads/<?php
        echo $_SESSION['profile_pic'];
?>" alt=""
                     style="width:49px;height:49px;"/>

                <div class="slider_title_style2">

                    <span
                        class="author_slide_top author_upload_name"><?php
        echo $entry['firstname'];
?><?php
        echo $entry['lastname'];
?></span><?php
        if ($get_user_info['gender'] == "Male") {
            echo $msg;
        } else {
            echo $msg;
        }
?>
                   <p class="update_profile_date"><?php
        echo WallModel::time_elapsed_string($entry['date']);
?>
                   </p></div>

            </div>
            <div style="padding:10px;width:100%;">
                <p>
                    <?php
        echo Functions::addLink($entry['text']);
?></p></div>

            <?php
        if (strlen($entry['link']) > 0) {
?>
               <div class="display_profile_pic">
                    <?php
            if (strlen($entry['link_photo']) > 0):
?>
                       <img src="<?php
                echo $entry['link_photo'];
?>" alt=""/>
                    <?php
            endif;
?>
                   <div style="padding:10px;">

                        <p><a target="_blank" href="<?php
            echo $entry['link'];
?>"
                              style="color: #000;font-size: 18pt;line-height: 1.2em;"><?php
            echo $entry['link_title'];
?></a>
                        </p>
                        <p style="font-size:12pt;"><?php
            echo $entry['link_description'];
?></p></div>
                    <div class="clear"></div>
                </div>
            <?php
        }
?>

            <div style="padding:10px;">
              
                <?php
        if (!empty($entry['photos'])) {
?>
                   <div class="crispbx">
                        <div class="crispcont">

                            <!-- <p> -->
                            <?php
            $i = 0;
            //echo'<pre>';print_r(count($entry['photos']));die;
            if (count($entry['photos']) >= 2) {
                //echo'<pre>';print_r(count($entry['photos']));die;
                $linhas = floor(count($entry['photos']) / 3);
                $resto  = count($entry['photos']) % 3;
                
                $tam1h = '143px';
                $tam2h = '216.5px';
                $tam3h = '436px';
                
                $tam1w = '32.8%';
                $tam2w = '49.2%';
                $tam3w = '99.1%';
                
            }
            //echo'<pre>';print_r('aaaa');die;
            $photoDiv .= '<div class="wall_photoboard">';
            $i = 1;
            foreach ($entry['photos'] as $path) {
                
                if ($i <= ($linhas * 3)) {
                    
                    // $tamAtual = $tam1;
                    
                    $tamAtual  = $tam1w;
                    $tamAtualh = $tam1h;
                    
                } else {
                    if ($resto == 2) {
                        
                        // $tamAtual = $tam2;
                        
                        $tamAtual  = $tam2w;
                        $tamAtualh = $tam2h;
                        
                    } else
                    // $tamAtual = $tam3;
                        $tamAtual = $tam3w;
                    $tamAtualh = $tam3h;
                    
                }
                
                $margin = 'margin: 2px 0 0 2px';
                if (count($entry['photos']) == 1) {
                    $margin    = 'margin: 0';
                    $tamAtualh = '100%';
                    $tamAtual  = '100%';
                }
                
                $photoDiv .= '<a class="mid2wall" href="javascript:void(0)" ><img  style="width: ' . $tamAtual . '; height: ' . $tamAtualh . '; border-radius:px;' . $margin . ';border:1px solid #ccc !important;"  src="' . $thumb . '"/></a>';
                $i++;
                if ($i >= 9 && count($entry['photos']) > 9) {
                    $photoDiv .= ' and ' . (count($entry['photos']) - 9) . ' more photos';
                    break;
                }
                
            }
            $photoDiv .= '</div>';
            
            echo $photoDiv;
            
            $photoDiv = '';
?>
                           <!-- photos uploaded</p> -->
                            <!-- <div class="upimgwrap">

                                <div class="big-photo-container">
                                    <a class="wall_popup"
                                       href="wall_popup.php?wall_id=<?php
            echo $entry['id'];
?>&photoid=<?php
            echo $entry['photos'][0]['id'];
?>&postion=0">
                                        <img src="uploads/<?php
            echo $entry['photos'][0]['file'];
?>" alt=""
                                             class="upbigimg"/>
                                    </a>
                                </div>
                                <div class="upsmimg photo-grid-container">
                                    <?php
            $postion = 1;
            foreach ($entry['photos'] as $key => $photo) {
                if ($key == 0)
                    continue;
?>
                                       <div class="small-photo-container photo-grid-item">
                                            <a class="wall_popup"
                                               href="wall_popup.php?wall_id=<?php
                echo $entry['id'];
?>&photoid=<?php
                echo $photo['id'];
?>&postion=<?php
                echo $postion;
?>">
                                                <img src="uploads/<?php
                echo $photo['file'];
?>" alt=""/>
                                            </a>
                                        </div>
                                        <?php
                $postion++;
            }
?>
                               </div>
                            </div> -->
                        </div>
                    </div>
                <?php
        }
?>


            </div>

            <div class="like_comment_share">


                <a href="#"><span><i class="fa fa-thumbs-up"></i>Like</span></a>

                <a href="#"><span><i class="fa fa-heart"></i>Love it</span></a>
                <a href="#" class="add-comment-link" rel="<?php
        echo $entry['id'];
?>"><span><i
                            class="fa fa-commenting"></i>Comment</span></a>
                <a href="#"><span><i class="fa fa-share-square-o"></i>Share</span></a>


            </div>    <?php
        
        if (empty($_SESSION['profile_pic'])) {
            $profile_pic = "uploads/default/Maledefault.png";
            
        } else {
            
            $profile_pic = 'uploads/' . $_SESSION['profile_pic'];
        }
?>
                       <div class="comment_box " id="comment_box_<?php
        echo $entry['id'];
?>"  style="display: block;" rel="">

                        <form rel="<?php
        echo $entry['id'];
?>" id="commform_<?php
        echo $entry['id'];
?>" action=""
                              method="post" enctype="multipart/form-data" onsubmit="return false;">
                            <img src="<?php
        echo $profile_pic;
?>" alt=""
                                 style="width:37px;height:37px;">
                            <span><input id="postid_<?php
        echo $entry['id'];
?>" class="comment_box_inline comment_input"
                                         placeholder="Write your comment here" type="text"></span>
                            <div class="comment_box_icon comment_box_inline postbutton-comments" id="<?php
        echo $entry['id'];
?>">
                            <a href="javascript:void"><span><i
                                            class="comment_box_left fa fa-camera"></i></span></a>
                               </div>
                            <h5 class="press_enter_post">Press enter to post: </h5>
                        </form>
                    </div>


            <div data="<?php
        echo $entry['id'];
?>" id="comment_<?php
        echo $entry['id'];
?>">
                <?php //echo '<pre>'; print_r($entry['comments']); 
?>
               <?php
        foreach ($entry['comments'] as $comment):
            $cuserdetails = WallModel::getUserDetails($entityManager, $comment['author_id']);
        //echo '<pre>'; print_r($cuserdetails);
?>

                    <div class="slider_two_title">
                        <img src="uploads/<?php
            echo $cuserdetails[0]['profile_pic'];
?>"
                             alt="<?php
            echo $comment['1firstname'];
?> <?php
            echo $comment['1lastname'];
?>"
                             style="width:37px;height:37px;">
                        <div class="slider_title_style 1single-comment">
                            <a href="#"><span
                                    class="author_slide_top"><?php
            echo $comment['firstname'];
?><?php
            echo $comment['lastname'];
?></span></a>
                            <span
                                class="comment_box_bottom_text"><?php
            echo Functions::addLink($comment['text']);
?></span>
                            <a href="#"><p class="like_and_reply"><span>Like</span><span
                                        class="comment_reply">Reply</span>
                                    <?php
            echo date('m/d/Y H:i:s', strtotime($comment['date']));
?></p></a>
                        </div>
                    </div>
                    <div class="line"></div>
                <?php
        endforeach;
?>
           </div>


        </div>


       <?php
        $postids++;
    }
    
}

if ($_POST['func'] == "del_cover_photo") {
    
    $params              = array();
    $params['wall_id']   = 100;
    $params['author_id'] = '' . $session->getSession('userid');
    Profile::removeCoverPhoto($entityManager, $params);
    unset($params['wall_id']);
    //    $params['other'] = true;
    //    $result = Albums::getPhotos($entityManager, $params);
    //    echo $result;
}
//var_dump(AlbumUtil::userHaveFolder());  