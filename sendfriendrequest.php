 <?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require 'bootstrap.php';
require_once 'entities/Friend.php';
require_once 'entities/Notification.php';
require_once 'model/Friends.php';
require_once 'model/Wall.php';
require_once 'model/Notification.php';
require_once 'model/SendMail.php';
require_once 'classes/Session.class.php';
$session = new Session();
if (isset($_REQUEST['invtiations']) && count($_REQUEST['invtiations']) > 0) {
    $friend = new Friends();
    foreach ($_REQUEST['invtiations'] as $item) {
        echo $result = $messages->sendFriendRequest($entityManager, $session->getSession('userid'), $item);
        
        $params = array(
            'id_friend' => '' . $item,
            'id_user' => '' . $session->getSession('userid'),
            'message' => 'thanks for adding me',
            'type' => '1'
        );
        
        NotificationModel::addFriendNotification($entityManager, $params);
        SendMail::sendFriendshipRequest($entityManager, $params['id_user'], $params['id_friend']);
    }
    echo 'ok';
}
?> 