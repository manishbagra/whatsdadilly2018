<?php
error_reporting(-1);
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once "bootstrap.php";
require_once 'model/Signup.php';
require_once 'classes/Session.class.php';

$signupObject = new Signup();

$session = new Session();

$country_id = $_POST['country_id'];

$getAllState = $signupObject->getAllStateByCountryId($entityManager, $country_id);
?>
<link rel="stylesheet" href="css/choosen.css">
<script type="text/javascript" src="js/choosen.js"></script>
<script>
$(document).ready(function(){
        $("#state").chosen();
        
        $("#state_chosen").on('click',function(){
            var singleValue = $("div#state_chosen a.chosen-single span").html();
            
            $("div#state_chosen .chosen-search input").val(singleValue);
        });
        
        $("#state").on('change',function(){
            var state_id = $(this).val();
            //alert(state_id);
            $.ajax({
                type     : 'post',
                dataType : 'html',
                url      : 'getCityByState.php',
                data     : {state_id : state_id},
                success  : function(data){
                    $("#cities").html(data);
                }
            });
        }); 
        
        $("#login_btn2").on('click',function(){
                                    
            // if($("#country").val()==""){
                // $("#ErrCountry").show();
            // }
            if($("#state").val()==""){
                $("#ErrState").show().delay(5000).fadeOut();
            }
            // if($("#cities").val()==""){
                // $("#ErrCities").show().delay(5000).fadeOut();
                // return false;
            // }
            
        });
});    
</script>
<div class="" id='states'>Current Province
<select required name="state" id="state" value="" class="form-control chosen">
    <option value="">Select Province</option>
    <?php
foreach ($getAllState as $state) {
?>
   <option value="<?php
    echo $state['id'];
?>"><?php
    echo $state['name'];
?></option>
    <?php
}
?>
</select>
</div>
