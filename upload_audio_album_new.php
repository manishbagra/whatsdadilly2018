<?php
error_reporting(-1);

require 'getid3/getid3.php';
require_once "bootstrap.php";
require_once 'model/Wall.php';
require_once 'model/mp3file.php';
include_once("config.php");
require_once 'classes/Session.class.php';

$session = new Session();

$WallModel = new WallModel();

$json_array = array();

$userid = $session->getSession('userid');

$output_dir = __DIR__ . "/uploads/tmp_audio/";

$output_img_dir = $output_dir . '/tmp_img/';

//echo "<pre>" ; print_r($_FILES);die;

$name = $_FILES["file"]["name"];

$temp_name = $_FILES["file"]["tmp_name"];

//echo $temp_name ;die;

$allowed = array(
    'iff',
    'wav',
    'flac',
    'alac',
    'ogg',
    'mp2',
    'mp3',
    'aac',
    'amr',
    'wma',
    'm4a'
);

$ext = pathinfo($name, PATHINFO_EXTENSION);

if (!in_array($ext, $allowed)) {
    
    $json_array = array(
        "status" => false
    );
    
} else {
    
    $getID3 = new getID3;
    
    $fileinfo = $getID3->analyze($temp_name);
    
    $data['filesize'] = $WallModel->formatSizeUnits($fileinfo['filesize']);
    
    $data['title'] = @$fileinfo['id3v1']['title'];
    
    $data['picture'] = @$fileinfo['id3v2']['APIC'][0]['data'];
    
    $data['type'] = @$fileinfo['id3v2']['APIC'][0]['image_mime'];
    
    if ($data['picture'] == "") {
        
        $data['picture'] = @$fileinfo['comments']['picture'][0]['data'];
        
        $data['type'] = @$fileinfo['comments']['picture'][0]['image_mime'];
        
    }
    
    
    $data['artist'] = @$fileinfo['id3v1']['artist'];
    
    $data['album'] = @$fileinfo['id3v1']['album'];
    
    if (empty($data['filesize'])) {
        
        $size = '0:00';
    } else {
        $size = $data['filesize'];
    }
    
    if (empty($data['title'])) {
        $revnm   = strrev($name);
        $explext = explode('.', $revnm);
        unset($explext[0]);
        $imploded = implode(' ', $explext);
        $title    = strrev($imploded);
    } else {
        $title = $data['title'];
    }
    
    if (empty($data['artist'])) {
        $artist = 'No artist found.';
    } else {
        $artist = $data['artist'];
    }
    if (empty($data['album'])) {
        $album = 'No album name found.';
    } else {
        $album = $data['album'];
    }
    
    $uniId = uniqid(rand());
    
    $songName = $uniId . '.' . $ext;
    
    if (move_uploaded_file($temp_name, $output_dir . $songName)) {
        
        $exp_ext = explode('/', $data['type']);
        
        if (!empty($data['picture'])) {
            
            file_put_contents($output_img_dir . $uniId . '.' . $exp_ext[1], $data['picture']);
            
            $src = 'uploads/tmp_audio/tmp_img/' . $uniId . '.' . $exp_ext[1];
            
        } else {
            
            $src = 'images/bg.png';
            
        }
        
        
        $json_array = array(
            'status' => true,
            'songName' => $songName,
            'name' => $name,
            'size' => $size,
            'title' => $title,
            'img' => $src,
            'artist' => $artist,
            'album' => $album
        );
        
        
        
    } else {
        
        $json_array = array(
            "status" => false
        );
        
    }
    
}

echo json_encode($json_array);
exit();