 <?php
//error_reporting(-1);
require_once "bootstrap.php";
require_once "entities/Videos.php";
require_once "model/VideosModel.php";
require_once 'model/Wall.php';
require_once 'model/Comments.php';
include 'html/wall/functions.php';
include_once("config.php");
require_once 'model/PhotosModel.php';
include_once("twitteroauth/twitteroauth.php");
require_once 'classes/Session.class.php';
session_start();
$videos        = new VideosModel();
$param         = $_POST;
$thisVideo     = $videos->getVedio($entityManager, $_POST['vid']);
$dateTime      = $_REQUEST['curTime'];
$differSecTime = $_REQUEST['differSec'];

$dateinsec = strtotime($dateTime);
$newdate   = $dateinsec + $differSecTime;
$finalTime = date('Y-m-d H:i:s', $newdate);
$params    = array(
    'owner_id' => $_POST['owner_id'],
    'author_id' => $_POST['author_id'],
    'text' => $_POST['title'],
    'link' => '',
    'link_description' => '',
    'link_photo' => '',
    'link_title' => '',
    'date' => date('Y-m-d H:i:s')
);

$param['wall_id'] = WallModel::addWallEntry($entityManager, $params);
$videos->saveVideoDetails($entityManager, $param, $thisVideo);
$previous_post = WallModel::getPrviousPost($entityManager, $param['wall_id']);

foreach ($previous_post as $key => $value) {
    $comments = CommentsModel::getComments($previous_post[$key]['id'], $entityManager);
    $photos   = PhotosModel::getPhotos($previous_post[$key]['id'], $entityManager);
    
    $previous_post[$key]['comments'] = $comments;
    $previous_post[$key]['photos']   = $photos;
    $previous_post[$key]['videos']   = VideosModel::getVideos($previous_post[$key]['id'], $entityManager);
}
?>
<?php

$postids = 0;
foreach ($previous_post as $entry) {
    
    $userdetails = WallModel::getUserDetails($entityManager, $entry['author_id']);
    if ($entry['type'] == 1) {
        $infodetails = WallModel::getUserDetails($entityManager, $entry['owner_id']);
    }
?>
   
   <div class="middle_area_four crispbx crispbxmain" id="wall<?php
    echo $entry['id'];
?>" style="margin-bottom:5px;display:none;" data="<?php
    echo $entry['id'];
?>" data-count="<?php
    echo $postids;
?>"> 
  
   <input type="hidden" id="wall_entry_id" value="<?php
    echo $entry['id'];
?>" />

<div class="slider_two_title">


<img class="image_border_style" src="uploads/<?php
    echo $_SESSION['profile_pic'];
?>" alt="" style="width:49px;height:49px;"/>

            <div class="slider_title_style2">

<span class="author_slide_top author_upload_name Font18">

<a style="font-size:16px;" href="./profile.php?profileid=<?php
    echo base64_encode($userdetails[0]['user_id']);
?> ">
<?php
    echo $userdetails[0]['firstname'];
?> <?php
    echo $userdetails[0]['lastname'];
?>
</a>
<?php
    if ($entry['type'] == 1) {
        //echo '<i class="fa fa-caret-right" style="color:#A4A4A4;margin:0 5px;"></i>' .$infodetails[0]['firstname'].' '.$infodetails[0]['lastname'];
        $text = ' <span style="font-size:14px;">posted on </span>';
        if (trim($entry['link']) != '') {
            $text = ' <span style="font-size:14px;">shared a <a href="' . $entry['link'] . '" target="_blank">link</a> on</span> ';
        }
        $username = ' <a style="font-size:16px;" href="./profile.php?profileid=' . base64_encode($infodetails[0]['user_id']) . '">' . $infodetails[0]['firstname'] . ' ' . $infodetails[0]['lastname'] . '</a> ';
        if ($_SESSION['userid'] == $infodetails[0]['user_id']) {
            $username = ' <span style="font-size:14px;">Your</span> ';
        }
        echo $text . $username . '<span style="font-size:14px;">Whiteboard</span>';
    }
    echo '<br /><span style="font-size:12px;font-weight:normal;">' . WallModel::time_elapsed_string($entry['date']) . '</span>';
?>

</span>

<p class="update_profile_date">
</p></div>

</div>
<?php
    if (!empty($entry['videos'])) {
?>
<div style="width: 98%;margin-left: 5px;">
<div class="progress"  style="background:none;background-color:#f5f5f5;padding-right:0px;">
      <div class="progress-bar" id="progress<?php
        echo $entry['id'];
?>" role="progressbar" aria-valuenow="70"
      aria-valuemin="0" aria-valuemax="100" style="width:0%">
        <span>Processing...</span>
      </div>
</div>
</div>
<?php
    }
?>
<div style="padding:10px;width:100%;">
<p>                                    
               <?php
    echo Functions::addLink($entry['text']);
?></p></div>

<?php
    if (strlen($entry['link']) > 0) {
?>
<div class="display_profile_pic">
<?php
        if (strlen($entry['link_photo']) > 0):
?>
                           <img src="<?php
            echo $entry['link_photo'];
?>" alt="" />
        <?php
        endif;
?>
                       <div style="padding:10px;">

                    <p><a target="_blank" href="<?php
        echo $entry['link'];
?>" style="color: #000;font-size: 18pt;line-height: 1.2em;"><?php
        echo $entry['link_title'];
?></a></p>
               <p style="font-size:12pt;"><?php
        echo $entry['link_description'];
?></p></div>
                <div class="clear"></div>
            </div>
<?php
    }
?>

<div style="padding:10px;">

<?php
    if (!empty($entry['photos'])) {
?>
            <div class="crispbx">
                <div class="crispcont">

                    <!--<p>-->
                    <?php //echo count($entry['photos']) 
?> 
                    <!--photos uploaded</p> -->
                        <div class="upimgwrap">

                            <div class="big-photo-container">
                                <a class="wall_popup" href="wall_popup.php?wall_id=<?php
        echo $entry['id'];
?>&photoid=<?php
        echo $entry['photos'][0]['id'];
?>&postion=0">
                                    <img src="uploads/<?php
        echo $entry['photos'][0]['file'];
?>" alt=""  class="upbigimg"/>
                                </a>
                            </div>
                            <div class="upsmimg photo-grid-container">
<?php
        $postion = 1;
        foreach ($entry['photos'] as $key => $photo) {
            if ($key == 0)
                continue;
?>
                       <div class="small-photo-container photo-grid-item">
                            <a class="wall_popup" href="wall_popup.php?wall_id=<?php
            echo $entry['id'];
?>&photoid=<?php
            echo $photo['id'];
?>&postion=<?php
            echo $postion;
?>">
                                <img src="uploads/<?php
            echo $photo['file'];
?>" alt="" />
                            </a>
                        </div>
<?php
            $postion++;
        }
?>
                   </div>
                </div>
            </div>
        </div>
<?php
    }
?>



<?php
    if (!empty($entry['videos'])) {
        $poster = 'uploads/videos/thumbnails/' . $entry['videos'][0]['id'] . '/thumb_1.jpg';
        if ($entry['videos'][0]['thumbnail']) {
            $poster = $entry['videos'][0]['thumbnail'];
        }
        $ext = pathinfo($entry['videos'][0]['file'], PATHINFO_EXTENSION);
?>

   <video id="my-video<?php
        echo $entry['id'];
?>"  class="video-js  vjs-default-skin" controls  width="560" 
  poster="<?php
        echo $poster;
?>" style="opacity:0.4;" data-setup="{}" style="opacity:0.4;" >
    <source src="<?php
        echo $entry['videos'][0]['file'];
?>" type='video/<?php
        echo $ext;
?>'>
    <p class="vjs-no-js">
      To view this video please enable JavaScript, and consider upgrading to a web browser that
      <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
    </p>
  </video>
<?php
        echo '<br>' . $entry['videos'][0]['description'];
        echo '<br> Tags: ' . $entry['videos'][0]['tags'] . ',' . $entry['videos'][0]['tags_more'];
    }
?>


</div>

        <div class="like_comment_share">


<a href="#"><span><i class="fa fa-thumbs-up"></i>Like</span></a>

                            <a href="#"><span><i class="fa fa-heart"></i>Love it</span></a>
                                                       <a href="#" class="add-comment-link" rel="<?php
    echo $entry['id'];
?>" ><span><i class="fa fa-commenting"></i>Comment</span></a>
                            <a href="#"><span><i class="fa fa-share-square-o"></i>Share</span></a>

            
                </div>
<div data="<?php
    echo $entry['id'];
?>" id="post_comment_<?php
    echo $entry['id'];
?>"> </div>
<div class="comment_box" rel="<?php
    echo $entry['id'];
?>">



   <form rel="<?php
    echo $entry['id'];
?>" id="commform_<?php
    echo $entry['id'];
?>" action="" method="post" enctype="multipart/form-data" onsubmit="return false;">


                            <img src="uploads/<?php
    echo $_SESSION['profile_pic'];
?>" alt="" style="width:37px;height:37px;">
                            <span>
                            <input class="comment_box_inline grybord comment_input" placeholder="Write your comment here" id="postid_<?php
    echo $entry['id'];
?>" name="comment" type="text" autocomplete="off"></span>
                            <div class="comment_box_icon comment_box_inline"><a href="#"><span><i class="comment_box_left fa fa-camera"></i></span></a>
                            <a href="#"><span><i class="comment_box_left2 fa fa-smile-o"></i></span></a></div>
                            <h5 class="press_enter_post">Press enter to post</h5>
    </form>
    </div>


<div data="<?php
    echo $entry['id'];
?>" id="comment_<?php
    echo $entry['id'];
?>"> 
<?php //echo '<pre>'; print_r($entry['comments']); 
?>
                                           <?php
    foreach ($entry['comments'] as $comment):
        $cuserdetails = WallModel::getUserDetails($entityManager, $comment['author_id']);
    //echo '<pre>'; print_r($cuserdetails);
?>
                   
<div class="slider_two_title">
                                <img src="uploads/<?php
        echo $cuserdetails[0]['profile_pic'];
?>" alt="<?php
        echo $comment['1firstname'];
?> <?php
        echo $comment['1lastname'];
?>" style="width:37px;height:37px;">
                                <div class="slider_title_style 1single-comment">
                                    <a href="#"><span class="author_slide_top"><?php
        echo $comment['firstname'];
?> <?php
        echo $comment['lastname'];
?></span></a>
                                    <span class="comment_box_bottom_text"><?php
        echo Functions::addLink($comment['text']);
?></span>
                                    <a href="#"><p class="like_and_reply"><span>Like</span><span class="comment_reply">Reply</span> 路 <?php
        echo date('m/d/Y H:i:s', strtotime($comment['date']));
?></p></a>                                    
                                </div>
                            </div><div class="line"></div>
<?php
    endforeach;
?>
</div>


</div>




<?php
    $postids++;
}
?>
<?php
die();

?> 