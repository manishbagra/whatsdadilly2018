 <?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once "bootstrap.php";

require_once 'model/Comments.php';
include 'html/wall/functions.php';
include_once("config.php");

$postId         = $_POST['post_id'];
$start          = $_POST['start'];
$length         = $_POST['length'];
$totalComments  = count(CommentsModel::getCountCommentsMainPost($postId, $entityManager));
$getAllComments = CommentsModel::getCommentsMainPostWithLimit($postId, $start, $length, $entityManager);
$getAllComments = array_reverse($getAllComments);
if ($totalComments > ($start + $length)):
    $start = $start + $length;
    echo "<div id='loadmore_" . $postId . "' class='loadmorecomment'><a onclick='loadMore(" . $postId . "," . $start . "," . $length . ")'>Load More (" . $length . ")</a><span class='loadspin loadno'><img src='images/loader.gif'></div>";
endif;
foreach ($getAllComments as $comment) {
    //print_r($comment);
    //$commentId = $commentVal['id'];
    $replies            = CommentsModel::getCommentOfReply($comment['id'], $entityManager);
    $comment['replies'] = $replies;
    
?>

<div class="media new">
      <div class="media-left media-middle">
        <a href="#">
          <img class="media-object" style='width:48px;height:47px;margin-bottom:41px;' src="uploads/<?php
    echo $comment['profile_pic'];
?>" alt="...">
        </a>
      </div>
      <div class="media-body lefts">
        <h4 class="media-heading"><?php
    echo $comment['firstname'] . ' ' . $comment['lastname'];
?></h4>
         <span><?php
    echo $comment['text'];
?></span>
         <br/>
        <span><a href="">like </a> <a href="">reply</a> <p><?php
    echo date('F j, Y, g:i a', strtotime($comment['date']));
?></p></span>
        
      </div>
</div>

<div class="line"></div>
<?php
    if (count($comment['replies'])) {
        $allReplies = $comment['replies'];
        foreach ($allReplies as $replyKey => $replyValue) {
?>
<div class="media new" id="reply_<?= $replyValue['id'] ?>">
      <div class="media-left media-middle">
        <a href="#">
          <img class="media-object" style='width:48px;height:47px;margin-bottom:41px;' src="uploads/<?php
            echo $replyValue['profile_pic'];
?>" alt="...">
        </a>
      </div>
      <div class="media-body lefts">
        <h4 class="media-heading"><?php
            echo $replyValue['firstname'] . ' ' . $replyValue['lastname'];
?></h4>
         <span><?php
            echo $replyValue['text'];
?></span>
         <br/>
        <span><a href="">like </a> <a href="">reply</a> <p><?php
            echo date('F j, Y, g:i a', strtotime($replyValue['date']));
?></p></span>
        
      </div>
</div>

<?php
        }
    }
?>

<div class="line"></div>

<?php
}
?> 