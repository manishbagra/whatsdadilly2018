<?php
//error_reporting(-1);

require 'bootstrap.php';
require_once 'entities/Album.php';
require_once 'model/Albums.php';
require_once 'model/Wall.php';
require_once 'classes/Albums.class.php';
require_once 'classes/Session.class.php';

$album_desc    = "";
$picDesc       = array();
$albumLocation = "";

$last_photo_id = 0;

if (!empty($_POST)) {
    
    $imageNames = (array) json_decode($_POST['count']);
    
    $picDesc = explode(',', $_POST['picDesc']);
    
    $image64 = explode('@DadillY@', $_POST['image64']);
    
    //echo '<pre>';print_r($image64);die;
    
    $albumLocation = $_POST['album_location'];
    
    $album_desc = $_POST["description"];
    
    
    $output_dir = __DIR__ . "/uploads";
    
    $session = new Session();
    
    $userid = $session->getSession('userid');
    
    $params = array();
    
    $params['userid'] = $userid;
    
    $params['id_album'] = $session->getSession('id_album');
    
    $params['title'] = $_POST['title'];
    
    
    $photo = AlbumUtil::getAlbum($entityManager, $params);
    
    if ($photo == false) {
        die('trying to acess an album that isnt yours');
    }
    
    AlbumUtil::userHaveFolder();
    
    $output_dir .= "/" . $userid . "/";
    
    if (!file_exists($output_dir)) {
        $mask = umask(0);
        mkdir($output_dir, 0777);
        umask($mask);
    }
    
    
    $output_dir .= $params['id_album'] . "/";
    
    if (!file_exists($output_dir)) {
        $mask = umask(0);
        mkdir($output_dir, 0777);
        umask($mask);
    }
    
    
    $resp = array();
    
    $total_photos = count($image64);
    
    $last_photo_id = Albums::getLastPhotoIdOnly($entityManager);
    
    if ($last_photo_id == '') {
        $last_photo_id = 0;
    }
    
    $i = $last_photo_id + 1;
    
    $pIds_array = array();
    
    foreach ($image64 as $key => $value) {
        
        $pIds_array[] = $i;
        
        $i++;
        
        $params = array(
            'id_owner' => $userid,
            'id_album' => $params['id_album'],
            'title' => $params['title'],
            'pic_desc' => addslashes($picDesc[$key])
        );
        
        $photo = Albums::createPhoto($entityManager, $params);
        
        if ($key == 0) {
            
            $photoId = $photo->getIdPhoto();
            
            $params1 = array(
                'id_album' => $params['id_album'],
                'title' => addslashes($params['title']),
                'id_photo' => $photoId,
                'about_album' => addslashes($album_desc),
                'location' => addslashes($albumLocation)
            );
            
            
            Albums::setCover($entityManager, $params1);
            
            
        }
        
        
        $dir = __DIR__ . "/uploads/temp/";
        
        $expl_ext = explode('.', $value);
        
        $extVal = $expl_ext[1];
        
        $extVal = 'jpg';
        
        if ($extVal == 'jpeg' || $extVal == 'JPEG') {
            $extNM = -5;
        } else {
            $extNM = -4;
        }
        
        if (@copy($dir . $value, $output_dir . str_replace(substr($value, 0, $extNM), $photoId, $value))) {
            if (unlink($dir . $value)) {
                $photoId = $photoId + 1;
                continue;
            } else {
                echo "Del Failed";
                exit;
            }
        }
        
    }
    
    $message = array(
        "flag" => "1",
        'album_id' => $params['id_album'],
        'total_photos' => $total_photos,
        'photos_array' => $pIds_array
    );
    
    //echo '<pre>'; print_r($message);die;
    
    echo json_encode($message);
    exit();
    
} else {
    echo "Failed";
    exit;
}