<script src="video-player/js/video.min.js"></script>

<?php

require_once "bootstrap.php";
require_once 'model/Wall.php';
require_once 'model/PhotosModel.php';
require_once 'model/Comments.php';
include 'html/wall/functions.php';
require_once 'model/Instagram.php';
require_once 'model/VideosModel.php';
require_once 'model/Audio.php';
require_once 'classes/Session.class.php';

if($_GET['count'] != 'undefined')
{

	
$wall_id = $_GET['wall_id'];	

	$limit1 = $_GET['count'] + 1;

   $limit2 = $_GET['count'] + 11;
 


$data = null;

$AudioModel  = new AudioModel();

$WallModel   = new WallModel();

$session 	 = new Session();

$insta   	 = new Insta();

$function 	 = new Functions();

$video_model = new VideosModel();

if (isset($_GET['profileid']) && $_GET['profileid'] != 'undefined'){

    $data = ["userid" => base64_decode($_GET['profileid'])];

    $entries = $WallModel->getUserInfiniteWall($entityManager, $limit1, $limit2, $data);
	
}else{
	
	$entries = $WallModel->getEntriesInfinit($entityManager, $limit1, $limit2);
	
} 

$count = count($entries);

if ($count != 1) {
    foreach ($entries as $key => $value) {
		
        $comments = CommentsModel::getCommentsMainPostWithLimit($entries[$key]['id'], 0, 5, $entityManager);
        $totalComments = count(CommentsModel::getCountCommentsMainPost($entries[$key]['id'], $entityManager));
    
        $photos = PhotosModel::getPhotos($entries[$key]['id'], $entityManager);
       
        foreach ($comments as $keyId => $commentVal) {
            $commentId = $commentVal['id'];
            $replies = CommentsModel::getCommentOfReply($commentId, $entityManager);
            $comments[$keyId]['replies'] = $replies;
           
        }

        $entries[$key]['comments_count'] = $totalComments;
		
        $entries[$key]['comments'] = array_reverse($comments);

        $entries[$key]['photos'] = $photos;
		
		$entries[$key]['videos'] = $video_model->getVideos($entries[$key]['id'], $entityManager);
		
		$entries[$key]['audios'] = $AudioModel->getAudios($entries[$key]['id'], $entityManager);
		
		
    }
   
    $postids = $limit1;
   
    foreach ($entries as $entry) {
      
        $userdetails = $WallModel->getUserDetails($entityManager, $entry['user_id']);
       
        if ($entry['type'] == 1) {
			
            $userdetails = $WallModel->getUserDetails($entityManager, $entry['author_id']);
			
            $infodetails = $WallModel->getUserDetails($entityManager, $entry['owner_id']);
			
        } else {
            $userdetails = $WallModel->getUserDetails($entityManager, $entry['user_id']);
        }
        ?>
		
		

		    <div class="middle_area_four crispbx crispbxmain" style="margin-bottom:5px;" id="wall<?php echo $entry['id']; ?>" data="<?php echo $entry['id']; ?>"  data-count="<?php echo $postids; ?>">

                    <div class="slider_two_title">
					
						<?php  
						if($userdetails[0]['profile_pic']==''){ ?>
						
						   <img src="uploads/default/Maledefault.png" alt=""
                             style="width:49px;height:49px;"/>
							 <?php
							
							
						}else {?>
					   <img src="uploads/<?php echo $userdetails[0]['profile_pic']; ?>" alt=""
                             style="width:49px;height:49px;"/>
						<?php }?>
                     
                        <div class="slider_title_style2">

	<!--------------------- Fancybox ----------------->		
						
    <span class="author_slide_top author_upload_name Font18">
	<a style="font-size:16px;" href="profilesummery.php?profileid=<?php echo base64_encode ($userdetails[0]['user_id']); ?>" onClick="return hs.htmlExpand(this, { objectType: 'ajax'} )" ><?php echo $userdetails[0]['firstname']; ?><?php echo $userdetails[0]['lastname']; ?></a>

<?php if($entry['count_photos'] != 0 ){ ?>
<span style="color:#90949C;font-size:12px"> added <?php echo $entry['count_photos'];  ?> new photos. </span>
<?php } ?>
	<?php if ($entry['type'] == 1) {
            
            $text = ' <span style="font-size:14px;">posted on </span>';
            if (trim($entry['link']) != '') {
                $text = ' <span style="font-size:14px;">shared a <a href="' . $entry['link'] . '" target="_blank">link</a> on</span> ';
            }

            $username = ' <a style="font-size:16px;" href="./profile.php?profileid=' . base64_encode($infodetails[0]['user_id']) . '">' . $infodetails[0]['firstname'] . ' ' . $infodetails[0]['lastname'] . '</a> ';
            if ($_SESSION['userid'] == $infodetails[0]['user_id']) {
                $username = ' <span style="font-size:14px;">your</span> ';
            }
            echo $text . $username . '<span style="font-size:14px;">Whiteboard</span>';
        }
        echo '<br><span style="font-size:12px;font-weight:normal;">' . WallModel::time_elapsed_string($entry['date']) . '</span>&nbsp;';
       ?> 
	  
		<span style="font-weight:normal !important;font-size:13px"><?php if(@$entry['post_type']==2){ echo "via - <img src='images/instragram.png' style='width:2.7%;' alt=''> - Instagram" ; }?></span>
		
    </span>
	                   
                        <span id='foo'></span>
                            <p class="update_profile_date"></p>
                        </div>
						<?php
                          if(strlen($entry['link']) > 0){
							   $IdArray1 = explode('=',$entry['link']);
                               $About_Album  =  Albums::getAlbumInfoByAlbumId($entityManager,$IdArray1[1]);	
							
						 if($IdArray1[0] == "./album_detail.php?id"){  ?>
						<span style="font-family:Roboto;font-size: 15px;font-weight: 300;text-overflow: ellipsis;"><strong><?php echo $About_Album['title']; ?></strong><?php echo " : ".$About_Album['about_album']; ?>
						<br/>
						<i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp;<span style="color:gray;font-size:13px"><?php echo $About_Album['location']; ?></span></span>
						</span>
			            
						<?php }  } ?>
				   </div>
				   
				   
				   
		
					


                    <div style="width:100%;padding:10px;color:#333;">
                        <p style="font-size:15px;">
						<a style="color:#333;">
						   <?php if(!empty($entry['text'])){ echo $function->addLink($entry['text']); } ?>
						</a>   
						
						</p>
					</div>
                    <?php if (strlen($entry['link']) > 0 && $entry['link_title'] !='') {
						
                        ?>
                        <div class="display_profile_pic" style='width:97.5%;border: 1px solid #ccc;box-shadow: 0 0 1px #ccc;line-height: 16px;overflow: hidden;border-radius:4px;'>
                            <?php if (strlen($entry['link_photo']) > 0): ?>
                                <a target="_blank" href="<?php echo $entry['link'] ?>">
								<img src="<?php echo $entry['link_photo'] ?>" alt=""/>
								</a>
                            <?php endif ?>
							
							 

                            <div style="padding:10px;">
                                <p>
								<a target="_blank" href="<?php echo $entry['link']; ?>"
                                      style="color: #000;font-size: 18pt;line-height: 1.2em;"><span style='font-size:20px;font-family: TimesNewRoman,Times New Roman,Times,Baskerville,Georgia,serif;line-height: 20px;text-align:justify;'><?php  echo $entry['link_title']; ?></span></a>
                                </p>
                                <p style="font-size:12pt;font-family: TimesNewRoman,Times New Roman,Times,Baskerville,Georgia,serif;text-align:justify;"><?php echo $entry['link_description'] ?></p>
                            </div>
							
                        </div>
                    <?php } ?>


                    <div style="padding:10px;font-size:14px;color:#333;">

                        <?php if (!empty($entry['photos'])) { ?>
                            <div  class="crispbx">
                                <div class="crispcont1">

                                    <!-- <p> -->
                                    <?php 
									$photoDiv = '';
                                      
                                        $i = 0;
                                        if (count($entry['photos']) <= 9) {
                                            $linhas = floor(count($entry['photos']) / 3);
                                            $resto = count($entry['photos']) % 3;

                                            $tam1h = '143px';
                                            $tam2h = '216.5px';
                                            $tam3h = '436px';
                                                                                        
                                            $tam1w = '32.8%';
                                            $tam2w = '49.2%';
                                            $tam3w = '99.1%';
                                            
                                        }
                                        $photoDiv .= '<div class="wall_photoboard">';
                                        $i = 1;
                                        foreach ($entry['photos'] as $path) {

                                            if($i <= ($linhas * 3)){

                                                $tamAtual = $tam1w;
                                                $tamAtualh = $tam1h;   

                                            }else{
												
                                                if ($resto == 2) {
                                                    $tamAtual = $tam2w;
                                                    $tamAtualh = $tam2h;

                                                }else{
													$tamAtual = $tam3w;
                                                    $tamAtualh = $tam3h;
												}   
                    
                                            }

                                            $margin = 'margin: 2px 0 0 2px';
                                            if (count($entry['photos']) == 1) {
                                                $margin = 'margin: 0';
                                                $tamAtualh = '100%';
                                                $tamAtual = '100%';
                                            }
                                            $photoDiv .= '<a class="mid2wall" href="photo_detail.php?id=' . $path['id'] . '" ><img  style="width: ' . $tamAtual . '; height: ' . $tamAtualh . '; border-radius:px;' . $margin .';border:1px solid #ccc !important;"  src="uploads/' . $path['file'] . '"/></a>';
                                            $i++;
                                            if ($i >= 9 && count($entry['photos']) > 9) {
                                                $photoDiv .= ' and ' . (count($entry['photos']) - 9) . ' more photos';
                                                break;
                                            }

                                        }
                                        $photoDiv .= '</div>';

                                        echo $photoDiv;

                                        $photoDiv = '';

                                     ?>
                                    
           <!--  <div class="upimgwrap">





                <div class="big-photo-container1">

                    <a class="wall_popup group1" href="wall_popup.php?wall_id=<?php //echo $entry['id']; ?>&photoid=<?php //echo $entry['photos'][0]['id']; ?>&postion=0">

                        <img src="uploads/<?php //echo $entry['photos'][0]['file'] ?>" alt=""  class="upbigimg"/>

                    </a>

                </div>

                <div class="upsmimg">

                    <?php

                    $postion = 1;

                    foreach ($entry['photos'] as $key => $photo) {

                        if ($key == 0)

                            continue;

                    ?>

                        <div class="small-photo-container">

                            <a class="wall_popup group1" href="wall_popup.php?wall_id=<?php //echo $entry['id']; ?>&photoid=<?php// echo $photo['id']; ?>&postion=<?php //echo $postion; ?>">                                                                            <img src="uploads/<?php //echo $photo['file'] ?>" alt="" />

                            </a>

                        </div>

                    <?php } ?>

                </div>

            </div> -->
					</div>
				</div>
			<?php } ?>
		</div>
		
		
				
		<?php  if (!empty($entry['videos'])) {	
		
		$count = count($entry['videos']);
		
		switch($count){
			
			case $count === 1:
			
			$padding_bottom = '67.25%';
			
			break ;
			
			case $count === 2:
			
			$padding_bottom = '41.25%';
			
			break ;
			
			case $count === 3:
			
			$padding_bottom = '32.25%';
			
			break ;
			
			case $count >= 4 && $count <= 6 :
			
			$padding_bottom = '59.25%';
			
			break ;
			
			case $count >= 7 && $count <= 9 :
			
			$padding_bottom = '86.25%';
			
			break ;
			
		}
		
		?>
		
		<div style="padding:10px;font-size:14px;color:#333;">
			
			<div style="padding-bottom: <?php echo $padding_bottom; ?>;display:block;" class="card mb-3 single-post">
			
			<?php	
			
			if (count($entry['videos']) <= 9) {

				$lin = floor(count($entry['videos']) / 3);

				$rest = count($entry['videos']) % 3;		

				$tamv1w = '32.5%';$tamv1h = '150px';

				$tamv2w = '49.2%';$tamv2h = '200px';

				$tamv3w = '99.1%';$tamv3h = '350px';

			}
			
			$i = 1; 
			
			foreach($entry['videos'] as $key=>$videoShow){
				
			$poster = 'uploads/videos/thumbnails/' . $videoShow['id'] . '/thumb_1.jpg';

				if ($videoShow['thumbnail']) {

					$poster = $videoShow['thumbnail'];

				}

				$ext = pathinfo($videoShow['file'], PATHINFO_EXTENSION);	
			
			if ($i <= ($lin * 3)) {

				$tamvAtual = $tamv1w; $tamvAtualh = $tamv1h;
				
			} else {

				if ($rest == 2) {

					$tamvAtual = $tamv2w; $tamvAtualh = $tamv2h;

				}else{

					$tamvAtual = $tamv3w; $tamvAtualh = $tamv3h;
				
				}
					
			}	
			
			echo '<style>
				.single-post #video_'.$videoShow['id'].' {
				position: relative;
				float:left;
				margin:2px;
				
				top: 0;
				left: 0;
				width: '.$tamvAtual.';
				height: '.$tamvAtualh.';
				}
				</style>';
			
			
			?>
					
				<!-- Start video Area -->
					
				<video id="video_<?php echo $videoShow['id']; ?>" data-video="<?php echo $videoShow['file'] ?>" controls class="video-js vjs-default-skin rs_video_player vjs-big-play-centered"  poster="<?php echo $poster; ?>" data-setup='{"playbackRates": []}' >

					<source src="<?php echo $videoShow['file'] ?>"  type="video/mp4"></source>

				</video>	
				
				
			<?php 

			if($key > 8){ break; } }

			

			?>		
				
			</div>
			
		

		<?php  
		
		
		if(count($entry['videos']) === 1 ){
			
			echo $entry['videos'][0]['description'];

				if (trim($entry['videos'][0]['tags']) != '') {

					echo '<br> Tags: ' . $entry['videos'][0]['tags'] . ',' . $entry['videos'][0]['tags_more'];
					
					echo "<br><br>";

				}
				
				
			} ?>
			
			</div>


	<?php	} ?>
		
					
		<?php if (!empty($entry['audios'])) { ?>
	

			<div class="player_body" style='background-image: url("<?php echo $entry['audios']['thumb']; ?>");'><!-- Start Player Body -->


                <div class="player_content">

                    <div class="column add-bottom">

                        <div id="mainwrap">

                            <div id="nowPlay">

                                <h3 class="left" id="songTitle_<?php echo $entry['audios']['id']; ?>"><?php echo $entry['audios']['title']; ?></h3>

                                <p  class="right" id="songAuther_<?php echo $entry['audios']['id']; ?>"><?php echo $entry['audios']['playlist_desc']; ?></p>

                            </div>

                            <div id="audiowrap">

                                <div id="waveform_<?php echo $entry['audios']['id']; ?>" data='<?php echo $entry['audios']['file']; ?>' class='waveform' ></div>


							   <!-- <div id="time-total"></div> -->

                                <span class="waveform__counter" id='counter_<?php echo $entry['audios']['id']; ?>'></span>

                                <span class="waveform__duration" id='duration_<?php echo $entry['audios']['id']; ?>'></span>
								<script>
													
									    var wavesurfer_<?php echo $entry['audios']['id']; ?>='';
										
											wavesurfer_<?php echo $entry['audios']['id']; ?> = WaveSurfer.create({
													container		: 	'#waveform_<?php echo $entry['audios']['id']; ?>',
													waveColor		: 	'#B2B2B2',
													cursorColor		: 	'transparent',
													progressColor	: 	'#fd8331',
													barWidth		:	'3',
													height			:	'180',
													renderer		: 	'MultiCanvas',
													pixelRatio		:  	 5,
													timeInterval	: 	 100,
													backend			: 	'MediaElement',
												});
											
											
											// Change Volume  
											wavesurfer_<?php echo $entry['audios']['id']; ?>.setVolume(0.4);
											
											document.querySelector('#volume_<?php echo $entry['audios']['id']; ?>').value = wavesurfer_<?php echo $entry['audios']['id']; ?>.backend.getVolume();
									
									</script>	
                                <div class="custom_controls"> 

                                    <div class="volbox">

                                        <i id="toggleMuteBtn" class="fas fa-volume-up"></i>

                                        <input class="volume" id="volume_<?php echo $entry['audios']['id']; ?>" type="range" min="0" max="1" value="0.1" step="0.1">

                                    </div>

                                   <span class='mp3_menu_right'>

									<a class="backward" onclick="backward(wavesurfer_<?php echo $entry['audios']['id']; ?>,<?php echo $entry['audios']['id']; ?>)"><i class="fa fa-backward"></i></a>
					
									<span id="playPause_<?php echo $entry['audios']['id']; ?>">

										<button id='ply_<?php echo $entry['audios']['id']; ?>' onclick="play_pause(wavesurfer_<?php echo $entry['audios']['id']; ?>,<?php echo $entry['audios']['id']; ?>,'<?php echo $entry['audios']['file']; ?>')">

											<i class="fas fa-play"></i>

										</button>
										
										<!--<button style='display:none;' id='pause_<?php //echo $entry['audios']['id']; ?>' onclick="pause(wavesurfer_<?php //echo $entry['audios']['id']; ?>,<?php //echo $entry['audios']['id']; ?>)">

											<i class="fas fa-pause"></i>

										</button>
										
										<button style='display:none;' id='plya_<?php //echo $entry['audios']['id']; ?>' onclick="playAgain(wavesurfer_<?php //echo $entry['audios']['id']; ?>,<?php //echo $entry['audios']['id']; ?>)">

											<i class="fas fa-play"></i>

										</button>-->

									</span>
					
                                    <a class="forward" onclick="forward(wavesurfer_<?php echo $entry['audios']['id']; ?>,<?php echo $entry['audios']['id']; ?>)" ><i class="fa fa-forward"></i></a>

									<a onClick="play_pause(wavesurfer_<?php echo $entry['audios']['id']; ?>,<?php echo $entry['audios']['id']; ?>,'<?php echo $entry['audios']['file']; ?>')" id="repeateFun"><i class="fas fa-redo-alt"></i></a>

									<!--<a onClick="shuffleOnOff()" id="shuffle"><i class="fa fa-random"></i></a>-->
									
									</span>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div><!-- End Player Body -->

			<br>

			<br>

	<?php	} ?>
					

                    <div class="like_comment_share">
                        <a href="#"><span><i class="fa fa-thumbs-up"></i>Like</span></a>

                        <a href="#" ><span><i class="fa fa-heart"></i>Love it</span></a>
                        <a href="#" class="add-comment-link add-comment-link" rel="<?php echo $entry['id'] ?>"><span>
						<i class="fa fa-commenting"></i>Comment</span></a>
                        <a href="#"><span><i class="fa fa-share-square-o"></i>Share</span></a>
                   
				   </div>

              	<?php 
					$profile_pic = 'uploads/'.$_SESSION['profile_pic'];
					
					if($_SESSION['profile_pic'] == ''){
						
						$profile_pic = "uploads/default/Maledefault.png";
	
					}
				?>
					   <div data="<?php echo $entry['id']; ?>"  id="post_comment_<?php echo $entry['id'] ?>">
                        
                        <?php
                        //echo "TOTALCOMMENT::".count($entry['comments']);
                        $setLimit = 2;
                        $entry['comments_count'] = 6;
                        if ($entry['comments_count'] > 5) {
                            echo "<div id='loadmore_" . $entry['id'] . "' class='loadmorecomment'><a onclick='loadMore(" . $entry['id'] . ",5," . $setLimit . ")'>Load More (" . $setLimit . ")</a><span class='loadspin loadno'><img src='images/loader.gif'></span></div>";
                        }
						
				
                        foreach ($entry['comments'] as $comment):
							
									$profile_pic='uploads/'.$comment['profile_pic'];
										if($comment['profile_pic']==''){
												$profile_pic="uploads/default/Maledefault.png";
												
										}
							
							
							?>
                            <div class="slider_two_title postcomment" id="comment_<?php echo  $comment['id'] ?>">
                                <img src="<?php echo $profile_pic; ?>"
                                     alt="<?php echo $comment['firstname'] ?> <?php echo $comment['lastname'] ?>"
                                     style="width:37px;height:37px;">
                                <div class="slider_title_style 1single-comment">
                                    <div class="comment_text">
                                        <a href="#"><span class="author_slide_top"><?php echo $comment['firstname'] ?> <?php echo $comment['lastname'] ?></span></a>
                                        <span class="comment_box_bottom_text"><?php if(!empty($comment['text'])){ echo $comment['text']; }  ?></span>
                                    </div>
                                    <a href="javascript:void(0)"><p class="like_and_reply"><span>Like</span>
									<span class="replytext" id="replybox<?php echo  $comment['id'] ?>">Reply</span> 
												
												<?php echo date('F j, Y, g:i a', strtotime($comment['date'])) ?>
                                        </p></a>

                                    <div id="allreply_<?php echo $comment['id'] ?>" class="allReply">
                                        <?php
										
                                        if (count($comment['replies'])):
                                            $allReplies =array_reverse( $comment['replies']);
                                            foreach ($allReplies as $replyKey => $replyValue):
									
		
										$profile_pic='uploads/'.$replyValue['profile_pic'];
										if($replyValue['profile_pic']==''){
												$profile_pic="uploads/default/Maledefault.png";
												
										}
							
		
		
		
                                                ?>
                                                <!-- <div class="line"></div> -->
                                              <div class="slider_two_title" id="reply_<?php echo $replyValue['id'] ?>">
											  
                                                    <img src="<?php echo $profile_pic; ?>"
                                                         alt="<?php echo $replyValue['firstname'] ?> <?php echo $replyValue['lastname'] ?>"
                                                         style="width:37px;height:37px;">
                                                    <div class="slider_title_style 1single-comment">
                                                        <div class="comment_text">
                                                            <a href="#"><span
                                                                    class="author_slide_top"><?php echo $replyValue['firstname'] ?> <?php echo $replyValue['lastname'] ?></span></a>
                                                            <span class="comment_box_bottom_text"><?php if(!empty($replyValue['text'])){ echo $replyValue['text']; }  ?>
                            
                                                            </span>
                                                        </div>
                                                        <a href="#"><p
                                                                class="like_and_reply reply"><?php echo date('F j, Y, g:i a', strtotime($replyValue['date'])) ?></p>
                                                        </a>
														
						

                                                    </div>

                                                </div>
                                                <!-- <div class="line"></div> -->
                                               

							

											   <?php
                                            endforeach;
                                        endif; ?>
										
									<?php	$profile_pic='uploads/'.$_SESSION['profile_pic'];
										if($_SESSION['profile_pic']==''){
												$profile_pic="uploads/default/Maledefault.png";
												
										}
										
                                        ?>
								 <div class="comment_box_reply comment_box replycomment" 
								 style="display:none;" id="reply<?php echo $comment['id'] ?>" style="display: block;">
											<img src="<?php echo $profile_pic; ?>" alt=""
												 style="width:37px;height:37px;">
											<span>
											
											<input id="replyid<?php echo $comment['id'] ?>" 
											rel="<?php echo $entry['id']; ?>"
														 class="comment_box_inline  comment_reply "
														 placeholder="Write your comment here" type="text">
														 
														 
														 </span>
											<div class="comment_box_icon comment_box_inline"><a href="#"><span><i
															class="comment_box_left fa fa-camera"></i></span></a>
												<a href="#"><span><i class="comment_box_left2 fa fa-smile-o"></i></span></a>
											</div>
											<h5 class="press_enter_post">Press enter to post : </h5>
                                    </div>
                                    </div>
                                    <!-- Reply Code -->
								
                                    <div class="comment_box_reply comment_box">
                                        <img src="<?php echo $profile_pic ?>" alt=""
                                             style="width:37px;height:37px;">
                                        <span><input id="commentid_<?php echo $entry['id'] ?>_<?php echo $comment['id'] ?>"
                                                     class="comment_box_inline  "
                                                     placeholder="Write your comment here" type="text"></span>
                                        <div class="comment_box_icon comment_box_inline"><a href="#"><span><i
                                                        class="comment_box_left fa fa-camera"></i></span></a>
                                            <a href="#"><span><i class="comment_box_left2 fa fa-smile-o"></i></span></a>
                                        </div>
                                        <h5 class="press_enter_post">Press enter to post : </h5>
                                    </div>
                                </div>

                            </div>

                        <?php endforeach ?>
						
                    <div class="comment_box " id="comment_box_<?php echo $entry['id'] ?>"  style="display: block;" rel="">

                        <form rel="<?php echo $entry['id'] ?>" id="commform_<?php echo $entry['id'] ?>" 
						action="" method="post" enctype="multipart/form-data" onsubmit="return false;">
                            <img src="<?php echo $profile_pic; ?>" alt="" style="width:37px;height:37px;">
                            <span><input id="postid_<?php echo $entry['id'] ?>" class="comment_box_inline comment_input" placeholder="Write your comment here" type="text"></span>
                            <div class="comment_box_icon comment_box_inline postbutton-comments" id="<?php echo $entry['id'] ?>">
							  <a href="javascript:void"><span><i class="comment_box_left fa fa-camera"></i></span></a>
                            </div>
                            <h5 class="press_enter_post">Press enter to post: </h5>
                        </form>
                    </div>
					
                    </div>
					
                </div>


		<?php
			echo "<span class='last_element' data=".$postids."></span>";
			$postids++;
			}
		
				unset($_SESSION['reach_end']);
				
			}else if ($_SESSION['reach_end'] != true){
				
				$_SESSION['reach_end'] = true;
		?>
    <center><h2>There is no more post</h2></center>
    
	<?php } ?>	
	
	<script type="text/javascript" src="js/rs_video.js"></script>
	
<?php }else{
	echo "There is no more post";
} ?>

	
	