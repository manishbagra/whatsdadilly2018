 <?php
error_reporting(-1);

require 'bootstrap.php';
require_once 'model/Wall.php';
require_once 'model/VideosModel.php';
require_once "model/Friends.php";
require_once 'classes/Session.class.php';

$session = new Session();

$video_model = new VideosModel();

$WallModel = new WallModel();

$imgname_array = array();

$messages = new Friends();


if (!empty($_POST)) {
    
    if (trim($_POST['imgN_array']) != "") {
        
        if ($_POST['profile_id'] == '') {
            
            $owner_id = $session->getSession('userid');
            
        } else {
            
            $owner_id = $_POST['profile_id'];
        }
        if ($session->getSession('userid') == $_POST['profile_id'] || $_POST['profile_id'] == '') {
            $post_type = 0;
        } else {
            $post_type = 1;
        }
        
        $imgname_array = explode('@DadillY@', $_POST['imgN_array']);
        
        $total_images = count($imgname_array);
        
        $imgTname_array = explode('@DadillY@', $_POST['imgTN_array']);
        
        $imgSrc_array = explode('@DadillY@', $_POST['vSrc_array']);
        
        $title_array = explode('@DadillY@', $_POST['title_array']);
        
        $descrip_array = explode('@DadillY@', $_POST['descrip_array']);
        
        $type_array = explode('@DadillY@', $_POST['type_array']);
        
        $tags_array = explode('@DadillY@', $_POST['tags_array']);
        
        $category_array = explode('@DadillY@', $_POST['category_array']);
        
        $vDesc_array = explode('@DadillY@', $_POST['vDesc_array']);
        
        
        $userid = $session->getSession('userid');
        
        $output_dir = __DIR__ . "/uploads/videos/" . $userid;
        
        $dir = __DIR__ . "/uploads/video/videomp4/";
        
        if (!file_exists($output_dir)) {
            
            mkdir($output_dir, 0777, true);
            
            mkdir($output_dir . "/thumbnails", 0777, true);
            
        }
        
        
        $thumbind = "";
        
        $wall_id = "";
        
        for ($i = 0; $i < $total_images; $i++) {
            
            $expl_thumb = explode("/", $imgSrc_array[$i]);
            
            $thumbind = "uploads/videos/" . $userid . "/thumbnails/" . $expl_thumb[4];
            
            $currentDT = date("Y-m-d h:m:s");
            
            $params = array(
                'user_id' => $userid,
                'file' => "uploads/videos/" . $userid . "/" . $imgname_array[$i],
                'title' => addslashes($title_array[$i]),
                'date' => $currentDT,
                'tags' => addslashes($tags_array[$i]),
                'cat' => addslashes($category_array[$i]),
                'description' => addslashes($descrip_array[$i]),
                'privacy' => $type_array[$i],
                'thumbnail' => $thumbind
            );
            
            
            if ($i === 0) {
                
                $lastId = $WallModel->addVideosTitle($userid, $owner_id, $post_type, '', $currentDT, $entityManager);
                
                $wall_id = $lastId;
            }
            
            $result = $video_model->addVideoDetails($wall_id, $params, $entityManager);
            
            if (@copy($dir . $imgname_array[$i], $output_dir . "/" . $imgname_array[$i]) && @copy($dir . "thumbnails/" . $expl_thumb[4], $output_dir . "/thumbnails/" . $expl_thumb[4])) {
                
                if (unlink($dir . $imgname_array[$i]) && unlink($imgSrc_array[$i])) {
                    continue;
                } else {
                    echo "Del Failed";
                    exit;
                }
            }
            
            
        }
        $thumbind = "uploads/videos/" . $userid . "/thumbnails/" . $expl_thumb[4];
        
        $get_user_info = $messages->getUserInfoById($entityManager, $userid);
        
        $fullname = $get_user_info['firstname'] . ' ' . $get_user_info['lastname'];
        
        $frnds_info     = $messages->getFriendsEmail($entityManager, $userid);
        $newfriendarray = array();
        foreach ($frnds_info as $key => $friendss) {
            if ($friendss['user_id'] != $owner_id) {
                $newfriendarray[] = $friendss;
                
            }
            
        }
        $frnds_info = $newfriendarray;
        
        
        $get_user_profile = $messages->getUserInfoByOwnerId($entityManager, $owner_id);
        
        
        
        //$get_user_profile_id = $get_user_profile ['user_id'];
        if ($owner_id != $userid) {
            $frnds_info = array_merge($frnds_info, $get_user_profile);
        }
        $noti_file = $thumbind;
        $noti_type = 9;
        $noti_read = 0;
        
        $noti_id_friend = $userid;
        
        $get_user_profile1 = $messages->getUserInfoByOwnerId($entityManager, $owner_id);
        
        $profileownerfullname = $get_user_profile1[0]['firstname'] . ' ' . $get_user_profile1[0]['lastname'];
        
        
        
        foreach ($frnds_info as $key => $value) {
            
            $noti_id_user = $value['user_id'];
            if ($owner_id == $userid) {
                $msg = addslashes("<span>" . $fullname . "</span> publish new photo . You can comment it and share.");
            } else if ($value['user_id'] == $owner_id) {
                $msg = addslashes("<span>" . $fullname . "</span> posted a video on your Whiteboard");
            } else {
                $msg = addslashes("<span>" . $fullname . "</span> posted a video on " . $profileownerfullname . " Whiteboard");
            }
            
            $sql = "INSERT INTO `notifications` (`id_user`,`id_friend`,`type`,`message`,`thumb`,`readed`) values ('" . $noti_id_user . "','" . $noti_id_friend . "','" . $noti_type . "','" . $msg . "','" . $noti_file . "','" . $noti_read . "')";
            
            $notifications->addNotificationNEW($entityManager, $sql);
            
            
        }
        
        
        
        
        // echo'<pre>';print_r($userid);
        // echo'<pre>';print_r($owner_id);die;
        // $get_user_profile = $messages->getUserInfoByOwnerId($entityManager,$owner_id);
        // $profileownerfullname = $get_user_profile[0]['firstname'].' '.$get_user_profile[0]['lastname'];
        // $friends = Friends::getFriendsList($entityManager, $session->getSession('userid'), 5000);
        // if($userid == $ownerid)
        // {
        // $msg = addslashes("<span>".$session->getSession('firstname')." ".$session->getSession('lastname'). " </span>publish new video . You can comment it and share. ");
        // }else{
        // $msg = addslashes("<span>".$session->getSession('firstname')." ".$session->getSession('lastname'). " </span>publish new video . You can comment it and share. ");
        // }
        // foreach ($friends as $friend) {
        // $params = array(
        // 'id_friend' => $session->getSession('userid'),
        // 'id_user'   => $friend['user_id'],
        // 'message'   => ,
        // 'thumb'    =>  $thumbind,
        // 'type' => '2',
        // 'read' => 0
        // );
        //echo'<pre>';print_r($params);
        // NotificationModel::addNotificationForAudio($entityManager, $params);
        
        
        // }    
        $message = array(
            "flag" => "1"
        );
        
        
        
    } else {
        $message = array(
            "flag" => 0
        );
    }
    
    // echo json_encode($message);exit();
    
?>
   
    <?php
    if (!empty($entry['videos'])) {
        
        $count = count($entry['videos']);
        
        switch ($count) {
            
            case $count === 1:
                
                $padding_bottom = '67.25%';
                
                break;
            
            case $count === 2:
                
                $padding_bottom = '36.25%';
                
                break;
            
            case $count === 3:
                
                $padding_bottom = '35.25%';
                
                break;
            
            case $count >= 4 && $count <= 6:
                
                $padding_bottom = '69.25%';
                
                break;
            
            case $count >= 7 && $count <= 9:
                
                $padding_bottom = '103.25%';
                
                break;
                
        }
        
        
        
?>
       
        <div style="padding:10px;font-size:14px;color:#333;">
            
            <div style="padding-bottom: <?php
        echo $padding_bottom;
?>" class="card mb-3 single-post">
            
            <?php
        
        if (count($entry['videos']) <= 9) {
            
            $lin = floor(count($entry['videos']) / 3);
            
            $rest = count($entry['videos']) % 3;
            
            $tamv1w = '32.5%';
            $tamv1h = '150px';
            
            $tamv2w = '49.2%';
            $tamv2h = '200px';
            
            $tamv3w = '99.1%';
            $tamv3h = '350px';
            
        }
        
        $i = 1;
        
        if (count($entry['videos']) > 1) {
?>


    <div class="elementor-widget-container">

        <div id="<?php //echo  $entry['id']; 
?>" class="gg_gallery_wrap gg_columnized_gallery gid_5501 ggom_103   ggom_vc_txt" data-gg_ol="103" data-col-maxw="200" rel="5501" data-nores-txt="No images found in this page">                

        <div class="gg_loader" style="display: none;">

        <div class="ggl_1">

        </div>

        <div class="ggl_2">
        </div>

        <div class="ggl_3">
        </div>

        <div class="ggl_4">

        </div>

        </div>            

        <!-- Start Vieo light box plugin -->

        <div class="gg_container">
                
                <?php
            foreach ($entry['videos'] as $key => $videoShow) {
                
                
                
                if ($videoShow['thumbnail']) {
                    
                    $poster = $videoShow['thumbnail'];
                    
                    $video_thumb = "poster='$poster'";
                    
                } else {
                    
                    $video_thumb = "preload";
                    
                }
                
                
?>

            <div data-gg-link="<?php
                echo URL . $videoShow["file"];
?>"

                        class="gg_img 5ab884825d316-2 gg_shown" 

                        data-gg-url="<?php
                echo URL . $poster;
?>" 

                        data-gg-title="<?php
                echo $videoShow["title"];
?>" 

                        data-gg-author="<?php
                echo URL . $poster;
?>" 

                        data-gg-descr="<?php
                echo $videoShow["title"];
?>" 

                        data-img-id="<?php
                echo $videoShow["id"];
?>" 

                        rel="<?php
                echo $videoShow["id"];
?>" 

                        style="width: 31.5%" >


    <div class="gg_img_inner" style="padding-bottom: 100%">

    <!-- Start video Area -->
        
    <video style="width:100%;height:171px;" id="video_<?php
                echo $videoShow['id'];
?>"  class="video-js"  <?php
                echo $video_thumb;
?> data-setup='{"playbackRates": []}' >

        <source src="<?php //echo $videoShow['file'] 
?>"  type="video/mp4"></source>

    </video>

    </div>
                
            </div>    
                
                <?php
            }
?>
       </div>    

    </div>
    
    </div>

        

        <?php
        } else {
            
            //$poster = 'uploads/videos/thumbnails/'.$entry['videos'][0] .'/thumb_1.jpg';
            
            if ($entry['videos'][0]['thumbnail']) {
                
                $poster = $entry['videos'][0]['thumbnail'];
                
                $video_thumb = "poster='$poster'";
                
            } else {
                $video_thumb = "preload";
            }
            
            echo '<style>
                .single-post #video_' . $entry['videos'][0]['id'] . ' {
                position: relative;
                float:left;
                margin:2px;
                top: 0;
                left: 0;
                width: 99.1%;
                height: 350px;
                }
                </style>';
            
?>    

            <!-- Start video Area -->
                    
                <video id="video_<?php
            echo $entry['videos'][0]['id'];
?>" data-video="<?php
            echo $entry['videos'][0]['file'];
?>" controls class="video-js vjs-default-skin rs_video_player vjs-big-play-centered" <?php
            echo $video_thumb;
?> data-setup='{"playbackRates": []}' >

                    <source src="<?php
            echo $entry['videos'][0]['file'];
?>"  type="video/mp4"></source>

                </video>

        <?php
        }
?>        
                
            </div>
            
        

        <?php
        
        
        if (count($entry['videos']) === 1) {
            
            echo $entry['videos'][0]['description'];
            
            if (trim($entry['videos'][0]['tags']) != '') {
                
                echo '<br> Tags: ' . $entry['videos'][0]['tags'] . ',' . $entry['videos'][0]['tags_more'];
                
                echo "<br><br>";
                
            }
            
            
        }
?>
           
            </div>


    <?php
    }
?>
   
<?php
} else {
    echo "Failed";
    exit;
}
?> 