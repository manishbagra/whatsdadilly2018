// scroll to top on page's opening to avoid issues with side menu placement
window.scrollTo(0,0);


jQuery(document).ready(function() {
	var baseurl = PCAD_PAGEURL;	
	var sect_id = ""; 
	
	var subsect_after_ajax = false; // var used to auto select a subsection after ajax load
	var popping_state = false; // flag to knowe wheter script is reading an url popchange and then no other URL changes must be performed
	var track_subsect_sel_on_scroll = false; // flag to know whether to change selected subsection on scrolling 
		
		
	// selected section toggle
	jQuery(document).on("click", "#pcad_sidebar_inner > ul > li:not(.pcad_sel_sect)", function() {
		var $old_sel = jQuery(".pcad_sel_sect");
		var $new_sel = jQuery(this);

		// move to top
		jQuery("html, body").animate({"scrollTop": 0}, 500);

		// browser history
		var name = jQuery(this).find("span").text();
		sect_id = jQuery(this).attr("rel");
		
		if(!popping_state) {
			history.pushState(false, "PrivateContent - Dev Documentation", baseurl +"?section="+ sect_id );
		}
		
		// ajax call for new contents
		jQuery("#pcad_txt_wrap").html("<div id='pcad_main_loader'></div>");
		
		var data = {pcad_sect : sect_id};
		jQuery.post(baseurl, data, function(response) {
			jQuery("#pcad_txt_wrap").html(response);	
			pcad_code_hl();
			
			$old_sel.removeClass("pcad_sel_sect");
			$old_sel.find("ul").slideUp(450);
			$old_sel.find("ul li.pcad_sel_subs").removeClass("pcad_sel_subs");
			
			$new_sel.addClass("pcad_sel_sect");
			$new_sel.find("ul").slideDown(450);
			$new_sel.find("ul li").first().addClass("pcad_sel_subs");
			
			
			if(subsect_after_ajax && jQuery('.pcad_sel_sect li[rel="'+ subsect_after_ajax +'"]').length) {
				jQuery('.pcad_sel_sect li[rel="'+ subsect_after_ajax +'"]').trigger('click');
				subsect_after_ajax = false;	
			}
			else {
				// scroll to init
				jQuery("html, body").animate({"scrollTop": jQuery("#pcad_txt_wrap > h2:first-child").offset().top - 25}, 500);
			}
			
			popping_state = false;
			
			setTimeout(function() {
				track_subsect_sel_on_scroll = true;
			}, 1000); // equal to animation timing 
		});
	});
	
	
	
	// select subsection - scroll to that
	jQuery(document).on("click", ".pcad_sel_sect .pcad_subs li", function(e) {
		e.preventDefault();
		var sel_subs = jQuery(this).attr("rel");
		
		if( jQuery("#"+sel_subs).size() ) {
			
			jQuery(".pcad_sel_subs").removeClass("pcad_sel_subs");
			jQuery(this).addClass("pcad_sel_subs");
			
			if(!popping_state) {
				history.pushState(false, "PrivateContent - Dev Documentation", "#" + sel_subs);
			}
			jQuery("html, body").animate({"scrollTop": jQuery("#"+sel_subs).offset().top - 30}, 500);
			
			popping_state = false;
		}
	});
	
	
	
	// subsection scroll on page's opening
	if(window.location.href.indexOf('#') !== -1) {
		var curr_url_arr = window.location.href.split('#');
		
		setTimeout(function() {
			if(jQuery('.pcad_sel_sect li[rel="'+ curr_url_arr[1] +'"]').length) {
				jQuery('.pcad_sel_sect li[rel="'+ curr_url_arr[1] +'"]').trigger('click');	
			}
			
			setTimeout(function() {
				track_subsect_sel_on_scroll = true;
			}, 1000); // equal to animation timing 
		}, 100);
	}
	else {
		track_subsect_sel_on_scroll = true;	
	}
	
	
	window.onpopstate = function(e) {
		var curr_url = window.location.href;
		
		if(curr_url.indexOf('?section') == -1) {
			return false;	
		}
		track_subsect_sel_on_scroll = false;
		
		var arr = curr_url.split('?section=');
		var raw_sect = arr[1];


		// is there any subsection?
		if(raw_sect.indexOf('#') !== -1 && raw_sect.substr(raw_sect.length - 1) != '#') {
			var raw_subsect = raw_sect.split('#'); 
			var subsect = raw_subsect[1];	
		}
		else {
			var subsect = false;	
		}
		
		
		// section retrieval
		if(raw_sect.indexOf('#') !== -1) {
			raw_sect = raw_sect.split('#'); 	
			var sect = raw_sect[0];
		}
		else {
			var sect = raw_sect;
		}
		
		
		// is the same section? just switch subsection
		if(jQuery('.pcad_sect[rel="'+ sect +'"]').length && jQuery('.pcad_sect[rel="'+ sect +'"]').hasClass('pcad_sel_sect')) {
			if(subsect) {
				if(jQuery('.pcad_sel_sect li[rel="'+ subsect +'"]').length) {
					popping_state = true;
					jQuery('.pcad_sel_sect li[rel="'+ subsect +'"]').trigger('click');	
				}	
			}
			
			// no subsect? move to first
			else {
				if(jQuery('.pcad_sel_sect li').length > 1 && !jQuery('.pcad_sel_sect li').first().hasClass('pcad_sel_subs')) {
					popping_state = true;
					jQuery('.pcad_sel_sect li').first().trigger('click');		
				}
			}
			
			setTimeout(function() {
				track_subsect_sel_on_scroll = true;
			}, 1000); // equal to animation timing 
		}
		
		// otherwise use ajax
		else {
		
			if(jQuery('.pcad_sect[rel="'+ sect +'"]').length) {
				popping_state = true;
				
				subsect_after_ajax = subsect;
				jQuery('.pcad_sect[rel="'+ sect +'"]').trigger('click');	
			}
		}
	}
	
	
	
	// snippet-ize
	var pcad_code_hl = function() {
		jQuery("#pcad_txt_wrap pre").each(function() {
			var contents = jQuery(this).html().replace(/>/g, "&gt;").replace(/</g, "&lt;");
			jQuery(this).html( contents);
		});

		jQuery("#pcad_txt_wrap pre").snippet("php", {style:"lcweb", clipboard: PCAD_URL +"/js/jquery.snippet/ZeroClipboard.swf"});	
	}
	pcad_code_hl();
	
	
	
	// function to set active subsection on scroll
	var select_subsect_on_scroll = function() {
		if(!track_subsect_sel_on_scroll || jQuery('.pcad_sel_sect li').length < 2) {
			return false;	
		}
		
		if(typeof(ssos_timeout) != 'undefined') {clearTimeout(ssos_timeout);}
		
		ssos_timeout = setTimeout(function() {
			var st = jQuery(window).scrollTop(); 
			
			
			jQuery('#pcad_txt_wrap article').each(function() {	
				var el_top_pos = jQuery(this).position().top;
				if(st > el_top_pos && st < (el_top_pos + jQuery(this).height() + 20)) {
					
					
					var subsect = jQuery(this).find('> h5').attr('id');
					
					if(!jQuery('.pcad_sel_sect li[rel="'+ subsect +'"]').hasClass('pcad_sel_subs')) {
						
						jQuery('.pcad_sel_sect li').removeClass('pcad_sel_subs');
						jQuery('.pcad_sel_sect li[rel="'+ subsect +'"]').addClass('pcad_sel_subs');
						
						history.replaceState(false, "PrivateContent - Dev Documentation", "#" + subsect);
						return false;
					}
				}
			});
		}, 100);
	};
	
	
	// keep nav visible
	jQuery(window).scroll(function() {
		// calculate header's height
		var head_height = jQuery('header.masthead').outerHeight(true) + jQuery('header.elementor-element').outerHeight();
		
		var st = jQuery(window).scrollTop() - head_height;
		var $wrap = jQuery('#pcad_sidebar');
		var $subj = jQuery('#pcad_sidebar > div');
		var additional_margin = 0;
		
		if(st > 0) {
			//var offset = $wrap.offset();
	
			//if(st < (offset.top + ($wrap.height() - ($subj.height() + additional_margin) )) ) {
				//var margin_top = Math.floor( st + additional_margin); 
				//$subj.css('margin-top', margin_top);
				$wrap.addClass('pcad_knv');
			//}
		}
		else {
			//$subj.css('margin-top', 0);
			$wrap.removeClass('pcad_knv');
		}
		
		select_subsect_on_scroll();
	});
	
	// trigger scroll to avoid issues with deeplinking
	jQuery(window).trigger('scroll');
});