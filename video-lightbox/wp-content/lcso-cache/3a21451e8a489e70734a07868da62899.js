jQuery(document).ready(function() {
	var pcpp_is_acting = false;
	
	// create renew order and redirect (no plan change)
	jQuery(document.body).delegate('a.pcpp_renew_plan_btn', 'click', function() {
		if(pcpp_is_acting) {return false;}
		pcpp_is_acting = true;
		
		var $subj = jQuery(this);
		$subj.html(pcpp_renew_is_acting);
		
		jQuery.ajax({
			type: "POST",
			url: window.location.href,
			dataType: "json",
			data: "type=pcpp_renew_order&uid=" + parseInt($subj.attr('rel')),
			success: function(response){
				if(response.status == 'success') {
					$subj.html(pcpp_renew_ok);
					window.location.href = response.url;
				} 
				else {
					$subj.html(response.error);	
				}

				pcpp_is_acting = false;
			}
		});
	});
	
	
	// create renew order and redirect (with plan change)
	jQuery(document.body).delegate('.pcpp_change_plan_btn', 'click', function() {
		if(pcpp_is_acting) {return false;}
		pcpp_is_acting = true;
		
		var $form = jQuery(this).parents('form');
		var $subj = jQuery(this);
		
		$subj.addClass('pc_loading_btn');
		$form.find('.pcpp_form_message').empty();
		
		var data = {
			type	: 'pcpp_renew_order', 
			uid 	: $form.find('input[name=pcpp_uid]').val(),
			pid		: $form.find('select').val(),
			coupon	: ($form.find('.pcpp_hidden_coupon_field').length) ? $form.find('.pcpp_hidden_coupon_field').val() : ''	
		};
		
		jQuery.ajax({
			type: "POST",
			url: window.location.href,
			dataType: "json",
			data: data,
			success: function(response){
				if(response.status == 'success') {
					$form.find('.pcpp_form_message').html('<span class="pc_success_mess">' + pcpp_renew_ok + '</span>');
					window.location.href = response.url;
				} 
				else {
					$form.find('.pcpp_form_message').html('<span class="pc_error_mess">' + response.error + '</span>');	
				}

				pcpp_is_acting = false;
			}
		});
	});
	
	
	//////////////////////////////////////////////////////////
	
	
	// manage plan switch
	jQuery(document.body).delegate('.pcpp_plan_ul > li:not(.pcpp_chosen)', 'click', function() {
		var $subj = jQuery(this).parents('form');
		var new_val = jQuery(this).attr('rel');
		
		// deactivate old one
		$subj.find('.pcpp_plan_ul li.pcpp_chosen').removeClass('pcpp_chosen');
		jQuery(this).addClass('pcpp_chosen');
		
		// set dropdown
		$subj.find('.pcpp_plan_dd option').removeAttr('selected');
		$subj.find('.pcpp_plan_dd option[value='+ new_val +']').attr('selected', 'selected');
		
		// remove expanded mode in case
		if($subj.find('.pcpp_dd_expanded').size()) {
			$subj.find('#pcpp_plan_dd_label i').trigger('click');	
		}
	});
	
	
	//  dropdown simulation for 1-col
	jQuery(document.body).delegate('#pcpp_plan_dd_label i', 'click', function() {
		var $subj = jQuery(this).parents('.pcpp_plan_choice_wrap');
		
		if($subj.hasClass('pcpp_dd_expanded')) {
			$subj.removeClass('pcpp_dd_expanded');
		}
		else {
			$subj.addClass('pcpp_dd_expanded');	
		}
	});
	
	
	//////////////////////////////////////////////////////////
	
	
	// coupon check
	var submit_coupon = function($input) {
		if(!$input.val() || pcpp_is_acting) {return false;}
		
		pcpp_is_acting = true;
		$input.parents('.pcpp_coupon_wrap').find('.pc_field_container > *').fadeTo(200, 0.7);
		
		var data = {
			type		: 'pcpp_coupon_check', 
			pcpp_coupon : $input.val()	
		};
		jQuery.ajax({
			type: "POST",
			url: window.location.href,
			dataType: "json",
			data: data,
			success: function(response){		
				if(response.status == 'success') {
					var $plans_wrap = $input.parents('form').find('.pcpp_plan_ul');
					
					jQuery.each(response.prices, function(plan_id, new_price) {
						var $price_tag = $plans_wrap.find('li[rel='+ plan_id +'] .pcpp_plan_price');
						
						var old_price = $price_tag.html();
						$price_tag.html('<mark class="pcpp_old_price">'+ old_price +'</mark>'+ new_price); 
					});
					
					var $clone = $input.clone();
					$clone.insertBefore($input);
					
					$clone.attr('disabled', 'disabled');
					$input.addClass('pcpp_hidden_coupon_field').attr('type', 'hidden');
					$input.next('.pcpp_try_coupon').addClass('pcpp_remove_coupon');
				} 
				else {
					$input.parents('section').prepend('<div class="pc_field_error">'+ response.mess +'</div>');		
				}
				pcpp_is_acting = false;
			}
		});
	};
	
	
	// submit coupon clicking icon
	jQuery(document).delegate('.pcpp_try_coupon:not(.pcpp_remove_coupon)', 'click', function() {
		submit_coupon(jQuery(this).parents('.pcpp_coupon_wrap').find('input'));
	});
	
	// submit coupon with enter key
	setTimeout(function() {
		jQuery('input[name="pcpp_coupon"]').unbind("keypress").keypress(function(event){
			event.cancelBubble = true;
			if(event.stopPropagation) event.stopPropagation();
			
			if(event.keyCode === 13){ // enter key
				event.preventDefault();
				submit_coupon(jQuery(this));
			}
		});
	}, 100);
	
	
	// remove coupon
	jQuery(document).delegate('.pcpp_remove_coupon', 'click', function() {
		jQuery(this).removeClass('pcpp_remove_coupon');
		jQuery('.pcpp_hidden_coupon_field').remove();
		
		// reset field
		jQuery(this).prev('.pcpp_coupon').removeAttr('disabled').val('');
		
		// reset prices
		var $plans_wrap = jQuery(this).parents('form').find('.pcpp_plan_ul');
		$plans_wrap.find('li .pcpp_plan_price').each(function() {
           if(jQuery(this).find('mark').length) {
				jQuery(this).html( jQuery(this).find('mark').html() );   
		   }
        });
	});
	
	
});(function(b){lcl_objs=[];lcl_is_active=lcl_shown=!1;lcl_slideshow=void 0;lcl_on_mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent);
lcl_hashless_url=lcl_deeplink_tracked=lcl_curr_vars=lcl_curr_opts=lcl_curr_obj=!1;lcl_url_hash="";lc_lightbox=function(k,G){if("string"!=typeof k&&("object"!=typeof k||!k.length))return!1;var v=!1;b.each(lcl_objs,function(b,h){if(JSON.stringify(h)==JSON.stringify(k))return v=h,!1});if(!1===v){var w=new H(k,G);lcl_objs.push(w);return w}return v};lcl_destroy=function(k){k=b.inArray(k,lcl_objs);-1!==k&&lcl_objs.splice(k,1)};var H=function(k,G){var v=b.extend({gallery:!0,gallery_hook:"rel",live_elements:!0,
preload_all:!1,global_type:!1,deeplink:!1,img_zoom:!1,comments:!1,src_attr:"href",title_attr:"title",txt_attr:"data-lcl-txt",author_attr:"data-lcl-author",slideshow:!0,open_close_time:400,ol_time_diff:100,fading_time:80,animation_time:250,slideshow_time:6E3,autoplay:!1,counter:!1,progressbar:!0,carousel:!0,max_width:"93%",max_height:"93%",wrap_padding:!1,ol_opacity:.7,ol_color:"#111",ol_pattern:!1,border_w:0,border_col:"#ddd",padding:0,radius:0,shadow:!0,remove_scrollbar:!0,wrap_class:"",skin:"light",
data_position:"over",cmd_position:"inner",ins_close_pos:"normal",nav_btn_pos:"normal",txt_hidden:500,show_title:!0,show_descr:!0,show_author:!0,thumbs_nav:!0,tn_icons:!0,tn_hidden:500,thumbs_w:110,thumbs_h:110,thumb_attr:!1,thumbs_maker_url:!1,fullscreen:!1,fs_img_behavior:"fit",fs_only:500,browser_fs_mode:!0,socials:!1,fb_direct_share:!1,txt_toggle_cmd:!0,download:!1,touchswipe:!0,mousewheel:!0,modal:!1,rclick_prevent:!1,elems_parsed:function(){},html_is_ready:function(){},on_open:function(){},on_elem_switch:function(){},
slideshow_start:function(){},slideshow_end:function(){},on_fs_enter:function(){},on_fs_exit:function(){},on_close:function(){}},G),w={elems:[],is_arr_instance:"string"!=typeof k&&"undefined"==typeof k[0].childNodes?!0:!1,elems_count:"string"!=typeof k&&"undefined"==typeof k[0].childNodes?k.length:b(k).length,elems_selector:"string"==typeof k?k:!1,elem_index:!1,gallery_hook_val:!1,preload_all_used:!1,img_sizes_cache:[],inner_cmd_w:!1,txt_exists:!1,txt_und_sizes:!1,force_fullscreen:!1,html_style:"",
body_style:""};"string"==typeof k&&(k=b(k));var l=b.data(k,"lcl_settings",v),h=b.data(k,"lcl_vars",w),A=function(b){if("string"!=typeof b)return b;for(var c=0,f=0,e=b.toString().length;f<e;)c=(c<<5)-c+b.charCodeAt(f++)<<0;return 0>c?-1*c:c},H=function(c){var d=!1;b.each(h.elems,function(b,e){if(e.hash==c)return d=e,!1});return d},D=function(c){if(!c)return c;c=c.replace(/&lt;/g,"<").replace(/&gt;/g,">").replace(/&amp;/g,"&").replace(/&quot;/g,'"').replace(/&#039;/g,"'");return b.trim(c)},I=function(c,
d){var f=l[d];return-1!==f.indexOf("> ")?c.find(f.replace("> ","")).length?b.trim(c.find(f.replace("> ","")).html()):"":"undefined"!=typeof c.attr(f)?D(c.attr(f)):""},Z=function(c){var d=l,f=[];c.each(function(){var c=b(this),g=c.attr(d.src_attr),n=A(g);if(h.gallery_hook_val&&c.attr(d.gallery_hook)!=h.gallery_hook_val)return!0;n=H(n);if(!n){n=g;var k=c.data("lcl-type");"undefined"==typeof k&&(k=l.global_type);-1===b.inArray(k,"image video youtube vimeo dailymotion html iframe".split(" "))&&(n=n.toLowerCase(),
k=/^https?:\/\/(?:[a-z\-]+\.)+[a-z]{2,6}(?:\/[^\/#?]+)+\.(?:jpe?g|gif|png)$/.test(n)?"image":"#"==n.substr(0,1)&&b(n).length?"html":"unknown");"unknown"!=k?(n={src:g,type:k,hash:d.deeplink?A(g):!1,title:d.show_title?I(c,"title_attr"):"",txt:d.show_descr?I(c,"txt_attr"):"",author:d.show_author?I(c,"author_attr"):"",thumb:d.thumb_attr&&"undefined"!=typeof d.thumb_attr?c.attr(d.thumb_attr):"",width:"image"!=k&&"undefined"!=typeof c.data("lcl-w")?c.data("lcl-w"):!1,height:"image"!=k&&"undefined"!=typeof c.data("lcl-h")?
c.data("lcl-h"):!1,force_over_data:"undefined"!=typeof c.data("lcl-force-over-data")?parseInt(c.data("lcl-force-over-data"),10):"",force_outer_cmd:"undefined"!=typeof c.data("lcl-outer-cmd")?c.data("lcl-outer-cmd"):"",canonical_url:"undefined"!=typeof c.data("lcl-canonical-url")?c.data("lcl-canonical-url"):""},n.download="image"==k?"undefined"!=typeof c.data("lcl-path")?c.data("lcl-path"):g:!1):n={src:g,type:k,hash:d.deeplink?A(g):!1}}f.push(n)});2>f.length&&b(".lcl_prev, .lcl_next, #lcl_thumb_nav").remove();
if(!f.length)return!1;h.elems=f;return!0},O=function(){if(2>h.elems.length||!l.gallery)return!1;0<h.elem_index&&y(!1,h.elem_index-1);h.elem_index!=h.elems.length-1&&y(!1,h.elem_index+1)},y=function(c,d,f){var e=h;"undefined"==typeof d&&(d=e.elem_index);if("undefined"==typeof d)return!1;var g="image"==e.elems[d].type||-1!==b.inArray(e.elems[d].type,["video","youtube","vimeo","dailymotion"])&&e.elems[d].poster?"image"==e.elems[d].type?e.elems[d].src:e.elems[d].poster:"";g&&"undefined"==typeof e.img_sizes_cache[g]?
b("<img/>").bind("load",function(){e.img_sizes_cache[g]={w:this.width,h:this.height};c&&d==e.elem_index&&P()}).attr("src",g):((c||"undefined"!=typeof f)&&b("#lcl_loader").addClass("no_loader"),c&&P())},aa=function(){var c=b("#lcl_wrap[lc-lelem="+h.elem_index+"] #lcl_elem_wrap > iframe");if(!c.length)return!1;b("#lcl_wrap").addClass("lcl_loading_iframe");"undefined"!=typeof lcl_slideshow&&clearInterval(lcl_slideshow);c.on("load",function(){b("#lcl_wrap").removeClass("lcl_loading_iframe");setTimeout(function(){b("iframe.lcl_elem").length&&
b("iframe.lcl_elem")[0].contentWindow.focus()},20);b(".lcl_is_playing").length&&lcl_start_slideshow(!0)})},Q=function(c,d){var f=b.data(c,"lcl_settings"),e=b.data(c,"lcl_vars");if(e.is_arr_instance){var g=[];b.each(c,function(c,d){var e={},h="undefined"==typeof d.type&&f.global_type?f.global_type:!1;"undefined"!=typeof d.type&&(h=d.type);h&&-1!==b.inArray(h,"image video youtube vimeo dailymotion html iframe".split(" "))?"undefined"!=typeof d.src&&d.src&&(e.src=d.src,e.type=h,e.hash=A(d.src),e.title=
"undefined"==typeof d.title?"":D(d.title),e.txt="undefined"==typeof d.txt?"":D(d.txt),e.author="undefined"==typeof d.author?"":D(d.author),e.width="undefined"==typeof d.width?!1:d.width,e.height="undefined"==typeof d.height?!1:d.height,e.force_over_data="undefined"==typeof d.force_over_data?!1:parseInt(d.force_over_data,10),e.force_outer_cmd="undefined"==typeof d.force_outer_cmd?!1:d.force_outer_cmd,e.canonical_url="undefined"==typeof d.canonical_url?!1:d.canonical_url,e.thumb="undefined"==typeof d.thumb?
!1:d.thumb,e.download="image"==h?"undefined"!=typeof d.download?d.download:d.src:!1,e.poster="image"==h||"undefined"==typeof d.poster?"":d.poster,g.push(e)):(e={src:e.src,type:"unknown",hash:f.deeplink?A(e.src):!1},g.push(e))});e.elems=g}else{var n=c;f.live_elements&&e.elems_selector&&(n=d&&f.gallery&&f.gallery_hook&&"undefined"!=typeof b(k[0]).attr(f.gallery_hook)?e.elems_selector+"["+f.gallery_hook+"="+d.attr(f.gallery_hook)+"]":e.elems_selector,n=b(n));if(!Z(n))return(!f.live_elements||f.live_elements&&
!e.elems_selector)&&console.error("LC Lightbox - no valid elements found"),!1}f.preload_all&&!e.preload_all_used&&(e.preload_all_used=!0,b(document).ready(function(c){b.each(e.elems,function(b,c){y(!1,b)})}));"function"==typeof f.elems_parsed&&f.elems_parsed.call({opts:l,vars:h});e.is_arr_instance||(n=e.elems_selector?b(e.elems_selector):c,n.first().trigger("lcl_elems_parsed",[e.elems]));return!0};Q(k);var K=function(c,d){if(lcl_shown||lcl_is_active)return!1;lcl_is_active=lcl_shown=!0;lcl_curr_obj=
c;l=b.data(c,"lcl_settings");h=b.data(c,"lcl_vars");lcl_curr_opts=l;lcl_curr_vars=h;var f=l,e=h,g="undefined"!=typeof d?d:!1;if(!h)return console.error("LC Lightbox - cannot open. Object not initialized"),!1;e.gallery_hook_val=g&&f.gallery&&f.gallery_hook&&"undefined"!=typeof g.attr(f.gallery_hook)?g.attr(f.gallery_hook):!1;if(!Q(c,d))return!1;if(g)b.each(e.elems,function(b,c){if(c.src==g.attr(f.src_attr))return e.elem_index=b,!1});else if(parseInt(e.elem_index,10)>=e.elems_count)return console.error("LC Lightbox - selected index does not exist"),
!1;y(!1);ba();ca();e.force_fullscreen&&J(!0,!0);b("#lcl_thumbs_nav").length&&da();y(!0);O()},R=function(){b("#lcl_wrap").removeClass("lcl_pre_show").addClass("lcl_shown");b("#lcl_loader").removeClass("lcl_loader_pre_first_el")},ba=function(){var c=l,d=h,f=[],e="";"number"==typeof document.documentMode&&(b("body").addClass("lcl_old_ie"),"outer"!=c.cmd_position&&(c.nav_btn_pos="normal"));b("#lcl_wrap").length&&b("#lcl_wrap").remove();b("body").append('<div id="lcl_wrap" class="lcl_pre_show lcl_pre_first_el lcl_first_sizing lcl_is_resizing"><div id="lcl_window"><div id="lcl_corner_close" title="close"></div><div id="lcl_loader" class="lcl_loader_pre_first_el"><span id="lcll_1"></span><span id="lcll_2"></span></div><div id="lcl_nav_cmd"><div class="lcl_icon lcl_prev" title="previous"></div><div class="lcl_icon lcl_play"></div><div class="lcl_icon lcl_next" title="next"></div><div class="lcl_icon lcl_counter"></div><div class="lcl_icon lcl_right_icon lcl_close" title="close"></div><div class="lcl_icon lcl_right_icon lcl_fullscreen" title="toggle fullscreen"></div><div class="lcl_icon lcl_right_icon lcl_txt_toggle" title="toggle text"></div><div class="lcl_icon lcl_right_icon lcl_download" title="download"></div><div class="lcl_icon lcl_right_icon lcl_thumbs_toggle" title="toggle thumbnails"></div><div class="lcl_icon lcl_right_icon lcl_socials" title="toggle socials"></div><div class="lcl_icon lcl_right_icon lcl_zoom_icon lcl_zoom_in" title="zoom in"></div><div class="lcl_icon lcl_right_icon lcl_zoom_icon lcl_zoom_out" title="zoom out"></div></div><div id="lcl_contents_wrap"><div id="lcl_subj"><div id="lcl_elem_wrap"></div></div><div id="lcl_txt"></div></div></div><div id="lcl_thumbs_nav"></div><div id="lcl_overlay"></div></div>');
b("#lcl_wrap").attr("data-lcl-max-w",c.max_width).attr("data-lcl-max-h",c.max_height);f.push("lcl_"+c.ins_close_pos+"_close lcl_nav_btn_"+c.nav_btn_pos+" lcl_"+c.ins_close_pos+"_close lcl_nav_btn_"+c.nav_btn_pos);(!0===c.tn_hidden||"number"==typeof c.tn_hidden&&(b(window).width()<c.tn_hidden||b(window).height()<c.tn_hidden))&&f.push("lcl_tn_hidden");(!0===c.txt_hidden||"number"==typeof c.txt_hidden&&(b(window).width()<c.txt_hidden||b(window).height()<c.txt_hidden))&&f.push("lcl_hidden_txt");c.carousel||
f.push("lcl_no_carousel");c.comments&&f.push("lcl_has_comments");lcl_on_mobile&&f.push("lcl_on_mobile");c.wrap_class&&f.push(c.wrap_class);f.push("lcl_"+c.cmd_position+"_cmd");if("inner"!=c.cmd_position){var g=b("#lcl_nav_cmd").detach();b("#lcl_wrap").prepend(g)}c.slideshow||b(".lcl_play").remove();c.txt_toggle_cmd||b(".lcl_txt_toggle").remove();c.socials||b(".lcl_socials").remove();c.download||b(".lcl_download").remove();c.img_zoom||b(".lcl_zoom_icon").remove();(!c.counter||2>d.elems.length||!c.gallery)&&
b(".lcl_counter").remove();d.force_fullscreen=!1;if(!c.fullscreen)b(".lcl_fullscreen").remove();else if(!0===c.fs_only||"number"==typeof c.fs_only&&(b(window).width()<c.fs_only||b(window).height()<c.fs_only))b(".lcl_fullscreen").remove(),h.force_fullscreen=!0;2>d.elems.length||!c.gallery?b(".lcl_prev, .lcl_play, .lcl_next").remove():"middle"==c.nav_btn_pos&&(e+=".lcl_prev, .lcl_next {margin: "+c.padding+"px;}");!c.thumbs_nav||2>h.elems.length||!c.gallery?b("#lcl_thumbs_nav, .lcl_thumbs_toggle").remove():
(b("#lcl_thumbs_nav").css("height",c.thumbs_h),g=b("#lcl_thumbs_nav").outerHeight(!0)-c.thumbs_h,e+="#lcl_window {margin-top: "+-1*(c.thumbs_h-g)+"px;}",e+=".lcl_tn_hidden.lcl_outer_cmd:not(.lcl_fullscreen_mode) #lcl_window {margin-bottom: "+-1*b(".lcl_close").outerHeight(!0)+"px;}");f.push("lcl_txt_"+c.data_position+" lcl_"+c.skin);e+="#lcl_overlay {background-color: "+c.thumbs_h+"px; opacity: "+c.ol_opacity+";}";c.ol_pattern&&b("#lcl_overlay").addClass("lcl_pattern_"+c.ol_pattern);c.modal&&b("#lcl_overlay").addClass("lcl_modal");
c.wrap_padding&&(e+="#lcl_wrap {padding: "+c.wrap_padding+";}");c.border_w&&(e+="#lcl_window {border: "+c.border_w+"px solid "+c.border_col+";}");c.padding&&(e+="#lcl_subj, #lcl_txt, #lcl_nav_cmd {margin: "+c.padding+"px;}");c.radius&&(e+="#lcl_window, #lcl_contents_wrap {border-radius: "+c.radius+"px;}");c.shadow&&(e+="#lcl_window {box-shadow: 0 4px 12px rgba(0, 0, 0, 0.4);}");"inner"==c.cmd_position&&"corner"==c.ins_close_pos&&(e+="#lcl_corner_close {top: "+-1*(c.border_w+Math.ceil(b("#lcl_corner_close").outerWidth()/
2))+"px;right: "+-1*(c.border_w+Math.ceil(b("#lcl_corner_close").outerHeight()/2))+";}",b("#lcl_nav_cmd > *:not(.lcl_close)").length||(e+="#lcl_wrap:not(.lcl_fullscreen_mode):not(.lcl_forced_outer_cmd) #lcl_nav_cmd {display: none;}"));b("#lcl_inline_style").length&&b("#lcl_inline_style").remove();b("head").append('<style type="text/css" id="lcl_inline_style">'+e+"#lcl_overlay {background-color: "+c.ol_color+";opacity: "+c.ol_opacity+";}#lcl_window, #lcl_txt, #lcl_subj {-webkit-transition-duration: "+
c.animation_time+"ms; transition-duration: "+c.animation_time+"ms;}#lcl_overlay {-webkit-transition-duration: "+c.open_close_time+"ms; transition-duration: "+c.open_close_time+"ms;}.lcl_first_sizing #lcl_window, .lcl_is_closing #lcl_window {-webkit-transition-duration: "+(c.open_close_time-c.ol_time_diff)+"ms; transition-duration: "+(c.open_close_time-c.ol_time_diff)+"ms;}.lcl_first_sizing #lcl_window {-webkit-transition-delay: "+c.ol_time_diff+"ms; transition-delay: "+c.ol_time_diff+"ms;}#lcl_loader, #lcl_contents_wrap, #lcl_corner_close {-webkit-transition-duration: "+
c.fading_time+"ms; transition-duration: "+c.fading_time+"ms;}.lcl_toggling_txt #lcl_subj {-webkit-transition-delay: "+(c.fading_time+200)+"ms !important;  transition-delay: "+(c.fading_time+200)+"ms !important;}.lcl_fullscreen_mode.lcl_txt_over:not(.lcl_tn_hidden) #lcl_txt, .lcl_fullscreen_mode.lcl_force_txt_over:not(.lcl_tn_hidden) #lcl_txt {max-height: calc(100% - 42px - "+c.thumbs_h+"px);}.lcl_fullscreen_mode.lcl_playing_video.lcl_txt_over:not(.lcl_tn_hidden) #lcl_txt,.lcl_fullscreen_mode.lcl_playing_video.lcl_force_txt_over:not(.lcl_tn_hidden) #lcl_txt {max-height: calc(100% - 42px - 45px - "+
c.thumbs_h+"px);}</style>");c.remove_scrollbar&&(h.html_style="undefined"!=typeof jQuery("html").attr("style")?jQuery("html").attr("style"):"",h.body_style="undefined"!=typeof jQuery("body").attr("style")?jQuery("body").attr("style"):"",e=b(window).width(),b("html").css("overflow","hidden"),b("html").css({"margin-right":b(window).width()-e,"touch-action":"none"}),b("body").css({overflow:"visible","touch-action":"none"}));e=h.elems[d.elem_index];"image"!=e.type||"image"==e.type&&"undefined"!=typeof d.img_sizes_cache[e.src]?
f.push("lcl_show_already_shaped"):R();b("#lcl_wrap").addClass(f.join(" "));"function"==typeof c.html_is_ready&&c.html_is_ready.call({opts:l,vars:h});h.is_arr_instance||(h.elems_selector?b(h.elems_selector):lcl_curr_obj).first().trigger("lcl_html_is_ready",[l,h])},ea=function(c){var d=b(c)[0],f=null;d.addEventListener("touchstart",function(b){1===b.targetTouches.length&&(f=b.targetTouches[0].clientY)},!1);d.addEventListener("touchmove",function(b){if(1===b.targetTouches.length){var c=b.targetTouches[0].clientY-
f;0===d.scrollTop&&0<c&&b.preventDefault();d.scrollHeight-d.scrollTop<=d.clientHeight&&0>c&&b.preventDefault()}},!1)},P=function(){if(!lcl_shown)return!1;var c=h,d=c.elems[c.elem_index];b("#lcl_wrap").attr("lc-lelem",c.elem_index);l.carousel||(b("#lcl_wrap").removeClass("lcl_first_elem lcl_last_elem"),c.elem_index?c.elem_index==c.elems.length-1&&b("#lcl_wrap").addClass("lcl_last_elem"):b("#lcl_wrap").addClass("lcl_first_elem"));b(document).trigger("lcl_before_populate_global",[d,c.elem_index]);var f=
h.elem_index;b("#lcl_elem_wrap").removeAttr("style").removeAttr("class").empty();b("#lcl_wrap").attr("lcl-type",d.type);b("#lcl_elem_wrap").addClass("lcl_"+d.type+"_elem");switch(d.type){case "image":b("#lcl_elem_wrap").css("background-image","url('"+d.src+"')");break;case "html":b("#lcl_elem_wrap").html('<div class="lcl_html_container">'+b(d.src).html()+"</div>");break;default:b("#lcl_elem_wrap").html('<div id="lcl_inline" class="lcl_elem"><br/>Error loading the resource .. </div>')}-1===b.inArray(d.type,
["image","video","unknown","html"])&&aa();if(lcl_curr_opts.download)if(d.download){b(".lcl_download").show();var e=d.download.split("/");e=e[e.length-1];b(".lcl_download").html('<a href="'+d.download+'" target="_blank" download="'+e+'"></a>')}else b(".lcl_download").hide();b(".lcl_counter").html(f+1+" / "+h.elems.length);L(d)&&"unknown"!=d.type?(b("#lcl_wrap").removeClass("lcl_no_txt"),b(".lcl_txt_toggle").show(),d.title&&b("#lcl_txt").append('<h3 id="lcl_title">'+d.title+"</h3>"),d.author&&b("#lcl_txt").append('<h5 id="lcl_author">by '+
d.author+"</h5>"),d.txt&&b("#lcl_txt").append('<section id="lcl_descr">'+d.txt+"</section>"),d.txt&&(d.title&&d.author?b("#lcl_txt h5").addClass("lcl_txt_border"):b("#lcl_txt h3").length?b("#lcl_txt h3").addClass("lcl_txt_border"):b("#lcl_txt h5").addClass("lcl_txt_border")),fa()):(b(".lcl_txt_toggle").hide(),b("#lcl_wrap").addClass("lcl_no_txt"));ea("#lcl_txt");c.is_arr_instance||(f=c.elems_selector?b(c.elems_selector):lcl_curr_obj,f.first().trigger("lcl_before_show",[d,c.elem_index]));b(document).trigger("lcl_before_show_global",
[d,c.elem_index]);b("#lcl_wrap").hasClass("lcl_pre_first_el")&&("function"==typeof l.on_open&&l.on_open.call({opts:l,vars:h}),c.is_arr_instance||(f=c.elems_selector?b(c.elems_selector):lcl_curr_obj,f.first().trigger("lcl_on_open",[d,c.elem_index])));x(d);b("#lcl_subj").removeClass("lcl_switching_el")},L=function(b){return b.title||b.txt||b.author?!0:!1},E=function(c,d,f){var e=0,g=b("#lcl_wrap"),h=b(window).width()-parseInt(g.css("padding-left"),10)-parseInt(g.css("padding-right"),10);g=b(window).height()-
parseInt(g.css("padding-top"),10)-parseInt(g.css("padding-bottom"),10);!isNaN(parseFloat(c))&&isFinite(c)?e=parseInt(c,10):-1!==c.toString().indexOf("%")?e=("w"==d?h:g)*(parseInt(c,10)/100):-1!==c.toString().indexOf("vw")?e=h*(parseInt(c,10)/100):-1!==c.toString().indexOf("vh")&&(e=g*(parseInt(c,10)/100));"undefined"==typeof f&&("w"==d&&e>h&&(e=h),"h"==d&&e>g&&(e=g));return e},x=function(c,d,f){var e=l,g=h;"undefined"==typeof d&&(d={});var k=b(".lcl_fullscreen_mode").length?!0:!1;f=k?0:2*parseInt(e.border_w,
10)+2*parseInt(e.padding,10);"undefined"!=typeof d.side_txt_checked||"undefined"!=typeof d.no_txt_under&&d.no_txt_under||b("#lcl_wrap").removeClass("lcl_force_txt_over");var B=b(".lcl_force_txt_over").length||b(".lcl_hidden_txt").length||-1===b.inArray(e.data_position,["rside","lside"])||!L(c)?0:b("#lcl_txt").outerWidth(),t=k||!b("#lcl_thumbs_nav").length||b(".lcl_tn_hidden").length?0:b("#lcl_thumbs_nav").outerHeight(!0)-parseInt(b("#lcl_wrap").css("padding-bottom"),10),q=!k&&b(".lcl_outer_cmd").length?
b(".lcl_close").outerHeight(!0)+parseInt(b("#lcl_nav_cmd").css("padding-top"),10)+parseInt(b("#lcl_nav_cmd").css("padding-bottom"),10):0;var p=f+B;t=f+t+q;var m=b("#lcl_wrap").attr("data-lcl-max-w");q=b("#lcl_wrap").attr("data-lcl-max-h");m=k?b(window).width():Math.floor(E(m,"w"))-p;q=k?b(window).height():Math.floor(E(q,"h"))-t;if("object"==typeof g.txt_und_sizes){p=g.txt_und_sizes.w;var r=g.txt_und_sizes.h;if("image"==c.type)var u=g.img_sizes_cache[c.src]}else switch(c.type){case "image":b("#lcl_elem_wrap").css("bottom",
0);if("undefined"==typeof g.img_sizes_cache[c.src])return!1;u=g.img_sizes_cache[c.src];u.w<=m?(p=u.w,r=u.h):(p=m,r=Math.floor(u.h/u.w*p));r>q&&(r=q,p=Math.floor(u.w/u.h*r));if(L(c)&&!b(".lcl_hidden_txt").length&&"under"==e.data_position&&"undefined"==typeof d.no_txt_under)return S(p,r,q),b(document).off("lcl_txt_und_calc").on("lcl_txt_und_calc",function(){if(g.txt_und_sizes)return"no_under"==g.txt_und_sizes&&(d.no_txt_under=!0),x(g.elems[g.elem_index],d)}),!1;b("#lcl_subj").css("maxHeight","none");
break;case "html":c.width&&(m=E(c.width,"w")-p);c.height&&(q=E(c.height,"h")-t);p=m;"under"==e.data_position&&(k?b("#lcl_wrap").addClass("lcl_force_txt_over"):b("#lcl_wrap").removeClass("lcl_force_txt_over"));t=k?"none":q;"under"!=e.data_position||k?(b("#lcl_contents_wrap").css("maxHeight","none"),b("#lcl_subj").css("maxHeight",t)):(b("#lcl_contents_wrap").css("maxHeight",t),b("#lcl_subj").css("maxHeight","none"));k||(b(".lcl_first_sizing").length?b("#lcl_window").css("height","auto"):(b(".lcl_html_elem").css("width",
p),r=b("#lcl_elem_wrap").outerHeight(!0),b(".lcl_html_elem").css("width","auto"),"under"==e.data_position&&!k&&b("#lcl_txt").length&&(b("#lcl_txt").css("width",p),r+=b("#lcl_txt").outerHeight(!0),b("#lcl_txt").css("width","auto")),r>q&&(r=q)));break;default:p=280,r=125}if(("rside"==e.data_position||"lside"==e.data_position)&&!b(".lcl_no_txt").length&&"undefined"==typeof d.side_txt_checked&&(u="image"==c.type?g.img_sizes_cache[c.src]:"",(t=c.force_over_data)||(t=400),("image"!=c.type||"image"==c.type&&
u.w>t&&u.h>t)&&!ha(c,t,p+f,r+f,B)))return d.side_txt_checked=!0,x(c,d);g.txt_und_sizes=!1;if("undefined"==typeof d.inner_cmd_checked&&("inner"==e.cmd_position||c.force_outer_cmd)&&ia(c,p))return d.inner_cmd_checked=!0,x(c,d);b("#lcl_wrap").removeClass("lcl_pre_first_el");b("#lcl_window").css({width:k?"100%":p+f+B,height:k?"100%":r+f});b(".lcl_show_already_shaped").length&&setTimeout(function(){b("#lcl_wrap").removeClass("lcl_show_already_shaped");R()},10);T();"undefined"!=typeof lcl_size_n_show_timeout&&
clearTimeout(lcl_size_n_show_timeout);r=b(".lcl_first_sizing").length?e.open_close_time+20:e.animation_time;if(b(".lcl_browser_resize").length||b(".lcl_toggling_fs").length||k)r=0;lcl_size_n_show_timeout=setTimeout(function(){lcl_is_active&&(lcl_is_active=!1);b(".lcl_first_sizing").length&&e.autoplay&&1<g.elems.length&&(e.carousel||g.elem_index<g.elems.length-1)&&lcl_start_slideshow();"html"!=c.type||k||b(".lcl_first_sizing").length||b("#lcl_window").css("height","auto");if("image"==c.type){if(b(".lcl_fullscreen_mode").length){var d=
u,f=l.fs_img_behavior;if(b(".lcl_fullscreen_mode").length&&d.w<=b("#lcl_subj").width()&&d.h<=b("#lcl_subj").height())b(".lcl_image_elem").css("background-size","auto");else if("fit"==f)b(".lcl_image_elem").css("background-size","contain");else if("fill"==f)b(".lcl_image_elem").css("background-size","cover");else if("undefined"==typeof d)b(".lcl_image_elem").css("background-size","cover");else{f=b(window).width()/b(window).height()-d.w/d.h;var n=b(window).width()-d.w;d=b(window).height()-d.h;1.15>=
f&&-1.15<=f&&350>=n&&350>=d?b(".lcl_image_elem").css("background-size","cover"):b(".lcl_image_elem").css("background-size","contain")}}else b(".lcl_image_elem").css("background-size","cover");b(".lcl_zoomable").length&&(parseFloat(b("#lcl_elem_wrap").attr("lcl-zoom")),d=h,d=d.elems[d.elem_index],"undefined"!=typeof d&&(d=h.img_sizes_cache[d.src],d.w<=b("#lcl_subj").width()&&d.h<=b("#lcl_subj").height()?(b(".lcl_zoom_icon").hide(),b("#lcl_elem_wrap").css({width:"100%",height:"100%"}),b("#lcl_elem_wrap").attr("lcl-zoom",
1),b(".lcl_zoom_out").addClass("lcl_zoom_disabled"),b(".lcl_zoom_in").removeClass("lcl_zoom_disabled"),b("#lcl_subj").removeClass("lcl_zoom_wrap")):b(".lcl_zoom_icon").show()))}b(".lcl_loading_iframe").length&&ja();b("#lcl_wrap").removeClass("lcl_first_sizing lcl_switching_elem lcl_is_resizing lcl_browser_resize");b("#lcl_loader").removeClass("no_loader");b(document).trigger("lcl_resized_window")},r)};b(window).resize(function(){if(!lcl_shown||k!=lcl_curr_obj||b(".lcl_toggling_fs").length)return!1;
b("#lcl_wrap").addClass("lcl_browser_resize");"undefined"!=typeof lcl_rs_defer&&clearTimeout(lcl_rs_defer);lcl_rs_defer=setTimeout(function(){lcl_resize()},50)});var S=function(c,d,f,e){var g="undefined"==typeof e?1:e,k=b(".lcl_fullscreen_mode").length;b("#lcl_txt").outerHeight();var B=c/d;if(k&&b("#lcl_thumbs_nav").length)return b("#lcl_wrap").addClass("lcl_force_txt_over"),b("#lcl_subj").css("maxHeight","none"),b("#lcl_txt").css({right:0,width:"auto"}),h.txt_und_sizes="no_under",b(document).trigger("lcl_txt_und_calc"),
!1;b("#lcl_wrap").removeClass("lcl_force_txt_over").addClass("lcl_txt_under_calc");k?b("#lcl_txt").css({right:0,width:"auto"}):b("#lcl_txt").css({right:"auto",width:c});"undefined"!=typeof lcl_txt_under_calc&&clearInterval(lcl_txt_under_calc);lcl_txt_under_calc=setTimeout(function(){var n=Math.ceil(b("#lcl_txt").outerHeight()),q=d+n-f;if(k)return b("#lcl_wrap").removeClass("lcl_txt_under_calc"),b("#lcl_subj").css("maxHeight","calc(100% - "+n+"px)"),h.txt_und_sizes={w:c,h:d},b(document).trigger("lcl_txt_und_calc"),
!1;if(0<q&&("undefined"==typeof e||10>e)){n=d-q;q=Math.floor(n*B);var p=h.elems[h.elem_index].force_over_data;p||(p=400);return q<p||n<p?(b("#lcl_wrap").removeClass("lcl_txt_under_calc").addClass("lcl_force_txt_over"),b("#lcl_subj").css("maxHeight","none"),b("#lcl_txt").css({right:0,width:"auto"}),h.txt_und_sizes="no_under",b(document).trigger("lcl_txt_und_calc"),!0):S(q,n,f,g+1)}b("#lcl_wrap").removeClass("lcl_txt_under_calc");b("#lcl_subj").css("maxHeight",d+l.padding);h.txt_und_sizes={w:c,h:d+
n};b(document).trigger("lcl_txt_und_calc");return!0},120)},ha=function(c,d,f,e,g){g=b(".lcl_force_txt_over").length;if(f<d||"html"!=c.type&&e<d){if(g)return!0;b("#lcl_wrap").addClass("lcl_force_txt_over")}else{if(!g)return!0;b("#lcl_wrap").removeClass("lcl_force_txt_over")}return!1},ia=function(c,d){var f=l,e=b(".lcl_fullscreen_mode").length?!0:!1;if(b(".lcl_forced_outer_cmd").length){b("#lcl_wrap").removeClass("lcl_forced_outer_cmd");b("#lcl_wrap").removeClass("lcl_outer_cmd").addClass("lcl_inner_cmd");
var g=b("#lcl_nav_cmd").detach();b("#lcl_window").prepend(g)}e||!1!==h.inner_cmd_w||(h.inner_cmd_w=0,jQuery("#lcl_nav_cmd .lcl_icon").each(function(){if((b(this).hasClass("lcl_prev")||b(this).hasClass("lcl_next"))&&"middle"==f.nav_btn_pos)return!0;h.inner_cmd_w+=b(this).outerWidth(!0)}));return e||c.force_outer_cmd||d<=h.inner_cmd_w?(b("#lcl_wrap").addClass("lcl_forced_outer_cmd"),b("#lcl_wrap").removeClass("lcl_inner_cmd").addClass("lcl_outer_cmd"),g=b("#lcl_nav_cmd").detach(),b("#lcl_wrap").prepend(g),
!0):!1},m=function(c,d){var f=h,e=l.carousel;if(lcl_is_active||2>f.elems.length||!l.gallery||b(".lcl_switching_elem").length)return!1;if("next"==c)if(f.elem_index==f.elems.length-1){if(!e)return!1;c=0}else c=f.elem_index+1;else if("prev"==c)if(f.elem_index)c=f.elem_index-1;else{if(!e)return!1;c=f.elems.length-1}else if(c=parseInt(c,10),0>c||c>=f.elems.length||c==f.elem_index)return!1;"undefined"!=typeof lcl_slideshow&&("undefined"==typeof d||!e&&c==f.elems.length-1)&&lcl_stop_slideshow();lcl_is_active=
!0;U(c);y(!1,c,!0);b("#lcl_wrap").addClass("lcl_switching_elem");setTimeout(function(){b("#lcl_wrap").removeClass("lcl_playing_video");"html"==f.elems[f.elem_index].type&&(b("#lcl_window").css("height",b("#lcl_contents_wrap").outerHeight()),b("#lcl_contents_wrap").css("maxHeight","none"));"function"==typeof l.on_elem_switch&&l.on_elem_switch.call({opts:l,vars:h,new_el:c});!f.is_arr_instance&&lcl_curr_obj&&(f.elems_selector?b(f.elems_selector):lcl_curr_obj).first().trigger("lcl_on_elem_switch",[f.elem_index,
c]);b("#lcl_wrap").removeClass("lcl_no_txt lcl_loading_iframe");b("#lcl_txt").empty();f.elem_index=c;y(!0);O()},l.fading_time)},V=function(c){var d=l;if(!d.progressbar)return!1;c=c?0:d.animation_time+d.fading_time;var f=d.slideshow_time+d.animation_time-c;b("#lcl_progressbar").length||b("#lcl_wrap").append('<div id="lcl_progressbar"></div>');"undefined"!=typeof lcl_pb_timeout&&clearTimeout(lcl_pb_timeout);lcl_pb_timeout=setTimeout(function(){b("#lcl_progressbar").stop(!0).removeAttr("style").css("width",
0).animate({width:"100%"},f,"linear",function(){b("#lcl_progressbar").fadeTo(0,0)})},c)},M=function(){if(!lcl_shown)return!1;"function"==typeof l.on_close&&l.on_close.call({opts:l,vars:h});h.is_arr_instance||(h.elems_selector?b(h.elems_selector):lcl_curr_obj).first().trigger("lcl_on_close");b(document).trigger("lcl_on_close_global");b("#lcl_wrap").removeClass("lcl_shown").addClass("lcl_is_closing lcl_tn_hidden");lcl_stop_slideshow();b(".lcl_fullscreen_mode").length&&W();setTimeout(function(){b("#lcl_wrap, #lcl_inline_style").remove();
l.remove_scrollbar&&(jQuery("html").attr("style",h.html_style),jQuery("body").attr("style",h.body_style));b(document).trigger("lcl_closed_global");lcl_is_active=lcl_shown=lcl_curr_vars=lcl_curr_opts=lcl_curr_obj=!1},l.open_close_time+80);"undefined"!=typeof lcl_size_check&&clearTimeout(lcl_size_check)},F=function(){lcl_hashless_url=window.location.href;if(-1!==lcl_hashless_url.indexOf("#")){var b=lcl_hashless_url.split("#");lcl_hashless_url=b[0];lcl_url_hash="#"+b[1]}var d={};b=lcl_hashless_url.slice(lcl_hashless_url.indexOf("?")+
1).split("&");jQuery.each(b,function(b,c){var e=c.split("=");d[e[0]]=e[1]});return d};if(0==lcl_objs.length)b(document).on("lcl_before_show_global",function(b,d){if(lcl_curr_opts.deeplink&&d.hash&&-1===navigator.appVersion.indexOf("MSIE 9.")){var c=F(),e=d.title?d.hash+"/"+encodeURIComponent(d.title.replace(/\s+/g,"-")):d.hash,g="lcl="+e+lcl_url_hash;if(-1===lcl_hashless_url.indexOf("?"))history.pushState(null,null,lcl_hashless_url+"?"+g);else{if("undefined"!=typeof c.lcl&&c.lcl==e)return!0;var h=
lcl_hashless_url.slice(0,lcl_hashless_url.indexOf("?")+1),k=0,l=!1;jQuery.each(c,function(b,c){"undefined"!=typeof b&&(0<k&&(h+="&"),"lcl"!=b&&(h+=c?b+"="+c:b,l=!0,k++))});l&&(h+="&");h+=g;history.pushState(null,null,h)}}});if(0==lcl_objs.length)b(document).on("lcl_on_close_global",function(){var b=F();if("undefined"==typeof b.lcl||-1!==navigator.appVersion.indexOf("MSIE 9."))return!0;var d=[];jQuery.each(b,function(b,c){"undefined"!=typeof b&&b&&"lcl"!=b&&d.push(c?b+"="+c:b)});b=d.length?"?":"";
b=lcl_hashless_url.slice(0,lcl_hashless_url.indexOf("?"))+b+d.join("&")+lcl_url_hash;history.pushState(null,null,b)});v.deeplink&&!lcl_deeplink_tracked&&(lcl_deeplink_tracked=!0,window.onpopstate=function(c){c=F();if("undefined"==typeof c.lcl)lcl_shown&&lcl_close();else{var d=c.lcl.split("/");lcl_shown?b.each(lcl_curr_vars.elems,function(b,c){if(c.hash==d[0])return lcl_switch(b),!1}):b.each(lcl_objs,function(c,e){b(document).trigger("lcl_open_from_hash",[e,d[0]])})}});var ka=function(){var c=F();
if(!lcl_shown&&"undefined"!=typeof c.lcl){var d=c.lcl.split("/");b.each(lcl_objs,function(c,e){b(document).trigger("lcl_open_from_hash",[e,d[0]])})}};b(document).ready(function(){setTimeout(function(){b(document).trigger("lcl_look_for_deeplink")},100)});b(document).off("lcl_look_for_deeplink").on("lcl_look_for_deeplink",function(){ka()});b(document).off("lcl_open_from_hash").on("lcl_open_from_hash",function(c,d,f){c=b.data(d,"lcl_vars");if("undefined"==typeof c||!c)return!1;b.each(c.elems,function(b,
c){if(c.hash==f)return lcl_curr_obj=d,lcl_open(d,b),!1})});var J=function(c,d){"undefined"==typeof d&&(d=!1);if(!lcl_shown||!l.fullscreen||!d&&lcl_is_active)return!1;var f=l,e=h;b("#lcl_wrap").addClass("lcl_toggling_fs");f.browser_fs_mode&&"undefined"!=typeof c&&(document.documentElement.requestFullscreen?document.documentElement.requestFullscreen():document.documentElement.msRequestFullscreen?document.documentElement.msRequestFullscreen():document.documentElement.mozRequestFullScreen?document.documentElement.mozRequestFullScreen():
document.documentElement.webkitRequestFullscreen&&document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT));setTimeout(function(){b("#lcl_wrap").addClass("lcl_fullscreen_mode");x(e.elems[e.elem_index]);b(document).on("lcl_resized_window",function(){b(document).off("lcl_resized_window");(d||"under"==lcl_curr_opts.data_position&&!b(".lcl_force_txt_over").length)&&x(lcl_curr_vars.elems[lcl_curr_vars.elem_index]);setTimeout(function(){b("#lcl_wrap").removeClass("lcl_toggling_fs")},
150)})},d?f.open_close_time:f.fading_time);"function"==typeof f.on_fs_enter&&f.on_fs_enter.call({opts:f,vars:e});h.is_arr_instance||lcl_curr_obj.first().trigger("lcl_on_fs_enter")},X=function(c){if(!lcl_shown||!l.fullscreen||lcl_is_active)return!1;var d=l;b("#lcl_wrap").addClass("lcl_toggling_fs");b("#lcl_window").fadeTo(70,0);setTimeout(function(){if(d.browser_fs_mode&&"undefined"!=typeof c){W();var f=250}else f=0;b("#lcl_wrap").removeClass("lcl_fullscreen_mode");setTimeout(function(){x(h.elems[h.elem_index]);
var c=c||navigator.userAgent;c=-1<c.indexOf("MSIE ")||-1<c.indexOf("Trident/")?100:0;setTimeout(function(){b("#lcl_window").fadeTo(30,1);b("#lcl_wrap").removeClass("lcl_toggling_fs")},300+c)},f)},70);"function"==typeof d.on_fs_exit&&d.on_fs_exit.call({opts:l,vars:h});h.is_arr_instance||(h.elems_selector?b(h.elems_selector):lcl_curr_obj).first().trigger("lcl_on_fs_exit")},W=function(){document.exitFullscreen?document.exitFullscreen():document.msExitFullscreen?document.msExitFullscreen():document.mozCancelFullScreen?
document.mozCancelFullScreen():document.webkitExitFullscreen&&document.webkitExitFullscreen()};if(0==lcl_objs.length)b(document).on("lcl_before_show_global",function(c,d,f){b(".lcl_zoom_icon").hide();b("#lcl_subj").removeClass("lcl_zoomable lcl_zoom_wrap");if("image"!=d.type||!lcl_curr_opts.img_zoom)return b("#lcl_elem_wrap").removeAttr("lcl-zoom"),!0;c=lcl_curr_vars.img_sizes_cache[d.src];if(b("#lcl_subj").width()>=c.w&&b("#lcl_subj").height()>=c.h)return!0;b("#lcl_elem_wrap").css({width:"100%",
height:"100%"});b("#lcl_elem_wrap").attr("lcl-zoom",1);b("#lcl_subj").addClass("lcl_zoomable");b(".lcl_zoom_out").addClass("lcl_zoom_disabled");b(".lcl_zoom_in").removeClass("lcl_zoom_disabled");b(".lcl_zoom_in .lcl_zoom_helper").remove();b(".lcl_zoom_icon").show()});var z=function(c,d){var f=b("#lcl_elem_wrap"),e=f.width(),g=f.height(),k=parseFloat(b("#lcl_elem_wrap").attr("lcl-zoom")),l=0,t=h,q=h.img_sizes_cache[t.elems[t.elem_index].src];c&&(e<q.w||g<q.h)?(l=k+.25,f.css({width:100*l+"%",height:100*
l+"%"}),lcl_stop_slideshow()):!c&&e>b("#lcl_subj").width()&&(l=k-.25,1>=l&&(l=1),f.css({width:100*l+"%",height:100*l+"%"}));l&&(b("#lcl_elem_wrap").attr("lcl-zoom",l),b(".lcl_zoom_in .lcl_zoom_helper").remove(),1<l&&b(".lcl_zoom_in").append('<span class="lcl_zoom_helper">'+l+"x</span>"),1<l?(b(".lcl_zoom_out").removeClass("lcl_zoom_disabled"),b("#lcl_subj").addClass("lcl_zoom_wrap"),b("#lcl_subj").lcl_smoothscroll(.35,400),e=b("#lcl_subj").scrollTop(),g=b("#lcl_subj").scrollLeft(),k=c?1.25:.75,b("#lcl_subj").clearQueue().animate({scrollTop:e||
g?e*k:b("#lcl_subj").height()/4,scrollLeft:e||g?g*k:b("#lcl_subj").width()/4},300,"linear"),setTimeout(function(){q.w<=f.width()&&q.h<=f.height()?b(".lcl_zoom_in").addClass("lcl_zoom_disabled"):b(".lcl_zoom_in").removeClass("lcl_zoom_disabled")},310)):(b(".lcl_zoom_out").addClass("lcl_zoom_disabled"),b(".lcl_zoom_in").removeClass("lcl_zoom_disabled"),b("#lcl_subj").removeClass("lcl_zoom_wrap")))};b(document).on("click",".lcl_zoom_in",function(){if(k!=lcl_curr_obj)return!0;z(!0)});b(document).on("click",
".lcl_zoom_out",function(){if(k!=lcl_curr_obj)return!0;z(!1)});var fa=function(){var c=l;if(!c.comments||"undefined"==typeof c.comments.type||"rside"!=c.data_position&&"lside"!=c.data_position)return!1;var d=h.elems[h.elem_index];d=d.canonical_url?d.canonical_url:window.location.origin+"?lcl="+d.hash;"facebook"==c.comments.type?(b("#lcl_txt > *").last().addClass("lcl_txt_border"),b("#lcl_txt").append('<div class="lcl_comments_wrap" class="lcl_fb_comments"><div class="fb-comments" data-href="'+d+'" data-width="100%" data-numposts="1" data-colorscheme="'+
c.comments.style+'"></div></div><script type="text/javascript">if(jQuery("#fb-root").length && window.FB) {FB.XFBML.parse();}\x3c/script>')):"disqus"==c.comments.type&&(b("#lcl_txt > *").last().addClass("lcl_txt_border"),b("#lcl_txt").append('<div class="lcl_comments_wrap" class="lcl_disqus_comments"><div id="disqus_thread"></div></div>'),b(document).ready(function(b){b=document;var d=b.createElement("script");d.src="//"+c.comments.shortname+".disqus.com/embed.js";d.setAttribute("data-timestamp",
+new Date);(b.head||b.body).appendChild(d)}))},da=function(){var c=!1,d=!1,f=Date.now();b("#lcl_thumbs_nav").append('<span class="lcl_tn_prev"></span><ul class="lcl_tn_inner"></ul><span class="lcl_tn_next"></span>');b("#lcl_thumbs_nav").attr("rel",f);b.each(h.elems,function(e,g){if("unknown"!=g.type){c||(d&&d!=g.type?c=!0:d=g.type);var k="",m="";tpc="";if(g.thumb)m=g.thumb,k="style=\"background-image: url('"+g.thumb+"');\"";else{switch(g.type){case "image":m=g.src;break;case "html":g.poster&&(m=g.poster)}m&&
(l.thumbs_maker_url&&(g.poster||-1===b.inArray(g.type,["youtube","vimeo","dailymotion"]))&&(m=l.thumbs_maker_url.replace("%URL%",encodeURIComponent(m)).replace("%W%",l.thumbs_w).replace("%H%",l.thumbs_h)),k="style=\"background-image: url('"+m+"');\"",-1===b.inArray(g.type,["youtube","vimeo","dailymotion"])||g.poster||(h.elems[e].vid_poster=m))}if(("html"==g.type||"iframe"==g.type)&&!k)return!0;var t="video"!=g.type||k?"":'<video src="'+g.src+'"></video>';tpc="lcl_tn_preload";b(".lcl_tn_inner").append('<li class="lcl_tn_'+
g.type+" "+tpc+'" title="'+g.title+'" rel="'+e+'" '+k+">"+t+"</li>");tpc&&la(m,e,f)}});if(2>b(".lcl_tn_inner > li").length)return b("#lcl_thumbs_nav").remove(),!1;b(".lcl_tn_inner > li").css("width",l.thumbs_w);lcl_on_mobile||b(".lcl_tn_inner").lcl_smoothscroll(.3,400,!1,!0);c&&l.tn_icons&&b(".lcl_tn_inner").addClass("lcl_tn_mixed_types");U(h.elem_index)},la=function(c,d,f){b("<img/>").bind("load",function(){if(!h)return!1;h.img_sizes_cache[c]={w:this.width,h:this.height};b("#lcl_thumbs_nav[rel="+
f+"] li[rel="+d+"]").removeClass("lcl_tn_preload");setTimeout(function(){T();N()},500)}).attr("src",c)},Y=function(){var c=0;b(".lcl_tn_inner > li").each(function(){c+=b(this).outerWidth(!0)});return c},T=function(){if(!b("#lcl_thumbs_nav").length)return!1;Y()>b(".lcl_tn_inner").width()?b("#lcl_thumbs_nav").addClass("lcl_tn_has_arr"):b("#lcl_thumbs_nav").removeClass("lcl_tn_has_arr")},N=function(){var c=b(".lcl_tn_inner").scrollLeft();c?b(".lcl_tn_prev").removeClass("lcl_tn_disabled_arr").stop(!0).fadeTo(150,
1):b(".lcl_tn_prev").addClass("lcl_tn_disabled_arr").stop(!0).fadeTo(150,.5);c>=Y()-b(".lcl_tn_inner").width()?b(".lcl_tn_next").addClass("lcl_tn_disabled_arr").stop(!0).fadeTo(150,.5):b(".lcl_tn_next").removeClass("lcl_tn_disabled_arr").stop(!0).fadeTo(150,1)};b(document).on("lcl_smoothscroll_end",".lcl_tn_inner",function(b){if(k!=lcl_curr_obj)return!0;N()});var U=function(c){var d=b(".lcl_tn_inner > li[rel="+c+"]");if(!d.length)return!1;var f=0;b(".lcl_tn_inner > li").each(function(d,e){if(b(this).attr("rel")==
c)return f=d,!1});var e=b(".lcl_tn_inner > li").last().outerWidth(),g=parseInt(b(".lcl_tn_inner > li").last().css("margin-left"),10);b(".lcl_tn_inner").width();var h=Math.floor((b(".lcl_tn_inner").width()-e-g)/2);e=e*f+g*(f-1)+Math.floor(g/2)-h;b(".lcl_tn_inner").stop(!0).animate({scrollLeft:e},500,function(){b(".lcl_tn_inner").trigger("lcl_smoothscroll_end")});b(".lcl_tn_inner > li").removeClass("lcl_sel_thumb");d.addClass("lcl_sel_thumb")};b.fn.lcl_smoothscroll=function(c,d,f,e){if(lcl_on_mobile)return!1;
this.off("mousemove mousedown mouseup mouseenter mouseleave");var g=this,h="undefined"!=typeof f&&f?!1:!0,k="undefined"!=typeof e&&e?!1:!0,l=!1,m=!1,p=0,v=0,r=0,u=0;g.mousemove(function(b){!0===m&&(g.stop(!0),h&&g.scrollLeft(u+(v-b.pageX)),k&&g.scrollTop(r+(p-b.pageY)))});g.mouseover(function(){l&&clearTimeout(l)});g.mouseout(function(){l=setTimeout(function(){l=m=!1},500)});g.mousedown(function(b){"undefined"!=typeof lc_sms_timeout&&clearTimeout(lc_sms_timeout);m=!0;r=g.scrollTop();u=g.scrollLeft();
p=b.pageY;v=b.pageX});g.mouseup(function(e){m=!1;var f=g.scrollTop(),l=-1*(r-f);f+=l*c;var n=g.scrollLeft(),p=-1*(u-n);n+=p*c;if(3>l&&-3<l&&3>p&&-3<p)return b(e.target).trigger("lcl_tn_elem_click"),!1;if(20<l||20<p)e={},k&&(e.scrollTop=f),h&&(e.scrollLeft=n),g.stop(!0).animate(e,d,"linear",function(){g.trigger("lcl_smoothscroll_end")})})};if(!w.is_arr_instance)if(v.live_elements&&w.elems_selector)b(document).off("click",w.elems_selector).on("click",w.elems_selector,function(c){c.preventDefault();
b.data(k,"lcl_vars").elems_count=b(w.elems_selector).length;K(k,b(this));k.first().trigger("lcl_clicked_elem",[b(this)])});else k.off("click"),k.on("click",function(c){c.preventDefault();K(k,b(this));k.first().trigger("lcl_clicked_elem",[b(this)])});b(document).on("click","#lcl_overlay:not(.lcl_modal), .lcl_close, #lcl_corner_close",function(b){if(k!=lcl_curr_obj)return!0;M()});b(document).on("click",".lcl_prev",function(b){if(k!=lcl_curr_obj)return!0;m("prev")});b(document).on("click",".lcl_next",
function(b){if(k!=lcl_curr_obj)return!0;m("next")});b(document).bind("keydown",function(c){if(lcl_shown){if(k!=lcl_curr_obj)return!0;39==c.keyCode?(c.preventDefault(),m("next")):37==c.keyCode?(c.preventDefault(),m("prev")):27==c.keyCode?(c.preventDefault(),M()):122==c.keyCode&&l.fullscreen&&("undefined"!=typeof lcl_fs_key_timeout&&clearTimeout(lcl_fs_key_timeout),lcl_fs_key_timeout=setTimeout(function(){b(".lcl_fullscreen_mode").length?X():J()},50))}});b(document).on("wheel","#lcl_overlay, #lcl_window, #lcl_thumbs_nav:not(.lcl_tn_has_arr)",
function(c){if(k!=lcl_curr_obj||!lcl_curr_opts.mousewheel)return!0;var d=b(c.target);if(d.is("#lcl_window")||d.parents("#lcl_window").length){var f=!0;for(a=0;20>a&&!d.is("#lcl_window");a++)if(d[0].scrollHeight>d.outerHeight()){f=!1;break}else d=d.parent();f&&(c.preventDefault(),c=c.originalEvent.deltaY,0<c?m("next"):m("prev"))}else c.preventDefault(),c=c.originalEvent.deltaY,0<c?m("next"):m("prev")});b(document).on("click",".lcl_image_elem",function(c){if(k!=lcl_curr_obj)return!0;lcl_img_click_track=
setTimeout(function(){b(".lcl_zoom_wrap").length||m("next")},250)});b(document).on("dblclick",".lcl_image_elem",function(c){if(k!=lcl_curr_obj||!lcl_curr_opts.img_zoom||!b(".lcl_zoom_icon").length)return!0;"undefined"!=typeof lcl_img_click_track&&(clearTimeout(lcl_img_click_track),z(!0))});b(document).on("click",".lcl_txt_toggle",function(c){if(k!=lcl_curr_obj)return!0;c=l;if(!lcl_is_active&&!b(".lcl_no_txt").length&&!b(".lcl_toggling_txt").length)if("over"!=c.data_position){var d="rside"==c.data_position||
"lside"==c.data_position?!0:!1,f=b(".lcl_force_txt_over").length,e=150>c.animation_time?c.animation_time:150,g=0;d&&!f?b("#lcl_subj").fadeTo(e,0):f||(b("#lcl_contents_wrap").fadeTo(e,0),g=e);setTimeout(function(){b("#lcl_wrap").toggleClass("lcl_hidden_txt")},g);f||(lcl_is_active=!0,b("#lcl_wrap").addClass("lcl_toggling_txt"),setTimeout(function(){lcl_is_active=!1;lcl_resize()},c.animation_time),setTimeout(function(){b("#lcl_wrap").removeClass("lcl_toggling_txt");d&&!f?b("#lcl_subj").fadeTo(e,1):f||
b("#lcl_contents_wrap").fadeTo(e,1)},2*c.animation_time+50))}else b("#lcl_wrap").toggleClass("lcl_hidden_txt")});b(document).on("click",".lcl_play",function(c){if(k!=lcl_curr_obj)return!0;b(".lcl_is_playing").length?lcl_stop_slideshow():lcl_start_slideshow()});b(document).on("click",".lcl_elem",function(c){if(k!=lcl_curr_obj)return!0;b(".lcl_playing_video").length||-1===b.inArray(b("#lcl_wrap").attr("lcl-type"),["video"])||(lcl_stop_slideshow(),b("#lcl_wrap").addClass("lcl_playing_video"))});var ja=
function(){"undefined"!=typeof lcl_iframe_click_intval&&clearInterval(lcl_iframe_click_intval);lcl_iframe_click_intval=setInterval(function(){var c=b(document.activeElement);c.is("iframe")&&c.hasClass("lcl_elem")&&(b(".lcl_youtube_elem").length||b(".lcl_vimeo_elem").length||b(".lcl_dailymotion_elem").length)&&(lcl_stop_slideshow(),b("#lcl_wrap").addClass("lcl_playing_video"),clearInterval(lcl_iframe_click_intval))},300)};b(document).on("click",".lcl_socials",function(c){if(k!=lcl_curr_obj)return!0;
if(b(".lcl_socials > div").length)b(".lcl_socials_tt").removeClass("lcl_show_tt"),setTimeout(function(){b(".lcl_socials").removeClass("lcl_socials_shown").empty()},260);else{var d=lcl_curr_vars.elems[lcl_curr_vars.elem_index];c=encodeURIComponent(window.location.href);var f=encodeURIComponent(d.title).replace(/'/g,"\\'");encodeURIComponent(d.txt).replace(/'/g,"\\'");if("image"==d.type)var e=d.src;else e=d.poster?d.poster:!1,e||"undefined"==typeof d.vid_poster||(e=d.vid_poster);var g='<div class="lcl_socials_tt lcl_tooltip lcl_tt_bottom">';
g=lcl_curr_opts.fb_direct_share?g+'<a class="lcl_icon lcl_fb" href="javascript: void(0)"></a>':g+('<a class="lcl_icon lcl_fb" onClick="window.open(\'https://www.facebook.com/sharer?u='+c+"&display=popup','sharer','toolbar=0,status=0,width=590,height=325');\" href=\"javascript: void(0)\"></a>");g+='<a class="lcl_icon lcl_twit" onClick="window.open(\'https://twitter.com/share?text=Check%20out%20%22'+f+"%22%20@&url="+c+"','sharer','toolbar=0,status=0,width=548,height=325');\" href=\"javascript: void(0)\"></a>";
lcl_on_mobile&&(g+='<br/><a class="lcl_icon lcl_wa" href="whatsapp://send?text='+c+'" data-action="share/whatsapp/share"></a>');e&&(g+='<a class="lcl_icon lcl_pint" onClick="window.open(\'http://pinterest.com/pin/create/button/?url='+c+"&media="+encodeURIComponent(e)+"&description="+f+"','sharer','toolbar=0,status=0,width=575,height=330');\" href=\"javascript: void(0)\"></a>");g+="</div>";b(".lcl_socials").addClass("lcl_socials_shown").html(g);setTimeout(function(){b(".lcl_socials_tt").addClass("lcl_show_tt")},
20);if(lcl_curr_opts.fb_direct_share)b(document).off("click",".lcl_fb").on("click",".lcl_fb",function(b){FB.ui({method:"share_open_graph",action_type:"og.shares",action_properties:JSON.stringify({object:{"og:url":window.location.href,"og:title":d.title,"og:description":d.txt,"og:image":e}})},function(b){window.close()})})}});b(document).on("click",".lcl_fullscreen",function(c){if(k!=lcl_curr_obj)return!0;b(".lcl_fullscreen_mode").length?X(!0):J(!0)});b(document).on("click",".lcl_thumbs_toggle",function(c){if(k!=
lcl_curr_obj)return!0;c=b(".lcl_fullscreen_mode").length;b("#lcl_wrap").addClass("lcl_toggling_tn").toggleClass("lcl_tn_hidden");c||setTimeout(function(){lcl_resize()},160);setTimeout(function(){b("#lcl_wrap").removeClass("lcl_toggling_tn")},lcl_curr_opts.animation_time+50)});v=lcl_on_mobile?" click":"";b(document).on("lcl_tn_elem_click"+v,".lcl_tn_inner > li",function(c){if(k!=lcl_curr_obj)return!0;c=b(this).attr("rel");m(c)});b(document).on("click",".lcl_tn_prev:not(.lcl_tn_disabled_arr)",function(c){if(k!=
lcl_curr_obj)return!0;b(".lcl_tn_inner").stop(!0).animate({scrollLeft:b(".lcl_tn_inner").scrollLeft()-lcl_curr_opts.thumbs_w-10},300,"linear",function(){b(".lcl_tn_inner").trigger("lcl_smoothscroll_end")})});b(document).on("click",".lcl_tn_next:not(.lcl_tn_disabled_arr)",function(c){if(k!=lcl_curr_obj)return!0;b(".lcl_tn_inner").stop(!0).animate({scrollLeft:b(".lcl_tn_inner").scrollLeft()+lcl_curr_opts.thumbs_w+10},300,"linear",function(){b(".lcl_tn_inner").trigger("lcl_smoothscroll_end")})});b(document).on("wheel",
"#lcl_thumbs_nav.lcl_tn_has_arr",function(c){if(k!=lcl_curr_obj)return!0;c.preventDefault();0<c.originalEvent.deltaY?b(".lcl_tn_prev:not(.lcl_tn_disabled_arr)").trigger("click"):b(".lcl_tn_next:not(.lcl_tn_disabled_arr)").trigger("click")});b(document).on("contextmenu","#lcl_wrap *",function(){if(k!=lcl_curr_obj)return!0;if(l.rclick_prevent)return!1});b(window).on("touchmove",function(c){b(c.target);if(!lcl_shown||!lcl_on_mobile||k!=lcl_curr_obj)return!0;b(c.target).parents("#lcl_window").length||
b(c.target).parents("#lcl_thumbs_nav").length||c.preventDefault()});var ca=function(){if("function"!=typeof AlloyFinger)return!1;lcl_is_pinching=!1;var c=document.querySelector("#lcl_wrap");new AlloyFinger(c,{singleTap:function(c){"lcl_overlay"!=b(c.target).attr("id")||l.modal||lcl_close()},doubleTap:function(b){b.preventDefault();z(!0)},pinch:function(b){b.preventDefault();lcl_is_pinching=!0;"undefined"!=typeof lcl_swipe_delay&&clearTimeout(lcl_swipe_delay);"undefined"!=typeof lcl_pinch_delay&&clearTimeout(lcl_pinch_delay);
lcl_pinch_delay=setTimeout(function(){1.2<b.scale?z(!0):.8>b.scale&&z(!1);setTimeout(function(){lcl_is_pinching=!1},300)},20)},touchStart:function(b){lcl_touchstartX=b.changedTouches[0].clientX},touchEnd:function(c){var d=lcl_touchstartX-c.changedTouches[0].clientX;if((-50>d||50<d)&&!lcl_is_pinching){if(b(c.target).parents("#lcl_thumbs_nav").length||b(c.target).parents(".lcl_zoom_wrap").length)return!1;c=b(c.target).parents(".lcl_zoomable").length?250:0;"undefined"!=typeof lcl_swipe_delay&&clearTimeout(lcl_swipe_delay);
lcl_swipe_delay=setTimeout(function(){-50>d?m("prev"):m("next")},c)}}})},C=function(){if(!lcl_curr_obj)return!1;h=b.data(lcl_curr_obj,"lcl_vars");l=b.data(lcl_curr_obj,"lcl_settings");return h?!0:(console.error("LC Lightbox. Object not initialized"),!1)};lcl_open=function(c,d){var f=h=b.data(c,"lcl_vars");if(f){if("undefined"==typeof f.elems[d])return console.error("LC Lightbox - cannot open. Unexisting index"),!1;f.elem_index=d;$clicked_obj=f.is_arr_instance?!1:b(c[d]);return K(c,$clicked_obj)}console.error("LC Lightbox - cannot open. Object not initialized");
return!1};lcl_resize=function(){if(!lcl_shown||lcl_is_active||!C())return!1;var c=h;"undefined"!=typeof lcl_size_check&&clearTimeout(lcl_size_check);lcl_size_check=setTimeout(function(){b("#lcl_wrap").addClass("lcl_is_resizing");N();return x(c.elems[c.elem_index])},20)};lcl_close=function(){return lcl_shown&&!lcl_is_active&&C()?M():!1};lcl_switch=function(b){return lcl_shown&&!lcl_is_active&&C()?m(b):!1};lcl_start_slideshow=function(c){if(!lcl_shown||"undefined"==typeof c&&"undefined"!=typeof lcl_slideshow||
!C())return!1;var d=l;if(!d.carousel&&h.elem_index==h.elems.length-1)return!1;"undefined"!=typeof lcl_slideshow&&clearInterval(lcl_slideshow);b("#lcl_wrap").addClass("lcl_is_playing");var f=d.animation_time+d.slideshow_time;V(!0);lcl_slideshow=setInterval(function(){V(!1);m("next",!0)},f);"undefined"==typeof c&&("function"==typeof d.slideshow_start&&d.slideshow_start.call({opts:d,vars:h}),h.is_arr_instance||(h.elems_selector?b(h.elems_selector):lcl_curr_obj).first().trigger("lcl_slideshow_start",
[f]));return!0};lcl_stop_slideshow=function(){if(!lcl_shown||"undefined"==typeof lcl_slideshow||!C())return!1;var c=l;if(!c)return console.error("LC Lightbox. Object not initialized"),!1;clearInterval(lcl_slideshow);lcl_slideshow=void 0;b("#lcl_wrap").removeClass("lcl_is_playing");b("#lcl_progressbar").stop(!0).animate({marginTop:-3*b("#lcl_progressbar").height()},300,function(){b(this).remove()});"function"==typeof c.slideshow_end&&c.slideshow_end.call({opts:l,vars:h});h.is_arr_instance||(h.elems_selector?
b(h.elems_selector):lcl_curr_obj).first().trigger("lcl_slideshow_end",[]);return!0};return k}})(jQuery);jQuery(document).ready(function(){
	var pcma_psw_is_acting = false; // security var to avoid multiple calls
	
	// show recovery form
	jQuery(document).delegate('.pcma_psw_recovery_trigger', 'click', function() {
		var $pcma_form = jQuery(this).parents('form');
		
		$pcma_form.children().hide();
		$pcma_form.find('.pcma_psw_recovery_wrap').fadeIn();
	});
	
	
	// hide recovery form
	jQuery(document).delegate('.pcma_del_recovery', 'click', function() {
		var $pcma_form = jQuery(this).parents('form');
		
		$pcma_form.children().fadeIn();
		$pcma_form.find('.pcma_psw_recovery_wrap').hide();
	});
	
	
	// handle recovery request
	jQuery(document).delegate('.pcma_do_recovery', 'click', function(e) {
		e.preventDefault();
		
		$target_div = jQuery(this).parents('form');
		var pcma_username = $target_div.find('.pcma_psw_username').val();

		if(!pcma_psw_is_acting && jQuery.trim(pcma_username) != '') {
			pcma_psw_is_acting = true;
			
			$target_div.find('.pcma_do_recovery').addClass('pc_loading_btn');
			$target_div.find('.pcma_del_recovery').fadeOut('fast');
			
			var cur_url = jQuery(location).attr('href');
			jQuery.ajax({
				type: "POST",
				url: cur_url,
				dataType: "json",
				data: "pcma_psw_recovery=1&pcma_username=" + pcma_username,
				success: function(pcma_data){
					pcma_psw_is_acting = false;

					if(pcma_data.resp == 'success') {
						$target_div.find('.pcma_psw_recovery_message').empty().append('<span class="pc_success_mess">' + pcma_data.mess + '</span>');
						jQuery('.pcma_do_recovery').fadeOut(function() {
							jQuery(this).remove();
						});
					}
					else {
						$target_div.find('.pcma_psw_recovery_message').empty().append('<span class="pc_error_mess">' + pcma_data.mess + '</span>');	
					}
					
					// a bit of delay to display the loader
					setTimeout(function() {
						$target_div.find('.pcma_do_recovery').removeClass('pc_loading_btn');
					}, 370);

					$target_div.find('.pcma_del_recovery').fadeIn('fast');
				}
			});
		}
	});	
	
});jQuery(document).ready(function(){
	jQuery('body').delegate('.pc_custom_form_btn:not(.pc_loading_btn)', 'click', function(e) {
		e.preventDefault();
		
		$pcud_target_form = jQuery(this).parents('form');
		var f_data = jQuery(this).parents('form').serialize();
		
		// HTML5 validate first
		if(!$pcud_target_form.pc_validate_form_fieldset()) {
			return false;	
		}
		
		$pcud_target_form.find('.pc_custom_form_btn').addClass('pc_loading_btn');
		$pcud_target_form.find('.pc_custom_form_message').empty();
		
		jQuery.ajax({
			type: "POST",
			url: window.location.href,
			data: "type=pcud_cf_submit&" + f_data,
			dataType: "json",
			success: function(pc_data){
				if(pc_data.resp == 'success') {
					$pcud_target_form.find('.pc_custom_form_message').empty().append('<span class="pc_success_mess">' + pc_data.mess + '</span>');
				}
				else {
					$pcud_target_form.find('.pc_custom_form_message').empty().append('<span class="pc_error_mess">' + pc_data.mess + '</span>');
				}
				
				// redirect
				if(typeof(pc_data.redirect) != 'undefined' && pc_data.redirect) {
					setTimeout(function() {
					  window.location.href = pc_data.redirect;
					}, 1000);		
				}
				
				// a bit of delay to display the loader
				setTimeout(function() {
					$pcud_target_form.find('.pc_custom_form_btn').removeClass('pc_loading_btn');
				}, 370);
			}
		});
	});
	
	// enter key handler
	jQuery('.pc_custom_form .pc_rf_field input').keypress(function(event){
		if(event.keyCode === 13){
			jQuery(this).parents('form').find('.pc_custom_form_btn').trigger('click');
		}
		
		event.cancelBubble = true;
		if(event.stopPropagation) event.stopPropagation();
   	});
	
	
	// datepicker
	if(jQuery('.pcud_datepicker').size() > 0) {
		var pcud_datepicker_init = function(type) {
			return {
				dateFormat : (type == 'eu') ? 'dd/mm/yy' : 'mm/dd/yy',
				beforeShow: function(input, inst) {
					if( !jQuery('#ui-datepicker-div').parent().hasClass('pcud_dp') ) {
						jQuery('#ui-datepicker-div').wrap('<div class="pcud_dp"></div>');
					}
				},
				monthNames: 		pcud_datepick_str.monthNames,
				monthNamesShort: 	pcud_datepick_str.monthNamesShort,
				dayNames: 			pcud_datepick_str.dayNames,
				dayNamesShort: 		pcud_datepick_str.dayNamesShort,
				dayNamesMin:		pcud_datepick_str.dayNamesMin,
				isRTL:				pcud_datepick_str.isRTL
			};	
		}
		
		jQuery('.pcud_dp_eu_date').datepicker( pcud_datepicker_init('eu') );
		jQuery('.pcud_dp_us_date').datepicker( pcud_datepicker_init('us') );
	}
});jQuery(function(t){if("undefined"==typeof wc_add_to_cart_params)return!1;var a=function(){t(document.body).on("click",".add_to_cart_button",this.onAddToCart).on("click",".remove_from_cart_button",this.onRemoveFromCart).on("added_to_cart",this.updateButton).on("added_to_cart",this.updateCartPage).on("added_to_cart removed_from_cart",this.updateFragments)};a.prototype.onAddToCart=function(a){var o=t(this);if(o.is(".ajax_add_to_cart")){if(!o.attr("data-product_id"))return!0;a.preventDefault(),o.removeClass("added"),o.addClass("loading");var r={};t.each(o.data(),function(t,a){r[t]=a}),t(document.body).trigger("adding_to_cart",[o,r]),t.post(wc_add_to_cart_params.wc_ajax_url.toString().replace("%%endpoint%%","add_to_cart"),r,function(a){a&&(a.error&&a.product_url?window.location=a.product_url:"yes"!==wc_add_to_cart_params.cart_redirect_after_add?t(document.body).trigger("added_to_cart",[a.fragments,a.cart_hash,o]):window.location=wc_add_to_cart_params.cart_url)})}},a.prototype.onRemoveFromCart=function(a){var o=t(this),r=o.closest(".woocommerce-mini-cart-item");a.preventDefault(),r.block({message:null,overlayCSS:{opacity:.6}}),t.post(wc_add_to_cart_params.wc_ajax_url.toString().replace("%%endpoint%%","remove_from_cart"),{cart_item_key:o.data("cart_item_key")},function(a){a&&a.fragments?t(document.body).trigger("removed_from_cart",[a.fragments,a.cart_hash]):window.location=o.attr("href")}).fail(function(){window.location=o.attr("href")})},a.prototype.updateButton=function(a,o,r,e){(e=void 0!==e&&e)&&(e.removeClass("loading"),e.addClass("added"),wc_add_to_cart_params.is_cart||0!==e.parent().find(".added_to_cart").length||e.after(' <a href="'+wc_add_to_cart_params.cart_url+'" class="added_to_cart wc-forward" title="'+wc_add_to_cart_params.i18n_view_cart+'">'+wc_add_to_cart_params.i18n_view_cart+"</a>"),t(document.body).trigger("wc_cart_button_updated",[e]))},a.prototype.updateCartPage=function(){var a=window.location.toString().replace("add-to-cart","added-to-cart");t(".shop_table.cart").load(a+" .shop_table.cart:eq(0) > *",function(){t(".shop_table.cart").stop(!0).css("opacity","1").unblock(),t(document.body).trigger("cart_page_refreshed")}),t(".cart_totals").load(a+" .cart_totals:eq(0) > *",function(){t(".cart_totals").stop(!0).css("opacity","1").unblock(),t(document.body).trigger("cart_totals_refreshed")})},a.prototype.updateFragments=function(a,o){o&&(t.each(o,function(a){t(a).addClass("updating").fadeTo("400","0.6").block({message:null,overlayCSS:{opacity:.6}})}),t.each(o,function(a,o){t(a).replaceWith(o),t(a).stop(!0).css("opacity","1").unblock()}),t(document.body).trigger("wc_fragments_loaded"))},new a});!function(){"use strict";function e(e){function t(t,n){var s,h,k=t==window,y=n&&n.message!==undefined?n.message:undefined;if(!(n=e.extend({},e.blockUI.defaults,n||{})).ignoreIfBlocked||!e(t).data("blockUI.isBlocked")){if(n.overlayCSS=e.extend({},e.blockUI.defaults.overlayCSS,n.overlayCSS||{}),s=e.extend({},e.blockUI.defaults.css,n.css||{}),n.onOverlayClick&&(n.overlayCSS.cursor="pointer"),h=e.extend({},e.blockUI.defaults.themedCSS,n.themedCSS||{}),y=y===undefined?n.message:y,k&&p&&o(window,{fadeOut:0}),y&&"string"!=typeof y&&(y.parentNode||y.jquery)){var m=y.jquery?y[0]:y,g={};e(t).data("blockUI.history",g),g.el=m,g.parent=m.parentNode,g.display=m.style.display,g.position=m.style.position,g.parent&&g.parent.removeChild(m)}e(t).data("blockUI.onUnblock",n.onUnblock);var v,I,w,U,x=n.baseZ;v=e(r||n.forceIframe?'<iframe class="blockUI" style="z-index:'+x+++';display:none;border:none;margin:0;padding:0;position:absolute;width:100%;height:100%;top:0;left:0" src="'+n.iframeSrc+'"></iframe>':'<div class="blockUI" style="display:none"></div>'),I=e(n.theme?'<div class="blockUI blockOverlay ui-widget-overlay" style="z-index:'+x+++';display:none"></div>':'<div class="blockUI blockOverlay" style="z-index:'+x+++';display:none;border:none;margin:0;padding:0;width:100%;height:100%;top:0;left:0"></div>'),n.theme&&k?(U='<div class="blockUI '+n.blockMsgClass+' blockPage ui-dialog ui-widget ui-corner-all" style="z-index:'+(x+10)+';display:none;position:fixed">',n.title&&(U+='<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">'+(n.title||"&nbsp;")+"</div>"),U+='<div class="ui-widget-content ui-dialog-content"></div>',U+="</div>"):n.theme?(U='<div class="blockUI '+n.blockMsgClass+' blockElement ui-dialog ui-widget ui-corner-all" style="z-index:'+(x+10)+';display:none;position:absolute">',n.title&&(U+='<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">'+(n.title||"&nbsp;")+"</div>"),U+='<div class="ui-widget-content ui-dialog-content"></div>',U+="</div>"):U=k?'<div class="blockUI '+n.blockMsgClass+' blockPage" style="z-index:'+(x+10)+';display:none;position:fixed"></div>':'<div class="blockUI '+n.blockMsgClass+' blockElement" style="z-index:'+(x+10)+';display:none;position:absolute"></div>',w=e(U),y&&(n.theme?(w.css(h),w.addClass("ui-widget-content")):w.css(s)),n.theme||I.css(n.overlayCSS),I.css("position",k?"fixed":"absolute"),(r||n.forceIframe)&&v.css("opacity",0);var C=[v,I,w],S=e(k?"body":t);e.each(C,function(){this.appendTo(S)}),n.theme&&n.draggable&&e.fn.draggable&&w.draggable({handle:".ui-dialog-titlebar",cancel:"li"});var O=f&&(!e.support.boxModel||e("object,embed",k?null:t).length>0);if(u||O){if(k&&n.allowBodyStretch&&e.support.boxModel&&e("html,body").css("height","100%"),(u||!e.support.boxModel)&&!k)var E=a(t,"borderTopWidth"),T=a(t,"borderLeftWidth"),M=E?"(0 - "+E+")":0,B=T?"(0 - "+T+")":0;e.each(C,function(e,t){var o=t[0].style;if(o.position="absolute",e<2)k?o.setExpression("height","Math.max(document.body.scrollHeight, document.body.offsetHeight) - (jQuery.support.boxModel?0:"+n.quirksmodeOffsetHack+') + "px"'):o.setExpression("height",'this.parentNode.offsetHeight + "px"'),k?o.setExpression("width",'jQuery.support.boxModel && document.documentElement.clientWidth || document.body.clientWidth + "px"'):o.setExpression("width",'this.parentNode.offsetWidth + "px"'),B&&o.setExpression("left",B),M&&o.setExpression("top",M);else if(n.centerY)k&&o.setExpression("top",'(document.documentElement.clientHeight || document.body.clientHeight) / 2 - (this.offsetHeight / 2) + (blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "px"'),o.marginTop=0;else if(!n.centerY&&k){var i="((document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "+(n.css&&n.css.top?parseInt(n.css.top,10):0)+') + "px"';o.setExpression("top",i)}})}if(y&&(n.theme?w.find(".ui-widget-content").append(y):w.append(y),(y.jquery||y.nodeType)&&e(y).show()),(r||n.forceIframe)&&n.showOverlay&&v.show(),n.fadeIn){var j=n.onBlock?n.onBlock:c,H=n.showOverlay&&!y?j:c,z=y?j:c;n.showOverlay&&I._fadeIn(n.fadeIn,H),y&&w._fadeIn(n.fadeIn,z)}else n.showOverlay&&I.show(),y&&w.show(),n.onBlock&&n.onBlock.bind(w)();if(i(1,t,n),k?(p=w[0],b=e(n.focusableElements,p),n.focusInput&&setTimeout(l,20)):d(w[0],n.centerX,n.centerY),n.timeout){var W=setTimeout(function(){k?e.unblockUI(n):e(t).unblock(n)},n.timeout);e(t).data("blockUI.timeout",W)}}}function o(t,o){var s,l=t==window,d=e(t),a=d.data("blockUI.history"),c=d.data("blockUI.timeout");c&&(clearTimeout(c),d.removeData("blockUI.timeout")),o=e.extend({},e.blockUI.defaults,o||{}),i(0,t,o),null===o.onUnblock&&(o.onUnblock=d.data("blockUI.onUnblock"),d.removeData("blockUI.onUnblock"));var r;r=l?e(document.body).children().filter(".blockUI").add("body > .blockUI"):d.find(">.blockUI"),o.cursorReset&&(r.length>1&&(r[1].style.cursor=o.cursorReset),r.length>2&&(r[2].style.cursor=o.cursorReset)),l&&(p=b=null),o.fadeOut?(s=r.length,r.stop().fadeOut(o.fadeOut,function(){0==--s&&n(r,a,o,t)})):n(r,a,o,t)}function n(t,o,n,i){var s=e(i);if(!s.data("blockUI.isBlocked")){t.each(function(e,t){this.parentNode&&this.parentNode.removeChild(this)}),o&&o.el&&(o.el.style.display=o.display,o.el.style.position=o.position,o.el.style.cursor="default",o.parent&&o.parent.appendChild(o.el),s.removeData("blockUI.history")),s.data("blockUI.static")&&s.css("position","static"),"function"==typeof n.onUnblock&&n.onUnblock(i,n);var l=e(document.body),d=l.width(),a=l[0].style.width;l.width(d-1).width(d),l[0].style.width=a}}function i(t,o,n){var i=o==window,l=e(o);if((t||(!i||p)&&(i||l.data("blockUI.isBlocked")))&&(l.data("blockUI.isBlocked",t),i&&n.bindEvents&&(!t||n.showOverlay))){var d="mousedown mouseup keydown keypress keyup touchstart touchend touchmove";t?e(document).bind(d,n,s):e(document).unbind(d,s)}}function s(t){if("keydown"===t.type&&t.keyCode&&9==t.keyCode&&p&&t.data.constrainTabKey){var o=b,n=!t.shiftKey&&t.target===o[o.length-1],i=t.shiftKey&&t.target===o[0];if(n||i)return setTimeout(function(){l(i)},10),!1}var s=t.data,d=e(t.target);return d.hasClass("blockOverlay")&&s.onOverlayClick&&s.onOverlayClick(t),d.parents("div."+s.blockMsgClass).length>0||0===d.parents().children().filter("div.blockUI").length}function l(e){if(b){var t=b[!0===e?b.length-1:0];t&&t.focus()}}function d(e,t,o){var n=e.parentNode,i=e.style,s=(n.offsetWidth-e.offsetWidth)/2-a(n,"borderLeftWidth"),l=(n.offsetHeight-e.offsetHeight)/2-a(n,"borderTopWidth");t&&(i.left=s>0?s+"px":"0"),o&&(i.top=l>0?l+"px":"0")}function a(t,o){return parseInt(e.css(t,o),10)||0}e.fn._fadeIn=e.fn.fadeIn;var c=e.noop||function(){},r=/MSIE/.test(navigator.userAgent),u=/MSIE 6.0/.test(navigator.userAgent)&&!/MSIE 8.0/.test(navigator.userAgent),f=(document.documentMode,e.isFunction(document.createElement("div").style.setExpression));e.blockUI=function(e){t(window,e)},e.unblockUI=function(e){o(window,e)},e.growlUI=function(t,o,n,i){var s=e('<div class="growlUI"></div>');t&&s.append("<h1>"+t+"</h1>"),o&&s.append("<h2>"+o+"</h2>"),n===undefined&&(n=3e3);var l=function(t){t=t||{},e.blockUI({message:s,fadeIn:"undefined"!=typeof t.fadeIn?t.fadeIn:700,fadeOut:"undefined"!=typeof t.fadeOut?t.fadeOut:1e3,timeout:"undefined"!=typeof t.timeout?t.timeout:n,centerY:!1,showOverlay:!1,onUnblock:i,css:e.blockUI.defaults.growlCSS})};l();s.css("opacity");s.mouseover(function(){l({fadeIn:0,timeout:3e4});var t=e(".blockMsg");t.stop(),t.fadeTo(300,1)}).mouseout(function(){e(".blockMsg").fadeOut(1e3)})},e.fn.block=function(o){if(this[0]===window)return e.blockUI(o),this;var n=e.extend({},e.blockUI.defaults,o||{});return this.each(function(){var t=e(this);n.ignoreIfBlocked&&t.data("blockUI.isBlocked")||t.unblock({fadeOut:0})}),this.each(function(){"static"==e.css(this,"position")&&(this.style.position="relative",e(this).data("blockUI.static",!0)),this.style.zoom=1,t(this,o)})},e.fn.unblock=function(t){return this[0]===window?(e.unblockUI(t),this):this.each(function(){o(this,t)})},e.blockUI.version=2.7,e.blockUI.defaults={message:"<h1>Please wait...</h1>",title:null,draggable:!0,theme:!1,css:{padding:0,margin:0,width:"30%",top:"40%",left:"35%",textAlign:"center",color:"#000",border:"3px solid #aaa",backgroundColor:"#fff",cursor:"wait"},themedCSS:{width:"30%",top:"40%",left:"35%"},overlayCSS:{backgroundColor:"#000",opacity:.6,cursor:"wait"},cursorReset:"default",growlCSS:{width:"350px",top:"10px",left:"",right:"10px",border:"none",padding:"5px",opacity:.6,cursor:"default",color:"#fff",backgroundColor:"#000","-webkit-border-radius":"10px","-moz-border-radius":"10px","border-radius":"10px"},iframeSrc:/^https/i.test(window.location.href||"")?"javascript:false":"about:blank",forceIframe:!1,baseZ:1e3,centerX:!0,centerY:!0,allowBodyStretch:!0,bindEvents:!0,constrainTabKey:!0,fadeIn:200,fadeOut:400,timeout:0,showOverlay:!0,focusInput:!0,focusableElements:":input:enabled:visible",onBlock:null,onUnblock:null,onOverlayClick:null,quirksmodeOffsetHack:4,blockMsgClass:"blockMsg",ignoreIfBlocked:!1};var p=null,b=[]}"function"==typeof define&&define.amd&&define.amd.jQuery?define(["jquery"],e):e(jQuery)}();!function(e){var n=!1;if("function"==typeof define&&define.amd&&(define(e),n=!0),"object"==typeof exports&&(module.exports=e(),n=!0),!n){var o=window.Cookies,t=window.Cookies=e();t.noConflict=function(){return window.Cookies=o,t}}}(function(){function e(){for(var e=0,n={};e<arguments.length;e++){var o=arguments[e];for(var t in o)n[t]=o[t]}return n}function n(o){function t(n,r,i){var c;if("undefined"!=typeof document){if(arguments.length>1){if("number"==typeof(i=e({path:"/"},t.defaults,i)).expires){var a=new Date;a.setMilliseconds(a.getMilliseconds()+864e5*i.expires),i.expires=a}i.expires=i.expires?i.expires.toUTCString():"";try{c=JSON.stringify(r),/^[\{\[]/.test(c)&&(r=c)}catch(m){}r=o.write?o.write(r,n):encodeURIComponent(String(r)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g,decodeURIComponent),n=(n=(n=encodeURIComponent(String(n))).replace(/%(23|24|26|2B|5E|60|7C)/g,decodeURIComponent)).replace(/[\(\)]/g,escape);var f="";for(var s in i)i[s]&&(f+="; "+s,!0!==i[s]&&(f+="="+i[s]));return document.cookie=n+"="+r+f}n||(c={});for(var p=document.cookie?document.cookie.split("; "):[],d=/(%[0-9A-Z]{2})+/g,u=0;u<p.length;u++){var l=p[u].split("="),C=l.slice(1).join("=");'"'===C.charAt(0)&&(C=C.slice(1,-1));try{var g=l[0].replace(d,decodeURIComponent);if(C=o.read?o.read(C,g):o(C,g)||C.replace(d,decodeURIComponent),this.json)try{C=JSON.parse(C)}catch(m){}if(n===g){c=C;break}n||(c[g]=C)}catch(m){}}return c}}return t.set=t,t.get=function(e){return t.call(t,e)},t.getJSON=function(){return t.apply({json:!0},[].slice.call(arguments))},t.defaults={},t.remove=function(n,o){t(n,"",e(o,{expires:-1}))},t.withConverter=n,t}return n(function(){})});jQuery(function(o){o(".woocommerce-ordering").on("change","select.orderby",function(){o(this).closest("form").submit()}),o("input.qty:not(.product-quantity input.qty)").each(function(){var e=parseFloat(o(this).attr("min"));e>=0&&parseFloat(o(this).val())<e&&o(this).val(e)}),jQuery(".woocommerce-store-notice__dismiss-link").click(function(){Cookies.set("store_notice","hidden",{path:"/"}),jQuery(".woocommerce-store-notice").hide()}),"hidden"===Cookies.get("store_notice")?jQuery(".woocommerce-store-notice").hide():jQuery(".woocommerce-store-notice").show()});jQuery(function(e){function t(){o&&sessionStorage.setItem("wc_cart_created",(new Date).getTime())}function n(e){o&&(localStorage.setItem(a,e),sessionStorage.setItem(a,e))}function r(){e.ajax(s)}if("undefined"==typeof wc_cart_fragments_params)return!1;var o,a=wc_cart_fragments_params.ajax_url.toString()+"-wc_cart_hash";try{o="sessionStorage"in window&&null!==window.sessionStorage,window.sessionStorage.setItem("wc","test"),window.sessionStorage.removeItem("wc"),window.localStorage.setItem("wc","test"),window.localStorage.removeItem("wc")}catch(w){o=!1}var s={url:wc_cart_fragments_params.wc_ajax_url.toString().replace("%%endpoint%%","get_refreshed_fragments"),type:"POST",success:function(r){r&&r.fragments&&(e.each(r.fragments,function(t,n){e(t).replaceWith(n)}),o&&(sessionStorage.setItem(wc_cart_fragments_params.fragment_name,JSON.stringify(r.fragments)),n(r.cart_hash),r.cart_hash&&t()),e(document.body).trigger("wc_fragments_refreshed"))}};if(o){var i=null;e(document.body).on("wc_fragment_refresh updated_wc_div",function(){r()}),e(document.body).on("added_to_cart",function(e,r,o){var s=sessionStorage.getItem(a);null!==s&&s!==undefined&&""!==s||t(),sessionStorage.setItem(wc_cart_fragments_params.fragment_name,JSON.stringify(r)),n(o)}),e(document.body).on("wc_fragments_refreshed",function(){clearTimeout(i),i=setTimeout(r,864e5)}),e(window).on("storage onstorage",function(e){a===e.originalEvent.key&&localStorage.getItem(a)!==sessionStorage.getItem(a)&&r()}),e(window).on("pageshow",function(t){t.originalEvent.persisted&&(e(".widget_shopping_cart_content").empty(),e(document.body).trigger("wc_fragment_refresh"))});try{var c=e.parseJSON(sessionStorage.getItem(wc_cart_fragments_params.fragment_name)),_=sessionStorage.getItem(a),g=Cookies.get("woocommerce_cart_hash"),m=sessionStorage.getItem("wc_cart_created");if(null!==_&&_!==undefined&&""!==_||(_=""),null!==g&&g!==undefined&&""!==g||(g=""),_&&(null===m||m===undefined||""===m))throw"No cart_created";if(m){var d=1*m+864e5,f=(new Date).getTime();if(d<f)throw"Fragment expired";i=setTimeout(r,d-f)}if(!c||!c["div.widget_shopping_cart_content"]||_!==g)throw"No fragment";e.each(c,function(t,n){e(t).replaceWith(n)}),e(document.body).trigger("wc_fragments_loaded")}catch(w){r()}}else r();Cookies.get("woocommerce_items_in_cart")>0?e(".hide_cart_widget_if_empty").closest(".widget_shopping_cart").show():e(".hide_cart_widget_if_empty").closest(".widget_shopping_cart").hide(),e(document.body).on("adding_to_cart",function(){e(".hide_cart_widget_if_empty").closest(".widget_shopping_cart").show()})});(function(c){c.fn.hoverIntent=function(a,d,e){var f={interval:100,sensitivity:7,timeout:0};"object"==typeof a?f=c.extend(f,a):c.isFunction(d)?f=c.extend(f,{over:a,out:d,selector:e}):f=c.extend(f,{over:a,out:a,selector:d});var p,h,n,l,q=function(a){p=a.pageX;h=a.pageY},t=function(a,e){e.hoverIntent_t=clearTimeout(e.hoverIntent_t);if(Math.abs(n-p)+Math.abs(l-h)<f.sensitivity)return c(e).off("mousemove.hoverIntent",q),e.hoverIntent_s=1,f.over.apply(e,[a]);n=p;l=h;e.hoverIntent_t=setTimeout(function(){t(a,
e)},f.interval)};a=function(a){var e=jQuery.extend({},a),d=this;d.hoverIntent_t&&(d.hoverIntent_t=clearTimeout(d.hoverIntent_t));"mouseenter"==a.type?(n=e.pageX,l=e.pageY,c(d).on("mousemove.hoverIntent",q),1!=d.hoverIntent_s&&(d.hoverIntent_t=setTimeout(function(){t(e,d)},f.interval))):(c(d).off("mousemove.hoverIntent",q),1==d.hoverIntent_s&&(d.hoverIntent_t=setTimeout(function(){d.hoverIntent_t=clearTimeout(d.hoverIntent_t);d.hoverIntent_s=0;f.out.apply(d,[e])},f.timeout)))};return this.on({"mouseenter.hoverIntent":a,
"mouseleave.hoverIntent":a},f.selector)}})(jQuery);
(function(c){c.fn.superfish=function(a){var e=c.fn.superfish,d=e.c,p=c('<span class="'+d.arrowClass+'"> &#187;</span>'),h=function(a){a=c(this);var e=l(a);clearTimeout(e.sfTimer);a.showSuperfishUl().siblings().hideSuperfishUl()},n=function(){var a=c(this),d=l(a),f=e.op;clearTimeout(d.sfTimer);d.sfTimer=setTimeout(function(){f.retainPath=-1<c.inArray(a[0],f.$path);a.hideSuperfishUl();f.$path.length&&1>a.parents("li."+f.hoverClass).length&&(f.onIdle.call(this),h.call(f.$path))},f.delay)},l=function(a){a.hasClass(d.menuClass)&&
c.error("Superfish requires you to update to a version of hoverIntent that supports event-delegation, such as this one: https://github.com/joeldbirch/onHoverIntent");a=a.closest("."+d.menuClass)[0];e.op=e.o[a.serial];return a},q=function(a){c.fn.hoverIntent&&!e.op.disableHI?a.hoverIntent(h,n,"li:has(ul)"):(a.on("mouseenter","li:has(ul)",h),a.on("mouseleave","li:has(ul)",n));a.on("focusin","li:has(ul)",h);a.on("focusout","li:has(ul)",n)};return this.addClass(d.menuClass).each(function(){var f=this.serial=
e.o.length,h=c.extend({},e.defaults,a),r=c(this);h.$path=r.find("li."+h.pathClass).slice(0,h.pathLevels).each(function(){c(this).addClass(h.hoverClass+" "+d.bcClass).filter("li:has(ul)").removeClass(h.pathClass)});e.o[f]=e.op=h;q(r);r.find("li:has(ul)").each(function(){h.autoArrows&&c(">a:first-child",this).addClass(d.anchorClass).append(p.clone())}).not("."+d.bcClass).hideSuperfishUl();h.onInit.call(this)})};var a=c.fn.superfish;a.o=[];a.op={};a.c={bcClass:"sf-breadcrumb",menuClass:"sf-js-enabled",
anchorClass:"sf-with-ul",arrowClass:"sf-sub-indicator"};a.defaults={hoverClass:"sfHover",pathClass:"overideThisToUse",pathLevels:1,delay:800,animation:{opacity:"show"},speed:"normal",autoArrows:!0,disableHI:!1,onInit:function(){},onBeforeShow:function(){},onShow:function(){},onHide:function(){},onIdle:function(){}};c.fn.extend({hideSuperfishUl:function(){var d=a.op,e=!0===d.retainPath?d.$path:"";d.retainPath=!1;e=c("li."+d.hoverClass,this).add(this).not(e).removeClass(d.hoverClass).find(">ul").hide().css("visibility",
"hidden");d.onHide.call(e);return this},showSuperfishUl:function(){var c=a.op,e=this.addClass(c.hoverClass).find(">ul:hidden").css("visibility","visible");c.onBeforeShow.call(e);e.animate(c.animation,c.speed,function(){c.onShow.call(e)});return this}})})(jQuery);
+function(c){function a(e,d){var f,h=c.proxy(this.process,this);this.$element=c(e).is("body")?c(window):c(e);this.$body=c("body");this.$scrollElement=this.$element.on("scroll.bs.scroll-spy.data-api",h);this.options=c.extend({},a.DEFAULTS,d);this.selector=(this.options.target||(f=c(e).attr("href"))&&f.replace(/.*(?=#[^\s]+$)/,"")||"")+" .x-nav li > a";this.offsets=c([]);this.targets=c([]);this.activeTarget=null;this.refresh();this.process()}a.DEFAULTS={offset:10};a.prototype.refresh=function(){var a=
this.$element[0]==window?"offset":"position";this.offsets=c([]);this.targets=c([]);var d=this;this.$body.find(this.selector).map(function(){var e=c(this),e=e.data("target")||e.attr("href"),f=/^#\w/.test(e)&&c(e);return f&&f.length&&[[f[a]().top+(!c.isWindow(d.$scrollElement.get(0))&&d.$scrollElement.scrollTop()),e]]||null}).sort(function(a,c){return a[0]-c[0]}).each(function(){d.offsets.push(this[0]);d.targets.push(this[1])})};a.prototype.process=function(){var a=this.$scrollElement.scrollTop()+this.options.offset,
c=(this.$scrollElement[0].scrollHeight||this.$body[0].scrollHeight)-this.$scrollElement.height(),d=this.offsets,h=this.targets,n=this.activeTarget,l;if(a>=c)return n!=(l=h.last()[0])&&this.activate(l);for(l=d.length;l--;)n!=h[l]&&a>=d[l]&&(!d[l+1]||a<=d[l+1])&&this.activate(h[l])};a.prototype.activate=function(a){this.activeTarget=a;c(this.selector).parents(".current-menu-item").removeClass("current-menu-item");a=c(this.selector+'[data-target="'+a+'"],'+this.selector+'[href="'+a+'"]').parents("li").addClass("current-menu-item");
a.parent(".dropdown-menu").length&&(a=a.closest("li.dropdown").addClass("current-menu-item"));a.trigger("activate.bs.scrollspy")};var d=c.fn.scrollspy;c.fn.scrollspy=function(d){return this.each(function(){var e=c(this),p=e.data("bs.scrollspy"),h="object"==typeof d&&d;p||e.data("bs.scrollspy",p=new a(this,h));"string"==typeof d&&p[d]()})};c.fn.scrollspy.Constructor=a;c.fn.scrollspy.noConflict=function(){c.fn.scrollspy=d;return this};c(window).on("load",function(){c('[data-spy="scroll"]').each(function(){var a=
c(this);a.scrollspy(a.data())})})}(jQuery);
jQuery(document).ready(function(c){var a=c("body"),d=a.outerHeight(),e=c("#wpadminbar").outerHeight(),f=c(".x-navbar").outerHeight();c('.x-nav-scrollspy > li > a[href^="#"]').click(function(a){a.preventDefault();a=c(this).attr("href");c("html, body").animate({scrollTop:c(a).offset().top-e-f+1},850,"easeInOutExpo")});a.scrollspy({target:".x-nav-collapse",offset:e+f});c(window).resize(function(){a.scrollspy("refresh")});var p=0,h=setInterval(function(){p+=1;a.outerHeight()!==d&&a.scrollspy("refresh");
10===p&&clearInterval(h)},500)});
(function(c,a,d){var e=c.document.documentElement,f=c.Modernizr,p=function(b){return b.charAt(0).toUpperCase()+b.slice(1)},h=["Moz","Webkit","O","Ms"],n=function(b){var a=e.style,g;if("string"==typeof a[b])return b;b=p(b);for(var k=0,c=h.length;k<c;k++)if(g=h[k]+b,"string"==typeof a[g])return g},l=n("transform"),q=n("transitionProperty");d={csstransforms:function(){return!!l},csstransforms3d:function(){var b=!!n("perspective");if(b&&"webkitPerspective"in e.style){var m=a("<style>@media (transform-3d),(-webkit-transform-3d){#modernizr{height:3px}}</style>").appendTo("head"),
g=a('<div id="modernizr" />').appendTo("html"),b=3===g.height();g.remove();m.remove()}return b},csstransitions:function(){return!!q}};var t;if(f)for(t in d)f.hasOwnProperty(t)||f.addTest(t,d[t]);else{var f=c.Modernizr={_version:"1.6ish: miniModernizr for Isotope"},u=" ",r;for(t in d)r=d[t](),f[t]=r,u+=" "+(r?"":"no-")+t;a("html").addClass(u)}if(f.csstransforms){var w=f.csstransforms3d?{translate:function(b){return"translate3d("+b[0]+"px, "+b[1]+"px, 0) "},scale:function(b){return"scale3d("+b+", "+
b+", 1) "}}:{translate:function(b){return"translate("+b[0]+"px, "+b[1]+"px) "},scale:function(b){return"scale("+b+") "}},v=function(b,m,g){var k=a.data(b,"isoTransform")||{},c={},d,e={};c[m]=g;a.extend(k,c);for(d in k)m=k[d],e[d]=w[d](m);d=(e.translate||"")+(e.scale||"");a.data(b,"isoTransform",k);b.style[l]=d};a.cssNumber.scale=!0;a.cssHooks.scale={set:function(b,a){v(b,"scale",a)},get:function(b,m){var g=a.data(b,"isoTransform");return g&&g.scale?g.scale:1}};a.fx.step.scale=function(b){a.cssHooks.scale.set(b.elem,
b.now+b.unit)};a.cssNumber.translate=!0;a.cssHooks.translate={set:function(b,a){v(b,"translate",a)},get:function(b,m){var g=a.data(b,"isoTransform");return g&&g.translate?g.translate:[0,0]}}}var y,A;f.csstransitions&&(y={WebkitTransitionProperty:"webkitTransitionEnd",MozTransitionProperty:"transitionend",OTransitionProperty:"oTransitionEnd otransitionend",transitionProperty:"transitionend"}[q],A=n("transitionDuration"));var x=a.event,C=a.event.handle?"handle":"dispatch",z;x.special.smartresize={setup:function(){a(this).bind("resize",
x.special.smartresize.handler)},teardown:function(){a(this).unbind("resize",x.special.smartresize.handler)},handler:function(b,a){var g=this,k=arguments;b.type="smartresize";z&&clearTimeout(z);z=setTimeout(function(){x[C].apply(g,k)},"execAsap"===a?0:100)}};a.fn.smartresize=function(b){return b?this.bind("smartresize",b):this.trigger("smartresize",["execAsap"])};a.Isotope=function(b,m,g){this.element=a(m);this._create(b);this._init(g)};var D=["width","height"],B=a(c);a.Isotope.settings={resizable:!0,
layoutMode:"masonry",containerClass:"isotope",itemClass:"isotope-item",hiddenClass:"isotope-hidden",hiddenStyle:{opacity:0,scale:.001},visibleStyle:{opacity:1,scale:1},containerStyle:{position:"relative",overflow:"hidden"},animationEngine:"best-available",animationOptions:{queue:!1,duration:800},sortBy:"original-order",sortAscending:!0,resizesContainer:!0,transformsEnabled:!0,itemPositionDataEnabled:!1};a.Isotope.prototype={_create:function(b){this.options=a.extend({},a.Isotope.settings,b);this.styleQueue=
[];this.elemCount=0;b=this.element[0].style;this.originalStyle={};var m=D.slice(0),g;for(g in this.options.containerStyle)m.push(g);for(var k=0,c=m.length;k<c;k++)g=m[k],this.originalStyle[g]=b[g]||"";this.element.css(this.options.containerStyle);this._updateAnimationEngine();this._updateUsingTransforms();this.options.getSortData=a.extend(this.options.getSortData,{"original-order":function(b,a){a.elemCount++;return a.elemCount},random:function(){return Math.random()}});this.reloadItems();this.offset=
{left:parseInt(this.element.css("padding-left")||0,10),top:parseInt(this.element.css("padding-top")||0,10)};var d=this;setTimeout(function(){d.element.addClass(d.options.containerClass)},0);this.options.resizable&&B.bind("smartresize.isotope",function(){d.resize()});this.element.delegate("."+this.options.hiddenClass,"click",function(){return!1})},_getAtoms:function(b){var a=this.options.itemSelector;b=a?b.filter(a).add(b.find(a)):b;a={position:"absolute"};b=b.filter(function(b,a){return 1===a.nodeType});
this.usingTransforms&&(a.left=0,a.top=0);b.css(a).addClass(this.options.itemClass);this.updateSortData(b,!0);return b},_init:function(b){this.$filteredAtoms=this._filter(this.$allAtoms);this._sort();this.reLayout(b)},option:function(b){if(a.isPlainObject(b)){this.options=a.extend(!0,this.options,b);for(var m in b)b="_update"+p(m),this[b]&&this[b]()}},_updateAnimationEngine:function(){var b;switch(this.options.animationEngine.toLowerCase().replace(/[ _\-]/g,"")){case "css":case "none":b=!1;break;case "jquery":b=
!0;break;default:b=!f.csstransitions}this.isUsingJQueryAnimation=b;this._updateUsingTransforms()},_updateTransformsEnabled:function(){this._updateUsingTransforms()},_updateUsingTransforms:function(){var b=this.usingTransforms=this.options.transformsEnabled&&f.csstransforms&&f.csstransitions&&!this.isUsingJQueryAnimation;b||(delete this.options.hiddenStyle.scale,delete this.options.visibleStyle.scale);this.getPositionStyles=b?this._translate:this._positionAbs},_filter:function(b){var a=""===this.options.filter?
"*":this.options.filter;if(!a)return b;var g=this.options.hiddenClass,k="."+g,c=b.filter(k),d=c;"*"!==a&&(d=c.filter(a),k=b.not(k).not(a).addClass(g),this.styleQueue.push({$el:k,style:this.options.hiddenStyle}));this.styleQueue.push({$el:d,style:this.options.visibleStyle});d.removeClass(g);return b.filter(a)},updateSortData:function(b,m){var g=this,k=this.options.getSortData,c,d;b.each(function(){c=a(this);d={};for(var b in k)m||"original-order"!==b?d[b]=k[b](c,g):d[b]=a.data(this,"isotope-sort-data")[b];
a.data(this,"isotope-sort-data",d)})},_sort:function(){var b=this.options.sortBy,a=this._getSorter,g=this.options.sortAscending?1:-1;this.$filteredAtoms.sort(function(k,c){var d=a(k,b),m=a(c,b);d===m&&"original-order"!==b&&(d=a(k,"original-order"),m=a(c,"original-order"));return(d>m?1:d<m?-1:0)*g})},_getSorter:function(b,c){return a.data(b,"isotope-sort-data")[c]},_translate:function(b,a){return{translate:[b,a]}},_positionAbs:function(b,a){return{left:b,top:a}},_pushPosition:function(a,c,g){c=Math.round(c+
this.offset.left);g=Math.round(g+this.offset.top);var b=this.getPositionStyles(c,g);this.styleQueue.push({$el:a,style:b});this.options.itemPositionDataEnabled&&a.data("isotope-item-position",{x:c,y:g})},layout:function(a,c){var b=this.options.layoutMode;this["_"+b+"Layout"](a);this.options.resizesContainer&&(b=this["_"+b+"GetContainerSize"](),this.styleQueue.push({$el:this.element,style:b}));this._processStyleQueue(a,c);this.isLaidOut=!0},_processStyleQueue:function(b,c){var g=this.isLaidOut?this.isUsingJQueryAnimation?
"animate":"css":"css",k=this.options.animationOptions,d=this.options.onLayout,m,e,h,r;e=function(a,b){b.$el[g](b.style,k)};if(this._isInserting&&this.isUsingJQueryAnimation)e=function(a,b){m=b.$el.hasClass("no-transition")?"css":g;b.$el[m](b.style,k)};else if(c||d||k.complete){var w=!1,n=[c,d,k.complete],v=this;h=!0;r=function(){if(!w){for(var a,g=0,c=n.length;g<c;g++)a=n[g],"function"==typeof a&&a.call(v.element,b,v);w=!0}};if(this.isUsingJQueryAnimation&&"animate"===g)k.complete=r,h=!1;else if(f.csstransitions){for(var d=
0,l=this.styleQueue[0],l=l&&l.$el;!l||!l.length;){l=this.styleQueue[d++];if(!l)return;l=l.$el}0<parseFloat(getComputedStyle(l[0])[A])&&(e=function(a,b){b.$el[g](b.style,k).one(y,r)},h=!1)}}a.each(this.styleQueue,e);h&&r();this.styleQueue=[]},resize:function(){this["_"+this.options.layoutMode+"ResizeChanged"]()&&this.reLayout()},reLayout:function(a){this["_"+this.options.layoutMode+"Reset"]();this.layout(this.$filteredAtoms,a)},addItems:function(a,c){var b=this._getAtoms(a);this.$allAtoms=this.$allAtoms.add(b);
c&&c(b)},insert:function(a,c){this.element.append(a);var b=this;this.addItems(a,function(a){a=b._filter(a);b._addHideAppended(a);b._sort();b.reLayout();b._revealAppended(a,c)})},appended:function(a,c){var b=this;this.addItems(a,function(a){b._addHideAppended(a);b.layout(a);b._revealAppended(a,c)})},_addHideAppended:function(a){this.$filteredAtoms=this.$filteredAtoms.add(a);a.addClass("no-transition");this._isInserting=!0;this.styleQueue.push({$el:a,style:this.options.hiddenStyle})},_revealAppended:function(a,
c){var b=this;setTimeout(function(){a.removeClass("no-transition");b.styleQueue.push({$el:a,style:b.options.visibleStyle});b._isInserting=!1;b._processStyleQueue(a,c)},10)},reloadItems:function(){this.$allAtoms=this._getAtoms(this.element.children())},remove:function(a,c){this.$allAtoms=this.$allAtoms.not(a);this.$filteredAtoms=this.$filteredAtoms.not(a);var b=this,d=function(){a.remove();c&&c.call(b.element)};a.filter(":not(."+this.options.hiddenClass+")").length?(this.styleQueue.push({$el:a,style:this.options.hiddenStyle}),
this._sort(),this.reLayout(d)):d()},shuffle:function(a){this.updateSortData(this.$allAtoms);this.options.sortBy="random";this._sort();this.reLayout(a)},destroy:function(){var a=this.usingTransforms,c=this.options;this.$allAtoms.removeClass(c.hiddenClass+" "+c.itemClass).each(function(){var b=this.style;b.position="";b.top="";b.left="";b.opacity="";a&&(b[l]="")});var d=this.element[0].style,k;for(k in this.originalStyle)d[k]=this.originalStyle[k];this.element.unbind(".isotope").undelegate("."+c.hiddenClass,
"click").removeClass(c.containerClass).removeData("isotope");B.unbind(".isotope")},_getSegments:function(a){var b=this.options.layoutMode,c=a?"rowHeight":"columnWidth",d=a?"height":"width";a=a?"rows":"cols";var e=this.element[d](),d=this.options[b]&&this.options[b][c]||this.$filteredAtoms["outer"+p(d)](!0)||e,e=Math.floor(e/d),e=Math.max(e,1);this[b][a]=e;this[b][c]=d},_checkIfSegmentsChanged:function(a){var b=this.options.layoutMode,c=a?"rows":"cols",d=this[b][c];this._getSegments(a);return this[b][c]!==
d},_masonryReset:function(){this.masonry={};this._getSegments();var a=this.masonry.cols;for(this.masonry.colYs=[];a--;)this.masonry.colYs.push(0)},_masonryLayout:function(b){if("undefined"==typeof b)return!1;var c=this,d=c.masonry;b.each(function(){var b=a(this),g=Math.ceil(b.outerWidth(!0)/d.columnWidth),g=Math.min(g,d.cols);if(1===g)c._masonryPlaceBrick(b,d.colYs);else{var e=d.cols+1-g,m=[],f,h;for(h=0;h<e;h++)f=d.colYs.slice(h,h+g),m[h]=Math.max.apply(Math,f);c._masonryPlaceBrick(b,m)}})},_masonryPlaceBrick:function(a,
c){for(var b=Math.min.apply(Math,c),d=0,e=0,m=c.length;e<m;e++)if(c[e]===b){d=e;break}this._pushPosition(a,this.masonry.columnWidth*d,b);b+=a.outerHeight(!0);m=this.masonry.cols+1-m;for(e=0;e<m;e++)this.masonry.colYs[d+e]=b},_masonryGetContainerSize:function(){return{height:Math.max.apply(Math,this.masonry.colYs)}},_masonryResizeChanged:function(){return this._checkIfSegmentsChanged()},_fitRowsReset:function(){this.fitRows={x:0,y:0,height:0}},_fitRowsLayout:function(b){var c=this,d=this.element.width(),
k=this.fitRows;b.each(function(){var b=a(this),g=b.outerWidth(!0),e=b.outerHeight(!0);0!==k.x&&g+k.x>d&&(k.x=0,k.y=k.height);c._pushPosition(b,k.x,k.y);k.height=Math.max(k.y+e,k.height);k.x+=g})},_fitRowsGetContainerSize:function(){return{height:this.fitRows.height}},_fitRowsResizeChanged:function(){return!0},_cellsByRowReset:function(){this.cellsByRow={index:0};this._getSegments();this._getSegments(!0)},_cellsByRowLayout:function(b){var c=this,d=this.cellsByRow;b.each(function(){var b=a(this),g=
Math.floor(d.index/d.cols),e=(d.index%d.cols+.5)*d.columnWidth-b.outerWidth(!0)/2,g=(g+.5)*d.rowHeight-b.outerHeight(!0)/2;c._pushPosition(b,e,g);d.index++})},_cellsByRowGetContainerSize:function(){return{height:Math.ceil(this.$filteredAtoms.length/this.cellsByRow.cols)*this.cellsByRow.rowHeight+this.offset.top}},_cellsByRowResizeChanged:function(){return this._checkIfSegmentsChanged()},_straightDownReset:function(){this.straightDown={y:0}},_straightDownLayout:function(b){var c=this;b.each(function(b){b=
a(this);c._pushPosition(b,0,c.straightDown.y);c.straightDown.y+=b.outerHeight(!0)})},_straightDownGetContainerSize:function(){return{height:this.straightDown.y}},_straightDownResizeChanged:function(){return!0},_masonryHorizontalReset:function(){this.masonryHorizontal={};this._getSegments(!0);var a=this.masonryHorizontal.rows;for(this.masonryHorizontal.rowXs=[];a--;)this.masonryHorizontal.rowXs.push(0)},_masonryHorizontalLayout:function(b){var c=this,d=c.masonryHorizontal;b.each(function(){var b=a(this),
e=Math.ceil(b.outerHeight(!0)/d.rowHeight),e=Math.min(e,d.rows);if(1===e)c._masonryHorizontalPlaceBrick(b,d.rowXs);else{var g=d.rows+1-e,f=[],m,h;for(h=0;h<g;h++)m=d.rowXs.slice(h,h+e),f[h]=Math.max.apply(Math,m);c._masonryHorizontalPlaceBrick(b,f)}})},_masonryHorizontalPlaceBrick:function(a,c){for(var b=Math.min.apply(Math,c),d=0,e=0,f=c.length;e<f;e++)if(c[e]===b){d=e;break}this._pushPosition(a,b,this.masonryHorizontal.rowHeight*d);b+=a.outerWidth(!0);f=this.masonryHorizontal.rows+1-f;for(e=0;e<
f;e++)this.masonryHorizontal.rowXs[d+e]=b},_masonryHorizontalGetContainerSize:function(){return{width:Math.max.apply(Math,this.masonryHorizontal.rowXs)}},_masonryHorizontalResizeChanged:function(){return this._checkIfSegmentsChanged(!0)},_fitColumnsReset:function(){this.fitColumns={x:0,y:0,width:0}},_fitColumnsLayout:function(b){var c=this,d=this.element.height(),e=this.fitColumns;b.each(function(){var b=a(this),g=b.outerWidth(!0),k=b.outerHeight(!0);0!==e.y&&k+e.y>d&&(e.x=e.width,e.y=0);c._pushPosition(b,
e.x,e.y);e.width=Math.max(e.x+g,e.width);e.y+=k})},_fitColumnsGetContainerSize:function(){return{width:this.fitColumns.width}},_fitColumnsResizeChanged:function(){return!0},_cellsByColumnReset:function(){this.cellsByColumn={index:0};this._getSegments();this._getSegments(!0)},_cellsByColumnLayout:function(b){var c=this,d=this.cellsByColumn;b.each(function(){var b=a(this),e=d.index%d.rows,g=(Math.floor(d.index/d.rows)+.5)*d.columnWidth-b.outerWidth(!0)/2,e=(e+.5)*d.rowHeight-b.outerHeight(!0)/2;c._pushPosition(b,
g,e);d.index++})},_cellsByColumnGetContainerSize:function(){return{width:Math.ceil(this.$filteredAtoms.length/this.cellsByColumn.rows)*this.cellsByColumn.columnWidth}},_cellsByColumnResizeChanged:function(){return this._checkIfSegmentsChanged(!0)},_straightAcrossReset:function(){this.straightAcross={x:0}},_straightAcrossLayout:function(b){var c=this;b.each(function(b){b=a(this);c._pushPosition(b,c.straightAcross.x,0);c.straightAcross.x+=b.outerWidth(!0)})},_straightAcrossGetContainerSize:function(){return{width:this.straightAcross.x}},
_straightAcrossResizeChanged:function(){return!0}};a.fn.imagesLoaded=function(b){function c(){b.call(e,f)}function d(b){b=b.target;b.src!==r&&-1===a.inArray(b,l)&&(l.push(b),0>=--h&&(setTimeout(c),f.unbind(".imagesLoaded",d)))}var e=this,f=e.find("img").add(e.filter("img")),h=f.length,r="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==",l=[];h||c();f.bind("load.imagesLoaded error.imagesLoaded",d).each(function(){var a=this.src;this.src=r;this.src=a});return e};a.fn.isotope=function(b,
d){if("string"==typeof b){var e=Array.prototype.slice.call(arguments,1);this.each(function(){var d=a.data(this,"isotope");d?a.isFunction(d[b])&&"_"!==b.charAt(0)?d[b].apply(d,e):c.console&&c.console.error("no such method '"+b+"' for isotope instance"):c.console&&c.console.error("cannot call methods on isotope prior to initialization; attempted to call method '"+b+"'")})}else this.each(function(){var c=a.data(this,"isotope");c?(c.option(b),c._init(d)):a.data(this,"isotope",new a.Isotope(b,this,d))});
return this}})(window,jQuery);
(function(c,a,d){function e(a){var c={},e=/^jQuery\d+$/;d.each(a.attributes,function(a,d){d.specified&&!e.test(d.name)&&(c[d.name]=d.value)});return c}function f(a,c){var e=d(this);if(this.value==e.attr("placeholder")&&e.hasClass("placeholder"))if(e.data("placeholder-password")){e=e.hide().next().show().attr("id",e.removeAttr("id").data("placeholder-id"));if(!0===a)return e[0].value=c;e.focus()}else this.value="",e.removeClass("placeholder"),this==h()&&this.select()}function p(){var a,c=d(this),h=
this.id;if(""==this.value){if("password"==this.type){if(!c.data("placeholder-textinput")){try{a=c.clone().attr({type:"text"})}catch(y){a=d("<input>").attr(d.extend(e(this),{type:"text"}))}a.removeAttr("name").data({"placeholder-password":c,"placeholder-id":h}).bind("focus.placeholder",f);c.data({"placeholder-textinput":a,"placeholder-id":h}).before(a)}c=c.removeAttr("id").hide().prev().attr("id",h).show()}c.addClass("placeholder");c[0].value=c.attr("placeholder")}else c.removeClass("placeholder")}
function h(){try{return a.activeElement}catch(r){}}var n="[object OperaMini]"==Object.prototype.toString.call(c.operamini),l="placeholder"in a.createElement("input")&&!n,n="placeholder"in a.createElement("textarea")&&!n,q=d.fn,t=d.valHooks,u=d.propHooks;l&&n?(q=q.placeholder=function(){return this},q.input=q.textarea=!0):(q=q.placeholder=function(){this.filter((l?"textarea":":input")+"[placeholder]").not(".placeholder").bind({"focus.placeholder":f,"blur.placeholder":p}).data("placeholder-enabled",
!0).trigger("blur.placeholder");return this},q.input=l,q.textarea=n,q={get:function(a){var c=d(a),e=c.data("placeholder-password");return e?e[0].value:c.data("placeholder-enabled")&&c.hasClass("placeholder")?"":a.value},set:function(a,c){var e=d(a),l=e.data("placeholder-password");if(l)return l[0].value=c;if(!e.data("placeholder-enabled"))return a.value=c;""==c?(a.value=c,a!=h()&&p.call(a)):e.hasClass("placeholder")?f.call(a,!0,c)||(a.value=c):a.value=c;return e}},l||(t.input=q,u.value=q),n||(t.textarea=
q,u.value=q),d(function(){d(a).delegate("form","submit.placeholder",function(){var a=d(".placeholder",this).each(f);setTimeout(function(){a.each(p)},10)})}),d(c).bind("beforeunload.placeholder",function(){d(".placeholder").each(function(){this.value=""})}))})(this,document,jQuery);
jQuery(document).ready(function(c){c(".sf-menu").superfish({delay:650,speed:"fast"});var a=c("body"),d=a.outerHeight(),e=c("#wpadminbar").outerHeight(),f=c(".x-navbar").outerHeight();a.scrollspy({target:".x-nav-collapse",offset:e+f});c('.x-nav-scrollspy > li > a[href^="#"]').click(function(a){a.preventDefault();a=c(this).attr("href");c("html, body").animate({scrollTop:c(a).offset().top-e-f+1},850,"easeInOutExpo")});c(window).resize(function(){a.scrollspy("refresh")});var p=0,h=setInterval(function(){p+=
1;a.outerHeight()!==d&&a.scrollspy("refresh");10===p&&clearInterval(h)},500);c("input, textarea").placeholder()});jQuery(document).ready(function(e){var t=e(window),n=t.height(),r=e(this);t.resize(function(){n=t.height()});e.fn.parallaxContentBand=function(r,i){function u(){var e=t.scrollTop();s.each(function(){var t=s.offset().top,u=s.outerHeight();if(t+u<e||t>e+n)return;s.css("background-position",r+" "+Math.floor((o-e)*i)+"px")})}var s=e(this),o;s.each(function(){o=s.offset().top});t.resize(function(){s.each(function(){o=s.offset().top})});t.bind("scroll",u).resize(u);u()};e('.x-column[data-fade="true"]').each(function(){e(this).waypoint(function(){e(this).data("fade-animation")==="in-from-top"?e(this).animate({opacity:"1",top:"0"},750,"easeOutExpo"):e(this).data("fade-animation")==="in-from-left"?e(this).animate({opacity:"1",left:"0"},750,"easeOutExpo"):e(this).data("fade-animation")==="in-from-right"?e(this).animate({opacity:"1",right:"0"},750,"easeOutExpo"):e(this).data("fade-animation")==="in-from-bottom"?e(this).animate({opacity:"1",bottom:"0"},750,"easeOutExpo"):e(this).animate({opacity:"1"},750,"easeOutExpo")},{offset:"65%",triggerOnce:!0})});e('.x-recent-posts[data-fade="true"]').each(function(){e(this).waypoint(function(){e(this).find("a").each(function(t){e(this).delay(t*90).animate({opacity:"1"},750,"easeOutExpo")});setTimeout(function(){e(this).addClass("complete")},e(this).find("a").length*90+400)},{offset:"75%",triggerOnce:!0})});e(".x-skill-bar").each(function(){e(this).waypoint(function(){var t=e(this).data("percentage");e(this).find(".bar").animate({width:t},750,"easeInOutExpo")},{offset:"95%",triggerOnce:!0})});e(".x-counter").each(function(){e(this).waypoint(function(){var t=e(this).data("num-end"),n=e(this).data("num-speed");e(this).find(".number").animateNumber({number:t},n)},{offset:"85%",triggerOnce:!0})});e(".x-accordion-toggle[data-parent]").click(function(){e(this).closest(".x-accordion").find(".x-accordion-toggle:not(.collapsed)").addClass("collapsed")})});jQuery(window).load(function(){if(Modernizr.touch)jQuery(".x-content-band.bg-image.parallax, .x-content-band.bg-pattern.parallax").css("background-attachment","scroll");else{jQuery(".x-content-band.bg-image.parallax").each(function(){var e=jQuery(this).attr("id");jQuery("#"+e+".parallax").parallaxContentBand("50%",.1)});jQuery(".x-content-band.bg-pattern.parallax").each(function(){var e=jQuery(this).attr("id");jQuery("#"+e+".parallax").parallaxContentBand("50%",.3)})}});jQuery.easing.jswing=jQuery.easing.swing;jQuery.extend(jQuery.easing,{def:"easeOutQuad",swing:function(e,t,n,r,i){return jQuery.easing[jQuery.easing.def](e,t,n,r,i)},easeInQuad:function(e,t,n,r,i){return r*(t/=i)*t+n},easeOutQuad:function(e,t,n,r,i){return-r*(t/=i)*(t-2)+n},easeInOutQuad:function(e,t,n,r,i){return(t/=i/2)<1?r/2*t*t+n:-r/2*(--t*(t-2)-1)+n},easeInCubic:function(e,t,n,r,i){return r*(t/=i)*t*t+n},easeOutCubic:function(e,t,n,r,i){return r*((t=t/i-1)*t*t+1)+n},easeInOutCubic:function(e,t,n,r,i){return(t/=i/2)<1?r/2*t*t*t+n:r/2*((t-=2)*t*t+2)+n},easeInQuart:function(e,t,n,r,i){return r*(t/=i)*t*t*t+n},easeOutQuart:function(e,t,n,r,i){return-r*((t=t/i-1)*t*t*t-1)+n},easeInOutQuart:function(e,t,n,r,i){return(t/=i/2)<1?r/2*t*t*t*t+n:-r/2*((t-=2)*t*t*t-2)+n},easeInQuint:function(e,t,n,r,i){return r*(t/=i)*t*t*t*t+n},easeOutQuint:function(e,t,n,r,i){return r*((t=t/i-1)*t*t*t*t+1)+n},easeInOutQuint:function(e,t,n,r,i){return(t/=i/2)<1?r/2*t*t*t*t*t+n:r/2*((t-=2)*t*t*t*t+2)+n},easeInSine:function(e,t,n,r,i){return-r*Math.cos(t/i*(Math.PI/2))+r+n},easeOutSine:function(e,t,n,r,i){return r*Math.sin(t/i*(Math.PI/2))+n},easeInOutSine:function(e,t,n,r,i){return-r/2*(Math.cos(Math.PI*t/i)-1)+n},easeInExpo:function(e,t,n,r,i){return t==0?n:r*Math.pow(2,10*(t/i-1))+n},easeOutExpo:function(e,t,n,r,i){return t==i?n+r:r*(-Math.pow(2,-10*t/i)+1)+n},easeInOutExpo:function(e,t,n,r,i){return t==0?n:t==i?n+r:(t/=i/2)<1?r/2*Math.pow(2,10*(t-1))+n:r/2*(-Math.pow(2,-10*--t)+2)+n},easeInCirc:function(e,t,n,r,i){return-r*(Math.sqrt(1-(t/=i)*t)-1)+n},easeOutCirc:function(e,t,n,r,i){return r*Math.sqrt(1-(t=t/i-1)*t)+n},easeInOutCirc:function(e,t,n,r,i){return(t/=i/2)<1?-r/2*(Math.sqrt(1-t*t)-1)+n:r/2*(Math.sqrt(1-(t-=2)*t)+1)+n},easeInElastic:function(e,t,n,r,i){var s=1.70158,o=0,u=r;if(t==0)return n;if((t/=i)==1)return n+r;o||(o=i*.3);if(u<Math.abs(r)){u=r;var s=o/4}else var s=o/(2*Math.PI)*Math.asin(r/u);return-(u*Math.pow(2,10*(t-=1))*Math.sin((t*i-s)*2*Math.PI/o))+n},easeOutElastic:function(e,t,n,r,i){var s=1.70158,o=0,u=r;if(t==0)return n;if((t/=i)==1)return n+r;o||(o=i*.3);if(u<Math.abs(r)){u=r;var s=o/4}else var s=o/(2*Math.PI)*Math.asin(r/u);return u*Math.pow(2,-10*t)*Math.sin((t*i-s)*2*Math.PI/o)+r+n},easeInOutElastic:function(e,t,n,r,i){var s=1.70158,o=0,u=r;if(t==0)return n;if((t/=i/2)==2)return n+r;o||(o=i*.3*1.5);if(u<Math.abs(r)){u=r;var s=o/4}else var s=o/(2*Math.PI)*Math.asin(r/u);return t<1?-0.5*u*Math.pow(2,10*(t-=1))*Math.sin((t*i-s)*2*Math.PI/o)+n:u*Math.pow(2,-10*(t-=1))*Math.sin((t*i-s)*2*Math.PI/o)*.5+r+n},easeInBack:function(e,t,n,r,i,s){s==undefined&&(s=1.70158);return r*(t/=i)*t*((s+1)*t-s)+n},easeOutBack:function(e,t,n,r,i,s){s==undefined&&(s=1.70158);return r*((t=t/i-1)*t*((s+1)*t+s)+1)+n},easeInOutBack:function(e,t,n,r,i,s){s==undefined&&(s=1.70158);return(t/=i/2)<1?r/2*t*t*(((s*=1.525)+1)*t-s)+n:r/2*((t-=2)*t*(((s*=1.525)+1)*t+s)+2)+n},easeInBounce:function(e,t,n,r,i){return r-jQuery.easing.easeOutBounce(e,i-t,0,r,i)+n},easeOutBounce:function(e,t,n,r,i){return(t/=i)<1/2.75?r*7.5625*t*t+n:t<2/2.75?r*(7.5625*(t-=1.5/2.75)*t+.75)+n:t<2.5/2.75?r*(7.5625*(t-=2.25/2.75)*t+.9375)+n:r*(7.5625*(t-=2.625/2.75)*t+.984375)+n},easeInOutBounce:function(e,t,n,r,i){return t<i/2?jQuery.easing.easeInBounce(e,t*2,0,r,i)*.5+n:jQuery.easing.easeOutBounce(e,t*2-i,0,r,i)*.5+r*.5+n}});(function(e){e.flexslider=function(t,n){var r=e(t);r.vars=e.extend({},e.flexslider.defaults,n);var i=r.vars.namespace,s=window.navigator&&window.navigator.msPointerEnabled&&window.MSGesture,o=("ontouchstart"in window||s||window.DocumentTouch&&document instanceof DocumentTouch)&&r.vars.touch,u="click touchend MSPointerUp",a="",f,l=r.vars.direction==="vertical",c=r.vars.reverse,h=r.vars.itemWidth>0,p=r.vars.animation==="fade",d=r.vars.asNavFor!=="",v={},m=!0;e.data(t,"flexslider",r);v={init:function(){r.animating=!1;r.currentSlide=parseInt(r.vars.startAt?r.vars.startAt:0,10);isNaN(r.currentSlide)&&(r.currentSlide=0);r.animatingTo=r.currentSlide;r.atEnd=r.currentSlide===0||r.currentSlide===r.last;r.containerSelector=r.vars.selector.substr(0,r.vars.selector.search(" "));r.slides=e(r.vars.selector,r);r.container=e(r.containerSelector,r);r.count=r.slides.length;r.syncExists=e(r.vars.sync).length>0;r.vars.animation==="slide"&&(r.vars.animation="swing");r.prop=l?"top":"marginLeft";r.args={};r.manualPause=!1;r.stopped=!1;r.started=!1;r.startTimeout=null;r.transitions=!r.vars.video&&!p&&r.vars.useCSS&&function(){var e=document.createElement("div"),t=["perspectiveProperty","WebkitPerspective","MozPerspective","OPerspective","msPerspective"];for(var n in t)if(e.style[t[n]]!==undefined){r.pfx=t[n].replace("Perspective","").toLowerCase();r.prop="-"+r.pfx+"-transform";return!0}return!1}();r.ensureAnimationEnd="";r.vars.controlsContainer!==""&&(r.controlsContainer=e(r.vars.controlsContainer).length>0&&e(r.vars.controlsContainer));r.vars.manualControls!==""&&(r.manualControls=e(r.vars.manualControls).length>0&&e(r.vars.manualControls));if(r.vars.randomize){r.slides.sort(function(){return Math.round(Math.random())-.5});r.container.empty().append(r.slides)}r.doMath();r.setup("init");r.vars.controlNav&&v.controlNav.setup();r.vars.directionNav&&v.directionNav.setup();r.vars.keyboard&&(e(r.containerSelector).length===1||r.vars.multipleKeyboard)&&e(document).bind("keyup",function(e){var t=e.keyCode;if(!r.animating&&(t===39||t===37)){var n=t===39?r.getTarget("next"):t===37?r.getTarget("prev"):!1;r.flexAnimate(n,r.vars.pauseOnAction)}});r.vars.mousewheel&&r.bind("mousewheel",function(e,t,n,i){e.preventDefault();var s=t<0?r.getTarget("next"):r.getTarget("prev");r.flexAnimate(s,r.vars.pauseOnAction)});r.vars.pausePlay&&v.pausePlay.setup();r.vars.slideshow&&r.vars.pauseInvisible&&v.pauseInvisible.init();if(r.vars.slideshow){r.vars.pauseOnHover&&r.hover(function(){!r.manualPlay&&!r.manualPause&&r.pause()},function(){!r.manualPause&&!r.manualPlay&&!r.stopped&&r.play()});if(!r.vars.pauseInvisible||!v.pauseInvisible.isHidden())r.vars.initDelay>0?r.startTimeout=setTimeout(r.play,r.vars.initDelay):r.play()}d&&v.asNav.setup();o&&r.vars.touch&&v.touch();(!p||p&&r.vars.smoothHeight)&&e(window).bind("resize orientationchange focus",v.resize);r.find("img").attr("draggable","false");setTimeout(function(){r.vars.start(r)},200)},asNav:{setup:function(){r.asNav=!0;r.animatingTo=Math.floor(r.currentSlide/r.move);r.currentItem=r.currentSlide;r.slides.removeClass(i+"active-slide").eq(r.currentItem).addClass(i+"active-slide");if(!s)r.slides.on(u,function(t){t.preventDefault();var n=e(this),s=n.index(),o=n.offset().left-e(r).scrollLeft();if(o<=0&&n.hasClass(i+"active-slide"))r.flexAnimate(r.getTarget("prev"),!0);else if(!e(r.vars.asNavFor).data("flexslider").animating&&!n.hasClass(i+"active-slide")){r.direction=r.currentItem<s?"next":"prev";r.flexAnimate(s,r.vars.pauseOnAction,!1,!0,!0)}});else{t._slider=r;r.slides.each(function(){var t=this;t._gesture=new MSGesture;t._gesture.target=t;t.addEventListener("MSPointerDown",function(e){e.preventDefault();e.currentTarget._gesture&&e.currentTarget._gesture.addPointer(e.pointerId)},!1);t.addEventListener("MSGestureTap",function(t){t.preventDefault();var n=e(this),i=n.index();if(!e(r.vars.asNavFor).data("flexslider").animating&&!n.hasClass("active")){r.direction=r.currentItem<i?"next":"prev";r.flexAnimate(i,r.vars.pauseOnAction,!1,!0,!0)}})})}}},controlNav:{setup:function(){r.manualControls?v.controlNav.setupManual():v.controlNav.setupPaging()},setupPaging:function(){var t=r.vars.controlNav==="thumbnails"?"control-thumbs":"control-paging",n=1,s,o;r.controlNavScaffold=e('<ol class="'+i+"control-nav "+i+t+'"></ol>');if(r.pagingCount>1)for(var f=0;f<r.pagingCount;f++){o=r.slides.eq(f);s=r.vars.controlNav==="thumbnails"?'<img src="'+o.attr("data-thumb")+'"/>':"<a>"+n+"</a>";if("thumbnails"===r.vars.controlNav&&!0===r.vars.thumbCaptions){var l=o.attr("data-thumbcaption");""!=l&&undefined!=l&&(s+='<span class="'+i+'caption">'+l+"</span>")}r.controlNavScaffold.append("<li>"+s+"</li>");n++}r.controlsContainer?e(r.controlsContainer).append(r.controlNavScaffold):r.append(r.controlNavScaffold);v.controlNav.set();v.controlNav.active();r.controlNavScaffold.delegate("a, img",u,function(t){t.preventDefault();if(a===""||a===t.type){var n=e(this),s=r.controlNav.index(n);if(!n.hasClass(i+"active")){r.direction=s>r.currentSlide?"next":"prev";r.flexAnimate(s,r.vars.pauseOnAction)}}a===""&&(a=t.type);v.setToClearWatchedEvent()})},setupManual:function(){r.controlNav=r.manualControls;v.controlNav.active();r.controlNav.bind(u,function(t){t.preventDefault();if(a===""||a===t.type){var n=e(this),s=r.controlNav.index(n);if(!n.hasClass(i+"active")){s>r.currentSlide?r.direction="next":r.direction="prev";r.flexAnimate(s,r.vars.pauseOnAction)}}a===""&&(a=t.type);v.setToClearWatchedEvent()})},set:function(){var t=r.vars.controlNav==="thumbnails"?"img":"a";r.controlNav=e("."+i+"control-nav li "+t,r.controlsContainer?r.controlsContainer:r)},active:function(){r.controlNav.removeClass(i+"active").eq(r.animatingTo).addClass(i+"active")},update:function(t,n){r.pagingCount>1&&t==="add"?r.controlNavScaffold.append(e("<li><a>"+r.count+"</a></li>")):r.pagingCount===1?r.controlNavScaffold.find("li").remove():r.controlNav.eq(n).closest("li").remove();v.controlNav.set();r.pagingCount>1&&r.pagingCount!==r.controlNav.length?r.update(n,t):v.controlNav.active()}},directionNav:{setup:function(){var t=e('<ul class="'+i+'direction-nav"><li><a class="'+i+'prev" href="#">'+r.vars.prevText+'</a></li><li><a class="'+i+'next" href="#">'+r.vars.nextText+"</a></li></ul>");if(r.controlsContainer){e(r.controlsContainer).append(t);r.directionNav=e("."+i+"direction-nav li a",r.controlsContainer)}else{r.append(t);r.directionNav=e("."+i+"direction-nav li a",r)}v.directionNav.update();r.directionNav.bind(u,function(t){t.preventDefault();var n;if(a===""||a===t.type){n=e(this).hasClass(i+"next")?r.getTarget("next"):r.getTarget("prev");r.flexAnimate(n,r.vars.pauseOnAction)}a===""&&(a=t.type);v.setToClearWatchedEvent()})},update:function(){var e=i+"disabled";r.pagingCount===1?r.directionNav.addClass(e).attr("tabindex","-1"):r.vars.animationLoop?r.directionNav.removeClass(e).removeAttr("tabindex"):r.animatingTo===0?r.directionNav.removeClass(e).filter("."+i+"prev").addClass(e).attr("tabindex","-1"):r.animatingTo===r.last?r.directionNav.removeClass(e).filter("."+i+"next").addClass(e).attr("tabindex","-1"):r.directionNav.removeClass(e).removeAttr("tabindex")}},pausePlay:{setup:function(){var t=e('<div class="'+i+'pauseplay"><a></a></div>');if(r.controlsContainer){r.controlsContainer.append(t);r.pausePlay=e("."+i+"pauseplay a",r.controlsContainer)}else{r.append(t);r.pausePlay=e("."+i+"pauseplay a",r)}v.pausePlay.update(r.vars.slideshow?i+"pause":i+"play");r.pausePlay.bind(u,function(t){t.preventDefault();if(a===""||a===t.type)if(e(this).hasClass(i+"pause")){r.manualPause=!0;r.manualPlay=!1;r.pause()}else{r.manualPause=!1;r.manualPlay=!0;r.play()}a===""&&(a=t.type);v.setToClearWatchedEvent()})},update:function(e){e==="play"?r.pausePlay.removeClass(i+"pause").addClass(i+"play").html(r.vars.playText):r.pausePlay.removeClass(i+"play").addClass(i+"pause").html(r.vars.pauseText)}},touch:function(){var e,n,i,o,u,a,f=!1,d=0,v=0,m=0;if(!s){t.addEventListener("touchstart",g,!1);function g(s){if(r.animating)s.preventDefault();else if(window.navigator.msPointerEnabled||s.touches.length===1){r.pause();o=l?r.h:r.w;a=Number(new Date);d=s.touches[0].pageX;v=s.touches[0].pageY;i=h&&c&&r.animatingTo===r.last?0:h&&c?r.limit-(r.itemW+r.vars.itemMargin)*r.move*r.animatingTo:h&&r.currentSlide===r.last?r.limit:h?(r.itemW+r.vars.itemMargin)*r.move*r.currentSlide:c?(r.last-r.currentSlide+r.cloneOffset)*o:(r.currentSlide+r.cloneOffset)*o;e=l?v:d;n=l?d:v;t.addEventListener("touchmove",y,!1);t.addEventListener("touchend",b,!1)}}function y(t){d=t.touches[0].pageX;v=t.touches[0].pageY;u=l?e-v:e-d;f=l?Math.abs(u)<Math.abs(d-n):Math.abs(u)<Math.abs(v-n);var s=500;if(!f||Number(new Date)-a>s){t.preventDefault();if(!p&&r.transitions){r.vars.animationLoop||(u/=r.currentSlide===0&&u<0||r.currentSlide===r.last&&u>0?Math.abs(u)/o+2:1);r.setProps(i+u,"setTouch")}}}function b(s){t.removeEventListener("touchmove",y,!1);if(r.animatingTo===r.currentSlide&&!f&&u!==null){var l=c?-u:u,h=l>0?r.getTarget("next"):r.getTarget("prev");r.canAdvance(h)&&(Number(new Date)-a<550&&Math.abs(l)>50||Math.abs(l)>o/2)?r.flexAnimate(h,r.vars.pauseOnAction):p||r.flexAnimate(r.currentSlide,r.vars.pauseOnAction,!0)}t.removeEventListener("touchend",b,!1);e=null;n=null;u=null;i=null}}else{t.style.msTouchAction="none";t._gesture=new MSGesture;t._gesture.target=t;t.addEventListener("MSPointerDown",w,!1);t._slider=r;t.addEventListener("MSGestureChange",E,!1);t.addEventListener("MSGestureEnd",S,!1);function w(e){e.stopPropagation();if(r.animating)e.preventDefault();else{r.pause();t._gesture.addPointer(e.pointerId);m=0;o=l?r.h:r.w;a=Number(new Date);i=h&&c&&r.animatingTo===r.last?0:h&&c?r.limit-(r.itemW+r.vars.itemMargin)*r.move*r.animatingTo:h&&r.currentSlide===r.last?r.limit:h?(r.itemW+r.vars.itemMargin)*r.move*r.currentSlide:c?(r.last-r.currentSlide+r.cloneOffset)*o:(r.currentSlide+r.cloneOffset)*o}}function E(e){e.stopPropagation();var n=e.target._slider;if(!n)return;var r=-e.translationX,s=-e.translationY;m+=l?s:r;u=m;f=l?Math.abs(m)<Math.abs(-r):Math.abs(m)<Math.abs(-s);if(e.detail===e.MSGESTURE_FLAG_INERTIA){setImmediate(function(){t._gesture.stop()});return}if(!f||Number(new Date)-a>500){e.preventDefault();if(!p&&n.transitions){n.vars.animationLoop||(u=m/(n.currentSlide===0&&m<0||n.currentSlide===n.last&&m>0?Math.abs(m)/o+2:1));n.setProps(i+u,"setTouch")}}}function S(t){t.stopPropagation();var r=t.target._slider;if(!r)return;if(r.animatingTo===r.currentSlide&&!f&&u!==null){var s=c?-u:u,l=s>0?r.getTarget("next"):r.getTarget("prev");r.canAdvance(l)&&(Number(new Date)-a<550&&Math.abs(s)>50||Math.abs(s)>o/2)?r.flexAnimate(l,r.vars.pauseOnAction):p||r.flexAnimate(r.currentSlide,r.vars.pauseOnAction,!0)}e=null;n=null;u=null;i=null;m=0}}},resize:function(){if(!r.animating&&r.is(":visible")){h||r.doMath();if(p)v.smoothHeight();else if(h){r.slides.width(r.computedW);r.update(r.pagingCount);r.setProps()}else if(l){r.viewport.height(r.h);r.setProps(r.h,"setTotal")}else{r.vars.smoothHeight&&v.smoothHeight();r.newSlides.width(r.computedW);r.setProps(r.computedW,"setTotal")}}},smoothHeight:function(e){if(!l||p){var t=p?r:r.viewport;e?t.animate({height:r.slides.eq(r.animatingTo).height()},e):t.height(r.slides.eq(r.animatingTo).height())}},sync:function(t){var n=e(r.vars.sync).data("flexslider"),i=r.animatingTo;switch(t){case"animate":n.flexAnimate(i,r.vars.pauseOnAction,!1,!0);break;case"play":!n.playing&&!n.asNav&&n.play();break;case"pause":n.pause()}},uniqueID:function(t){t.find("[id]").each(function(){var t=e(this);t.attr("id",t.attr("id")+"_clone")});return t},pauseInvisible:{visProp:null,init:function(){var e=["webkit","moz","ms","o"];if("hidden"in document)return"hidden";for(var t=0;t<e.length;t++)e[t]+"Hidden"in document&&(v.pauseInvisible.visProp=e[t]+"Hidden");if(v.pauseInvisible.visProp){var n=v.pauseInvisible.visProp.replace(/[H|h]idden/,"")+"visibilitychange";document.addEventListener(n,function(){v.pauseInvisible.isHidden()?r.startTimeout?clearTimeout(r.startTimeout):r.pause():r.started?r.play():r.vars.initDelay>0?setTimeout(r.play,r.vars.initDelay):r.play()})}},isHidden:function(){return document[v.pauseInvisible.visProp]||!1}},setToClearWatchedEvent:function(){clearTimeout(f);f=setTimeout(function(){a=""},3e3)}};r.flexAnimate=function(t,n,s,u,a){!r.vars.animationLoop&&t!==r.currentSlide&&(r.direction=t>r.currentSlide?"next":"prev");d&&r.pagingCount===1&&(r.direction=r.currentItem<t?"next":"prev");if(!r.animating&&(r.canAdvance(t,a)||s)&&r.is(":visible")){if(d&&u){var f=e(r.vars.asNavFor).data("flexslider");r.atEnd=t===0||t===r.count-1;f.flexAnimate(t,!0,!1,!0,a);r.direction=r.currentItem<t?"next":"prev";f.direction=r.direction;if(Math.ceil((t+1)/r.visible)-1===r.currentSlide||t===0){r.currentItem=t;r.slides.removeClass(i+"active-slide").eq(t).addClass(i+"active-slide");return!1}r.currentItem=t;r.slides.removeClass(i+"active-slide").eq(t).addClass(i+"active-slide");t=Math.floor(t/r.visible)}r.animating=!0;r.animatingTo=t;n&&r.pause();r.vars.before(r);r.syncExists&&!a&&v.sync("animate");r.vars.controlNav&&v.controlNav.active();h||r.slides.removeClass(i+"active-slide").eq(t).addClass(i+"active-slide");r.atEnd=t===0||t===r.last;r.vars.directionNav&&v.directionNav.update();if(t===r.last){r.vars.end(r);r.vars.animationLoop||r.pause()}if(!p){var m=l?r.slides.filter(":first").height():r.computedW,g,y,b;if(h){g=r.vars.itemMargin;b=(r.itemW+g)*r.move*r.animatingTo;y=b>r.limit&&r.visible!==1?r.limit:b}else r.currentSlide===0&&t===r.count-1&&r.vars.animationLoop&&r.direction!=="next"?y=c?(r.count+r.cloneOffset)*m:0:r.currentSlide===r.last&&t===0&&r.vars.animationLoop&&r.direction!=="prev"?y=c?0:(r.count+1)*m:y=c?(r.count-1-t+r.cloneOffset)*m:(t+r.cloneOffset)*m;r.setProps(y,"",r.vars.animationSpeed);if(r.transitions){if(!r.vars.animationLoop||!r.atEnd){r.animating=!1;r.currentSlide=r.animatingTo}r.container.unbind("webkitTransitionEnd transitionend");r.container.bind("webkitTransitionEnd transitionend",function(){clearTimeout(r.ensureAnimationEnd);r.wrapup(m)});clearTimeout(r.ensureAnimationEnd);r.ensureAnimationEnd=setTimeout(function(){r.wrapup(m)},r.vars.animationSpeed+100)}else r.container.animate(r.args,r.vars.animationSpeed,r.vars.easing,function(){r.wrapup(m)})}else if(!o){r.slides.eq(r.currentSlide).css({zIndex:1}).animate({opacity:0},r.vars.animationSpeed,r.vars.easing);r.slides.eq(t).css({zIndex:2}).animate({opacity:1},r.vars.animationSpeed,r.vars.easing,r.wrapup)}else{r.slides.eq(r.currentSlide).css({opacity:0,zIndex:1});r.slides.eq(t).css({opacity:1,zIndex:2});r.wrapup(m)}r.vars.smoothHeight&&v.smoothHeight(r.vars.animationSpeed)}};r.wrapup=function(e){!p&&!h&&(r.currentSlide===0&&r.animatingTo===r.last&&r.vars.animationLoop?r.setProps(e,"jumpEnd"):r.currentSlide===r.last&&r.animatingTo===0&&r.vars.animationLoop&&r.setProps(e,"jumpStart"));r.animating=!1;r.currentSlide=r.animatingTo;r.vars.after(r)};r.animateSlides=function(){!r.animating&&m&&r.flexAnimate(r.getTarget("next"))};r.pause=function(){clearInterval(r.animatedSlides);r.animatedSlides=null;r.playing=!1;r.vars.pausePlay&&v.pausePlay.update("play");r.syncExists&&v.sync("pause")};r.play=function(){r.playing&&clearInterval(r.animatedSlides);r.animatedSlides=r.animatedSlides||setInterval(r.animateSlides,r.vars.slideshowSpeed);r.started=r.playing=!0;r.vars.pausePlay&&v.pausePlay.update("pause");r.syncExists&&v.sync("play")};r.stop=function(){r.pause();r.stopped=!0};r.canAdvance=function(e,t){var n=d?r.pagingCount-1:r.last;return t?!0:d&&r.currentItem===r.count-1&&e===0&&r.direction==="prev"?!0:d&&r.currentItem===0&&e===r.pagingCount-1&&r.direction!=="next"?!1:e===r.currentSlide&&!d?!1:r.vars.animationLoop?!0:r.atEnd&&r.currentSlide===0&&e===n&&r.direction!=="next"?!1:r.atEnd&&r.currentSlide===n&&e===0&&r.direction==="next"?!1:!0};r.getTarget=function(e){r.direction=e;return e==="next"?r.currentSlide===r.last?0:r.currentSlide+1:r.currentSlide===0?r.last:r.currentSlide-1};r.setProps=function(e,t,n){var i=function(){var n=e?e:(r.itemW+r.vars.itemMargin)*r.move*r.animatingTo,i=function(){if(h)return t==="setTouch"?e:c&&r.animatingTo===r.last?0:c?r.limit-(r.itemW+r.vars.itemMargin)*r.move*r.animatingTo:r.animatingTo===r.last?r.limit:n;switch(t){case"setTotal":return c?(r.count-1-r.currentSlide+r.cloneOffset)*e:(r.currentSlide+r.cloneOffset)*e;case"setTouch":return c?e:e;case"jumpEnd":return c?e:r.count*e;case"jumpStart":return c?r.count*e:e;default:return e}}();return i*-1+"px"}();if(r.transitions){i=l?"translate3d(0,"+i+",0)":"translate3d("+i+",0,0)";n=n!==undefined?n/1e3+"s":"0s";r.container.css("-"+r.pfx+"-transition-duration",n);r.container.css("transition-duration",n)}r.args[r.prop]=i;(r.transitions||n===undefined)&&r.container.css(r.args);r.container.css("transform",i)};r.setup=function(t){if(!p){var n,s;if(t==="init"){r.viewport=e('<div class="'+i+'viewport"></div>').css({overflow:"hidden",position:"relative"}).appendTo(r).append(r.container);r.cloneCount=0;r.cloneOffset=0;if(c){s=e.makeArray(r.slides).reverse();r.slides=e(s);r.container.empty().append(r.slides)}}if(r.vars.animationLoop&&!h){r.cloneCount=2;r.cloneOffset=1;t!=="init"&&r.container.find(".clone").remove();v.uniqueID(r.slides.first().clone().addClass("clone").attr("aria-hidden","true")).appendTo(r.container);v.uniqueID(r.slides.last().clone().addClass("clone").attr("aria-hidden","true")).prependTo(r.container)}r.newSlides=e(r.vars.selector,r);n=c?r.count-1-r.currentSlide+r.cloneOffset:r.currentSlide+r.cloneOffset;if(l&&!h){r.container.height((r.count+r.cloneCount)*200+"%").css("position","absolute").width("100%");setTimeout(function(){r.newSlides.css({display:"block"});r.doMath();r.viewport.height(r.h);r.setProps(n*r.h,"init")},t==="init"?100:0)}else{r.container.width((r.count+r.cloneCount)*200+"%");r.setProps(n*r.computedW,"init");setTimeout(function(){r.doMath();r.newSlides.css({width:r.computedW,"float":"left",display:"block"});r.vars.smoothHeight&&v.smoothHeight()},t==="init"?100:0)}}else{r.slides.css({width:"100%","float":"left",marginRight:"-100%",position:"relative"});t==="init"&&(o?r.slides.css({opacity:0,display:"block",webkitTransition:"opacity "+r.vars.animationSpeed/1e3+"s ease",zIndex:1}).eq(r.currentSlide).css({opacity:1,zIndex:2}):r.slides.css({opacity:0,display:"block",zIndex:1}).eq(r.currentSlide).css({zIndex:2}).animate({opacity:1},r.vars.animationSpeed,r.vars.easing));r.vars.smoothHeight&&v.smoothHeight()}h||r.slides.removeClass(i+"active-slide").eq(r.currentSlide).addClass(i+"active-slide");r.vars.init(r)};r.doMath=function(){var e=r.slides.first(),t=r.vars.itemMargin,n=r.vars.minItems,i=r.vars.maxItems;r.w=r.viewport===undefined?r.width():r.viewport.width();r.h=e.height();r.boxPadding=e.outerWidth()-e.width();if(h){r.itemT=r.vars.itemWidth+t;r.minW=n?n*r.itemT:r.w;r.maxW=i?i*r.itemT-t:r.w;r.itemW=r.minW>r.w?(r.w-t*(n-1))/n:r.maxW<r.w?(r.w-t*(i-1))/i:r.vars.itemWidth>r.w?r.w:r.vars.itemWidth;r.visible=Math.floor(r.w/r.itemW);r.move=r.vars.move>0&&r.vars.move<r.visible?r.vars.move:r.visible;r.pagingCount=Math.ceil((r.count-r.visible)/r.move+1);r.last=r.pagingCount-1;r.limit=r.pagingCount===1?0:r.vars.itemWidth>r.w?r.itemW*(r.count-1)+t*(r.count-1):(r.itemW+t)*r.count-r.w-t}else{r.itemW=r.w;r.pagingCount=r.count;r.last=r.count-1}r.computedW=r.itemW-r.boxPadding};r.update=function(e,t){r.doMath();if(!h){e<r.currentSlide?r.currentSlide+=1:e<=r.currentSlide&&e!==0&&(r.currentSlide-=1);r.animatingTo=r.currentSlide}if(r.vars.controlNav&&!r.manualControls)if(t==="add"&&!h||r.pagingCount>r.controlNav.length)v.controlNav.update("add");else if(t==="remove"&&!h||r.pagingCount<r.controlNav.length){if(h&&r.currentSlide>r.last){r.currentSlide-=1;r.animatingTo-=1}v.controlNav.update("remove",r.last)}r.vars.directionNav&&v.directionNav.update()};r.addSlide=function(t,n){var i=e(t);r.count+=1;r.last=r.count-1;l&&c?n!==undefined?r.slides.eq(r.count-n).after(i):r.container.prepend(i):n!==undefined?r.slides.eq(n).before(i):r.container.append(i);r.update(n,"add");r.slides=e(r.vars.selector+":not(.clone)",r);r.setup();r.vars.added(r)};r.removeSlide=function(t){var n=isNaN(t)?r.slides.index(e(t)):t;r.count-=1;r.last=r.count-1;isNaN(t)?e(t,r.slides).remove():l&&c?r.slides.eq(r.last).remove():r.slides.eq(t).remove();r.doMath();r.update(n,"remove");r.slides=e(r.vars.selector+":not(.clone)",r);r.setup();r.vars.removed(r)};v.init()};e(window).blur(function(e){focused=!1}).focus(function(e){focused=!0});e.flexslider.defaults={namespace:"flex-",selector:".slides > li",animation:"fade",easing:"swing",direction:"horizontal",reverse:!1,animationLoop:!0,smoothHeight:!1,startAt:0,slideshow:!0,slideshowSpeed:7e3,animationSpeed:600,initDelay:0,randomize:!1,thumbCaptions:!1,pauseOnAction:!0,pauseOnHover:!1,pauseInvisible:!0,useCSS:!0,touch:!0,video:!1,controlNav:!0,directionNav:!0,prevText:"Previous",nextText:"Next",keyboard:!0,multipleKeyboard:!1,mousewheel:!1,pausePlay:!1,pauseText:"Pause",playText:"Play",controlsContainer:"",manualControls:"",sync:"",asNavFor:"",itemWidth:0,itemMargin:0,minItems:1,maxItems:0,move:0,allowOneSlide:!0,start:function(){},before:function(){},after:function(){},end:function(){},added:function(){},removed:function(){},init:function(){}};e.fn.flexslider=function(t){t===undefined&&(t={});if(typeof t=="object")return this.each(function(){var n=e(this),r=t.selector?t.selector:".slides > li",i=n.find(r);if(i.length===1&&t.allowOneSlide===!0||i.length===0){i.fadeIn(400);t.start&&t.start(n)}else n.data("flexslider")===undefined&&new e.flexslider(this,t)});var n=e(this).data("flexslider");switch(t){case"play":n.play();break;case"pause":n.pause();break;case"stop":n.stop();break;case"next":n.flexAnimate(n.getTarget("next"),!0);break;case"prev":case"previous":n.flexAnimate(n.getTarget("prev"),!0);break;default:typeof t=="number"&&n.flexAnimate(t,!0)}}})(jQuery);(function(){var e=[].indexOf||function(e){for(var t=0,n=this.length;t<n;t++)if(t in this&&this[t]===e)return t;return-1},t=[].slice;(function(e,t){return typeof define=="function"&&define.amd?define("waypoints",["jquery"],function(n){return t(n,e)}):t(e.jQuery,e)})(this,function(n,r){var i,s,o,u,a,f,l,c,h,p,d,v,m,g,y,b;i=n(r);c=e.call(r,"ontouchstart")>=0;u={horizontal:{},vertical:{}};a=1;l={};f="waypoints-context-id";d="resize.waypoints";v="scroll.waypoints";m=1;g="waypoints-waypoint-ids";y="waypoint";b="waypoints";s=function(){function e(e){var t=this;this.$element=e;this.element=e[0];this.didResize=!1;this.didScroll=!1;this.id="context"+a++;this.oldScroll={x:e.scrollLeft(),y:e.scrollTop()};this.waypoints={horizontal:{},vertical:{}};this.element[f]=this.id;l[this.id]=this;e.bind(v,function(){var e;if(!t.didScroll&&!c){t.didScroll=!0;e=function(){t.doScroll();return t.didScroll=!1};return r.setTimeout(e,n[b].settings.scrollThrottle)}});e.bind(d,function(){var e;if(!t.didResize){t.didResize=!0;e=function(){n[b]("refresh");return t.didResize=!1};return r.setTimeout(e,n[b].settings.resizeThrottle)}})}e.prototype.doScroll=function(){var e,t=this;e={horizontal:{newScroll:this.$element.scrollLeft(),oldScroll:this.oldScroll.x,forward:"right",backward:"left"},vertical:{newScroll:this.$element.scrollTop(),oldScroll:this.oldScroll.y,forward:"down",backward:"up"}};c&&(!e.vertical.oldScroll||!e.vertical.newScroll)&&n[b]("refresh");n.each(e,function(e,r){var i,s,o;o=[];s=r.newScroll>r.oldScroll;i=s?r.forward:r.backward;n.each(t.waypoints[e],function(e,t){var n,i;if(r.oldScroll<(n=t.offset)&&n<=r.newScroll)return o.push(t);if(r.newScroll<(i=t.offset)&&i<=r.oldScroll)return o.push(t)});o.sort(function(e,t){return e.offset-t.offset});s||o.reverse();return n.each(o,function(e,t){if(t.options.continuous||e===o.length-1)return t.trigger([i])})});return this.oldScroll={x:e.horizontal.newScroll,y:e.vertical.newScroll}};e.prototype.refresh=function(){var e,t,r,i=this;r=n.isWindow(this.element);t=this.$element.offset();this.doScroll();e={horizontal:{contextOffset:r?0:t.left,contextScroll:r?0:this.oldScroll.x,contextDimension:this.$element.width(),oldScroll:this.oldScroll.x,forward:"right",backward:"left",offsetProp:"left"},vertical:{contextOffset:r?0:t.top,contextScroll:r?0:this.oldScroll.y,contextDimension:r?n[b]("viewportHeight"):this.$element.height(),oldScroll:this.oldScroll.y,forward:"down",backward:"up",offsetProp:"top"}};return n.each(e,function(e,t){return n.each(i.waypoints[e],function(e,r){var i,s,o,u,a;i=r.options.offset;o=r.offset;s=n.isWindow(r.element)?0:r.$element.offset()[t.offsetProp];if(n.isFunction(i))i=i.apply(r.element);else if(typeof i=="string"){i=parseFloat(i);r.options.offset.indexOf("%")>-1&&(i=Math.ceil(t.contextDimension*i/100))}r.offset=s-t.contextOffset+t.contextScroll-i;if(r.options.onlyOnScroll&&o!=null||!r.enabled)return;if(o!==null&&o<(u=t.oldScroll)&&u<=r.offset)return r.trigger([t.backward]);if(o!==null&&o>(a=t.oldScroll)&&a>=r.offset)return r.trigger([t.forward]);if(o===null&&t.oldScroll>=r.offset)return r.trigger([t.forward])})})};e.prototype.checkEmpty=function(){if(n.isEmptyObject(this.waypoints.horizontal)&&n.isEmptyObject(this.waypoints.vertical)){this.$element.unbind([d,v].join(" "));return delete l[this.id]}};return e}();o=function(){function e(e,t,r){var i,s;r=n.extend({},n.fn[y].defaults,r);r.offset==="bottom-in-view"&&(r.offset=function(){var e;e=n[b]("viewportHeight");n.isWindow(t.element)||(e=t.$element.height());return e-n(this).outerHeight()});this.$element=e;this.element=e[0];this.axis=r.horizontal?"horizontal":"vertical";this.callback=r.handler;this.context=t;this.enabled=r.enabled;this.id="waypoints"+m++;this.offset=null;this.options=r;t.waypoints[this.axis][this.id]=this;u[this.axis][this.id]=this;i=(s=this.element[g])!=null?s:[];i.push(this.id);this.element[g]=i}e.prototype.trigger=function(e){if(!this.enabled)return;this.callback!=null&&this.callback.apply(this.element,e);if(this.options.triggerOnce)return this.destroy()};e.prototype.disable=function(){return this.enabled=!1};e.prototype.enable=function(){this.context.refresh();return this.enabled=!0};e.prototype.destroy=function(){delete u[this.axis][this.id];delete this.context.waypoints[this.axis][this.id];return this.context.checkEmpty()};e.getWaypointsByElement=function(e){var t,r;r=e[g];if(!r)return[];t=n.extend({},u.horizontal,u.vertical);return n.map(r,function(e){return t[e]})};return e}();p={init:function(e,t){var r;t==null&&(t={});(r=t.handler)==null&&(t.handler=e);this.each(function(){var e,r,i,u;e=n(this);i=(u=t.context)!=null?u:n.fn[y].defaults.context;n.isWindow(i)||(i=e.closest(i));i=n(i);r=l[i[0][f]];r||(r=new s(i));return new o(e,r,t)});n[b]("refresh");return this},disable:function(){return p._invoke.call(this,"disable")},enable:function(){return p._invoke.call(this,"enable")},destroy:function(){return p._invoke.call(this,"destroy")},prev:function(e,t){return p._traverse.call(this,e,t,function(e,t,n){if(t>0)return e.push(n[t-1])})},next:function(e,t){return p._traverse.call(this,e,t,function(e,t,n){if(t<n.length-1)return e.push(n[t+1])})},_traverse:function(e,t,i){var s,o;e==null&&(e="vertical");t==null&&(t=r);o=h.aggregate(t);s=[];this.each(function(){var t;t=n.inArray(this,o[e]);return i(s,t,
o[e])});return this.pushStack(s)},_invoke:function(e){this.each(function(){var t;t=o.getWaypointsByElement(this);return n.each(t,function(t,n){n[e]();return!0})});return this}};n.fn[y]=function(){var e,r;r=arguments[0],e=2<=arguments.length?t.call(arguments,1):[];return p[r]?p[r].apply(this,e):n.isFunction(r)?p.init.apply(this,arguments):n.isPlainObject(r)?p.init.apply(this,[null,r]):r?n.error("The "+r+" method does not exist in jQuery Waypoints."):n.error("jQuery Waypoints needs a callback function or handler option.")};n.fn[y].defaults={context:r,continuous:!0,enabled:!0,horizontal:!1,offset:0,triggerOnce:!1};h={refresh:function(){return n.each(l,function(e,t){return t.refresh()})},viewportHeight:function(){var e;return(e=r.innerHeight)!=null?e:i.height()},aggregate:function(e){var t,r,i;t=u;e&&(t=(i=l[n(e)[0][f]])!=null?i.waypoints:void 0);if(!t)return[];r={horizontal:[],vertical:[]};n.each(r,function(e,i){n.each(t[e],function(e,t){return i.push(t)});i.sort(function(e,t){return e.offset-t.offset});r[e]=n.map(i,function(e){return e.element});return r[e]=n.unique(r[e])});return r},above:function(e){e==null&&(e=r);return h._filter(e,"vertical",function(e,t){return t.offset<=e.oldScroll.y})},below:function(e){e==null&&(e=r);return h._filter(e,"vertical",function(e,t){return t.offset>e.oldScroll.y})},left:function(e){e==null&&(e=r);return h._filter(e,"horizontal",function(e,t){return t.offset<=e.oldScroll.x})},right:function(e){e==null&&(e=r);return h._filter(e,"horizontal",function(e,t){return t.offset>e.oldScroll.x})},enable:function(){return h._invoke("enable")},disable:function(){return h._invoke("disable")},destroy:function(){return h._invoke("destroy")},extendFn:function(e,t){return p[e]=t},_invoke:function(e){var t;t=n.extend({},u.vertical,u.horizontal);return n.each(t,function(t,n){n[e]();return!0})},_filter:function(e,t,r){var i,s;i=l[n(e)[0][f]];if(!i)return[];s=[];n.each(i.waypoints[t],function(e,t){if(r(i,t))return s.push(t)});s.sort(function(e,t){return e.offset-t.offset});return n.map(s,function(e){return e.element})}};n[b]=function(){var e,n;n=arguments[0],e=2<=arguments.length?t.call(arguments,1):[];return h[n]?h[n].apply(null,e):h.aggregate.call(null,n)};n[b].settings={resizeThrottle:100,scrollThrottle:30};return i.load(function(){return n[b]("refresh")})})}).call(this);(function(e){var t=function(e){return e.split("").reverse().join("")},n={numberStep:function(t,n){var r=Math.floor(t),i=e(n.elem);i.text(r)}},r=function(e){var t=e.elem;if(t.nodeType&&t.parentNode){var r=t._animateNumberSetter;r||(r=n.numberStep);r(e.now,e)}};!e.Tween||!e.Tween.propHooks?e.fx.step.number=r:e.Tween.propHooks.number={set:r};var i=function(e,t){var n=e.split("").reverse(),r=[],i,s,o;for(var u=0,a=Math.ceil(e.length/t);u<a;u++){i="";for(o=0;o<t;o++){s=u*t+o;if(s===e.length)break;i+=n[s]}r.push(i)}return r},s=function(e){var n=e.length-1,r=t(e[n]);e[n]=t(parseInt(r,10).toString());return e};e.animateNumber={numberStepFactories:{append:function(t){return function(n,r){var i=Math.floor(n),s=e(r.elem);s.prop("number",n).text(i+t)}},separator:function(n,r){n=n||" ";r=r||3;return function(o,u){var a=Math.floor(o),f=a.toString(),l=e(u.elem);if(f.length>r){var c=i(f,r);f=s(c).join(n);f=t(f)}l.prop("number",o).text(f)}}}};e.fn.animateNumber=function(){var t=arguments[0],r=e.extend({},n,t),i=e(this),s=[r];for(var o=1,u=arguments.length;o<u;o++)s.push(arguments[o]);if(t.numberStep){var a=this.each(function(){this._animateNumberSetter=t.numberStep}),f=r.complete;r.complete=function(){a.each(function(){delete this._animateNumberSetter});f&&f.apply(this,arguments)}}return i.animate.apply(i,s)}})(jQuery);+function(e){"use strict";function r(t){return this.each(function(){var r=e(this),i=r.data("bs.alert");i||r.data("bs.alert",i=new n(this));typeof t=="string"&&i[t].call(r)})}var t='[data-dismiss="alert"]',n=function(n){e(n).on("click",t,this.close)};n.VERSION="3.2.0";n.prototype.close=function(t){function s(){i.detach().trigger("closed.bs.alert").remove()}var n=e(this),r=n.attr("data-target");if(!r){r=n.attr("href");r=r&&r.replace(/.*(?=#[^\s]*$)/,"")}var i=e(r);t&&t.preventDefault();i.length||(i=n.hasClass("alert")?n:n.parent());i.trigger(t=e.Event("close.bs.alert"));if(t.isDefaultPrevented())return;i.removeClass("in");e.support.transition&&i.hasClass("fade")?i.one("bsTransitionEnd",s).emulateTransitionEnd(150):s()};var i=e.fn.alert;e.fn.alert=r;e.fn.alert.Constructor=n;e.fn.alert.noConflict=function(){e.fn.alert=i;return this};e(document).on("click.bs.alert.data-api",t,n.prototype.close)}(jQuery);+function(e){"use strict";function n(n){return this.each(function(){var r=e(this),i=r.data("bs.collapse"),s=e.extend({},t.DEFAULTS,r.data(),typeof n=="object"&&n);!i&&s.toggle&&n=="show"&&(n=!n);i||r.data("bs.collapse",i=new t(this,s));typeof n=="string"&&i[n]()})}var t=function(n,r){this.$element=e(n);this.options=e.extend({},t.DEFAULTS,r);this.transitioning=null;this.options.parent&&(this.$parent=e(this.options.parent));this.options.toggle&&this.toggle()};t.VERSION="3.2.0";t.DEFAULTS={toggle:!0};t.prototype.dimension=function(){var e=this.$element.hasClass("width");return e?"width":"height"};t.prototype.show=function(){if(this.transitioning||this.$element.hasClass("in"))return;var t=e.Event("show.bs.collapse");this.$element.trigger(t);if(t.isDefaultPrevented())return;var r=this.$parent&&this.$parent.find("> .x-accordion-group > .in");if(r&&r.length){var i=r.data("bs.collapse");if(i&&i.transitioning)return;n.call(r,"hide");i||r.data("bs.collapse",null)}var s=this.dimension();this.$element.removeClass("collapse").addClass("collapsing")[s](0);this.transitioning=1;var o=function(){this.$element.removeClass("collapsing").addClass("collapse in")[s]("");this.transitioning=0;this.$element.trigger("shown.bs.collapse")};if(!e.support.transition)return o.call(this);var u=e.camelCase(["scroll",s].join("-"));this.$element.one("bsTransitionEnd",e.proxy(o,this)).emulateTransitionEnd(350)[s](this.$element[0][u])};t.prototype.hide=function(){if(this.transitioning||!this.$element.hasClass("in"))return;var t=e.Event("hide.bs.collapse");this.$element.trigger(t);if(t.isDefaultPrevented())return;var n=this.dimension();this.$element[n](this.$element[n]())[0].offsetHeight;this.$element.addClass("collapsing").removeClass("collapse").removeClass("in");this.transitioning=1;var r=function(){this.transitioning=0;this.$element.trigger("hidden.bs.collapse").removeClass("collapsing").addClass("collapse")};if(!e.support.transition)return r.call(this);this.$element[n](0).one("bsTransitionEnd",e.proxy(r,this)).emulateTransitionEnd(350)};t.prototype.toggle=function(){this[this.$element.hasClass("in")?"hide":"show"]()};var r=e.fn.collapse;e.fn.collapse=n;e.fn.collapse.Constructor=t;e.fn.collapse.noConflict=function(){e.fn.collapse=r;return this};e(document).on("click.bs.collapse.data-api",'[data-toggle="collapse"]',function(t){var r,i=e(this),s=i.attr("data-target")||t.preventDefault()||(r=i.attr("href"))&&r.replace(/.*(?=#[^\s]+$)/,""),o=e(s),u=o.data("bs.collapse"),a=u?"toggle":i.data(),f=i.attr("data-parent"),l=f&&e(f);if(!u||!u.transitioning){l&&l.find('[data-toggle="collapse"][data-parent="'+f+'"]').not(i).addClass("collapsed");i[o.hasClass("in")?"addClass":"removeClass"]("collapsed")}n.call(o,a)})}(jQuery);+function(e){"use strict";function n(n){return this.each(function(){var r=e(this),i=r.data("bs.tab");i||r.data("bs.tab",i=new t(this));typeof n=="string"&&i[n]()})}var t=function(t){this.element=e(t)};t.VERSION="3.2.0";t.prototype.show=function(){var t=this.element,n=t.closest("ul:not(.dropdown-menu)"),r=t.data("target");if(!r){r=t.attr("href");r=r&&r.replace(/.*(?=#[^\s]*$)/,"")}if(t.parent("li").hasClass("active"))return;var i=n.find(".active:last a")[0],s=e.Event("show.bs.tab",{relatedTarget:i});t.trigger(s);if(s.isDefaultPrevented())return;var o=e(r);this.activate(t.closest("li"),n);this.activate(o,o.parent(),function(){t.trigger({type:"shown.bs.tab",relatedTarget:i})})};t.prototype.activate=function(t,n,r){function o(){i.removeClass("active").find("> .dropdown-menu > .active").removeClass("active");t.addClass("active");if(s){t[0].offsetWidth;t.addClass("in")}else t.removeClass("fade");t.parent(".dropdown-menu")&&t.closest("li.dropdown").addClass("active");r&&r()}var i=n.find("> .active"),s=r&&e.support.transition&&i.hasClass("fade");s?i.one("bsTransitionEnd",o).emulateTransitionEnd(150):o();i.removeClass("in")};var r=e.fn.tab;e.fn.tab=n;e.fn.tab.Constructor=t;e.fn.tab.noConflict=function(){e.fn.tab=r;return this};e(document).on("click.bs.tab.data-api",'[data-toggle="tab"], [data-toggle="pill"]',function(t){t.preventDefault();n.call(e(this),"show")})}(jQuery);+function(e){"use strict";function t(){var e=document.createElement("bootstrap"),t={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"};for(var n in t)if(e.style[n]!==undefined)return{end:t[n]};return!1}e.fn.emulateTransitionEnd=function(t){var n=!1,r=this;e(this).one("bsTransitionEnd",function(){n=!0});var i=function(){n||e(r).trigger(e.support.transition.end)};setTimeout(i,t);return this};e(function(){e.support.transition=t();if(!e.support.transition)return;e.event.special.bsTransitionEnd={bindType:e.support.transition.end,delegateType:e.support.transition.end,handle:function(t){if(e(t.target).is(this))return t.handleObj.handler.apply(this,arguments)}}})}(jQuery);+function(e){"use strict";function n(n){return this.each(function(){var r=e(this),i=r.data("bs.tooltip"),s=typeof n=="object"&&n;if(!i&&n=="destroy")return;i||r.data("bs.tooltip",i=new t(this,s));typeof n=="string"&&i[n]()})}var t=function(e,t){this.type=this.options=this.enabled=this.timeout=this.hoverState=this.$element=null;this.init("tooltip",e,t)};t.VERSION="3.2.0";t.DEFAULTS={animation:!0,placement:"top",selector:!1,template:'<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover focus",title:"",delay:0,html:!1,container:!1,viewport:{selector:"body",padding:0}};t.prototype.init=function(t,n,r){this.enabled=!0;this.type=t;this.$element=e(n);this.options=this.getOptions(r);this.$viewport=this.options.viewport&&e(this.options.viewport.selector||this.options.viewport);var i=this.options.trigger.split(" ");for(var s=i.length;s--;){var o=i[s];if(o=="click")this.$element.on("click."+this.type,this.options.selector,e.proxy(this.toggle,this));else if(o!="manual"){var u=o=="hover"?"mouseenter":"focusin",a=o=="hover"?"mouseleave":"focusout";this.$element.on(u+"."+this.type,this.options.selector,e.proxy(this.enter,this));this.$element.on(a+"."+this.type,this.options.selector,e.proxy(this.leave,this))}}this.options.selector?this._options=e.extend({},this.options,{trigger:"manual",selector:""}):this.fixTitle()};t.prototype.getDefaults=function(){return t.DEFAULTS};t.prototype.getOptions=function(t){t=e.extend({},this.getDefaults(),this.$element.data(),t);t.delay&&typeof t.delay=="number"&&(t.delay={show:t.delay,hide:t.delay});return t};t.prototype.getDelegateOptions=function(){var t={},n=this.getDefaults();this._options&&e.each(this._options,function(e,r){n[e]!=r&&(t[e]=r)});return t};t.prototype.enter=function(t){var n=t instanceof this.constructor?t:e(t.currentTarget).data("bs."+this.type);if(!n){n=new this.constructor(t.currentTarget,this.getDelegateOptions());e(t.currentTarget).data("bs."+this.type,n)}clearTimeout(n.timeout);n.hoverState="in";if(!n.options.delay||!n.options.delay.show)return n.show();n.timeout=setTimeout(function(){n.hoverState=="in"&&n.show()},n.options.delay.show)};t.prototype.leave=function(t){var n=t instanceof this.constructor?t:e(t.currentTarget).data("bs."+this.type);if(!n){n=new this.constructor(t.currentTarget,this.getDelegateOptions());e(t.currentTarget).data("bs."+this.type,n)}clearTimeout(n.timeout);n.hoverState="out";if(!n.options.delay||!n.options.delay.hide)return n.hide();n.timeout=setTimeout(function(){n.hoverState=="out"&&n.hide()},n.options.delay.hide)};t.prototype.show=function(){var t=e.Event("show.bs."+this.type);if(this.hasContent()&&this.enabled){this.$element.trigger(t);var n=e.contains(document.documentElement,this.$element[0]);if(t.isDefaultPrevented()||!n)return;var r=this,i=this.tip(),s=this.getUID(this.type);this.setContent();i.attr("id",s);this.$element.attr("aria-describedby",s);this.options.animation&&i.addClass("fade");var o=typeof this.options.placement=="function"?this.options.placement.call(this,i[0],this.$element[0]):this.options.placement,u=/\s?auto?\s?/i,a=u.test(o);a&&(o=o.replace(u,"")||"top");i.detach().css({top:0,left:0,display:"block"}).addClass(o).data("bs."+this.type,this);this.options.container?i.appendTo(this.options.container):i.insertAfter(this.$element);var f=this.getPosition(),l=i[0].offsetWidth,c=i[0].offsetHeight;if(a){var h=o,p=this.$element.parent(),d=this.getPosition(p);o=o=="bottom"&&f.top+f.height+c-d.scroll>d.height?"top":o=="top"&&f.top-d.scroll-c<0?"bottom":o=="right"&&f.right+l>d.width?"left":o=="left"&&f.left-l<d.left?"right":o;i.removeClass(h).addClass(o)}var v=this.getCalculatedOffset(o,f,l,c);this.applyPlacement(v,o);var m=function(){r.$element.trigger("shown.bs."+r.type);r.hoverState=null};e.support.transition&&this.$tip.hasClass("fade")?i.one("bsTransitionEnd",m).emulateTransitionEnd(150):m()}};t.prototype.applyPlacement=function(t,n){var r=this.tip(),i=r[0].offsetWidth,s=r[0].offsetHeight,o=parseInt(r.css("margin-top"),10),u=parseInt(r.css("margin-left"),10);isNaN(o)&&(o=0);isNaN(u)&&(u=0);t.top=t.top+o;t.left=t.left+u;e.offset.setOffset(r[0],e.extend({using:function(e){r.css({top:Math.round(e.top),left:Math.round(e.left)})}},t),0);r.addClass("in");var a=r[0].offsetWidth,f=r[0].offsetHeight;n=="top"&&f!=s&&(t.top=t.top+s-f);var l=this.getViewportAdjustedDelta(n,t,a,f);l.left?t.left+=l.left:t.top+=l.top;var c=l.left?l.left*2-i+a:l.top*2-s+f,h=l.left?"left":"top",p=l.left?"offsetWidth":"offsetHeight";r.offset(t);this.replaceArrow(c,r[0][p],h)};t.prototype.replaceArrow=function(e,t,n){this.arrow().css(n,e?50*(1-e/t)+"%":"")};t.prototype.setContent=function(){var e=this.tip(),t=this.getTitle();e.find(".tooltip-inner")[this.options.html?"html":"text"](t);e.removeClass("fade in top bottom left right")};t.prototype.hide=function(){function i(){t.hoverState!="in"&&n.detach();t.$element.trigger("hidden.bs."+t.type)}var t=this,n=this.tip(),r=e.Event("hide.bs."+this.type);this.$element.removeAttr("aria-describedby");this.$element.trigger(r);if(r.isDefaultPrevented())return;n.removeClass("in");e.support.transition&&this.$tip.hasClass("fade")?n.one("bsTransitionEnd",i).emulateTransitionEnd(150):i();this.hoverState=null;return this};t.prototype.fixTitle=function(){var e=this.$element;(e.attr("title")||typeof e.attr("data-original-title")!="string")&&e.attr("data-original-title",e.attr("title")||"").attr("title","")};t.prototype.hasContent=function(){return this.getTitle()};t.prototype.getPosition=function(t){t=t||this.$element;var n=t[0],r=n.tagName=="BODY";return e.extend({},typeof n.getBoundingClientRect=="function"?n.getBoundingClientRect():null,{scroll:r?document.documentElement.scrollTop||document.body.scrollTop:t.scrollTop(),width:r?e(window).width():t.outerWidth(),height:r?e(window).height():t.outerHeight()},r?{top:0,left:0}:t.offset())};t.prototype.getCalculatedOffset=function(e,t,n,r){return e=="bottom"?{top:t.top+t.height,left:t.left+t.width/2-n/2}:e=="top"?{top:t.top-r,left:t.left+t.width/2-n/2}:e=="left"?{top:t.top+t.height/2-r/2,left:t.left-n}:{top:t.top+t.height/2-r/2,left:t.left+t.width}};t.prototype.getViewportAdjustedDelta=function(e,t,n,r){var i={top:0,left:0};if(!this.$viewport)return i;var s=this.options.viewport&&this.options.viewport.padding||0,o=this.getPosition(this.$viewport);if(/right|left/.test(e)){var u=t.top-s-o.scroll,a=t.top+s-o.scroll+r;u<o.top?i.top=o.top-u:a>o.top+o.height&&(i.top=o.top+o.height-a)}else{var f=t.left-s,l=t.left+s+n;f<o.left?i.left=o.left-f:l>o.width&&(i.left=o.left+o.width-l)}return i};t.prototype.getTitle=function(){var e,t=this.$element,n=this.options;e=t.attr("data-original-title")||(typeof n.title=="function"?n.title.call(t[0]):n.title);return e};t.prototype.getUID=function(e){do e+=~~(Math.random()*1e6);while(document.getElementById(e));return e};t.prototype.tip=function(){return this.$tip=this.$tip||e(this.options.template)};t.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".tooltip-arrow")};t.prototype.validate=function(){if(!this.$element[0].parentNode){this.hide();this.$element=null;this.options=null}};t.prototype.enable=function(){this.enabled=!0};t.prototype.disable=function(){this.enabled=!1};t.prototype.toggleEnabled=function(){this.enabled=!this.enabled};t.prototype.toggle=function(t){var n=this;if(t){n=e(t.currentTarget).data("bs."+this.type);if(!n){n=new this.constructor(t.currentTarget,this.getDelegateOptions());e(t.currentTarget).data("bs."+this.type,n)}}n.tip().hasClass("in")?n.leave(n):n.enter(n)};t.prototype.destroy=function(){clearTimeout(this.timeout);this.hide().$element.off("."+this.type).removeData("bs."+this.type)};var r=e.fn.tooltip;e.fn.tooltip=n;e.fn.tooltip.Constructor=t;e.fn.tooltip.noConflict=function(){e.fn.tooltip=r;return this}}(jQuery);+function(e){"use strict";function n(n){return this.each(function(){var r=e(this),i=r.data("bs.popover"),s=typeof n=="object"&&n;if(!i&&n=="destroy")return;i||r.data("bs.popover",i=new t(this,s));typeof n=="string"&&i[n]()})}var t=function(e,t){this.init("popover",e,t)};if(!e.fn.tooltip)throw new Error("Popover requires tooltip.js");t.VERSION="3.2.0";t.DEFAULTS=e.extend({},e.fn.tooltip.Constructor.DEFAULTS,{placement:"right",trigger:"click",content:"",template:'<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'});t.prototype=e.extend({},e.fn.tooltip.Constructor.prototype);t.prototype.constructor=t;t.prototype.getDefaults=function(){return t.DEFAULTS};t.prototype.setContent=function(){var e=this.tip(),t=this.getTitle(),n=this.getContent();e.find(".popover-title")[this.options.html?"html":"text"](t);e.find(".popover-content").empty()[this.options.html?typeof n=="string"?"html":"append":"text"](n);e.removeClass("fade top bottom left right in");e.find(".popover-title").html()||e.find(".popover-title").hide()};t.prototype.hasContent=function(){return this.getTitle()||this.getContent()};t.prototype.getContent=function(){var e=this.$element,t=this.options;return e.attr("data-content")||(typeof t.content=="function"?t.content.call(e[0]):t.content)};t.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".arrow")};t.prototype.tip=function(){this.$tip||(this.$tip=e(this.options.template));return this.$tip};var r=e.fn.popover;e.fn.popover=n;e.fn.popover.Constructor=t;e.fn.popover.noConflict=function(){e.fn.popover=r;return this}}(jQuery);jQuery(document).ready(function(e){e('[data-toggle="tooltip"]').tooltip({animation:!0,html:!1,delay:{show:0,hide:300}});e('[data-toggle="popover"]').popover({animation:!0,html:!1,delay:{show:0,hide:300}})});jQuery(window).load(function(){jQuery(".x-flexslider-featured-gallery").flexslider({controlNav:!1,selector:".x-slides > li",prevText:'<i class="x-icon-chevron-left"></i>',nextText:'<i class="x-icon-chevron-right"></i>',animation:"fade",easing:"easeInOutExpo",smoothHeight:!0,slideshow:!1});jQuery(".x-flexslider-flickr").flexslider({controlNav:!1,selector:".x-slides > li",prevText:'<i class="x-icon-chevron-left"></i>',nextText:'<i class="x-icon-chevron-right"></i>',animation:"fade",easing:"easeInOutExpo",smoothHeight:!0,slideshow:!1})});jQuery(document).ready(function() {
	pc_form_pag_acting 	= false; // just one form pagination per time
	pc_reg_is_acting 	= false; // know when registration form is being submitted
	pc_login_is_acting 	= false; // security var to avoid multiple calls
	
	pc_curr_url = window.location.href;
	var pre_timestamp = (pc_curr_url.indexOf('?') !== -1) ? '&' : '?';
	
	
	// fields focus/blur tracking
	var focus_track_subj = '.pc_field_container input';
	jQuery(document).on('focus', focus_track_subj, function(e) {
		jQuery(this).parents('.pc_field_container').addClass('pc_focused_field');
		
	}).on('blur', focus_track_subj, function(e) {
		jQuery(this).parents('.pc_field_container').removeClass('pc_focused_field');
	});
	
	
	/**************************
			 LOGIN
	**************************/
	
	// triggers
	jQuery('body, form').delegate('.pc_auth_btn', 'click', function(e) {
		e.preventDefault();
			
		var $target_form = jQuery(this).parents('form');
		var f_data = $target_form.serialize();

		pc_submit_login($target_form, f_data);
	});
	jQuery('.pc_login_row input').keypress(function(event){
		if(event.keyCode === 13){
			var $target_form = jQuery(this).parents('form');
			var f_data = $target_form.serialize();

			pc_submit_login($target_form, f_data);
		}
		
		event.cancelBubble = true;
		if(event.stopPropagation) event.stopPropagation();
   	});
	
	
	// handle form
	pc_submit_login = function($form, f_data) {
		if(!pc_login_is_acting) {
			
			if(!$form.find('input[name="pc_auth_username"]').val() || !$form.find('input[name="pc_auth_psw"]').val()) {
				return false;	
			}
			
			pc_login_is_acting = true;	
			var forced_redirect = $form.data('pc_redirect');

			$form.find('.pc_auth_btn').addClass('pc_loading_btn');
			$form.find('.pcma_psw_recovery_trigger').fadeTo(200, 0);
			$form.find('#pc_auth_message').empty();
			
			jQuery.ajax({
				type: "POST",
				url: pc_curr_url,
				dataType: "json",
				data: "type=js_ajax_auth&" + f_data,
				success: function(pc_data){
					pc_login_is_acting = false;
					
					// a bit of delay to display the loader
					setTimeout(function() {
						$form.find('.pc_auth_btn').removeClass('pc_loading_btn');
					}, 370);
			
					if(pc_data.resp == 'success') {
						$form.find('#pc_auth_message').append('<div class="pc_success_mess"><span>'+ pc_data.mess +'<span></div>');
						
						if(typeof(forced_redirect) == 'undefined' || forced_redirect == 'refresh') {
							if(pc_data.redirect == '' || forced_redirect == 'refresh') {var red_url = pc_curr_url + pre_timestamp + new Date().getTime();}
							else {var red_url = pc_data.redirect;}
						}
						else {red_url = forced_redirect;}
						
						// redirect
						setTimeout(function() {
						  window.location.href = red_url;
						}, 1000);
					}
					else {
						$form.find('#pc_auth_message').empty().append('<div class="pc_error_mess"><span>'+ pc_data.mess +'</span></div>');	
						$form.find('.pcma_psw_recovery_trigger').fadeTo(200, 1);
					}
				}
			});
		}
	};
	
	
	/* check to avoid smalls over button on small screens */
	pc_login_smalls_display_check = function() {
		jQuery('.pc_rm_login .pcma_psw_recovery_trigger').each(function() {
            var $form = jQuery(this).parents('.pc_login_form');
			var $smalls = $form.find('.pc_login_smalls > *');
			
			// check smalls
			var smalls_w = 0;
			$smalls.each(function() {
                smalls_w = smalls_w + jQuery(this).outerWidth(true);
            });
			
			
			if( 
				($form.width() - smalls_w) < ($form.find('.pc_auth_btn').outerWidth(true) + 10)
			) {
				$form.addClass('pc_mobile_login');
			} else {
				$form.removeClass('pc_mobile_login');
			}
        });
	};
	pc_login_smalls_display_check();


	/* LONG LABELS CHECK */
	var pc_lf_labels_h_check = function() {
		jQuery('.pc_login_form').not('.pc_lf_long_labels').each(function() {
			var user_h 		= jQuery(this).find('label[for=pc_auth_username]').outerHeight();
			var user_f_h 	= jQuery(this).find('input[name="pc_auth_username"]').outerHeight();
			
			var psw_h 	= jQuery(this).find('label[for=pc_auth_psw]').outerHeight();
			var psw_f_h = jQuery(this).find('input[name="pc_auth_psw"]').outerHeight();
			
			if((user_h > user_f_h || psw_h > psw_f_h) && jQuery(window).width() >= 440) {
				jQuery(this).addClass('pc_lf_long_labels');		
			} else {
				jQuery(this).removeClass('pc_lf_long_labels');
			}
        });	
	};
	pc_lf_labels_h_check();
	
	
	// on resize
	jQuery(window).resize(function() {
		if(typeof(pc_is_resizing) != 'undefined') {clearTimeout(pc_is_resizing);}
		
		pc_is_resizing = setTimeout(function() {
			pc_login_smalls_display_check();
			pc_lf_labels_h_check();
		}, 50);
	});
	
	
	
	
	/**************************
			 LOGOUT
	**************************/
	
	// execute logout		 
	jQuery(document).delegate('.pc_logout_btn', 'click', function(e) {	
		e.preventDefault();
		
		var forced_redirect = jQuery(this).data('pc_redirect');
		jQuery(this).addClass('pc_loading_btn');
		
		jQuery.ajax({
			type: "POST",
			url: pc_curr_url,
			data: "type=pc_logout",
			success: function(response){
				resp = jQuery.trim(response);
				
				if(typeof(forced_redirect) == 'undefined') {
					if(resp == '') {window.location.href = pc_curr_url + pre_timestamp + new Date().getTime();}
					else {window.location.href = resp;}
				}
				else {window.location.href = forced_redirect;}
			}
		});
	});
	
			
		
	/**************************
		   REGISTRATION
	**************************/	
	
	// triggers
	jQuery('body, form').delegate('.pc_reg_btn', 'click', function(e) {	
		e.preventDefault();
		
		var $target_form = jQuery(this).parents('form');
		var f_data = $target_form.serialize();

		pc_submit_registration($target_form, f_data);
	});
	jQuery('.pc_registration_form input, .pc_registration_form textarea').keypress(function(event){
		if(event.keyCode === 13){ // enter key
			var $target_form = jQuery(this).parents('form');
			var f_data = $target_form.serialize();
			
			pc_submit_registration($target_form, f_data);
		}
		
		event.cancelBubble = true;
		if(event.stopPropagation) event.stopPropagation();
   	});
	
	
	// handle form
	pc_submit_registration = function($form, f_data) {
		if(pc_reg_is_acting) {return false;}
				
		// HTML5 validate first
		if(!$form.pc_validate_form_fieldset()) {
			return false;	
		}
			
		pc_reg_is_acting = true;
		
		var cc = (typeof($form.data('pc_cc')) == 'undefined') ? '' : $form.data('pc_cc');
		var redir = $form.data('pc_redirect');
		
		$form.find('.pc_reg_btn').addClass('pc_loading_btn');
		$form.find('.pc_form_response').empty();
		
		var data = 
			'type=pc_registration'+
			'&form_id=' + $form.attr('rel') +
			'&pc_cc='+ cc +
			'&' + $form.serialize()
		;
		jQuery.ajax({
			type: "POST",
			url: pc_curr_url,
			data: data,
			dataType: "json",
			success: function(pc_data){
				if(pc_data.resp == 'success') {
					$form.find('.pc_form_response').append('<div class="pc_success_mess"><span>'+ pc_data.mess +'<span></div>');
					
					// redirect
					var redirect = (typeof(redir) != 'undefined') ? redir : pc_data.redirect;
					if(redirect == 'refresh') {redirect = pc_curr_url;}
					if(redirect) {
						setTimeout(function() {
						  window.location.href = redirect;
						}, 1000);	
					}
				}
				else {
					$form.find('.pc_form_response').append('<div class="pc_error_mess">'+ pc_data.mess +'</div>');
					
					// if exist recaptcha - reload
					if( jQuery('#recaptcha_response_field').length) {
						Recaptcha.reload();	
					}
				}
				
				// a bit of delay to display the loader
				setTimeout(function() {
					$form.find('.pc_reg_btn').removeClass('pc_loading_btn');
				}, 370);
				
				pc_reg_is_acting = false;
			}
		});
	};
	
	
	/* setup multiple select plugin */
	var pc_multiselect_setup = function() {
		if(jQuery('.pc_multiselect, .pc_singleselect').length && typeof(jQuery.fn.multipleSelect) == 'function') {
			jQuery('.pc_multiselect select').each(function() {
				jQuery(this).multipleSelect( {
					selectAll: false,
					filter: true,
					countSelected: pc_ms_countSelected,
					allSelected: pc_ms_allSelected,
					placeholder: jQuery(this).data('placeh'),	
				});
			});
			
			jQuery('.pc_singleselect select').each(function() {
				jQuery(this).multipleSelect( {
					single: true,
					selectAll: false,
					filter: true,
					placeholder: jQuery(this).data('placeh'),	
				});
			});
				
			
			setTimeout(function() {
				jQuery('.ms-search input').attr('placeholder', pc_ms_search_placeh +' ..');
				jQuery('.ms-search .ms-no-results').text(pc_ms_search_nores +' ..');	
			}, 50);
		}
	};
	pc_multiselect_setup();
	
	
	/* setup custom checkboxes */
	var pc_checkboxes_setup = function() {
		jQuery('.pc_login_form input[type=checkbox], .pc_check_wrap input[type=checkbox], .pc_single_check input[type=checkbox], .pc_rf_disclaimer input[type=checkbox]').each(function() {
			if(jQuery(this).hasClass('pc_checkboxed')) {return true;}
			
			var $subj = jQuery(this);
			$subj.addClass('pc_checkboxed');
			
			var checked = ($subj.is(':checked')) ? 'pc_checked' : '';
			$subj.after('<div class="pc_checkbox '+ checked +'" data-name="'+ $subj.attr('name') +'" data-val="'+ $subj.val() +'"><span>&#10003;</span></div>');
		});
		
		
		
		jQuery('form').undelegate('div.pc_checkbox', 'click').delegate('div.pc_checkbox', 'click', function() {
			var $subj = jQuery(this);
			var $checkbox = $subj.parents('section').find('input[type=checkbox][name="'+ $subj.data('name') +'"][value="'+ $subj.data('val') +'"]');
			
			if(!$checkbox.length) {return true;}
			
			if($subj.hasClass('pc_checked')) {
				$subj.removeClass('pc_checked');
				$checkbox.removeAttr('checked');
			}
			else {
				$subj.addClass('pc_checked');	
				$checkbox.attr('checked', 'checked');
			}
		});
	};
	pc_checkboxes_setup();
	

	
	
	/* fluid forms - columnizer */
	pc_fluid_form_columnizer = function() {
		jQuery('.pc_fluid_form').each(function() {
			// calculate
			var form_w = jQuery(this).width();

			var col = Math.round( form_w / 315 );
			if(col > 5) {col = 5;}
			if(col < 1) {col = 1;}

			// if is not first check - remove past column 
			if(typeof( jQuery(this).attr('pc_col') ) != 'undefined') {
				var curr_col = jQuery(this).attr('pc_col');
				if(col != curr_col) {
					jQuery(this).removeClass('pc_form_'+curr_col+'col');	
				}
			}
			
			// apply
			jQuery(this).attr('pc_col', col);
			jQuery(this).addClass('pc_form_'+col+'col');		
        });	
	};
	pc_fluid_form_columnizer();
	
	jQuery(window).resize(function() { 
		if(typeof(pc_ffc) != 'undefined') {clearTimeout(pc_ffc);}
		pc_ffc = setTimeout(function() {
			pc_fluid_form_columnizer();
		}, 50);
	});
	

	
	/////////////////////////////////////////////////////////////////////
	
	
	/**************************
		  FORM PAGINATION
	**************************/	
	
	jQuery('body, form').delegate('.pc_pag_btn', 'click', function(e) {
		if(pc_form_pag_acting || pc_reg_is_acting) {return true;}
		
		var $form = jQuery(this).parents('form');
		var tot_pag = $form.find('fieldset').length;
		
		var curr_pag = parseInt($form.data('form-pag'));
		var $curr_fieldset = $form.find('fieldset.pc_f_pag_'+curr_pag);
		
		// HTML5 validate first
		if(!$form.pc_validate_form_fieldset()) {
			return true;	
		}
		
		// next
		if(jQuery(e.target).hasClass('pc_pag_next')) {
			var new_pag = curr_pag + 1;
			if(new_pag > tot_pag) {return true;}
			
			var $new_fieldset = $form.find('fieldset.pc_f_pag_'+new_pag); 	
		}
		// prev
		else {
			var new_pag = curr_pag - 1;
			if(new_pag < 0) {return true;}
			
			var $new_fieldset = $form.find('fieldset.pc_f_pag_'+new_pag); 		
		}
		
		
		// apply
		pc_form_pag_acting = true;

		$form.css('height', $form.outerHeight());
		$form.data('form-pag', new_pag);
		$form.find('> *').animate({opacity : 0}, 150);
		
		setTimeout(function() {
			$new_fieldset.removeClass('pc_hidden_fieldset');

			var new_form_h = ($form.outerHeight() - $curr_fieldset.outerHeight(true)) + $new_fieldset.outerHeight(true);  
			$form.animate({height : new_form_h}, 300);
			
			$curr_fieldset.addClass('pc_hidden_fieldset');
			
			if(new_pag == tot_pag) {
				$form.find('.pc_pag_submit').show();
			} else {
				$form.find('.pc_pag_submit').hide();	
			}
			
			setTimeout(function() {	
				$form.find('fieldset, .pc_pag_submit, .pc_pag_btn, .pc_form_response').animate({opacity : 1}, 150);
				
				// next btn and submit visibility
				if(new_pag == tot_pag) {
					$form.find('.pc_pag_next').css('visibility', 'hidden');
				} else {
					$form.find('.pc_pag_next').css('visibility', 'visible');	
				}
				
				// prev btn visibility
				if(new_pag == 1) {
					$form.find('.pc_pag_prev').css('visibility', 'hidden');	
				}
				else {
					$form.find('.pc_pag_prev').css('visibility', 'visible');	
				}
				
				$form.css('height', 'auto');
				pc_form_pag_acting = false;
			}, 350);
		}, 300);
		
	});
	
	
	
	/////////////////////////////////////////////////////////////////////
	
	
	/**************************
		  FORM VALIDATION
	**************************/	
	
	
	// validate fields using HTML5 engine
	jQuery.fn.pc_validate_fields = function() {
		if(typeof(pc_html5_validation) == 'undefined' || !pc_html5_validation) {return true;}
		
		// if browser doesn't support validation - ignore
		if(!(typeof document.createElement( 'input' ).checkValidity == 'function')) {return true;}
		var errorless = true;
		 
		jQuery(this).each(function() {
			jQuery(this).parents('section').find('.pc_field_error').remove();
			
            if( !jQuery(this)[0].checkValidity() ) {
				errorless = false;
				var mess = jQuery(this)[0].validationMessage; 
				
				// remove ugly point at the end
				if( mess.substr(mess.length - 1) == '.') {
					mess = mess.substr(0, (mess.length - 1));
				}
				
				jQuery(this).parents('section').prepend('<div class="pc_field_error">'+ mess +'</div>');	
			}
			
        });
		
		return errorless;
	};
	
	// shortcut to validate active fieldset fields
	jQuery.fn.pc_validate_form_fieldset = function() {
		return jQuery(this).find('fieldset').not('.pc_hidden_fieldset').find('input, select, textarea').pc_validate_fields();
	};
	
	
	// re-validate on field change
	jQuery('body, form').delegate('.pc_rf_field input, .pc_rf_field select, .pc_rf_field textarea, .pc_rf_disclaimer input', 'change', function() {
		
		if(jQuery(this).pc_validate_fields()) {
			jQuery(this).parents('.pc_rf_field').find('.pc_field_error').pc_close_tooltip();	
		}
	})
	
	
	// close field error tooltip
	jQuery.fn.pc_close_tooltip = function() {
		var $subj = jQuery(this);
		$subj.addClass('pc_fe_closing');
		
		setTimeout(function() {
			//$subj.remove();
		}, 310);
	};
	
	// close on click
	jQuery('body, form').delegate('.pc_field_error', 'click', function() {
		jQuery(this).pc_close_tooltip();
	});
	
	
	/////////////////////////////////////////////////////////////////////
	

	/**************************
			LIGHTBOX
	**************************/	
		
	if(typeof(pc_lb_classes) != 'undefined' && typeof(jQuery.magnificPopup) != 'undefined') {
		
		// persistent check to preload contents
		var pc_lb_load_intval = setInterval(function() {
			var to_load = [];
			
			jQuery.each(pc_lb_classes, function(i, v) {
				var id = v.replace('.pc_lb_trig_', '');
				if(jQuery.inArray(id, pc_ready_lb) !== -1) {return true;}
				
				// ajax call to get
				if(jQuery(v).length) {
					to_load.push(id);
					pc_ready_lb.push(id);
				}
			});
			
			if(to_load.length) {
				pc_is_loading_lb = true;

				// just one ajax call to get codes
				var data = {
					type 	: 'pc_lightbox_load',
					ids		: to_load
				};
				jQuery.ajax({
					type: "POST",
					url: pc_curr_url,
					data: data,
					success: function(data){
						jQuery('#pc_lb_codes').append(data);
					}
				});
			}
			
			// if loaded every lightbox - end interval 
			if(pc_lb_classes.length == pc_ready_lb.length) {
				clearInterval(pc_lb_load_intval);	
			}
		}, 200);
		
		
		
		// track lightbox triggers click
		jQuery.each(pc_lb_classes, function(i,v) {
			var lb_id = v.replace('.pc_lb_trig_', '');
			
			jQuery(document).delegate(v, 'click', function(e) {
				if(!jQuery('.pc_lb_'+lb_id).length) {return true;}
				e.preventDefault();
				
				jQuery.magnificPopup.open({
					items : {
						src: '.pc_lb_'+lb_id,
						type: 'inline'
					},
					mainClass			: 'pc_lightbox',
					closeOnContentClick	: false,
					closeOnBgClick		: false, 
					preloader			: false,
					modal				: (jQuery(this).hasClass('pc_modal_lb')) ? true : false,
					focus				: 'input',
					removalDelay		: 300,
					callbacks: {
						open: function() {
							pc_multiselect_setup();
							pc_checkboxes_setup();
							pc_fluid_form_columnizer();
							
							// if last element is a form - remove bottom margin
							if(jQuery('.pc_lightbox_contents > *').eq(-2).hasClass('pc_aligned_form')) {
								jQuery('.pc_lightbox_contents > *').eq(-2).find('form').css('margin-bottom', 0);
							}
							
							// allow other plugins to hook here
							jQuery(document).trigger('pc_opening_lightbox');
						}
					}
				});
				
				return false;
			});
		});	
	}
	
	

});(function(d){function g(a,c){var b=this,e=a.attr("name")||c.name||"";this.options=c;this.$el=a.hide();this.$label=this.$el.closest("label");0===this.$label.length&&this.$el.attr("id")&&(this.$label=d(f('label[for="%s"]',this.$el.attr("id").replace(/:/g,"\\:"))));this.$parent=d(f('<div class="ms-parent %s" %s/>',a.attr("class")||"",f('title="%s"',a.attr("title"))));this.$choice=d(f('<button type="button" class="ms-choice"><span class="placeholder">%s</span><div></div></button>',this.options.placeholder));
this.$drop=d(f('<div class="ms-drop %s"%s></div>',this.options.position,f(' style="width: %s"',this.options.dropWidth)));this.$el.after(this.$parent);this.$parent.append(this.$choice);this.$parent.append(this.$drop);this.$el.prop("disabled")&&this.$choice.addClass("disabled");this.$parent.css("width",this.options.width||this.$el.css("width")||this.$el.outerWidth()+20);this.selectAllName='data-name="selectAll'+e+'"';this.selectGroupName='data-name="selectGroup'+e+'"';this.selectItemName='data-name="selectItem'+
e+'"';this.options.keepOpen||d(document).click(function(c){d(c.target)[0]!==b.$choice[0]&&d(c.target).parents(".ms-choice")[0]!==b.$choice[0]&&(d(c.target)[0]===b.$drop[0]||d(c.target).parents(".ms-drop")[0]!==b.$drop[0]&&c.target!==a[0])&&b.options.isOpen&&b.close()})}var f=function(a){var c=arguments,b=!0,d=1;a=a.replace(/%s/g,function(){var a=c[d++];return"undefined"===typeof a?(b=!1,""):a});return b?a:""},l=function(a){for(var c=[{base:"A",letters:/[\u0041\u24B6\uFF21\u00C0\u00C1\u00C2\u1EA6\u1EA4\u1EAA\u1EA8\u00C3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\u00C4\u01DE\u1EA2\u00C5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F]/g},
{base:"AA",letters:/[\uA732]/g},{base:"AE",letters:/[\u00C6\u01FC\u01E2]/g},{base:"AO",letters:/[\uA734]/g},{base:"AU",letters:/[\uA736]/g},{base:"AV",letters:/[\uA738\uA73A]/g},{base:"AY",letters:/[\uA73C]/g},{base:"B",letters:/[\u0042\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181]/g},{base:"C",letters:/[\u0043\u24B8\uFF23\u0106\u0108\u010A\u010C\u00C7\u1E08\u0187\u023B\uA73E]/g},{base:"D",letters:/[\u0044\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779]/g},{base:"DZ",
letters:/[\u01F1\u01C4]/g},{base:"Dz",letters:/[\u01F2\u01C5]/g},{base:"E",letters:/[\u0045\u24BA\uFF25\u00C8\u00C9\u00CA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\u00CB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E]/g},{base:"F",letters:/[\u0046\u24BB\uFF26\u1E1E\u0191\uA77B]/g},{base:"G",letters:/[\u0047\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E]/g},{base:"H",letters:/[\u0048\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D]/g},
{base:"I",letters:/[\u0049\u24BE\uFF29\u00CC\u00CD\u00CE\u0128\u012A\u012C\u0130\u00CF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197]/g},{base:"J",letters:/[\u004A\u24BF\uFF2A\u0134\u0248]/g},{base:"K",letters:/[\u004B\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2]/g},{base:"L",letters:/[\u004C\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780]/g},{base:"LJ",letters:/[\u01C7]/g},{base:"Lj",letters:/[\u01C8]/g},
{base:"M",letters:/[\u004D\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C]/g},{base:"N",letters:/[\u004E\u24C3\uFF2E\u01F8\u0143\u00D1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4]/g},{base:"NJ",letters:/[\u01CA]/g},{base:"Nj",letters:/[\u01CB]/g},{base:"O",letters:/[\u004F\u24C4\uFF2F\u00D2\u00D3\u00D4\u1ED2\u1ED0\u1ED6\u1ED4\u00D5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\u00D6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\u00D8\u01FE\u0186\u019F\uA74A\uA74C]/g},
{base:"OI",letters:/[\u01A2]/g},{base:"OO",letters:/[\uA74E]/g},{base:"OU",letters:/[\u0222]/g},{base:"P",letters:/[\u0050\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754]/g},{base:"Q",letters:/[\u0051\u24C6\uFF31\uA756\uA758\u024A]/g},{base:"R",letters:/[\u0052\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782]/g},{base:"S",letters:/[\u0053\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784]/g},{base:"T",
letters:/[\u0054\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786]/g},{base:"TZ",letters:/[\uA728]/g},{base:"U",letters:/[\u0055\u24CA\uFF35\u00D9\u00DA\u00DB\u0168\u1E78\u016A\u1E7A\u016C\u00DC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244]/g},{base:"V",letters:/[\u0056\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245]/g},{base:"VY",letters:/[\uA760]/g},{base:"W",letters:/[\u0057\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72]/g},
{base:"X",letters:/[\u0058\u24CD\uFF38\u1E8A\u1E8C]/g},{base:"Y",letters:/[\u0059\u24CE\uFF39\u1EF2\u00DD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE]/g},{base:"Z",letters:/[\u005A\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762]/g},{base:"a",letters:/[\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250]/g},
{base:"aa",letters:/[\uA733]/g},{base:"ae",letters:/[\u00E6\u01FD\u01E3]/g},{base:"ao",letters:/[\uA735]/g},{base:"au",letters:/[\uA737]/g},{base:"av",letters:/[\uA739\uA73B]/g},{base:"ay",letters:/[\uA73D]/g},{base:"b",letters:/[\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253]/g},{base:"c",letters:/[\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184]/g},{base:"d",letters:/[\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A]/g},
{base:"dz",letters:/[\u01F3\u01C6]/g},{base:"e",letters:/[\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD]/g},{base:"f",letters:/[\u0066\u24D5\uFF46\u1E1F\u0192\uA77C]/g},{base:"g",letters:/[\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F]/g},{base:"h",letters:/[\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265]/g},
{base:"hv",letters:/[\u0195]/g},{base:"i",letters:/[\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131]/g},{base:"j",letters:/[\u006A\u24D9\uFF4A\u0135\u01F0\u0249]/g},{base:"k",letters:/[\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3]/g},{base:"l",letters:/[\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747]/g},{base:"lj",letters:/[\u01C9]/g},
{base:"m",letters:/[\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F]/g},{base:"n",letters:/[\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5]/g},{base:"nj",letters:/[\u01CC]/g},{base:"o",letters:/[\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275]/g},
{base:"oi",letters:/[\u01A3]/g},{base:"ou",letters:/[\u0223]/g},{base:"oo",letters:/[\uA74F]/g},{base:"p",letters:/[\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755]/g},{base:"q",letters:/[\u0071\u24E0\uFF51\u024B\uA757\uA759]/g},{base:"r",letters:/[\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783]/g},{base:"s",letters:/[\u0073\u24E2\uFF53\u00DF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B]/g},
{base:"t",letters:/[\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787]/g},{base:"tz",letters:/[\uA729]/g},{base:"u",letters:/[\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289]/g},{base:"v",letters:/[\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C]/g},{base:"vy",letters:/[\uA761]/g},{base:"w",letters:/[\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73]/g},
{base:"x",letters:/[\u0078\u24E7\uFF58\u1E8B\u1E8D]/g},{base:"y",letters:/[\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF]/g},{base:"z",letters:/[\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763]/g}],b=0;b<c.length;b++)a=a.replace(c[b].letters,c[b].base);return a};g.prototype={constructor:g,init:function(){var a=this,c=d("<ul></ul>");this.$drop.html("");this.options.filter&&this.$drop.append('<div class="ms-search"><input type="text" autocomplete="off" autocorrect="off" autocapitilize="off" spellcheck="false"></div>');
this.options.selectAll&&!this.options.single&&c.append(['<li class="ms-select-all"><label>',f('<input type="checkbox" %s /> ',this.selectAllName),this.options.selectAllDelimiter[0],this.options.selectAllText,this.options.selectAllDelimiter[1],"</label></li>"].join(""));d.each(this.$el.children(),function(b,d){c.append(a.optionToHtml(b,d))});c.append(f('<li class="ms-no-results">%s</li>',this.options.noMatchesFound));this.$drop.append(c);this.$drop.find("ul").css("max-height",this.options.maxHeight+
"px");this.$drop.find(".multiple").css("width",this.options.multipleWidth+"px");this.$searchInput=this.$drop.find(".ms-search input");this.$selectAll=this.$drop.find("input["+this.selectAllName+"]");this.$selectGroups=this.$drop.find("input["+this.selectGroupName+"]");this.$selectItems=this.$drop.find("input["+this.selectItemName+"]:enabled");this.$disableItems=this.$drop.find("input["+this.selectItemName+"]:disabled");this.$noResults=this.$drop.find(".ms-no-results");this.events();this.updateSelectAll(!0);
this.update(!0);this.options.isOpen&&this.open()},optionToHtml:function(a,c,b,e){var n=this;c=d(c);var h=c.attr("class")||"",k=f('title="%s"',c.attr("title")),q=this.options.multiple?"multiple":"",r=this.options.single?"radio":"checkbox";if(c.is("option")){a=c.val();var g=n.options.textTemplate(c),l=c.prop("selected"),t=f('style="%s"',this.options.styler(a));var m=e||c.prop("disabled");e=d([f('<li class="%s %s" %s %s>',q,h,k,t),f('<label class="%s">',m?"disabled":""),f('<input type="%s" %s%s%s%s>',
r,this.selectItemName,l?' checked="checked"':"",m?' disabled="disabled"':"",f(' data-group="%s"',b)),f("<span>%s</span>",g),"</label></li>"].join(""));e.find("input").val(a);return e}if(c.is("optgroup")){e=n.options.labelTemplate(c);var p=d("<div/>");b="group_"+a;m=c.prop("disabled");p.append(['<li class="group">',f('<label class="optgroup %s" data-group="%s">',m?"disabled":"",b),this.options.hideOptgroupCheckboxes||this.options.single?"":f('<input type="checkbox" %s %s>',this.selectGroupName,m?'disabled="disabled"':
""),e,"</label></li>"].join(""));d.each(c.children(),function(a,c){p.append(n.optionToHtml(a,c,b,m))});return p.html()}},events:function(){var a=this,c=function(b){b.preventDefault();a[a.options.isOpen?"close":"open"]()};if(this.$label)this.$label.off("click").on("click",function(b){"label"===b.target.nodeName.toLowerCase()&&b.target===this&&(c(b),a.options.filter&&a.options.isOpen||a.focus(),b.stopPropagation())});this.$choice.off("click").on("click",c).off("focus").on("focus",this.options.onFocus).off("blur").on("blur",
this.options.onBlur);this.$parent.off("keydown").on("keydown",function(b){switch(b.which){case 27:a.close(),a.$choice.focus()}});this.$searchInput.off("keydown").on("keydown",function(b){9===b.keyCode&&b.shiftKey&&a.close()}).off("keyup").on("keyup",function(b){a.options.filterAcceptOnEnter&&(13===b.which||32==b.which)&&a.$searchInput.val()?(a.$selectAll.click(),a.close(),a.focus()):a.filter()});this.$selectAll.off("click").on("click",function(){var b=d(this).prop("checked"),c=a.$selectItems.filter(":visible");
if(c.length===a.$selectItems.length)a[b?"checkAll":"uncheckAll"]();else a.$selectGroups.prop("checked",b),c.prop("checked",b),a.options[b?"onCheckAll":"onUncheckAll"](),a.update()});this.$selectGroups.off("click").on("click",function(){var b=d(this).parent().attr("data-group"),b=a.$selectItems.filter(":visible").filter(f('[data-group="%s"]',b)),c=b.length!==b.filter(":checked").length;b.prop("checked",c);a.updateSelectAll();a.update();a.options.onOptgroupClick({label:d(this).parent().text(),checked:c,
children:b.get(),instance:a})});this.$selectItems.off("click").on("click",function(){a.updateSelectAll();a.update();a.updateOptGroupSelect();a.options.onClick({label:d(this).parent().text(),value:d(this).val(),checked:d(this).prop("checked"),instance:a});a.options.single&&a.options.isOpen&&!a.options.keepOpen&&a.close();if(a.options.single){var b=d(this).val();a.$selectItems.filter(function(){return d(this).val()!==b}).each(function(){d(this).prop("checked",!1)});a.update()}})},open:function(){if(!this.$choice.hasClass("disabled")){this.options.isOpen=
!0;this.$choice.find(">div").addClass("open");this.$drop[this.animateMethod("show")]();this.$selectAll.parent().show();this.$noResults.hide();this.$el.children().length||(this.$selectAll.parent().hide(),this.$noResults.show());if(this.options.container){var a=this.$drop.offset();this.$drop.appendTo(d(this.options.container));this.$drop.offset({top:a.top,left:a.left})}this.options.filter&&(this.$searchInput.val(""),this.$searchInput.focus(),this.filter());this.options.onOpen()}},close:function(){this.options.isOpen=
!1;this.$choice.find(">div").removeClass("open");this.$drop[this.animateMethod("hide")]();this.options.container&&(this.$parent.append(this.$drop),this.$drop.css({top:"auto",left:"auto"}));this.options.onClose()},animateMethod:function(a){return{show:{fade:"fadeIn",slide:"slideDown"},hide:{fade:"fadeOut",slide:"slideUp"}}[a][this.options.animate]||a},update:function(a){var c=this.options.displayValues?this.getSelects():this.getSelects("text"),b=this.$choice.find(">span"),e=c.length;0===e?b.addClass("placeholder").html(this.options.placeholder):
this.options.allSelected&&e===this.$selectItems.length+this.$disableItems.length?b.removeClass("placeholder").html(this.options.allSelected):this.options.ellipsis&&e>this.options.minimumCountSelected?b.removeClass("placeholder").text(c.slice(0,this.options.minimumCountSelected).join(this.options.delimiter)+"..."):this.options.countSelected&&e>this.options.minimumCountSelected?b.removeClass("placeholder").html(this.options.countSelected.replace("#",c.length).replace("%",this.$selectItems.length+this.$disableItems.length)):
b.removeClass("placeholder").text(c.join(this.options.delimiter));this.options.addTitle&&b.prop("title",this.getSelects("text"));this.$el.val(this.getSelects()).trigger("change");this.$drop.find("li").removeClass("selected");this.$drop.find("input:checked").each(function(){d(this).parents("li").first().addClass("selected")});a||this.$el.trigger("change")},updateSelectAll:function(a){var c=this.$selectItems;a||(c=c.filter(":visible"));this.$selectAll.prop("checked",c.length&&c.length===c.filter(":checked").length);
if(!a&&this.$selectAll.prop("checked"))this.options.onCheckAll()},updateOptGroupSelect:function(){var a=this.$selectItems.filter(":visible");d.each(this.$selectGroups,function(c,b){var e=d(b).parent().attr("data-group"),e=a.filter(f('[data-group="%s"]',e));d(b).prop("checked",e.length&&e.length===e.filter(":checked").length)})},getSelects:function(a){var c=this,b=[],e=[];this.$drop.find(f("input[%s]:checked",this.selectItemName)).each(function(){b.push(d(this).parents("li").first().text());e.push(d(this).val())});
"text"===a&&this.$selectGroups.length&&(b=[],this.$selectGroups.each(function(){var a=[],e=d.trim(d(this).parent().text()),k=d(this).parent().data("group"),k=c.$drop.find(f('[%s][data-group="%s"]',c.selectItemName,k)),g=k.filter(":checked");if(g.length){a.push("[");a.push(e);if(k.length>g.length){var l=[];g.each(function(){l.push(d(this).parent().text())});a.push(": "+l.join(", "))}a.push("]");b.push(a.join(""))}}));return"text"===a?b:e},setSelects:function(a){var c=this;this.$selectItems.prop("checked",
!1);this.$disableItems.prop("checked",!1);d.each(a,function(a,d){c.$selectItems.filter(f('[value="%s"]',d)).prop("checked",!0);c.$disableItems.filter(f('[value="%s"]',d)).prop("checked",!0)});this.$selectAll.prop("checked",this.$selectItems.length===this.$selectItems.filter(":checked").length+this.$disableItems.filter(":checked").length);d.each(c.$selectGroups,function(a,e){var b=d(e).parent().attr("data-group"),b=c.$selectItems.filter('[data-group="'+b+'"]');d(e).prop("checked",b.length&&b.length===
b.filter(":checked").length)});this.update()},enable:function(){this.$choice.removeClass("disabled")},disable:function(){this.$choice.addClass("disabled")},checkAll:function(){this.$selectItems.prop("checked",!0);this.$selectGroups.prop("checked",!0);this.$selectAll.prop("checked",!0);this.update();this.options.onCheckAll()},uncheckAll:function(){this.$selectItems.prop("checked",!1);this.$selectGroups.prop("checked",!1);this.$selectAll.prop("checked",!1);this.update();this.options.onUncheckAll()},
focus:function(){this.$choice.focus();this.options.onFocus()},blur:function(){this.$choice.blur();this.options.onBlur()},refresh:function(){this.init()},filter:function(){var a=this,c=d.trim(this.$searchInput.val()).toLowerCase();0===c.length?(this.$selectAll.parent().show(),this.$selectItems.parent().show(),this.$disableItems.parent().show(),this.$selectGroups.parent().show(),this.$noResults.hide()):(this.$selectItems.each(function(){var a=d(this).parent();a[0>l(a.text().toLowerCase()).indexOf(l(c))?
"hide":"show"]()}),this.$disableItems.parent().hide(),this.$selectGroups.each(function(){var b=d(this).parent(),c=b.attr("data-group"),n=a.$selectItems.filter(":visible");b[n.filter(f('[data-group="%s"]',c)).length?"show":"hide"]()}),this.$selectItems.parent().filter(":visible").length?(this.$selectAll.parent().show(),this.$noResults.hide()):(this.$selectAll.parent().hide(),this.$noResults.show()));this.updateOptGroupSelect();this.updateSelectAll();this.options.onFilter(c)}};d.fn.multipleSelect=function(){var a=
arguments[0],c=arguments,b,e="getSelects setSelects enable disable open close checkAll uncheckAll focus blur refresh close".split(" ");this.each(function(){var f=d(this),h=f.data("multipleSelect"),k=d.extend({},d.fn.multipleSelect.defaults,f.data(),"object"===typeof a&&a);h||(h=new g(f,k),f.data("multipleSelect",h));if("string"===typeof a){if(0>d.inArray(a,e))throw"Unknown method: "+a;b=h[a](c[1])}else h.init(),c[1]&&(b=h[c[1]].apply(h,[].slice.call(c,2)))});return"undefined"!==typeof b?b:this};d.fn.multipleSelect.defaults=
{name:"",isOpen:!1,placeholder:"",selectAll:!0,selectAllDelimiter:["[","]"],minimumCountSelected:3,ellipsis:!1,multiple:!1,multipleWidth:80,single:!1,filter:!1,width:void 0,dropWidth:void 0,maxHeight:250,container:null,position:"bottom",keepOpen:!1,animate:"none",displayValues:!1,delimiter:", ",addTitle:!1,filterAcceptOnEnter:!1,hideOptgroupCheckboxes:!1,selectAllText:"Select all",allSelected:"All selected",countSelected:"# of % selected",noMatchesFound:"No matches found",styler:function(){return!1},
textTemplate:function(a){return a.html()},labelTemplate:function(a){return a.attr("label")},onOpen:function(){return!1},onClose:function(){return!1},onCheckAll:function(){return!1},onUncheckAll:function(){return!1},onFocus:function(){return!1},onBlur:function(){return!1},onOptgroupClick:function(){return!1},onClick:function(){return!1},onFilter:function(){return!1}}})(jQuery);// Magnific Popup v1.1.0 by Dmitry Semenov
// http://bit.ly/magnific-popup#build=inline
(function(a){typeof define=="function"&&define.amd?define(["jquery"],a):typeof exports=="object"?a(require("jquery")):a(window.jQuery||window.Zepto)})(function(a){var b="Close",c="BeforeClose",d="AfterClose",e="BeforeAppend",f="MarkupParse",g="Open",h="Change",i="mfp",j="."+i,k="mfp-ready",l="mfp-removing",m="mfp-prevent-close",n,o=function(){},p=!!window.jQuery,q,r=a(window),s,t,u,v,w=function(a,b){n.ev.on(i+a+j,b)},x=function(b,c,d,e){var f=document.createElement("div");return f.className="mfp-"+b,d&&(f.innerHTML=d),e?c&&c.appendChild(f):(f=a(f),c&&f.appendTo(c)),f},y=function(b,c){n.ev.triggerHandler(i+b,c),n.st.callbacks&&(b=b.charAt(0).toLowerCase()+b.slice(1),n.st.callbacks[b]&&n.st.callbacks[b].apply(n,a.isArray(c)?c:[c]))},z=function(b){if(b!==v||!n.currTemplate.closeBtn)n.currTemplate.closeBtn=a(n.st.closeMarkup.replace("%title%",n.st.tClose)),v=b;return n.currTemplate.closeBtn},A=function(){a.magnificPopup.instance||(n=new o,n.init(),a.magnificPopup.instance=n)},B=function(){var a=document.createElement("p").style,b=["ms","O","Moz","Webkit"];if(a.transition!==undefined)return!0;while(b.length)if(b.pop()+"Transition"in a)return!0;return!1};o.prototype={constructor:o,init:function(){var b=navigator.appVersion;n.isLowIE=n.isIE8=document.all&&!document.addEventListener,n.isAndroid=/android/gi.test(b),n.isIOS=/iphone|ipad|ipod/gi.test(b),n.supportsTransition=B(),n.probablyMobile=n.isAndroid||n.isIOS||/(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent),s=a(document),n.popupsCache={}},open:function(b){var c;if(b.isObj===!1){n.items=b.items.toArray(),n.index=0;var d=b.items,e;for(c=0;c<d.length;c++){e=d[c],e.parsed&&(e=e.el[0]);if(e===b.el[0]){n.index=c;break}}}else n.items=a.isArray(b.items)?b.items:[b.items],n.index=b.index||0;if(n.isOpen){n.updateItemHTML();return}n.types=[],u="",b.mainEl&&b.mainEl.length?n.ev=b.mainEl.eq(0):n.ev=s,b.key?(n.popupsCache[b.key]||(n.popupsCache[b.key]={}),n.currTemplate=n.popupsCache[b.key]):n.currTemplate={},n.st=a.extend(!0,{},a.magnificPopup.defaults,b),n.fixedContentPos=n.st.fixedContentPos==="auto"?!n.probablyMobile:n.st.fixedContentPos,n.st.modal&&(n.st.closeOnContentClick=!1,n.st.closeOnBgClick=!1,n.st.showCloseBtn=!1,n.st.enableEscapeKey=!1),n.bgOverlay||(n.bgOverlay=x("bg").on("click"+j,function(){n.close()}),n.wrap=x("wrap").attr("tabindex",-1).on("click"+j,function(a){n._checkIfClose(a.target)&&n.close()}),n.container=x("container",n.wrap)),n.contentContainer=x("content"),n.st.preloader&&(n.preloader=x("preloader",n.container,n.st.tLoading));var h=a.magnificPopup.modules;for(c=0;c<h.length;c++){var i=h[c];i=i.charAt(0).toUpperCase()+i.slice(1),n["init"+i].call(n)}y("BeforeOpen"),n.st.showCloseBtn&&(n.st.closeBtnInside?(w(f,function(a,b,c,d){c.close_replaceWith=z(d.type)}),u+=" mfp-close-btn-in"):n.wrap.append(z())),n.st.alignTop&&(u+=" mfp-align-top"),n.fixedContentPos?n.wrap.css({overflow:n.st.overflowY,overflowX:"hidden",overflowY:n.st.overflowY}):n.wrap.css({top:r.scrollTop(),position:"absolute"}),(n.st.fixedBgPos===!1||n.st.fixedBgPos==="auto"&&!n.fixedContentPos)&&n.bgOverlay.css({height:s.height(),position:"absolute"}),n.st.enableEscapeKey&&s.on("keyup"+j,function(a){a.keyCode===27&&n.close()}),r.on("resize"+j,function(){n.updateSize()}),n.st.closeOnContentClick||(u+=" mfp-auto-cursor"),u&&n.wrap.addClass(u);var l=n.wH=r.height(),m={};if(n.fixedContentPos&&n._hasScrollBar(l)){var o=n._getScrollbarSize();o&&(m.marginRight=o)}n.fixedContentPos&&(n.isIE7?a("body, html").css("overflow","hidden"):m.overflow="hidden");var p=n.st.mainClass;return n.isIE7&&(p+=" mfp-ie7"),p&&n._addClassToMFP(p),n.updateItemHTML(),y("BuildControls"),a("html").css(m),n.bgOverlay.add(n.wrap).prependTo(n.st.prependTo||a(document.body)),n._lastFocusedEl=document.activeElement,setTimeout(function(){n.content?(n._addClassToMFP(k),n._setFocus()):n.bgOverlay.addClass(k),s.on("focusin"+j,n._onFocusIn)},16),n.isOpen=!0,n.updateSize(l),y(g),b},close:function(){if(!n.isOpen)return;y(c),n.isOpen=!1,n.st.removalDelay&&!n.isLowIE&&n.supportsTransition?(n._addClassToMFP(l),setTimeout(function(){n._close()},n.st.removalDelay)):n._close()},_close:function(){y(b);var c=l+" "+k+" ";n.bgOverlay.detach(),n.wrap.detach(),n.container.empty(),n.st.mainClass&&(c+=n.st.mainClass+" "),n._removeClassFromMFP(c);if(n.fixedContentPos){var e={marginRight:""};n.isIE7?a("body, html").css("overflow",""):e.overflow="",a("html").css(e)}s.off("keyup"+j+" focusin"+j),n.ev.off(j),n.wrap.attr("class","mfp-wrap").removeAttr("style"),n.bgOverlay.attr("class","mfp-bg"),n.container.attr("class","mfp-container"),n.st.showCloseBtn&&(!n.st.closeBtnInside||n.currTemplate[n.currItem.type]===!0)&&n.currTemplate.closeBtn&&n.currTemplate.closeBtn.detach(),n.st.autoFocusLast&&n._lastFocusedEl&&a(n._lastFocusedEl).focus(),n.currItem=null,n.content=null,n.currTemplate=null,n.prevHeight=0,y(d)},updateSize:function(a){if(n.isIOS){var b=document.documentElement.clientWidth/window.innerWidth,c=window.innerHeight*b;n.wrap.css("height",c),n.wH=c}else n.wH=a||r.height();n.fixedContentPos||n.wrap.css("height",n.wH),y("Resize")},updateItemHTML:function(){var b=n.items[n.index];n.contentContainer.detach(),n.content&&n.content.detach(),b.parsed||(b=n.parseEl(n.index));var c=b.type;y("BeforeChange",[n.currItem?n.currItem.type:"",c]),n.currItem=b;if(!n.currTemplate[c]){var d=n.st[c]?n.st[c].markup:!1;y("FirstMarkupParse",d),d?n.currTemplate[c]=a(d):n.currTemplate[c]=!0}t&&t!==b.type&&n.container.removeClass("mfp-"+t+"-holder");var e=n["get"+c.charAt(0).toUpperCase()+c.slice(1)](b,n.currTemplate[c]);n.appendContent(e,c),b.preloaded=!0,y(h,b),t=b.type,n.container.prepend(n.contentContainer),y("AfterChange")},appendContent:function(a,b){n.content=a,a?n.st.showCloseBtn&&n.st.closeBtnInside&&n.currTemplate[b]===!0?n.content.find(".mfp-close").length||n.content.append(z()):n.content=a:n.content="",y(e),n.container.addClass("mfp-"+b+"-holder"),n.contentContainer.append(n.content)},parseEl:function(b){var c=n.items[b],d;c.tagName?c={el:a(c)}:(d=c.type,c={data:c,src:c.src});if(c.el){var e=n.types;for(var f=0;f<e.length;f++)if(c.el.hasClass("mfp-"+e[f])){d=e[f];break}c.src=c.el.attr("data-mfp-src"),c.src||(c.src=c.el.attr("href"))}return c.type=d||n.st.type||"inline",c.index=b,c.parsed=!0,n.items[b]=c,y("ElementParse",c),n.items[b]},addGroup:function(a,b){var c=function(c){c.mfpEl=this,n._openClick(c,a,b)};b||(b={});var d="click.magnificPopup";b.mainEl=a,b.items?(b.isObj=!0,a.off(d).on(d,c)):(b.isObj=!1,b.delegate?a.off(d).on(d,b.delegate,c):(b.items=a,a.off(d).on(d,c)))},_openClick:function(b,c,d){var e=d.midClick!==undefined?d.midClick:a.magnificPopup.defaults.midClick;if(!e&&(b.which===2||b.ctrlKey||b.metaKey||b.altKey||b.shiftKey))return;var f=d.disableOn!==undefined?d.disableOn:a.magnificPopup.defaults.disableOn;if(f)if(a.isFunction(f)){if(!f.call(n))return!0}else if(r.width()<f)return!0;b.type&&(b.preventDefault(),n.isOpen&&b.stopPropagation()),d.el=a(b.mfpEl),d.delegate&&(d.items=c.find(d.delegate)),n.open(d)},updateStatus:function(a,b){if(n.preloader){q!==a&&n.container.removeClass("mfp-s-"+q),!b&&a==="loading"&&(b=n.st.tLoading);var c={status:a,text:b};y("UpdateStatus",c),a=c.status,b=c.text,n.preloader.html(b),n.preloader.find("a").on("click",function(a){a.stopImmediatePropagation()}),n.container.addClass("mfp-s-"+a),q=a}},_checkIfClose:function(b){if(a(b).hasClass(m))return;var c=n.st.closeOnContentClick,d=n.st.closeOnBgClick;if(c&&d)return!0;if(!n.content||a(b).hasClass("mfp-close")||n.preloader&&b===n.preloader[0])return!0;if(b!==n.content[0]&&!a.contains(n.content[0],b)){if(d&&a.contains(document,b))return!0}else if(c)return!0;return!1},_addClassToMFP:function(a){n.bgOverlay.addClass(a),n.wrap.addClass(a)},_removeClassFromMFP:function(a){this.bgOverlay.removeClass(a),n.wrap.removeClass(a)},_hasScrollBar:function(a){return(n.isIE7?s.height():document.body.scrollHeight)>(a||r.height())},_setFocus:function(){(n.st.focus?n.content.find(n.st.focus).eq(0):n.wrap).focus()},_onFocusIn:function(b){if(b.target!==n.wrap[0]&&!a.contains(n.wrap[0],b.target))return n._setFocus(),!1},_parseMarkup:function(b,c,d){var e;d.data&&(c=a.extend(d.data,c)),y(f,[b,c,d]),a.each(c,function(c,d){if(d===undefined||d===!1)return!0;e=c.split("_");if(e.length>1){var f=b.find(j+"-"+e[0]);if(f.length>0){var g=e[1];g==="replaceWith"?f[0]!==d[0]&&f.replaceWith(d):g==="img"?f.is("img")?f.attr("src",d):f.replaceWith(a("<img>").attr("src",d).attr("class",f.attr("class"))):f.attr(e[1],d)}}else b.find(j+"-"+c).html(d)})},_getScrollbarSize:function(){if(n.scrollbarSize===undefined){var a=document.createElement("div");a.style.cssText="width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;",document.body.appendChild(a),n.scrollbarSize=a.offsetWidth-a.clientWidth,document.body.removeChild(a)}return n.scrollbarSize}},a.magnificPopup={instance:null,proto:o.prototype,modules:[],open:function(b,c){return A(),b?b=a.extend(!0,{},b):b={},b.isObj=!0,b.index=c||0,this.instance.open(b)},close:function(){return a.magnificPopup.instance&&a.magnificPopup.instance.close()},registerModule:function(b,c){c.options&&(a.magnificPopup.defaults[b]=c.options),a.extend(this.proto,c.proto),this.modules.push(b)},defaults:{disableOn:0,key:null,midClick:!1,mainClass:"",preloader:!0,focus:"",closeOnContentClick:!1,closeOnBgClick:!0,closeBtnInside:!0,showCloseBtn:!0,enableEscapeKey:!0,modal:!1,alignTop:!1,removalDelay:0,prependTo:null,fixedContentPos:"auto",fixedBgPos:"auto",overflowY:"auto",closeMarkup:'<button title="%title%" type="button" class="mfp-close">&#215;</button>',tClose:"Close (Esc)",tLoading:"Loading...",autoFocusLast:!0}},a.fn.magnificPopup=function(b){A();var c=a(this);if(typeof b=="string")if(b==="open"){var d,e=p?c.data("magnificPopup"):c[0].magnificPopup,f=parseInt(arguments[1],10)||0;e.items?d=e.items[f]:(d=c,e.delegate&&(d=d.find(e.delegate)),d=d.eq(f)),n._openClick({mfpEl:d},c,e)}else n.isOpen&&n[b].apply(n,Array.prototype.slice.call(arguments,1));else b=a.extend(!0,{},b),p?c.data("magnificPopup",b):c[0].magnificPopup=b,n.addGroup(c,b);return c};var C="inline",D,E,F,G=function(){F&&(E.after(F.addClass(D)).detach(),F=null)};a.magnificPopup.registerModule(C,{options:{hiddenClass:"hide",markup:"",tNotFound:"Content not found"},proto:{initInline:function(){n.types.push(C),w(b+"."+C,function(){G()})},getInline:function(b,c){G();if(b.src){var d=n.st.inline,e=a(b.src);if(e.length){var f=e[0].parentNode;f&&f.tagName&&(E||(D=d.hiddenClass,E=x(D),D="mfp-"+D),F=e.after(E).detach().removeClass(D)),n.updateStatus("ready")}else n.updateStatus("error",d.tNotFound),e=a("<div>");return b.inlineElement=e,e}return n.updateStatus("ready"),n._parseMarkup(c,{},b),c}}});var H,I=function(){return H===undefined&&(H=document.createElement("p").style.MozTransform!==undefined),H};a.magnificPopup.registerModule("zoom",{options:{enabled:!1,easing:"ease-in-out",duration:300,opener:function(a){return a.is("img")?a:a.find("img")}},proto:{initZoom:function(){var a=n.st.zoom,d=".zoom",e;if(!a.enabled||!n.supportsTransition)return;var f=a.duration,g=function(b){var c=b.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"),d="all "+a.duration/1e3+"s "+a.easing,e={position:"fixed",zIndex:9999,left:0,top:0,"-webkit-backface-visibility":"hidden"},f="transition";return e["-webkit-"+f]=e["-moz-"+f]=e["-o-"+f]=e[f]=d,c.css(e),c},h=function(){n.content.css("visibility","visible")},i,j;w("BuildControls"+d,function(){if(n._allowZoom()){clearTimeout(i),n.content.css("visibility","hidden"),e=n._getItemToZoom();if(!e){h();return}j=g(e),j.css(n._getOffset()),n.wrap.append(j),i=setTimeout(function(){j.css(n._getOffset(!0)),i=setTimeout(function(){h(),setTimeout(function(){j.remove(),e=j=null,y("ZoomAnimationEnded")},16)},f)},16)}}),w(c+d,function(){if(n._allowZoom()){clearTimeout(i),n.st.removalDelay=f;if(!e){e=n._getItemToZoom();if(!e)return;j=g(e)}j.css(n._getOffset(!0)),n.wrap.append(j),n.content.css("visibility","hidden"),setTimeout(function(){j.css(n._getOffset())},16)}}),w(b+d,function(){n._allowZoom()&&(h(),j&&j.remove(),e=null)})},_allowZoom:function(){return n.currItem.type==="image"},_getItemToZoom:function(){return n.currItem.hasSize?n.currItem.img:!1},_getOffset:function(b){var c;b?c=n.currItem.img:c=n.st.zoom.opener(n.currItem.el||n.currItem);var d=c.offset(),e=parseInt(c.css("padding-top"),10),f=parseInt(c.css("padding-bottom"),10);d.top-=a(window).scrollTop()-e;var g={width:c.width(),height:(p?c.innerHeight():c[0].offsetHeight)-f-e};return I()?g["-moz-transform"]=g.transform="translate("+d.left+"px,"+d.top+"px)":(g.left=d.left,g.top=d.top),g}}}),A()})