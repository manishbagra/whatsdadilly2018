gg_rtl=false;
gg_columnized_max_w=260;
gg_masonry_min_w=150;
gg_phosostr_min_w=120;
gg_coll_max_w=400;
gg_preload_hires_img=false;
gg_use_deeplink=true;
gg_monopage_filter=false;
gg_back_to_gall_scroll=false;
gg_galleria_toggle_info=false;
gg_galleria_fx='fadeslide';
gg_galleria_fx_time=400;
gg_galleria_img_crop=true;
gg_galleria_autoplay=false;
gg_galleria_interval=3000;
gg_delayed_fx=true;


pc_ms_countSelected="# of % selected";
pc_ms_allSelected="All selected";
pc_html5_validation=false;
pc_ms_search_placeh="Search options";
pc_ms_search_nores="No matches found";


mg_boxMargin=4;
mg_boxBorder=1;
mg_imgPadding=5;
mg_delayed_fx=true;
mg_filters_behav='standard';
mg_monopage_filter=false;
mg_lightbox_mode="mg_classic_lb";
mg_lb_carousel=true;
mg_lb_touchswipe=true;
mg_audio_loop=true;
mg_rtl=false;
mg_mobile=800;
mg_deeplinked_elems=['item','category','search','page'];
mg_full_deeplinking=false;mg_galleria_fx='fadeslide';
mg_galleria_fx_time=500;
mg_galleria_interval=3500;
mg_inl_slider_fx='zoom-out';
mg_inl_slider_easing='ease';
mg_inl_slider_fx_time=500;
mg_inl_slider_intval=3000;
mg_inl_slider_play_btn=true;
mg_inl_slider_pause_on_h=true;
mg_inl_slider_touch=true;
mg_kenburns_timing=8400;





pcpp_renew_is_acting="<em>Creating order..</em>";
pcpp_renew_ok="<em>Order successfully created!</em>";
pcpp_failed_renew="<em>Problem creating order, try again</em>";

nb_lightbox=true;
nb_touchswipe=true;
nb_min_news_h=130;
nb_min_news_w=200;
nb_min_horiz_w=400;
nb_read_more_txt="..";
nb_fb_share_fix="video-lightbox/wp-content/plugins/swift-box-wp/lcis_fb_img_fix.html";
nb_script_basepath="wp-content/plugins/swift-box-wp/js/sb/index.html";
nb_short_d_names=["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
nb_full_d_names=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
nb_short_m_names=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
nb_full_m_names=["January","February","March","April","May","June","July","August","September","October","November","December"];
nb_elapsed_names=["ago","seconds","minute","minutes","hour","hours","day","days","week","weeks","month","months"];

jQuery(document).ready(function($){
		if(typeof(gg_galleries_init)=="function"){
		gg_galleries_init("5ab884825d316");
		}
});


gg_no_lb=true;
jQuery(document).ready(function(){
jQuery('.gid_5501 .gg_img').each(
function(){

if(jQuery(this).data('img-id')!=5){
var src=jQuery(this).data('gg-link');


jQuery(this).data('lcl-path',src);}});
jQuery('.gg_gallery_wrap:not(.gid_5501) .gg_img').each(function(){
var src=jQuery(this).data('gg-url');
jQuery(this).data('lcl-path',src);});
lc_lightbox('.gid_5499 .gg_img',{wrap_class:'lcl_rtl_oc',
gallery:true,
gallery_hook:false,
global_type:'image',
preload_all:true,
deeplink:true,
img_zoom:true,
counter:true,
fullscreen:true,
socials:true,
fb_direct_share:'425190344259188',
download:true,
radius:3,
src_attr:'data-gg-url',
title_attr:'data-gg-title',
txt_attr:'data-gg-descr',
author_attr:'data-gg-author',
skin:'dark',

thumbs_maker_url:'',});

var $mc_elems=jQuery('.gid_5501 .gg_img');

$mc_elems.removeClass('gg_linked_img');

$mc_elems.eq(1).data('lcl-poster','auto');

lc_lightbox(
'.gid_5501 .gg_img',
{wrap_class:'lcl_rtl_oc',
gallery:true,
gallery_hook:false,
preload_all:true,
max_width:'85%',
max_height:'85%',
comments:{type:'disqus',shortname:'lcweb-1'},
deeplink:true,
img_zoom:true,
fullscreen:true,
socials:true,
fb_direct_share:'425190344259188',
download:true,
src_attr:'data-gg-link',
title_attr:'data-gg-title',
txt_attr:'data-gg-descr',
author_attr:'data-gg-author',
data_position:'rside',
skin:'minimal',
thumbs_maker_url:'',
});
});

gg_lcl_allow_deeplink=function(){
if(typeof(gg_no_lb)!='undefined'){return false;}
if(typeof(lcl_gg_nulling_prepare)!='undefined'){clearTimeout(lcl_gg_nulling_prepare);}

lcl_gg_prepare=true;jQuery('.gg_carousel_wrap, .gg_container:not(.gg_coll_container)').each(function(){

jQuery(this).find('.gg_img:not(.gg_linked_img, .gg_excluded_img)').first().trigger('click');

});

jQuery('.galleria-gg-lightbox').each(function(){


jQuery(this).trigger('click');

});

};


jQuery(document).ready(function(e){gg_lcl_allow_deeplink();});

var gg_lb_thumb=function(src){return'<?php echo URL; ?>video-lightbox/wp-content/plugins/global-gallery/classes/easy_wp_thumbs.php?src='+encodeURIComponent(src)+'&w=100&h=100';};

gg_throw_lb=function(gall_obj,rel,clicked_index,no_deeplink){

if(!Object.keys(gall_obj).length){return false;}


if(jQuery('#gg_lb_gall').length){jQuery('#gg_lb_gall').empty();}
else{jQuery('body').append('<div id="gg_lb_gall"></div>');}
if(typeof(gg_no_lb)!='undefined'){return false;}
var sel_img=[];
jQuery.each(Object.keys(gall_obj),function(i,v){var obj=gall_obj[v];
var o={src:obj.img,title:obj.title,txt:obj.descr,

author:obj.author,

canonical_url:({type:'disqus',shortname:'lcweb-1'})?"videos.php?lcl_canon="+encodeURIComponent(obj.img):false};

sel_img.push(o);})

var lcl_obj=lc_lightbox(sel_img,{deeplink:(typeof(no_deeplink)=='undefined')?true:false,img_zoom:true,global_type:'image',wrap_class:'lcl_bottop_v2_oc',slideshow:true,open_close_time:450,animation_time:220,slideshow_time:7000,autoplay:true,counter:true,progressbar:true,max_width:'95%',max_height:'93%',ol_opacity:0.9,ol_color:'#303030',ol_pattern:'ver-line-10',border_w:0,border_col:'#b2b2b2',padding:0,radius:3,shadow:true,remove_scrollbar:false,skin:'minimal',data_position:'rside',cmd_position:'inner',ins_close_pos:'corner',nav_btn_pos:'normal',txt_hidden:500,thumbs_nav:true,tn_hidden:500,thumbs_w:100,thumbs_h:100,

thumbs_maker_url:'<?php echo URL; ?>video-lightbox/wp-content/plugins/global-gallery/classes/easy_wp_thumbs.php?src=%URL%&w=%W%&h=%H%&q=80',

fullscreen:true,fs_only:700,socials:true,fb_direct_share:true,comments:{type:'disqus',shortname:'lcweb-1'},download:true,rclick_prevent:false,html_is_ready:function(){jQuery.each(this.vars.elems,function(i,v){v.download=v.src;});}});if(typeof(lcl_gg_prepare)=='undefined'||!lcl_gg_prepare||typeof(no_deeplink)!='undefined'){lcl_open(lcl_obj,clicked_index);}

else{if(typeof(lcl_gg_nulling_prepare)!='undefined'){clearTimeout(lcl_gg_nulling_prepare);}

lcl_gg_nulling_prepare=setTimeout(function(){lcl_gg_prepare=false;},150);}

};
	
	
	