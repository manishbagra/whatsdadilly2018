 <?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'bootstrap.php';
require_once 'entities/Friend.php';
require_once 'entities/Notification.php';
require_once 'classes/Session.class.php';
$session           = new Session();
$userId            = $session->getSession("sign_uid");
$contacts          = json_decode($_REQUEST['people']);
$arr['requests']   = '';
$arr['inviations'] = '';
$r                 = $i = 0;
$arr               = array(
    'requests' => '',
    'inviations' => '',
    'requestsCount' => $r,
    'inviationsCount' => $i
);
foreach ($contacts as $contact) {
    
    $name  = $contact->name != '' ? $contact->name : '---';
    $dql   = "SELECT b FROM UserContact b WHERE b.email = ?1 AND b.user_id = ?2";
    $query = $entityManager->createQuery($dql);
    $query->setParameter(1, $contact->email);
    $query->setParameter(2, $session->getSession("sign_uid"));
    $user_id = $query->getArrayResult();
    if (!count($user_id)) {
        $contactObj = new UserContact();
        $contactObj->setEmail($contact->email);
        $contactObj->setUser_id($session->getSession("sign_uid"));
        $entityManager->getConnection()->beginTransaction();
        try {
            $res = $entityManager->persist($contactObj);
            $entityManager->flush();
            $entityManager->getConnection()->commit();
        }
        catch (Exception $e) {
            $entityManager->getConnection()->rollback();
            $entityManager->close();
            throw $e;
        }
    }
    $params2 = array(
        'email' => $contact->email
    );
    $user    = $entityManager->getRepository('UserRegister')->findOneBy($params2);
    
    if ($user instanceof UserRegister) {
        $username   = $user->getFirstName();
        $userEmail  = $user->getEmail();
        $userId     = $user->getUser_id();
        $profilePic = $user->getProfilePic();
        ++$r;
        $arr['requests'] .= "<div class='profile_box'>
                            <img src='uploads/$profilePic' width='51' height='51' /> <span><strong style='margin-right:20px;'>$username</strong> $userEmail</span> <input name='invtiations[]' type='checkbox' value='$userId' checked='checked' />
                        </div>";
        
    } else {
        ++$i;
        $arr['inviations'] .= "<div class='profile_box1'><input type='checkbox' name='invtiations[]' value='$contact->email&$name&$userId' checked='checked' /> <span>$name</span><a href='#'>$contact->email</a></div>";
    }
}
$arr['requestsCount']   = $r;
$arr['inviationsCount'] = $i;
echo json_encode($arr);
?> 