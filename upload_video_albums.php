 <?php
error_reporting(-1);

require 'bootstrap.php';
require_once 'model/Wall.php';
require_once 'model/VideosModel.php';
require_once 'classes/Session.class.php';

$session = new Session();

$video_model = new VideosModel();

$WallModel = new WallModel();

$imgname_array = array();

if (!empty($_POST)) {
    
    
    if (trim($_POST['imgN_array']) != "") {
        
        $imgname_array = explode('@DadillY@', $_POST['imgN_array']);
        
        $total_images = count($imgname_array);
        
        $imgTname_array = explode('@DadillY@', $_POST['imgTN_array']);
        
        $imgSrc_array = explode('@DadillY@', $_POST['vSrc_array']);
        
        $title_array = explode('@DadillY@', $_POST['title_array']);
        
        $descrip_array = explode('@DadillY@', $_POST['descrip_array']);
        
        $type_array = explode('@DadillY@', $_POST['type_array']);
        
        $tags_array = explode('@DadillY@', $_POST['tags_array']);
        
        $category_array = explode('@DadillY@', $_POST['category_array']);
        
        $vDesc_array = explode('@DadillY@', $_POST['vDesc_array']);
        
        
        $userid = $session->getSession('userid');
        
        $output_dir = __DIR__ . "/uploads/videos/" . $userid;
        
        $dir = __DIR__ . "/uploads/video/videomp4/";
        
        if (!file_exists($output_dir)) {
            
            mkdir($output_dir, 0777, true);
            
            mkdir($output_dir . "/thumbnails", 0777, true);
            
        }
        
        
        $thumbind = "";
        
        $wall_id = "";
        
        for ($i = 0; $i < $total_images; $i++) {
            
            $expl_thumb = explode("/", $imgSrc_array[$i]);
            
            $thumbind = "uploads/videos/" . $userid . "/thumbnails/" . $expl_thumb[4];
            
            $currentDT = date("Y-m-d h:m:s");
            
            $params = array(
                'user_id' => $userid,
                'file' => "uploads/videos/" . $userid . "/" . $imgname_array[$i],
                'title' => addslashes($title_array[$i]),
                'date' => $currentDT,
                'tags' => addslashes($tags_array[$i]),
                'cat' => addslashes($category_array[$i]),
                'description' => addslashes($descrip_array[$i]),
                'privacy' => $type_array[$i],
                'thumbnail' => $thumbind
            );
            
            if ($i === 0) {
                
                $lastId = $WallModel->addVideosTitle($userid, '', $currentDT, $entityManager);
                
                $wall_id = $lastId;
            }
            
            $result = $video_model->addVideoDetails($wall_id, $params, $entityManager);
            
            if (@copy($dir . $imgname_array[$i], $output_dir . "/" . $imgname_array[$i]) && @copy($dir . "thumbnails/" . $expl_thumb[4], $output_dir . "/thumbnails/" . $expl_thumb[4])) {
                
                if (unlink($dir . $imgname_array[$i]) && unlink($imgSrc_array[$i])) {
                    continue;
                } else {
                    echo "Del Failed";
                    exit;
                }
            }
            
            
        }
        
        
        
        $message = array(
            "flag" => "1"
        );
        
    } else {
        $message = array(
            "flag" => 0
        );
    }
    
    echo json_encode($message);
    exit();
    
} else {
    echo "Failed";
    exit;
} 