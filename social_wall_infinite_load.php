<script type="text/javascript" src="js/wall.js"></script>
<?php
session_start();
require_once "bootstrap.php";
require_once 'model/Wall.php';
require_once 'model/PhotosModel.php';
require_once 'model/Comments.php';
include 'html/wall/functions.php';

$wall_id = $_GET['wall_id'];

$limit1 = $_GET['count'] + 1;

$limit2 = $_GET['count'] + 11;

$data = null;

if (isset($_GET['profileid']) && $_GET['profileid'] != undefined) {
    
    $data = [
        "userid" => base64_decode($_GET['profileid'])
    ];

    
    $entries = WallModel::getUserInfiniteWall($entityManager, $limit1, $limit2, $data);
} else {
    
    $entries = WallModel::getInfiniteWall($entityManager, $limit1, $limit2, $data);
    
}

$count = count($entries);

if ($count != 1) {
    foreach ($entries as $key => $value) {
        $comments      = CommentsModel::getCommentsMainPostWithLimit($entries[$key]['id'], 0, 5, $entityManager);
        $totalComments = count(CommentsModel::getCountCommentsMainPost($entries[$key]['id'], $entityManager));
        
        $photos = PhotosModel::getPhotos($entries[$key]['id'], $entityManager);
        
        foreach ($comments as $keyId => $commentVal) {
            $commentId                   = $commentVal['id'];
            $replies                     = CommentsModel::getCommentOfReply($commentId, $entityManager);
            $comments[$keyId]['replies'] = $replies;
            
        }
        
        $entries[$key]['comments_count'] = $totalComments;
        $entries[$key]['comments']       = array_reverse($comments);
        
        $entries[$key]['photos'] = $photos;
    }
?>
   <?php
    
    $postids = $_GET['count'];
    
    foreach ($entries as $entry) {
        
        $userdetails = WallModel::getUserDetails($entityManager, $entry['user_id']);
        
        if ($entry['type'] == 1) {
            $userdetails = WallModel::getUserDetails($entityManager, $entry['author_id']);
            $infodetails = WallModel::getUserDetails($entityManager, $entry['owner_id']);
        } else {
            $userdetails = WallModel::getUserDetails($entityManager, $entry['user_id']);
        }
?>

<div class="post_img_box crispbxmain" style='padding:10px;' id="wall<?php
        echo $entry['id'];
?>" data="<?php
        echo $entry['id'];
?>" data-count="<?php
        echo $postids;
?>">
                                <div class="media posta">
                                  <div class="media-left media-middle">
                                    <a href="#">
                                    <?php
        if ($userdetails[0]['profile_pic'] == '') {
?>
                                   <img src="uploads/default/Maledefault.png" alt="" style="width:49px;height:49px;max-width:none !important;"/>
                                    <?php
        } else {
?>
                                     <img class="media-object" src="uploads/<?php
            echo $userdetails[0]['profile_pic'];
?>" alt="" style="width:49px;height:49px;max-width:none !important;" />
                                    <?php
        }
?>
                                   </a>
                                  </div>
                                  <div class="media-body my">
                                    <h4 class="media-heading another"><a href="profilesummery.php?profileid=<?php
        echo base64_encode($userdetails[0]['user_id']);
?>" onClick="return hs.htmlExpand(this, { objectType: 'ajax'} )" ><?php
        echo $userdetails[0]['firstname'] . ' ' . $userdetails[0]['lastname'];
?></a></h4>
                                    
                                    <p><?php
        echo WallModel::time_elapsed_string($entry['date']);
?></p>
                                    
                                  </div>
                                </div>
                                
                                    <div style="width:100%;padding:10px;">
                                                <p style="font-size:15px;">
                                                <?php
        echo $entry['text'];
?>
                                               </p>
                                    </div>
                                        <?php
        if (strlen($entry['link']) > 0) {
?>
                                           <div class="display_profile_pic" style='width:97.5%;border: 1px solid #ccc;box-shadow: 0 0 1px #ccc;line-height: 16px;overflow: hidden;border-radius:4px;'>
                                                <?php
            if (strlen($entry['link_photo']) > 0):
?>
                                                   <a target="_blank" href="<?php
                echo $entry['link'];
?>">
                                                    <img src="<?php
                echo $entry['link_photo'];
?>" alt=""/>
                                                    </a>
                                                <?php
            endif;
?>

                                                <div style="padding:10px;">
                                                    <p><a target="_blank" href="<?php
            echo $entry['link'];
?>"
                                                          style="color: #000;font-size: 18pt;line-height: 1.2em;"><span style='font-size:20px;font-family: TimesNewRoman,Times New Roman,Times,Baskerville,Georgia,serif;line-height: 20px;text-align:justify;'><?php
            echo $entry['link_title'];
?></span></a>
                                                    </p>
                                                    <p style="font-size:12pt;font-family: TimesNewRoman,Times New Roman,Times,Baskerville,Georgia,serif;text-align:justify;"><?php
            echo $entry['link_description'];
?></p>
                                                </div>
                                            </div>
                                        <?php
        }
?>
                           
                                
                                
                <?php
        if (!empty($entry['photos'])) {
?>
                           
                                
                        <?php
            $photoDiv = '';
            $i        = 0;
            if (count($entry['photos']) <= 9) {
                $linhas = floor(count($entry['photos']) / 3);
                $resto  = count($entry['photos']) % 3;
                
                $tam1h = '143px';
                $tam2h = '216.5px';
                $tam3h = '436px';
                
                $tam1w = '32.8%';
                $tam2w = '49.2%';
                $tam3w = '99.1%';
                
            }
            $photoDiv .= '<div class="full_post_img">';
            $i = 1;
            foreach ($entry['photos'] as $path) {
                
                if ($i <= ($linhas * 3)) {
                    $tamAtual  = $tam1w;
                    $tamAtualh = $tam1h;
                    
                } else {
                    if ($resto == 2) {
                        $tamAtual  = $tam2w;
                        $tamAtualh = $tam2h;
                        
                    } else {
                        $tamAtual  = $tam3w;
                        $tamAtualh = $tam3h;
                    }
                    
                }
                
                $margin = 'margin: 2px 0 0 2px';
                if (count($entry['photos']) == 1) {
                    $margin    = 'margin: 0';
                    $tamAtualh = '100%';
                    $tamAtual  = '100%';
                }
                $photoDiv .= '<a class="mid2wall" href="photo_detail.php?id=' . $path['id'] . '" ><img  style="width: ' . $tamAtual . '; height: ' . $tamAtualh . '; border-radius:px;' . $margin . ';border:1px solid #ccc !important;"  src="uploads/' . $path['file'] . '"/></a>';
                $i++;
                if ($i >= 9 && count($entry['photos']) > 9) {
                    $photoDiv .= ' and ' . (count($entry['photos']) - 9) . ' more photos';
                    break;
                }
                
            }
            $photoDiv .= '</div>';
            
            echo $photoDiv;
            
            $photoDiv = '';
            
?>
                                   
          

               <!-- <div class="upsmimg">

                    <?php
            // $postion = 1;
            // foreach ($entry['photos'] as $key => $photo) {
            // if ($key == 0)
            // continue;
?>

                        
                    <?php //} 
?>
                </div> -->
                
                        <?php
        }
?>
                               
                                    <?php
        if (!empty($entry['videos'])) {
            $poster = 'uploads/videos/thumbnails/' . $entry['videos'][0]['id'] . '/thumb_1.jpg';
            if ($entry['videos'][0]['thumbnail']) {
                $poster = $entry['videos'][0]['thumbnail'];
            }
            $ext = pathinfo($entry['videos'][0]['file'], PATHINFO_EXTENSION);
            
            $countViews = $videoObj->countVideoViews($entityManager, $entry['videos'][0]['id']);
            
?>
                                   
                                    
                                    
                                   <video  style='width:100% !important;height:400px !important;' id="my-video|<?php
            echo $entry['videos'][0]['id'];
?>" controls 
                                    class="video-js  vjs-default-skin" 
                                    preload="auto"
                                    poster="<?php
            echo $poster;
?>" 
                                    data-setup='{ "aspectRatio":"16:9", "playbackRates": [2, 1.5, 1.25, 1, 0.5, 0.25 ] }' >
                                    <source src="<?php
            echo $entry['videos'][0]['file'];
?>"  type='video/<?php
            echo $ext;
?>' />
                                    </video>
                                    <p></p>
                                    <span style="color:#000;font-size:15px;text-shadow:none;"><b>Views</b>&nbsp;&nbsp;</span><span class='count_views'   style="opacity:0.5;font-size:13px;font-weight:bold" ><i  class="fa fa-play"></i>&nbsp;&nbsp;<span class="<?php
            echo $entry['videos'][0]['id'];
?>" value="<?php
            echo @$countViews;
?>"><?php
            echo @$countViews;
?></span></span>
                            
                                        <?php
            echo '<br>' . $entry['videos'][0]['description'];
            if (trim($entry['videos'][0]['tags']) != '') {
                echo '<br> Tags: ' . $entry['videos'][0]['tags'] . ',' . $entry['videos'][0]['tags_more'];
            }
            if (trim($entry['videos'][0]['tags_more']) != '') {
                echo ',' . $entry['videos'][0]['tags_more'];
            }
            
?>
                                       <?php
        }
?>
                               
                            
                                        
                            
                                
                                <div class="post_review">
                                    <a href=""> <span><i class="fa fa-thumbs-o-up"></i></span> like</a>
                                    <a href=""> <span><i class="fa fa-heart-o"></i></span> Love It</a>
                                    <a href=""> <span><i class="fa fa-commenting"></i></span> Comment</a>
                                    <a href=""> <span><i class="fa fa-share-square-o"></i></span> Share</a>
                                </div>
                                <div class="post_load_more" >
                                    <div class="load_more text-center">
                                        <?php
        $setLimit                = 2;
        $entry['comments_count'] = 6;
        if ($entry['comments_count'] > 5) {
            echo "<div id='loadmore_" . $entry['id'] . "' class='loadmorecomment'>
                                                     <a onclick='loadMore(" . $entry['id'] . ",5," . $setLimit . ")'>Load More (" . $setLimit . ")</a>
                                                     <span class='loadspin hide'><img src='images/loader.gif'></span>
                                                 </div>";
        }
?>
                                       
                                    </div>
                                </div>
                                
                                <?php
        $profile_pic = 'uploads/' . $_SESSION['profile_pic'];
        
        if ($_SESSION['profile_pic'] == '') {
            
            $profile_pic = "uploads/default/Maledefault.png";
            
        }
        
        foreach ($entry['comments'] as $comment) {
            
            $profile_pic = 'uploads/' . $comment['profile_pic'];
            if ($comment['profile_pic'] == '') {
                $profile_pic = "uploads/default/Maledefault.png";
                
            }
            
?>
                               
                                
                                <div class="media new" id="post_comment_<?php
            echo $entry['id'];
?>">
                                  <div class="media-left media-middle">
                                    <a href="#">
                                      <img class="media-object" style='width:48px;height:47px;margin-bottom:41px;max-width:none !important' src="<?php
            echo $profile_pic;
?>" alt="...">
                                    </a>
                                  </div>
                                  <div class="media-body lefts">
                                    <h4 class="media-heading"><?php
            echo $comment['firstname'] . ' ' . $comment['lastname'];
?></h4>
                                     <span><?php
            echo $comment['text'];
?></span>
                                     <br/>
                                    <span><a href="">like </a> <a href="">reply</a> <p><?php
            echo date('F j, Y, g:i a', strtotime($comment['date']));
?></p></span>
                                    
                                  </div>
                                </div>
                            
                                     <?php
        }
        $profile_pic = 'uploads/' . $_SESSION['profile_pic'];
        
        if ($_SESSION['profile_pic'] == '') {
            
            $profile_pic = "uploads/default/Maledefault.png";
            
        }
        
?>
                               <div class="new_commetn_box" id="comment_box_<?php
        echo $entry['id'];
?>">
                                    <div class="imge_left">
                                    
                                        <img class='media-object' style='width:48px;height:47px;' src="<?php
        echo $profile_pic;
?>" alt="" />
                                        
                                    </div>
                                    <div class="text_box">
                                        <form action="javascript:void(0)">
                                            <input type="text" id="postid_<?php
        echo $entry['id'];
?>" class='comment_input' placeholder="Write your comment here" /> 
                                            <i class="fa fa-camera"></i>
                                        </form>
                                        <a href="">Press: Enter your post</a>
                                    </div>
                                </div>
                            </div>

                            <?php
        $postids++;
    }
?>
                            <div class="tweet_list" id="tweet_list"> 
                              <div id="loadorders"></div>
                              <div id="loadMoreComments" style="display:none;" ></div>
                              <p id='last'></p>
                              </div>
                            <?php
    unset($_SESSION['reach_end']);
} else if ($_SESSION['reach_end'] != true) {
    $_SESSION['reach_end'] = true;
?>
                           <center><h2>There is no more post</h2></center>
                            <?php
}
?>