<?php
include_once("config.php");
include_once("twitteroauth/twitteroauth.php");
include_once("instagramoauth/instagram.class.php");
require_once "bootstrap.php";
require_once 'model/Signup.php';
require_once 'model/Twitter.php';
require_once 'model/Instagram.php';
require_once 'classes/Session.class.php';
require_once 'youtube/Google/autoload.php';
require_once 'youtube/Google/Client.php';
require_once 'youtube/Google/Service/YouTube.php';

$insta   = new Insta();
$session = new Session();

$data['userid'] = $session->getSession("userid");


//Twitter ...
$getAcc = Twitter::getAllTwitterAccounts($data, $entityManager);

$ifExist = count($getAcc);

if ($ifExist == 1) {
    $twitter_count = 1;
    $twitter_url   = 'javascript:void(0)';
    
} else {
    
    $twitter_count = 0;
    
    
    $OAUTH_CALLBACK = 'http://whatsdadilly.com/betaPhaseProjectWDD/get_social_auth_url.php';
    
    $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
    
    $request_token = $connection->getRequestToken($OAUTH_CALLBACK);
    
    $_SESSION['token'] = $request_token['oauth_token'];
    
    $_SESSION['token_secret'] = $request_token['oauth_token_secret'];
    
    if ($connection->http_code == '200') {
        
        $twitter_url = $connection->getAuthorizeURL($request_token['oauth_token']);
        
    } else {
        
        die("error connecting to twitter! try again later!");
    }
    
}





//End Twitter


//Insta ...

$uId = $session->getSession("userid");

$getAllInsta = $insta->getInstaAccount($entityManager, $uId);

$if_exit_insta = count($getAllInsta);

if ($if_exit_insta == 1) {
    $insta_login_url = 'javascript:void(0)';
} else {
    
    $instagram = new Instagram(array(
        'apiKey' => IG_CONSUMER_KEY,
        'apiSecret' => IG_CONSUMER_SECRET,
        'apiCallback' => 'http://whatsdadilly.com/betaPhaseProjectWDD/add_insta_token.php'
    ));
    
    $insta_login_url = $instagram->getLoginUrl();
    
    //End Instagram -------->
    
}


//Youtube ....

$getSocialAccountYoutubeInfo = $insta->checkExistYoutube($entityManager, $uId);

$ifExistYoutube = count($getSocialAccountYoutubeInfo);

$client = new Google_Client();
$client->setClientId(YOUTUBE_CONSUMER_KEY);
$client->setClientSecret(YOUTUBE_CONSUMER_SECRET);
$client->setScopes(array(
    'https://www.googleapis.com/auth/youtube',
    'https://www.googleapis.com/auth/userinfo.email',
    'https://www.googleapis.com/auth/userinfo.profile',
    'https://www.googleapis.com/auth/youtube.force-ssl'
));
$client->setRedirectUri(YOUTUBE_CONSUMER_CALLBACK);

$youtube = new Google_Service_YouTube($client);

$youtube_auth_url = 'javascript:void(0)';

$tokenSessionKey = 'token-' . $client->prepareScopes();



include 'html/social_network_add.php';