<?php
error_reporting(-1); 
require_once "bootstrap.php";

require_once 'model/Signup.php';

require_once 'model/Twitter.php';

require_once 'model/Instagram.php';

require_once 'model/Wall.php';

require_once 'classes/Session.class.php';

require_once 'model/PhotosModel.php';

require_once "model/Profile.php";

require_once 'model/VideosModel.php';

require_once 'model/Audio.php';

require_once 'model/Comments.php';

require_once 'model/Albums.php';

require_once 'model/Notification.php';

include 'html/wall/functions.php';

include_once("config.php");

include_once("twitteroauth/twitteroauth.php");

include_once("instagramoauth/instagram.class.php");

$session 		= new Session();

$insta 			= new Insta();

$function 		= new Functions();

$video_model 	= new VideosModel();

$Albums_obj 	= new Albums(); 

$Profile 		= new Signup();

$AudioModel  	= new AudioModel();

$wall = new WallModel();


$notification_obj = new NotificationModel();


if ($session->getSession("userid") != "" || $session->getSession("userid") != null) {

    if (isset($_GET['profileid']) && @$_GET['acc'] == null) {
		
		

        $twitt_val = '2';

        $data 	= array("userid" => base64_decode($_GET['profileid']));
		
		$_SESSION['profileid'] = base64_decode($_GET['profileid']);
		
		$userid = base64_decode($_GET['profileid']);
		
		$user_info 	= $Profile->profileView($data, $entityManager);
		
		$dates =  date('Y-m-d', strtotime(str_replace('-','/', $user_info[0]['dob'])));
		
		$date = explode('-',$dates);
		
		$work_edu  	   = $Profile->workEducations($data, $entityManager);
		
		
		$contact_info  = $Profile->contactInfo($data, $entityManager);
		
		if($contact_info['email'] != ''){
			$email = explode(',', $contact_info['email']);
		}else{
			$email = array('','');
		}
		
		if($contact_info['phone'] != ''){
			$phone = explode(',', $contact_info['phone']);
		}else{
			$phone = array('','');
		}
		
		
		if($contact_info['website'] != ''){
			$web = explode(',', $contact_info['website']);
		}else{
			$web = array('','');
		}
		
		$cityName = $Profile->getCityName($user_info[0]['current_city'], $entityManager);
		$stateName = $Profile->getStateName($user_info[0]['current_state'], $entityManager);
		$countryName = $Profile->getCountryName($user_info[0]['current_country'], $entityManager);
		
		
		$country 	= $Profile->allCountry($entityManager);
		
		$state 		= $Profile->getState($user_info[0]['current_country'], $entityManager);
		
		$city 		= $Profile->getCity($user_info[0]['current_state'], $entityManager);
		
        $params = array('id_owner' => base64_decode($_GET['profileid']));

        $screen_name = Twitter::getAllTwitterAccounts($data, $entityManager);
		
		if(isset($_GET['whiteboard']) && isset($_GET['comment_id']) && $_GET['whiteboard'] != '' && $_GET['comment_id'] != '' && $_GET['whiteboard'] != null && $_GET['comment_id'] != null) {
			
			$comment_id = $_GET['comment_id'] ;
			
			$notification = $notification_obj->updateNotificationClick($entityManager,$comment_id);
			
			
			$wall_id = $_GET['whiteboard'] ;
			
			
           
			$getRow = $wall->getOneWallRow($wall_id,$entityManager);
			
			//$entries = count(CommentsModel::getCountCommentsMainPost($wall_id, $entityManager));
			
			 $entry = array_slice(WallModel::getUserWallNew($entityManager, $data, $wall_id),0,10);
			
			$entries = array_merge($getRow,$entry);
			
			
			
		}else{
			
			 $entries = array_slice(WallModel::getUserWall($entityManager, $data),0,10);
			 //echo'<pre>';print_r($entries);die;
			
			
		}
		
		
		
		
		
		
		
		$getInstaAcc = $insta->getInstaAccount($entityManager,base64_decode($_GET['profileid']));
		
		$InstaExist   = $insta->getUserSocialAccount($entityManager,base64_decode($_GET['profileid']),'instagram');
		
		$TwitterExist = $insta->getUserSocialAccount($entityManager,base64_decode($_GET['profileid']),'twitter');
		
		
		$getSocialAccountYoutubeInfo = $insta->checkExistYoutube($entityManager,base64_decode($_GET['profileid']));
		
		
		
		$ifInstaExist = count($InstaExist);
		
		$ifTwitterExist = count($TwitterExist);
		
		$ifExistYoutube = count($getSocialAccountYoutubeInfo);
        
        foreach ($entries as $key => $value) {
			
            if(isset($_GET['whiteboard']) && isset($_GET['comment_id']) && $_GET['whiteboard'] != '' && $_GET['comment_id'] != '' && $_GET['whiteboard'] != null && $_GET['comment_id'] != null) {
				
				$totalComments = count(CommentsModel::getCountCommentsMainPost($entries[$key]['id'], $entityManager));
				
				$comments = CommentsModel::getCommentsMainPostWithLimit($entries[$key]['id'],0,$totalComments, $entityManager);
				
			
			}else{
			
			$comments = CommentsModel::getCommentsMainPostWithLimit($entries[$key]['id'],0,5, $entityManager);
             
			 
			}
			$totalComments = count(CommentsModel::getCountCommentsMainPost($entries[$key]['id'], $entityManager));
             
			$photos = PhotosModel::getPhotos($entries[$key]['id'], $entityManager);
             //echo'<pre>';print_r($photos);
			foreach ($comments as $keyId => $commentVal) {

				$commentId = $commentVal['id'];

				$replies = CommentsModel::getCommentOfReply($commentId, $entityManager);

				$comments[$keyId]['replies'] = $replies;

			}

			$entries[$key]['comments_count'] = $totalComments;
            //echo'<pre>';print_r($entries['comments_count']);  
			  
			$entries[$key]['comments'] = array_reverse($comments);

			$entries[$key]['photos'] = $photos;
			if(!empty($entries[$key]['photos']))
		      {
					$iscover = PhotosModel::getIsCover($entries[$key]['photos'][0]['wall_id'], $entityManager);
					$entries[$key]['iscover'] = $iscover;
		      }else{
		        $entries[$key]['iscover'][0]['is_cover'] =0;
		    }

			$entries[$key]['videos'] = $video_model->getVideos($entries[$key]['id'], $entityManager);
			
			$entries[$key]['audios'] = $AudioModel->getAudios($entries[$key]['id'], $entityManager);
			
			$id_album = $Albums_obj->checkAlbum($entries[$key]['id'], $entityManager);
		
		    $entries[$key]['album_photo'] = $Albums_obj->getPhotosByAlbumId($entityManager,$id_album);
		
        }//die;
     
        $screen_length = count($screen_name);

        $function_following = 'load_usersfollowing()';

        $function_followers = 'load_usersfollower()';

        $function_fav = 'load_userfav()';

        $function_mention = 'load_usermentions()';

        $screenname = @$screen_name[0]['screen_name']; 

        $twitterid = @$screen_name[0]['screen_id']; 

        $oauth_token = @$screen_name[0]['auth_token'];

        $oauth_token_secret = @$screen_name[0]['auth_secret'];

        $url = $_SERVER['REQUEST_URI'];

        $str = explode("/", $url);

        $cur_url = $str[count($str) - 1] . '&';

        $pcur_url = $str[count($str) - 1];

    } else if (isset($_GET['acc'])) {
		
		die('2222');

        $twitt_val = '2';

        $data = array(

            "userid" => base64_decode($_GET['profileid'])

        );



        $screen_name = Twitter::getAllTwitterAccounts($data, $entityManager);

        $entries = WallModel::getUserWall($entityManager, $data);

        foreach ($entries as $key => $value) {

            $comments = CommentsModel::getCommentsMainPostWithLimit($entries[$key]['id'],0,5, $entityManager);

			$totalComments = count(CommentsModel::getCountCommentsMainPost($entries[$key]['id'], $entityManager));

			$photos = PhotosModel::getPhotos($entries[$key]['id'], $entityManager);

			$videos = $video_model->getVideos($entries[$key]['id'], $entityManager);

			//Get Looping inside Comments

			foreach ($comments as $keyId => $commentVal) {

				$commentId = $commentVal['id'];

				$replies = CommentsModel::getCommentOfReply($commentId, $entityManager);

				$comments[$keyId]['replies'] = $replies;				

			}

			$entries[$key]['comments_count'] = $totalComments;

			$entries[$key]['comments'] = array_reverse($comments);

			$entries[$key]['photos'] = $photos;

			$entries[$key]['videos'] = $videos;

        }

		

        $screen_length = count($screen_name);

        $function_following = 'load_usersfollowing()';

        $function_followers = 'load_usersfollower()';

        $function_fav = 'load_userfav()';

        $function_mention = 'load_usermentions()';

        $screenname         = $session->getSession("screen_name_twitter");

        $twitterid          = $session->getSession("screen_id_twitter");

        $oauth_token        = $session->getSession("auth_token_twitter");

        $oauth_token_secret = $session->getSession("auth_secret_twitter");

        $url = $_SERVER['REQUEST_URI'];

        $str = explode("/", $url);

        $cur_url = $str[count($str) - 1] . '?';

        $pcur_url = $str[count($str) - 1];

    } else {
		
		die('1111');

        $twitt_val = '1';

        $data = array(

            "userid" => $session->getSession("userid")

        );

        $screen_name = Twitter::getAllTwitterAccounts($data, $entityManager);

        $entries = WallModel::getUserWall($entityManager, $data);

        foreach ($entries as $key => $value) {

			$comments = CommentsModel::getComments($entries[$key]['id'], $entityManager);

            $photos = PhotosModel::getPhotos($entries[$key]['id'], $entityManager);

			$videos = $video_model->getVideos($entries[$key]['id'], $entityManager);

            $entries[$key]['comments'] = $comments;

            $entries[$key]['photos'] = $photos;

			$entries[$key]['videos'] = $videos;

        }

        $screen_length = count($screen_name);

        $function_following = 'load_following()';

        $function_followers = 'load_followers()';

        $function_fav = 'load_userfav()';

        $function_mention = 'load_mentions()';

        $screenname = $session->getSession("screen_name_twitter");

        $twitterid = $session->getSession("screen_id_twitter");

        $oauth_token = $session->getSession("auth_token_twitter");

        $oauth_token_secret = $session->getSession("auth_secret_twitter");

        $url = $_SERVER['REQUEST_URI'];

        $str = explode("/", $url);

        $cur_url = $str[count($str) - 1] . '?';

        $pcur_url = $str[count($str) - 1];

    }

    $user_det = Signup::profileView($data, $entityManager);



    if (count($user_det) != 0) {

        $current_city = Signup::getTown($user_det[0]['current_city'], $entityManager);

        $home_city = Signup::getTown($user_det[0]['home_city'], $entityManager);

        // ($user_det[0]['cover_photo'] != '') {

          //$cover_photo = $user_det[0]['cover_photo'];

       // else {

            $cover_photo = 'images/banner.jpg';

       //

    } else {

        header('HTTP/1.0 404 Not Found');

        include 'html/error.php';

    }

    // $_GET['profileid'] = isset($_GET['profileid'] ) ? base64_decode($_GET['profileid']) : '';

    //if($_SESSION['userid'] == $_GET['profileid'] || $_GET['profileid'] == ''){

    //}

    if(isset($_GET['profileid']) != '' && (base64_decode($_GET['profileid']) != $session->getSession("userid"))) {

        $mine = false;

    }else{

        $mine = true;

    }

    include 'html/profile.php';

} else {

    header("Location:index.php");

}

?>