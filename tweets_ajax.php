<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
error_reporting(0);
require_once "bootstrap.php";
require_once 'model/Signup.php';
require_once 'model/Twitter.php';
require_once 'model/Wall.php';
require_once 'model/PhotosModel.php';
require_once 'model/VideosModel.php';
require_once 'model/Audio.php';
require_once 'model/Comments.php';
include 'html/wall/functions.php';
include_once("config.php");
include_once("twitteroauth/twitteroauth.php");
require_once 'classes/Session.class.php';


$session = new Session();

$AudioModel = new AudioModel();

//$dateTime = '2016-03-03 14:28:31';
$dateTime      = $_REQUEST['curTime'];
$differSecTime = $_REQUEST['differSec'];

$dateinsec = strtotime($dateTime);
$newdate   = $dateinsec + $differSecTime;
$finalTime = date('Y-m-d H:i:s', $newdate);


$entries = WallModel::getTweetsEntries($entityManager, $session->getSession("userid"), $finalTime);

//echo '<pre>';print_r($entries);die;

foreach ($entries as $key => $value) {
    $comments = CommentsModel::getComments($entries[$key]['id'], $entityManager);
    $photos   = PhotosModel::getPhotos($entries[$key]['id'], $entityManager);
    if ($photos == '') {
    } else {
        $iscover = PhotosModel::getIsCover($photos[0]['wall_id'], $entityManager);
    }
    $entries[$key]['comments'] = $comments;
    $entries[$key]['photos']   = $photos;
    $entries[$key]['iscover']  = $iscover;
    $entries[$key]['videos']   = VideosModel::getVideos($entries[$key]['id'], $entityManager);
    $entries[$key]['audios']   = $AudioModel->getAudios($entries[$key]['id'], $entityManager);
}
$dataMain = array(
    'total' => 4
);
//echo json_encode($dataMain);

$postids = 0;

foreach ($entries as $entry) {
    
	if ($entry['type'] == 1) {
        $userdetails = WallModel::getUserDetails($entityManager, $entry['author_id']);
        $infodetails = WallModel::getUserDetails($entityManager, $entry['owner_id']);
    } else {
        $userdetails = WallModel::getUserDetails($entityManager, $entry['user_id']);
    }
    // $userdetails = WallModel::getUserDetails($entityManager, $entry['user_id']);
    //echo'<pre>';print_r($userdetails);die;
?>
     <?php
			if ($userdetails[0]['gender'] == "Male") {
				$msg = " updated his billboard photo";
			} else {
				$msg = " updated her billboard photo";
			}
     ?>

    <div class="middle_area_four crispbx  notshow crispbxmain" style="margin-bottom:5px;"
         id="wall<?php
    echo $entry['id'];
?>" data="<?php
    echo $entry['id'];
?>" data-count="<?php
    echo $postids;
?>">
        <div class="slider_two_title">

            <img class="image_border_style" src="uploads/<?php
    echo $userdetails[0]['profile_pic'];
?>" alt=""
                 style="width:49px;height:49px;">


            <div class="slider_title_style2">
                <span class="author_slide_top author_upload_name Font18">
                <a style="font-size:16px;" href="./profile.php?profileid=<?php
    echo base64_encode($userdetails[0]['user_id']);
?> ">
                <?php
    if ($entry['type'] == 1) {
?>
                   
    <?php
        echo $userdetails[0]['firstname'] . '  ' . $userdetails[0]['lastname'];
?> ->
                        <?php
        echo $infodetails[0]['firstname'] . '  ' . $infodetails[0]['lastname'];
?>

                <?php
    } else if ($entry['iscover'][0]['is_cover'] == 1) {
?>
                     <?php
        echo $userdetails[0]['firstname'];
?> <?php
        echo $userdetails[0]['lastname'];
?></a><span style="font-size:14px;"><?php
        if ($userdetails[0]['gender'] == "Male")
            echo $msg;
        else
            echo $msg;
?></span>
                <?php
    } else {
?>
                   <?php
        echo $userdetails[0]['firstname'];
?> <?php
        echo $userdetails[0]['lastname'];
?> 
                    </a>
                <?php
    }
    echo '<br><span style="font-size:12px;font-weight:normal;">' . WallModel::time_elapsed_string($entry['date']) . '</span>';
?>
                   
                </span>
                <!-- <span></span> -->
                <p class="update_profile_date">

                </p></div>
        </div>
        <div style="padding:10px;width:100%;">
            <p><?php
    echo Functions::addLink($entry['text']);
?></p></div>

        <?php
    if (strlen($entry['link']) > 0) {
?>
           <div class="display_profile_pic">
                <?php
        if (strlen($entry['link_photo']) > 0):
?>
                   <img src="<?php
            echo $entry['link_photo'];
?>" alt=""/>
                <?php
        endif;
?>
               <div style="padding:10px;">
                    <p><a target="_blank" href="<?php
        echo $entry['link'];
?>"
                          style="color: #000;font-size: 18pt;line-height: 1.2em;"><?php
        echo $entry['link_title'];
?></a>
                    </p>
                    <p style="font-size:12pt;"><?php
        echo $entry['link_description'];
?></p></div>


            </div>
        <?php
    }
?>


        <div style="padding:10px;">
        
            <?php
    if (!empty($entry['photos'])) {
        //echo'<pre>';print_r(count($entry['photos']));
?>
               <div class="crispbx">
                    <div class="crispcont1">
                         <?php
        $i = 0;
        if (count($entry['photos']) >= 2) {
            //echo'<pre>';print_r(count($entry['photos']));die;
            $linhas = floor(count($entry['photos']) / 3);
            $resto  = count($entry['photos']) % 3;
            
            $tam1h = '143px';
            $tam2h = '216.5px';
            $tam3h = '436px';
            
            $tam1w = '32.8%';
            $tam2w = '49.2%';
            $tam3w = '99.1%';
            
        }
        $photoDiv = '<div class="wall_photoboard">';
        $i        = 1;
        foreach ($entry['photos'] as $path) {
            
            if ($i <= ($linhas * 3)) {
                
                // $tamAtual = $tam1;
                
                $tamAtual  = $tam1w;
                $tamAtualh = $tam1h;
                
            } else {
                if ($resto == 2) {
                    
                    // $tamAtual = $tam2;
                    
                    $tamAtual  = $tam2w;
                    $tamAtualh = $tam2h;
                    
                } else
                // $tamAtual = $tam3;
                    $tamAtual = $tam3w;
                $tamAtualh = $tam3h;
                
            }
            
            $margin = 'margin: 2px 0 0 2px';
            if (count($entry['photos']) == 1) {
                $margin    = 'margin: 0';
                $tamAtualh = '100%';
                $tamAtual  = '100%';
            }
            if ($entry['iscover'][0]['is_cover'] == 1) {
                $photoDiv .= '<a class="mid2wall" href="javascript:void(0)" ><img  style="width: ' . $tamAtual . '; height: ' . $tamAtualh . '; border-radius:px;' . $margin . ';border:1px solid #ccc !important;"  src="coverpic/images/' . $path['file'] . '"/></a>';
            } else {
                
                $photoDiv .= '<a class="mid2wall" href="javascript:void(0)" ><img  style="width: ' . $tamAtual . '; height: ' . $tamAtualh . '; border-radius:px;' . $margin . ';border:1px solid #ccc !important;"  src="uploads/' . $path['file'] . '"/></a>';
            }
            $i++;
            if ($i >= 9 && count($entry['photos']) > 9) {
                $photoDiv .= ' and ' . (count($entry['photos']) - 9) . ' more photos';
                break;
            }
            
        }
        $photoDiv .= '</div>';
        
        echo $photoDiv;
        
        $photoDiv = '';
?>
                   </div>
                </div>
            <?php
    }
?>

            <?php
    if (!empty($entry['videos'])) {
        echo $entry['videos'][0]['description'];
        $ext = pathinfo($entry['videos'][0]['file'], PATHINFO_EXTENSION);
?>

                <video  style="width:100%;height:171px;"id="my-video<?php
        echo $entry['videos'][0]['id'];
?>" class="video-js" controls preload="auto"
                      
                       poster="uploads/videos/thumbnails/<?php
        echo $entry['videos'][0]['id'];
?>/thumb_1.jpg"
                       data-setup="{}" onmouseover="stopvideo(<?php
        echo $entry['videos'][0]['id'];
?>)">
                    <source src="<?php
        echo $entry['videos'][0]['file'];
?>" type='video/<?php
        echo $ext;
?>' >
                    <p class="vjs-no-js">
                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                        <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                    </p>
                </video>

            <?php
    }
?>
       </div>
        
            <?php
    if (!empty($entry['audios'])) {
?>

    

            <div class="player_body"  id="player_body" style='background-image: url("<?php
        echo $entry['audios']['thumb'];
?>");opacity:1;'><!-- Start Player Body -->

                

                <div class="player_content">

                    <div class="column add-bottom">

                        <div id="mainwrap">

                            <div id="nowPlay">

                                <h3 class="left" id="songTitle_<?php
        echo $entry['audios']['id'];
?>"><?php
        echo $entry['audios']['title'];
?></h3>

                                <p  class="right" id="songAuther_<?php
        echo $entry['audios']['id'];
?>"><?php
        echo $entry['audios']['playlist_desc'];
?></p>

                            </div>

                            <div id="audiowrap">

                                <div id="waveform_<?php
        echo $entry['audios']['id'];
?>" data='<?php
        echo $entry['audios']['file'];
?>' class='waveform' ></div>


                               <!-- <div id="time-total"></div> -->

                                <span class="waveform__counter" id='counter_<?php
        echo $entry['audios']['id'];
?>'></span>

                                <span class="waveform__duration" id='duration_<?php
        echo $entry['audios']['id'];
?>'></span>
                                <script>
                                                                    
                                        var wavesurfer_<?php
        echo $entry['audios']['id'];
?> = WaveSurfer.create({
                                                    container        :     '#waveform_<?php
        echo $entry['audios']['id'];
?>',
                                                    waveColor        :     '#B2B2B2',
                                                    cursorColor        :     'transparent',
                                                    progressColor    :     '#fd8331',
                                                    barWidth        :    '3',
                                                    height            :    '180',
                                                    renderer        :     'MultiCanvas',
                                                    pixelRatio        :       5,
                                                    timeInterval    :      100,
                                                    backend            :     'MediaElement',
                                                });
                                                
                                            //wavesurfer_<?php
        echo $entry['audios']['id'];
?>.load("<?php
        echo $entry['audios']['file'];
?>");
                                            
                                            
                                            
                                            // Change Volume  
                                            wavesurfer_<?php
        echo $entry['audios']['id'];
?>.setVolume(0.4);
                                            
                                            document.querySelector('#volume_<?php
        echo $entry['audios']['id'];
?>').value = wavesurfer_<?php
        echo $entry['audios']['id'];
?>.backend.getVolume();
                                    
                                </script>    
                                <div class="custom_controls"> 

                                    <div class="volbox">

                                        <i id="toggleMuteBtn" class="fas fa-volume-up"></i>

                                        <input class="volume" id="volume_<?php
        echo $entry['audios']['id'];
?>" type="range" min="0" max="1" value="0.1" step="0.1">

                                    </div>

                                   <span class='mp3_menu_right'>

                                    <a class="backward" onclick="backward(wavesurfer_<?php
        echo $entry['audios']['id'];
?>,<?php
        echo $entry['audios']['id'];
?>)"><i class="fa fa-backward"></i></a>
                    
                                    <span id="playPause_<?php
        echo $entry['audios']['id'];
?>">

                                        <button id='ply_<?php
        echo $entry['audios']['id'];
?>' onclick="play_pause(wavesurfer_<?php
        echo $entry['audios']['id'];
?>,<?php
        echo $entry['audios']['id'];
?>,'<?php
        echo $entry['audios']['file'];
?>')">

                                            <i class="fas fa-play"></i>

                                        </button>
                                        
                                        <button style='display:none;' id='pause_<?php
        echo $entry['audios']['id'];
?>' onclick="pause(wavesurfer_<?php
        echo $entry['audios']['id'];
?>,<?php
        echo $entry['audios']['id'];
?>)">

                                            <i class="fas fa-pause"></i>

                                        </button>
                                        
                                        <button style='display:none;' id='plya_<?php
        echo $entry['audios']['id'];
?>' onclick="playAgain(wavesurfer_<?php
        echo $entry['audios']['id'];
?>,<?php
        echo $entry['audios']['id'];
?>)">

                                            <i class="fas fa-play"></i>

                                        </button>

                                    </span>
                    
                                    <a class="forward" onclick="forward(wavesurfer_<?php
        echo $entry['audios']['id'];
?>,<?php
        echo $entry['audios']['id'];
?>)" ><i class="fa fa-forward"></i></a>

                                    <a onClick="play_pause(wavesurfer_<?php
        echo $entry['audios']['id'];
?>,<?php
        echo $entry['audios']['id'];
?>,'<?php
        echo $entry['audios']['file'];
?>')" id="repeateFun"><i class="fas fa-redo-alt"></i></a>

                                    <!--<a onClick="shuffleOnOff()" id="shuffle"><i class="fa fa-random"></i></a>-->
                                    
                                    </span>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div><!-- End Player Body -->
            <br>

            <br>

    <?php
    }
?>


        <div class="like_comment_share">
            <a href="#"><span><i class="fa fa-thumbs-up"></i>Like</span></a>

            <a href="#"><span><i class="fa fa-heart"></i>Love it</span></a>
            <a href="#" class="add-comment-link" rel="<?php
    echo $entry['id'];
?>"><span><i class="fa fa-commenting"></i>Comment</span></a>
            <a href="#"><span><i class="fa fa-share-square-o"></i>Share</span></a>
        </div>


        <div class="comment_box" rel="<?php
    echo $entry['id'];
?>">

            <form rel="<?php
    echo $entry['id'];
?>" id="commform_<?php
    echo $entry['id'];
?>" action="" method="post"
                  enctype="multipart/form-data">


                <img src="uploads/<?php
    echo $_SESSION['profile_pic'];
?>" alt="" style="width:37px;height:37px;">
                <span><input class="comment_box_inline" placeholder="Write your comment here" type="text"></span>
                <div class="comment_box_icon comment_box_inline"><a href="#"><span><i
                                class="comment_box_left fa fa-camera"></i></span></a>
                    <a href="#"><span><i class="comment_box_left2 fa fa-smile-o"></i></span></a></div>
                <h5 class="press_enter_post">Press enter to post</h5>
        </div>


        <div data="<?php
    echo $entry['id'];
?>" id="comment_<?php
    echo $entry['id'];
?>">
            <?php //echo '<pre>'; print_r($entry['comments']); 
?>
           <?php
    foreach ($entry['comments'] as $comment):
        $cuserdetails = WallModel::getUserDetails($entityManager, $comment['author_id']);
    //echo '<pre>'; print_r($cuserdetails);
?>

                <div class="slider_two_title">
                    <img src="uploads/<?php
        echo $cuserdetails[0]['profile_pic'];
?>"
                         alt="<?php
        echo $comment['1firstname'];
?> <?php
        echo $comment['1lastname'];
?>"
                         style="width:37px;height:37px;">
                    <div class="slider_title_style 1single-comment">
                        <a href="#"><span
                                class="author_slide_top"><?php
        echo $comment['firstname'];
?><?php
        echo $comment['lastname'];
?></span></a>
                        <span class="comment_box_bottom_text"><?php
        echo Functions::addLink($comment['text']);
?></span>
                        <a href="#"><p class="like_and_reply"><span>Like</span><span class="comment_reply">Reply</span>
                                路 <?php
        echo date('m/d/Y H:i:s', strtotime($comment['date']));
?></p></a>
                    </div>
                </div>
                <div class="line"></div>
            <?php
    endforeach;
?>
       </div>


    </div>


    <?php
    $postids++;
}
?>