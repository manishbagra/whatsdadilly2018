 <?php
ini_set('error_reporting', E_ALL);

error_reporting(0);

require_once "bootstrap.php";
require_once 'classes/Session.class.php';
require_once 'model/Notification.php';
require_once 'model/Instagram.php';
require_once 'model/Wall.php';
require_once 'model/Comments.php';
$session = new Session();

$insta = new Insta();

$wall       = new WallModel();
$commentobj = new CommentsModel();

$notifications      = new NotificationModel();
$getnotification_id = array();
if ($session->getSession("userid") != "" || $session->getSession("userid") != null) {
    
    $results = $notifications->countNotificationByUser($entityManager, $session->getSession('userid'));
    
    $countNotificatio = count($results);
    
    
    if ($countNotificatio > 0) {
        
        
        $i = 0;
        
        foreach ($results as $key => $value) {
            
            $msg[$key] = $value['message'];
            
            $friendProfile = $notifications->getFriendProfile($entityManager, $value['id_friend']);
            
            $image[$key] = "uploads/" . $friendProfile['profile_pic'];
            
            $idArray[$key] = $value['id_notifications'];
            
            $post_id[$key] = $value['wall_id'];
            
            $getSingleWall[$key] = base64_encode($wall->getAutherId($entityManager, $post_id[$key]));
            
            $comment_id[$key] = $value['comment_id'];
            
            $time[$key] = $insta->time_elapsed_string($value['date']);
            
            
            $file[$key] = "uploads/" . $value['id_friend'] . "/posted/thumb/" . $value['thumb'];
            
            $thumb[$key] = $value['thumb'];
            
            $type[$key] = $value['type'];
            
            $getposttype[$key] = $commentobj->getPostType($entityManager, $post_id[$key]);
            
            
            $i++;
        }
        
        
        $getnotification_id = $notifications->getNotificationClick($entityManager);
        
        $countNotificatio_id = count($getnotification_id);
        
        foreach ($getnotification_id as $key => $value) {
            
            $notification_id[$key] = $value['id_notifications'];
            
        }
        
        
        $jsonarray = array(
            
            'count' => $countNotificatio,
            'id' => $results[0]['id_notifications'],
            'msg' => $msg,
            'id_array' => $idArray,
            'getnotificationclickid' => $notification_id,
            'user_id' => $getSingleWall,
            'image' => $image,
            'file' => $file,
            'type' => $type,
            'thumb' => $thumb,
            'postid' => $post_id,
            'commentid' => $comment_id,
            'getposttype' => $getposttype,
            'countNotificatio_id' => $countNotificatio_id,
            'time' => $time
            
        );
        
        //echo'<pre>';print_r($jsonarray);die;
        echo json_encode($jsonarray);
        exit();
        
    } else {
        
        $jsonarray = array(
            'count' => 0
        );
        
        echo json_encode($jsonarray);
        exit();
        
    }
    
} else {
    
    header("Location:index.php");
    
} 