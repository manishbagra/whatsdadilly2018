<div class="tab-1" style="display:none;" id="twitter-tab">

    <input type="hidden" name="current_url" id="current_url" value="<?php echo $cur_url; ?>"/>
    <input type="hidden" name="current_tscreen" id="current_tscreen" value="<?php echo $screenname; ?>"/>
    <input type="hidden" name="current_tid" id="current_tid" value="<?php echo $twitterid; ?>"/>
    <input type="hidden" name="pcurrent_url" id="pcurrent_url" value="<?php echo $pcur_url; ?>"/>

    <?php
    $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $oauth_token, $oauth_token_secret);

    $user_info = $connection->get('https://api.twitter.com/1.1/account/verify_credentials.json', array("include_entities" => true, "include_rts" => true, "contributor_details" => true)
        ,'auth');
    if (isset($_GET['profileid']) && (base64_decode($_GET['profileid']) != $session->getSession("userid"))) {
        $live_timeline = $connection->get('statuses/user_timeline', array('screen_name' => $screenname, "count" => 50, "contributor_details" => true, "include_entities" => true, "include_my_retweet" => true),'usertimeline');
    } else {
        $live_timeline = $connection->get('https://api.twitter.com/1.1/statuses/home_timeline.json', array('screen_name' => $screenname, 'count' => 50, "contributor_details" => true, "include_entities" => true, "include_my_retweet" => true),'hometimeline');
    }
    // echo '<pre>';
    // print_r($user_info);
    // echo "</pre>";
    ?><?php if ($screen_length != 0) { ?>
        <div class="twt-wall-tw-info-title">
            <div class="TwtLogo">
                <a href="#"><img src="images/twitter-logo.png"></a>
                <!-- <span>@<?php //echo $user_info->screen_name;                      ?><!--</span>-->
            </div>
            <?php if (isset($_GET['profileid']) && (base64_decode($_GET['profileid']) != $session->getSession("userid"))) {
                ?>

            <?php } else {
                ?>
                <div class="HomeTwBtn"><a href='javascript:void(0)' onclick='load_index();'>
                        <i class="fa fa-home"></i>
                        Home</a></div>
            <?php } ?>
            <div class="TweetsQntty TweetsQntty1 ">
                <a href='javascript:void(0)' onclick='load_connect();'>
                    <span class="twCount"> <?php echo $user_info->statuses_count; ?> </span> <br/>
                    Tweets</a>
            </div>
            <div class="FollowingQntty TweetsQntty">
                <a href='javascript:void(0)' onclick='<?php echo $function_following; ?>'>
                                                <span class="twCount">
                                                <?php echo $user_info->friends_count; ?></span> <br/>
                    Following</a></div>
            <div class="FollowerQntty TweetsQntty">
                <a href='javascript:void(0)' onclick='<?php echo $function_followers; ?>'><span
                        class="twCount"><?php echo $user_info->followers_count; ?> </span> <br/>
                    Followers </a></div>
            <div class="Favorites TweetsQntty">
                <a href='javascript:void(0)' onclick="<?php echo $function_fav; ?>"><span
                        class="twCount"><?php echo $user_info->favourites_count; ?></span> <br/>
                    Favorites</a></div>
            <div class="Mentions TweetsQntty">&nbsp; <br/><a href='javascript:void(0)'
                                                             onclick='<?php echo $function_mention; ?>'>Mentions</a>
            </div>
            <div class="msgBtn">
                                            <span class="envelope"> <i class="fa fa-envelope" aria-hidden="true"></i>
                                            </span><br>
                <span class="flw">
                                                <button style="display:none;" type="button" class="btn btn-primary">Followings</button>
                                                Direct Mail
                                            </span>
            </div>
            <!--<div class="UserTimeline TweetsQntty">&nbsp; <br/><a href='javascript:void(0)' onclick='load_connect();'>UserTimeline</a></div>-->
        </div>
    <?php } ?>
    <div class="wrapper" id="postedComments" style="display:block;">
        <div style="positive: relative; margin: 0px auto;width: 100px; height: 2px;">
            <div class="demo-cb-tweets" style="text-align:center;position:fixed;"></div>
        </div>
        <?php
        //echo '<div class="welcome_txt">Welcome <strong>' . $screenname . '</strong> (Twitter ID : ' . $twitterid . '). <a href="index.php?reset=1">Logout</a>!</div>';
        //echo '<pre>';
        //print_r($live_timeline);
        // echo '<div class="tweet_box">';
        // echo '<form id="imageform" method="post" enctype="multipart/form-data" action="tweet_image.php">';
        // echo '<table><tr>';
        // echo '<td colspan="2"><textarea name="updateme" id="updateme" cols="60" rows="4"></textarea></td>';
        // echo '</tr>';
        // echo '<tr><td>';
        // echo '<input type="file" name="photoimg" id="photoimg" value="" />';
        //  echo '</td>';
        //  echo '<td><input type="button" value="Tweet" onclick="posttweet()"/></td>';
        //  echo '</tr></table>';
        //  echo '</form>';
        //  echo "<div id='preview'></div>";
        // echo '</div>';
        if ($screen_length != 0) {
            if ($live_timeline->errors[0]->message == '') {
                echo '<input type="hidden" name="twitter_condition" id="twitter_condition" value="' . $twitt_val . '" />';
            } else {
                echo '<center><h3><b>Time limit exceed please try after some times.</b></h3></center>';
                echo $live_timeline->errors[0]->message;
                die();
            }
        }
        //echo "<div id='menu' style='padding: 15px 7px 15px 22px;'>";
        //echo "<ul>";
        //echo "<li><a href='javascript:void(0)' onclick='load_index();'>HOME</a></li>";
        //echo "<li><a href='javascript:void(0)' onclick='load_connect();'>USER TIMELINE</a></li>";
        //echo "<li><a href='javascript:void(0)' onclick='load_mentions();'>MENTIONS</a></li>";
        //  echo "<li><a href='javascript:void(0)' onclick='load_followers();'>FOLLOWERS</a></li>";
        // echo "<li><a href='javascript:void(0)' onclick='load_following();'>FOLLOWING</a></li>";
        // echo '<li><button title="Direct messages" type="button" class="btn dm-button" style="padding: 1px 28px; !important;width:25px;" onclick="load_dm();">  <i class="dm-envelope"><span class="dm-new"></span></i></button></li>';
        //  echo "</ul>";
        // echo "</div>";
        // $live_timeline = $connection->get('statuses/home_timeline', array('screen_name' => $screen_name, 'count' => 50, "contributor_details" => true, "include_entities" => true, "include_my_retweet" => true,"since_id" => "309224665145028608"));
        echo '<div class="tweet_count_dis"></div>';

        $user_info = $connection->get('account/verify_credentials', array("include_entities" => true, "include_rts" => true, "contributor_details" => true));
        ?>
        <script>
            document.body.style.background = "#f3f3f3 url(<?php echo $user_info->profile_background_image_url; ?>) fixed";
            document.body.style.backgroundRepeat = "repeat";
            document.body.style.height = "100%";
        </script>
        <?php
        echo '<div class="tweet_list" id="tweet_list">';
        echo '<div class="home_timeline">';
        $tweet_count = count($live_timeline) - 1;
        echo '<input type="hidden" name="tcount" id="tcount" value="' . $tweet_count . '" />';
        echo '<input type="hidden" name="oauthscreen_name" id="oauthscreen_name" value="' . $screenname . '" />';
        if ($screen_length != 0) {
            foreach ($live_timeline as $k => $my_tweet) {

                $media_flag = '';
                $image_are = '';
                $conversation = '';
                $RT_link = '';
                $Delete_link = '';
                $fav = '';
                $RT = '';
                $disp_url = '';
                echo "<input type='hidden' name='tweet_id$k' id='tweet_id$k' value='$my_tweet->id_str'>";
                echo "<input type='hidden' name='screen_name$k' id='screen_name$k' value='" . $my_tweet->user->screen_name . "'>";
                echo "<input type='hidden' name='uretweet_id$k' id='uretweet_id$k' value='" . $my_tweet->current_user_retweet->id_str . "'>";

                if ($my_tweet->retweeted_status->id == '') {

                    echo "<input type='hidden' name='rtweet_id$k' id='rtweet_id$k' value='$my_tweet->id_str'>";

                    if ($twitterid == $my_tweet->user->id_str && $_GET['profileid'] == '') {
                        $Delete_link = '<a href="javascript:void(0);" onclick="delete_tweet(' . $k . ')"><i class="sm-trash"></i><b>Delete</b></a>';
                    }

                    if ($my_tweet->current_user_retweet->id_str != '') {
                        $RT = 'retweeted';
                        $RT_link = '<a href="javascript:void(0);" onclick="destory_tweet(' . $k . ')"><i class="sm-rt"></i><b>Retweeted</b></a>';
                    } else {
                        $RT_link = '<a href="#" class="big-link" data-reveal-id="myModals' . $k . '" data-animation="none"><i class="sm-rt"></i><b>Retweet</b></a>';
                    }
                    if ($my_tweet->favorited != '') {
                        $fav = 'favorited';
                        $Fav_link = '<a href="javascript:void(0);" onclick="undofavorite_tweet(' . $k . ')"><i class="sm-fav"></i><b>Favorited</b></a>';
                    } else {
                        $Fav_link = '<a href="javascript:void(0)" onclick="favorite(' . $k . ')"><i class="sm-fav"></i><b>Favorite</b></a>';
                    }
                    if ($my_tweet->entities->urls[0]->display_url != '') {
                        $disp_url = $my_tweet->entities->urls[0]->display_url;
                    } else if ($my_tweet->entities->media[0]->display_url != '') {
                        $disp_url = $my_tweet->entities->media[0]->display_url;
                    }
                    echo "<input type='hidden' name='reply_to_status_id$k' id='reply_to_status_id$k' value='" . $my_tweet->in_reply_to_status_id_str . "'>";
                    $text = htmlentities($my_tweet->text, ENT_QUOTES, 'utf-8');
                    $text = preg_replace('@(https?://([-\w\.]+)+(/([\w/_\.]*(\?\S+)?(#\S+)?)?)?)@', '<a href="$1" target="_blank">' . $disp_url . '</a>', $text);
                    $text = preg_replace('/@(\w+)/', '<a href="twitter_ajax/twitpic.php?screenname=$1" data-toggle="modal" data-target="#myModal" class="fancybox fancybox.ajax">@$1</a>', $text);

                    $text = preg_replace('/\s#(\w+)/', ' <a href="twitter_ajax/twitter_search.php?q=%23$1" class="fancybox fancybox.ajax">#$1</a>', $text);
                    echo '<div class="tweet-outer center_area" id="tweet-outer' . $k . '" data="' . $my_tweet->id_str . '" data-count="' . $k . '">';

                    echo '<div class="tweet-txt  ' . $fav . ' ' . $RT . '" id="tweet-txt' . $k . '">';
                    echo '<i class="dogear"></i>';
                    echo "<div class='tweet-pic'><a href='twitter_ajax/twitpic.php?screenname=" . $my_tweet->user->screen_name . "' class='fancybox fancybox.ajax'><img src='" . $my_tweet->user->profile_image_url . "' title='" . $my_tweet->user->name . "' class='profile_pic'></a></div>";
                    echo '<div class="tweet-content">';
                    echo '<div class="stream-item-header">';
                    echo '<b>' . $my_tweet->user->name . '</b>&nbsp;<span class="username js-action-profile-name"><a href="twitter_ajax/twitpic.php?screenname=' . $my_tweet->user->screen_name . '" data-toggle="modal" data-target="#myModal" class="fancybox fancybox.ajax"><span>@' . $my_tweet->user->screen_name . '</a></span></span>';
                    echo '</div>';
                    echo $text . ' <br>-<span>' . $my_tweet->created_at . '</span></div>';
                    echo '<div class="tweet-counts"></div>';
                    echo ' </div>';


                    //Popup
                    echo '<div id="myModals' . $k . '" class=" reveal-modal">';
                    echo "<div class='tweet-pic'><img src='" . $my_tweet->user->profile_image_url . "' title='" . $my_tweet->user->name . "' class='profile_pic'></div>";
                    echo '<div class="tweet-content">' . $text . ' <br>-<span>' . $my_tweet->created_at . '</span></div>';
                    echo '<input type="button" name="retweet" value="Retweet" onclick="retweet(' . $k . ')" class="tweet-rt">';
                    echo '<a class="close-reveal-modal">&#215;</a></div>';


                    if ($my_tweet->in_reply_to_status_id_str != '') {

                        $conversation = '<span  id="replied' . $k . '"><a href="javascript:void(0)" onclick="getReplies(' . $k . ')">  <span class="details-icon js-icon-container">
                                             <i class="sm-chat"></i>
                                                </span><b>
                                                  <span class="expand-stream-item js-view-details">
                                                    View conversation
                                                  </span>
                                                </b></a></span>';
                    }
                    $imge_id = explode('/', $my_tweet->entities->urls[0]->display_url);
                    $vid = explode('/', $my_tweet->entities->urls[0]->expanded_url);
                    $ct = count($vid) - 1;
                    $video_id = explode('=', $vid[$ct]);
                    if ($my_tweet->entities->media[0]->id_str != '') {
                        $w = $my_tweet->entities->media[0]->media_url->sizes->large->w;
                        $h = $my_tweet->entities->media[0]->media_url->sizes->large->h;
                        $media_flag = '<a href="javascript:void(0)" onclick="displaymedia(' . $k . ')"><i class="sm-image"></i> View Media</a>';
                        $image_are = "<div class='tweet_outer tweet-medias' id='yfrog$k' style='display:;'><a href='" . $my_tweet->entities->media[0]->media_url . "' class='fancybox-effects-a' title = 'Photo'><img src='" . $my_tweet->entities->media[0]->media_url . "' ></a></div>";
                    } else if ($imge_id[0] == 'twitpic.com') {
                        $w = $my_tweet->entities->media[0]->media_url->sizes->large->w;
                        $h = $my_tweet->entities->media[0]->media_url->sizes->large->h;
                        $media_flag = '<a href="javascript:void(0)" onclick="displaymedia(' . $k . ')"><i class="sm-image"></i> View Media</a>';
                        $image_are = "<div class='tweet_outer tweet-medias' id='yfrog$k' style='display:;'><a href='http://twitpic.com/show/full/" . $imge_id[1] . ".jpg' class='fancybox-effects-a' title = 'Photo'><img src='http://twitpic.com/show/full/" . $imge_id[1] . ".jpg'></a></div>";
                    } else if ($imge_id[0] == 'yfrog.com') {
                        $w = $my_tweet->entities->media[0]->media_url->sizes->large->w;
                        $h = $my_tweet->entities->media[0]->media_url->sizes->large->h;
                        $media_flag = '<a href="javascript:void(0)" onclick="displaymedia(' . $k . ')"><i class="sm-image"></i> View Media</a>';
                        $image_are = "<div class='tweet_outer tweet-medias' id='yfrog$k' style='display:;'><a href='http://yfrog.com/" . $imge_id[1] . ":medium' class='fancybox-effects-a' title = 'Photo'><img src='http://yfrog.com/" . $imge_id[1] . ":medium'></a></div>";
                    } else if ($imge_id[0] == 'youtube.com') {
                        $len = count($video_id) - 1;
                        $video_ids = $video_id[$len];
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, "http://gdata.youtube.com/feeds/api/videos?q=" . $video_id[$len]);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        $feed = curl_exec($ch);
                        curl_close($ch);
                        $xml = simplexml_load_string($feed);
                        $entry = $xml->entry[0];
                        $media = $entry->children('media', true);
                        $group = $media[0];
                        $title = $group->title;
                        $desc = $group->description;

                        $media_flag = '<a href="javascript:void(0)" onclick="displaymedia(' . $k . ')"><i class="sm-image"></i> View Media</a>';
                        $image_are = "<div class='tweet_outer tweet-medias' id='yfrog$k' style='display:none;'><object width='425' height='349' type='application/x-shockwave-flash' data='http://www.youtube.com/v/" . $video_id[$len] . "'><param name='movie' value='http://www.youtube.com/v/" . $video_id[$len] . "' /></object>" .
                            "<br><b>" . $title . "</b><br>" .
                            "<b>" . $desc . "</b><br>"
                            . "</div>";
                    } else if ($imge_id[0] == 'vine.co') {
                        $media_flag = '<a href="javascript:void(0)" onclick="displaymedia(' . $k . ')"><i class="sm-image"></i> View Media</a>';
                        $image_are = "<div class='tweet_outer tweet-medias' id='yfrog$k' style='display:none;'><iframe src='https://vine.co/v/" . $imge_id[2] . "/card?mute=1' width='300px' height='300px' frameborder='0'></iframe>" .
                            "<br><b>" . $title . "</b><br>" .
                            "<b>" . $desc . "</b><br>"
                            . "</div>";
                    }
                    echo '<div class="tweet-options"><a href="javascript:void(0);" onclick="displayRetweeters(' . $k . ')" id="retweet_img' . $k . '">Expand</a><a href="javascript:void(0)" onclick="hideRetweeters(' . $k . ')"  id="hretweet_img' . $k . '" style="display:none;">Collapse</a>&nbsp;&nbsp;<a href="javascript:void(0)" onclick="displayConversation(' . $k . ')"  id="hreplied' . $k . '" style="display:none;">Hide Conversation</a>' . $conversation . '&nbsp;&nbsp;&nbsp;' . $Delete_link . '&nbsp;&nbsp;&nbsp;' . $RT_link . '&nbsp;&nbsp;&nbsp;' . $Fav_link . '&nbsp;&nbsp;&nbsp;' . $media_flag . '&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="displayreply(' . $k . ')"><i class="sm-reply"></i>
                                 <b>Reply</b></a>';
                    echo '</div>';
                    echo "<div class='tweet-retweeters' id='tweet-retweeters$k' style='display:none;'></div>";
                    echo $image_are;
                    echo "<div class='tweet-reply' id='tweet-reply$k' style='display:none;'>";
                    echo '<table><tr>';
                    $mentions = '';
                    for ($jk = 0; $jk < count($my_tweet->entities->user_mentions); $jk++) {
                        $mentions .= '@' . $my_tweet->entities->user_mentions[$jk]->screen_name . ' ';
                    }
                    echo '<td><textarea class="form-control" name="reply_message' . $k . '" id="reply_message' . $k . '" cols="60" rows="4">@' . $my_tweet->user->screen_name . ' ' . $mentions . '</textarea></td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td align="right"><input type="button"  class="btn btn-default" value="Tweet" onclick="reply(' . $k . ')"/></td>';
                    echo '</tr></table>';
                    echo '</div>';
                    echo "<div class='rtweet-replies' id='rtweet-replies$k' style='display:none;'></div>";
                    echo "<div class='tweet-replied' id='tweet-replied$k' style='display:none;'></div>";


                    echo '</div>';
                } else {
                    echo "<input type='hidden' name='rtweet_id$k' id='rtweet_id$k' value='" . $my_tweet->retweeted_status->id_str . "'>";
                    //  echo $my_tweet->retweeted_status->current_user_retweet->id_str;
                    if ($my_tweet->current_user_retweet->id_str != '') {
                        $RT = 'retweeted';
                        $RT_link = '<a href="javascript:void(0);" onclick="destory_tweet(' . $k . ')"><i class="sm-rt"></i><b>Retweeted</b></a>';
                    } else {
                        $RT_link = '<a href="#" class="big-link" data-reveal-id="myModals' . $k . '" data-animation="none"><i class="sm-rt"></i><b>Retweet</b></a>';
                    }
                    if ($my_tweet->retweeted_status->entities->urls[0]->display_url != '') {
                        $disp_url = $my_tweet->retweeted_status->entities->urls[0]->display_url;
                    } else if ($my_tweet->retweeted_status->entities->media[0]->display_url != '') {
                        $disp_url = $my_tweet->retweeted_status->entities->media[0]->display_url;
                    }
                    echo "<input type='hidden' name='reply_to_status_id$k' id='reply_to_status_id$k' value='" . $my_tweet->retweeted_status->in_reply_to_status_id_str . "'>";
                    $text = htmlentities($my_tweet->retweeted_status->text, ENT_QUOTES, 'utf-8');
                    $text = preg_replace('@(https?://([-\w\.]+)+(/([\w/_\.]*(\?\S+)?(#\S+)?)?)?)@', '<a href="$1" target="_blank">' . $disp_url . '</a>', $text);
                    $text = preg_replace('/@(\w+)/', '<a href="twitter_ajax/twitpic.php?screenname=$1" class="fancybox fancybox.ajax">@$1</a>', $text);
                    $text = preg_replace('/\s#(\w+)/', ' <a href="twitter_ajax/twitter_search.php?q=%23$1" class="fancybox fancybox.ajax">#$1</a>', $text);
                    echo '<div class="tweet-outer center_area" id="tweet-outer' . $k . '" data="' . $my_tweet->id_str . '" data-count="' . $k . '">';

                    echo '<div class="tweet-txt  ' . $fav . ' ' . $RT . '" id="tweet-txt' . $k . '">';

                    echo "<div class='tweet-pic'><a href='twitter_ajax/twitpic.php?screenname=" . $my_tweet->retweeted_status->user->screen_name . "' class='fancybox fancybox.ajax'><img src='" . $my_tweet->retweeted_status->user->profile_image_url . "' title='" . $my_tweet->retweeted_status->user->name . "' class='profile_pic'></a></div>";

                    echo '<div class="tweet-content">';
                    echo '<div class="stream-item-header">';
                    echo '<b>' . $my_tweet->retweeted_status->user->name . '</b>&nbsp;<span class="username js-action-profile-name"><a href="twitter_ajax/twitpic.php?screenname=' . $my_tweet->retweeted_status->user->screen_name . '" class="fancybox fancybox.ajax"><span>@' . $my_tweet->retweeted_status->user->screen_name . '</span></a></span>';
                    echo '</div>';
                    echo $text . ' <br>-<span>' . $my_tweet->retweeted_status->created_at . '</span></div>';
                    echo '<div class="tweet-counts">Retweet By ' . $my_tweet->user->name;
                    echo '</div>';
                    echo '</div>';


                    echo '<div id="myModals' . $k . '" class=" reveal-modal">';
                    echo "<div class='tweet-pic'><img src='" . $my_tweet->retweeted_status->user->profile_image_url . "' title='" . $my_tweet->retweeted_status->user->name . "' class='profile_pic'></div>";
                    echo '<div class="tweet-content">' . $text . ' <br>-<i>' . $my_tweet->retweeted_status->created_at . '</i></div>';
                    echo '<a href="javascript:void(0)" onclick="retweet(' . $k . ')"  class="tweet-rt">Retweet</a>';
                    echo '<a class="close-reveal-modal">&#215;</a></div>';

                    if ($my_tweet->retweeted_status->in_reply_to_status_id_str != '') {

                        $conversation = '<a href="javascript:void(0)" id="replied' . $k . '" onclick="getReplies(' . $k . ')">  <span class="details-icon js-icon-container">
                                                     <i class="sm-chat"></i>
                                                         </span>
                                                             <b>
                                                                <span class="expand-stream-item js-view-details">
                                                                    View conversation
                                                                </span>

                                                             </b></a>';
                    }

                    $imge_id = explode('/', $my_tweet->retweeted_status->entities->urls[0]->display_url);
                    $vid = explode('/', $my_tweet->retweeted_status->entities->urls[0]->expanded_url);
                    $ct = count($vid) - 1;
                    $video_id = explode('=', $vid[$ct]);

                    if ($my_tweet->retweeted_status->entities->media[0]->id_str != '') {
                        $w = $my_tweet->retweeted_status->entities->media[0]->media_url->sizes->large->w;
                        $h = $my_tweet->retweeted_status->entities->media[0]->media_url->sizes->large->h;
                        $media_flag = '<a href="javascript:void(0)" onclick="displaymedia(' . $k . ')"><i class="sm-image"></i> View Media</a>';
                        $image_are = "<div class='tweet_outer tweet-medias' id='yfrog$k' style='display:;'><a href='" . $my_tweet->retweeted_status->entities->media[0]->media_url . "' class='fancybox-effects-a' title = 'Photo'><img src='" . $my_tweet->retweeted_status->entities->media[0]->media_url . "' ></a></div>";
                    } else if ($imge_id[0] == 'twitpic.com') {
                        $w = $my_tweet->retweeted_status->entities->media[0]->media_url->sizes->large->w;
                        $h = $my_tweet->retweeted_status->entities->media[0]->media_url->sizes->large->h;
                        $media_flag = '<a href="javascript:void(0)" onclick="displaymedia(' . $k . ')"><i class="sm-image"></i> View Media</a>';
                        $image_are = "<div class='tweet_outer tweet-medias' id='yfrog$k' style='display:;'><a href='http://twitpic.com/show/full/" . $imge_id[1] . ".jpg' class='fancybox-effects-a' title = 'Photo'><img src='http://twitpic.com/show/full/" . $imge_id[1] . ".jpg'></a></div>";
                    } else if ($imge_id[0] == 'yfrog.com') {
                        $w = $my_tweet->retweeted_status->entities->media[0]->media_url->sizes->large->w;
                        $h = $my_tweet->retweeted_status->entities->media[0]->media_url->sizes->large->h;
                        $media_flag = '<a href="javascript:void(0)" onclick="displaymedia(' . $k . ')"><i class="sm-image"></i> View Media</a>';
                        $image_are = "<div class='tweet_outer tweet-medias' id='yfrog$k' style='display:;'><a href='http://yfrog.com/" . $imge_id[1] . ":medium'' class='fancybox-effects-a' title = 'Photo'><img src='http://yfrog.com/" . $imge_id[1] . ":medium'></a></div>";
                    } else if ($imge_id[0] == 'youtube.com') {
                        $len = count($video_id) - 1;
                        $video_ids = $video_id[$len];
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, "http://gdata.youtube.com/feeds/api/videos?q=" . $video_id[$len]);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        $feed = curl_exec($ch);
                        curl_close($ch);
                        $xml = simplexml_load_string($feed);
                        $entry = $xml->entry[0];
                        $media = $entry->children('media', true);
                        $group = $media[0];
                        $title = $group->title;
                        $desc = $group->description;

                        $media_flag = '<a href="javascript:void(0)" onclick="displaymedia(' . $k . ')"><i class="sm-image"></i> View Media</a>';
                        $image_are = "<div class='tweet_outer tweet-medias' id='yfrog$k' style='display:none;'><object width='425' height='349' type='application/x-shockwave-flash' data='http://www.youtube.com/v/" . $video_id[$len] . "'><param name='movie' value='http://www.youtube.com/v/" . $video_id[$len] . "' /></object>" .
                            "<br><b>" . $title . "</b><br>" .
                            "<b>" . $desc . "</b><br>"
                            . "</div>";
                    } else if ($imge_id[0] == 'vine.co') {
                        $media_flag = '<a href="javascript:void(0)" onclick="displaymedia(' . $k . ')"><i class="sm-image"></i> View Media</a>';
                        $image_are = "<div class='tweet_outer tweet-medias' id='yfrog$k' style='display:none;'><iframe src='https://vine.co/v/" . $imge_id[2] . "/card?mute=1' width='300px' height='300px' frameborder='0'></iframe>" .
                            "<br><b>" . $title . "</b><br>" .
                            "<b>" . $desc . "</b><br>"
                            . "</div>";
                    }
                    echo '<div class="tweet-options"><a href="javascript:void(0);" onclick = "displayRetweeters(' . $k . ')" id="retweet_img' . $k . '">Expand</a><a href="javascript:void(0)" onclick="hideRetweeters(' . $k . ')"  id="hretweet_img' . $k . '" style="display:none;">Collapse</a>&nbsp;&nbsp;<a href="javascript:void(0)" onclick="displayConversation(' . $k . ')"  id="hreplied' . $k . '" style="display:none;">Hide Conversation</a>' . $conversation . '&nbsp;&nbsp;&nbsp;' . $RT_link . '&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="favorite(' . $k . ')"><i class="sm-fav"></i><b>Favorite</b></a>&nbsp;&nbsp;&nbsp;' . $media_flag . '&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="displayreply(' . $k . ')"><i class="sm-reply"></i>
                             <b>Reply</b></a>';
                    echo '</div>';
                    echo "<div class='tweet-retweeters' id='tweet-retweeters$k' style='display:none;'></div>";
                    echo $image_are;
                    echo "<div class='tweet-reply' id='tweet-reply$k' style='display:none;'>";
                    echo '<table><tr>';
                    $mentions = '';
                    for ($jk = 0; $jk < count($my_tweet->retweeted_status->entities->user_mentions); $jk++) {
                        $mentions .= '@' . $my_tweet->retweeted_status->entities->user_mentions[$jk]->screen_name . ' ';
                    }
                    echo '<td><textarea class="form-control" name="reply_message' . $k . '" id="reply_message' . $k . '" cols="60" rows="4">@' . $my_tweet->retweeted_status->user->screen_name . ' ' . $mentions . '</textarea></td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td align="right"><input type="button" class="btn btn-default" value="Tweet" onclick="reply(' . $k . ')"/></td>';
                    echo '</tr></table>';
                    echo '</div>';
                    echo "<div class='rtweet-replies' id='rtweet-replies$k' style='display:none;'></div>";
                    echo "<div class='tweet-replied' id='tweet-replied$k' style='display:none;'></div>";

                    echo '</div>';
                }
            }
        }
        echo ' <div id="loadorders"></div>';
        echo '<div id="loadMoreComments" style="display:none;" ></div>';
        echo "<p id='last'></p>";
        echo '</div></div></div>';
        ?>

        <div id="loadmoreajaxloader" style="display:none;">
            <center><img src="images/ajax-loader.gif"/></center>
        </div>
    </div>

    <script type="text/javascript">
        load_index();
    </script>