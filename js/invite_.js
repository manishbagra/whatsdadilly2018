var clientId = '467437228-2eb0vqjkbpmbnchg6s6rnmadpusua37f.apps.googleusercontent.com';

$(document).on("click",".google", function(){
    $('.InviteFriendsInputs').hide();
	$('.progress-striped.loader').show();
	var config = {
       'client_id': clientId,
       'scope': 'https://www.google.com/m8/feeds',
	   'immediate': false,
	   'approval_prompt':'force'
     };
	 
     gapi.auth.authorize(config, function() {
       fetch(gapi.auth.getToken());  
	   var auth2 = gapi.auth.signOut();
   
	   //gapi.auth.setToken(null);
	   //gapi.auth.signOut();
     });
	 
	 function fetch(token) {
		 $.ajax({
			 url: "https://www.google.com/m8/feeds/contacts/default/full?access_token=" + token.access_token + "&alt=json&max-results=700&v=3.0",
			 dataType: "jsonp",
			 success:function(data) {
					// display all your data in console
					//console.log(JSON.stringify(data));
					 var email= [];
					 html = "";
					if(data.feed.entry.length > 0){
						
						for ( var i= 0 ; i < data.feed.entry.length ; i++)
						{
							if(data.feed.entry[i].gd$phoneNumber)
							{
								continue; 
							}
							if(data.feed.entry[i].gd$email){
							   email.push({
									name: data.feed.entry[i].title.$t,
									email: data.feed.entry[i].gd$email[0].address
								});
								username = (data.feed.entry[i].title.$t && data.feed.entry[i].title.$t!=='') ? data.feed.entry[i].title.$t : '---';
								html += "<div class='profile_box1'><input type='checkbox' /> <span>"+username+"</span><a href='#'>"+data.feed.entry[i].gd$email[0].address+"</a></div>";
							}
							else
							{ 
								continue;
							}
						}
						//console.log(email);
						getRegisteredUsers(email);
					}
				}
		});
	} 
});

function getRegisteredUsers(email){
	
	$.ajax({
		url: "getfriends.php",
		method: "post",
		data: {people:JSON.stringify(email)},
		type: "post",
		dataType:"json",
		success: function(data){
			if(data.requests!=''){
				$('.secrol').html(data.requests);
				$('.requestcount').html(data.requestsCount);
				requestCount = data.requestsCount;
			}
			if(data.inviations!=''){
				$('.contact-list').html(data.inviations);
				$('.invitationcount').html(data.inviationsCount);
				invitationCount = data.inviationsCount;
			}
			$('.Step2Process a.toggle-steps').click();
			$('.InviteFriendsInputs').show();
			$('.progress-striped.loader').hide();
			$('#sendFriendRequestfrm input[name="invtiations[]"]').on('click',function(){
				if($(this).is(':checked')){
					$('.requestcount').html( parseInt($('.requestcount').html())+1);
				}else{
					$('.requestcount').html( parseInt($('.requestcount').html())-1);
				}
			});
			
			$('#sendinvitation input[name="invtiations[]"]').on('click',function(){
				if($(this).is(':checked')){
					$('.invitationcount').html( parseInt($('.invitationcount').html())+1);
				}else{
					$('.invitationcount').html( parseInt($('.invitationcount').html())-1);
				}
			});
		},
		error: function(){
			//popup.hideLoader();
			//popup.alert('error', 'There are some errors');
		}
	});

}


function SendInvitation(){
	$.ajax({
		url: "sendinvitation.php",
		method: "post",
		data: $( '#sendinvitation' ).serialize(),
		type: "post",
		success: function(data){
			alert('Invitations has been sent successfully');
			document.location.href="signup2.php";
		},
		error: function(){
			//popup.hideLoader();
			//popup.alert('error', 'There are some errors');
		}
	});
}


function sendFrientRequest(){
	$.ajax({
		url: "sendfriendrequest.php",
		method: "post",
		data: $( '#sendFriendRequestfrm' ).serialize(),
		type: "post",
		success: function(data){
			$('.Step3Process a.toggle-steps').click();
		},
		error: function(){
			//popup.hideLoader();
			//popup.alert('error', 'There are some errors');
		}
	});
}

var YAHOO_CLIENT_ID = {
	'whatsdadilly.com' : 'dj0yJmk9Z1hOVVc4ZGxtNUFpJmQ9WVdrOWVUUmhabkpPTXpRbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD03NQ--'
}[window.location.hostname];

var CLIENT_IDS_ALL = {
	yahoo : YAHOO_CLIENT_ID
};

var OAUTH_PROXY_URL = 'https://auth-server.herokuapp.com/proxy';
hello.init( {
		yahoo : CLIENT_IDS_ALL.yahoo
	}
	, {
		redirect_uri:'http://whatsdadilly.com/betaPhase/invite.php',
		oauth_proxy : OAUTH_PROXY_URL,
		scope:"friends"
	}
);


function getFriends(network, path){

	/*var list = document.getElementById('list');
	list.innerHTML = '';
	var btn_more = document.getElementById('more');
	btn_more.style.display = 'none';*/

	// login
	hello(network).login({scope:'friends'}).then(function(auth) {
		// Get the friends
		// using path, me/friends or me/contacts
		hello(network).api(path, {limit:100}).then(function responseHandler(r) {
			console.log(r.data);
			for(var i=0;i<r.data.length;i++){
				var o = r.data[i];
				var li = document.createElement('li');
				li.innerHTML = o.name + (o.thumbnail?" <img src="+o.thumbnail+" />":'');
				list.appendChild(li);
			};

			btn_more.onclick = function(){
				// Make another request and handle is in the same way
				hello( network ).api( r.paging.next, {limit:10}, responseHandler );
			};
			btn_more.innerHTML = "Next from "+network;
			btn_more.style.display = 'block';
		});
	}, function() {
		if(!auth||auth.error){
			console.log("Signin aborted");
			return;
		}
	});
}

