	var formatTime = function (time) {

			return [

				Math.floor((time % 3600) / 60), // minutes

				('00' + Math.floor(time % 60)).slice(-2) // seconds

			].join('.');

	};
	
	
	function forward(wavesurfer,id){
		
		previous_object.skipForward();
		
	}
	
	function backward(wavesurfer,id){
		
		previous_object.skipBackward();
		
	}
	
	// function pause(wavesurfer,id){
		
		// $("#pause_"+id).hide();
		
		// $("#plya_"+id).show();
		
		// previous_object.pause();
		
	// }
	
	// function playAgain(wavesurfer,id){
		
		// $("#pause_"+id).show();
		
		// $("#plya_"+id).hide();
		
		// previous_object.play();
		
	// }
	
	var previous_object = '';
	
	var previous_id = '';
	
	var previous_path = '';
	
	
	function play_pause(wavesurfer,id,path){
		
		if(id===previous_id){
			
			//alert($("#ply_"+previous_id).html());
			
			if($("#ply_"+previous_id).html() === '<i class="fas fa-play"></i>'){
				
				$("#ply_"+previous_id).html('<i class="fas fa-pause"></i>');
				
			}else{
				
				$("#ply_"+previous_id).html('<i class="fas fa-play"></i>');
			}
			
			previous_object.playPause();
			
		}else{
			
			$("#ply_"+id).html('<i class="fas fa-pause"></i>');
			
				setTimeout(function(){
				
					if(previous_object){
						
						$("#ply_"+previous_id).html('<i class="fas fa-play"></i>');
						
						previous_object.empty();
			
						$('#counter_'+previous_id).text('');
																
						$('#duration_'+previous_id).text('');
					
						previous_object = '';
						
						previous_id		= '';
						
						previous_path	= '';
					
					}
				
					setTimeout(function(){
							
						wavesurfer.load(path);
						
						wavesurfer.on('ready', function () {
																
							$('#duration_'+id).text( formatTime(wavesurfer.getDuration()));
						
						});
				
						wavesurfer.on('audioprocess', function() {
							
							$('#counter_'+id).text( formatTime(wavesurfer.getCurrentTime()));
							
							if(wavesurfer.isPlaying()) { }
							
						});
					
						wavesurfer.on('finish',function(){
							
							// $("#pause_"+id).hide();
							// $("#plya_"+id).hide();
							// $("#ply_"+id).show();
							
						});

						var volumeInput = document.querySelector('#volume_'+id);
						
						var onChangeVolume = function (e) {
							
						wavesurfer.setVolume(e.target.value);
						
						console.log(e.target.value);
						
						};
					
						volumeInput.addEventListener('input', onChangeVolume);
						
						volumeInput.addEventListener('change', onChangeVolume);
						
						wavesurfer.play();
						
						previous_object = 	wavesurfer;
						previous_id 	=   id;
						previous_path 	=   path;
						
					},100);
				
				},100);
		
		}

	
		
	
	}