function findUrls(text)
{
  var source = (text || '').toString();
  var urlArray = [
  ];
  var url;
  var matchArray;
  var regexToken = /(((ftp|https?):\/\/)[\-\w@:%_\+.~#?,&\/\/=]+)|((mailto:)?[_.\w-]+@([\w][\w\-]+\.)+[a-zA-Z]{2,3})/g;
  while ((matchArray = regexToken.exec(source)) !== null)
  {
    var token = matchArray[0];
    urlArray.push(token);
  }
  return urlArray;
}
var lastParsedUrl = '';
var interval = false;
var semaphore = false;
function swithPhoto(move)
{
  var max = $('.image-wrapper').length - 1;
  var current = parseInt($('.image-wrapper:not(.hidden)').attr('rel'));
  var next = current + move;
  if (next === - 1)
  {
    next = max;
  } 
  else if (next > max)
  {
    next = 0;
  }
  $('#link_image').val($('.image-wrapper[rel="' + next + '"] img').attr('src'));
  $('.image-wrapper').addClass('hidden');
  $('.image-wrapper[rel="' + next + '"]').removeClass('hidden');
}
function attachFileReader(fileInput)
{
  if (window.FileReader) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $(fileInput).siblings('.preview').append($('<img></img>').attr('src', e.target.result).width(100));
      /*and now add one more*/
      var next = $('<div></div>').addClass('one-photo');
      var preview = $('<div></div>').addClass('preview');
      var icon = $('<i></i>').addClass('fa fa-plus');

      if($.cookie('socal_active') == 'twitter'){
        var input = $('<input></input>').attr('type', 'file').addClass('fileUpload').attr('name', 'photo[]');
      }
      else{
        var input = $('<input></input>').attr('type', 'file').addClass('fileUpload').attr('name', 'photo_' + $('.fileUpload').length);
      }
      
      var removeThisPicture = $('<div></div>').addClass('removePhoto').html('x').click(function (e)
      {
        e.preventDefault();
        next.remove();
      });
      next.append(input);
      next.append(removeThisPicture);
      next.append(icon);
      next.append(preview);
      $(fileInput).parent('.one-photo').after(next);
      input.change(function () {
        attachFileReader(this);
      });
    };
    reader.readAsDataURL($(fileInput) [0].files[0]);
  } 
  else
  {
    var next = $('<div></div>').addClass('one-photo');
    var preview = $('<div></div>').addClass('preview');

     if($.cookie('socal_active') == 'twitter'){
        var input = $('<input></input>').attr('type', 'file').addClass('fileUpload').attr('name', 'photo[]');
      }
      else{
        var input = $('<input></input>').attr('type', 'file').addClass('fileUpload').attr('name', 'photo_' + $('.fileUpload').length);
      }

      input.change(function () {
      attachFileReader(this);
    });
    var removeThisPicture = $('<div></div>').addClass('removePhoto').html('x').click(function (e)
    {
      e.preventDefault();
      next.remove();
    });
    var icon = $('<i></i>').addClass('fa fa-plus');
    next.append(input);
    next.append(removeThisPicture);
    next.append(icon);
    next.append(preview);
    $(fileInput).parent('.one-photo').after(next);
  }
}
function attachWallEvents()
{
  $('.add-comment textarea,.add-comment input').focus(function ()
  {
    clearInterval(interval);
  });
  $('.add-comment-link').click(function (e)
  {
    // alert($(this).attr('rel'));
    clearInterval(interval);
    e.preventDefault();
    $('.add-comment[rel="' + $(this).attr('rel') + '"]').fadeIn(500);
  });
  $('.add-comment input[type="file"]').change(function ()
  {
    var textarea = $(this).siblings('textarea');
    $(textarea).off('keydown');
    $(textarea).keydown(function (e)
    {
      if (e.keyCode === 13)
      {
        /*submit dat form!*/
      }
    });
  });
  //    $('.fancybox').click(function(e)
  //    {
  //        e.preventDefaulf();
  //    });
  //    $('.fancybox').fancybox({
  //        'transitionIn': 'elastic',
  //        'transitionOut': 'elastic',
  //        'speedIn': 600,
  //        'speedOut': 200
  //    });
  $('.add-comment textarea').keydown(function (e)
  {
    var id = $(this).parent('form').attr('rel');
    if (e.keyCode === 13)
    {
      /*enter*/
      e.preventDefault();
      /*ajax*/
      $.ajax({
        type: 'post',
        url: 'wall.php',
        data: {
          add_comment: true,
          id: id,
          text: $(this).val()
        },
        success: function (data)
        {
          /*reload the wall*/
          $('.wallEntries').html(data);
          attachWallEvents();
        }
      }
      );
    }
  });
}
var lastHtml = '';
function getWallAjax()
{
  semaphore = true;
  $.ajax({
    type: 'post',
    url: 'wall.php',
    data: {
      ajax_get: true,
      limit: $('input[name="limit"]').val()
    },
    success: function (data)
    {
      if (lastHtml === data)
      {
      } 
      else
      {
        $('.wallEntriessss').html(data);
        lastHtml = data;
        attachWallEvents();
      }
      $('.lazyLoader').html('');
      semaphore = false;
    }
  }
  );
}
$(document).ready(function ()
{
  // $('.postbutton').click(function(e)
  //        {
  //        $("#upload_photos").submit(function(event)
  //      {
  //          // var myData = $( "#upload_photos" ).serialize();
  //            var myData = new FormData(document.getElementById("upload_photos"));
  //
  //           $.ajax({
  //                type: "POST",
  //                enctype: 'multipart/form-data',
  //                url: "home.php",
  //                data: myData,
  //                success: function( data )
  //                {
  //                     alert( data );
  //                }
  //           });
  //           return false;
  //      });
  //        });
  $('.wallEntries').append('<p id=\'last\'></p>');
  $('#postedComments').append('<p id=\'last\'></p>');
  $(window).data('ajaxready', true).scroll(function (e) {
    console.log('4rth ');
    console.log($(window).data('ajaxready'));
    // alert("test");
    if ($(window).data('ajaxready') == false) return;
    //alert("test");
    var distanceTop = $('#last').offset().top - $(window).height();
    var style_display = $('#postedComments').css('display');
    // console.log("Window:"+$(window).scrollTop()+"  Distance"+parseInt(distanceTop));
    if ($(window).scrollTop() >= parseInt(distanceTop)) {
      // alert($(window).scrollTop() + ' ' + parseInt(distanceTop));
      // $(window).data('ajaxready', false);
      //  alert(style_display);
      var social_active = $('#social_menu').val();
      console.log(social_active);
      if (social_active == 'twitter') {
        // temporary inactive start
        // alert(social_active);
        // $(window).data('ajaxready', false);
        // //if($(".stream-container:last").attr('data-cursor') != 'undefined') {
        // $('div#loadmoreajaxloader').show();
        // $.ajax({
        //     cache: false,
        //     dataType : "html" ,
        //     url: "./twitter_ajax/tweet_infinite_load.php?twittid="+ $(".tweet-outer:last").attr('data')+"&count="+$(".tweet-outer:last").attr('data-count'),
        //     beforeSend: function()
        //     {
        //         $('body').show();
        //         $('.version').text(NProgress.version);
        //         NProgress.start();
        //     },
        //     complete: function(){
        //         setTimeout(function() {
        //             NProgress.done();
        //             $('.fade').removeClass('out');
        //         }, 500);
        //     },
        //     success: function(html) {
        //         if(html != '0' || html != ''){
        //             //$("#postedComments").append(html);
        //             $("#tw-loadorders").append(html);
        //             $("#last").remove();
        //             $("#postedComments").append( "<p id='last'></p>" );
        //             $('div#tw-loadMoreComments').hide();
        //         }else{
        //             $('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
        //         }
        //         $('.fancybox').fancybox();
        //         $(".fancybox-wrap").easydrag();
        //         $(".fancybox-effects-a").fancybox({
        //             helpers: {
        //                 title : {
        //                     type : 'outside'
        //                 },
        //                 overlay : {
        //                     speedOut : 0
        //                 }
        //             }
        //         });
        //         $(window).data('ajaxready', true);
        //     }
        // });
        // temporary inactive end .. 
        // new code from twitter home start 
        var twDistanceTop = $('#postedComments #last').offset().top - $(window).height();
        var condition = $('#twitter_condition').val();
        console.log("Window:"+$(window).scrollTop()+"  Distance"+parseInt(twDistanceTop));
        console.log(condition);
        if( parseInt($(window).scrollTop()) >= parseInt(twDistanceTop) && $('#tweet_list').has('center').length < 1){
        // if( parseInt($(window).scrollTop()) >= 2485 ){
            // alert('your')
          if (condition == '1') {
              if (style_display.toString() != 'none' ) {
                $(window).data('ajaxready', false);
                //if($(".stream-container:last").attr('data-cursor') != 'undefined') {
                $('div#loadmoreajaxloader').show();
                console.log(condition);
                $.ajax({
                  cache: false,
                  dataType: 'html',
                  url: './twitter_ajax/tweet_infinite_load.php?twittid=' + $('.tweet-outer:last').attr('data') + '&count=' + $('.tweet-outer:last').attr('data-count'),
                  success: function (html) {
                    if (html != '0' || html != '') {
                      //$("#loaders").append(html);
                      $('#postedComments #tweet_list').append(html);
                      $('#postedComments #last').remove();
                      $('#postedComments').append('<p id=\'last\'></p>');
                      $('div#loadMoreComments').hide();
                      $('div#loadmoreajaxloader').hide();
                    } else {
                      //$('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
                    }
                    $('.fancybox').fancybox();
                    $('.fancybox-wrap').easydrag();
                    $('.fancybox-effects-a').fancybox({
                      helpers: {
                        title: {
                          type: 'outside'
                        },
                        overlay: {
                          speedOut: 0
                        }
                      }
                    });
                    $(window).data('ajaxready', true);
                  }
                });
              }
            } else if (condition == '2') {
              $(window).data('ajaxready', false);
              //if($(".stream-container:last").attr('data-cursor') != 'undefined') {
              var tid = $('#current_tid').val();
              var screen = $('#current_tscreen').val();
              $('div#loadmoreajaxloader').show();
              $.ajax({
                cache: false,
                dataType: 'html',
                url: './twitter_ajax/twitter_user_infiniteload.php?tid=' + tid + '&screen=' + screen + '&twittid=' + $('.tweet-outer:last').attr('data') + '&count=' + $('.tweet-outer:last').attr('data-count'),
                success: function (html) {
                  if (html != '0' || html == '') {
                    //$("#loaders").append(html);
                      $('#postedComments #tweet_list').append(html);
                      $('#postedComments #last').remove();
                      $('#postedComments').append('<p id=\'last\'></p>');
                      $('div#loadMoreComments').hide();
                      $('div#loadmoreajaxloader').hide();
                  } else {
                    //$('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
                  }
                  $('.fancybox').fancybox();
                  $('.fancybox-wrap').easydrag();
                  $('.fancybox-effects-a').fancybox({
                    helpers: {
                      title: {
                        type: 'outside'
                      },
                      overlay: {
                        speedOut: 0
                      }
                    }
                  });
                  $(window).data('ajaxready', true);
                }
              });
          } 
          else if (condition == '4') {
            $(window).data('ajaxready', false);
            // if($(".stream-container:last").attr('data-cursor') != 'undefined') {
            $('div#loadmoreajaxloader').show();
            $.ajax({
              cache: false,
              dataType: 'html',
              url: './twitter_ajax/followers_infinite_load.php?next_cursor=' + $('.stream-container:last').attr('data-cursor'),
              success: function (html) {
                if (html != '0' || html == '') {
                  //$("#loaders").append(html);
                      $('#postedComments #tweet_list').append(html);
                      $('#postedComments #last').remove();
                      $('#postedComments').append('<p id=\'last\'></p>');
                      $('div#loadMoreComments').hide();
                      $('div#loadmoreajaxloader').hide();
                } else {
                  //$('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
                }
                $('.fancybox').fancybox();
                $('.fancybox-wrap').easydrag();
                $('.fancybox-effects-a').fancybox({
                  helpers: {
                    title: {
                      type: 'outside'
                    },
                    overlay: {
                      speedOut: 0
                    }
                  }
                });
                $(window).data('ajaxready', true);
              }
            });
            // }
          } else if (condition == '5') {
            $(window).data('ajaxready', false);
            $('div#loadmoreajaxloader').show();
            $.ajax({
              cache: false,
              dataType: 'html',
              url: './twitter_ajax/following_infinite_load.php?next_cursor=' + $('.stream-container:last').attr('data-cursor'),
              success: function (html) {
                if (html != '0' || html == '') {
                  //$("#loaders").append(html);
                      $('#postedComments #tweet_list').append(html);
                      $('#postedComments #last').remove();
                      $('#postedComments').append('<p id=\'last\'></p>');
                      $('div#loadMoreComments').hide();
                      $('div#loadmoreajaxloader').hide();
                } else if (html == 0) {
                  $('div#loadmoreajaxloader').hide();
                } 
                else {
                  //$('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
                }
                $('.fancybox').fancybox();
                $('.fancybox-wrap').easydrag();
                $('.fancybox-effects-a').fancybox({
                  helpers: {
                    title: {
                      type: 'outside'
                    },
                    overlay: {
                      speedOut: 0
                    }
                  }
                });
                $(window).data('ajaxready', true);
              }
            });
            } else if (condition == '6') {
              $(window).data('ajaxready', false);
              // if($(".stream-container:last").attr('data-cursor') != 'undefined') {
              $('div#loadmoreajaxloader').show();
              var screen = $('#current_tscreen').val();
              $.ajax({
                cache: false,
                dataType: 'html',
                url: './twitter_ajax/userfollowers_infinite_load.php?screen=' + screen + '&next_cursor=' + $('.stream-container:last').attr('data-cursor'),
                success: function (html) {
                  if (html != '0' || html == '') {
                    //$("#loaders").append(html);
                      $('#postedComments #tweet_list').append(html);
                      $('#postedComments #last').remove();
                      $('#postedComments').append('<p id=\'last\'></p>');
                      $('div#loadMoreComments').hide();
                      $('div#loadmoreajaxloader').hide();
                  } else {
                    //$('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
                  }
                  $('.fancybox').fancybox();
                  $('.fancybox-effects-a').fancybox({
                    helpers: {
                      title: {
                        type: 'outside'
                      },
                      overlay: {
                        speedOut: 0
                      }
                    }
                  });
                  $(window).data('ajaxready', true);
                }
              });
              // }
            } else if (condition == '7') {
              $(window).data('ajaxready', false);
              // if($(".stream-container:last").attr('data-cursor') != 'undefined') {
              $('div#loadmoreajaxloader').show();
              var screen = $('#current_tscreen').val();
              $.ajax({
                cache: false,
                dataType: 'html',
                url: './twitter_ajax/userfollowing_infinite_load.php?screen=' + screen + '&next_cursor=' + $('.stream-container:last').attr('data-cursor'),
                success: function (html) {
                  if (html != '0' || html == '') {
                    //$("#loaders").append(html);
                      $('#postedComments #tweet_list').append(html);
                      $('#postedComments #last').remove();
                      $('#postedComments').append('<p id=\'last\'></p>');
                      $('div#loadMoreComments').hide();
                      $('div#loadmoreajaxloader').hide();
                  } else {
                    //$('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
                  }
                  $('.fancybox').fancybox();
                  $('.fancybox-effects-a').fancybox({
                    helpers: {
                      title: {
                        type: 'outside'
                      },
                      overlay: {
                        speedOut: 0
                      }
                    }
                  });
                  $(window).data('ajaxready', true);
                }
              });
              // }
            }        // new code from twitter home end

        }

        

      } else if (social_active == 'wdd') {
        // alert(social_active);
         $(window).data('ajaxready', false);
         
        $.ajax({
          cache: false,
          dataType: 'html',
          url: 'wall_infinite_load.php?wall_id=' + $('.crispbxmain:last').attr('data') + '&count=' + $('.crispbxmain:last').attr('data-count'),
          beforeSend: function ()
          {
            $('body').show();
            $('.version').text(NProgress.version);
            NProgress.start();
          },
          complete: function () {
            setTimeout(function () {
              NProgress.done();
              $('.fade').removeClass('out');
            }, 500);
          },
          success: function (html) {
            //alert(html);
            if (html != '0' || html != '') {
              // $('#loadorders').append(html);
              $('.wallEntries').append(html);
              $('.wallEntries #last').remove();
              $('.wallEntries').append('<p id=\'last\'></p>');
              $('div#loadMoreComments').hide();
            } else {
              $('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
            }
            $(window).data('ajaxready', true);
          }
        });
      } else if (social_active == 'instagram') {
        var ig_active = $('#ig_menu_active').val();
        // alert(ig_active);
        if (ig_active == 'ig_home_wall') {
          var lastid = $('.timelineItem:last').attr('data-id');
          //alert(social_active);
          //alert(lastid);
          $.ajax({
            cache: false,
            dataType: 'html',
            url: './instagram_ajax/ig_infinite_load.php?nextid=' + lastid,
            beforeSend: function ()
            {
              $('body').show();
              $('.version').text(NProgress.version);
              NProgress.start();
            },
            complete: function () {
              setTimeout(function () {
                NProgress.done();
                $('.fade').removeClass('out');
              }, 500);
            },
            success: function (html) {
              if (html != '0' || html == '') {
                $('.wallEntries').append(html);
                //                                                              $("#loadorders").append(html);
                $('#last').remove();
                $('#postedComments').append('<p id=\'last\'></p>');
                //                                                           $('div#loadMoreComments').hide();
              } else {
                $('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
              }
              $(window).data('ajaxready', true);
            }
          });
        } 
        else if (ig_active == 'ig_user_wall') {
          var lastidus = $('.timelineItem:last').attr('data-id');
          $.ajax({
            cache: false,
            dataType: 'html',
            url: './instagram_ajax/ig_infinite_load_userwall.php?nextid=' + lastidus,
            beforeSend: function ()
            {
              $('body').show();
              $('.version').text(NProgress.version);
              NProgress.start();
            },
            complete: function () {
              setTimeout(function () {
                NProgress.done();
                $('.fade').removeClass('out');
              }, 500);
            },
            success: function (html) {
              if (html != '0' || html == '') {
                $('.wallEntries').append(html);
                //                                                              $("#loadorders").append(html);
                $('#last').remove();
                $('#postedComments').append('<p id=\'last\'></p>');
                //                                                           $('div#loadMoreComments').hide();
              } else {
                $('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
              }
              $(window).data('ajaxready', true);
            }
          });
        } else if (ig_active == 'ig_user_followers') {
          var lastidf = $('.stream-container:last').attr('data-cursor');
          var userid = $('.stream-container:last').attr('data-userid');
          $.ajax({
            cache: false,
            dataType: 'html',
            type: 'POST',
            url: './instagram_ajax/ig_infinite_load_followers.php?nextid=' + lastidf + '&userid=' + userid,
            beforeSend: function ()
            {
              $('body').show();
              $('.version').text(NProgress.version);
              NProgress.start();
            },
            complete: function () {
              setTimeout(function () {
                NProgress.done();
                $('.fade').removeClass('out');
              }, 500);
            },
            success: function (html) {
              if (html != '0' || html == '') {
                $('.wallEntriesIgFollowers').append(html);
                $('#loadorders').append(html);
                $('#last').remove();
                $('#postedComments').append('<p id=\'last\'></p>');
                $('div#loadMoreComments').hide();
              } else {
                $('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
              }
              $(window).data('ajaxready', true);
            }
          });
        } else if (ig_active == 'ig_user_following') {
          var lastidfw = $('.stream-container:last').attr('data-cursor');
          var useridfw = $('.stream-container:last').attr('data-userid');
          $.ajax({
            cache: false,
            dataType: 'html',
            type: 'POST',
            url: './instagram_ajax/ig_infinite_load_following.php?nextid=' + lastidfw + '&userid=' + useridfw,
            beforeSend: function ()
            {
              $('body').show();
              $('.version').text(NProgress.version);
              NProgress.start();
            },
            complete: function () {
              setTimeout(function () {
                NProgress.done();
                $('.fade').removeClass('out');
              }, 500);
            },
            success: function (html) {
              if (html != '0' || html == '') {
                $('.wallEntriesIgFollowing').append(html);
                $('#loadorders').append(html);
                $('#last').remove();
                $('#postedComments').append('<p id=\'last\'></p>');
                $('div#loadMoreComments').hide();
              } else {
                $('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
              }
              $(window).data('ajaxready', true);
            }
          });
        }
      }
    }
  });
  $('.removePhoto').click(function ()
  {
    $(this).parent('.one-photo').remove();
  });
  $('.picon').click(function (e)
  {
    e.preventDefault();
    $('#picture').fadeIn(500);
    //$('textarea.grybord').css('display','none');
    $('.postbutton').off('click');
    $('.postbutton').click(function (e)
    {
      // $('#upload_photos').submit();
    });
  });
  $('.grybord').focus(function ()
  {
    if ($(this).val() === 'Whats in your head?')
    {
      $(this).val('');
    }
  });
  $('#picture input[type="file"]').change(function ()
  {
    attachFileReader(this);
  });
  /*$("#tabs").tabs();*/
  if (typeof $('.wallEntries') [0] !== 'undefined')
  {
    interval = setInterval(function ()
    {
      getWallAjax();
    }, 17500);
  }
  getWallAjax();
  $('.postbutton').click(function (e)
  {
    e.preventDefault();
    $(this).after($('<div></div>').addClass('ajax-loader'));
    var ownerId = '';
    if ($('#ownerId').val()) {
      ownerId = $('#ownerId').val();
    }
    $.ajax({
      url: 'wallentry.php',
      type: 'post',
      data: {
        ajax_add: 'true',
        text: $('.grybord').val(),
        link: $('#link').val(),
        ownerId: ownerId,
        link_photo: $('#link_image').val(),
        link_title: $('input[id="link_title"]').val(),
        link_description: $('input[id="link_description"]').val()
      },
      success: function (msg)
      {
        $('.wallEntries').prepend(msg);
        $('.ajax-loader').remove();
        $('#link').val('');
        $('#link_image').val('');
        $('input[id="link_title"]').val('');
        $('input[id="link_description"]').val('');
        $('textarea[name="status"]').val('');
        $('#link_info').html('');
        $('#link_info').css('display', 'none');
        lastParsedUrl = false;
      }
    });
  });
  $('.postbutton-comments').click(function (e)
  {
    //alert($(this).attr('id'));
    var wall_id = $(this).attr('id');
    var formdata = $('#commform_' + wall_id).serialize();
    $.ajax({
      url: 'wall_comment_post.php',
      type: 'post',
      data: formdata,
      success: function (msg)
      {
        $('#comment_' + wall_id).prepend(msg);
        $('#comment_' + wall_id).css('display', 'block');
      }
    });
    //wall_comment_post
  });
  /*analise status to getch http or https*/
  $('textarea.grybord').keyup(function ()
  {
    /*get url*/
    var urls = findUrls($('textarea.grybord').val());
    if (urls.length > 0 && lastParsedUrl !== urls[0])
    {
      $.ajax({
        url: ajaxUrl,
        type: 'post',
        data: {
          action: 'getUrlData',
          url: urls[0]
        },
        beforeSend: function ()
        {
          lastParsedUrl = urls[0];
          $('#link_info').html('');
          $('#link_info').css('display', 'table').addClass('info_linkbox');
          $('#link_info').append($('<div></div>').addClass('ajax-loader'));
        },
        dataType: 'json',
        success: function (data)
        {
          $('#link_info').css('display', 'table').addClass('info_linkbox');
          $('.ajax-loader').remove();
          $('#link_info').html('');
          /*now render the link*/
          var content_wrapper = $('<div></div>').attr('class', 'content_wrapper').attr('style', 'padding:10px;');
          var photos = $('<div class="photo-browser" style="width:100%;"></div>');
          var title = $('<h4 class="scrapedTitle" style="float:left;line-height:1.0em;"></p>').html(data.title);
          var description = $('<p style="float:left;font-size:12pt;line-height:1.0em;"></p>').html(data.description);
          var left = $('<div style="display:none;">&laquo</div>').addClass('left-arrow').click(function (e)
          {
            e.preventDefault();
            swithPhoto( - 1);
          });
          var right = $('<div style="display:none;">&raquo</div>').addClass('right-arrow').click(function (e)
          {
            e.preventDefault();
            swithPhoto(1);
          });
          var terminator = $('<a style="float:left;margin:5px;position:absolute;left:93%;color: #ccc;font-weight: bold;cursor:pointer;"></a>').addClass('terminator').html('X').click(function ()
          {
            $('#link_info').html('');
            $('#link_info').css('display', 'none');
          });
          //var link_image = $('<input></input>').attr('type', 'hidden').attr('id', 'link_image').val(data.images);
          // $('#link_info').append(link_image);
          //var img = $('<img width="150px" height="120px" align="center"></img>').attr('src', data.images);
          // $('#link_info').append(img);
          $(photos).append(left);
          $(photos).append(right);
          var i;
          for (i = 0; i < data.images.length; i++)
          {
            //alert()
            var imgWrapper = $('<div></div>').addClass('image-wrapper').attr('rel', i);
            var img = $('<img style="width:100%;"></img>').attr('src', data.images[i]);
            if (i > 0)
            {
              imgWrapper.addClass('hidden');
            } 
            else
            {
              var link_image = $('<input></input>').attr('type', 'hidden').attr('id', 'link_image').val(data.images[i]);
              if (data.contenttype != 'video') {
                $('#link_info').append(link_image);
              }
            }
            $(imgWrapper).append(img);
            $(photos).append(imgWrapper);
          }
          var link_title = $('<input></input>').attr('id', 'link_title').val(data.title).attr('type', 'hidden');
          var link = $('<input></input>').attr('id', 'link').val(urls[0]).attr('type', 'hidden');
          var link_description = $('<input></input>').attr('id', 'link_description').val(data.description).attr('type', 'hidden');
          $('#link_info').append(terminator);
          $('#link_info').append(photos);
          $('#link_info').append(content_wrapper);
          //$(content_wrapper).append(terminator);
          $(content_wrapper).append(title);
          $(content_wrapper).append(description);
          $('#link_info').append(link_title);
          $('#link_info').append(link);
          $('#link_info').append(link_description);
        }
      });
    }
  });
  $('.add-comment-link').click(function (e) {
    var id = $(this).attr('rel');
    $('#comment_' + id).css('display', 'block');
  })
  $(window).scroll(function (e) {
    console.log('2nd ');
    var scroll = parseInt($(window).scrollTop());
    var height = parseInt($(window).height());
    var percent = scroll / height;
    if (percent > 0.8 && !semaphore)
    {
      semaphore = true;
      $('input[name="limit"]').val(parseInt($('input[name="limit"]').val()) + 10);
      $('.lazyLoader').append($('<div></div>').addClass('ajax-loader'));
    }
  });
  $('#back-top').hide();
  // fade in #back-top
  $(function () {
    $(window).scroll(function () {
      console.log('3rd ');
      if ($(this).scrollTop() > 100) {
        $('#back-top').fadeIn();
      } else {
        $('#back-top').fadeOut();
      }
    });
    // scroll body to 0px on click
    $('#back-top a').click(function () {
      $('body,html').animate({
        scrollTop: 0
      }, 800);
      return false;
    });
  });
});
