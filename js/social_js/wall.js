function getSearchParameters() {
    var prmstr = window.location.search.substr(1);
    return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
}

function transformToAssocArray(prmstr) {
    var params = {};
    var prmarr = prmstr.split("&");
    for (var i = 0; i < prmarr.length; i++) {
        var tmparr = prmarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }
    return params;
}

var params = getSearchParameters();

function findUrls(text) {
    var source = (text || '').toString();
    var urlArray = [];
    var url;
    var matchArray;
    var regexToken = /(((ftp|https?):\/\/)[\-\w@:%_\+.~#?,&\/\/=]+)|((mailto:)?[_.\w-]+@([\w][\w\-]+\.)+[a-zA-Z]{2,3})/g;
    while ((matchArray = regexToken.exec(source)) !== null) {
        var token = matchArray[0];
        urlArray.push(token);
    }
    return urlArray;
}
var lastParsedUrl = '';
var interval = false;
var semaphore = false;

function swithPhoto(move) {
    var max = $('.image-wrapper').length - 1;
    var current = parseInt($('.image-wrapper:not(.hidden)').attr('rel'));
    var next = current + move;
    if (next === -1) {
        next = max;
    } else if (next > max) {
        next = 0;
    }
    $('#link_image').val($('.image-wrapper[rel="' + next + '"] img').attr('src'));
    $('.image-wrapper').addClass('hidden');
    $('.image-wrapper[rel="' + next + '"]').removeClass('hidden');
}

var nextIndex = 1;

function attachFileReader(fileInput) {
    if (window.FileReader) {
        var reader = new FileReader();
        reader.onload = function(e) {
            if ($(fileInput).siblings('.preview').html() != "") {
                // $(fileInput).siblings('.preview').html('');
                $(fileInput).siblings('.preview').html($('<img></img>').attr('src', e.target.result).width(100));
                return;
            }

            $(fileInput).siblings('.preview').append($('<img></img>').attr('src', e.target.result).width(100));
            /*and now add one more*/
            // nextIndex = $('#picture input[type="file"]').size() + 1;
            nextIndex = nextIndex + 1;
            var next = $('<div></div>').addClass('one-photo');
            var preview = $('<div></div>').addClass('preview');
            var icon = $('<i></i>').addClass('fa fa-plus');

            if ($.cookie('socal_active') == 'twitter') {
                var input = $('<input></input>').attr('type', 'file').addClass('fileUpload').addClass('imageUploads').attr('data-index', nextIndex).attr('name', 'photo[]');
            } else {
                var input = $('<input></input>').attr('type', 'file').addClass('fileUpload').addClass('imageUploads').attr('data-index', nextIndex).attr('name', 'photo_' + $('.fileUpload').length);
            }

            var removeThisPicture = $('<div></div>').addClass('removePhoto').html('x').click(function(e) {
                if ($('#picture .one-photo').size() > 1) {

                    if ($(this).parent('.one-photo').children('.preview').html() != "") {
                        // if ($('#picture .one-photo .preview').html() != "") {
                        // console.log($(this).parent('.one-photo').children('.preview').html());
                        console.log($(this).tagName);
                        e.preventDefault();
                        next.remove();
                    }

                } else {
                    e.preventDefault();
                    $('#imageUploads').val('');
                    $('#picture').fadeOut(500);
                    // $(".one-photo").hide();
                }
            });
            next.append(input);
            next.append(removeThisPicture);
            next.append(icon);
            next.append(preview);
            $(fileInput).parent('.one-photo').after(next);
            input.change(function() {
                attachFileReader(this);
            });
        };
        reader.readAsDataURL($(fileInput)[0].files[0]);
    } else {
        var next = $('<div></div>').addClass('one-photo');
        var preview = $('<div></div>').addClass('preview');

        if ($.cookie('socal_active') == 'twitter') {
            var input = $('<input></input>').attr('type', 'file').addClass('fileUpload').attr('name', 'photo[]');
        } else {
            var input = $('<input></input>').attr('type', 'file').addClass('fileUpload').attr('name', 'photo_' + $('.fileUpload').length);
        }

        input.change(function() {
            attachFileReader(this);
        });
        var removeThisPicture = $('<div></div>').addClass('removePhoto').html('x').click(function(e) {
            if ($('#picture .one-photo').size() > 1) {

                if ($(this).parent('.one-photo').children('.preview').html() != "") {
                    // if ($('#picture .one-photo .preview').html() != "") {
                    // console.log($(this).parent('.one-photo').children('.preview').html());
                    console.log($(this).tagName);
                    e.preventDefault();
                    next.remove();
                }

            } else {
                e.preventDefault();
                $('#imageUploads').val('');
                $('#picture').fadeOut(500);
                // $(".one-photo").hide();
            }
        });
        var icon = $('<i></i>').addClass('fa fa-plus');
        next.append(input);
        next.append(removeThisPicture);
        next.append(icon);
        next.append(preview);
        $(fileInput).parent('.one-photo').after(next);
    }
}

function attachWallEvents() {
    $('.add-comment textarea,.add-comment input').focus(function() {
        clearInterval(interval);
    });
    $('.add-comment-link').click(function(e) {
        // alert($(this).attr('rel'));
        clearInterval(interval);
        e.preventDefault();
        $('.add-comment[rel="' + $(this).attr('rel') + '"]').fadeIn(500);
    });
    $('.add-comment input[type="file"]').change(function() {
        var textarea = $(this).siblings('textarea');
        $(textarea).off('keydown');
        $(textarea).keydown(function(e) {
            if (e.keyCode === 13) {
                /*submit dat form!*/
            }
        });
    });
    //    $('.fancybox').click(function(e)
    //    {
    //        e.preventDefaulf();
    //    });
    //    $('.fancybox').fancybox({
    //        'transitionIn': 'elastic',
    //        'transitionOut': 'elastic',
    //        'speedIn': 600,
    //        'speedOut': 200
    //    });
    $('.add-comment textarea').keydown(function(e) {
        var id = $(this).parent('form').attr('rel');
        if (e.keyCode === 13) {
            /*enter*/
            e.preventDefault();
            /*ajax*/
            $.ajax({
                type: 'post',
                url: 'wall.php',
                data: {
                    add_comment: true,
                    id: id,
                    text: $(this).val()
                },
                success: function(data) {
                    /*reload the wall*/
                    $('.wallEntries').html(data);
                    attachWallEvents();
                }
            });
        }
    });
}
var lastHtml = '';

function getWallAjax() {
    semaphore = true;
    $.ajax({
        type: 'post',
        url: 'wall.php',
        data: {
            ajax_get: true,
            limit: $('input[name="limit"]').val()
        },
        success: function(data) {
            if (lastHtml === data) {} else {
                $('.wallEntriessss').html(data);
                lastHtml = data;
                attachWallEvents();
            }
            $('.lazyLoader').html('');
            semaphore = false;
        }
    });
}
$(document).ready(function() {
  
    $('.wallEntries').append('<p id=\'last\'></p>');
    $('#postedComments').append('<p id=\'last\'></p>');
    $(window).data('ajaxready', true).scroll(function(e) {
        console.log('4rth ');
        console.log($(window).data('ajaxready'));
        // alert("test");
        if ($(window).data('ajaxready') == false) return;
        //alert("test");
        var distanceTop = $('#last').offset().top - $(window).height();
        var style_display = $('#postedComments').css('display');
        // console.log("Window:"+$(window).scrollTop()+"  Distance"+parseInt(distanceTop));
        if ($(window).scrollTop() >= parseInt(distanceTop)) {
            // alert($(window).scrollTop() + ' ' + parseInt(distanceTop));
            // $(window).data('ajaxready', false);
            //  alert(style_display);
            var social_active = $('#social_menu').val();

            if (social_active == 'twitter') {
                // temporary inactive start
                // alert(social_active);
                // $(window).data('ajaxready', false);
                // //if($(".stream-container:last").attr('data-cursor') != 'undefined') {
                // $('div#loadmoreajaxloader').show();
                // $.ajax({
                //     cache: false,
                //     dataType : "html" ,
                //     url: "./twitter_ajax/tweet_infinite_load.php?twittid="+ $(".tweet-outer:last").attr('data')+"&count="+$(".tweet-outer:last").attr('data-count'),
                //     beforeSend: function()
                //     {
                //         $('body').show();
                //         $('.version').text(NProgress.version);
                //         NProgress.start();
                //     },
                //     complete: function(){
                //         setTimeout(function() {
                //             NProgress.done();
                //             $('.fade').removeClass('out');
                //         }, 500);
                //     },
                //     success: function(html) {
                //         if(html != '0' || html != ''){
                //             //$("#postedComments").append(html);
                //             $("#tw-loadorders").append(html);
                //             $("#last").remove();
                //             $("#postedComments").append( "<p id='last'></p>" );
                //             $('div#tw-loadMoreComments').hide();
                //         }else{
                //             $('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
                //         }
                //         $('.fancybox').fancybox();
                //         $(".fancybox-wrap").easydrag();
                //         $(".fancybox-effects-a").fancybox({
                //             helpers: {
                //                 title : {
                //                     type : 'outside'
                //                 },
                //                 overlay : {
                //                     speedOut : 0
                //                 }
                //             }
                //         });
                //         $(window).data('ajaxready', true);
                //     }
                // });
                // temporary inactive end .. 
                // new code from twitter home start 
                var twDistanceTop = $('#postedComments #last').offset().top - $(window).height();
                var condition = $('#twitter_condition').val();
                console.log("Window:" + $(window).scrollTop() + "  Distance" + parseInt(twDistanceTop));
                console.log(condition);
                if (parseInt($(window).scrollTop()) >= parseInt(twDistanceTop) && $('#tweet_list').has('center').length < 1) {
                    // if( parseInt($(window).scrollTop()) >= 2485 ){
                    // alert('your')
                    if (condition == '1') {
                        if (style_display.toString() != 'none') {
                            $(window).data('ajaxready', false);
                            //if($(".stream-container:last").attr('data-cursor') != 'undefined') {
                            $('div#loadmoreajaxloader').show();
                            console.log(condition);
                            $.ajax({
                                cache: false,
                                dataType: 'html',
                                url: './twitter_ajax/tweet_infinite_load.php?twittid=' + $('.tweet-outer:last').attr('data') + '&count=' + $('.tweet-outer:last').attr('data-count'),
                                success: function(html) {
                                    if (html != '0' || html != '') {
                                        //$("#loaders").append(html);
                                        $('#postedComments #tweet_list').append(html);
                                        $('#postedComments #last').remove();
                                        $('#postedComments').append('<p id=\'last\'></p>');
                                        $('div#loadMoreComments').hide();
                                        $('div#loadmoreajaxloader').hide();
                                    } else {
                                        //$('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
                                    }
                                    $('.fancybox').fancybox();
                                    $('.fancybox-wrap').easydrag();
                                    $('.fancybox-effects-a').fancybox({
                                        helpers: {
                                            title: {
                                                type: 'outside'
                                            },
                                            overlay: {
                                                speedOut: 0
                                            }
                                        }
                                    });
                                    $(window).data('ajaxready', true);
                                }
                            });
                        }
                    } else if (condition == '2') {
                        $(window).data('ajaxready', false);
                        //if($(".stream-container:last").attr('data-cursor') != 'undefined') {
                        var tid = $('#current_tid').val();
                        var screen = $('#current_tscreen').val();
                        $('div#loadmoreajaxloader').show();
                        $.ajax({
                            cache: false,
                            dataType: 'html',
                            url: './twitter_ajax/twitter_user_infiniteload.php?tid=' + tid + '&screen=' + screen + '&twittid=' + $('.tweet-outer:last').attr('data') + '&count=' + $('.tweet-outer:last').attr('data-count'),
                            success: function(html) {
                                if (html != '0' || html == '') {
                                    //$("#loaders").append(html);
                                    $('#postedComments #tweet_list').append(html);
                                    $('#postedComments #last').remove();
                                    $('#postedComments').append('<p id=\'last\'></p>');
                                    $('div#loadMoreComments').hide();
                                    $('div#loadmoreajaxloader').hide();
                                } else {
                                    //$('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
                                }
                                $('.fancybox').fancybox();
                                $('.fancybox-wrap').easydrag();
                                $('.fancybox-effects-a').fancybox({
                                    helpers: {
                                        title: {
                                            type: 'outside'
                                        },
                                        overlay: {
                                            speedOut: 0
                                        }
                                    }
                                });
                                $(window).data('ajaxready', true);
                            }
                        });
                    } else if (condition == '4') {
                        $(window).data('ajaxready', false);
                        // if($(".stream-container:last").attr('data-cursor') != 'undefined') {
                        $('div#loadmoreajaxloader').show();
                        $.ajax({
                            cache: false,
                            dataType: 'html',
                            url: './twitter_ajax/followers_infinite_load.php?next_cursor=' + $('.stream-container:last').attr('data-cursor'),
                            success: function(html) {
                                if (html != '0' || html == '') {
                                    //$("#loaders").append(html);
                                    $('#postedComments #tweet_list').append(html);
                                    $('#postedComments #last').remove();
                                    $('#postedComments').append('<p id=\'last\'></p>');
                                    $('div#loadMoreComments').hide();
                                    $('div#loadmoreajaxloader').hide();
                                } else {
                                    //$('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
                                }
                                $('.fancybox').fancybox();
                                $('.fancybox-wrap').easydrag();
                                $('.fancybox-effects-a').fancybox({
                                    helpers: {
                                        title: {
                                            type: 'outside'
                                        },
                                        overlay: {
                                            speedOut: 0
                                        }
                                    }
                                });
                                $(window).data('ajaxready', true);
                            }
                        });
                        // }
                    } else if (condition == '5') {
                        $(window).data('ajaxready', false);
                        $('div#loadmoreajaxloader').show();
                        $.ajax({
                            cache: false,
                            dataType: 'html',
                            url: './twitter_ajax/following_infinite_load.php?next_cursor=' + $('.stream-container:last').attr('data-cursor'),
                            success: function(html) {
                                if (html != '0' || html == '') {
                                    //$("#loaders").append(html);
                                    $('#postedComments #tweet_list').append(html);
                                    $('#postedComments #last').remove();
                                    $('#postedComments').append('<p id=\'last\'></p>');
                                    $('div#loadMoreComments').hide();
                                    $('div#loadmoreajaxloader').hide();
                                } else if (html == 0) {
                                    $('div#loadmoreajaxloader').hide();
                                } else {
                                    //$('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
                                }
                                $('.fancybox').fancybox();
                                $('.fancybox-wrap').easydrag();
                                $('.fancybox-effects-a').fancybox({
                                    helpers: {
                                        title: {
                                            type: 'outside'
                                        },
                                        overlay: {
                                            speedOut: 0
                                        }
                                    }
                                });
                                $(window).data('ajaxready', true);
                            }
                        });
                    } else if (condition == '6') {
                        $(window).data('ajaxready', false);
                        // if($(".stream-container:last").attr('data-cursor') != 'undefined') {
                        $('div#loadmoreajaxloader').show();
                        var screen = $('#current_tscreen').val();
                        $.ajax({
                            cache: false,
                            dataType: 'html',
                            url: './twitter_ajax/userfollowers_infinite_load.php?screen=' + screen + '&next_cursor=' + $('.stream-container:last').attr('data-cursor'),
                            success: function(html) {
                                if (html != '0' || html == '') {
                                    //$("#loaders").append(html);
                                    $('#postedComments #tweet_list').append(html);
                                    $('#postedComments #last').remove();
                                    $('#postedComments').append('<p id=\'last\'></p>');
                                    $('div#loadMoreComments').hide();
                                    $('div#loadmoreajaxloader').hide();
                                } else {
                                    //$('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
                                }
                                $('.fancybox').fancybox();
                                $('.fancybox-effects-a').fancybox({
                                    helpers: {
                                        title: {
                                            type: 'outside'
                                        },
                                        overlay: {
                                            speedOut: 0
                                        }
                                    }
                                });
                                $(window).data('ajaxready', true);
                            }
                        });
                        // }
                    } else if (condition == '7') {
                        $(window).data('ajaxready', false);
                        // if($(".stream-container:last").attr('data-cursor') != 'undefined') {
                        $('div#loadmoreajaxloader').show();
                        var screen = $('#current_tscreen').val();
                        $.ajax({
                            cache: false,
                            dataType: 'html',
                            url: './twitter_ajax/userfollowing_infinite_load.php?screen=' + screen + '&next_cursor=' + $('.stream-container:last').attr('data-cursor'),
                            success: function(html) {
                                if (html != '0' || html == '') {
                                    //$("#loaders").append(html);
                                    $('#postedComments #tweet_list').append(html);
                                    $('#postedComments #last').remove();
                                    $('#postedComments').append('<p id=\'last\'></p>');
                                    $('div#loadMoreComments').hide();
                                    $('div#loadmoreajaxloader').hide();
                                } else {
                                    //$('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
                                }
                                $('.fancybox').fancybox();
                                $('.fancybox-effects-a').fancybox({
                                    helpers: {
                                        title: {
                                            type: 'outside'
                                        },
                                        overlay: {
                                            speedOut: 0
                                        }
                                    }
                                });
                                $(window).data('ajaxready', true);
                            }
                        });
                        // }
                    } // new code from twitter home end

                }



            } else if (social_active == 'wdd') {
                // alert(social_active);
                $(window).data('ajaxready', false);
                var profileid = $('#profileid').val();
                //alert($('.crispbxmain:last').attr('data-count'));return false;
                $.ajax({
                    cache: false,
                    dataType: 'html',
                    url: 'social_wall_infinite_load.php?profileid=' + profileid + '&wall_id=' + $('.crispbxmain:last').attr('data') + '&count=' + $('.crispbxmain:last').attr('data-count'),
                    beforeSend: function() {
                        $('body').show();
                        $('.version').text(NProgress.version);
                        NProgress.start();
                    },
                    complete: function() {
                        setTimeout(function() {
                            NProgress.done();
                            $('.fade').removeClass('out');
                        }, 500);
                    },
                    success: function(html) {
                        //alert(html);
                        if (html != '0' || html != '') {
                            // $('#loadorders').append(html);
                            $('.wallEntries').append(html);
                            $('.wallEntries #last').remove();
                            $('.wallEntries').append('<p id=\'last\'></p>');
                            $('div#loadMoreComments').hide();
                        } else {
                            $('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
                        }
                        $(window).data('ajaxready', true);
                    }
                });
            } else if (social_active == 'instagram') {
                var ig_active = $('#ig_menu_active').val();
                // alert(ig_active);
                if (ig_active == 'ig_home_wall') {
                    var lastid = $('.timelineItem:last').attr('data-id');
                    //alert(social_active);
                    //alert(lastid);
                    $.ajax({
                        cache: false,
                        dataType: 'html',
                        url: './instagram_ajax/ig_infinite_load.php?nextid=' + lastid,
                        beforeSend: function() {
                            $('body').show();
                            $('.version').text(NProgress.version);
                            NProgress.start();
                        },
                        complete: function() {
                            setTimeout(function() {
                                NProgress.done();
                                $('.fade').removeClass('out');
                            }, 500);
                        },
                        success: function(html) {
                            if (html != '0' || html == '') {
                                $('.wallEntries').append(html);
                                //                                                              $("#loadorders").append(html);
                                $('#last').remove();
                                $('#postedComments').append('<p id=\'last\'></p>');
                                //                                                           $('div#loadMoreComments').hide();
                            } else {
                                $('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
                            }
                            $(window).data('ajaxready', true);
                        }
                    });
                } else if (ig_active == 'ig_user_wall') {
                    var lastidus = $('.timelineItem:last').attr('data-id');
                    $.ajax({
                        cache: false,
                        dataType: 'html',
                        url: './instagram_ajax/ig_infinite_load_userwall.php?nextid=' + lastidus,
                        beforeSend: function() {
                            $('body').show();
                            $('.version').text(NProgress.version);
                            NProgress.start();
                        },
                        complete: function() {
                            setTimeout(function() {
                                NProgress.done();
                                $('.fade').removeClass('out');
                            }, 500);
                        },
                        success: function(html) {
                            if (html != '0' || html == '') {
                                $('.wallEntries').append(html);
                                //                                                              $("#loadorders").append(html);
                                $('#last').remove();
                                $('#postedComments').append('<p id=\'last\'></p>');
                                //                                                           $('div#loadMoreComments').hide();
                            } else {
                                $('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
                            }
                            $(window).data('ajaxready', true);
                        }
                    });
                } else if (ig_active == 'ig_user_followers') {
                    var lastidf = $('.stream-container:last').attr('data-cursor');
                    var userid = $('.stream-container:last').attr('data-userid');
                    $.ajax({
                        cache: false,
                        dataType: 'html',
                        type: 'POST',
                        url: './instagram_ajax/ig_infinite_load_followers.php?nextid=' + lastidf + '&userid=' + userid,
                        beforeSend: function() {
                            $('body').show();
                            $('.version').text(NProgress.version);
                            NProgress.start();
                        },
                        complete: function() {
                            setTimeout(function() {
                                NProgress.done();
                                $('.fade').removeClass('out');
                            }, 500);
                        },
                        success: function(html) {
                            if (html != '0' || html == '') {
                                $('.wallEntriesIgFollowers').append(html);
                                $('#loadorders').append(html);
                                $('#last').remove();
                                $('#postedComments').append('<p id=\'last\'></p>');
                                $('div#loadMoreComments').hide();
                            } else {
                                $('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
                            }
                            $(window).data('ajaxready', true);
                        }
                    });
                } else if (ig_active == 'ig_user_following') {
                    var lastidfw = $('.stream-container:last').attr('data-cursor');
                    var useridfw = $('.stream-container:last').attr('data-userid');
                    $.ajax({
                        cache: false,
                        dataType: 'html',
                        type: 'POST',
                        url: './instagram_ajax/ig_infinite_load_following.php?nextid=' + lastidfw + '&userid=' + useridfw,
                        beforeSend: function() {
                            $('body').show();
                            $('.version').text(NProgress.version);
                            NProgress.start();
                        },
                        complete: function() {
                            setTimeout(function() {
                                NProgress.done();
                                $('.fade').removeClass('out');
                            }, 500);
                        },
                        success: function(html) {
                            if (html != '0' || html == '') {
                                $('.wallEntriesIgFollowing').append(html);
                                $('#loadorders').append(html);
                                $('#last').remove();
                                $('#postedComments').append('<p id=\'last\'></p>');
                                $('div#loadMoreComments').hide();
                            } else {
                                $('div#loadmoreajaxloader').html('<center><h3><b>Time limit exceed please try after some times.</b></h3></center>');
                            }
                            $(window).data('ajaxready', true);
                        }
                    });
                }
            }
        }
    });
    $('.removePhoto').click(function(e) {
        // $(this).parent('.one-photo').remove();
        if ($('#picture .one-photo').size() > 1) {

            if ($(this).parent('.one-photo').children('.preview').html() != "") {
                // if ($('#picture .one-photo .preview').html() != "") {
                console.log($(this)[0].parent('.one-photo').children('.preview').html());
                // console.log($(this));
                e.preventDefault();
                $(this).parent('.one-photo').remove();
            }

        } else {
            e.preventDefault();
            $('#imageUploads').val('');
            $('#picture').fadeOut(500);
            // $(".one-photo").hide();
        }
    });
    $('.picon').click(function(e) {
            e.preventDefault();
            $('#picture').fadeIn(500);
            $(".one-photo").show();
            //$('textarea.grybord').css('display','none');

            // $('.postbutton').off('click');
            // $('.postbutton').click(function (e)
            // {
            //   // $('#upload_photos').submit();re
            // });
        }
        // , function() {

        //   e.preventDefault();
        //   $('#picture').fadeOut(500);
        //   $(".one-photo").hide();

        // }
    );
    $('.grybord').focus(function() {
        if ($(this).val() === 'Whats in your head?') {
            $(this).val('');
        }
    });
    $('#picture input[type="file"]').change(function() {
        // alert($('#picture input[type="file"]').size()); 
        // alert($(this).attr('data-index')); 
        var dataIndex = $(this).attr('data-index');
        var photoCount = $('#picture input[type="file"]').size();
        // alert(dataIndex + photoCount);
        // if( dataIndex == photoCount )
        attachFileReader(this);
    });
    /*$("#tabs").tabs();*/
    if (typeof $('.wallEntries')[0] !== 'undefined') {
        interval = setInterval(function() {
            getWallAjax();
        }, 17500);
    }
    getWallAjax();
    $('.postbutton').on('click', function(e) {



        if ($.cookie('socal_active') == 'wdd' || $.cookie('socal_active') == 'undefined') {

            if ($('#picture .one-photo').size() >= 1 && $('#picture .one-photo .preview').html() != "") {
                return;
            }

            // if($('.grybord').val() == '')
            // {
            //     alert('please write something');                  
            //     return null;
            // }
            e.preventDefault();
            $(this).after($('<div></div>').addClass('ajax-loader'));
            var ownerId = '';
            if ($('#ownerId').val()) {
                ownerId = $('#ownerId').val();
            }
            $.ajax({
                url: 'wallentry.php',
                type: 'post',
                data: {
                    ajax_add: 'true',
                    text: $('.grybord').val(),
                    link: $('#link').val(),
                    ownerId: ownerId,
                    link_photo: $('#link_image').val(),
                    link_title: $('input[id="link_title"]').val(),
                    link_description: $('input[id="link_description"]').val()
                },
                success: function(msg) {

                    $('.wallEntries').prepend(msg);
                    $('.ajax-loader').remove();
                    $('#link').val('');
                    $('#link_image').val('');
                    $('input[id="link_title"]').val('');
                    $('input[id="link_description"]').val('');
                    $('textarea[name="status"]').val('');
                    $('#link_info').html('');
                    $('#link_info').css('display', 'none');
                    lastParsedUrl = false;
                }
            });

        }
    });
    $('.postbutton-comments').click(function(e) {
        //alert($(this).attr('id'));
        var wall_id = $(this).attr('id');
        var formdata = $('#commform_' + wall_id).serialize();
        $.ajax({
            url: 'wall_comment_post.php',
            type: 'post',
            data: formdata,
            success: function(msg) {
                $('#comment_' + wall_id).prepend(msg);
                $('#comment_' + wall_id).css('display', 'block');
            }
        });
        //wall_comment_post
    });
    /*analise status to getch http or https*/
    $('textarea.grybord').on('keyup',function() {
        /*get url*/
		
        var urls = findUrls($('textarea.grybord').val());
	
        if (urls.length > 0 && lastParsedUrl !== urls[0]) {
            $.ajax({
                url : ajaxUrl,
                type: 'post',
                data: {
                    action: 'getUrlData',
                    url   : urls[0]
                },
                beforeSend: function() {
                    lastParsedUrl = urls[0];
                    $('#link_info').html('');
                    $('#link_info').css('display', 'table').addClass('info_linkbox');
                    $('#link_info').append($('<div></div>').addClass('ajax-loader'));
                },
                dataType: 'json',
                success: function(data) {
					
                    $('#link_info').css('display', 'table').addClass('info_linkbox');
                    $('.ajax-loader').remove();
                    $('#link_info').html('');
                    /*now render the link*/
                    var content_wrapper = $('<div></div>').attr('class', 'content_wrapper').attr('style', 'padding:10px;');
                    var photos = $('<div class="photo-browser" style="width:100%;"></div>');
                    var title = $('<h4 class="scrapedTitle" style="float:left;font-size:20px;font-family: TimesNewRoman,Times New Roman,Times,Baskerville,Georgia,serif;line-height: 20px;text-align:justify;"></p>').html(data.title);
                    var description = $('<p style="float:left;font-size:11pt;font-family: TimesNewRoman,Times New Roman,Times,Baskerville,Georgia,serif;text-align:justify;"></p>').html(data.description);
                    var left = $('<div style="display:none;">&laquo</div>').addClass('left-arrow').click(function(e) {
                        e.preventDefault();
                        swithPhoto(-1);
                    });
                    var right = $('<div style="display:none;">&raquo</div>').addClass('right-arrow').click(function(e) {
                        e.preventDefault();
                        swithPhoto(1);
                    });
                    var terminator = $('<a style="float:left;margin:5px;position:absolute;left:93%;color: #ccc;font-weight: bold;cursor:pointer;"></a>').addClass('terminator').html('X').click(function() {
                        $('#link_info').html('');
                        $('#link_info').css('display', 'none');
                    });
                    //var link_image = $('<input></input>').attr('type', 'hidden').attr('id', 'link_image').val(data.images);
                    // $('#link_info').append(link_image);
                    //var img = $('<img width="150px" height="120px" align="center"></img>').attr('src', data.images);
                    // $('#link_info').append(img);
                    $(photos).append(left);
                    $(photos).append(right);
                    var i;
                    for (i = 0; i < data.images.length; i++) {
                        //alert()
                        var imgWrapper = $('<div></div>').addClass('image-wrapper').attr('rel', i);
                        var img = $('<img style="width:100%;"></img>').attr('src', data.images[i]);
                        if (i > 0) {
                            imgWrapper.addClass('hidden');
                        } else {
                            var link_image = $('<input></input>').attr('type', 'hidden').attr('id', 'link_image').val(data.images[i]);
                            if (data.contenttype != 'video') {
                                $('#link_info').append(link_image);
                            }
                        }
                        $(imgWrapper).append(img);
                        $(photos).append(imgWrapper);
                    }
                    var link_title = $('<input></input>').attr('id', 'link_title').val(data.title).attr('type', 'hidden');
                    var link = $('<input></input>').attr('id', 'link').val(urls[0]).attr('type', 'hidden');
                    var link_description = $('<input></input>').attr('id', 'link_description').val(data.description).attr('type', 'hidden');
                    $('#link_info').append(terminator);
                    $('#link_info').append(photos);
                    $('#link_info').append(content_wrapper);
                    //$(content_wrapper).append(terminator);
                    $(content_wrapper).append(title);
                    $(content_wrapper).append(description);
                    $('#link_info').append(link_title);
                    $('#link_info').append(link);
                    $('#link_info').append(link_description);
                }
            });
        }
    });


    $(window).scroll(function(e) {
        console.log('2nd ');
        var scroll = parseInt($(window).scrollTop());
        var height = parseInt($(window).height());
        var percent = scroll / height;
        if (percent > 0.8 && !semaphore) {
            semaphore = true;
            $('input[name="limit"]').val(parseInt($('input[name="limit"]').val()) + 10);
            $('.lazyLoader').append($('<div></div>').addClass('ajax-loader'));
        }
    });
    $('#back-top').hide();
    // fade in #back-top
    $(function() {
        $(window).scroll(function() {
            console.log('3rd ');
            if ($(this).scrollTop() > 100) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-top a').click(function() {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });
});

function checkNewMessage() {

    var dataArray = new Array();
    $('.crispbxmain').each(function() {
        var dataLayer = $(this).attr("id");
        var commentdata = $('#' + dataLayer + ' div.postcomment:last').attr('id');
        if (commentdata != undefined) {

            var lastcommentid = commentdata.replace("comment_", "");
            var item = [dataLayer, lastcommentid];
            dataArray.push(item);
        } else {
            var item = [dataLayer, 0];
            dataArray.push(item);

        }




    });


    if (dataArray != '') {
        $.ajax({
            url: 'wdd_ajax_load_comment_first.php',
            type: 'post',
            dataType: 'json',

            data: {
                dataArray: dataArray
            },
            success: function(msg) {

                $.each(msg, function(index, val) {
				var item = val;
		
                 html = "<div class='slider_two_title postcomment' id='comment_" + item.comment.id + "'><img src='" + item.userdata.profile_pic + "'style='width:37px;height:37px;'><div class='slider_title_style 1single-comment'><div class='comment_text'><a href='#'><span class='author_slide_top'>" + item.userdata.firstname + " " + item.userdata.lastname + "</span></a><span class='comment_box_bottom_text'>" + item.comment.text + "</span></div><a href='javascript:void(0)'><p class='like_and_reply'><span>Like</span><span class='comment_reply replytext' id='replybox" + item.comment.id + "'>Reply</span> " + item.comment.date + " </p></a></div><div id='allreply_" + item.comment.id + "' class='allReply'> <div class='comment_box_reply comment_box replycomment' style='display: none;' id='reply" + item.comment.id + "'> <img src='" + item.userdata.profile_pic + "' alt='' style='width:37px;height:37px;'><span><input id='replyid" +item.comment.id+ "' rel='" +item.wallid + "' class='comment_box_inline comment_reply' placeholder='Write your comment here'type='text'> </span> <div class='comment_box_icon comment_box_inline'><a href='#'><span><i class='comment_box_left fa fa-camera'></i></span></a> <a href='#'><span><i class='comment_box_left2 fa fa-smile-o'></i></span></a> </div> <h5 class='press_enter_post'>Press enter to post : </h5> </div></div></div>";
			

			$(html).insertBefore('#comment_box_' + item.wallid);
           //  $('#comment_box_' + item.wallid).appendTo(html);
                    
                });

                /* 	  $.each(msg, function(index, el) {
                	
                		alert(index); alert(el);
                	  
                	
                		  } */

            }
        });


    }



}
//  below code for comment section we use in wall #commentbymb
$(function() {


    var refreshId = setInterval("checkNewMessage()", 3000);

    $('.add-comment-link').click(function(e) {
        var id = $(this).attr('rel');
        $('#comment_' + id).toggle();
    });



    $(document.body).on('keyup', '.comment_input', function(e) {


        if (e.which == 13) {
            e.preventDefault();
            var wall_id = $(this).attr('id');


            var comment = $.trim($(this).val());
            var type = 1;

            if (comment != '') {


                $.ajax({
                    url: 'wall_comment_post.php',
                    type: 'post',
                    dataType: 'json',

                    data: {
                        post_id: wall_id,
                        comment: comment,
                        type: type
                    },
                    success: function(msg) {

                        html = "<div class='slider_two_title postcomment' id='comment_" + msg.lastcommentid + "'><img src='" + msg.userdata.profile_pic + "'style='width:37px;height:37px;'><div class='slider_title_style 1single-comment'><div class='comment_text'><a href='#'><span class='author_slide_top'>" + msg.userdata.firstname + " " + msg.userdata.lastname + "</span></a><span class='comment_box_bottom_text'>" + msg.comments.text + "</span></div><a href='javascript:void(0)'><p class='like_and_reply'><span>Like</span><span class='comment_reply replytext' id='replybox" + msg.lastcommentid + "'>Reply</span> " + msg.comments.date + " </p></a></div><div id='allreply_" + msg.lastcommentid + "' class='allReply'> <div class='comment_box_reply comment_box replycomment' style='display: none;' id='reply" + msg.lastcommentid + "'> <img src='" + msg.userdata.profile_pic + "' alt='' style='width:37px;height:37px;'> <span> <input id='replyid" + msg.lastcommentid + "' rel='" + wall_id + "' class='comment_box_inline comment_reply' placeholder='Write your comment here'type='text'> </span> <div class='comment_box_icon comment_box_inline'><a href='#'><span><i class='comment_box_left fa fa-camera'></i></span></a> <a href='#'><span><i class='comment_box_left2 fa fa-smile-o'></i></span></a> </div> <h5 class='press_enter_post'>Press enter to post : </h5> </div></div></div>";

                        $('#comment_box_' + wall_id).appendTo(html);
						
                        $("#" + wall_id).val('');
						
                        wall_id = wall_id.replace('postid_', '');

                        $(html).insertBefore('#comment_box_' + wall_id);
                        
                        
                        //$('#comment_' + wall_id).css('display', 'block');
                    }
                });
            }


        }
    });

    $(document.body).on('click', '.replytext', function() {


        var id = $(this).attr('id');
        id = id.replace('replybox', 'reply');
        $("#" + id).show();


    });
    $(document.body).on('keyup', '.comment_reply', function(e) {

        if (e.which == 13) {
            e.preventDefault();
            var id = $(this).attr('id');
            var postid = $(this).attr('rel');
            var comment = $(this).val();
            var type = 'reply';
            wallid = id.replace('replyid', '');
            if (comment != '') {


                $.ajax({
                    url: 'wall_comment_post.php',
                    type: 'post',
                    dataType: 'json',

                    data: {
                        reply: id,
                        comment: comment,
                        type: type,
                        postid: postid
                    },
                    success: function(msg) {

                        $("#replyid" + wallid).val('');
                        html = "<div class='slider_two_title postcomment' id='comment_'><img src='" + msg.userdata.profile_pic + "'style='width:37px;height:37px;'><div class='slider_title_style 1single-comment'><div class='comment_text'><a href='#'><span class='author_slide_top'>" + msg.userdata.firstname + " " + msg.userdata.lastname + "</span></a><span class='comment_box_bottom_text'>" + msg.comments.text + "</span></div><a href='javascript:void(0)'><p class='like_and_reply'>&nbsp; " + msg.comments.date + " </p></a></div></div>";
                        $('#reply' + wallid).before(html);

                    }
                });
            }

        }
    });

});