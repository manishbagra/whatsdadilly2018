var clientId = '376645511287-6qhcl885b0rlvprja6gik7mvs8jpvf1h.apps.googleusercontent.com';

$(document).on("click",".google", function(){
    $('.InviteFriendsInputs').hide();
	$('.progress-striped.loader').show();
	setTimeout(function(){
		$('.InviteFriendsInputs').show();
		$('.progress-striped.loader').hide();
	}, 15000);
	var config = {
       'client_id': clientId,
       'scope': 'https://www.google.com/m8/feeds',
	   'immediate': false,
	   'approval_prompt':'force'
     };
	 
     gapi.auth.authorize(config, function() {
       fetch(gapi.auth.getToken());  
	   var auth2 = gapi.auth.signOut();
   
	   //gapi.auth.setToken(null);
	   //gapi.auth.signOut();
     });
	 
	 function fetch(token) {
		 $.ajax({
			 url: "https://www.google.com/m8/feeds/contacts/default/full?access_token=" + token.access_token + "&alt=json&max-results=700&v=3.0",
			 dataType: "jsonp",
			 success:function(data) {
					// display all your data in console
					//console.log(JSON.stringify(data));
					 var email= [];
					 
					if(data.feed.entry.length > 0){
						
						for ( var i= 0 ; i < data.feed.entry.length ; i++)
						{
							if(data.feed.entry[i].gd$phoneNumber)
							{
								continue; 
							}
							if(data.feed.entry[i].gd$email){
							   email.push({
									name: data.feed.entry[i].title.$t,
									email: data.feed.entry[i].gd$email[0].address
								});
								/*
								username = (data.feed.entry[i].title.$t && data.feed.entry[i].title.$t!=='') ? data.feed.entry[i].title.$t : '---';
								html += "<div class='profile_box1'><input type='checkbox' /> <span>"+username+"</span><a href='#'>"+data.feed.entry[i].gd$email[0].address+"</a></div>";*/
							}
							else
							{ 
								continue;
							}
						}
						//console.log(email);
						getRegisteredUsers(email,'gmail');
					}
				}
		});
	} 
});




WL.init({
		client_id: '0000000044184D2B',
		redirect_uri: 'http://whatsdadilly.com/betaPhaseProjectWDD/invite.php',
		scope: ["wl.basic", "wl.contacts_emails"],
		response_type: "token"
	});
	
	jQuery( document ).ready(function() {

	//live.com api
	jQuery('#import').click(function(e) {
	    e.preventDefault();
	    $('.InviteFriendsInputs').hide();
		$('.progress-striped.loader').show();
		setTimeout(function(){
		$('.InviteFriendsInputs').show();
		$('.progress-striped.loader').hide();
	}, 15000);
		WL.login({
	        scope: ["wl.basic", "wl.contacts_emails"]
	    }).then(function (response) 
	    {
			WL.api({
	            path: "me/contacts",
	            method: "GET"
	        }).then(
	            function (response) {
					var email= [];
					 html = "";
					if(response.data.length > 0){
						for ( var i= 0 ; i < response.data.length ; i++)
						{
							if(response.data[i].emails && response.data[i].emails.preferred != null ){
							   email.push({
									name: response.data[i].name,
									email: response.data[i].emails.preferred
								});
								/*
								username = (response.data[i].name && response.data[i].name !=='null') ? response.data[i].name : '---';
								html += "<div class='profile_box1'><input type='checkbox' /> <span>"+username+"</span><a href='#'>"+response.data[i].emails.preferred+"</a></div>";*/
							}
						}
						 	
						getRegisteredUsers(email,'hotmail');
						
					}	
                        //your response data with contacts 
	            	//console.log(JSON.stringify(response.data));
					
	            },
	            function (responseFailed) {
	            	console.log(responseFailed);
					
	            }
	        );
	        
	    },
	    function (responseFailed) 
	    {
	        console.log("Error signing in: " + responseFailed.error_description);
	    });
	});    
   
   
});

function getRegisteredUsers(email,source){
	$('.InviteFriendsInputs').hide();
	$('.progress-striped.loader').show();
	if(source=='yahoo'){
		$('.invite-source').html('<img src="rgimages/yahoo-login-icon.png"> Yahoo Contacts <a href="#step1" data-toggle="tab" aria-expanded="true">Try another service</a>');
	}else if(source=='hotmail'){
		$('.invite-source').html('<img src="rgimages/outlook-login-icon.png"> Hotmail Contacts <a href="#step1" data-toggle="tab" aria-expanded="true">Try another service</a>');
	}else{
		$('.invite-source').html('<img src="images/email.png"> Gmail Contacts <a href="#step1" data-toggle="tab" aria-expanded="true">Try another service</a>');
	}
	$.ajax({
		url: "getfriends.php",
		method: "post",
		data: {people:JSON.stringify(email)},
		type: "post",
		dataType:"json",
		success: function(data){
			if(data.requests!=''){
				$('.secrol').html(data.requests);
				$('.requestcount').html(data.requestsCount);
				requestCount = data.requestsCount;
			}
			if(data.inviations!=''){
				$('.contact-list').html(data.inviations);
				$('.invitationcount').html(data.inviationsCount);
				invitationCount = data.inviationsCount;
			}
			$('.Step2Process a.toggle-steps').click();
			$('.InviteFriendsInputs').show();
			$('.progress-striped.loader').hide();
			$('#sendFriendRequestfrm input[name="invtiations[]"]').on('click',function(){
				if($(this).is(':checked')){
					$('.requestcount').html( parseInt($('.requestcount').html())+1);
				}else{
					$('.requestcount').html( parseInt($('.requestcount').html())-1);
				}
			});
			
			$('#sendinvitation input[name="invtiations[]"]').on('click',function(){
				if($(this).is(':checked')){
					$('.invitationcount').html( parseInt($('.invitationcount').html())+1);
				}else{
					$('.invitationcount').html( parseInt($('.invitationcount').html())-1);
				}
			});
		},
		error: function(){
			$('.InviteFriendsInputs').show();
			$('.progress-striped.loader').hide();
			//popup.hideLoader();
			//popup.alert('error', 'There are some errors');
		}
	});

}


function SendInvitation(){
	$.ajax({
		url: "sendinvitation.php",
		method: "post",
		data: $( '#sendinvitation' ).serialize(),
		type: "post",
		success: function(data){
			alert('Invitations has been sent successfully');
			document.location.href="signup2.php";
		},
		error: function(){
			//popup.hideLoader();
			//popup.alert('error', 'There are some errors');
		}
	});
}


function sendFrientRequest(){
	$.ajax({
		url: "sendfriendrequest.php",
		method: "post",
		data: $( '#sendFriendRequestfrm' ).serialize(),
		type: "post",
		success: function(data){
			$('.Step3Process a.toggle-steps').click();
		},
		error: function(){
			//popup.hideLoader();
			//popup.alert('error', 'There are some errors');
		}
	});
}

/*
var YAHOO_CLIENT_ID = {
	'whatsdadilly.com' : 'dj0yJmk9Z1hOVVc4ZGxtNUFpJmQ9WVdrOWVUUmhabkpPTXpRbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD03NQ--'
}[window.location.hostname];
*/
var YAHOO_CLIENT_ID = 'dj0yJmk9WWx0dTBUQ1lqNFBsJmQ9WVdrOVNYSm5TVlpoTjJVbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD0wMw--';

var CLIENT_IDS_ALL = {
	yahoo : YAHOO_CLIENT_ID
};

var OAUTH_PROXY_URL = 'https://auth-server.herokuapp.com/proxy';
hello.init( {
		yahoo : CLIENT_IDS_ALL.yahoo
	}
	, {
		redirect_uri:'http://whatsdadilly.com/betaPhaseProjectWDD/invite.php',
		oauth_proxy : OAUTH_PROXY_URL,
		scope:"contacts"
	}
);


function getFriends(network, path){
	$('.InviteFriendsInputs').hide();
	$('.progress-striped.loader').show();
	setTimeout(function(){
		$('.InviteFriendsInputs').show();
		$('.progress-striped.loader').hide();
	}, 15000);
	hello(network).login({scope:'contacts'}).then(function(auth) {
		// Get the friends
		// using path, me/friends or me/contacts
		hello(network).api('me/following', {count:'max',start:'0'}).then(function responseHandler(r) {
			var email= [];
			for(var i=0;i<r.data.length;i++){
				var o = r.data[i];
				if(o.email){
					email.push({
						name: (o.name) ? o.name : '---',
						email: o.email
					});
				}
			};
			getRegisteredUsers(email,'yahoo');
		});
	}, function() {
		if(!auth||auth.error){
			console.log("Signin aborted");
			return;
		}
	});
}

