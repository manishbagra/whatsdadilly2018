//
// Yahoo
//
// Register Yahoo developer
(function (hello) {

    hello.init({
        'yahoo': {
            // Ensure that you define an oauth_proxy
            oauth: {
                version: 2,
                auth: "https://api.login.yahoo.com/oauth2/request_auth",
                grant: 'https://api.login.yahoo.com/oauth2/get_token'
            },

            // Login handler
            login: function (p) {
                // Change the default popup window to be atleast 560
                // Yahoo does dynamically change it on the fly for the signin screen (only, what if your already signed in)
                p.options.window_width = 560;


                // Yahoo throws an parameter error if for whatever reason the state.scope contains a comma, so lets remove scope
                try { delete p.qs.scope; delete p.qs.state.scope; } catch (e) { }
            },
            /*
            // AUTO REFRESH FIX: Bug in Yahoo can't get this to work with node-oauth-shim
            login : function(o){
                // Is the user already logged in
                var auth = hello('yahoo').getAuthResponse();

                // Is this a refresh token?
                if(o.options.display==='none'&&auth&&auth.access_token&&auth.refresh_token){
                    // Add the old token and the refresh token, including path to the query
                    // See http://developer.yahoo.com/oauth/guide/oauth-refreshaccesstoken.html
                    o.qs.access_token = auth.access_token;
                    o.qs.refresh_token = auth.refresh_token;
                    o.qs.token_url = 'https://api.login.yahoo.com/oauth/v2/get_token';
                }
            },
            */

            base: "https://social.yahooapis.com/v1/",

            get: {
                "me": yql('select * from social.profile where guid=me'),
                "me/friends": yql('SELECT * FROM social.contacts(0) WHERE guid=me'),
                "me/following": yql('select * from social.contacts where guid=me')
            },
            wrap: {
                me: formatUser,
                // Can't get ID's
                // It might be better to loop through the social.relationshipd table with has unique ID's of users.
                "me/friends": formatFriends,
                "me/following": formatFriends,
                "default": paging
            },

            xhr: function (p) {
                //alert(dump(p.options.access_token));
                /*if( p.method !== 'get' && p.method !== 'delete' && !hello.utils.hasBinary(p.data) ){

                    // Does this have a data-uri to upload as a file?
                    if( typeof( p.data.file ) === 'string' ){
                        p.data.file = hello.utils.toBlob(p.data.file);
                    }else{
                        p.data = JSON.stringify(p.data);
                        p.headers = {
                            'Content-Type' : 'application/json'
                        };
                    }
                }*/
                p.options.curl = '';
                if (typeof (p.options.access_token) != 'undefined') {
                    p.headers = {
                        'Authorization': 'Bearer ' + p.options.access_token
                    };
                }
                return true;
            }
        }
    });

    function formatError(o) {
        if (o && 'meta' in o && 'error_type' in o.meta) {
            o.error = {
                code: o.meta.error_type,
                message: o.meta.error_message
            };
        }
    }

    function formatUser(o) {

        formatError(o);
        if (o.query && o.query.results && o.query.results.profile) {
            o = o.query.results.profile;
            o.id = o.guid;
            o.last_name = o.familyName;
            o.first_name = o.givenName || o.nickname;
            var a = [];
            if (o.first_name) {
                a.push(o.first_name);
            }

            if (o.last_name) {
                a.push(o.last_name);
            }

            o.name = a.join(' ');
            o.email = (o.emails && o.emails[0]) ? o.emails[0].handle : null;
            o.thumbnail = o.image ? o.image.imageUrl : null;
        }

        return o;
    }

    function formatFriends(o, headers, request) {
        formatError(o);
        paging(o, headers, request);
        var contact;
        var field;
        if (o.query && o.query.results && o.query.results.contact) {
            o.data = o.query.results.contact;
            delete o.query;

            if (!Array.isArray(o.data)) {
                o.data = [o.data];
            }

            o.data.forEach(formatFriend);
        }

        return o;
    }

    function formatFriend(contact) {
        contact.id = null;

        // #362: Reports of responses returning a single item, rather than an Array of items.
        // Format the contact.fields to be an array.
        if (contact.fields && !(contact.fields instanceof Array)) {
            contact.fields = [contact.fields];
        }

        (contact.fields || []).forEach(function (field) {
            if (field.type === 'email') {
                contact.email = field.value;
            }

            if (field.type === 'name') {
                contact.first_name = field.value.givenName;
                contact.last_name = field.value.familyName;
                contact.name = field.value.givenName + ' ' + field.value.familyName;
            }

            if (field.type === 'yahooid') {
                contact.id = field.value;
            }
        });
    }

    function paging(res, headers, request) {

        // See: http://developer.yahoo.com/yql/guide/paging.html#local_limits
        if (res.query && res.query.count && request.options) {
            res.paging = {
                next: '?start=' + (res.query.count + (+request.options.start || 1))
            };
        }

        return res;
    }

    function yql(q) {
       return 'http://whatsdadilly.com/betaPhaseProjectWDD/curl.php';
	   //return 'https://social.yahooapis.com/v1/user/me/contacts?format=json';
	   // return 'https://query.yahooapis.com/v1/yql?q=' + (q + ' limit @{limit|100} offset @{start|0}').replace(/\s/g, '%20') + '&format=json';
    }

})(hello);