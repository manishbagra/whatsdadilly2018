
var Rs_video = new function() {
	this.init = function(){
		this.enableVideoJsEachMainVideo();
		this.createCanvas();
		this.jsEventSet();

	}

	this.options = {
		'canvasId':"rs_video_canvas",
		'expand_poster':"images/video-placeholder.jpg",
		'videoSingleItems':$('.rs_video_player_load'),
		'videoIdPrefix':'rs_cvsv_',
		videoWidth:300,
		videoHeight:250
	};
	
	
	this.videoObjsets = {};
	this.canvasId = this.options.canvasId;
	this.canvasVidePre = "rs_video_canvas";
	this.videoSingleItems = this.options.videoSingleItems;
	this.video=[];
	this.mainVideoObjects = {};
	this.allMainPlayer = [];

	this.addVideo = function(videoId){
		var index = this.length;
		this.video[index] = videoId;
	};
	this.addExpandButton = function(data) {
		var $this = this;

 		var videoPlayer = data.player;
		var Button = videojs.getComponent('Button');
		var ExpandButton = videojs.extend(Button, {
		  constructor: function() {
		    Button.apply(this, arguments);
		    this.addClass('fa');
		    this.addClass('pip-btn');
		    this.addClass('fa-1');
		    this.addClass('line-height');
		  },

		  handleClick: function() {
		    videoPlayer.pause();
		    videoPlayer.poster($this.options.expand_poster);
		    $this.createPlayer(videoPlayer);
		    videoPlayer.controls(false);
		  }
		});

		videojs.registerComponent('ExpandButton', ExpandButton);
		videoPlayer.getChild('controlBar').addChild('ExpandButton', {}, 11);

	}
	this.addBackButton = function(data) {
		var $this = this;

 		var videoPlayer = data.player;
		var Button = videojs.getComponent('Button');
		var BackButton = videojs.extend(Button, {
		  constructor: function() {
		    Button.apply(this, arguments);
		    this.addClass('fa');
		    this.addClass('fa-minus-square-o');
		    this.addClass('fa-1');
		    this.addClass('line-height');
		  },

		  handleClick: function() {
		    $this.removePlayer(videoPlayer);
		  }
		});

		videojs.registerComponent('BackButton', BackButton);
		videoPlayer.getChild('controlBar').addChild('BackButton', {}, 11);

	}

	this.enableVideoJsEachMainVideo = function(){
		var that = this;
		this.options.videoSingleItems.each(function(index, el) {
			that.allMainPlayer[index] = videojs($(this).attr("id"));
			that.allMainPlayer[index].addClass('rs_video_parent');
			that.addExpandButton({
			    player: that.allMainPlayer[index],
			    icon: ""
			});
		});
		
	}

	this.createPlayer = function(videoPlayer){
		
		var videoId = videoPlayer.id();
		this.mainVideoObjects[videoId] = videoPlayer;
		/*if(this.video.indexOf(videoId)>=0){
			this.toggleState(videoId);
			return;
		}*/
		var $this =this;
		this.pauseAllMainVideo();
		var totalPlayer = this.video.length;
		this.video[totalPlayer] = videoId;
		var videoPlayerId = this.options.videoIdPrefix+videoId;
		var videoUrl = $("#"+videoId).attr('data-video');
		var player = 	'<div data-video-id="'+videoId+'" class="rs_single_canvas_item"><video id="'+videoPlayerId+'" class="video-js vjs-default-skin" controls preload="none" width="640" height="264"'
				     +'poster="video/cars.png"'
				     +'data-setup="{}">'
				    		+'<source src="'+videoUrl+'" type="video/mp4" />'
					+'</video></div>';
		$('#'+this.canvasId).prepend(player);
		$( ".rs_single_canvas_item" ).draggable({handle:"video"});
		$( ".rs_single_canvas_item" ).resizable({
			handles: 'n, e, s, w',
			aspectRatio:true,
			resize:function(event,ui){
				var videoPlayer = $this.videoObjsets[ui.originalElement.attr('data-video-id')];
				videoPlayer.dimensions(ui.size.width,ui.size.height);
				//console.log(ui.size.width);
			}
		});


		this.videoObjsets[videoId] = videojs(
							videoPlayerId, 
							{
								"controls": true, 
								"autoplay": false,
								"width": this.options.videoWidth,
								"height": this.options.videoHeight,
								"preload": "auto", 
								"controlBar": {
								"FullscreenToggle": false,
								}
							}
					);

		// add back back button
		this.addBackButton({"player": this.videoObjsets[videoId]});

		this.pauseAllVideo(videoId);
		this.playVideoById(videoId);
	};
	this.toggleState = function(videoId){
		if(this.videoObjsets[videoId].paused()){
			this.playVideoById(videoId);
		}else{
			this.pauseVideoById(videoId);
		}
	}
	/*Play video*/
	this.playVideoById = function(videoId){
		$("#"+videoId).addClass("rs_video_in_canvas");
		this.videoObjsets[videoId].play();
	}
	this.pauseVideoById = function(videoId){
		if(this.videoObjsets[videoId] != null){
			try{
				this.videoObjsets[videoId].pause();
			}catch(e){
				
			}
		}
	}
	this.pauseAllVideo = function(){
		for(var  i=0; i<this.video.length; i++){
			this.pauseVideoById(this.video[i]);
		}
	}
	this.removePlayer = function(canvasVideo){
		this.pauseAllMainVideo();
		this.pauseAllVideo();
		var mainVideoId = canvasVideo.id().substr(8);
		var mainVideoPlayer = this.mainVideoObjects[mainVideoId];
		mainVideoPlayer.play();
		mainVideoPlayer.currentTime(canvasVideo.currentTime());
		mainVideoPlayer.poster($("#"+mainVideoId).attr("poster"));
		mainVideoPlayer.controls(true);
		$('#'+mainVideoId).removeClass('rs_video_in_canvas');
		$("#"+canvasVideo.id()).closest('.rs_single_canvas_item').remove();
		canvasVideo.dispose();
	}

	this.createCanvas = function(){
		$('body').append('<div id="'+this.canvasId+'"></div>');
	}



	this.jsEventSet = function(){
		var $this = this;
		$(this.videoSingleItems).on('click', function(event) {
			event.preventDefault();
			
		});
	}

	this.pauseAllMainVideo = function(){
		var that = this;
		this.options.videoSingleItems.each(function(index, el) {
			that.allMainPlayer[index].pause();
		});
	}
}
Rs_video.init();