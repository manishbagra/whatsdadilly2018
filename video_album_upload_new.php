 <?php
error_reporting(-1);

require 'getid3/getid3.php';
require 'bootstrap.php';
require_once 'entities/Album.php';
require_once 'model/Albums.php';
require_once 'model/Wall.php';
require_once 'classes/Albums.class.php';
require_once 'classes/Session.class.php';

$WallModel = new WallModel();

$output_dir = __DIR__ . "/uploads/video/";

$output_dir_con = __DIR__ . "/uploads/video/videomp4/";

$allowed = array(
    "webm",
    "wma",
    "wmv",
    "mov",
    "mp4",
    "ogv",
    "ogg",
    "flv",
    "mkv",
    "3gp"
);

$getID3 = new getID3;

$name      = $_FILES["file"]["name"];
$temp_name = $_FILES["file"]["tmp_name"];

$file = $getID3->analyze($temp_name);


// $width  = $file['video']['resolution_x'];

// $height = $file['video']['resolution_y'];

// $resolution = $width."X".$height;


$ext = pathinfo($name, PATHINFO_EXTENSION);

if (!in_array($ext, $allowed)) {
    
    $json_array = array(
        "status" => false
    );
    
} else {
    
    $uniqid = uniqid(rand()) . '.' . $ext;
    
    if ($ext == 'mp4' || $ext == 'ogg') {
        
        if (move_uploaded_file($temp_name, $output_dir_con . $uniqid)) {
            
            $file_thumb = $WallModel->create_movie_thumb($output_dir_con . $uniqid, $uniqid, $output_dir_con);
            
            for ($i = 0; $i < count($file_thumb); $i++) {
                
                $newthumb = explode('.', strrev($file_thumb[$i]));
                
                $newthu = explode('/', $newthumb[1]);
                
                $Thumbs[$i] = strrev($newthu[0]) . '.' . strrev($newthumb[0]);
                
            }
            
            $imageName = $uniqid;
            
            $imageTmpName = str_replace($ext, "", $_FILES["file"]["name"]);
            
        }
        
    } else {
        
        if (move_uploaded_file($temp_name, $output_dir . $uniqid)) {
            
            $result = $WallModel->mp4toflv($output_dir . $uniqid, $uniqid, $output_dir);
            
            if (!empty($result)) {
                
                $uniId = explode(".", $uniqid);
                
                $uniqid = $uniId[0] . ".mp4";
                
                $file_thumb = $WallModel->create_movie_thumb($output_dir_con . $uniqid, $uniqid, $output_dir_con);
                
                for ($i = 0; $i < count($file_thumb); $i++) {
                    
                    $newthumb = explode('.', strrev($file_thumb[$i]));
                    
                    $newthu = explode('/', $newthumb[1]);
                    
                    $Thumbs[$i] = strrev($newthu[0]) . '.' . strrev($newthumb[0]);
                    
                }
                
                $imageName = $uniqid;
                
                $imageTmpName = str_replace($ext, "", $_FILES["file"]["name"]);
                
            }
            
        }
        
    }
    
    $json_array = array(
        'status' => true,
        'file_thumb' => $Thumbs,
        'name' => $uniqid,
        'tmp_name' => $imageTmpName
    );
    
}

//header("Content-type:application/json");

echo json_encode($json_array);
exit(); 