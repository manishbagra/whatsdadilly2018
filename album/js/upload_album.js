
function chooseThumb(id,vid_id){
	
	$("#"+id).prop('checked',true);
	
	$("#"+vid_id).attr('src',$("#"+id).val());
	
}

jQuery(document).ready(function() {
	
	var heightSaved = 0;
	
	 jQuery(".video-js").hover(function(){
		 
			var Height = jQuery(this).height();
			
			jQuery(this).css("height",Height);
			 
			var vidSrc = $(this).attr("id");
		
			var src    = $(this).attr("src");
			
			if(src == ""){
				jQuery(this).attr("src", vidSrc);
				jQuery("source" , this).attr("src" ,vidSrc);
			}
			
			jQuery(this).get(0).play(); 
		
	},function(){
		
		jQuery(this).get(0).pause();
		
	});
	
	var maxHeight = Math.max.apply(null, jQuery(".img-photos").map(function ()
	{
		return jQuery(this).height();
		
	}).get());
	
	
	
	
	
	jQuery(".img-photos").each(function(i, e) {
		
		
		
		var Height = jQuery(this).height();
		
		
		
		if(maxHeight > Height){
			
			var id = jQuery(this).attr("id");
			
			jQuery("#"+id).css({'height' : maxHeight});
			
		}
	    
    });
	
	
	jQuery("#openBox").colorbox({rel: 'openBox', 

		iframe: false,
		escKey: false,
		overlayClose: false,
		width: "100%",
		height: "85%",
		onClosed: function() {
			clearAlbum();
		}

	});
	
	jQuery(document).on('change', '#file-upload-button', function(event){
                  
		jQuery(document).find('a#openBox').trigger('click');
		
		//return false;
                    
		var filesCount = jQuery(document).find("form#img_up :input");
		 
		var place = jQuery(document).find(".right");
		
		var total_selected_files = filesCount[0].files.length;
		
		var total_files_content = filesCount[0].files;
		
		myHTML='';
		
		var countVar = 0;
					
	    for(var k=0;k<total_selected_files;k++){
			
				myHTML +='<div class="col-sm-4 uplod remove-img" id="rem_'+countVar+'" data-attr=""><i class="fa fa-trash remove-image" id="rem_'+countVar+'" data-attr="" aria-hidden="true"></i><form class="frmMyImage" enctype="multipart/form-data">';
				myHTML +='<div class="uplode"></div><div class="image-preview" id="img_'+countVar+'"><img id="img'+countVar+'" src="images/emty_load.gif"></div><input id="controlss" name="pic_desc[]" class="pic_desc" placeholder="Say something about this photo" type="text"><div class="bar" id="bar2_'+countVar+'"><div class="bar1" id="bar1_'+countVar+'"></div></div></div>';
				myHTML += '</form></div>';
				countVar++;
			
		}			
					
                    setTimeout(function(){
						
						//myHTML +=	'<div class="col-sm-2 upload uplodMan"><div class="image-preview" style="max-height:400px !important;min-height:400px !important;width:90%;" onClick="abc();"><img id="uploadMore" src="images/addnew.png"/></div></div>';
						
						
						
						var place = jQuery(document).find(".right");
					 
						place.html(myHTML);
						
						//return false;
						
						var file_data = jQuery('input[type="file"]')[0].files;
						
						 
						
					    var countBar = 0; 
						
                        var completedCount = 0;
						
						var divHeight = jQuery("#cboxLoadedContent").height();
	
						if(divHeight != ''){
							
							var heightRight = divHeight - 86 ;
							
							//var heightLeft  =  divHeight - 60 ;
							
							$(".right").css('height', heightRight+"px");
							
						}
						
						if(divHeight > 600){
							
							jQuery(".left_side_br").css({"height": ""});
							
						}else{
							
							var heightLeft  =  divHeight - 60 ;
							
							jQuery(".left_side_br").css({"height": heightLeft+"px" , "overflow-y": "scroll"});
						}
						
						 jQuery.each(file_data, function(i, file) {
							 
							 
							 
							var fd = new FormData();
							
						    fd.append("file", file);
							
							var bar1 = jQuery("#colorbox").find("#bar1_"+countBar);
							
                            bar1.css({"width":"1%"});
							
							jQuery(".complete_bar1").css({"width":"1%"});
							
							
							 jQuery.ajax({
                                url : "album_upload_new.php",
                                type: "POST",
                                data : fd,
                                contentType: false,
                                cache: false,
                                processData:false,
								
                                xhr: function(){
                                    
                                    var xhr = jQuery.ajaxSettings.xhr(); 
                                    
                                    if (xhr.upload) {
										
                                        xhr.upload.addEventListener('progress', function(event) {
                                            
											var percent = 0;
											
                                            var position = event.loaded || event.position;
                                            
                                            var total = event.total;
											
                                            if (event.lengthComputable) {
												
                                                percent = Math.ceil(position / total * 100);
                                            }
											
											
											jQuery(".complete_bar1").css({"width":percent+"%"});
                                           
                                            bar1.css({"width":percent+"%"});
                                           
                                            
                                        }, false);

                                        xhr.upload.addEventListener("loadend", function(e){
                                            console.log(e);
                                            var img = jQuery("img", "#img_"+completedCount);
                                            img.css({"filter":"blur(0px)","-webkit-filter":"blur(0px)","-moz-filter":"blur(0px)"});
                                            jQuery("#bar2_"+completedCount).hide();
											jQuery(".complete_bar").hide();
                                            completedCount++;
                                        }, false);
                                    }
                                    countBar++;
                                    return xhr;
                                },
                                mimeType:"multipart/form-data"
								
                            }).done(function(res){
								
								var respo = JSON.parse(res);
								
								jQuery("#img"+i).attr("src","uploads/temp/thumb/"+respo.image);
								
								jQuery("#rem_"+i).attr("data-attr",respo.image);
								
                            });
							
							 
							 
						 });
                      
                       
                    }, 1000);
	});
	
	
	jQuery(document).on('click', '.date1' ,function(){
				
		$(document).find('.title').val($('#title_text').val());
				
		jQuery(".title").val(jQuery("#title_text").val());
				
		jQuery(".description").val(jQuery("#album_desc").val());
				
		$(".album_location").val($("#searchInput").val());
				
		var s1 = [];
				
		$(".pic_desc").each(function(){
			var row = [];
			row.push($(this).val());
			s1.push(row);
		});
				
		var Image64 = new Array();
				
		$('.right .uplod').each(function(){
			
			Image64.push($(this).attr('data-attr'));
			
		});
				
		$("#image64").val(Image64.join("@DadillY@"));
				
		$('#picDesc').val(s1.join("@DadillYWhats@"));
				
		if($.trim(jQuery(".title").val()) != ''){
			
			jQuery(".formSubmit").trigger('click');
			
		}else{
			
			jQuery(document).find("#title_text").css("border", "1px solid red");
			
			return false;
		}
                    
	});
	
	$("form#img_up").on("submit",function(event) {
		
		event.preventDefault();
		var form_data = $(this).serialize(); 
		$.ajax({
			url : "upload_albums.php",
			type: "POST",
			data : form_data,
			dataType : 'json',
			success: function(result){
				
				var html="";
				
				if(result.flag == 1) {
					
					setTimeout( function () {
						
						refreshAlbum(result.album_id,result.total_photos,result.photos_array);
						
						 setTimeout( function () {

						html += "<a href='album_detail.php?id="+result.album.id_album+"'>";

							html += "<div class='col-lg-2 col-md-6 single-album'>";
							
								html += "<div class='img-container'>";
											html += "<div class='thumb'><img class'img-fluid' src='"+result.cover+"' alt='' ></div>";
											html += "<a class='edit-btn' href='#'><i class='far fa-trash-alt'></i></a>";
								html += "</div>";
								
									html += "<a href='upload.html'><h6 class='album-title'>"+result.album.title+"</h6></a>";
								html += "<p>"+result.countPhoto+" Photos</p>";
							html += "</div>";
							
						html += "</a>";	
		
					$("#album_cover").prepend(html);
					 
							$(".alert-success").show().fadeOut(10000);
							
							$.fn.colorbox.close();
							
						 },1000);  
						 
					},1000);  
				}
				
			} 
		});

	});
	
	
	$("#video-upload-button").on('change',function(){

			$(".video_upload").removeClass('hide');
			
			$("body").addClass("modal-open");
			
			var filesCount = jQuery(document).find("form#vid_up :input");
			
            var total_selected_files = filesCount[0].files.length;
			
            var total_files_content = filesCount[0].files;
			
			var myHTML = '';
			
			var countVar = 0;
			
for(var k=0; k<total_selected_files; k++){
			
				
myHTML += '<div class="main-area row"><div class="col-lg-3 col-md-3 col-sm-12 tab-wrap  left-wrap"><img class="img-fluid" id="vid_'+k+'" src="images/emty_load.gif" alt=""><h4 class="status">Upload Status</h4>';

myHTML += '<p class="status_sub">Processing your video</p><div class="info-sec"><div class="d-flex flex-row"><i class="fa fa-info-circle"></i><div><h4>This Video is currently processing</h4><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed</p></div>';

myHTML += '</div><div class="d-flex flex-row"><input id="checkBox" type="checkbox" checked><p>Notify me when my video done processing</p></div></div></div>';
	
myHTML += '<div class="col-lg-9 col-md-9 col-sm-12 right-wrap">';

myHTML += '<input  name="img_name" class="vName" id="img_name_'+k+'"  type="hidden" /> <input  class="vTname" id="temp_name_'+k+'"  name="temp_name" type="hidden" />';

myHTML += "<div class='form-group'><div class='progress'><div class='progress-bar' id='bar1_"+k+"' role='progressbar' aria-valuenow='1' aria-valuemin='0' aria-valuemax='100' style='width:1%'>1%</div></div><p>Your video is still Uploading. Please keep this page opne until it's done</p></div>";				

myHTML += '<div class="form-group"><label for="full_name_id" class="control-label">Video Title<span class="star">*</span></label><input type="text" class="form-control vTitle"  name="vtitle_'+k+'" id="vtitle_'+k+'" placeholder="Name your playlist"></div>';
	
myHTML += '<div class="form-group"><label for="street1_id" class="control-label">Say something about this playlist <span class="star">*</span></label><input type="text" class="form-control vDesc" id="descrp_'+k+'" name="descrp_'+k+'" placeholder="Say something"></div>';					
            
myHTML += '<div class="form-group"><label class="control-label " for="message">Descrioption<span class="star">*</span></label><textarea class="form-control vDescrip message" cols="20" id="message_'+k+'" placeholder="Describe your playlist" name="message_'+k+'" id="" rows="5"></textarea></div>';            

myHTML += '<div class="form-group"><label class="control-label " for="message">Additional Tags<span class="star">*</span></label><textarea class="vTags tags" id="demo_'+k+'" name="demo_'+k+'"></textarea></div>';

myHTML += '<div class="form-group"><label for="state_id" class="control-label">Video Category <span class="star">*</span></label><select class="vCats" id="vid_cat_'+k+'" name="vid_cat_'+k+'"><option value="1">Beauty & Fashion</option><option value="2">Alaska</option><option value="3">Arizona</option><option value="4">Arkansas</option></select></div>';
           
myHTML += '<div class="form-group video-thumb"><label for="state_id" class="control-label">Video Thumbnails <span class="star">*</span></label><p>Thumbnail selection will appear when the video has finished processing</p><div class="video-thumbs row justify-content-center" id="vid-thumb-'+k+'" ></div></div></div></div>';

myHTML += '<div class="col-lg-12 col-md-12 col-sm-12 footer-part"><div class="row align-items-center"><div class="col-lg-3 no-padding"><a href="another.html" class="add-another btn btn-primary btn-gray">Add Another Video</a></div><div class="col-lg-9 no-padding publish-section"><div class="form-group row align-items-center footer-group"><div class="col-lg-2 bal1"><span>Required fields</span><span class="star">*</span></div><div class="col-lg-2 bal2" ><span>Set Video as<span class="star">*</span></span>';

myHTML += '</div><div class="col-lg-3 bal3"><select class="vType" id="type_'+k+'"><option value="0">Public</option><option value="1">Private</option></select></div><div class="col-lg-5 bbtn" style="padding-left:0px;"><button type="submit" class="btn btn-primary btn-black">Publish</button><button type="submit" class="btn btn-primary btn-gray" style="margin-right: 10px;">Cancel</button></div></div></div></div></div>';


						
}
					
					
					setTimeout(function(){
						
                    var place = jQuery(document).find("#video-content");
					
					place.html(myHTML);

					place.append("<script src='upload-video/js/jquery.nice-select.min.js'></script><script>$('select').niceSelect();</script><script src='upload-video/js/jquery.tag-editor.js'></script><script>$('.tags').tagEditor({ initialTags: ['Hello', 'World', 'Example', 'Tags'], delimiter: ', ', placeholder: 'Enter tags ...' }).css('display', 'block').attr('readonly', true);</script>");


					//return false;
					
                        var countBar = 0; 
						
						
					   
					   
					   var file_data = $('input[type="file"]')[0].files;
						
                       $.each(file_data, function(i, file) {
						  
							setTimeout(function(){
							 
							var bar1 = $("#video-content").find("#bar1_"+countBar);
							
							var status_text = $("#video-content").find("#text_"+countBar);
							
							var vid_id = $("#video-content").find("#vid_"+countBar); 
							
							var fd = new FormData();
							
						    fd.append("file", file);
						
							bar1.css({"width":"1%"});
							bar1.html("1%");
							
							vid_id.attr("src", "images/emty_load.gif");
							
							
							$.ajax({
								
                                url   		: 	"video_album_upload_new.php",
                                type  		: 	"POST",
                                data        : 	fd,
                                contentType : 	false,
                                cache		: 	false,
                                processData	: 	false,
								beforeSend	: 	function() {},
                                xhr			: 	function(){
									
									var xhr = $.ajaxSettings.xhr();
                                    
                                    if (xhr.upload) {
										
											xhr.upload.addEventListener('progress', function(event) {
												
												var percent = 0;
												
												var position = event.loaded || event.position;
												
												var total = event.total;
												
												if (event.lengthComputable) {
													
													percent = Math.ceil(position / total * 100);
												}
											   
												bar1.css({"width":percent+"%"});
												
												bar1.html(percent+"%");
											
											// if(percent==100){
												
												// status_text.html("Now processing your video to mp4. Please keep this page open until its completed.");
												
												
											// }
											
											}, false);
													
													

                                        xhr.upload.addEventListener("loadend", function(e){}, false);
                                    }
									
									countBar++;
									
                                    return xhr;
									
                                },
                                mimeType:"multipart/form-data"
                            }).done(function(res){
								
								var resp = $.parseJSON(res);
								
								if(resp.status==true){
									
									if(resp.file_thumb == undefined){
									
										$("#rem_"+i).remove();
										
										$("#rem_"+i).html('<span class="valid_exten">The file you are uploading may not be a valid video file.<a target="_blank" href="video_extentions.php">See recommended file</a> types for uploads.</span>');
									
									}
									else{
										
										$.each(resp.file_thumb, function(key, value){
											
										   $("#vid-thumb-"+i).append('<div class="col no-padding" onclick="chooseThumb(\'thumb' + i + '_' + key + '\' , \'vid_' + i + '\')" ><input type="radio" name="radiothmb_'+i+'" class="checkThumb hide" value="uploads/video/videomp4/thumbnails/'+value+'" id="thumb'+i+'_'+key+'" ><img class="img-fluid" src="uploads/video/videomp4/thumbnails/'+value+'" id="thumbs'+i+'_'+key+'" alt=""></div>');
											
										   
										
										});
										
								//$("#vid-thumb-"+i).append('<div class="col no-padding"><img style="height: 95px;" class="img-fluid" src="multuple/img/thumb2.jpg" alt=""><div class="details"><a href="#"><p><span>+</span> <br>Add Custom thumb</p></a></div></div>');
										
										
										
										
										$("#thumb"+i+"_0").prop('checked', true);
										
										$("#vid_"+i).attr("src" , "uploads/video/videomp4/thumbnails/"+resp.file_thumb[0]);
										
										$("#img_name_"+i).val(resp.name);
										
										$("#temp_name_"+i).val(resp.tmp_name);
										
										$("#vtitle_"+i).val(resp.tmp_name);
									
									}
									
								}else{
									
									$("#rem_"+i).html('<span class="valid_exten">The file you are uploading may not be a valid video file.<a target="_blank" href="video_extensions.php">See recommended file</a> types for uploads.</span>');
									
								}
								
								
								
                            });
							
							
							
							 }, 2000);
						 
                        });
						
                    }, 1000);	
			
		});
	
	
	
	jQuery(document).on('click', '.remove-image', function() {
             
		var id = jQuery(this).attr('id');
		var data = jQuery(this).attr('data-attr');

		var countValue = jQuery.parseJSON(jQuery('form#img_up').find('input[name="count"]').val());
		
		y = jQuery.grep(countValue.name, function(value) {
		  return value != data;
		});

		countValue.name = y;

		//jQuery('form#img_up').find('input[name="count"]').val(JSON.stringify(countValue));
		
		jQuery("#"+id).remove();
		
	});
	
		jQuery(document).on('click', '.publish_All' ,function(){
		
		// For Image Name .....
		
		var vName = new Array();
		
		$('#video-content .vName').each(function(){
			vName.push($(this).val());
		});
		$("#imgN_array").val(vName.join("@DadillY@"));
		
		// For Image Temp Name .....
		
		var vTname = new Array();
		
		$('#video-content .vTname').each(function(){
			
			vTname.push($(this).val());
			
		});
		
		$("#imgTN_array").val(vTname.join("@DadillY@"));
		
		
		// For Image vSrc Name .....
		
		var vSrc = new Array();
		
		var i = 0;
		
		$('#video-content .video-thumbs').each(function(){
			
			if($(this).parent().find('input[name=radiothmb_'+i+']').is(':checked')) {
				
				vSrc.push($('input[name=radiothmb_'+i+']:checked', this).val());
				
			i++;	
			}
			
		});
		
		
		
		$("#vSrc_array").val(vSrc.join("@DadillY@"));
		
		
		
		
		// For Title .....
		
		var vTitle = new Array();
		
		$('#video-content .vTitle').each(function(){
			
			vTitle.push($(this).val());
			
		});
				
		$("#title_array").val(vTitle.join("@DadillY@"));
		
		// For Type .....
		
		var vType = new Array();
		
		$('#video-content .vType').each(function(){
			vType.push($(this).val());
		});
				
		$("#type_array").val(vType.join("@DadillY@"));
		
		// For Description ....
		
		var vDescrip = new Array();
		
		$('#video-content .vDescrip').each(function(){
			vDescrip.push($(this).val());
		});
				
		$("#descrip_array").val(vDescrip.join("@DadillY@"));
		
		// For Tags ....
		
		var vTags = new Array();
		
		$('#video-content .vTags').each(function(){
			
			vTags.push($(this).val());
			
		});
				
		$("#tags_array").val(vTags.join("@DadillY@"));
		
		var vCats = new Array();
		
		$('#video-content .vCats').each(function(){
			
			vCats.push($(this).val());
			
		});
		
		$("#category_array").val(vCats.join("@DadillY@"));
		
		var vDesc = new Array();
		
		$('#video-content .vDesc').each(function(){
			
			vDesc.push($(this).val());
			
		});
		
		$("#vDesc_array").val(vDesc.join("@DadillY@"));
		
		
		
		$(".formSubmit").trigger('click');
		
	});	
	
	
	
	
	$("form#vid_up").on("submit",function(event) {
		
			event.preventDefault();
			
			var form_data = $(this).serialize(); 
			
			$.ajax({
				url : "upload_video_albums.php",
				type: "POST",
				data : form_data,
				dataType : 'json',
				success: function(result){
					if(result.flag == 1) {
						setTimeout( function () {
							window.location.reload();
						},1000);
					}else{
						alert("Please choose atleast one video");
					}
					
				} 
			});

	});
	
	
});	