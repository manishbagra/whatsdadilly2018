 <?php
ini_set('error_reporting', E_ALL);
require_once "bootstrap.php";
require_once 'classes/Session.class.php';
require_once 'model/Friends.php';
require_once 'model/Notification.php';
require_once 'model/Wall.php';
require_once 'model/Albums.php';
require_once 'model/PhotosModel.php';
require_once 'model/VideosModel.php';
require_once 'model/Audio.php';
require_once 'model/Comments.php';
include 'html/wall/functions.php';

$session = new Session();

if ($session->getSession("userid") != "" || $session->getSession("userid") != null) {
    
    if (isset($_GET['wall_id']) && isset($_GET['comment_id']) && $_GET['wall_id'] != '' && $_GET['comment_id'] != '' && $_GET['wall_id'] != null && $_GET['comment_id'] != null) {
        
        $friend        = new Friends();
        $notifications = new NotificationModel();
        $PhotosModel   = new PhotosModel();
        $VideosModel   = new VideosModel();
        $AudioModel    = new AudioModel();
        $Albums        = new Albums();
        $CommentsModel = new CommentsModel();
        
        $wall = new WallModel();
        
        $wall_id = $_GET['wall_id'];
        
        $getSingleWall = $wall->getSingleWallId($entityManager, $wall_id);
        
        //echo'<pre>';print_r($getSingleWall);die;
        
        $comments = CommentsModel::getCommentsMainPostWithLimit($wall_id, 0, 5, $entityManager);
        
        //echo "<pre>"; print_r($comments);die;
        
        $totalComments = count(CommentsModel::getCountCommentsMainPost($wall_id, $entityManager));
        
        $comment_id = $_GET['comment_id'];
        
        $getPostType = $CommentsModel->getPostCommentType($entityManager, $comment_id);
        
        $freindrequestdata = $friend->getFriendsRequest($entityManager, $session->getSession('userid'));
        
        $notificationdata = $notifications->getNotifications($entityManager, $session->getSession('userid'));
        
        switch ($getPostType) {
            
            case 1:
                
                $resultData = $PhotosModel->getPhotos($wall_id, $entityManager);
                
                break;
            
            case 2:
                
                $resultData = $VideosModel->getVideos($wall_id, $entityManager);
                
                break;
            
            case 3:
                
                $resultData = $AudioModel->getAudios($wall_id, $entityManager);
                
                break;
            
            case 4:
                
                $id_album = $Albums->checkAlbum($wall_id, $entityManager);
                
                $resultData = $Albums->getPhotosByAlbumId($entityManager, $id_album);
                
                break;
            
            default:
                
                //$resultData = $PhotosModel->getPhotos($wall_id, $entityManager);
                
                break;
        }
        
        //echo'<pre>';print_r($resultData);die;
        
    } else {
        
        echo "post id and comment id not sent";
        
    }
    
    include "html/post.php";
    
} else {
    
    header("Location:index.php");
    
}

?> 