<?php
error_reporting(0);
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require 'getid3/getid3.php';
require_once "bootstrap.php";
require_once 'model/Signup.php';
require_once 'model/Twitter.php';
require_once 'model/Wall.php';
require_once 'model/PhotosModel.php';
require_once 'model/Comments.php';
require_once 'model/Notification.php';
require_once 'model/Friends.php';
require_once 'model/Albums.php';
include 'html/wall/functions.php';
include_once("config.php");
include_once("twitteroauth/twitteroauth.php");
require_once 'classes/Session.class.php';

$session = new Session();
$Albums  = new Albums();


if ($session->getSession("userid") != "" || $session->getSession("userid") != null) {
    
    $userId = $session->getSession("userid");
    
    $img_p = $session->getSession("profile_pic");
    
    $data = array(
        "userid" => $session->getSession("userid")
    );
    
    $screen_name = Twitter::getAllTwitterAccounts($data, $entityManager);
    
    $frd_obj = new Friends();
    
    $notifications = new NotificationModel();
    
    function slugify($text)
    {
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        $text = trim($text, '-');
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = strtolower($text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        
        if (empty($text)) {
            return 'n-a';
        }
        
        return $text;
    }
    
    if (!is_dir('uploads')) {
        
        mkdir('uploads');
        
    } else {
        
        $output_dir = __DIR__ . "/uploads/";
        
        $output_dir .= $userId . "/";
        
        if (!file_exists($output_dir)) {
            $mask = umask(0);
            mkdir($output_dir, 0777);
            umask($mask);
        }
        
        $output_dir .= "posted/";
        
        if (!file_exists($output_dir)) {
            $mask = umask(0);
            mkdir($output_dir, 0777);
            umask($mask);
        }
        
        $thumb_out = $output_dir . "thumb";
        
        if (!file_exists($thumb_out)) {
            $mask = umask(0);
            mkdir($thumb_out, 0777);
            umask($mask);
        }
        
    }
    
    
    
    $status_saved = false;
    
    $types = array(
        'image/png',
        'image/jpg',
        'image/gif',
        'image/jpeg'
    );
    
    if (isset($_POST['post_id'])) {
        $comment = $_POST['comment-' . $_POST['post_id']];
        $post_id = $_POST['post_id'];
        
        
        $ext = array_pop(explode('.', $_FILES['photo']['name']));
        if ($_FILES['photo']['error'] == 0 && in_array($_FILES['photo']['type'], $types)) {
            $new_file = slugify($_FILES['photo']['name']) . '.' . $ext;
            move_uploaded_file($_FILES['photo']['tmp_name'], $output_dir . $new_file);
            $comment .= '<div class="comment-photo"><img src=" ' . $output_dir . $new_file . '"></div>';
        }
        $params              = array();
        $params['author_id'] = $_SESSION['userid'];
        $params['post_id']   = $post_id;
        $params['text']      = $comment;
        $params['date']      = date('Y-m-d H:i:s');
        CommentsModel::addComment($entityManager, $params);
        
    } else {
        
        
        
        foreach ($_FILES as $file) {
            
            //echo'<pre>';print_r($file);die;
            $ext = array_pop(explode('.', $file['name']));
            
            if ($file['error'] == 0 && in_array($file['type'], $types)) {
                
                
                $new_file  = slugify($file['name']) . '.' . $ext;
                //echo'<pre>';print_r($new_file);
                $imageName = $file['name'];
                
                $uploadedfile = $file['tmp_name'];
                //echo'<pre>';print_r();
                $getID3       = new getID3;
                
                $fileinfo = $getID3->analyze($uploadedfile);
                //echo'<pre>';print_r()
                $width    = $fileinfo['video']['resolution_x'];
                
                $height = $fileinfo['video']['resolution_y'];
                
                $imgNm = uniqid();
                
                $extension = $Albums->getExtension($imageName);
                
                $extension = strtolower($extension);
                
                if (!$status_saved) {
                    
                    if ($_POST['status'] == 'Whats in your head?') {
                        $_POST['status'] = '';
                    }
                    
                    
                    
                    $params                     = array();
                    $params['author_id']        = $_SESSION['userid'];
                    $params['owner_id']         = $_SESSION['userid'];
                    $params['text']             = @$_POST['status'];
                    $params['link']             = '';
                    $params['link_description'] = '';
                    $params['link_photo']       = '';
                    $params['link_title']       = '';
                    $params['date']             = date('Y-m-d H:i:s');
                    
                    $id_wall = WallModel::addWallEntry($entityManager, $params);
                    
                    $new_file = slugify($file['name']) . '.' . $ext;
                    
                    $fullnamefile = $output_dir . $new_file;
                    
                    $status_saved = true;
                    
                    $get_user_info = $frd_obj->getUserInfoById($entityManager, $userId);
                    
                    $fullname = $get_user_info['firstname'] . ' ' . $get_user_info['lastname'];
                    
                    $frnds_info = $frd_obj->getFriendsEmail($entityManager, $userId);
                    
                    $msg = addslashes("<span>" . $fullname . "</span> publish new photo . You can comment it and share.");
                    
                    $noti_file = 'uploads/' . $userId . '/posted/' . $imgNm . "." . $extension;
                    $noti_type = 7;
                    $noti_read = 0;
                    
                    $noti_id_friend = $userId;
                    
                    foreach ($frnds_info as $key => $value) {
                        
                        $noti_id_user = $value['user_id'];
                        
                        $sql = "INSERT INTO `notifications` (`id_user`,`id_friend`,`type`,`message`,`thumb`,`readed`) values ('" . $noti_id_user . "','" . $noti_id_friend . "','" . $noti_type . "','" . $msg . "','" . $noti_file . "','" . $noti_read . "')";
                        
                        $notifications->addNotificationNEW($entityManager, $sql);
                        
                        
                    }
                    
                    
                    
                }
                
                
                
                if ($extension == "jpg" || $extension == "jpeg") {
                    $src = imagecreatefromstring(file_get_contents($uploadedfile));
                    
                } else if ($extension == "png") {
                    $src = imagecreatefromstring(file_get_contents($uploadedfile));
                    
                } else {
                    $src = imagecreatefromstring(file_get_contents($uploadedfile));
                }
                
                switch ($width && $height) {
                    
                    case $width > 250 && $height > 260:
                        $newwidth3  = 250;
                        $newheight3 = 260;
                        break;
                    case $width > 250 && $height < 260:
                        $newwidth3  = 250;
                        $newheight3 = $height;
                        break;
                    case $width < 250 && $height > 260:
                        $newwidth3  = $width;
                        $newheight3 = 260;
                        break;
                    default:
                        $newwidth3  = $width;
                        $newheight3 = $height;
                        break;
                }
                
                $original_aspect = $width / $height;
                
                $thumb_aspect = $newwidth3 / $newheight3;
                
                if ($original_aspect >= $thumb_aspect) {
                    
                    $new_height = $newheight3;
                    
                    $new_width = $width / ($height / $newheight3);
                } else {
                    
                    $new_width  = $newwidth3;
                    $new_height = $height / ($width / $newwidth3);
                }
                
                $thumb = imagecreatetruecolor($newwidth3, $newheight3);
                
                imagecopyresampled($thumb, $src, 0 - ($new_width - $newwidth3) / 2, 0 - ($new_height - $newheight3) / 2, 0, 0, $new_width, $new_height, $width, $height);
                
                
                
                $image = $imgNm . "." . $extension;
                
                $filename = $thumb_out . "/" . $image;
                
                
                if (move_uploaded_file($uploadedfile, $output_dir . "/" . $image)) {
                    
                    imagejpeg($thumb, $filename, 80);
                    
                    $params              = array();
                    $params['wall_id']   = $id_wall;
                    $params['file']      = $image;
                    $params['date']      = date('Y-m-d H:i:s');
                    $params['posted_by'] = $_SESSION['userid'];
                    $id                  = PhotosModel::addPhotoNew($entityManager, $params);
                    
                }
                if (!copy($output_dir . "/" . $image, 'uploads/' . $image)) {
                    throw new Exception('Could not move 2nd file');
                }
                //move_uploaded_file($file['tmp_name'], $output_dir.$new_file);
                
                
            }
        }
    }
    $previous_post = WallModel::getPrviousPost($entityManager, $id_wall);
    
    $comments = CommentsModel::getComments($previous_post[0]['id'], $entityManager);
    $photos   = PhotosModel::getPhotos($previous_post[0]['id'], $entityManager);
    
    $previous_post[0]['comments'] = $comments;
    $previous_post[0]['photos']   = $photos;
    $postids                      = 0;
    //echo'<pre>';print_r($previous_post['date']);die;
    foreach ($previous_post as $entry) {
?>

        <div class="middle_area_four crispbx crispbxmain" id="wall<?php
        echo $entry['id'];
?>"
             style="margin-bottom:5px;" data="<?php
        echo $entry['id'];
?>" data-count="<?php
        echo $postids;
?>">

            <div class="slider_two_title">


                <img class="image_border_style" src="uploads/<?php
        echo $_SESSION['profile_pic'];
?>" alt=""
                     style="width:49px;height:49px;"/>

                <div class="slider_title_style2">

                    <span
                        class="author_slide_top author_upload_name"><?php
        echo $entry['firstname'];
?><?php
        echo $entry['lastname'];
?></span><span></span>
                    <p class="update_profile_date"><?php
        echo WallModel::time_elapsed_string($entry['date']);
?>
                   </p></div>

            </div>
            <div style="padding:10px;width:100%;">
                <p>
                    <?php
        echo Functions::addLink($entry['text']);
?></p></div>

            <?php
        if (strlen($entry['link']) > 0) {
?>
               <div class="display_profile_pic">
                    <?php
            if (strlen($entry['link_photo']) > 0):
?>
                       <img src="<?php
                echo $entry['link_photo'];
?>" alt=""/>
                    <?php
            endif;
?>
                   <div style="padding:10px;">

                        <p><a target="_blank" href="<?php
            echo $entry['link'];
?>"
                              style="color: #000;font-size: 18pt;line-height: 1.2em;"><?php
            echo $entry['link_title'];
?></a>
                        </p>
                        <p style="font-size:12pt;"><?php
            echo $entry['link_description'];
?></p></div>
                    <div class="clear"></div>
                </div>
            <?php
        }
?>

            <div style="padding:10px;">
              
                <?php
        if (!empty($entry['photos'])) {
?>
                   <div class="crispbx">
                        <div class="crispcont">

                            <!-- <p> -->
                            <?php
            $i = 0;
            if (count($entry['photos']) >= 2) {
                //echo'<pre>';print_r(count($entry['photos']));die;
                $linhas = floor(count($entry['photos']) / 3);
                $resto  = count($entry['photos']) % 3;
                
                $tam1h = '143px';
                $tam2h = '216.5px';
                $tam3h = '436px';
                
                $tam1w = '32.8%';
                $tam2w = '49.2%';
                $tam3w = '99.1%';
                
            }
            //echo'<pre>';print_r('aaaa');die;
            $photoDiv .= '<div class="wall_photoboard">';
            $i = 1;
            foreach ($entry['photos'] as $path) {
                
                if ($i <= ($linhas * 3)) {
                    
                    // $tamAtual = $tam1;
                    
                    $tamAtual  = $tam1w;
                    $tamAtualh = $tam1h;
                    
                } else {
                    if ($resto == 2) {
                        
                        // $tamAtual = $tam2;
                        
                        $tamAtual  = $tam2w;
                        $tamAtualh = $tam2h;
                        
                    } else
                    // $tamAtual = $tam3;
                        $tamAtual = $tam3w;
                    $tamAtualh = $tam3h;
                    
                }
                
                $margin = 'margin: 2px 0 0 2px';
                if (count($entry['photos']) == 1) {
                    $margin    = 'margin: 0';
                    $tamAtualh = '100%';
                    $tamAtual  = '100%';
                }
                
                $photoDiv .= '<a class="mid2wall" href="javascript:void(0)" ><img  style="width: ' . $tamAtual . '; height: ' . $tamAtualh . '; border-radius:px;' . $margin . ';border:1px solid #ccc !important;"  src="uploads/' . $userId . '/posted/' . $path['file'] . '"/></a>';
                $i++;
                if ($i >= 9 && count($entry['photos']) > 9) {
                    $photoDiv .= ' and ' . (count($entry['photos']) - 9) . ' more photos';
                    break;
                }
                
            }
            $photoDiv .= '</div>';
            
            echo $photoDiv;
            
            $photoDiv = '';
?>
                           <!-- photos uploaded</p> -->
                            <!-- <div class="upimgwrap">

                                <div class="big-photo-container">
                                    <a class="wall_popup"
                                       href="wall_popup.php?wall_id=<?php
            echo $entry['id'];
?>&photoid=<?php
            echo $entry['photos'][0]['id'];
?>&postion=0">
                                        <img src="uploads/<?php
            echo $entry['photos'][0]['file'];
?>" alt=""
                                             class="upbigimg"/>
                                    </a>
                                </div>
                                <div class="upsmimg photo-grid-container">
                                    <?php
            $postion = 1;
            foreach ($entry['photos'] as $key => $photo) {
                if ($key == 0)
                    continue;
?>
                                       <div class="small-photo-container photo-grid-item">
                                            <a class="wall_popup"
                                               href="wall_popup.php?wall_id=<?php
                echo $entry['id'];
?>&photoid=<?php
                echo $photo['id'];
?>&postion=<?php
                echo $postion;
?>">
                                                <img src="uploads/<?php
                echo $photo['file'];
?>" alt=""/>
                                            </a>
                                        </div>
                                        <?php
                $postion++;
            }
?>
                               </div>
                            </div> -->
                        </div>
                    </div>
                <?php
        }
?>


            </div>

            <div class="like_comment_share">


                <a href="#"><span><i class="fa fa-thumbs-up"></i>Like</span></a>

                <a href="#"><span><i class="fa fa-heart"></i>Love it</span></a>
                <a href="#" class="add-comment-link" rel="<?php
        echo $entry['id'];
?>"><span><i
                            class="fa fa-commenting"></i>Comment</span></a>
                <a href="#"><span><i class="fa fa-share-square-o"></i>Share</span></a>


            </div>    <?php
        
        if (empty($_SESSION['profile_pic'])) {
            $profile_pic = "uploads/default/Maledefault.png";
            
        } else {
            
            $profile_pic = 'uploads/' . $_SESSION['profile_pic'];
        }
?>
                       <div class="comment_box " id="comment_box_<?php
        echo $entry['id'];
?>"  style="display: block;" rel="">

                        <form rel="<?php
        echo $entry['id'];
?>" id="commform_<?php
        echo $entry['id'];
?>" action=""
                              method="post" enctype="multipart/form-data" onsubmit="return false;">
                            <img src="<?php
        echo $profile_pic;
?>" alt=""
                                 style="width:37px;height:37px;">
                            <span><input id="postid_<?php
        echo $entry['id'];
?>" class="comment_box_inline comment_input"
                                         placeholder="Write your comment here" type="text"></span>
                            <div class="comment_box_icon comment_box_inline postbutton-comments" id="<?php
        echo $entry['id'];
?>">
                            <a href="javascript:void"><span><i
                                            class="comment_box_left fa fa-camera"></i></span></a>
                               </div>
                            <h5 class="press_enter_post">Press enter to post: </h5>
                        </form>
                    </div>


            <div data="<?php
        echo $entry['id'];
?>" id="comment_<?php
        echo $entry['id'];
?>">
                <?php //echo '<pre>'; print_r($entry['comments']); 
?>
               <?php
        foreach ($entry['comments'] as $comment):
            $cuserdetails = WallModel::getUserDetails($entityManager, $comment['author_id']);
        //echo '<pre>'; print_r($cuserdetails);
?>

                    <div class="slider_two_title">
                        <img src="uploads/<?php
            echo $cuserdetails[0]['profile_pic'];
?>"
                             alt="<?php
            echo $comment['1firstname'];
?> <?php
            echo $comment['1lastname'];
?>"
                             style="width:37px;height:37px;">
                        <div class="slider_title_style 1single-comment">
                            <a href="#"><span
                                    class="author_slide_top"><?php
            echo $comment['firstname'];
?><?php
            echo $comment['lastname'];
?></span></a>
                            <span
                                class="comment_box_bottom_text"><?php
            echo Functions::addLink($comment['text']);
?></span>
                            <a href="#"><p class="like_and_reply"><span>Like</span><span
                                        class="comment_reply">Reply</span>
                                    <?php
            echo date('m/d/Y H:i:s', strtotime($comment['date']));
?></p></a>
                        </div>
                    </div>
                    <div class="line"></div>
                <?php
        endforeach;
?>
           </div>


        </div>


        <?php
        $postids++;
    }
?>
   <?php
    die();
} else {
    header("Location:index.php");
}