<?php

/**
 * @Entity(repositoryClass="ContactRepository") @Table(name="contacts")
 */
class UserContact {

    /** @Id @Column(type="integer") @GeneratedValue */
    protected $id;
	
	/** @user_id @Column(type="integer") */
    protected $user_id;
    
    /** @Column(type="string") */
    protected $email;
    /** @Column(type="string") */
   
    public function getId() {
        return $this->id;
    }
    
    public function setUser_id($id){
        $this->user_id = $id;
    }
	
	public function getUser_id() {
        return $this->user_id;
    }
   
    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }
}