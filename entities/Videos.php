<?php

/**
 * @Entity @Table(name="videos")
 */
class Videos {

    /** @Id @Column(type="integer") @GeneratedValue */
    protected $id;

    /** @Column(type="integer") */
    protected $wall_id;

    /** @Column(type="string") */
    protected $file;

    /** @Column(type="string") */
    protected $title;

    /** @Column(type="string") */
    protected $description;

    /** @Column(type="string") */
    protected $tags;

    /** @Column(type="string") */
    protected $tags_more;

    /** @Column(type="string") */
    protected $status;

    /** @Column(type="string") */
    protected $cat;

    /** @Column(type="string") */
    protected $tumbnails;

    /** @Column(type="string") */
    protected $notify;

    /** @Column(type="string") */
    protected $date;
	
	 /** @Column(type="integer") */
    protected $privacy;
	
	 /** @Column(type="integer") */
    protected $thumbnail;

    public function getId() {
        return $this->id;
    }

    public function getWallId() {
        return $this->wall_id;
    }

    public function setWallId($wall_id) {
        $this->wall_id = $wall_id;
    }
	
	public function setPrivacy($privacy) {
        $this->privacy = $privacy;
    }
	
	public function getPrivacy($privacy) {
        return $this->privacy;
    }

    public function getFile() {
        return $this->file;
    }

    public function setFile($file) {
        $this->file = $file;
    }

    public function getDate() {
        return $this->date;
    }

    public function setDate($date) {
        $this->date = $date;
    }
	
	public function setThumbnail($thumbnail) {
        $this->thumbnail = $thumbnail;
    }
	
	public function getThumbnail(){
        return $this->thumbnail;
    }

    function getWall_id() {
        return $this->wall_id;
    }

    function getTitle() {
        return $this->title;
    }

    function getDescription() {
        return $this->description;
    }

    function getTags() {
        return $this->tags;
    }

    function getTags_more() {
        return $this->tags_more;
    }

    function getStatus() {
        return $this->status;
    }

    function getCat() {
        return $this->cat;
    }

    function getTumbnails() {
        return $this->tumbnails;
    }

    function getNotify() {
        return $this->notify;
    }

    function setWall_id($wall_id) {
        $this->wall_id = $wall_id;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setTags($tags) {
        $this->tags = $tags;
    }

    function setTags_more($tags_more) {
        $this->tags_more = $tags_more;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setCat($cat) {
        $this->cat = $cat;
    }

    function setTumbnails($tumbnails) {
        $this->tumbnails = $tumbnails;
    }

    function setNotify($notify) {
        $this->notify = $notify;
    }

}
