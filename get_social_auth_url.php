 <?php
include_once("config.php");
include_once("twitteroauth/twitteroauth.php");
require_once "bootstrap.php";
require_once 'model/Signup.php';
require_once 'model/Twitter.php';
require_once 'classes/Session.class.php';

session_start();

$OAUTH_CALLBACK = 'http://whatsdadilly.com/betaPhaseProjectWDD/get_social_auth_url.php';

if (isset($_REQUEST['oauth_token']) && $_SESSION['token'] !== $_REQUEST['oauth_token']) {
    
    session_destroy();
    
} elseif (isset($_REQUEST['oauth_token']) && $_SESSION['token'] == $_REQUEST['oauth_token']) {
    
    $session = new Session();
    
    $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['token'], $_SESSION['token_secret']);
    
    $access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);
    
    
    if ($connection->http_code == '200') {
        
        
        $_SESSION['status']       = 'verified';
        $_SESSION['request_vars'] = $access_token;
        $_SESSION['twitter']      = 1;
        
        
        
        $screenname         = $_SESSION['request_vars']['screen_name'];
        $twitterid          = $_SESSION['request_vars']['user_id'];
        $oauth_token        = $_SESSION['request_vars']['oauth_token'];
        $oauth_token_secret = $_SESSION['request_vars']['oauth_token_secret'];
        
        
        $session->setSession("twitter", 1);
        $session->setSession("screen_name_twitter", $_SESSION['request_vars']['screen_name']);
        $session->setSession("screen_id_twitter", $_SESSION['request_vars']['user_id']);
        $session->setSession("auth_token_twitter", $_SESSION['request_vars']['oauth_token']);
        $session->setSession("auth_secret_twitter", $_SESSION['request_vars']['oauth_token_secret']);
        
        $data = array(
            "networkname" => "twitter",
            "auth_token" => $oauth_token,
            "auth_secret" => $oauth_token_secret,
            "user_id" => $session->getSession("userid"),
            "screen_name" => $screenname,
            "img_link" => 'img/icons/boy.png',
            "screen_id" => $twitterid
        );
        
        $twit_val = Twitter::getTwitterValidation($data, $entityManager);
        
        if (count($twit_val) == 0) {
            Signup::addToken($data, $entityManager);
            $redirect = "home.php?msg=success";
        } else {
            $redirect = "home.php?msg=error";
        }
        unset($_SESSION['token']);
        unset($_SESSION['token_secret']);
        header('Location:' . $redirect);
    } else {
        die("error, try again later!");
    }
} else {
    if (isset($_GET["denied"])) {
        header('Location: index.php?option=com_community&view=fprofile');
        die();
    }
    
    $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
    
    $request_token = $connection->getRequestToken($OAUTH_CALLBACK);
    
    //received token info from twitter
    $_SESSION['token'] = $request_token['oauth_token'];
    
    $_SESSION['token_secret'] = $request_token['oauth_token_secret'];
    
    if ($connection->http_code == '200') {
        
        $twitter_url = $connection->getAuthorizeURL($request_token['oauth_token']);
        
        header('Location: ' . $twitter_url);
        
    } else {
        
        die("error connecting to twitter! try again later!");
    }
    
} 