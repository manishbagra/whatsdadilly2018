<?php
error_reporting(-1);
require_once "bootstrap.php";
include_once("config.php");
require_once 'classes/Session.class.php';
require_once 'classes/Albums.class.php';
require_once 'entities/Album.php';
require_once 'model/Albums.php';
require_once 'model/Wall.php';
require_once 'model/PhotosModel.php';

$session = new Session();

$Albums = new Albums();

$WallModel = new WallModel();

$PhotosModel = new PhotosModel();


if ($session->getSession("userid") != "" || $session->getSession("userid") != null) {
    
    if (isset($_SESSION['profileid']) && $_SESSION['profileid'] != "" || isset($_SESSION['profileid']) && $_SESSION['profileid'] != null) {
        
        $user_id = $_SESSION['profileid'];
        
        $result = $PhotosModel->getPhotosByPostedId($user_id, $entityManager);
        
    } else {
        
        $user_id = $session->getSession("userid");
        
        $result = $PhotosModel->getPhotosByPostedId($user_id, $entityManager);
    }
    
    
    
    include 'html/posted_photos.php';
    
} else {
    header("Location:index.php");
}