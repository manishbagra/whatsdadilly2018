
        <link rel="stylesheet" href="audio/css/nice-select.css"> 
       <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">   
        <link rel="stylesheet" href="audio/css/main.css">
		<link rel="stylesheet" href="audio/css/audio_player.css">
        <script src="audio/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>  
		
	<div class="overlay audio">
			<div style="float: none !important"  class="col-lg-7 main-wrap">
					<div class="main-area">
					  <div class="col-lg-3 tab-wrap"> <!-- required for floating -->
						<!-- Nav tabs -->
						<ul class="nav nav-tabs tabs-left sideways">
						  <li class="active"><a href="#home-v" data-toggle="tab">Basic info</a></li>
						  <li><a href="#profile-v" data-toggle="tab">Metadata</a></li>
						  <li><a href="#messages-v" data-toggle="tab">Permission</a></li>
						</ul>
					  </div>
					  <div class="col-lg-9 left-wrap">
						<!-- Tab panes -->
						<div class="tab-content">
						  <div class="tab-pane active" id="home-v">
							  <div class="col-lg-3 left-bar">
								<img class="img-responsive" src="images/bg.png" alt="">
								<a class="add-change-btn" href="#">Add/Change Photo</a>
								<br>
								<p><span><b>Attachment Details</b></span></p>
								<p><span id='file_name'>Melody01521.mp3</span></p>
								<p>Date: <span id='file_date'>31st jan 18</span></p>
								<p>Size: <span id='file_size'>3.86MB</span></p>
								<p>Length: <span id='file_time'>4:25 min</span></p>
								<br>
								<p>Overall Progress</p>
								<div class="progress" style="height:20px">
									<div class="progress-bar" id='audio_process' style="width:1%" ><span style='color:#FFF;font-weight:600'>1%</span></div>
								</div> 
							  </div>
							  <div class="col-lg-9 right-content">        
								<form action='javascript:void(0)' id='audio_form' >
								<input type='hidden' name='unique_file' id='unique_file'>
								<input type='hidden' name='file_thumb' id='file_thumb'>
								
								  <div class="form-group"> <!-- Full Name -->
									<label for="full_name_id" class="control-label">Playlist Title <span class="star">*</span></label>
									<input type="text" class="form-control" id="full_name_id" name="full_name" placeholder="Name your playlist">
								  </div>  

								  <div class="form-group"> <!-- Street 1 -->
									<label for="street1_id" class="control-label">Say something about this playlist <span class="star">*</span> </label>
									<input type="text" class="form-control" id="street1_id" name="street1" placeholder="Say something">
								  </div>          
								  
								  <div class="form-group">
									<div id="fullname">http://whatsdadily.com/whatsdadily/set</div><a href='javascript:void(0);' class="btn float-right-btn"><i class="fa fa-edit"></i></a>
								  </div>                      
								  <div class="form-group"> <!--  type -->
									<label for="state_id" class="control-label">Playlist Type <span class="star">*</span></label>
									<select name='playlist_type' >
									  <option value="1">Personal creation</option>
									  <option value="2">Alaska</option>
									  <option value="3">Arizona</option>
									  <option value="4">Arkansas</option>
									</select>         
								  </div>


								  <div class="form-group col-lg-6 drop-downs"> <!--  release date -->
									<label for="state_id" class="control-label">Release Date <span class="star">*</span></label>
								    <input  class="form-control" id="datepicker" type="text" placeholder="31st Jan 2018" name="bday">                                
								  </div>
								  <div class="form-group col-lg-6 drop-downs2"> <!--  genre -->
									<label for="state_id" class="control-label">Genre <span class="star">*</span></label>
									<select name='genre' >
									  <option value="1">Western Melody</option>
									  <option value="2">Alaska</option>
									  <option value="3">Arizona</option>
									  <option value="4">Arkansas</option>
									</select>                                
								  </div>                                            
								  
								  <div class="form-group"> <!-- tags-->
									<label for="zip_id" class="control-label">Additional Tags<span class="star">*</span></label>
									<input type="text" class="form-control" id="zip_id" name="zip" placeholder="Melody,Western,folk,Metal">
								  </div>   

								   <div class="form-group"> <!-- description field -->
									<label class="control-label " for="message">Descrioption<span class="star">*</span></label>
									<textarea class="form-control message" cols="20" id="message" placeholder="Describe your playlist" name="message" rows="5"></textarea>
								  </div> 
					 
							  </div>                   
							  <div class="col-lg-12 more-less">
									<div class="row" id='right-box'>
									  
									</div>
								  <div class="col-lg-12 bottom-wrap">
									<div class="form-group"> 
										<p class="col-lg-2 bal1">
										  <span>Required fields</span> <span class="star">*</span></p>
										<p class="col-lg-2 bal2" >
										  <span>Set This media post</span> 
										</p>
										<p class="col-lg-3">                        
											<select name='post_type'>
											  <option value="1">Public</option>
											  <option value="0">Private</option>
											</select> 
										</p>
										<!-- Submit Button -->
										<div class="col-lg-5 bbtn">
											<a href="javascript:void(0);" type="submit" class="btn btn-primary btn-gray" style="margin-right: 10px;">Cancel</a>
											<a href="javascript:void(0);" type="submit" id="publish_audio" class="btn btn-primary btn-black">Publish</a>
										</div>
									  </div>     
									</form>  
								  </div>
													
							  </div>    


						  </div>
						  <div class="tab-pane" id="profile-v">Metadata Tab.</div>
						  <div class="tab-pane" id="messages-v">Permission Tab.</div>
						</div>
					</div>          
				</div>
			</div> 
	</div>   	
      <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

      <script src="audio/js/jquery.nice-select.min.js"></script>  
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
      <script src="audio/js/main.js"></script>
      <script>
        $('#fullname').click(function(){
          var name = $(this).text();
          $(this).html('');
          $('<input></input>')
				.attr({
					'type': 'text',
					'name': 'fname',
					'id': 'txt_fullname',
					'size': '30',
					'value': name
				})
              .appendTo('#fullname');
          $('#txt_fullname').focus();
      });

      $(document).on('blur','#txt_fullname', function(){
          var name = $(this).val();
          $('#fullname').text(name);
      });
      </script>
      <script>
                  $('select').niceSelect();
      </script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>      