<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>:WHATSDADILLY:</title>

    <link rel="stylesheet" href="css/reset-min.css" type="text/css" />
	
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
	
    <link href="css/style-album.css" type="text/css" rel="stylesheet" />

    <link href="css/bootstrap.min.css" rel="stylesheet"/>

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>

    <script type="text/javascript" src="js/jquery.colorbox.js"></script>

    <link rel="stylesheet" href="css/colorbox.css" type="text/css" />

    <script type="text/javascript" src="js/albums_header.js"></script>

    <link rel="stylesheet" href="js/build/coverphoto.css" />
    
    <link rel="stylesheet" href="css/profilestyle.css" />

    <link rel="stylesheet" href="css/profile.css" />

    <script src="js/owl.carousel.js"></script>

    <script type="text/javascript" src="js/main.js"></script>


    <script type="text/javascript" src="js/build/coverphoto.js"></script>

  

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

 
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link href="css/albumstyle.css" rel="stylesheet">    
	
	<script src="js/jquery.row-grid.js"></script>

<style>

#map {
	width: 100%;
	height: 400px;
}
.controls {
	margin-top: 10px;
	border: 1px solid transparent;
	border-radius: 2px 0 0 2px;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	height: 32px;
	outline: none;
	box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
}
#controlss{
	border: 1px solid #cbcbcb;
    border-radius: 2px 0 0 2px;
    /* box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3); */
    box-sizing: border-box;
    height: 37px;
    margin-top: 1px;
    outline: medium none;
    width: 100%;
	padding-left:7px;
	font-family: Roboto;
	font-size: 15px;
	font-weight: 300;
	text-overflow: ellipsis;
}
.controlss {
	border: 1px solid #cbcbcb;
    border-radius: 2px 0 0 2px;
    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    box-sizing: border-box;
    height: 37px;
    margin-top: 1px;
    outline: medium none;
    width: 223px;
	padding-left:7px;
	font-family: Roboto;
	font-size: 15px;
	font-weight: 300;
	text-overflow: ellipsis;
	background-color: #fff;
	width: 100%;
}
#searchInput {
	background-color: #fff;
	font-family: Roboto;
	font-size: 15px;
	font-weight: 300;
	margin-left: 0px;
	padding: 0 11px 0 13px;
	text-overflow: ellipsis;
	width: 100%;
}
#searchInput:focus {
 	border-color: #4d90fe;
}
#cboxWrapper{
	width:100% !important;
}
#cboxContent{
	width:100% !important;
}
#cboxLoadedContent{
	width:100% !important;
	overflow:hidden !important;
}
#uploadMore{
	filter: none !important;
	width:100%;
}
#cboxOverlay{ 

visibility: visible; opacity: 0.9 !important;

}
#cboxOverlay:before {
    content: "\25AE";
    font-family: FontAwesome;
    left:-5px;
    position:absolute;
    float:right;
	top:0;
 }

.modal-backdrop.in{ opacity :0.9 !important; }


</style>

<style>
   
    .GalleryGrid:before,
	
    .GalleryGrid:after {
      GalleryGrid: "";
      display: table;
    }
    .GalleryGrid:after {
      clear: both;
    }
    .item {
      float: left;
      margin-bottom: 15px;
	  
    }
    .item img {
      max-width: 100%;
      max-height: 100%;
      vertical-align: bottom;
    }
    .first-item {
      clear: both;
    }
 
    .last-row, .last-row ~ .item {
      margin-bottom: 0;
    }
  </style>

<script>
function closePopup(){
	$('#EscModal').modal('show');
	$("#colorbox").css('opacity','0.7');
}

$(document).ready(function() {
	
	 $(".GalleryGrid").rowGrid({itemSelector: ".item", minMargin: 10, maxMargin: 25, firstItemClass: "first-item", lastItemClass: "last-item"});
	
	$(".closeOpacity").on('click',function(){
		$("#colorbox").css('opacity','1');
	});
	
	$(document).keydown(function(e) {
		if (e.keyCode == 27) {
			$('#EscModal').modal('show');
			$("#colorbox").css('opacity','0.7');
		}   
	});
	
		
	
	$('#DiscardAlbum').on('click',function(){
		location.reload();
	});

        $("#togglebtn").click( function() {

            var x = document.getElementById('mySidenav');
            var y = document.getElementById('imagecontainer') ;
            var z= document.getElementById('button');

            console.log("just enterted");

            if ($(x).is(":visible")) {
                console.log("!clicked");
                x.style.display = 'none';
                y.className="col-md-12";
                z.className ="col-md-4 col-md-offset-8";
                var classChange =document.querySelectorAll('.col') ;
                for (var i = 0; i<classChange.length; i++){
                    classChange[i].className = "col-md-2";
                }

            } else {
                console.log("clicked");
                x.style.display = 'block';
                x.style.backgroundColor="white";
                y.className="col-md-10 col-md-offset-2";
                z.className = "col-md-4 col-md-offset-8";
                var classChange =document.querySelectorAll('.col-md-2') ;
                for (var i = 0; i<classChange.length; i++){
                    classChange[i].className = "col";
                }

            }
        });

        $("#openBox").colorbox({rel: 'openBox', iframe: false,escKey: false,overlayClose: false, width: "100%", height: "85%",
           
            onClosed: function() {

                clearAlbum();

            }

        });
		
         

        // $("#openBox2").colorbox({rel: 'openBox', iframe: true,escKey: false, overlayClose: false, width: "100%", height: "85%",

            // onClosed: function() {

                // clearAlbum();

            // }

        // });

                   

                    

});

</script>

        </head>



        <body  class="">    
<style>
.file-upload-container {
    width: 140px;
    /* border: 1px solid #efefef; */
    padding: 2px;
    border-radius: 6px;
    -webkit-border-radius: 6px;
    -moz-border-radius: 6px;
    background: #fbfbfa;
    font-family: calibri;
    font-size: 16px;
}
.file-upload-override-button {
    position: relative;
    overflow: hidden;
    cursor: pointer;
    background-image: -webkit-linear-gradient(top, rgb(255,255,255) 2%, rgb(240,240,240) 2%, rgb(222,222,222) 100%);
    
    -moz-border-radius: 6px;
    -webkit-border-radius: 0;
    border: 1px solid #888;
    color: #444;
    font-size: 10px;
    font-weight: 600;
    padding: 8px 6px;
    text-decoration: none;
    font-family: sans-serif,arial;
    text-transform: uppercase;
    margin-top: 4px;
}
.file-upload-override-button i {
    margin-right: 5px;
}
.file-upload-button {
    position: absolute;
    height: 50px;
    top: -10px;
    left: -10px;
    cursor: pointer;
    background: #fff;
    opacity: 0;
}
.file-upload-filename {
    margin-left: 10px;
    height: auto;
    padding: 8px;
}
.both {
    clear: both;
}

</style>
            <!-- <body  class="nobg">     -->

            

<?php include 'headerHome.php'; ?>


            <i id="togglebtn" class="fa fa-bars" aria-hidden="true"></i>  
            <div id="content-section" class="container-fluid">
                <div id="sidebar" class="sidenav-class">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="mySidenav" class="sidenav">
                                    <ul id="sidenav" class="sidenav-ul">
                                        <li>
                                            <a class="nav-active" href="album.php"><span>Albums</span></a>
                                        </li>
                                        <li>
                                            <a href="#"><span> Posted Photos</span></a>
                                        </li>
                                        <li>
                                            <a href="videos.php"><span>Video's</span></a>
                                        </li>
                                    </ul>
                                    <div id="social">Social</div>
                                    <ul class="sidenav-ul">
                                        <li class="li-class" >
                                            <a href="#"><div ><img class="img-li" src="images/instragram.png"/> </div><div class="img-text">Instragram</div></a>
                                        </li>
                                        <li class="li-class">
                                           <a href="#"><div ><img class="img-li" src="images/tumblr_icon.png"/> </div> <div class="img-text">Tumblr</div></a>
                                        </li>
                                        <li class="li-class">
											<a href="#"><div><img class="img-li" src="images/Pinterest.png"/> </div> <div class="img-text">Pinterest</div></a>
										</li>
										<li class="li-class">
											<a href="#"><div><img class="img-li" src="images/youtube.png"/> </div> <div class="img-text">Youtube</div></a>
										</li>
									</ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<br/><br/><br/>
            <div id="imagecontainer" class="col-md-10 col-md-offset-2">
                <div id="button" class="col-sm-4 col-md-offset-8" style="padding: 0">
                    
                     <!--<div class="col-sm-6" style="padding: 0">
                        <a style="" class="newbutton cboxElement" id="openBox2"  href="album_new.php" title="new photos or videos" style="margin-bottom: 4px;"><i class="fa fa-plus-circle" style="vertical-align: middle;font-size:18px;"></i>&nbsp; Add Photos / Videos</a>
                     </div>-->
					 
                    <!-- <button id="addphoto">Add Photos/Video</button> -->
                    <div class="album_form col-sm-6" style="z-index:100;position:relative;float:right;">
                        <div class="file-upload-container">
                        <a style=" display: none;width: auto;" class="newbutton" id="openBox" rel="openBox" href="imageuploader.php" title="New Album" style="margin-bottom: 4px;"></a>
                            <div class="file-upload-override-button left"><i class="fa fa-plus-circle" style="vertical-align: middle;font-size:18px;"></i>Create Album<br>
                            <form method="post" id="img_up" enctype="multipart/form-data">
                            <input name="files[]" class="file-upload-button" title="New Album" id="file-upload-button" type="file" multiple />
                            <input  type="hidden" class="title" name="title"/>
							<input  type="hidden" id="picDesc" name="picDesc"/>
							<input  type="hidden" id="image64" name="image64"/>
							<input  type="hidden" class="album_location" name="album_location"/>
							<input  type="hidden" class="description" name="description"/>
                            <input  type="hidden" id="countImg" name="count"/>
                            <input type="submit" class="formSubmit" name="submit" style="display:none;" />
                            </form>
							</div>
                            
                        <div class="both"></div>
                        </div>
                       <!--  <input type="file" name="files[]" />
                        <a style="width: auto;" class="newbutton" id="openBox" rel="openBox" href="imageuploader.php" title="New Album" style="margin-bottom: 4px;">
                            <i class="fa fa-plus-circle" style="vertical-align: middle;font-size:18px;"></i>&nbsp;&nbsp;&nbsp;Create Album

                        </a> -->

                    </div>
                </div>
               
                <div class="col-md-12" id="below-nav"></div>

                <div class="container-fluid">

                    <?php echo Albums::listAlbum($entityManager, $params); ?>
                    
                </div>
            </div> 
        <script>
            jQuery(document).on('change', '#file-upload-button', function(event){
				
                    
					jQuery(document).find('a#openBox').trigger('click');
					
					
                    
					var filesCount = jQuery(document).find("form#img_up :input");
                    
					var place = jQuery(document).find(".right");
					
					
                    
					//Total no of files
                    var total_selected_files = filesCount[0].files.length;
                    //Files content
					
                    var total_files_content = filesCount[0].files;

                    //alert(total_files_content[0].name);
					
					myHTML='';
                    
					var countVar = 0;
					
				   for(var k=0;k<total_selected_files;k++){
                        var reader = new FileReader();
						
                        reader.onload = function(event) {
                            myHTML +='<div class="col-sm-2 uplod remove-img" id="rem_'+countVar+'" data-attr="'+total_files_content[countVar].name+'"><i class="fa fa-trash remove-image" id="rem_'+countVar+'" data-attr="'+total_files_content[countVar].name+'" aria-hidden="true"></i><form class="frmMyImage" enctype="multipart/form-data">';
                            myHTML +='<div class="uplode"></div><div class="image-preview" id="img_'+countVar+'"><img src="'+event.target.result+'"></div><input id="controlss" name="pic_desc[]" class="pic_desc" placeholder="Say something about this photo" type="text"><div class="bar" id="bar2_'+countVar+'"><div class="bar1" id="bar1_'+countVar+'"></div></div></div>';
                            myHTML += '</form></div>';
                            countVar++;
                        }
                        reader.readAsDataURL(total_files_content[k]); 
                    }
					
					
					
                    setTimeout(function(){
						
						
						
						myHTML +=	'<div class="col-sm-2 upload uplodMan"><div class="image-preview" style="max-height:400px !important;min-height:400px !important;width:90%;" onClick="abc();"><img id="uploadMore" src="images/addnew.png"/></div></div>';
	
						
						
                     var place = jQuery(document).find(".right");
                     place.html(myHTML);
                        var fd = new FormData();
						
                        var file_data = $('input[type="file"]')[0].files; // for multiple files
						
						
                        for(var j = 0;j<file_data.length;j++){
                            fd.append("file_"+j, file_data[j]);
                        }
                        // main upload ajax
						
                        var countBar = 0; 
						
                        var completedCount = 0;
						
						var divHeight = $("#cboxLoadedContent").height();
	
						if(divHeight != ''){
							$(".right").css('height', divHeight);
						}
						
						if(divHeight == '490'){
							$(".left_side_br").css({"height": "438px", "overflow-y": "scroll"});
						}
						
                        for(var n=0;n<total_selected_files;n++){
							
                        setTimeout(function(){ 
						
						
						
                            var dc = jQuery(document);
							
                            var formData = new FormData();
							
                            var other_data = $('form#img_up').serializeArray();
							
                            $.each(other_data,function(key,input){
								fd.append(input.name,input.value);
							});
							
                            var bar1 = $("#colorbox").find("#bar1_"+countBar);
							
                            bar1.css({"width":"1%"});
							
							$(".complete_bar1").css({"width":"1%"});
							
                            $.ajax({
                                url : "album_upload_new.php",
                                type: "POST",
                                data : fd,
                                contentType: false,
                                cache: false,
                                processData:false,
								
                                xhr: function(){
                                    
                                    var xhr = $.ajaxSettings.xhr(); 
                                    
                                    if (xhr.upload) {
										
                                        xhr.upload.addEventListener('progress', function(event) {
                                            
											var percent = 0;
											
                                            var position = event.loaded || event.position;
                                            
                                            var total = event.total;
											
                                            if (event.lengthComputable) {
												
                                                percent = Math.ceil(position / total * 100);
                                            }
											
											
											$(".complete_bar1").css({"width":percent+"%"});
                                           
                                            bar1.css({"width":percent+"%"});
                                           
                                            
                                        }, false);

                                        xhr.upload.addEventListener("loadend", function(e){
                                            console.log(e);
                                            var img = $("img", "#img_"+completedCount);
                                            img.css({"filter":"blur(0px)","-webkit-filter":"blur(0px)","-moz-filter":"blur(0px)"});
                                            $("#bar2_"+completedCount).hide();
											$(".complete_bar").hide();
                                            completedCount++;
                                        }, false);
                                    }
                                    countBar++;
                                    return xhr;
                                },
                                mimeType:"multipart/form-data"
                            }).done(function(res){
								
                                $('#countImg').val(res);
                            });
                            }, 2000);
                        }
                    }, 1000);
                });


                
            $(document).ready(function(){

            //on form submit
            $("form#img_up").on("submit",function(event) {
                    event.preventDefault();
                    var form_data = $(this).serialize(); 
                $.ajax({
                    url : "upload_albums.php",
                    type: "POST",
                    data : form_data,
                    dataType : 'json',
                    success: function(result){
                        if(result.flag == 1) {
                            setTimeout( function () {
								
								refreshAlbum(result.album_id,result.total_photos,result.photos_array);
								
								 setTimeout( function () {
									 
									window.location.reload();
									
								 },1000);  
								 
                            },1000);  
                        }
                        
                    } 
                });

            });


            jQuery(document).on('click', '.date1' ,function(){
				
                $(document).find('.title').val($('#title_text').val());
				
                jQuery(".title").val(jQuery("#title_text").val());
				
                jQuery(".description").val(jQuery("#album_desc").val());
				
				$(".album_location").val($("#searchInput").val());
				
				var s1 = [];
				
				$(".pic_desc").each(function(){
					var row = [];
					row.push($(this).val());
					s1.push(row);
				});
				
				var Image64 = new Array();
				
				$('.right .uplod').each(function(){
					
					Image64.push($(this).attr('data-attr'));
					
				});
				
				$("#image64").val(Image64.join("@DadillY@"));
				
				$('#picDesc').val(s1.join("@DadillYWhats@"));
				
                    if($.trim(jQuery(".title").val()) != ''){
                        jQuery(".formSubmit").trigger('click');
                        
                    }else{
                        jQuery(document).find("#title_text").css("border", "1px solid red");
                        return false;
                    }
                    
                });

            jQuery(document).on('click', '.remove-image', function() {
             
                var id = jQuery(this).attr('id');
                var data = jQuery(this).attr('data-attr');

                var countValue = jQuery.parseJSON(jQuery('form#img_up').find('input[name="count"]').val());
                
                y = jQuery.grep(countValue.name, function(value) {
                  return value != data;
                });

                countValue.name = y;

                //jQuery('form#img_up').find('input[name="count"]').val(JSON.stringify(countValue));
                
				jQuery("#"+id).remove();
            });
                    
            });
        </script>
		<!-- Modal -->
			  <div class="modal fade" style='z-index:15000;margin-top:22%;' id="EscModal"  role="dialog">
				<div class="modal-dialog">
				
				  <!-- Modal content-->
				  <div class="modal-content">
					<div class="modal-header" style='padding:5px 5px 5px 11px;'>
					  <button type="button" class="close closeOpacity" data-dismiss="modal">&times;</button>
					  <h5 class="modal-title" style='color:#000;font-weight:600'>Discard Changes?</h5>
					</div>
					<div class="modal-body" style='padding:0px;'>
					  <p style="color:#000; font-size:16px;margin-top:2%;margin-left:2%;">If you discard now, you'll lose your progress.</p>
					</div>
					<div class="modal-footer" style='padding:5px;'>
					  <button type="button" style='padding: 2px 9px;' class="btn btn-default closeOpacity" data-dismiss="modal">Go Back</button>
					  <button type="button" style='padding: 2px 9px;' id='DiscardAlbum' class="btn btn-primary" data-dismiss="modal">Discard</button>
					</div>
				  </div>
				  
				</div>
			  </div>
		
    </body>
</html>