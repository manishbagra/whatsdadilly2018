<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>:WHATSDADILLY:</title>
            <link rel="stylesheet" href="css/reset-min.css" type="text/css" />
            <link rel="stylesheet" href="css/style.css" type="text/css" />
            <link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />

            <script type="text/javascript" src="js/jquery-1.9.1.js"></script>
            <script type="text/javascript" src="js/jquery-ui.js"></script>
			<script type="text/javascript" src="js/choosen.js"></script>
            <link href="css/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
            <!-- Bootstrap -->
            <link href="css/bootstrap.min1.css" rel="stylesheet" />
            <link href="css/bootstrap-theme.min.css" rel="stylesheet" />
            <link href="css/registration-process1.css" rel="stylesheet" />
			
			<link rel="stylesheet" href="css/choosen.css">
			
			<!-- Bootstrap core CSS -->

			<!-- custom CSS -->
			<link href="css/Landingstyle.css" rel="stylesheet">
			<link href="font-awesome/css/font-awesome.css" rel="stylesheet">
			<link href="fonts/stylesheet.css" rel="stylesheet">
                       

                        <script type="text/javascript">
                            $(document).ready(function()
                            {
								$("#country").chosen();
								$("#state").chosen();
								$("#city").chosen();
								
								$("#login_btn2").on('click',function(){
									
									if($("#country").val()==""){
										$("#ErrCountry").show().delay(5000).fadeOut();
									}
									
									
								});
								
								$("#country_chosen").on('click',function(){
									var singleValue = $("div#country_chosen a.chosen-single span").html();
									$("div#country_chosen .chosen-search input").val(singleValue);
								});
								
								$("#state_chosen").on('click',function(){
									var singleValue = $("div#state_chosen a.chosen-single span").html();
									
									$("div#state_chosen .chosen-search input").val(singleValue);
								});
								
								$("#city_chosen").on('click',function(){
									var singleValue = $("div#city_chosen a.chosen-single span").html();
									$("div#city_chosen .chosen-search input").val(singleValue);
								});
								
								
                                $( "#country" ).change(function() {
                                    $("#country_hide").val(($(this).val()));
                                });
								
                               
                                
							
                            $("#country").on('change',function(){
								var country_id = $(this).val();
								$.ajax({
									type     : 'post',
									dataType : 'html',
									url      : 'getStatesBycountry.php',
									data     : {country_id : country_id},
									success  : function(data){
										$("#states").html(data);
									}
								});
							}); 	
							
								
                            });
                        </script>

                          
                        <style>

                            #login_topblack
                            {
                                top:0px !important;
                                position:absolute;
                            }

                            body
                            {
                                background:#fff !important;
                            }
                            td
                            {
                                color:#333 !important;
                            }

                        </style>
                        </head>
                        <body>
                            <?php include 'header.php'; ?>
                            <div class="cols-xs" style="margin-top:15%;">
                                <div class="StepProgresses">
                                    <div class="Step1Process">
                                        Step 1 <br/>
                                        <small>Personal Details</small>
                                    </div>
                                    <div class="Step2Process">
                                        Step 2 <br/>
                                        <small>Location details</small>
                                    </div>
                                    <div class="Step3Process">
                                        Step 3 <br/>
                                        <small>Profile Picture</small>
                                    </div>
                                    <div class="Step4Process">
                                        Step 4 <br/>
                                        <small>Invie Your Friends</small>
                                    </div>

                                </div>
                                <form id="AuthForm" class="Form FancyForm AuthForm" action="signup2.php" method="POST" accept-charset="utf-8">
                                    <input type="hidden" name="country_hide" id="country_hide" value="" />
                                    <input type="hidden" name="current_city_id" id="current_city_id" value="" />
                                    <input type="hidden" name="home_city_id" id="home_city_id" value="" />
                                    <div class="progress" style="margin-top:0 !important;">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
                                            <span class="fright" style="margin-right:20px;"><strong>50% </strong>Complete</span>
                                        </div>
                                    </div>
                                    <p class="InviteYourFrieds"><i>Select Your country ,state, city</i></p>
                                    <div class="SelectCounCityDetails">
                                        <div class="SelectCountry">Current Country
                                           <select name="country" id="country" required class="form-control chosen">
                                                <option value="">Select Country</option>
                                                <?php for ($i=0;$i<count($country);$i++) {
                                                ?>
                                                    <option value="<?php echo $country[$i]['id']; ?>"><?php echo $country[$i]['name']; ?></option>
                                                <?php } ?>
                                            </select>
											
                                        </div>
										<span id="ErrCountry" style='display:none;' class="ErrAddress">Please select country</span>
										
                                      
                                        <div class="SelectCountry" id='states'>Current Province
                                            <select name="state" id="state" class="form-control chosen">
                                                <option value="">Select Province</option>
                                            </select>
                                        </div>
										
										<span id="ErrState" style='display:none;' class="ErrAddress">Please select Province</span>
										
                                        <div class="SelectCountry" id="cities">Current City
                                           <select name="city" id="city" class="form-control chosen">
                                                <option value="">Select City</option>
                                            </select>
                                        </div>
										
										<span id="ErrCities" style='display:none;'  class="ErrAddress">Please select City</span>
										
										
                                    </div>

                                    <div class="SaveOrSkip">
                                        <span style="margin-left:20px; padding-top:20px;vertical-align:middle;">
                                            <a href="index.php" style="color:#cccccc;"> <img src="rgimages/back-icon.png"> Back</a>
                                        </span>
                                        <div class="fright">or <a href="signup3.php" id="login_btn1">Skip This Step</a>
                                            <input type="submit" id="login_btn2" value="Save & Continue" class="btn btn-default">
                                        </div>
                                    </div>
                                </form>



<style>
  .ErrAddress{
  color:red;
    margin-left:55%;
  }
</style>


                            </div>
                            <div class="FooterBelowHints">By selecting your city, we can ensure that content specific to your area is communicated to you.
                                Network with people in your area to find resources easily. <br/><br/>
                                Always keep you in the loop about what's happening in the city around you since its always a good idea to be aware of current events that are close to home. </div>

                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
	</body>
</html>