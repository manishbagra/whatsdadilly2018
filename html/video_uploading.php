<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="upload-video/css/nice-select.css"> 
        <link rel="stylesheet" href="upload-video/css/jquery-ui.css">  
        <link rel="stylesheet" href="upload-video/css/jquery.tag-editor.css">       
        <link rel="stylesheet" href="upload-video/css/main.css">
        
    </head>
    <body>

      <div class="container total-wrap  another">
        <div class="row justify-content-center">
          <div class="col-lg-9 col-md-12 main-wrap">

              <div class="total-process-area row">
                <div class="col-lg-3 col-md-3 col-sm-12 tab-wrap no-padding left-wrap">
                    <h4 style="font-size:0.9rem !important;">Uploaded 00 of 02 Videos</h4>
                    <p>
                      Your videos are still Uploading <br>
                      Please keep this page opne until it's done
                    </p>
                </div>   
                <div class="col-lg-9 col-md-9 col-sm-12 right-wrap no-padding">
                  <div class="form-group">
                     <div class="progress">
                      <div class="progress-bar" role="progressbar" aria-valuenow="1"
                      aria-valuemin="0" aria-valuemax="100" style="width:1%">
                        1%
                      </div>
                    </div>
                  </div>
                  <div class="form-group row align-items-center justify-content-end footer-group"> 
                    <div class="col-lg-2 bal2" >
                     <span>Set video as<span class="star">*</span>  </span> 
                    </div>
                    <div class="col-lg-3">                        
                      <select class="" id="state_id">
                      <option value="pp">Public</option>
                      <option value="pv">Private</option>
                      </select> 
                    </div>
                    <!-- Submit Button -->
                    <div class="col-lg-6 bbtn">
                      <button type="submit" class="btn btn-primary btn-black publish_All">Publish all</button>
                      <button type="submit" class="btn btn-primary btn-gray" style="margin-right: 10px;">Cancel all</button>                              
                    </div>
                  </div>                   
                </div>
              </div>  

              <div id="video-content"></div>

              </form>                         
            </div>
          </div>      
        </div>
      </div>

      <script src="upload-video/js/jquery-1.11.1.min.js"></script>

      <script src="upload-video/js/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

      <script src="upload-video/js/bootstrap-4.0.0.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

      <script src="upload-video/js/jquery.nice-select.min.js"></script>   
      <script src="https://code.jquery.com/ui/1.10.2/jquery-ui.min.js"></script>
      <script src="upload-video/js/jquery.caret.min.js"></script>
      <script src="upload-video/js/jquery.tag-editor.js"></script>
      <script src="upload-video/js/main.js"></script>

  
    </body>
</html>
