<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/fav.png">
    <!-- Author Meta -->
    <meta name="author" content="">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Whatsdadilly?</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700,900" rel="stylesheet">
    <!--
		CSS
	============================================= -->
    <link rel="stylesheet" href="header/css/linearicons.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link rel="stylesheet" href="header/css/bootstrap.css">
    <link rel="stylesheet" href="header/css/animate.min.css">    
    <link rel="stylesheet" href="header/css/main.css">
	
	<script src="header/js/vendor/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="header/js/vendor/bootstrap.min.js"></script>
    <script src="header/js/wow.min.js"></script>        
    <script src="header/js/main.js"></script>
	
	
	<!--<link rel="stylesheet" href="album/css/linearicons.css" />
    <link rel="stylesheet" href="album/css/bootstrap.css" />-->
	<link href="css/whatsdadilly.css" type="text/css" rel="stylesheet" />
   
	<link rel="stylesheet" href="album/css/lightbox.css" />
    <link rel="stylesheet" href="css/colorbox.css" type="text/css" />

	<link href="css/style-album.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" id="lcso-head-styles" type="text/css" href="video-lightbox/wp-content/lcso-cache/album-lightbox.css" />
	
	<!-- start video css -->
	<link rel="stylesheet" type="text/css" href="css/rs_video.css" />
	<link href="video-player/css/video-js.min.css" rel="stylesheet"/>
	<link rel="stylesheet" href="css/vid_pip.css" />
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css" />
	<script src="video-player/js/video.min.js"></script>
	<!-- end video css -->
</head>

<body>
<span class="video_upload hide"><?php include 'video_uploading.php'; ?></span>
    <div class="container-wrap">
            <div class="container">
                <nav class="navbar navbar-dark sticky-top topnav-bg-bg flex-md-nowrap inside-wrap-menu ">
                    <a class="navbar-brand col-sm-12 col-md-3 mr-0 wow fadeInLeft" data-wow-duration=".8s" data-wow-delay=".9s" href="index.html" style="visibility: visible; animation-duration: 0.8s; animation-delay: 0.9s; animation-name: fadeInLeft;">
                        <img class="img-fluid" src="header/img/logo.png" alt="">
                    </a>
                    <button type="button" id="sidebarCollapse" class="btn navbar-btn collupse-btn">
                        <i class="fas fa-bars"></i>
                    </button>
                    <div class="search-form d-flex flex-row relative wow fadeInRight" data-wow-duration=".8s" data-wow-delay=".9s"style="visibility: visible; animation-duration: 0.8s; animation-delay: 0.9s; animation-name: fadeInRight;">
                        <input class="form-control form-control-dark w-100 dropdown-toggle" data-toggle="dropdown" placeholder="Search" aria-label="Search" type="text">
                        <button type="submit" class="search-btn"><i class="fa fa-search"></i></button>
                        <div class="dropdown-menu search-dropdown">
                            <div class="dropdown-top d-flex justify-content-between">
                                <p>Recent Searches</p>
                                <a href="#"><p>Edit</p></a>
                            </div>                    
                            <div class="notification-list message-list search-list">
                                <ul>
                                    <li>
                                        <a href="#" class="d-flex justify-content-start align-items-center">
                                            <div class="pro-thumb">
                                                <img src="header/img/pro-pic/p2.jpg" alt="">           
                                            </div>
                                            <div class="notification-details d-flex flex-column">
                                                <h6>Daniel Barnes</h6>
                                                <p>
                                                    You will wait? or going now?
                                                </p>
                                            </div>                                  
                                        </a>
                                    </li> 
                                    <li>
                                        <a href="#" class="d-flex justify-content-start align-items-center">
                                            <div class="pro-thumb">
                                                <img src="header/img/pro-pic/p3.jpg" alt="">           
                                            </div>
                                            <div class="notification-details d-flex flex-column">
                                                <h6>Jack Scrimshaw</h6>
                                                <p>
                                                    You will wait? or going now?
                                                </p>
                                            </div>                                  
                                        </a>
                                    </li> 
                                    <li>
                                        <a href="#" class="d-flex justify-content-start align-items-center">
                                            <div class="pro-thumb">
                                                <img src="header/img/pro-pic/p4.jpg" alt="">           
                                            </div>
                                            <div class="notification-details d-flex flex-column">
                                                <h6>Liên Kim</h6>
                                                <p>
                                                    You will wait? or going now?
                                                </p>
                                            </div>                                  
                                        </a>
                                    </li>                                                  
                                </ul>
                            </div>   
                            <div class="dropdown-bottom">
                                <a href="#">Custom Searches</a>
                            </div>
                        </div>  
                    </div>
                    <ul class="top-left-menu px-5 wow fadeInRight" data-wow-duration=".8s" data-wow-delay=".9s"style="visibility: visible; animation-duration: 0.8s; animation-delay: 0.9s; animation-name: fadeInRight;">
                        <li class="user-details">
                            <a href="#">
                            <img src="header/img/user.jpg" alt=""></a>
                            <p>
                                Rayshwan
                                <br> Williams
                            </p>
                        </li>
                        <li class="nav-item">
                            <a href="home.php" class="nav-link">
                                <svg xmlns="http://www.w3.org/2000/svg"  style="width:17px;height: 17px;" viewBox="0 0 15 14"><path class="homes" d="M14.8 6.8c.3-.3.2-.7-.1-1L8.1.2c-.4-.3-.8-.3-1.1 0l-6.7 6c-.3.3-.3.7 0 1l.2.2c.3.3.7.3 1 .1l.4-.5v6.4c0 .4.3.7.7.7h2.6c.4-.1.8-.4.8-.8V8.9h3.3v4.5c0 .4.3.7.7.7h2.8c.4 0 .7-.3.7-.7V7l.3.3c.2.1.5 0 .8-.3l.2-.2z" fill="#555"/></svg>
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
                                <svg xmlns="http://www.w3.org/2000/svg"  style="width:17px;height: 17px;" viewBox="0 0 20 14"><path class="homes" d="M-7.7 21.8c.3-.3.2-.7-.1-1l-6.7-5.7c-.3-.3-.8-.3-1.1 0l-6.7 6c-.3.3-.3.7 0 1l.2.2c.3.3.7.3 1 .1l.5-.4v6.4c0 .4.3.7.7.7h2.6c.4 0 .7-.3.7-.7v-4.5h3.3v4.5c0 .4.3.7.7.7h2.8c.4 0 .7-.3.7-.7V22l.3.3c.2.1.5 0 .8-.3l.3-.2zM10 8.3c2.1 0 3.8-1.9 3.8-4.1C13.8 1.9 12.1 0 10 0 7.9 0 6.2 1.9 6.2 4.1c0 2.3 1.7 4.2 3.8 4.2zm6.6 2.5l-4.5-2.3c-.6.4-1.4.6-2.2.6-.8 0-1.5-.2-2.2-.6l-4.4 2.3c-.3.2-.6.6-.6 1v1c0 .7.6 1.2 1.3 1.2h12c.7 0 1.2-.5 1.2-1.2v-1c0-.4-.2-.8-.6-1zm.1-4.3c1 0 1.7-.9 1.7-1.9 0-1.1-.8-1.9-1.7-1.9S15 3.6 15 4.6c-.1 1.1.7 1.9 1.7 1.9zm3 1.2l-2-1.1c-.3.2-.6.3-1 .3s-.7-.1-1-.3l-2 1.1c-.1.1-.2.1-.2.2l2.4 1.3h3.6c.3 0 .6-.2.6-.6v-.4c-.1-.2-.2-.4-.4-.5zM3.3 6.5c1 0 1.7-.9 1.7-1.9 0-1.1-.8-1.9-1.7-1.9s-1.7.9-1.7 1.9c0 1.1.8 1.9 1.7 1.9zm3.3 1.4c-.1-.1-.1-.2-.2-.2l-2-1.1c-.4.2-.7.3-1.1.3-.4 0-.7-.1-1-.3l-2 1.1c-.2.1-.3.3-.3.5v.4c0 .3.3.6.6.6h3.6l2.4-1.3z" fill="#555"/></svg>
                            </a>
                            <span class="badge badge-warning">12</span>                
                            <div class="dropdown-menu" id="friend-req-dropdown">
                                <div class="dropdown-top d-flex justify-content-between">
                                    <p>Accepted Requests</p>
                                    <a href="#"><p>Find Friends</p></a>
                                </div>
                                <div class="notification-list message-list">
                                    <ul>
                                        <li>
                                            <a href="#" class="d-flex justify-content-between">
                                                <div class="pro-thumb">
                                                    <img src="header/img/friends-thumb.jpg" alt="">           
                                                </div>
                                                <div class="notification-details" style="width: 80% !important;">
                                                    <p style="width: 100% !important;">
                                                        <span>Mike, Tati</span> and 3 others accepted your <br> friend requests.
                                                    </p>
                                                    <span class="time" style="width: 100% !important;"><img class="mr-10" src="img/users.jpg" alt="">2 hours ago</span>
                                                </div>
                                                <div class="notification-type">
                                                </div>                                    
                                            </a>
                                        </li>                                                          
                                    </ul>
                                </div>                      
                                <div class="notification-time">
                                    <p>Friend Requests</p>
                                </div>                     
                                <div class="notification-list message-list">
                                    <ul>
                                        <li>
                                            <a href="#" class="d-flex justify-content-between">
                                                <div class="pro-thumb">
                                                    <img src="header/img/pro-pic/p2.jpg" alt="">           
                                                </div>
                                                <div class="notification-details">
                                                    <h6 class="text-white">Liên Kim</h6>
                                                    <p>
                                                       Friends with Cynthia
                                                    </p>
                                                </div>
                                                <div class="notification-type d-flex flex-row">
                                                        <button type="button" class="btn ad-btns actives">
                                                          <i class="fas fa-user-plus"></i> Confirm
                                                        </button>
                                                        <button type="button" class="btn ad-btns">
                                                          <i class="fas fa-eye-slash"></i> Ignore
                                                        </button>                                            
                                                </div>                                    
                                            </a>
                                        </li> 
                                        <li>
                                            <a href="#" class="d-flex justify-content-between">
                                                <div class="pro-thumb">
                                                    <img src="header/img/pro-pic/p3.jpg" alt="">           
                                                </div>
                                                <div class="notification-details">
                                                    <h6 class="text-white">Jack Scrimshaw</h6>
                                                    <p>
                                                       Friends with Rachael
                                                    </p>
                                                </div>
                                                <div class="notification-type d-flex flex-row">
                                                        <button type="button" class="btn ad-btns actives">
                                                          <i class="fas fa-user-plus"></i> Confirm
                                                        </button>
                                                        <button type="button" class="btn ad-btns">
                                                          <i class="fas fa-eye-slash"></i> Ignore
                                                        </button>                                            
                                                </div>                                    
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="d-flex justify-content-between">
                                                <div class="pro-thumb">
                                                    <img src="header/img/pro-pic/p4.jpg" alt="">           
                                                </div>
                                                <div class="notification-details">
                                                    <h6 class="text-white">Liên Kim</h6>
                                                    <p>
                                                       Friends with Cynthia
                                                    </p>
                                                </div>
                                                <div class="notification-type d-flex flex-row">
                                                        <button type="button" class="btn ad-btns actives">
                                                          <i class="fas fa-user-plus"></i> Confirm
                                                        </button>
                                                        <button type="button" class="btn ad-btns">
                                                          <i class="fas fa-eye-slash"></i> Ignore
                                                        </button>                                            
                                                </div>                                    
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="d-flex justify-content-between">
                                                <div class="pro-thumb">
                                                    <img src="header/img/pro-pic/p5.jpg" alt="">           
                                                </div>
                                                <div class="notification-details">
                                                    <h6 class="text-white">Jack Scrimshaw</h6>
                                                    <p>
                                                       Friends with Rachael
                                                    </p>
                                                </div>
                                                <div class="notification-type d-flex flex-row">
                                                        <button type="button" class="btn ad-btns actives">
                                                          <i class="fas fa-user-plus"></i> Confirm
                                                        </button>
                                                        <button type="button" class="btn ad-btns">
                                                          <i class="fas fa-eye-slash"></i> Ignore
                                                        </button>                                            
                                                </div>                                    
                                            </a>
                                        </li> 
                                        <li>
                                            <a href="#" class="d-flex justify-content-between">
                                                <div class="pro-thumb">
                                                    <img src="header/img/pro-pic/p5.jpg" alt="">           
                                                </div>
                                                <div class="notification-details">
                                                    <h6 class="text-white">Liên Kim</h6>
                                                    <p>
                                                       Friends with Rachael
                                                    </p>
                                                </div>
                                                <div class="notification-type d-flex flex-row">
                                                        <button type="button" class="btn ad-btns actives">
                                                          <i class="fas fa-user-plus"></i> Confirm
                                                        </button>
                                                        <button type="button" class="btn ad-btns">
                                                          <i class="fas fa-eye-slash"></i> Ignore
                                                        </button>                                            
                                                </div>                                    
                                            </a>
                                        </li>                                                                                                                                                                           
                                    </ul>
                                </div>   
                                <div class="dropdown-bottom">
                                    <a href="#">See All</a>
                                </div>
                            </div>                  
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
                                <svg xmlns="http://www.w3.org/2000/svg"  style="width:17px;height: 17px;" viewBox="0 0 15 14"><path class="homes" d="M-15.2-2.2c.3-.3.2-.7-.1-1L-22-8.9c-.3-.3-.8-.3-1.1 0l-6.7 6c-.3.3-.3.7 0 1l.2.2c.3.3.7.3 1 .1l.5-.4v6.4c0 .4.3.7.7.7h2.6c.4 0 .7-.3.7-.7V-.1h3.3v4.5c0 .4.3.7.7.7h2.8c.4 0 .7-.3.7-.7V-2l.3.3c.2.1.5 0 .8-.3l.3-.2zM2.5-15.7c2.1 0 3.8-1.9 3.8-4.1 0-2.3-1.7-4.1-3.8-4.1-2.1 0-3.8 1.9-3.8 4.1 0 2.2 1.7 4.1 3.8 4.1zm6.6 2.5l-4.5-2.3c-.6.4-1.4.6-2.2.6-.8 0-1.5-.2-2.2-.6l-4.4 2.3c-.4.2-.7.6-.7 1.1v1c0 .7.6 1.2 1.2 1.2h12c.7 0 1.2-.5 1.2-1.2v-1c.2-.5 0-.9-.4-1.1zm.1-4.3c1 0 1.7-.9 1.7-1.9 0-1.1-.8-1.9-1.7-1.9s-1.7.9-1.7 1.9c-.1 1.1.7 1.9 1.7 1.9zm3 1.2l-2-1.1c-.3.2-.6.3-1 .3s-.7-.1-1-.3l-2 1.1c-.1.1-.2.1-.2.2l2.4 1.3H12c.3 0 .6-.2.6-.6v-.4c-.1-.2-.2-.4-.4-.5zm-16.4-1.2c1 0 1.7-.9 1.7-1.9 0-1.1-.8-1.9-1.7-1.9s-1.7.9-1.7 1.9c0 1.1.8 1.9 1.7 1.9zm3.3 1.4c-.1-.1-.1-.2-.2-.2l-2-1.1c-.3.2-.6.3-1 .3s-.7-.1-1-.3l-2 1.1c-.2.1-.3.3-.3.5v.4c0 .3.3.6.6.6h3.6l2.3-1.3zM12.8 3.1l-.2.1-1.1.1-.3.5-.2-.1-.9-.8-.2-.4-.1-.5-.6-.5-.7-.1v.3l.6.6.3.4-.3.2-.3-.1-.4-.2v-.3l-.5-.2-.2.8-.6.1.1.5.7.1.1-.7.6.1.3.2h.5l.3.6.8.8-.1.3h-.6l-1.2.5-.8 1-.1.5h-.3l-.6-.3-.5.3.1.6.2-.3H7v.5l.3.1.3.4.7-.2.6.1.7.2h.4l.6.7 1.2.7-.8 1.5-.8.4-.2.9-1.2.8-.1.5c3-.7 5.3-3.5 5.3-6.8 0-1.4-.4-2.8-1.2-3.9zm-5 7.6l-.5-1 .5-1-.5-.1-.5-.5-1.2-.3-.4-.8v.5h-.1L4 6.1V5l-.7-1.2-1.2.2h-.7L1 3.8l.5-.4-.5.1C.4 4.5 0 5.7 0 7c0 3.9 3.1 7 7 7 .3 0 .6 0 .9-.1l-.1-.8.3-1.3-.3-1.1zM2.6 2.3l1.2-.2.6-.3.7.2 1-.1.4-.6.5.1 1.3-.1.3-.4.4-.3.7.1h.3C9.1.2 8.1 0 7 0 4.8 0 2.9 1 1.6 2.5l1-.2zM7.3.7L8 .3l.5.3-.7.5h-.6L6.9 1l.4-.3zM5.2.8l.3.1.4-.1.2.4-1 .3-.4-.4.5-.3z" fill="#555"/></svg>                    
                            </a>
                            <span class="badge badge-warning">10</span>  
                            <div class="dropdown-menu" id="notification-dropdown">
                                <div class="dropdown-top d-flex justify-content-between">
                                    <p>Notifications</p>
                                    <a href="#"><p>Mark all as read</p></a>
                                </div>
                                <div class="notification-time">
                                    <p>New<span class="red"> ! </span></p>
                                </div>
                                <div class="notification-list">
                                    <ul>
                                        <li class="read">
                                            <a href="#" class="d-flex justify-content-between">
                                                <div class="pro-thumb">
                                                    <img src="header/img/pro-pic/p1.jpg" alt="">           
                                                </div>
                                                <div class="notification-details">
                                                    <p>
                                                        <span>Your video</span> has been successfully published
                                                        and ready to view. You can now watch it and share.
                                                    </p>
                                                    <span class="time"><i class="red fa fa-play-circle" aria-hidden="true"></i>2 hours ago</span>
                                                </div>
                                                <div class="notification-type">
                                                    <div class="thumb-wrap relative">
                                                        <div class="overlay overlay-bg"></div>
                                                        <div class="thumb"> 
                                                            <img src="header/img/pro-pic/t1.jpg" alt="">
                                                        </div>
                                                        <div class="icon">
                                                            <i class="red fa fa-play-circle" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                </div>                                    
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="d-flex justify-content-between">
                                                <div class="pro-thumb">
                                                    <img src="header/img/pro-pic/p2.jpg" alt="">           
                                                </div>
                                                <div class="notification-details">
                                                    <p>
                                                        <span>Tati Fallahi</span> Commented on your cover photo. <br> Rebel Wilson and Mayi Coqui.
                                                    </p>
                                                    <span class="time"><i class="green fa fa-comments" aria-hidden="true"></i>2 hours ago</span>
                                                </div>
                                                <div class="notification-type">

                                                </div>                                    
                                            </a>
                                        </li>                                                           
                                    </ul>
                                </div>                      
                                <div class="notification-time">
                                    <p>Earlier Today</p>
                                </div>                     
                                <div class="notification-list">
                                    <ul>
                                        <li>
                                            <a href="#" class="d-flex justify-content-between">
                                                <div class="pro-thumb">
                                                    <img src="header/img/pro-pic/p1.jpg" alt="">           
                                                </div>
                                                <div class="notification-details">
                                                    <p>
                                                        <span>Your video</span> has been successfully published
                                                        and ready to view. You can now watch it and share.
                                                    </p>
                                                    <span class="time"><i class="red fa fa-play-circle" aria-hidden="true"></i>2 hours ago</span>
                                                </div>
                                                <div class="notification-type">
                                                    <div class="thumb-wrap relative">
                                                        <div class="overlay overlay-bg"></div>
                                                        <div class="thumb"> 
                                                            <img src="header/img/pro-pic/t1.jpg" alt="">
                                                        </div>
                                                        <div class="icon">
                                                            <i class="red fa fa-play-circle" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                </div>                                    
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="d-flex justify-content-between">
                                                <div class="pro-thumb">
                                                    <img src="header/img/pro-pic/p2.jpg" alt="">           
                                                </div>
                                                <div class="notification-details">
                                                    <p>
                                                        <span>Tati Fallahi</span> Commented on your cover photo. <br> Rebel Wilson and Mayi Coqui.
                                                    </p>
                                                    <span class="time"><i class="green fa fa-comments" aria-hidden="true"></i>2 hours ago</span>
                                                </div>
                                                <div class="notification-type">

                                                </div>                                    
                                            </a>
                                        </li>
                                        <li class="read">
                                            <a href="#" class="d-flex justify-content-between">
                                                <div class="pro-thumb">
                                                    <img src="header/img/pro-pic/p1.jpg" alt="">           
                                                </div>
                                                <div class="notification-details">
                                                    <p>
                                                        <span>Your video</span> has been successfully published
                                                        and ready to view. You can now watch it and share.
                                                    </p>
                                                    <span class="time"><i class="pink fa fa-music" aria-hidden="true"></i>2 hours ago</span>
                                                </div>
                                                <div class="notification-type">
                                                    <div class="thumb-wrap relative">
                                                        <div class="overlay overlay-bg"></div>
                                                        <div class="thumb"> 
                                                            <img src="header/img/pro-pic/t2.jpg" alt="">
                                                        </div>
                                                        <div class="icon">
                                                            <i class="red fa fa-music" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                </div>                                    
                                            </a>
                                        </li>  
                                        <li>
                                            <a href="#" class="d-flex justify-content-between">
                                                <div class="pro-thumb">
                                                    <img src="header/img/pro-pic/p1.jpg" alt="">           
                                                </div>
                                                <div class="notification-details">
                                                    <p>
                                                        <span>Your video</span> has been successfully published
                                                        and ready to view. You can now watch it and share.
                                                    </p>
                                                    <span class="time"><i class="yellow fas fa-image" aria-hidden="true"></i>2 hours ago</span>
                                                </div>
                                                <div class="notification-type">

                                                </div>                                    
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="d-flex justify-content-between">
                                                <div class="pro-thumb">
                                                    <img src="header/img/pro-pic/p3.jpg" alt="">           
                                                </div>
                                                <div class="notification-details">
                                                    <p>
                                                        <span>Your video</span> has been successfully published
                                                        and ready to view. You can now watch it and share.
                                                    </p>
                                                    <span class="time"><i class="pink fa fa-music" aria-hidden="true"></i>2 hours ago</span>
                                                </div>
                                                <div class="notification-type">
                                                    <div class="thumb-wrap relative">
                                                        <div class="overlay overlay-bg"></div>
                                                        <div class="thumb"> 
                                                            <img src="img/pro-pic/t2.jpg" alt="">
                                                        </div>
                                                        <div class="icon">
                                                            <i class="red fa fa-music" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                </div>                                    
                                            </a>
                                        </li>      
                                        <li class="read">
                                            <a href="#" class="d-flex justify-content-between">
                                                <div class="pro-thumb">
                                                    <img src="header/img/pro-pic/p3.jpg" alt="">           
                                                </div>
                                                <div class="notification-details">
                                                    <p>
                                                        <span>Tati Fallahi</span> Commented on your cover photo. <br> Rebel Wilson and Mayi Coqui.
                                                    </p>
                                                    <span class="time"><i class="yellow fas fa-image" aria-hidden="true"></i>2 hours ago</span>
                                                </div>
                                                <div class="notification-type">

                                                </div>                                    
                                            </a>
                                        </li>                                                            
                                    </ul>
                                </div>   
                                <div class="dropdown-bottom">
                                    <a href="#">See All</a>
                                </div>
                            </div>                
                        </li>  
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
                                <svg xmlns="http://www.w3.org/2000/svg"  style="width:17px;height: 17px;" viewBox="0 0 17 14"><path class="homes" d="M42.3-16.2c.3-.3.2-.7-.1-1l-6.7-5.7c-.3-.3-.8-.3-1.1 0l-6.7 6c-.3.3-.3.7 0 1l.2.2c.3.3.7.3 1 .1l.5-.4v6.4c0 .4.3.7.7.7h2.6c.4 0 .7-.3.7-.7v-4.5h3.3v4.5c0 .4.3.7.7.7h2.8c.5-.1.8-.4.8-.8V-16l.3.3c.2.1.5 0 .8-.3l.2-.2zM60-29.7c2.1 0 3.8-1.9 3.8-4.1 0-2.3-1.7-4.1-3.8-4.1-2.1 0-3.8 1.9-3.8 4.1 0 2.2 1.7 4.1 3.8 4.1zm6.6 2.5l-4.5-2.3c-.6.4-1.4.6-2.2.6-.8 0-1.5-.2-2.2-.6l-4.4 2.3c-.4.2-.7.6-.7 1.1v1c0 .7.6 1.2 1.2 1.2h12c.7 0 1.2-.5 1.2-1.2v-1c.2-.5 0-.9-.4-1.1zm.1-4.3c1 0 1.7-.9 1.7-1.9 0-1.1-.8-1.9-1.7-1.9s-1.7.9-1.7 1.9c-.1 1.1.7 1.9 1.7 1.9zm3 1.2l-2-1.1c-.3.2-.6.3-1 .3s-.7-.1-1-.3l-2 1.1c-.1.1-.2.1-.2.2l2.4 1.3h3.6c.3 0 .6-.2.6-.6v-.4c-.1-.2-.2-.4-.4-.5zm-16.4-1.2c1 0 1.7-.9 1.7-1.9 0-1.1-.8-1.9-1.7-1.9s-1.7.9-1.7 1.9c0 1.1.8 1.9 1.7 1.9zm3.3 1.4c-.1-.1-.1-.2-.2-.2l-2-1.1c-.3.2-.6.3-1 .3s-.7-.1-1-.3l-2 1.1c-.3.1-.4.3-.4.5v.4c0 .3.3.6.6.6h3.6l2.4-1.3zM70.3-10.9l-.2.1-1.1.1-.3.5-.2-.1-.9-.8-.1-.4-.2-.5-.6-.5-.7-.1v.3l.6.6.3.4-.4.2-.3-.1-.4-.2v-.3l-.6-.2-.2.8-.4.1.1.5.7.1.1-.7.6.1.3.2h.5l.3.6.8.8v.3l-.7-.1-1.2.6-.8 1-.1.4h-.3l-.6-.3-.5.3.1.6.2-.3h.4v.5l.3.1.3.4.7-.1.6.1.7.2h.4l.6.7 1.2.7-.8 1.5-.8.4-.3.9-1.2.8-.1.5c3.1-.7 5.4-3.5 5.4-6.8 0-1.4-.4-2.8-1.2-3.9zm-5 7.6l-.5-.9.5-1-.5-.1-.5-.5-1.2-.3-.4-.9v.5h-.2l-1-1.4V-9l-.7-1.2-1.2.2h-.8l-.4-.3.5-.4-.5.1c-.6 1-1 2.2-1 3.5 0 3.9 3.1 7 7 7 .3 0 .6 0 .9-.1l-.1-.8.3-1.3c.1.1-.2-1-.2-1zm-5.2-8.4l1.2-.2.6-.3.6.2 1-.1.4-.6.5.1 1.3-.1.3-.4.5-.3.7.1h.3c-.9-.4-1.9-.7-3-.7-2.2 0-4.1 1-5.4 2.5l1-.2zm4.7-1.6l.7-.4.5.3-.7.5-.6.1-.3-.2.4-.3zm-2.1.1l.3.1.4-.1.2.4-1 .3-.5-.3c.1-.1.6-.4.6-.4zM1.6 4.3c.2.2.9.6 1.9 1.4 1.1.8 1.9 1.4 2.5 1.8.1 0 .2.1.4.3.3.2.4.3.6.4.1.1.3.2.5.3.2.2.3.3.5.3.2.1.3.1.5.1.1 0 .3 0 .5-.1s.4-.1.5-.3c.2-.1.4-.2.5-.3.1-.1.3-.2.5-.4s.3-.3.4-.3c.6-.4 2.1-1.5 4.5-3.2.5-.3.8-.7 1.2-1.2.3-.5.5-1 .5-1.5 0-.4-.2-.8-.5-1.1-.4-.3-.7-.5-1.1-.5h-14C1 0 .7.2.4.5.1.9 0 1.3 0 1.8c0 .4.2.9.5 1.4.4.5.7.8 1.1 1.1zm14.5 1.1C14 6.9 12.4 8 11.3 8.8c-.4.3-.7.5-.9.7-.2.2-.5.3-.9.5s-.7.2-1 .2c-.3 0-.7-.1-1-.2-.4-.2-.7-.4-.9-.5-.3-.2-.6-.4-.9-.7C4.8 8.2 3.2 7 1 5.4c-.4-.3-.7-.6-1-.9v7.9c0 .4.1.8.4 1.1s.7.5 1.1.5h14c.4 0 .8-.2 1.1-.5.3-.3.4-.7.4-1.1V4.5l-.9.9z" fill="#555"/></svg>
                            </a>
                            <div class="dropdown-menu" id="mail-dropdown">
                                <div class="dropdown-top d-flex justify-content-between">
                                    <p>Recent (13)</p>
                                    <p>Message Requests (12)</p>
                                    <a href="#"><p>Mark all as read</p></a>
                                </div>                    
                                <div class="notification-list message-list">
                                    <ul>
                                        <li>
                                            <a href="#" class="d-flex justify-content-between align-items-center">
                                                <div class="pro-thumb">
                                                    <img src="header/img/pro-pic/p2.jpg" alt="">           
                                                </div>
                                                <div class="notification-details" style="width: 80% !important;">
                                                    <h6 class="text-white">Daniel Barnes</h6>
                                                    <p style="width: 100% !important;">
                                                        You will wait? or going now?
                                                    </p>
                                                    <span class="time"style="width: 100% !important;">2 hours ago</span>
                                                </div>
                                                <div class="notification-type">
                                                    <i class="fas fa-circle"></i>
                                                </div>                                    
                                            </a>
                                        </li>  
                                        <li>
                                            <a href="#" class="d-flex justify-content-between align-items-center">
                                                <div class="pro-thumb">
                                                    <img src="header/img/pro-pic/p3.jpg" alt="">           
                                                </div>
                                                <div class="notification-details" style="width: 80% !important;">
                                                    <h6 class="text-white">Tati Fallahi</h6>
                                                    <p style="width: 100% !important;">
                                                        You will wait? or going now?
                                                    </p>
                                                    <span class="time" style="width: 100% !important;">2 hours ago</span>
                                                </div>
                                                <div class="notification-type">

                                                </div>                                    
                                            </a>
                                        </li>  
                                        <li>
                                            <a href="#" class="d-flex justify-content-between align-items-center">
                                                <div class="pro-thumb">
                                                    <img src="header/img/pro-pic/p4.jpg" alt="">           
                                                </div>
                                                <div class="notification-details" style="width: 80% !important;">
                                                    <h6 class="text-white">Daniel Barnes</h6>
                                                    <p style="width: 100% !important;">
                                                        You will wait? or going now?
                                                    </p>
                                                    <span class="time"style="width: 100% !important;">2 hours ago</span>
                                                </div>
                                                <div class="notification-type d-flex flex-row text-right">
                                                    <i class="mr-10 fas fa-check"></i>
                                                    <i class="mr-10 fas fa-paperclip"></i>
                                                </div>                                    
                                            </a>
                                        </li> 
                                        <li>
                                            <a href="#" class="d-flex justify-content-between align-items-center">
                                                <div class="pro-thumb">
                                                    <img src="header/img/pro-pic/p4.jpg" alt="">           
                                                </div>
                                                <div class="notification-details" style="width: 80% !important;">
                                                    <h6 class="text-white">Tati Fallahi</h6>
                                                    <p style="width: 100% !important;">
                                                        You will wait? or going now?
                                                    </p>
                                                    <span class="time" style="width: 100% !important;">2 hours ago</span>
                                                </div>
                                                <div class="notification-type d-flex flex-row text-right">
                                                    <img class="mr-10" src="img/arrow-right.png" alt="">
                                                    <img class="mr-10" src="img/arrow-left.png" alt="">
                                                </div>                                    
                                            </a>
                                        </li> 
                                        <li>
                                            <a href="#" class="d-flex justify-content-between align-items-center">
                                                <div class="pro-thumb">
                                                    <img src="header/img/pro-pic/p4.jpg" alt="">           
                                                </div>
                                                <div class="notification-details" style="width: 80% !important;">
                                                    <h6 class="text-white">Daniel Barnes</h6>
                                                    <p style="width: 100% !important;">
                                                        You will wait? or going now?
                                                    </p>
                                                    <span class="time" style="width: 100% !important;">2 hours ago</span>
                                                </div>
                                                <div class="notification-type d-flex flex-row text-right">

                                                </div>                                    
                                            </a>
                                        </li> 
                                        <li>
                                            <a href="#" class="d-flex justify-content-between align-items-center">
                                                <div class="pro-thumb">
                                                    <img src="header/img/pro-pic/p4.jpg" alt="">           
                                                </div>
                                                <div class="notification-details" style="width: 80% !important;">
                                                    <h6 class="text-white">Tati Fallahi</h6>
                                                    <p style="width: 100% !important;">
                                                        You will wait? or going now?
                                                    </p>
                                                    <span class="time" style="width: 100% !important;">2 hours ago</span>
                                                </div>
                                                <div class="notification-type d-flex flex-row text-right">

                                                </div>                                    
                                            </a>
                                        </li>                                                                                                                  
                                    </ul>
                                </div>   
                                <div class="dropdown-bottom">
                                    <a href="#">See All</a>
                                </div>
                            </div>                
                        </li>                        
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
                                <svg xmlns="http://www.w3.org/2000/svg"  style="width:17px;height: 17px;" viewBox="0 0 15 14"><path class="homes" d="M-2.2-23.1c.3-.3.2-.7-.1-1L-9-29.8c-.3-.3-.8-.3-1.1 0l-6.7 6c-.3.3-.3.7 0 1l.2.2c.3.3.7.3 1 .1l.5-.4v6.4c0 .4.3.7.7.7h2.6c.4 0 .7-.3.7-.7V-21h3.3v4.5c0 .4.3.7.7.7h2.8c.4 0 .7-.3.7-.7v-6.3l.3.3c.2.1.5 0 .8-.3l.3-.3zM15.5-36.7c2.1 0 3.8-1.9 3.8-4.1 0-2.3-1.7-4.1-3.8-4.1-2.1 0-3.8 1.9-3.8 4.1 0 2.2 1.7 4.1 3.8 4.1zm6.6 2.5l-4.5-2.3c-.6.4-1.4.6-2.2.6-.8 0-1.5-.2-2.2-.6l-4.4 2.3c-.4.2-.7.6-.7 1.1v1c0 .7.6 1.2 1.2 1.2h12c.7 0 1.2-.5 1.2-1.2v-1c.2-.5 0-.9-.4-1.1zm.1-4.3c1 0 1.7-.9 1.7-1.9 0-1.1-.8-1.9-1.7-1.9s-1.7.9-1.7 1.9c-.1 1.1.7 1.9 1.7 1.9zm3 1.2l-2-1.1c-.3.2-.6.3-1 .3s-.7-.1-1-.3l-2 1.1c-.1.1-.2.1-.2.2l2.4 1.3H25c.3 0 .6-.2.6-.6v-.4c-.1-.2-.2-.4-.4-.5zM8.8-38.5c1 0 1.7-.9 1.7-1.9 0-1.1-.8-1.9-1.7-1.9s-1.7.9-1.7 1.9c0 1.1.8 1.9 1.7 1.9zm3.3 1.4c-.1-.1-.1-.2-.2-.2l-2-1.1c-.3.2-.6.3-1 .3s-.7-.1-1-.3l-2 1.1c-.2.1-.3.3-.3.5v.4c0 .3.3.6.6.6h3.6l2.3-1.3zM25.8-17.9l-.2.1-1.1.1-.3.5-.2-.1-.9-.8-.1-.4-.2-.5-.6-.5-.7-.1v.3l.6.6.3.4-.4.2-.3-.1-.4-.2v-.3l-.6-.2-.2.8-.4.1.1.5.7.1.1-.7.6.1.3.2h.5l.3.6.8.8-.1.3-.7-.1-1.2.6-.8 1-.1.4h-.3l-.6-.3-.5.3.1.6.2-.3h.4v.5l.3.1.3.4.6-.2.6.1.7.2h.4l.6.7 1.2.7-.8 1.5-.8.4v1l-1.2.8-.1.5c3-.7 5.3-3.5 5.3-6.8 0-1.4-.4-2.8-1.2-3.9zm-5 7.6l-.5-.9.5-1-.5-.1-.5-.5-1.2-.3-.4-.9v.5H18l-1-1.4V-16l-.7-1.2-1.2.2h-.8l-.3-.2.5-.4-.5.1c-.6 1-1 2.2-1 3.5 0 3.9 3.1 7 7 7 .3 0 .6 0 .9-.1l-.1-.8.3-1.3-.3-1.1zm-5.2-8.4l1.2-.2.6-.3.6.2 1-.1.4-.6.5.1 1.3-.1.3-.4.5-.3.7.1h.3c-.9-.4-1.9-.7-3-.7-2.2 0-4.1 1-5.4 2.5l1-.2zm4.7-1.6l.7-.4.5.3-.7.5-.6.1-.3-.2.4-.3zm-2.1.1l.3.1.4-.1.2.4-1 .3-.5-.3c.1-.1.6-.4.6-.4zM-42.9-2.7c.2.2.9.6 1.9 1.4 1.1.8 1.9 1.4 2.5 1.8.1 0 .2.1.4.3.2.2.4.3.5.4.1.1.3.2.5.3.2.1.4.2.5.3.2.1.3.1.5.1.1 0 .3 0 .5-.1s.4-.1.5-.3c.2-.1.4-.2.5-.3.1-.1.3-.2.5-.4s.3-.3.4-.3c.6-.4 2.1-1.5 4.4-3.2.5-.3.8-.7 1.2-1.2.3-.5.5-1 .5-1.5 0-.4-.2-.8-.5-1.1-.2-.3-.5-.5-.9-.5h-14c-.5 0-.9.2-1.1.5-.3.3-.4.8-.4 1.3 0 .4.2.9.5 1.4.4.5.7.8 1.1 1.1zm14.5 1.1C-30.5-.1-32 1-33.1 1.8c-.4.3-.7.5-.9.7-.2.2-.5.3-.9.5s-.7.2-1 .2c-.3 0-.7-.1-1-.2-.4-.2-.7-.3-.9-.5-.2-.2-.5-.4-.9-.7-.9-.7-2.4-1.8-4.7-3.4-.4-.3-.7-.5-1-.9v7.9c0 .4.1.8.4 1.1s.6.5 1 .5h14c.4 0 .8-.2 1.1-.5.3-.3.4-.7.4-1.1v-7.9l-.9.9zM13.1 5.6h-1.6c-.1-.3-.2-.5-.3-.8l1.1-1.1c.4-.4.4-.9 0-1.3l-.7-.7c-.4-.3-1-.3-1.3 0L9.2 2.8c-.3-.1-.5-.2-.8-.3V.9C8.4.4 8 0 7.5 0h-1c-.5 0-.9.4-.9.9v1.6c-.3.1-.5.2-.8.3L3.7 1.7c-.3-.3-1-.3-1.3 0l-.7.7c-.2.2-.3.4-.3.7 0 .2.1.5.3.7l1.1 1.1c-.1.2-.2.4-.3.7H.9c-.5 0-.9.4-.9.9v1c0 .5.4.9.9.9h1.6c.1.3.2.5.3.8l-1.1 1.1c-.2.2-.3.4-.3.7 0 .2.1.5.3.7l.7.7c.3.4 1 .4 1.3 0l1.1-1.1c.2.1.5.2.8.3v1.6c0 .4.4.8.9.8h1c.5 0 .9-.4.9-.9v-1.6c.3-.1.5-.2.8-.3l1.1 1.1c.3.4 1 .4 1.3 0l.7-.7c.4-.4.4-.9 0-1.3l-1.1-1.1c.1-.2.2-.5.3-.8h1.6c.5 0 .9-.4.9-.9v-1c0-.5-.4-.9-.9-.9zM7 8.8C6 8.8 5.2 8 5.2 7S6 5.2 7 5.2 8.8 6 8.8 7 8 8.8 7 8.8z" fill="#555"/></svg>                    
                            </a>
                          <div class="dropdown-menu settings-menu">
                                <div class="dropdown-top">
                                    <p>Settings</p>
                                </div>
                                <ul class="seting-list">
                                    <li><a href="#"><img src="header/img/setting-icon/p-ico.png" alt="">Privacy</a></li>
                                    <li><a href="settings.php"><img src="header/img/setting-icon/s-ico.png" alt="">Settings</a></li>
                                    <li><a href="accounts.php"><img src="header/img/setting-icon/m-ico.png" alt="">Manage Accounts</a></li>
                                    <li><a href="logout.php"><img src="header/img/setting-icon/l-ico.png" alt="">Logout</a></li>
                                </ul>
                          </div>                
                        </li>                        
                    </ul>
                </nav>              
            </div>
    </div>

    

    

