<?php include 'html/headerAlbum.php'; ?>
 <div class="container-fluid" style="padding-top:80px;">
        <div class="row">
          <?php include 'html/album_side_menu.php' ?>
            <main role="main" class="main-page ml-sm-auto pt-3 px-5">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center  main-content-top border-bottom main-content-top-elements">
                    <h3 class="main-title-heading">Random Stuff</h3>
                    <div class="btn-toolbar mb-md-0">
                        <div class="btn-group btns">
                            <div class="btn-group left-grp">
                                <button type="button" class="btn btn-secondary btn-sm add-btn"><i class="fas fa-plus"></i> Add more photos/videos</button>
                                <button type="button" class="btn btn-secondary btn-sm add-btn"><i class="fas fa-trash"></i> Delete Album</button>
                                <button type="button" class="btn btn-secondary btn-sm add-btn"><i class="fas fa-cog"></i> Manage</button>
                            </div>
                            <button type="button" class="btn btn-secondary btn-sm add-btn upload-btn"><i class="fas fa-upload"></i> Upload</button>
                        </div>
                    </div>
                </div>
                  
					
		<div class="elementor-widget-container" >

			<div id="" class="gg_gallery_wrap gg_columnized_gallery gid_5501 ggom_103   ggom_vc_txt" data-gg_ol="103" data-col-maxw="200" rel="5501" data-nores-txt="No images found in this page">            	

				<div class="gg_loader" style="display: none;">

				<div class="ggl_1">

				</div>

				<div class="ggl_2">
				</div>

				<div class="ggl_3">
				</div>

				<div class="ggl_4">

				</div>

				</div>        	

				<!-- Start Vieo light box plugin -->

				<div class="gg_container">
				
				<div class="5acd93b5cfcaa-0 gg_shown img_size" style="float:left;" >		

					<div class="col-lg-12 col-md-6 single-album img-size">
						<a href="javascript:void(0)"><img class="img-fluid"  src="album/img/add-new.jpg" alt="" /></a>
					</div>	

				</div>
				
				
					<?php foreach($result as $key=>$photo) { 
					
					$path = "uploads/" . $photo['posted_by'] . "/posted/" ;
					
					$actual_image =  $path.$photo['file'];
					
					?>

					<div id="photo-<?php echo $photo['id']; ?>"  
					
					data-gg-link="<?php echo URL.$actual_image; ?>" 
					
					class="gg_img 5acd93b5cfcaa-0 gg_shown img_size" 
					
					data-gg-url="<?php echo URL.$actual_image; ?>" 
					
					data-gg-title="" 
					
					data-gg-author="Rayshawn Williams" 
					
					data-gg-descr="" 
					
					data-img-id="0"  rel="5501">
					
						<div class="col-lg-12 col-md-6 single-album img-size">
							<div class="img-container">
								<div class="thumb">
									<img class="img-fluid img-photos" id="img-photos-<?php echo $key; ?>"  src="<?php echo $path."thumb/".$photo['file']; ?>" alt="" >
								</div>
								<a class="edit-btn" href="javascript:void(0)"><i class="fas fa-pencil-alt"></i></a>
								<div class="detais">
									<p class="left-part">
										<a href="javascript:void(0)">Like</a>
										<a href="javascript:void(0)">Comment</a>
									</p>
									<p class="right-part">
										<span><i class="fas fa-thumbs-up"></i> 27 </span>
										<span><i class="fas fa-comment"></i> 27 </span>
									</p>
								</div>
							</div>
							<a href="javascript:void(0)"><h6 class="album-title"></h6></a>
						</div>
						
					</div>
						
					<?php  } ?>
				</div>	

			</div>
		
		</div>
                    
              

            </main>
        </div>
    </div>
	<?php include 'html/footerAlbum.php'; ?>