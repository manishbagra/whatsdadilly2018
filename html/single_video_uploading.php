<link rel="stylesheet" href="upl_vid/css/video_uploading.css" /> 
<link rel="stylesheet" href="upl_vid/css/nice-select.css" /> 
<link rel="stylesheet" href="upl_vid/css/jquery.tag-editor.css" />    
<link rel="stylesheet" href="upl_vid/css/main.css" /> 

<style>
.more-less-btn{text-align:left;}
</style>

  <div class="overlay videos"> 
   <div class="container total-wrap  another">
        <div class="row justify-content-center" style="margin:10% 0 0 10%;">
          <div class="col-lg-8 col-md-12 video-main-wrap">

              <div class="total-process-area row">
                <div class="col-lg-3 col-md-3 col-sm-12 tab-wrap no-padding left-wrap">
                    <h4>Uploaded 00 of 02 Videos</h4>
                    <p>
                      Your videos are still Uploading <br>
                      Please keep this page opne until it's done
                    </p>
                </div>   
                <div class="col-lg-9 col-md-9 col-sm-12 right-wrap no-padding">
                  <div class="form-group">
                     <div class="progress">
                      <div class="progress-bar" role="progressbar" aria-valuenow="70"
                      aria-valuemin="0" aria-valuemax="100" style="width:70%">
                        70%
                      </div>
                    </div>
                  </div>
                  <div class="form-group row align-items-center justify-content-end footer-group"> 
                    <div class="col-lg-2 bal2" >
                     <span>Set video as<span class="star">*</span>  </span> 
                    </div>
                    <div class="col-lg-3">                        
                      <select class="" id="state_id">
                      <option value="pp">Public</option>
                      <option value="pv">Private</option>
                      </select> 
                    </div>
                    <!-- Submit Button -->
                    <div class="col-lg-6 bbtn">
                      <button type="submit" class="btn btn-primary btn-black publish_All">Publish all</button>
                      <button type="submit" class="btn btn-primary btn-gray" >Cancel all</button>                              
                    </div>
                  </div>                   
                </div>
              </div>  

              <div id="video-content" class="video-main-area row">
         
              </div>
             
              
              </form>                         
            </div>
          </div>      
        </div>
    
  </div>  
<script src="upl_vid/js/jquery.nice-select.min.js"></script>   
<script src="upl_vid/js/jquery.caret.min.js"></script>
<script src="upl_vid/js/jquery.tag-editor.js"></script>
<script src="upl_vid/js/main.js"></script>

<script>$('select').niceSelect();</script>
