<?php include ("headerprofile.php");?>

<link href="css/FrndReqstyle.css" rel="stylesheet" type="text/css" />
<style>
.left_box{
	padding:5% 5% 5%;
}
.col-sm-12{
	margin-top:4.8%;
}
.left_box ul{
	font-weight:600;
	margin-left:1%;
}
</style>
<div class="outer">
	<div class="container">
		<div class="col-sm-12">
			<div class="left_box">
			
			<h1>Supported WhatsDadilly videos file formats</h1>
			
			<div class="intro cc">
			<br/><br/>
				<p>If you're not sure which format to save your video as or are getting&nbsp;an "invalid file format" error message when you're uploading, make sure that you’re using one of the following formats:</p>

				<ul>
				  <li>.MOV</li>
				  <li>.MPEG4</li>
				  <li>.MP4</li>
				  <li>.AVI</li>
				  <li>.WMV</li>
				  <li>.MPEGPS</li>
				  <li>.FLV</li>
				  <li>3GPP</li>
				  <li>WebM</li>
				  <li>DNxHR</li>
				  <li>ProRes</li>
				  <li>CineForm</li>
				  <li>HEVC (h265)</li>
				</ul>

				<p>If you're using a file format that's not listed above, use this troubleshooter to learn how to convert your file.</p>

				<p>For more advanced information on file formats you can read through this article on <a href="javascript:void(0)">encoding settings</a>.</p>
			
			</div>

			 
			</div>
		</div>
	</div>
</div>