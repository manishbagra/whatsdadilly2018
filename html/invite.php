<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <title>:WHATSDADILLY:</title>
<style>
.setting_dropdown{
	display:none;
}
.active{
	color: #000 !important;
}
</style>
<link rel="stylesheet" href="css/bootstrap.min.css" />  

<link href="css/registration-process.css" rel="stylesheet" />

 <!-- custom CSS -->
	<link href="css/Landingstyle.css" rel="stylesheet">
	<link href="font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="fonts/stylesheet.css" rel="stylesheet">

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/client.js"></script>
<script src="js/wl.js"></script>
<script src="js/yahoo-invite.js"></script>
<script src="js/yahoo.js"></script>
<script src="js/invite.js"></script>
<script>
	$(document).ready(function(){
		$("#frndrequestChk").change(function () {
			if($(this).prop("checked")){
				$("#sendFriendRequestfrm input:checkbox").prop('checked', $(this).prop("checked"));
				$('.requestcount').html(requestCount);
			}else{
				$("#sendFriendRequestfrm input:checkbox").prop('checked', $(this).prop("checked"));
				$('.requestcount').html(0);
			}
		});
		$(".inviteFrndChk").change(function () {
			if($(this).prop("checked")){
				$("#sendinvitation input:checkbox").prop('checked', $(this).prop("checked"));
				$('.invitationcount').html(invitationCount);
			}else{
				$("#sendinvitation input:checkbox").prop('checked', $(this).prop("checked"));
				$('.invitationcount').html( 0);
			}
		});
	});
	
</script>
</head>
<body>
 <?php include 'header.php'; ?>
            <!-- Modal content-->
            
                 <div class="cols-xs" style="margin-top:15%;">
                     <div class="StepProgresses nav nav-tabs">
                        <div class="Step1Process active" >
							<a data-toggle="tab" href="#step1" class="toggle-steps">                         
							   Step 1 <br/>
							   <small>Find Friends</small>
							</a>
                        </div>
                        <div class="Step2Process">
                           	<a data-toggle="tab" href="#step2" class="toggle-steps">                         
							   Step 2 <br/>
							   <small>Find Friends</small>
							</a>
						</div>
                        <div class="Step3Process">
							<a data-toggle="tab" href="#step3" class="toggle-steps">                         
								   Step 3 <br/>
								   <small>Find Friends</small>
							</a>
						</div>
                        <!--
						<div class="Step4Process">
                           Step 4 <br/>
                           <small>Find Friends</small>
                        </div>
						-->
                     </div>
                     <div class="tab-content">
					    <div id="step1" class="tab-pane fade in active">
							<div class="progress">
								<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 25%">
								   <span class="fright" style="margin-right:20px;"><strong>25%</strong> Complete</span>
								</div>
							 </div>
							 <p class="InviteYourFrieds"><i>Invite Your Frieds</i></p>
							 <div class="InviteFriendsInputs">
								<div class="gmailLogin ActiveLogin">
								   <span class="MailLoginIcon"><img src="rgimages/gmail-login-icon.png" class="Login-Social-Icon"/></span><span class="FormTitleText">Gmail</span> 
								   <form action="index.php" class="form-inline" role="form">
									  <button type="button" class="btn btn-default google">Find Friends</button>
								   </form>
								</div>
								<div class="Outlook-Hotmail">
								   <span class="MailLoginIcon"> 
								   <img src="rgimages/outlook-login-icon.png" class="Login-Social-Icon"/>
								   </span>
								   <span class="FormTitleText">Outlook (Hotmail) </span> 
								   <form class="form-inline" role="form">
									  <a href="#" id="import"><button type="button" class="btn btn-default">Find Friends</button></a>
								   </form>
								</div>
								<div class="YahooLogin">
								   <span class="MailLoginIcon"> <img src="rgimages/yahoo-login-icon.png" class="Login-Social-Icon"/></span>
								   <span class="FormTitleText">Yahoo! </span> 
								   <form class="form-inline" role="form">
									  <button type="button" class="btn btn-default" onclick="getFriends('yahoo', 'me/friends')">Find Friends</button>
								   </form>
								</div>
								<div class="OtherEmailService">
								   <span class="MailLoginIcon"> <img src="rgimages/other-email-icon.png" class="Login-Social-Icon"/></span>
								   <span class="FormTitleText">Other Email Service </span> 
								   <form class="form-inline" role="form">
									  <button type="submit" class="btn btn-default">Find Friends</button>
								   </form>
								</div>
								
							 </div>
							 <div class="progress-striped loader" style="min-height:200px;display:none;margin-top:100px;">
									<div class="bar" style="width: 20%; margin: 0px auto;"><img src="images/loadings.gif" alt="loading" ></div>
								</div>
							 <div class="SaveOrSkip">
								<div class="fright">
								<a href="signup2.php">
								<button type="button" class="skipstep">Skip Step</button>
								</a>
								<!--
								<a href="registration-process-step-2.html"><button type="button" class="btn btn-default">Save & Continue</button></a>
								-->
								</div>
							 </div>
						</div>
						<div id="step2" class="tab-pane fade contact">
							<div class="progress">
							   <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar progress-bar-warning">
								  <span style="margin-right:20px;" class="fright"><strong>50%</strong> Complete</span>
							   </div>
							</div>
							<div class="top_check_box"> <input type="checkbox" id="frndrequestChk" checked="checked" />  &nbsp;&nbsp;Select All/None</div>
							<div class="gmail_contact">
							   <h1 class="invite-source"><img src="images/email.png" /> Gmail contacts <a data-toggle="tab" href="#step1" >Try another service</a></h1>
							   <p style="width:100%;">Here are <span class="requestcount"> 0 </span> people for you to send friend request .You can
								  uncheck Select all or anyone you dont't want to follow.
							   </p>
							   <span style="display:none;">Invite Friends <input id="frndrequestChk" type="checkbox" /></span>
							</div>
							<form id="sendFriendRequestfrm" action="#" method="post">
								<div class="secrol">
								   <div class="no-contact">
									   <span>No friend to follow</span> 
									</div>
								</div>
								<div class="SaveOrSkip merge">
								   <span style=" padding-top:48px;vertical-align:middle;">
								   <a data-toggle="tab" href="#step3"><button type="button" class="skipstep">Skip Step</button></a>
								   </span> 
								   <div class="fright"> <button class="btn btn-default" type="button" onclick="sendFrientRequest()">Send Friend Request <span class="requestcount"> 0 </span> People</button></div>
								</div>
							</form>
						</div>
						<div id="step3" class="tab-pane fade contact">
							<div class="progress">
							   <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
								  <span class="fright" style="margin-right:20px;"><strong>50%</strong> Complete</span>
							   </div>
							</div>
							<div class="top_check_box"> <input type="checkbox" class="inviteFrndChk" checked="checked"  />  &nbsp;&nbsp;Select All/None</div>
							<div class="gmail_contact">
							   <h1 class="invite-source"><img src="images/email.png"> Gmail contacts <a  data-toggle="tab" href="#step1">Try another service</a></h1>
							   <p style="width:100%;">Here are <span class="invitationcount"> 0 </span> people for you to invite .You can
								  uncheck Select all or anyone you dont't want to follow.
							   </p>
							   <span style="display:none;">Invite <span class="invitationcount"> 0 </span> Friends <input type="checkbox" class="inviteFrndChk"></span>
							</div>
							<form id="sendinvitation" method="post" action="#" >
								<div class="contact-list">
									<div class="no-contact">
									   <span>No contact to invite</span> 
									</div>
									
								</div>
								<div class="invites_footer"><span class="invites">Invites will be sent in English (US) [<a href="#">change</a>]</span> <button class="button_ttp" type="button" onClick="SendInvitation()"> Invites <span class="invitationcount"> 0 </span> People</button>  
								<a href="signup2.php">
								<button type="button" class="skipstep" type="button" class="skipstep" >Skip</button>
								</a>
								</div>
							</form>
						</div>
					 </div>
                  </div>
                  <div class="FooterBelowHints">Keep all of your social outlets and email accounts in one convenient location for easy access and to ensure you won't miss a thing!</div>
				
</body>
</html>      