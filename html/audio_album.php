	<?php include 'html/headerAlbum.php'; ?>
	<link href="playlist/vendor/css/custom1.css" rel="stylesheet">
	<link href="playlist/vendor/css/jquery.mCustomScrollbar.css" rel="stylesheet">
	<link href="playlist/vendor/css/single_audio.css" rel="stylesheet">
	 
    <div class="container-fluid" style="padding-top:80px;">
	
        <div class="row">
		
            <?php include 'html/album_side_menu.php' ?>

            <main role="main" class="main-page ml-sm-auto pt-3 px-5">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center  main-content-top border-bottom">
                    <h3 class="main-title-heading">My Playlist</h3>
					
					<div class="alert alert-success" style="display:none;border-radius:0px;margin-bottom:0px;padding:0.4rem 1.25rem;">
						<strong>Success!</strong><a href="#" class="alert-link">Saved Album Successfully..!!</a>.
					</div>
					
					
                    <div class="btn-toolbar mb-md-0">
					
					
                        <div class="btn-group">
							
								
								<div class="upload-btn-wrapper">
								
									<button type="button" class="btn btn-secondary btn-sm add-btn"><i class="fas fa-plus"></i>Create Playlist</button>
									
								  
								</div>
							
                        </div>
                    </div>
                </div>
				
				
                <div class="row gutter-10">
                   <?php foreach($result_all as $key=> $result){ ?>
				   
                    <div class="col-lg-2 col-md-6 single-album"  onClick="playSingleTrack('<?php echo $result['file']; ?>','<?php echo $result['thumb']; ?>','<?php echo addslashes($result['title']); ?>','<?php echo addslashes($result['playlist_desc']); ?>')	;" data-toggle="modal" >
                        <div class="img-container">
                            <div class="thumb">
                                <img class="img-fluid" src="<?php echo $result['thumb']; ?>" alt="" >
                            </div>
                            <a class="edit-btn" href="#"><i class="fas fa-pencil-alt"></i></a>
                            <div class="detais">
                                <p class="left-part">
                                    <a href="#">Like</a>
                                    <a href="#">Comment</a>
                                </p>
                                <p class="right-part">
                                    <span><i class="fas fa-thumbs-up"></i> 27</span>
                                    <span><i class="fas fa-comment"></i> 27</span>
                                </p>
                            </div>
                        </div>
                        <a href="#"><h6 class="album-title"><?php echo $result['title']; ?></h6></a>
                    </div>
					
				   <?php }?>
                    
                </div>

            </main>
        </div>
    </div>
	
	
<div id="singleTrack" class="modal fade" role="dialog">

	<div class="player_body vertical-center" style='background-image: url("images/bg.png");'>
	
      
		<div class="player_content">
			<div class="column add-bottom">
				<div id="mainwrap">
					<div id="nowPlay">
						<h3 class="left" id="songTitle">All This Is</h3>
						<p class="right" style="overflow-y:hidden;float:left;margin:0px;" id="songAuther">by Joe L.'s Studio</p>
					</div>
					<div id="audiowrap">
						<div id="waveform" class='waveform' ></div>
					   <!-- <div id="time-total"></div> -->
						<span class="waveform__counter" id='counter'></span>
						<span class="waveform__duration" id='duration'></span>								
				
						<div class="custom_controls"> 
							<div class="volbox">
								<i id="toggleMuteBtn" class="fas fa-volume-up"></i>
								<input id="volume" min="0" max="1" value="0.1" step="0.1" type="range">
							</div>
							<a id="previous_btn" onclick="PrevchangeSong()" class="disable_class"><i class="fas fa-fast-backward"></i></a>
							<a id="backward"><i class="fa fa-backward"></i></a>
							<span id="playPause"><button class="play_btn" onclick="play()"><i class="fas fa-play"></i></button></span>
							<a id="forward"><i class="fa fa-forward"></i></a>
							<a id="next_btn" onclick="changeSong()"><i class="fas fa-fast-forward"></i></a>
							<a onclick="repeateFun()" id="repeateFun" class="repeat_off"><i class="fas fa-redo-alt"></i></a>
							<a onclick="shuffleOnOff()" id="shuffle"><i class="fa fa-random"></i></a>
						</div>
						
					</div>
				</div>
			</div>
			
		</div>
	</div><!-- End Player Body -->
  
</div>
	

	
	
	<!-- <script src="vendor/bootstrap/js/custom.js"></script> -->
        <script type="text/javascript" src="https://api.html5media.info/1.1.8/html5media.min.js"></script>
        <script type="text/javascript" src="playlist/vendor/bootstrap/js/plyr.js"></script>
        <!-- <script src="vendor/bootstrap/js/player.js"></script> -->
        <script src="playlist/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="playlist/vendor/js/jquery.mCustomScrollbar.concat.min.js"></script>
	    <script src="playlist/js/playlist.js"></script>
	<?php include 'html/footerAlbum.php'; ?>