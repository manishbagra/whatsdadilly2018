
        <div class="player" id="ap" >
            <div class="player_body">
                <div class="hamburger-menu">
                    <div class="bar"><span>Track List <i class="fas fa-sort-up"></i></span></div>
                </div>
                <div class="nav_menu" id="navMenu">
                    <div class="nav_list">
                        <div id="plwrap" >
                            <ul id="plList"></ul>
                        </div>
                    </div>
                </div>
                <div class="player_content">
                    <div class="column add-bottom">
                        <div id="mainwrap">
                            <div id="nowPlay">
                                <h3 class="left" id="songTitle"></h3>
                                <p class="right" id="songAuther"></p>
                            </div>
                            <div id="audiowrap">
                                <div id="waveform">
                                    <audio id="song" style="display: none" preload="false" src="http://api.soundcloud.com/tracks/45398925/stream?client_id=fa791b761f68cafa375ab5f7ea51927a"></audio>
                                </div>
                                <!-- <div id="time-total"></div> -->
                                <span class="waveform__counter"></span>
                                <span class="waveform__duration"></span>
                                <div class="custom_controls"> 
                                    <div class="volbox">
                                        <i id="toggleMuteBtn" class="fas fa-volume-up"></i>
                                        <input id="volume" type="range" min="0" max="1" value="0.1" step="0.1">
                                    </div>
                                    <a id="previous_btn" onclick="PrevchangeSong()"><i class="fas fa-fast-backward"></i></a>
                                    <a id="backward"><i class="fa fa-backward"></i></a>
                                    <span id="playPause">
                                        <button class="play_btn" onclick="play()">
                                            <i class="fas fa-play"></i>
                                        </button>
                                    </span>
                                    <a id="forward"><i class="fa fa-forward"></i></a>
                                    <a id="next_btn" onclick="changeSong()" ><i class="fas fa-fast-forward"></i></a>
                                    <a onClick="repeateFun()" id="repeateFun"><i class="fas fa-redo-alt"></i></a>
                                    <a onClick="shuffleOnOff()" id="shuffle"><i class="fa fa-random"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
