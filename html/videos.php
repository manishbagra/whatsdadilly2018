<?php include 'html/headerAlbum.php'; ?>
    <div class="container-fluid" style="padding-top:80px;">
        <div class="row">
          
		<?php include 'html/album_side_menu.php' ?>	
		
		

            <main role="main" class="main-page ml-sm-auto pt-3 px-5">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center  main-content-top border-bottom main-content-top-elements">
                    <h3 class="main-title-heading">Videos</h3>
                    <div class="btn-toolbar mb-md-0">
                        <div class="btn-group btns">
                            <div class="btn-group left-grp">
                                <button type="button" class="btn btn-secondary btn-sm add-btn"><i class="fas fa-plus"></i> Add more photos/videos</button>
                                <button type="button" class="btn btn-secondary btn-sm add-btn"><i class="fas fa-trash"></i> Delete Album</button>
                                <button type="button" class="btn btn-secondary btn-sm add-btn"><i class="fas fa-cog"></i> Manage</button>
                            </div>
							
							<div class="upload-btn-wrapper">
							
                            <button type="button" class="btn btn-secondary btn-sm add-btn upload-btn"><i class="fas fa-upload"></i> Upload</button>
                        
						    <form method="post" id="vid_up" enctype="multipart/form-data">
								<input name="files[]" class="video-upload-button" title="New Videos" id="video-upload-button" type="file" multiple />
								
								<input  type="hidden" id="imgN_array" name="imgN_array"/>
								<input  type="hidden" id="imgTN_array" name="imgTN_array"/>
								<input  type="hidden" id="vSrc_array" name="vSrc_array"/>
								<input  type="hidden" id="title_array" name="title_array"/>
								<input  type="hidden" id="descrip_array" name="descrip_array"/>
								<input  type="hidden" id="type_array" name="type_array"/>
								<input  type="hidden" id="tags_array" name="tags_array"/>
								<input  type="hidden" id="category_array" name="category_array"/>
								<input  type="hidden" id="vDesc_array" name="vDesc_array"/>
								
								<input type="submit"  class="formSubmit frmVidSub hide" name="submit" />
							</form>
							
						</div>	
						
						</div>
                    </div>
                </div>
				
						<div class="elementor-widget-container">

		<div id="5ab884825d316" class="gg_gallery_wrap gg_columnized_gallery gid_5501 ggom_103   ggom_vc_txt" data-gg_ol="103" data-col-maxw="200" rel="5501" data-nores-txt="No images found in this page">            	

        <div class="gg_loader" style="display: none;">

		<div class="ggl_1">

		</div>

		<div class="ggl_2">
		</div>

		<div class="ggl_3">
		</div>

		<div class="ggl_4">

		</div>

		</div>      

			<div class="gg_container">
				
			<?php foreach($result_all as $result): ?>

			<div data-gg-link="<?php echo URL.$result["file"]; ?>"

						class="gg_img 5ab884825d316-2 gg_shown" 

						data-gg-url="<?php echo URL.$result["thumbnail"]; ?>" 

						data-gg-title="<?php echo $result["title"]; ?>" 

						data-gg-author="<?php echo URL.$result["thumbnail"]; ?>" 
						
						data-gg-descr="<?php echo $result["description"]; ?>" 

						data-img-id="<?php echo $result["id"]; ?>" 

						rel="<?php echo $result["id"]; ?>" 

						style="width: 20%;">
						
				<div class="col-lg-12 single-video">
                        <div class="img-container">
                            <div class="thumb">
                               <?php if(!empty($result["thumbnail"])){ ?>
									<video style="width:100%;height:auto;"  id="<?php echo $result['file']; ?>" src=""   class="video-js  vjs-default-skin"  poster="<?php echo $result["thumbnail"]; ?>"  >
										<source src="" type="video/mp4" >
									</video>
								<?php }?>
                            </div>
                            <p class="time" style ="font-size:14px;">03:26</p>
                        </div>
                        <a href="javascript:void(0)" style="display:block !important;" >
                            <a href="javascript:void(0)" style="display:block !important;" ><h6 class="video-title" style="display:block !important;font-size:15px !important;text-transform:none !important;color: #222 !important;font-family: Roboto !important;font-weight: 600 !important;line-height: 1.2em !important;height:40px;" ><?php echo $session->substrword($result["title"], 40); ?></h6></a>
                        </a>
                        <a class="user-name" style="display:block !important;font-size:13px !important;" href="javascript:void(0)">NewsDudeperfect <i class="fas fa-check-circle"></i></a>
                        <div class="bottom-wrap">
                            <p>1.5M Views</p>
                            <p class="dates">02 days ago</p>
                        </div>
				</div>		
				
			</div>	
			
			
				
			<?php endforeach; ?>
			
		</div>	

	</div>
	
	</div>

            </main>
        </div>
    </div>
<?php include 'html/footerAlbum.php'; ?>	