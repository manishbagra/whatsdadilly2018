<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/m_style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	  <script type="text/javascript" src="js/jquery.easydrag.js"></script>
	  <script>
		$(document).ready(function() {
			$('.fancybox-outer').easydrag();
		});
    </script>
</head>
<body>

	<div id='main'>
		
		<div id='top'>
			<p id='heading'>Thanks for join WhatsDaDilly?</p>
			<p><small>to get started add your social network accounts </small></p>
			<p><small>to get the full use of platform <b>(Highly Recommended)</b> </small></p>
		</div>

		<div id='bottom'>
			
			<div class='socialTabMenu'>
				<span>
					<span class='imgalert space'>
					<?php if($twitter_count != "") {?>
					<img src="img_m/image.png">
					<?php }else{ ?>
					<img src="img_m/rectangle_2.png">
					<?php } ?>
					</span>
                    <span class='social_img space'><img style='width:37px;border-radius:10px;' src="images/Twitter-icon.png"></span>
                    <span class="social_name space">Twitter</span>&nbsp;&nbsp;&nbsp;&nbsp;
                    <span class='social_btn space'><a href='<?php echo $twitter_url; ?>' class='btn-maan'>Authorize Access</a></span>
                    <span class='social_btn space'><button class='btn btn-default'>cancel</button></span>
				</span>
				
			</div>

			

			<div class='socialTabMenu'>
				<span>
					<span class='imgalert space'>
					
					<?php if($if_exit_insta != "") {?>
					<img src="img_m/image.png">
					<?php }else{ ?>
					<img src="img_m/rectangle_2.png">
					<?php } ?>
					
					</span>
                    <span class='social_img space'><img src="img_m/instagram_logo.png"></span>
                    <span class="social_name space">Instagram</span>
                    <span class='social_btn space'><a href='<?php echo $insta_login_url ?>' class='btn-maan'>Authorize Access</a></span>
                    <span class='social_btn space'><a class='btn btn-default'>cancel</a></span>
				</span>
				
			</div>

			<div class='socialTabMenu'>
				<span>
					<span class='imgalert space'>
					
					<?php if($ifExistYoutube != "") {?>
					<img src="img_m/image.png">
					<?php }else{ ?>
					<img src="img_m/rectangle_2.png">
					<?php } ?>
					
					</span>
                    <span class='social_img space'><img style='width:37px;border-radius:0px;' src="images/youtube.png"></span>
                    <span class="social_name space">Youube</span>&nbsp;&nbsp;&nbsp;
                    <span class='social_btn space'><a class='btn-maan'>Authorize Access</a></span>
                    <span class='social_btn space'><button class='btn btn-default'>cancel</button></span>
				</span>
				
			</div>

			<div class='socialTabMenu'>
				<span>
                    <span class='social_btn space lastbtn'><button class='btn-maan'>Save and continue</button></span>
                    <span class='social_btn space lastbtn'><button style='width:100px' class='btn btn-default'>Skip</button></span>
				</span>
				
			</div>


		</div>
		<div id='last'></div>



	</div>

</body>
</html>