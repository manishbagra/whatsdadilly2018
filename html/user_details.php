<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <div class="main_user_info">
			<div class="left-tabs">
				<ul class="nav nav-tabs text-center">
					<li><a data-toggle="tab" href="#home">Basic Information</a></li>
					<li><a data-toggle="tab" href="#menu1">About You</a></li>
					<li><a data-toggle="tab" href="#menu2">Work & Education</a></li>
					<li><a data-toggle="tab" href="#menu3">Contact Info</a></li>
				</ul>
			</div>
			<div class="right-tab">
				<div class="tab-content">
					<div id="home" class="tab-pane fade in active">
						<div class="basic-info">
							<p><i class="fa fa-calendar" aria-hidden="true"></i><label>Date of Birth</label><span class="dot">:</span><span><?php

							echo date('d F Y', strtotime($user_info[0]["dob"]));
							
							
							?></span></p>
							<p><i class="fa fa-intersex"></i><label>Gender</label><span class="dot">:</span><span><?php echo $user_info[0]['gender']; ?></span></p>
							<p><i class="fa fa-home" aria-hidden="true"></i><label>Lives in</label><span class="dot">:</span><span><?php echo $cityName['name']; ?></span></p>
							<p><i class="fa fa-map-marker" aria-hidden="true"></i><label>Home</label><span class="dot">:</span><span><?php echo $stateName['name'].' , '.$countryName['name']; ?></span></p>
							<p><i class="fa fa-heart" aria-hidden="true"></i><label>Relationship</label><span class="dot">:</span><span>
							<?php 
							switch($user_info[0]['relationship']){
								case 1:
								$relation = "Single";
								break;
								
								case 2:
								$relation = "Married";
								break;
								
								case 3:
								$relation = "Divorced";
								break;
								
								case 4:
								$relation = "Engaged";
								break;
								
								case 5:
								$relation = "In Relationship";
								break;
								
								case 6:
								$relation = "Complicated";
								break;
								
								case 7:
								$relation = "Widowed";
								break;
								
								case 8:
								$relation = "Open Relationship";
								break;
							}
							echo $relation;
							
							?></span></p>
						<div class='half-content hide'>	
							<p><i class="fa fa-female" aria-hidden="true"></i><label>Interested in</label><span class="dot">:</span><span>
							<?php 
							switch($user_info[0]['interested']){
								case 1:
								$Interested = "Male";
								break;
								
								case 2:
								$Interested = "Female";
								break;
								
								case 3:
								$Interested = "Both";
								break;
							}
							echo $Interested;
							
							?>
							</span></p>
							<p><i class="fa fa-superpowers" aria-hidden="true"></i><label>Political Views</label><span class="dot">:</span>
							<span>
							<?php 
							switch($user_info[0]['politicalview']){
								case 1:
								$Interest = "Interested";
								break;
								
								case 2:
								$Interest = "Not Interested";
								break;
							}
							echo $Interest;
							
							?></span></p>
							<p><i class="fa fa-anchor" aria-hidden="true"></i><label>Religion</label><span class="dot">:</span><span>
							<?php 
							switch($user_info[0]['religion']){
								case 1:
								$religion = "Christianity";
								break;
								case 2:
								$religion = "Islam";
								break;
								case 3:
								$religion = "Hinduism";
								break;
								case 4:
								$religion = "Buddhism";
								break;
							}
							echo $religion;
							?>
							</span></p>
							<p><i class="fa fa-language" aria-hidden="true"></i><label>Languages</label><span class="dot">:</span><span>
							 
							<?php 
							switch($user_info[0]['language']){
								case 1:
								$language = "English";
								break;
								case 2:
								$language = "French";
								break;
								case 3:
								$language = "Spanish";
								break;
							}
							echo $language;
							?>
						</span></p>
							<p><i class="fa fa-key" aria-hidden="true"></i><label>Last Login</label><span class="dot">:</span><span><?php echo $user_info[0]['last_login']; ?></span></p>
						</div>
						</div>
						
					</div>
					<div id="menu1" class="tab-pane fade">
						<div class="basic-info about-section">
							<p><?php echo $user_info[0]['aboutyou']; ?></p>
						</div>
					</div>
					<div id="menu2" class="tab-pane fade">
					    <div class="basic-info">
						    <h1>Work</h1>
							<p><i class="fa fa-briefcase" aria-hidden="true"></i><label>Company</label><span class="dot">:</span><span><?php echo $work_edu['company']; ?></span></p>
							<p><i class="fa fa-user-circle" aria-hidden="true"></i><label>Position</label><span class="dot">:</span><span><?php echo $work_edu['position']; ?></span></p>
							<p><i class="fa fa-map-marker" aria-hidden="true"></i><label>City/Town</label><span class="dot">:</span><span><?php echo $work_edu['city']; ?></span></p>
							<div class='half-content hide'>
							<p><i class="fa fa-audio-description" aria-hidden="true"></i><label>Description</label><span class="dot">:</span><span><?php echo $work_edu['description']; ?></span></p>
							<div class="education">
								<h1>Education</h1>
								<p><i class="fa fa-university" aria-hidden="true"></i><label>College/University</label><span class="dot">:</span><span><?php echo $work_edu['college']; ?></span></p>
								<p><i class="fa fa-graduation-cap" aria-hidden="true"></i><label>High School</label><span class="dot">:</span><span><?php echo $work_edu['school']; ?></span></p>
							</div>
							</div>
						</div>
					</div>
					<div id="menu3" class="tab-pane fade">
						<div class="basic-info contact-info">
							<p><i class="fa fa-home" aria-hidden="true"></i><label>Address</label><span class="dot">:</span><span><?php echo $contact_info['address']; ?></span></p>
							<p><i class="fa fa-envelope" aria-hidden="true"></i><label>Email</label><span class="dot">:</span>
							<?php if(count($email === 2)){ 
							
							echo "<span>".$email[0]."<br>".$email[1]."</span>";
							
							}else{
								echo "<span>".$email[0]."</span>";
							}
							
							?>
							</p>
							<p><i class="fa fa-phone" aria-hidden="true"></i><label>Phone</label><span class="dot">:</span>
							
							<?php if(count($phone === 2)){ 
							
							echo "<span>".$phone[0]."<br>".$phone[1]."</span>";
							
							}else{
								echo "<span>".$phone[0]."</span>";
							}
							
							?>
							
							</p>
							<div class='half-content hide'>
							<p><i class="fa fa-map-marker" aria-hidden="true"></i><label>Home</label><span class="dot">:</span><span>Toronto Portugal</span></p>
							
							<p><i class="fa fa-globe" aria-hidden="true"></i><label>Websites</label><span class="dot">:</span>
							<?php if(count($web === 2)){ 
							
							echo "<span>".$web[0]."<br>".$web[1]."</span>";
							
							}else{
								echo "<span>".$web[0]."</span>";
							}
							
							?>
							</p>
						    </div> 
						</div>
					</div>
				</div>
				<div class="edit text-center" data-toggle="modal" data-target="#userDetails">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
			    </div>
				<div class="write text-center">
					<i class="fa fa-expand" aria-hidden="true"></i>
				</div>
				
		
			</div>
			
		</div>
		
		<!--modal-pop-up-start-->
		<div class="modal fade"  id="userDetails" role="dialog">
			<div class="modal-dialog">
			  <!-- Modal content-->
			<form id='update_userdetails' name='update_userdetails' action='javascript:void(0)'>
				<div class="modal-content change-modal">
					<div class="modal-header">
					<span id='upSuc' class='updMsg'>Updated successfully....!!!</span>
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
			<div class="left-tabs">
				<ul class="nav nav-tabs text-center">
					<li><a data-toggle="tab" href="#home1">Basic Information</a></li>
					<li><a data-toggle="tab" href="#menu4">About You</a></li>
					<li><a data-toggle="tab" href="#menu5">Work & Education</a></li>
					<li><a data-toggle="tab" href="#menu6">Contact Info</a></li>
				</ul>
			</div>
			
			<div class="right-tab-modal modal-height" style='height:500px !important;'>
				<div class="tab-content">
					<div id="home1" class="tab-pane fade in active">
						<div class="basic-info user-edit-info">
							<p><i class="fa fa-calendar" aria-hidden="true"></i><label>Date of Birth</label><span class="dot">:</span><span class="dob">
							<input type="text"name="dd"id="dd" value="<?php echo $date[2]; ?>" placeholder="dd">
							<input type="text"name="mm"id="mm" value="<?php echo $date[1]; ?>" placeholder="MM">
							<input type="text"name="yy"id="yy" value="<?php echo $date[0]; ?>" placeholder="YY">
							</span></p>
							
							<p><i class="fa fa-intersex"></i><label>Gender</label><span class="dot">:</span>
							<span class="rel">
							<select id='gender' name="gender" class="chosen select_box">
								<option value="Male" <?php if($user_info[0]['gender']=="Male"){ echo "selected"; }  ?> >Male</option>
								<option Value="Female" <?php if($user_info[0]['gender']=="Male"){ echo "selected"; }  ?>>Female</option>
							</select>
							
							</span>
							</p>
							
							<p><i class="fa fa-home" aria-hidden="true"></i><label>Lives in</label><span class="dot">:</span>
							
							<span>
							<input type='text' name='home_address' id='home_address' placeholder='Type an home address' />
							</span>
							
							</p>
							
							
							<p>
							<i class="fa fa-map-marker" aria-hidden="true"></i><label>Home</label><span class="dot">:</span>
							<span class="rel">
							
							
						
							<select name="country" id="country" class="chosen select_box">
								<option value="">Select Country</option>
									<?php for ($i=0;$i<count($country);$i++) {?>
									
										<option value="<?php echo $country[$i]['id']; ?>" <?php if($user_info[0]['current_country']==$country[$i]['id']){ echo 'selected';} ?> ><?php echo $country[$i]['name']; ?></option>
									
									<?php } ?>
							</select>
							
							
							
							<div class='choosen_dev' id='states'>
								<select name="state" id="state" class="chosen select_box">
										<option value="">Select Province</option>
										<?php foreach($state as $states): ?>
										<option value="<?php echo $states['id']; ?>" <?php if($user_info[0]['current_state']==$states['id']){ echo 'selected';} ?> ><?php echo $states['name']; ?></option>
										<?php endforeach; ?>
								</select>
							</div>
							
							<div class='choosen_dev' id='cities'>
								<select name="city" id="city" class="chosen select_box">
								
										<option value="">Select City</option>
										<?php foreach($city as $cities): ?>
										<option value="<?php echo $cities['id']; ?>" <?php if($user_info[0]['current_city']==$cities['id']){ echo 'selected';} ?> ><?php echo $cities['name']; ?></option>
										<?php endforeach; ?>
										
								</select>
							</div>
							
							</span>
							</p>
							
							<p>
							<i class="fa fa-heart" aria-hidden="true"></i><label>Relationship</label><span class="dot">:</span>
							<span class="rel">
							
							<select name="relationship" id="relationship" class="chosen select_box">
										<option value="1">Single</option>
										<option value="2">Married</option>
										<option value="3">Divorced</option>
										<option value="4">Engaged</option>
										<option value="5">In Relationship</option>
										<option value="6">Complicated</option>
										<option value="7">Widowed</option>
										<option value="8">Open Relationship</option>
							</select>
							
							</span></p>
							
							<p>
							<i class="fa fa-female" aria-hidden="true"></i><label>Interested in</label><span class="dot">:</span>
							<span class="rel">
							
								<select name="interested" id="interested" class="chosen select_box">
										<option value="1">Male</option>
										<option value="2">Female</option>
										<option value="3">Both</option>
								</select>
							</span>
							</p>
							
							
							<p>
							<i class="fa fa-superpowers" aria-hidden="true"></i><label>Political Views</label><span class="dot">:</span>
							
							<span class="rel">
							
							<select name="political" id="political" class="chosen select_box">
									<option value="1">Interested</option>
									<option value="2">Not Interested</option>
							</select>
								
							</span>
							
							</p>
							
							<p><i class="fa fa-anchor" aria-hidden="true"></i><label>Religion</label><span class="dot">:</span>
							<span class="rel">
								<select name="religion" id="religion" class="chosen select_box">
									<option value="1">Christianity</option>
									<option value="2">Islam</option>
									<option value="3">Hinduism</option>
									<option value="4">Buddhism</option>
								</select>
							</span>
							</p>
							
							<p><i class="fa fa-language" aria-hidden="true"></i><label>Languages</label><span class="dot">:</span>
							
								<span class="rel">
									<select name="language" id="language" class="chosen select_box">
										<option value="1">English</option>
										<option value="2">French</option>
										<option value="3">Spanish</option>
									</select>
								</span>
								
							</p>
							
						</div>
						
					</div>
					<div id="menu4" class="tab-pane fade">
						<div class="basic-info about-section">
						    <textarea name='about_you' id='about_you'><?php echo $user_info[0]['aboutyou']; ?></textarea>
						</div>
					</div>
					<div id="menu5" class="tab-pane fade">
					    <div class="basic-info">
						    <h1>Work</h1>
							<p><i class="fa fa-briefcase" aria-hidden="true"></i><label>Company</label><span class="dot">:</span><span><?php echo $work_edu['company']; ?></span></p>
							<p><i class="fa fa-user-circle" aria-hidden="true"></i><label>Position</label><span class="dot">:</span><span><?php echo $work_edu['position']; ?></span></p>
							<p><i class="fa fa-map-marker" aria-hidden="true"></i><label>City/Town</label><span class="dot">:</span><span><?php echo $work_edu['city']; ?></span></p>
							<p><i class="fa fa-audio-description" aria-hidden="true"></i><label>Description</label><span class="dot">:</span><span><?php echo $work_edu['description']; ?></span></p>
							<div class="education">
								<h1>Education</h1>
								<p><i class="fa fa-university" aria-hidden="true"></i><label>College/University</label><span class="dot">:</span><span><?php echo $work_edu['college']; ?></span></p>
								<p><i class="fa fa-graduation-cap" aria-hidden="true"></i><label>High School</label><span class="dot">:</span><span><?php echo $work_edu['school']; ?></span></p>
							</div>
						</div>
					</div>
					<div id="menu6" class="tab-pane fade">
						<div class="basic-info contact-info">
						<input type='hidden' name='user_id' id="user_id" value="<?php echo $user_info[0]['user_id']; ?>" >
								<p><i class="fa fa-home" aria-hidden="true"></i><label>Address</label><span class="dot">:</span><span><?php echo $contact_info['address']; ?></span></p>
								<p>
								<i class="fa fa-envelope" aria-hidden="true"></i><label>Email</label>
								<span class="dot">:</span>
								<span>
								
								<?php if($email[0] != ''){  ?>
								<span id='email1'><?php echo $email[0]; ?><button data="email1" type="button" class="close closeBtn">&times;</button></span>
								<?php }if($email[1] != ''){ ?>
								
								<?php if($email[0] != '' and $email[1] != ''){ echo '<br/>'; } ?>
								
								<span id='email2'><?php echo $email[1]; ?><button data="email2" type="button" class="close closeBtn">&times;</button></span><br class='add_email1' style='display:none;' >
								
								<?php } ?>
								<span class='add_email1' style='display:none;'><input name="add_email1" disabled id="add_email1" value="" placeholder="Type primary email" type="email"></span>
								<br class='add_email2' style='display:none;' ><span class='add_email2'  style='display:none;'><input name="add_email2" disabled id="add_email2" value="" placeholder="Type secondary email" type="email"></span>
								</span>
								
<?php if($email[0] != '' && $email[1] != '' ){  }else{  echo '<span data="add_email" class="next addIndex" id="addEmail"><i class="fa fa-plus" aria-hidden="true"></i> Add another email</span>';  } ?>

<span data="add_email" style='display:none;' class="next addIndex" id="addEmail"><i class="fa fa-plus" aria-hidden="true"></i> Add another email</span>
													
								</p>
								
								<p><i class="fa fa-phone" aria-hidden="true"></i><label>Phone</label><span class="dot">:</span>
                                <span>
								<?php if($phone[0] != ''){  ?>
								<span id='phone1'><?php echo $phone[0]; ?><button data="phone1" type="button" class="close closeBtn">&times;</button></span>
								<?php }if($phone[1] != ''){ ?>
								
								<?php if($phone[0] != '' and $phone[1] != ''){ echo '<br/>'; } ?>
								
								<span id='phone2'><?php echo $phone[1]; ?><button data="phone2" type="button" class="close closeBtn">&times;</button></span><br class='add_phone1' style='display:none;'>
								
								<?php } ?>
								<span class='add_phone1' style='display:none;'><input name="add_phone1" disabled id="add_phone1" value="" placeholder="Type primary number" type="text"></span>
								<br class='add_phone2' style='display:none;'><span class='add_phone2' style='display:none;'><input name="add_phone2" disabled id="add_phone2" value="" placeholder="Type secondary number" type="text"></span>
								
								</span>
                               
<?php if($phone[0] != '' && $phone[1] != '' ){ }else{ echo '<span data="add_phone" class="next addIndex" id="addPhone"><i class="fa fa-plus" aria-hidden="true"></i> Add another phone number</span>'; } ?>
<span data="add_phone" class="next addIndex" style='display:none;' id="addPhone"><i class="fa fa-plus" aria-hidden="true"></i> Add another phone number</span>								
								</p>
								<p><i class="fa fa-globe" aria-hidden="true"></i><label>Websites</label><span class="dot">:</span>
								<span>
								
								<?php if($web[0] != ''){  ?>
								<span id='web1'><?php echo $web[0]; ?><button type="button" data="web1" class="close closeBtn">&times;</button></span>
								<?php }if($web[1] != ''){ ?>
								<?php if($web[0] != '' and $web[1] != ''){ echo '<br/>'; } ?>
								
								<span id='web2'><?php echo $web[1]; ?><button type="button" data="web2" class="close closeBtn">&times;</button></span>
								<br class='add_web1' style='display:none;'>
								<?php } ?>
								
								<span class='add_web1' style='display:none;'><input name="add_web1" disabled id="add_web1" value="" placeholder="Type primary website" type="text"></span>
								<br class='add_web2' style='display:none;'><span class='add_web2' style='display:none;'><input name="add_web2" disabled id="add_web2" value="" placeholder="Type secondary website" type="text"></span>
								
								</span>
<?php if($web[0] != '' && $web[1] != '' ){ }else{ echo '<span data="add_web" class="next addIndex" id="addWeb" ><i class="fa fa-plus" aria-hidden="true"></i> Add another Website</span>'; } ?>
<span data="add_web" class="next addIndex" style='display:none;' id="addWeb" ><i class="fa fa-plus" aria-hidden="true"></i> Add another Website</span>								
						</div>
					</div>
				</div>
			
			</div>
					</div>
					
					<div class="button-div pull-right">
						<button type="button" class="btn btn-default cancel">Cancel</button>
						<button type="button" id='saveUserDeatils' class="btn btn-default save-but">Save Changes</button>
				    </div>

				</div>
				
			</form>	
				
			</div>
		</div>