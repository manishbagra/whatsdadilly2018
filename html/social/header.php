<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title> Social media feed </title> 
	<!-- google fonts --> 
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
	
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700,900" rel="stylesheet"> 
 	<!-- font awsome icon --->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" /> 
	
	<link rel="stylesheet" href="css/social_feed_css/slicknav.css" />  
	
    <!-- Bootstrap -->
    <link href="css/social_feed_css/bootstrap.min.css" rel="stylesheet">
	
    <link href="css/social_feed_css/style.css" rel="stylesheet">
	
    <link href="css/social_feed_css/responsive.css" rel="stylesheet">
	
	<link href="css/social_feed_css/profile.css" rel="stylesheet">
	
	<link href="css/social_feed_css/profilestyle.css" rel="stylesheet">
	
	<link href="css/social_feed_css/video-js.css" rel="stylesheet"/>
	
	<link rel="stylesheet" type="text/css" href="highslide/highslide.css"/>
	
	<style>
	   .notshow{

                display:none;

              }
	   .notActshow{

                display:none;

            }
	</style>
	
  </head>
  <body style='background-color:#FFF !important;font-family:lato !important;'>
     
	<div class="section" id="header_top_section">
		<div class="container space">
			<div class="row space"> 
				<div class="col-lg-7 col-md-6 col-sm-5">
					<div class="col-md-1 col-sm-3">
						<div class="question_icon">
							<div class="single_icon">
								<i class="fa fa-question"></i>
							</div>
						</div>
					</div>
					<div class="search_Box">
						<form action="">
							<fieldset class="field-container">
							  <input type="text" placeholder="Search..." class="field" />
							  <div class="icons-container">
								<div class="icon-search"></div>
								<div class="icon-close">
								  <div class="x-up"></div>
								  <div class="x-down"></div>
								</div>
							  </div>
							</fieldset>
						</form>
					</div>
				</div>
				<div class="col-lg-offset-2 col-lg-1 col-md-offset-1 col-md-2 col-sm-2">
					<div class="user_menu">
						<div class="user_box pull-right">
						<a href='profile.php?profileid=<?php echo base64_encode($_SESSION['userid']); ?>'>
							<?php if (count($session->getSession("profile_pic")) != 0) { ?>

								<img src="uploads/<?php echo $session->getSession("profile_pic"); ?>" alt=""  style="width:45px;"/>

								<?php } else { if ($session->getSession("gender") == 'Male') { ?>

									<img src="uploads/default/Maledefault.png" alt=""  style="width:42px;height:51px;"/>

									<?php } else { ?>

										<img src="uploads/default/female.jpg" alt=""  style="width:42px;height:51px;"/>

							<?php } } ?>
							
							</a>
							
							<p><?php echo ucfirst($session->getSession("firstname")); ?> <br /> <?php echo ucfirst($session->getSession("lastname")); ?></p> 
							
							
						</div> 
					</div>
				</div>
				<div class="col-lg-2 col-md-3 col-sm-4">
					<div class="maan_nav" >
					<?php

						$result = NotificationModel::getNotifications($entityManager, $session->getSession('userid'));
						
						//$messages->updateNotifications($entityManager, $session->getSession('userid'));
						
						$countNotification = NotificationModel::countNotificationByUser($entityManager, $session->getSession('userid'));
						
						
						?>

                         
						 
						 <style>
						   .notification_show:hover .n_dropdown  {  
						     display: none;
							}
						 </style>
						<ul>
						    
							<li><a class="active" href=""><i class="fa fa-home"></i></a></li>
						
							<li id='noti_1_main' style="cursor:pointer" class="notification_show"> 	
						
							<span><i class="fa fa-bell-o" aria-hidden="true"></i></span>
                          
						  <?php //if($countNotification != 0){ ?>
							
							<img id='red_simble_1' class="notification_red_simble" <?php //if($countNotification==0){ ?> style='display:none;' <?php //} ?> src="img/icons/notification.png" alt="" />
                            
							<div id='n_n_f_1' style='display:none;' class="notification_numbar first_n_number">

								<span class="notification_number n_n_f"><?php echo $countNotification;  ?></span>	

							</div>	
						
                           	<?php //} ?>				

							<ul style='display:none !important;' id='noti_1' class="n_dropdown">

								<li class="d_f_n">

									<img class="icon_top" src="img/icons/top_move.png" alt="" />

									<div class="dropdown_notification">

										<div class="d_notification_title">

											<p>Notifications <span> Mark All as Read</span></p>

										</div>											

										<div style="max-height:450px;overflow-x:hidden;overflow-y:scroll;

										/*border:solid*/

										" id="shownotification">

										<?php 

										if(count($result)){

											$var = '';

											$i = 0;

											$bg = '';
											
											foreach ($result as $item) {
											$profile_pic='uploads/'.$item['profile_pic'];
													if($item['profile_pic']==''){
															$profile_pic="uploads/default/Maledefault.png";
															
													}
												$bg = $i % 2 == 0 ? " nobg" : "";

												$var ='';

												$var .= ' <div class="beeperNub"></div> 
												
												<div class=" notificationwrap' . $bg . '" id="notification_'.$item['id_notifications'].'">

												<div class="notification_content">

													<img src="' . $profile_pic . '" alt="Image Friend" class="dimg friendrequestimg" onclick="var url = \'profile.php?profileid=' . base64_encode($item['id_friend'] ). '\';$(location).attr(\'href\',url); " style="width:60px;height:60px;"/>  

													<div class="notification_person">

														<!--<img class="n_image_right" src="img/notification/notificatin2.png" alt="">-->

														<p>'. $item['message'] . '</p>

														';						

														$var .= '<p class="notificationtime"></p></div></div></div>';



														$i++;

														echo $var;

													}

												}			

												?>

											</div>										



											<div class="d_notification_dropdown">

												<a href="#"><span>See All</span></a>

											</div>

										</div>

									</li>

								</ul>

							</li>
							
							<li><a href=""><i class="fa fa-users"></i></a></li>
							<li><a href=""><i class="fa fa-cog"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="side_menu">
		<nav class="navbar navbar-inverse sidebar" role="navigation">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
 				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
					<ul class="nav navbar-nav">
 						<li><a href=""><span>Streams</span></a><img src="img/social_img/c1.png" alt="" /></li> 
 						<li><a href=""><span>Publishers</span></a><img src="img/social_img/c2.png" alt="" /></li> 
 						<li><a href=""><span>Analytics</span></a><img src="img/social_img/c3.png" alt="" /></li> 
 						<li><a href=""><span>Assignments</span></a><img src="img/social_img/c4.png" alt="" /></li> 
 						<li><a href=""><span>Campaigns</span></a><img src="img/social_img/c5.png" alt="" /></li> 
 						<li><a href=""><span>App Directories</span></a><img src="img/social_img/c6.png" alt="" /></li> 
 						<li><a href=""><span>Tools</span></a><img src="img/social_img/c8.png" alt="" /></li> 
 						<li><a href=""><span>Help</span></a><img src="img/social_img/c7.png" alt="" /></li> 
					</ul>
				</div>
			</div>
		</nav>
	</div>
	
	<section id="main_wrapper">
	
	
	<section id="upgarde_option">
		<div class="container space">
			<div class="row space">
				<div class="col-md-3 col-sm-4">
					<div class="upgrade_input">
						<form action="">
							<span class="custom-dropdown big">
								<select>    
									<option value="">Send to <span class="pull-right">0 </span></option>
								</select>
							</span>
						</form>
					</div>
				</div>
				<div class="col-md-4 col-sm-5">
					<div class="compose_message">
						<input type="text" class="testrs" placeholder="Compose message" value="Compose message" />
						<label class="compose" for="">
							<ul>
								<li><img src="img/social_img/11.png" alt="" /></li>
								<li><img src="img/social_img/12.png" alt="" /></li>
								<li><img src="img/social_img/13.png" alt="" /></li>
								<li><img src="img/social_img/15.png" alt="" /></li>
								<li><img src="img/social_img/14.png" alt="" /></li>
							</ul>
						</label>
					</div>
				</div>
				<div class="col-md-offset-3 col-md-2 col-sm-2">
					<div class="upgard_button">
						<a href=""><input type="button" value="Upgrade my plan" name="Upgrade my plan" /></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="tab_area">
		<div class="container space">
			<div class="row space">
				<div class="tab_heaser">
					<div class="col-md-2 col-sm-5">
						<div class="col-md-6 col-sm-4"> 
							<div class="active_tab">
								<div class="active_tabs_add">
									<p>New tab</p>  <img src="img/social_img/close.png" alt="" />
								</div>
							</div>
						</div>
						<div class="col-md-1 col-sm-4">
							<div class="add_new_panl">
								<span><img src="img/social_img/plus.png" alt="" /></span>
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="full_witdh_bg"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="window_details">
					<div class="col-md-6">
						<div class="left_box">
							<div class="box"><img src="img/social_img/refress.png" alt="" /></div>
							<div class="box"><img src="img/social_img/drop.png" alt="" /></div>
							<div class="add_strem">
								<button class="btn btn-add"><img src="img/social_img/plus.png" alt="" /> Add Strem</button>
								<a href="<?php echo $youtube_auth_url; ?>" class="btn btn-add"><img src="img/social_img/plus.png" alt="" />  Add Social Network</a>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="right_box">
							<div class="box"><i class="fa fa-save"></i></div>
							<div class="box"><i class="fa fa-bars"></i></div>
							<div class="box"><i class="fa fa-arrows"></i></div>
						</div>
					</div>
				</div>
			</div>
<div class="row space" id="draggablePanelList2">