<style> 
#rcorners1 {
    border-radius: 25px;
    background: #73AD21;
    padding: 20px; 
    width: 200px;
    height: 150px;    
}

#rcorners2 {
    border-radius: 25px;
    border: 2px solid #73AD21;
    padding: 20px; 
    width: 200px;
    height: 150px; 
    background-repeat: no-repeat;
	background-size:100% 100%;
}

#rcorners3 {
    border-radius: 25px;
    background: url(paper.gif);
    background-position: left top;
    background-repeat: repeat;
    padding: 20px; 
    width: 200px;
    height: 150px;    
}
</style>

<?php foreach($subs_info->items as $subscribed){

$channel_id = $subscribed->snippet->resourceId->channelId;

$subsc_count_url = 'https://www.googleapis.com/youtube/v3/channels?part=statistics&id='.$channel_id.'&fields=items/statistics/subscriberCount&key='.$API_key.'';

$subscribed_data = $instaObj->curl_output($subsc_count_url);

$subscribed_counts = $subscribed_data->items[0]->statistics->subscriberCount

	?>
<div style='width:100%;height:160px;margin-bottom:60px;margin-top:16px;'>
<p id="rcorners2" style='background: url(<?php echo $subscribed->snippet->thumbnails->medium->url;  ?>);background-repeat:no-repeat;background-size:100% 100%;float:left;'></p>
	<div style='width:60%;height:160px;float:right;'>
	
	<p><strong><?php echo $instaObj->time_elapsed_string($subscribed->snippet->publishedAt,$full=false); ?></strong></p>
	<p><?php echo $subscribed->snippet->title; ?></p>
	<p><b>Subscribed : </b><?php echo number_format($subscribed_counts); ?></p>
	<p><?php
     //$pos=strpos($subscribed->snippet->description, ' ', 200);
      
	  echo substr($subscribed->snippet->description,0,200 );
	  
	  ?></p>
	</div>
</div>
<?php } ?>