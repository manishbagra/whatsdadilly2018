<?php

if($_SERVER["REQUEST_METHOD"] == "POST"){
    if(isset($_FILES["profile_pic"]) && $_FILES["profile_pic"]["error"] ==0 ){
        $allow = array(
            "jpg"=> "image/jpg",
            "JPG"=> "image/JPG",
            "jpeg"=> "image/jpeg",
            "JPEG"=> "image/JPEG",
            "gif"=> "image/gif",
            "GIF"=> "image/GIF",
            "png"=> "image/png",
            "PNG"=> "image/PNG"
        );
        $fname = $_FILES["profile_pic"]["name"];
        $ftype = $_FILES["profile_pic"]["type"];
        $fsize = $_FILES["profile_pic"]["size"];

        $extension = pathinfo($fname, PATHINFO_EXTENSION);

        if(!array_key_exists($extension, $allow)) 
            die("Error: Please select a valid file format.");

        $maximumsize = 5 * 1024 * 1024;

        if($fsize > $maximumsize) 
            die("Error: File size cross the maximum limit.");

        $profile_pic = $newfilename = time() . '.'. $extension;

        echo($newfilename);

        if(in_array($ftype, $allow)){
            if(file_exists("uploads/" .$newfilename)){
                echo $newfilename ."is already exists";
            } else{
                move_uploaded_file($_FILES["profile_pic"]["tmp_name"],"uploads/".$newfilename);
                echo "Your file was uploaded successfully.";
                // header("location:index.html");
                // die();
            }
        } else{
            echo "Error: There was a problem uploading your file, Please try again";
        }
    } 
    // else{
    //     echo "Error: ".$_FILES["profile_pic"]["error"];
    // }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once "bootstrap.php";
require_once 'model/Signup.php';
require_once 'model/Twitter.php';
require_once 'classes/Session.class.php';
$session = new Session();
if ($session->getSession("userid") != "" || $session->getSession("userid") != null) {
    if (isset($_POST['email']) && $_POST['email'] != "" && isset($_POST['firstname']) && $_POST['firstname'] != "") {
        // print_r($_POST);
        $data = array(
        	"firstname" => $_POST['firstname'],
            "lastname" => $_POST['lastname'],
            "email" => $_POST['email'],
            "profile_pic" => isset($profile_pic) ? $profile_pic : $_POST['oldpic'] ,
            "orginalpic" => isset($profile_pic) ? $profile_pic : $_POST['oldpic'],
            "user_id" => $session->getSession("userid")
        );
     	print_r($data);
        $result = Signup::profileEditPrimaryInfo($data, $entityManager);
        // print_r($result . ' - ' . 'tru');
    }

    if (isset($result) && $result == true) {
    	
    	echo json_encode(['success' => true, 'message' => 'Profile updated']);
    }
    else {
    	echo json_encode(['success' => false, 'message' => 'Profile not updated']);
    }

} else {
    echo json_encode(['success' => false, 'message' => 'Please you are not logged in']);
}
?>