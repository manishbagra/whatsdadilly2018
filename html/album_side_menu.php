<nav class="col-lg-2 col-md-3 d-none d-md-block sidebar-bg sidebar" id="sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column left-nav main-nav">
                        <div class="search-form2 d-flex flex-row mb-20">
                            <input class="form-control form-control-dark w-100" placeholder="Search" aria-label="Search" type="text">
                            <button type="submit" class="search-btn"><i class="fa fa-search"></i></button>
                        </div>
						<?php
						$projectWDD = "/WDDSocials";
						$base_uri = $_SERVER['REQUEST_URI'];
						?>
                        <li class="nav-item">
                            <a class="nav-link <?php if($base_uri == "/album.php"){ echo "active" ;} ?>" href="album.php">
                                <img src="album/img/folder-ico.png" alt=""> Albums <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php if($base_uri == "/posted_photos.php"){ echo "active" ;} ?>" href="posted_photos.php">
                                <img src="album/img/gallery-ico.png" alt="">Posted Photos
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php if($base_uri == "/videos.php"){ echo "active" ;} ?>" href="videos.php">
                                <img src="album/img/video-ico.png" alt="">Videos
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php if($base_uri == "/audio_album.php"){ echo "active" ;} ?>" href="audio_album.php">
                                <img src="album/img/playlist-ico.png" alt="">My Playlist
                            </a>
                        </li>
                    </ul>
                    <h5 class="mt-50 social-menu-title">Social Media</h5>
                    <ul class="nav flex-column left-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <img src="album/img/instra.png" alt=""> Instagram <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <img src="album/img/tum.png" alt="">Tumblr
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <img src="album/img/pin.png" alt="">Pinterest
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <img src="album/img/youtube.png" alt="">Youtube
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>