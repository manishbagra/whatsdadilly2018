<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>:WHATSDADILLY:</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/cropper.min.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/reset-min.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<link href="css/bootstrap-theme.min.css" rel="stylesheet">
	<link href="css/registration-process1.css" rel="stylesheet">
	<link href="css/cropcustom.css" rel="stylesheet">
	<script src="js/jquery-pack.js" type="text/javascript"></script>
	<script src="js/jquery.js" type="text/javascript"></script>
	
	<script src="js/fileuploader.js" type="text/javascript"></script>
	<script src="js/jquery.imgareaselect.min.js" type="text/javascript"></script>
	<script src="js/cropscript.js" type="text/javascript"></script>
  
  <!-- custom CSS -->
	<link href="css/Landingstyle.css" rel="stylesheet">
	<link href="font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="fonts/stylesheet.css" rel="stylesheet">
	

</head>
<body>
 <?php include 'header.php'; ?>
  <div class="cols-xs" id="crop-avatar" style="margin-top:15%;">
  
    <div class="StepProgresses">
		<div class="Step1Process">
			Step 1 <br/>
			<small>Invie Your Friends</small>

		</div>
		<div class="Step2Process">
			Step 2 <br/>
			<small>Invie Your Friends</small>

		</div>
		<div class="Step3Process">
			Step 3 <br/>
			<small>Invie Your Friends</small>

		</div>
		<div class="Step4Process">
			Step 4 <br/>
			<small>Invie Your Friends</small>

		</div>

	</div>

	<div class="progress">
		<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 75%">
			<span class="fright" style="margin-right:20px;"><strong>75% </strong>Complete</span>
		</div>
	</div>
	<p class="InviteYourFrieds"><i>Add Your Profile Picture</i></p>
    <!-- Current avatar -->
	<div class='row'>
	<div class="col-lg-6" id="crop-section" style="display:none">
		<img src="" id="thumbnail" alt="Create Thumbnail" />
		<div id="thumb_preview_holder">
			<img src=""  alt="Thumbnail Preview" id="thumb_preview" />
		</div>
		<div>
			<input type="hidden" name="filename" value="" id="filename" />
			<input type="hidden" name="x1" value="" id="x1" />
			<input type="hidden" name="y1" value="" id="y1" />
			<input type="hidden" name="x2" value="" id="x2" />
			<input type="hidden" name="y2" value="" id="y2" />
			<input type="hidden" name="w" value="" id="w" />
			<input type="hidden" name="h" value="" id="h" /><br>


		</div>
	</div>
	<div class="col-lg-6" id="uploader-section">
		
		<div class="product_image avatar-view"  title="Change the avatar">
		 <?php if ($session->getSession("sign_gender") == 'Male') {
													?>
			<img src="uploads/default/Maledefault.png" id='male' class="thumbnails" />
		  <?php } else { ?>
			<img src="uploads/default/female.jpg" id='female' class="thumbnails" />
		  <?php } ?>
		
			
		</div>
		<button id="upload" style='width:220px !important;height:40px !important;margin-top:0px;margin-left:20px;margin-bottom:15px;' class="btn btn-success avatar-view">
				<span class="glyphicon glyphicon-plus"></span>&nbsp;
				<span class="upload">Change profile picture</span>
		</button>
		
	</div>
		
	
	</div>
	
	<div class="SaveOrSkip merge">
	<span style="margin-left:20px; padding-top:20px;vertical-align:middle;">
	<a href="" style="color:#cccccc;"> 
	<img src="rgimages/back-icon.png"> Back</a>
	</span> 
	<div class="fright">or 
	<a href="javascript:void(0)" onclick="skipStep()">Skip This Step</a>
    <a href="#">
    <input type="submit" class="btn btn-primary button" name="upload_thumbnail" value="Save & Continue" onclick="skipStep()" id="save_thumb" /></a>
	</div>
	</div>

    <!-- Cropping modal -->
    <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
      <div class="modal-dialog modal-lg">
        <div class="modal-content" style="margin-top:40%;">
          <form class="avatar-form" action="crop.php" enctype="multipart/form-data" method="post">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" id="avatar-modal-label">Change Avatar</h4>
            </div>
            <div class="modal-body">
              <div class="avatar-body">

                <!-- Upload image and data -->
                <div class="avatar-upload" style='width:50%;float:left;'>
                  <input type="hidden" class="avatar-src" name="avatar_src">
                  <input type="hidden" class="avatar-data" name="avatar_data">
                  <label for="avatarInput">Local upload</label>
                  <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
				  
                </div>
				
				<div class="col-md-3">
					<i class="fa fa-search-minus fa-3" id="zoomout" aria-hidden="true"></i>
					  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<i class="fa fa-search-plus fa-3" id="zoomin" aria-hidden="true"></i>
					</a>
				</div>
				
				

                <!-- Crop and preview -->
                <div class="row">
                  <div class="col-md-12">
                    <div class="avatar-wrapper"></div>
                  </div>
                  <!--<div class="col-md-3">
                    <div class="avatar-preview preview-lg"></div>
                    <div class="avatar-preview preview-md"></div>
                    <div class="avatar-preview preview-sm"></div>
                  </div>-->
                </div>

                <div class="row avatar-btns">
                  <div class="col-md-9">
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary" data-method="rotate" data-option="-90" title="Rotate -90 degrees">Rotate Left</button>
                      <button type="button" class="btn btn-primary" data-method="rotate" data-option="-15">-15deg</button>
                      <button type="button" class="btn btn-primary" data-method="rotate" data-option="-30">-30deg</button>
                      <button type="button" class="btn btn-primary" data-method="rotate" data-option="-45">-45deg</button>
                    </div>
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary" data-method="rotate" data-option="90" title="Rotate 90 degrees">Rotate Right</button>
                      <button type="button" class="btn btn-primary" data-method="rotate" data-option="15">15deg</button>
                      <button type="button" class="btn btn-primary" data-method="rotate" data-option="30">30deg</button>
                      <button type="button" class="btn btn-primary" data-method="rotate" data-option="45">45deg</button>
                    </div>
                  </div>
				  
				  <div class="col-md-3">
                    <button type="submit" class="btn btn-primary btn-block avatar-save">Done</button>
                </div>
                  
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div><!-- /.modal -->

    <!-- Loading state -->
    <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
	
  </div>



<!-- include Stylesheet -->
<link href="css/demo-style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-theme.min.css" rel="stylesheet">

 


  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="cropjs/cropper.min.js"></script>
  <script src="cropjs/main.js"></script>
  <script type="text/javascript">


                                function skipStep(){
									var image_src = $(".thumbnails").attr('src');
									
									if((image_src == "uploads/default/Maledefault.png" || image_src == "uploads/default/female.jpg" )){
									var action = "noimage";	
									}else{
										var action = "yesimage";
									}
									//alert(action);
                                    $.ajax({
                                        type : 'POST',
                                        url: "crop_script.php",
                                        data: {image_src : image_src,action:action},
                                        success: function(data){
											//alert(data);
                                            window.location.href = "home.php";
                                        }
                                    });
                                }
                                //$(document).ready(function(){

                                   // $('#save_thumb').click(function() {
										//window.location.href = "home.php";
                                       // $("#multiform").submit();

                                        //                    var formdata =  new FormData($(this)[0]);
                                        //
                                        //                    $.ajax({
                                        //                        type: "POST",
                                        //                        url: "upload.php",
                                        //                        mimeType:"multipart/form-data",
                                        //                        data: formdata,
                                        //                        contentType: false,
                                        //                        cache: false,
                                        //                        processData:false,
                                        //
                                        //                        success: function(){alert('success');}
                                        //                    });
                                   // });

                                //});
                            </script>
							<script>
	  $(document).ready(function() {
		
		$('#zoomout').on('click', function() {
			//alert($('div.cropper-canvas').left());
			
			
			
			var imagewidth = $('div.cropper-canvas img').width() - 100;
			
			$('div.cropper-canvas img').width(imagewidth);
			$('div.cropper-canvas').width(imagewidth);
			
			var imageheight = $('div.cropper-canvas img').height() - 50;
			
			$('div.cropper-canvas img').height(imageheight);
			$('div.cropper-canvas').height(imageheight);
			
		});

		$('#zoomin').on('click', function() {
			
			var imagewidth = $('div.cropper-canvas img').width() + 100;
			
			$('div.cropper-canvas img').width(imagewidth);
			$('div.cropper-canvas').width(imagewidth);
			
			var imageheight = $('div.cropper-canvas img').height() + 50;
			
			$('div.cropper-canvas img').height(imageheight);
			$('div.cropper-canvas').height(imageheight);
		});
		
	  });
	</script>
</body>
</html>
