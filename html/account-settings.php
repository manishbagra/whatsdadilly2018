<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>:WHATSDADILLY:</title>

    <link rel="stylesheet" href="css/reset-min.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>

    <link href="css/bootstrap.min.css" rel="stylesheet"/>

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>

    <!-- <script type="text/javascript" src="js/albums_header.js"></script> -->

    
    <link rel="stylesheet" href="css/profilestyle.css" />

    <link rel="stylesheet" href="css/profile.css" />


    <!-- <script type="text/javascript" src="js/main.js"></script> -->


    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">


    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <style>
        @import url('https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700');

        body{
            font-family: 'Roboto', sans-serif;
            background: none !important;
        }
        .alignleft{
            float: left;
            margin-right: 15px;
        }
        .alignright{
            float: right;
            margin-left: 15px;
        }
        .aligncenter{
            display: block;
            margin: 0 auto 15px;
        }
        a:focus{
            outline: 0 solid;
        }
        img{
            max-width: 100%;
            height: auto;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6{
            margin: 0 0 15px;
            font-weight: 700;
            color: black !important;
        }
        /******** body-left css ************/
        .body-left{
            margin-right: -15px;
            border-right: 1px solid #EEEEEE;
            height: 1800px;
            padding-top: 100px;
        }
        .body-left ul{
            padding-left: 0px;
            display: inline-block;
            border: 1px solid #FF8728;
        }
        .body-left ul li a{
            background: #333;
            color: #fff;
            padding: 9px 0px;
            padding-left: 30px;
            padding-right: 24px;
            display: block;
            border-bottom: 1px solid #FF8728;
            -webkit-transition: .3s;
            transition: .3s;
        }
        .body-left ul li a:hover{
            background: #FF8728;
        }
        .body-left ul li a:after{
            font-family: fontAwesome;
            content: "\f0da";
            color: #fff;
            padding-left: 136px;
            float: right;
        }
        /********* body-right css ***********/
        .body-right{
            width: 100%;
            padding: 100px 0px 0px 15px;
            padding-left: ;
        }
        .div-one,
        .div-two{
            width: 100%;
            border: 1px solid #EBEBEB;
            border-radius: 3px;
        }
        .div-one h6,
        .div-two h6{
            background: #F7F8FA;
            display: block;
            font-size: 15px;
            font-weight: 500;
            padding: 15px 20px;
            margin-bottom: 0px;
            border-bottom: 1px solid #EBEBEB;
        }
        .div-one-password,
        .div-two-password{
            padding: 16px 20px;
        }
        .div-one-password img,
        .div-two-password img{
            margin-right: 10px;
        }
        .div-one-password h4,
        .div-two-password h4{
            display: inline-block;
            margin-bottom: 0px;
            font-size: 18px;
        }
        .div-one-password p,
        .div-two-password p{
            color: #777777;
            margin-left: 33px;
            font-size: 14px;
        }
        .div-one-password a{
            margin-top: -7% !important;
        }
        .div-one-password a,
        .div-two-password a{
            background: #F7F8FA;
            border: 1px solid #EBEBEB;
            padding: 4px 30px;
            float: right;
            font-size: 16px;
            font-weight: 500;
            margin-top: -10%;
            border-radius: 3px;
            color: #000;
        }
        .div-two{
            margin-top: 30px;
            display: none;
        }
        .div-two-password{
            background: #F3F3F3;
        }
        .div-two-password p{
            border-bottom: 1px solid #DDDDDD;
            padding-bottom: 25px;
        }
        .div-two-password a{
            background: #FFFFFF;
        }
        .password-field{
            padding-top: 50px;
            background: #F3F3F3;
        }
        .input-text {
            margin-left: 60px;
        }
        .input-text p{
            font-size: 14px;
            color: #777777;
            font-weight: 400;
            line-height: 40px;
        }
        .input-field {
            margin-left: -10px;
        }
        .input-field input {
            width: 240px;
            height: 35px;
            margin-bottom: 20px;
            padding: 5px 10px;
        }
        .input-field input:focus{
            outline: 2px solid #EC9794;
        }
        .input-field a{
            font-size: 14px;
            font-weight: 400;
            color: #1d7ff0;
            display: block;
            text-align: center;
            margin-left: 20px;
            margin-top: 10px;
        }
        .save-btn{
            border-top: 1px solid #DDDDDD;
            margin: 20px 15px;
        }
        .save-btn a{
            display: block;
            float: right;
            background: #434343;
            color: #fff;
            font-size: 16px;
            font-weight: 500;
            padding: 7px 25px;
            margin-top: 10px;
            border: 1px solid #FF8728;
            border-radius: 3px;
            -webkit-transition: .3s;
            transition: .3s;
            margin-bottom: 30px;
        }
        .save-btn a:hover{
            background: #FF8728;
        }
        .responsive-text{
            display: none;
        }
        .submit-btn{
            text-align: center;
            margin-bottom: 50px;

        }
        .submit-btn a{
            color: #000;
            background: #fff;
            padding: 8px 30px;
            border: 1px solid #ddd;
            border-radius: 3px;
            font-size: 14px;
            font-weight: 500;
            margin-left: 45px;

        }

        /* profile section */
        .account-heading{
            background: #F7F8FA;
            padding: 16px 20px;
            border-bottom: 1px solid #EBEBEB;
        }
        .account-heading h5{
            font-size: 18px;
            font-weight: 500;
            margin-bottom: 0px;
        }
        .name {
            padding: 30px 10px 10px 20px;
        }

        .name-icon img,h5 {
            display:  inline-block;
            margin-bottom: 8px;
        }
        .name-icon h5{
            font-size: 15px;
            font-weight: 500;
            margin-left: 5px;
        }
        .name-content, 
        .email-content{
            margin-left: -7%;
            border-bottom: 1px solid #DDDDDD;
            margin-bottom: 20px;
        }
        .name-field {
            display: inline-block;
            border-right: 1px solid #DDDDDD;
            padding-right: 33px;
        }

        .name-field span {
            color: #777777;
            font-weight:  400;
            font-size:  14px;
            margin-right: 27px;
        }

        .name-field input[type="text"] {
            margin-bottom: 20px;
            height: 35px;
            width: 243px;
            padding: 5px;
            outline: none;
            color: #000;
            font-size: 14px;
            border: 1px solid #D8D8DB;
            border-radius: 3px;
        }
        .name-field input[type="text"]:last-child{
            margin-bottom: 0px;
        }
        .profile-photo {
            display: inline-block;
            margin-left: 33px;
            height: 90px;
            width: 90px;
            position: relative;
            margin-bottom: -40px;
        }
        .profile-photo img {
            margin-top: -55px;
            width: 100%;
            height: 100%;
        }
        .photo-upload{
            position: absolute;
            left: 0;
            top: 10px;
            width: 100%;
            background: #FF8728;
            border-radius: 3px;
            text-align: center;
        }
        .photo-upload a{

            padding: 10px;
            color: #fff;
            font-size: 12px;

        }
        .photo-upload a img {
            width: 18px;
            height: 13px;
            margin-right: 5px;
            display: inline-block;
            float: left;
            margin-top: 9%;
            margin-left: 10px;
        }

        input[type="submit"] {
            display: inline-block;
            float: right;
            background: #FF8728;
            border: none;
            padding: 2px;
            margin-top: -23px;
            color: #fff;
        }

        input[type="file"] {
            position: absolute;
            top: -105px;
            left: -2px;
            font-size: 12px;
            overflow: hidden;
        }

        .name-content p{
            color: #777777;
            font-size: 12px;
            font-weight: 400;
            margin-left: 15%;
            line-height: 18px
        }
        .name-content p span,
        .manage-content p span,
        .email-otp p span{
            color: #818181;
            font-weight: 500;
            margin-right: 5px
        }
        .email{
            padding: 0px 20px;
            border-bottom: 
        }
        .email-content{
            padding-bottom: 15px;
        }
        .email-content i.fa{
            color: #2d84f0;
            margin-right: 15px;
        }
        .email-content h5{
            color: #818181;
            font-size: 14px;
            font-weight: 400;
            margin-right: 20px;

        }
        .email-content h6{
            font-size: 14px;
            font-weight: 400;
            color: #000;
            display: inline-block;
        }
        .email-content a{
            display: block;
            font-size: 14px;
            font-weight: 400;
            margin-left: 24%;
        }
        .modal-margin-top{
            margin-top: 45%;
            margin-right: -28%;
            margin-left: 35%;
        }
        .save-change-btn{
            display: block;
            float: right;
            margin: 10px 20px;
            padding-bottom: 20px;
            font-size: 16px;
            font-weight: 500;
        }
        .cancel{
            padding: 10px 30px;
            color: #000;
            background: #fff;
            border: 1px solid #ddd;
            border-radius: 5px;
            font-size: 16px;
            font-weight: 500;
            margin-right: 10px;
        }
        .cancel:hover{
            color: #000;
        }
        .save-btn{
            color: #fff;
            background: #444444;
            padding: 10px 30px;
            border: 1px solid #FF8728;
            border-radius: 5px;
            transition: .3s;
        }
        .save-btn:hover{
            color: #fff;
            background: #FF8728;
        }
        .manage-account{
            padding: 0px 20px
        }
        .manage-icon{
            margin-top: 15px;
        }
        .manage-content{
            margin-left: -7%;
            border-bottom: 1px solid #DDD;
            border-top: 1px solid #DDD;
            padding: 20px 0;
            margin-bottom: 20px;
        }
        .manage-content h6{
            font-size: 14px;
            font-weight: 500;
            margin-left: 15%;
        }
        .manage-content p{
            font-size: 12px;
            font-weight: 400;
            color: #777777;
            margin-left: 15%;
        }
        .delete-ac{
            margin-left: 15%;
            font-size: 12px;
            font-weight: 300;
        }
        .email-otp{
            margin-left: 15%;
            margin-top: 20px;
        }
        .email-otp h5{
            font-size: 14px;
            font-weight: 400;
        }
        .email-otp i.fa{
            color: #1684F0;
            padding: 0px 10px;
        }
        .email-otp h6{
            display: inline-block;
            margin-left: 0px;
            font-size: 14px;
            font-weight: 400;
            color: #777777;
        }
        .email-otp p{
            margin-left: 24%;
            margin-bottom: 0px;
        }
        .modal-body {
            text-align: center;
        }
        h5#exampleModalLabel {
            font-size: 16px;
            color: #000;
            font-weight: bold;
            text-align: center;
            margin-left: 34%;
        }
        .modal-footer {
            text-align: center;
            margin-right: 25%;
        }


        .alert {
            position: fixed;
            top: 100px;
            width: 600px;
            right: 50px;
        }

        .body-left ul li a .active {
            background: #FF8728;
        }

    </style>


    <?php include 'headerHome.php'; ?>



    <!----------- Main body start ----------- -->
    <section class="main-body">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="body-left">
                        <ul>
                            <li id="password-tab"><a href="#">Password</a></li>
                            <li id="account-tab"><a href="#">Account</a></li>

                            <li><a href="#">Dummy</a></li>
                            <li><a href="#">Dummy</a></li>
                            <li><a href="#">Dummy</a></li>
                            <li><a href="#">Dummy</a></li>
                            <li><a href="#">Dummy</a></li>
                            <li><a href="#">Dummy</a></li>
                            <li><a href="#">Dummy</a></li>
                            <li><a href="#">Dummy</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 password-tab">
                    <div class="body-right">
                        <!-- <pre>
                        <?php //print_r( $user_det );  ?>
                        </pre> -->
                        <div class="div-one">
                            <h6>Login</h6>
                            <div class="div-one-password">
                                <img src="img/icons/passwordt.png" alt="image">
                                <h4>Change Password</h4>
                                <p>It’s a good idea to use a strong password that you’re not using elsewhere</p>
                                <a class="show-divs" href="#" >Edit</a>
                            </div>
                        </div>
                        <div class="div-two first">
                            <h6>Login</h6>
                            <div class="div-two-password">
                                <img src="img/icons/passwordt.png" alt="image">
                                <h4>Change Password</h4>
                                <p>It’s a good idea to use a strong password that you’re not using elsewhere</p>
                                <a href="#">Close</a>
                            </div>
                            <div class="password-field">
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-3 offset-md-2">
                                            <div class="input-text">
                                                <p>Current Password</p>
                                                <p>New Password</p>
                                                <p>Re-type Password</p>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="input-field">
                                             <p class="responsive-text">Current Password</p>
                                             <input id="old_password" type="password" name="old_password">
                                             <p class="responsive-text">New Password</p>
                                             <input id="log_password" type="password" name="log_password">
                                             <p class="responsive-text">Re-type Password</p>
                                             <input type="password" id="log_password2">
                                             <a href="#">Forgot your password?</a>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                        <div class="save-btn">
                                            <button class="change-pass" id="change-pass">
                                                <a href="#">
                                                    Save Change
                                                </a>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 account-tab" style="display: none;">
              <div class="body-right">
                  <div class="account-heading">
                      <h5>Account</h5>
                  </div>
                  <div class="name">
                      <div class="row">
                          <div class="col-md-3">
                              <div class="name-icon">
                                  <img src="img/icons/man-icon.png" alt="Image">
                                  <h5>Name</h5>
                              </div>
                          </div>
                          <div class="col-md-9">
                              <div class="name-content">
                                  <div class="name-field">
                                      <span>First Name</span><input name="firstname" placeholder="" type="text" value="<?= $user_det[0]['firstname'] ?>"> <br>
                                      <span>Last Name</span><input name="lastname" placeholder="" type="text" value="<?= $user_det[0]['lastname'] ?>">
                                  </div>
                                  <div class="profile-photo">
                                      <img src="uploads/<?= $user_det[0]['profile_pic'] ?>" alt="image">
                                        <input name="oldpic" type="hidden" value="<?= $user_det[0]['profile_pic'] ?>">
                                      <div class="photo-upload">
<!--
                                              <a href="upload.php">
                                                  <img src="img/icons/camera-icon.png" alt="image">
                                                  Upload
                                              </a>
                                          -->
                                          <form action="upload.php" method="post" enctype="multipart/form-data">
                                            <input name="profile_pic" type="file">
                                            <!-- <a href="#"><img src="img/icons/camera-icon.png" alt="image"></a> -->
                                            <!-- <input name="submit" value="Upload" type="submit"> -->
                                        </form>
                                    </div>
                                </div>
                                <p><span>Please Note:</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. <a href="#">Learn More</a></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="email">
                  <div class="row">
                      <div class="col-md-3">
                          <div class="name-icon">
                              <img src="img/icons/man-icon.png" alt="Image">
                              <h5>Email</h5>
                          </div>
                      </div>
                      <div class="col-md-9">
                          <div class="email-content">
                              <i class="fa fa-check-circle" aria-hidden="true"></i>
                              <h5>Primary Email:</h5>
                              <input id="newPrimaryMail" type="hidden" name="email" value="<?= $user_det[0]['email'] ?>">
                              <h6 id="newPrimaryMailTxt"><?= $user_det[0]['email'] ?></h6> 
                              <!-- <a href="#"  data-toggle="modal" data-target="#primaryModal" > <i class="far fa-edit"></i> </a> -->
                              
                              <br>
                             
                              <div class="add-email">
                                  <!-- Button trigger modal -->

                                  <a href="#" data-toggle="modal" data-target="#primaryModal">+ Change Primary Email Address</a>
                                  <a href="#" data-toggle="modal" data-target="#optionalModal">+ Change Secondary Email Address</a>

                                  <!-- Modal -->
                                  <div class="modal fade" id="primaryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
                                      <div class="modal-dialog" role="document">
                                        <div class="modal-content modal-margin-top">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Add Primary Email</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                            Add Email: <input name="email" placeholder="Email" id="newEmail" type="email" value="<?= $user_det[0]['email'] ?>">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button onclick="mailFunction()" type="button" class="btn btn-dark" data-dismiss="modal">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal fade" id="optionalModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
                                      <div class="modal-dialog" role="document">
                                        <div class="modal-content modal-margin-top">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Add Another Email</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                            Add Email: <input name="email" value="<?= $user_det[0]['secondary_email'] ?>" placeholder="Email" id="newEmailOpt" type="email">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button onclick="mailFunctionOpt()" type="button" class="btn btn-dark" data-dismiss="modal">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="save-change">
      <div class="row">
          <div class="col-md-12">
              <div class="save-change-btn">
                  <a class="cancel" href="#"><span id="reset" >Cancel</span></a>
                  <a class="save-btn" href="#"><span  id="profile-save">Save Change</span></a>
              </div>
          </div>
      </div>
  </div>

  <div class="manage-account">
      <div class="row">
          <div class="col-md-4">
              <div class="name-icon manage-icon">
                  <img src="img/icons/man-icon.png" alt="Image">
                  <h5>Manage Account</h5>
              </div>
          </div>
          <div class="col-md-8">
              <div class="manage-content">
               <h6>Deactivate your account</h6>
               <p><span>Please Note:</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. <a href="#">Learn More</a></p>
               <a class="delete-ac" href="#">Deactivate your account.</a>

               <div class="email-otp">
                  <h5>Email opt out</h5>
                  <i class="fa fa-check-circle" aria-hidden="true"></i>
                  <h6>Opt out of receiving emails from whatdadilly</h6>
                  <p> <span>Please Note:</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
          </div>
      </div>
  </div>
</div>

<div class="save-change">
  <div class="row">
      <div class="col-md-12">
          <div class="save-change-btn">
              <a class="cancel" href="#">Cancel</a>
              <a class="save-info-btn" href="#">Save Change</a>
          </div>
      </div>
  </div>
</div>

</div>
</div>
</div>
</div>
</section>

<div class="alert alert-success" id="success-alert" style="display: none;">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Success! </strong>
    <p class="msg_text"></p>
</div>
<div class="alert alert-danger" id="danger-alert" style="display: none;">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Wrong! </strong>
    <p class="msg_text"></p>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#success-alert").hide();
        $("#danger-alert").hide();


        $(".show-divs").click(function () {
            $(".div-two").show(1000);
            console.log('o');
        });

        $(".first").children(".div-two-password").children("a").click(function () {$(".first").hide(1000);});
        $(".second").children(".div-two-password").children("a").click(function () {$(".second").hide(1000);});
        $(".third").children(".div-two-password").children("a").click(function () {$(".third").hide(1000);});

        $("#change-pass").on('click',function(){

            var old_password = $('#old_password').val();
            var log_password = $('#log_password').val();
            var log_password2 = $('#log_password2').val();

            if ( log_password != log_password2 || log_password == "") {
                $('#danger-alert .msg_text').html('Password didnt\'s matched');
                $('#danger-alert').fadeTo(2000, 500).slideUp(500, function(){
                    $("#danger-alert").slideUp(500);
                });   

                return;
            }

            $.ajax({
                url: 'modifypassword.php',
                type: 'post',
                data: {'old_password': old_password, 'log_password': log_password },
                dataType: 'json',
                success: function (response) {

                    console.log(response);

                    var alert_type = 'danger';
                    if (response.success) {
                        alert_type = 'success';
                    }

                    $("#"+ alert_type +"-alert .msg_text").html(response.message);

                    $("#"+ alert_type +"-alert").fadeTo(2000, 500).slideUp(500, function(){
                        $("#"+ alert_type +"-alert").slideUp(500);
                    });   

                },
                error: function (request, status, error) {
                    console.log("error setting url");
                }
            });

        });

        $("#profile-save").on('click',function(){

            console.log('yo');

            var formData = new FormData();
            formData.append('firstname', $('input[name="firstname"]').val() );
            formData.append('lastname', $('input[name="lastname"]').val() );
            formData.append('email', $('input[name="email"]').val() );
            formData.append('secondary_email', $('input[name="secondary_email"]').val() );
                // Attach file
                formData.append('oldpic', $('input[name="oldpic"]').val()); 
                formData.append('profile_pic', $('input[name="profile_pic"]')[0].files[0]); 

                $.ajax({
                    url: 'updatebasicprimaryinfo.php',
                    type: 'post',
                    data: formData,
                    dataType: 'json',
                    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                    processData: false, // NEEDED, DON'T OMIT THIS
                    success: function (response) {

                        console.log(response);

                        var alert_type = 'danger';
                        if (response.success) {
                            alert_type = 'success';
                        }

                        $("#"+ alert_type +"-alert .msg_text").html(response.message);
                        
                        $("#"+ alert_type +"-alert").fadeTo(2000, 500).slideUp(500, function(){
                            $("#"+ alert_type +"-alert").slideUp(500);
                        });  

                    },
                    error: function (request, status, error) {
                        console.log("error setting url");
                    }
                });

            });

        // tab function

        $("#account-tab").on('click',function(){
            $('.body-left ul li a').removeClass('active');

            $(this).children().addClass('active');
            $(".account-tab").show();
            $(".password-tab").hide();
        });

        $("#password-tab").on('click',function(){
            $('.body-left ul li a').removeClass('active');
            
            $(this).children().addClass('active');

            $(".account-tab").hide();
            $(".password-tab").show();
        });


    });

    function mailFunction(){
        var newMail = document.getElementById("newEmail").value;
        document.getElementById("newPrimaryMailTxt").innerHTML = newMail;
        document.getElementById("newPrimaryMail").value = newMail;
    }

    function mailFunctionOpt(){
        var newMail = document.getElementById("newEmailOpt").value;
        document.getElementById("newOptionalMailTxt").innerHTML = newMail;
        document.getElementById("newOptionalMail").value = newMail;
    }

</script>

    <!-- 8623ebbfa47ac3560781e328f91b7932 -->