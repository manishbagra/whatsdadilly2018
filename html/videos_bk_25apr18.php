<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

	<head>

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<title>:WHATSDADILLY:</title>
		
		<link href="css/style-album.css" type="text/css" rel="stylesheet" />
		
		<link href="css/whatsdadilly.css" type="text/css" rel="stylesheet" />

		<link rel="stylesheet" href="css/bootstrap.min.css" />

		
		<link rel="stylesheet" href="css/profilestyle.css" />

		<link rel="stylesheet" href="css/profile.css" />


		<!-- start video css -->
		<link rel="stylesheet" type="text/css" href="css/rs_video.css" />
		<link href="video-player/css/video-js.min.css" rel="stylesheet"/>
		<link rel="stylesheet" href="css/vid_pip.css" />
		<link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css" />
		<script src="video-player/js/video.min.js"></script>
		<!-- end video css -->

		<link href="css/albumstyle.css" rel="stylesheet" />



		<!-- video light box libraries  -->
		
		<script type='text/javascript' src='video-lightbox/wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4'></script> 
		
		<script type='text/javascript' src='video-lightbox/wp-includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1'></script>
	
		<link rel="stylesheet" id="lcso-head-styles" type="text/css" href="video-lightbox/wp-content/lcso-cache/55c2eace6f6bf9d540c7d0d4dbf05217.css"/>
		
		<script type="text/javascript" src="video-lightbox/wp-content/lcso-cache/23cae323688aa341762d970c789ca836.js"></script>
		
		<!-- end light box -->

		<style>
			.total-wrap.another{
				background-color: rgba(0, 0, 0, 0.9);
			    overflow-x: hidden;
			    position: fixed;
			    transition: all 0.5s ease 0s;
			    z-index: 200;
			    width: 100%;
			    height: 100%;
			}
			.justify-content-center{
			}
			.main-wrap{
				margin:10% 0 0 22%;
			}
		</style>	




    </head>

    <body ontouchstart>   

	<?php include 'headerHome.php'; ?>
	
	<span class="video_upload hide"><?php include 'video_uploading.php'; ?></span>


            <i id="togglebtn" class="fa fa-bars" aria-hidden="true"></i>  
            <div id="content-section" class="container-fluid">
                <div id="sidebar" class="sidenav-class">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="mySidenav" class="sidenav">
                                    <ul id="sidenav" class="sidenav-ul">
                                        <li>
                                            <a class="nav-active" href="album.php"><span>Albums</span></a>
                                        </li>
                                        <li>
                                            <a href="#"><span> Posted Photos</span></a>
                                        </li>
                                        <li>
                                            <a href="videos.php"><span>Video's</span></a>
                                        </li>
                                    </ul>
                                    <div id="social">Social</div>
                                    <ul class="sidenav-ul">
                                        <li class="li-class" >
                                            <a href="#"><div ><img class="img-li" src="images/instragram.png"/> </div><div class="img-text">Instragram</div></a>
                                        </li>
                                        <li class="li-class">
                                           <a href="#"><div ><img class="img-li" src="images/tumblr_icon.png"/> </div> <div class="img-text">Tumblr</div></a>
                                        </li>
                                        <li class="li-class">
											<a href="#"><div><img class="img-li" src="images/Pinterest.png"/> </div> <div class="img-text">Pinterest</div></a>
										</li>
										<li class="li-class">
											<a href="#"><div><img class="img-li" src="images/youtube.png"/> </div> <div class="img-text">Youtube</div></a>
										</li>
									</ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<br/><br/><br/>
            <div id="imagecontainer" class="col-md-10 col-md-offset-2">
                <div id="button" class="col-sm-4 col-md-offset-8 vidPopBtn">
                    <div class="album_form col-sm-6 vidColSix">
                        <div class="file-upload-container vidColSixCont">
                            <div class="file-upload-override-button left"><i class="fa fa-upload faUpld" aria-hidden="true"></i><span class="SpnUpld" >upload</span>
								<form method="post" id="vid_up" enctype="multipart/form-data">
								<input name="files[]" class="file-upload-button" title="New Videos" id="file-upload-button" type="file" multiple />
								
								<input  type="hidden" id="imgN_array" name="imgN_array"/>
								<input  type="hidden" id="imgTN_array" name="imgTN_array"/>
								<input  type="hidden" id="vSrc_array" name="vSrc_array"/>
								<input  type="hidden" id="title_array" name="title_array"/>
								<input  type="hidden" id="descrip_array" name="descrip_array"/>
								<input  type="hidden" id="type_array" name="type_array"/>
								<input  type="hidden" id="tags_array" name="tags_array"/>
								<input  type="hidden" id="category_array" name="category_array"/>
								<input  type="hidden" id="vDesc_array" name="vDesc_array"/>
								
								<input type="submit" class="formSubmit frmVidSub" name="submit" />
								</form>
							</div>
                        <div class="both"></div>
                        </div>
                    </div>
                </div>
               
                <div class="col-md-12" id="below-nav"></div>

                <div class="container-fluid">

   
		<div class="elementor-widget-container">

		<div id="5ab884825d316" class="gg_gallery_wrap gg_columnized_gallery gid_5501 ggom_103   ggom_vc_txt" data-gg_ol="103" data-col-maxw="200" rel="5501" data-nores-txt="No images found in this page">            	

        <div class="gg_loader" style="display: none;">

		<div class="ggl_1">

		</div>

		<div class="ggl_2">
		</div>

		<div class="ggl_3">
		</div>

		<div class="ggl_4">

		</div>

		</div>      

			<div class="gg_container">
				
				<?php foreach($result_all as $result): ?>

			<div data-gg-link="<?php echo URL.$result["file"]; ?>"

						class="gg_img 5ab884825d316-2 gg_shown" 

						data-gg-url="<?php echo URL.$result["thumbnail"]; ?>" 

						data-gg-title="<?php echo $result["title"]; ?>" 

						data-gg-author="<?php echo URL.$result["thumbnail"]; ?>" 

						
						data-gg-descr="<?php echo $result["title"]; ?>" 

						data-img-id="<?php echo $result["id"]; ?>" 

						rel="<?php echo $result["id"]; ?>" 

						style="width: calc(345px - 10px - 1px);">


				<div class="gg_img_inner" style="padding-bottom: 100%">

					<div class="gg_main_img_wrap">

						<div class="gg_img_wrap_inner">

							<div class="col-sm-12 uplod video">


<div class="image-preview videoGetSrc">

	<?php if(!empty($result["thumbnail"])){ ?>
		<video  id="<?php echo $result['file']; ?>" src=""   class="video-js  vjs-default-skin" poster="<?php echo $result["thumbnail"]; ?>"  >
			<source src="" type="video/mp4" >
		</video>
	<?php }else{ ?>
		<video id="<?php echo $result['file']; ?>" src=""   class="video-js  vjs-default-skin" poster="images/em_vid.png"  >
			<source src="" type="video/mp4" >
		</video>
	<?php } ?>	
	
</div>
											
											<div class="style-scope ytd-thumbnail">
												<span class="style-scope ytd-thumbnail-overlay-time-status-renderer">
													  4:34
												</span>
											</div>
											
											<h5 class="style-scope">
											<a id="video-title" class="vid_link" 
											title="<?php echo $result['title'] ?>" href="javascript:void(0)"><?php echo $video_model->substrword(str_replace("_"," ",$result['title']),50); ?></a>
											</h5>
											<p class='vid_info'><span class='vid_views'><?php echo $video_model->countVideoViews($entityManager,$result['id']); ?> views.</span><span class='vid_time'><?php echo $insta->time_elapsed_string($result['date']); ?></span>
											</p>

								</div>

						</div>

					</div>


				</div>
				
			</div>	
				
				<?php endforeach; ?>
		</div>	

	</div>
	
	</div>

		
                    
                </div>
            </div> 
        
		<!-- Modal -->
			  <div class="modal fade disModel" id="EscModal"  role="dialog">
				<div class="modal-dialog">
				
				  <!-- Modal content-->
				  <div class="modal-content">
					<div class="modal-header disHeader">
					  <button type="button" class="close closeOpacity" data-dismiss="modal">&times;</button>
					  <h5 class="modal-title disTitle">Discard Changes?</h5>
					</div>
					<div class="modal-body disBdy">
					  <p>If you discard now, you'll lose your progress.</p>
					</div>
					<div class="modal-footer disVidFoot">
					  <button type="button" class="btn btn-default closeOpacity" data-dismiss="modal">Go Back</button>
					  <button type="button" id='DiscardAlbum' class="btn btn-primary" data-dismiss="modal">Discard</button>
					</div>
				  </div>
				  
				</div>
			  </div>


			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

			<script src="js/whatsdadilly.js"></script>

			<script type="text/javascript" src="video-lightbox/wp-content/lcso-cache/aa85dd32a8de4f18579fe1cda16084a3.js"></script> 

			<script type="text/javascript" src="video-lightbox/wp-content/lcso-cache/lightbox.js"></script>

	</body>
</html>
