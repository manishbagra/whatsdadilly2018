	<?php include 'html/headerAlbum.php'; ?>
	
    <div class="container-fluid"style="padding-top:80px;">
	
        <div class="row">
		
            <?php include 'html/album_side_menu.php' ?>

            <main role="main" class="main-page ml-sm-auto pt-3 px-5">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center  main-content-top border-bottom">
                    <h3 class="main-title-heading">Album Topics</h3>
					
					<div class="alert alert-success" style="display:none;border-radius:0px;margin-bottom:0px;padding:0.4rem 1.25rem;">
						<strong>Success!</strong><a href="#" class="alert-link">Saved Album Successfully..!!</a>.
					</div>
					
					
                    <div class="btn-toolbar mb-md-0">
					
					
                        <div class="btn-group">
							
								<a style="display:none;" id="openBox" rel="openBox" href="imageuploader.php" title="New Album"></a>
								
								<div class="upload-btn-wrapper">
								
									<button type="button" class="btn btn-secondary btn-sm add-btn"><i class="fas fa-plus"></i>Create Album</button>
									
									<form method="post" id="img_up" enctype="multipart/form-data">
										<input type="file" title="CreateAlbum" id="file-upload-button" multiple name="file-upload-button" />
										<input  type="hidden" class="title" name="title"/>
										<input  type="hidden" id="picDesc" name="picDesc"/>
										<input  type="hidden" id="image64" name="image64"/>
										<input  type="hidden" class="album_location" name="album_location"/>
										<input  type="hidden" class="description" name="description"/>
										<input  type="hidden" id="countImg" name="count"/>
										<input type="submit" class="formSubmit" name="submit" style="display:none;" />
									</form>
									
									
								  
								</div>
							
							
                        </div>
                    </div>
                </div>
                <div class="row gutter-10" id="album_cover" >
				
				<?php foreach($albums as $key=> $album){
					
					$image_path = 'uploads/'.$album['id_owner'].'/albums/'.$album['id_album'].'/thumb/'.$album['cover'];
		
					$images = glob($image_path."*.{jpg,jpeg,png,gif}", GLOB_BRACE);
					
					//echo '<pre>'; print_r($images);die;
					
					$file = basename($images[0]);
					
					$explode = explode(".",$file);
					
					if($explode[0] == $album['cover']){
						
						$ext = $explode[1];
						
					}
					
					$cover = $image_path.'.'.$ext;
					
					

				?>
				
				<a href="album_detail.php?id=<?php echo $album['id_album'];  ?>">

                    <div class="col-lg-2 col-md-6 single-album">
                        <div class="img-container">
                            <div class="thumb">
                                <img class="img-fluid img-photos" id="img-fluid-<?php echo $album['cover']; ?>"  src="<?php echo $cover; ?>" alt="">
                            </div>
                            <a class="edit-btn" href="#"><i class="far fa-trash-alt"></i></a>
                        </div>
                        <a href="upload.html"><h6 class="album-title"><?php echo $album['title']; ?></h6></a>
                        <p><?php echo $Albums->countPhotosByaAlbumId($entityManager,$album['id_album']) ?> Photos</p>
                    </div>
					
				</a>	
					
				<?php } ?>	

                </div>

            </main>
        </div>
    </div>
	
	<?php include 'html/footerAlbum.php'; ?>