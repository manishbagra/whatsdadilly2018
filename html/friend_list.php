<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/fav.png">
    <!-- Author Meta -->
    <meta name="author" content="">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Whatsdadilly?</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700,900" rel="stylesheet">
    <!--
		CSS
	============================================= -->

    <link rel="stylesheet" href="friendlist/css/linearicons.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    
    <link rel="stylesheet" href="friendlist/css/animate.min.css">   
    <link rel="stylesheet" href="friendlistcss/owl.carousel.css">          
    <link rel="stylesheet" href="friendlist/css/main.css">

	<style>
	     #picture {

                display: none;

                padding: 10px;

                //    background: #d8d8d8;

                //max-height: 510px;

                //    overflow-y: auto;

            }





            #picture::-webkit-scrollbar-track

            {

               -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);

               background-color: #F5F5F5;

           }



           #picture::-webkit-scrollbar

           {

               width: 6px;

               background-color: #F5F5F5;

           }



           #picture::-webkit-scrollbar-thumb

           {

               background-color: #000000;

           }





           .preview img

           {



            width: 130px !important;

            height: 130px;

        }



        .fileUpload {

            position: relative;

            overflow: hidden;

            margin: 10px;

        }

        .fileUpload input.upload {

            position: absolute;

            top: 0;

            right: 0;

            margin: 0;

            padding: 0;

            font-size: 20px;

            cursor: pointer;

            opacity: 0;

            filter: alpha(opacity=0);

        }

        .fa-plus

        {

            margin-top:35px !important;

        }



        .one-photo

        {

            width: 130px;

            height: 130px;

            text-align: center;

            margin-top: 10px;

            border: 2px dashed #ccc;

            color: #ccc;

            font-size: 13pt;

            float:left;

            margin-right:5px;

            margin-bottom:5px;

        }



        .removePhoto

        {

            z-index: 100;

            position: relative;

            text-align: right;

            margin-right: 5px;

        }



        .fileUpload

        {



            z-index: 10;

            margin-top: 0px;

            opacity: 0;

            height: 100px;

            position: absolute;

        }



        .preview

        {

            position: relative;

            float: left;

            margin-top: -80px;

            width: 130px;

            margin-left: -2px;

            height: 130px;

        }

        .comment_box_reply{display:none;}





    </style>







    <!-- // css tech -->



    <style>





     .coverphoto, .output {

         width: 100%;

         /*width: 1140px;*/

         height: 358px;

         border: 0px solid black;

         margin: -5px auto;

         /*margin: 0px auto;*/

     }



     .coverphoto img {

        max-width: none;

    }

    ..cropit-preview {

        background-color: #f8f8f8;

        background-size: cover;

        /* border: 1px solid #ccc; */

        border-radius: 3px;

        height: 358px !important;

        margin-top: -5px;

        position: relative;

        /* width: 1140px; */

        width: 100% !important;

    } 

    .cropit-preview > img {

      height: 358px;

      width: 100%;

  }

  .change_photo {

      position: absolute;

      right: 7px;

      top: 34px;

  }

  .remove_photo {

      position: absolute;

      right: 7px;

      top: 82px;

  }



  .cropit-preview-image-container {

    cursor: default;

}

/*extra added by tech*/

.crop-control {

  padding: 1px;

  text-align: right;

  /* border-top: 1px solid #e5e5e5; */

  margin: 0 2 0 0;

  width: 77%;

  float: right;

  /*margin-top: -38px;*/

}

.image-size-label {

   float: left;

   width: 27%;

   height: 36px !important;

   text-align: left;

   color: white;

   font-size: 21px;

   /* font-weight: normal; */

   line-height: 13px;

   margin-left: 17px;

   padding: 5px;

   position: absolute;

   z-index: 1;

}



.image-size-label > .tiny-label {

 font-size: 11px;

 /* font-weight: normal; */

}



.crop-control img {

  height:36px !important;

}

.image-ranger-control {

 float: right;

 width: 30%;

 position: relative;

 /* left: 284; */

 padding: 8px;

 z-index: 2;

 margin-right: 300px;

 /* margin-left: 45%; */

}



.image-ranger-control img{

   width: 20px !important;

   height: auto !important; 

}



.image-action-control {

 float: left;

 width: 66%;

 z-index: 1;

 position: absolute;

 /* text-align: right; */

 padding: 5px;

}



.image-action-control > button {

 height:27px;

 /*padding:5px;*/

 line-height:5px;

}



/*ended extra added */







button[type="submit"] {

    /*margin-top: 10px;*/

}



#result {

    margin-top: 10px;

    width: 900px;

}



#result-data {

    display: block;

    overflow: hidden;

    white-space: nowrap;

    text-overflow: ellipsis;

    word-wrap: break-word;

}

.image-editor {

    /*display: inline-block;*/

    position: relative;

}



.add_photo {

 background: #515050 none repeat scroll 0 0;

 color: #fff;

 /*display: inline-block;*/

 display: none;

 font-weight: bold;

 padding: 6px;

 position: absolute;

 right: 0;

 /* top: 21px; */

 z-index: 1;

 width: 162px;

 margin: 35px 34px 0 0;

}



.add_photo > .fa {

 font-size: 16px;

 padding: 0 7px 0 5px;

}



                   /*#btn-add_cover, #btn-rem_cover {

                   	 display: none;

                     }*/



                   /*.coverphoto:hover {



                   }*/



                   .remove_photo {

                    background: #515050 none repeat scroll 0 0;

                    color: #fff;

                    /*display: inline-block;*/

                    display: none;

                    font-weight: bold;

                    padding: 6px;

                    position: absolute;

                    right: 0;

                    top: 40px;

                    z-index: 1;

                    width: 162px;

                    margin: 35px 34px 0 0;

                }

                #loading-image {

                    background: transparent url("coverpic/gif/loading4.gif") no-repeat scroll 0 0;

                    height: 100%;

                    /* left: 294px; */

                    position: absolute;

                    /* top: 63px; */

                    width: 11%;

                    z-index: 1;

                    display: none;

                    margin-left: 45%;

                    margin-top: 8%;

                }

                .add_photo:hover, .remove_photo:hover {

                    cursor: pointer;

                }



                /*input[type="range"]{*/

                   /*position: absolute !important;*/

                   /*z-index: 1 !important;*/

                   /*}*/



                   /* custom ranger design by tech*/



                   input[type=range] {

                     -webkit-appearance: none;

                     width: 100%;

                     margin: 7.1px 0;

                 }

                 input[type=range]:focus {

                     outline: none;

                 }

                 input[type=range]::-webkit-slider-runnable-track {

                     width: 100%;

                     height: 3.8px;

                     cursor: pointer;

                     box-shadow: 0px 0px 0px #000000, 0px 0px 0px #0d0d0d;

                     background: rgba(238, 238, 238, 0.78);

                     border-radius: 1.3px;

                     border: 0.2px solid rgba(0, 0, 0, 0);

                 }

                 input[type=range]::-webkit-slider-thumb {

                     box-shadow: 0.9px 0.9px 1px rgba(0, 0, 49, 0), 0px 0px 0.9px rgba(0, 0, 75, 0);

                     border: 1.8px solid rgba(0, 0, 0, 0.17);

                     height: 18px;

                     width: 18px;

                     border-radius: 15px;

                     background: #ffffff;

                     cursor: pointer;

                     -webkit-appearance: none;

                     margin-top: -7.3px;

                 }

                 input[type=range]:focus::-webkit-slider-runnable-track {

                     background: rgba(251, 251, 251, 0.78);

                 }

                 input[type=range]::-moz-range-track {

                     width: 100%;

                     height: 3.8px;

                     cursor: pointer;

                     box-shadow: 0px 0px 0px #000000, 0px 0px 0px #0d0d0d;

                     background: rgba(238, 238, 238, 0.78);

                     border-radius: 1.3px;

                     border: 0.2px solid rgba(0, 0, 0, 0);

                 }

                 input[type=range]::-moz-range-thumb {

                     box-shadow: 0.9px 0.9px 1px rgba(0, 0, 49, 0), 0px 0px 0.9px rgba(0, 0, 75, 0);

                     border: 1.8px solid rgba(0, 0, 0, 0.17);

                     height: 18px;

                     width: 18px;

                     border-radius: 15px;

                     background: #ffffff;

                     cursor: pointer;

                 }

                 input[type=range]::-ms-track {

                     width: 100%;

                     height: 3.8px;

                     cursor: pointer;

                     background: transparent;

                     border-color: transparent;

                     color: transparent;

                 }

                 input[type=range]::-ms-fill-lower {

                     background: rgba(225, 225, 225, 0.78);

                     border: 0.2px solid rgba(0, 0, 0, 0);

                     border-radius: 2.6px;

                     box-shadow: 0px 0px 0px #000000, 0px 0px 0px #0d0d0d;

                 }

                 input[type=range]::-ms-fill-upper {

                     background: rgba(238, 238, 238, 0.78);

                     border: 0.2px solid rgba(0, 0, 0, 0);

                     border-radius: 2.6px;

                     box-shadow: 0px 0px 0px #000000, 0px 0px 0px #0d0d0d;

                 }

                 input[type=range]::-ms-thumb {

                     box-shadow: 0.9px 0.9px 1px rgba(0, 0, 49, 0), 0px 0px 0.9px rgba(0, 0, 75, 0);

                     border: 1.8px solid rgba(0, 0, 0, 0.17);

                     height: 18px;

                     width: 18px;

                     border-radius: 15px;

                     background: #ffffff;

                     cursor: pointer;

                     height: 3.8px;

                 }

                 input[type=range]:focus::-ms-fill-lower {

                     background: rgba(238, 238, 238, 0.78);

                 }

                 input[type=range]:focus::-ms-fill-upper {

                     background: rgba(251, 251, 251, 0.78);

                 }



                 /* ranger css ended */



                 .profile_pic_overlay {

                   position: absolute;

                   z-index: 1;

                   width: 82%;

                   /*width: 237.5px;*/

                   height: 180px;

                   background: white;

                   opacity: 0;

               }



               div.profile_pic {

                   border: 3px solid black;

               }



               .button_ttp {

                 background: #4c4c4c;

                 background: -moz-linear-gradient(top, #4c4c4c 0%, #131313 100%);

                 background: -webkit-linear-gradient(top, #4c4c4c 0%,#131313 100%);

                 background: linear-gradient(to bottom, #4c4c4c 0%,#131313 100%);

                 filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4c4c4c', endColorstr='#131313',GradientType=0 );

                 border: 1PX solid #ff8500!important;

                 border-radius: 4px;

                 color: #ffffff;

                 padding: 7px 12px;

                 border: none;

                 /*height: 40px;*/

             }





             .notshow{

                display:none;

              }

            .notActshow{

                display:none;

            }

            .tweetsAct{

                cursor:pointer;

                background:white;

                color:blue;

                text-align:center;

                height: 40px;

                color: #00BDFF;

                margin-bottom: 15px;        



            }

            .tweetsAct P{

                padding:10px;

                font-weight:bold;

            }

            .tweetsAct:hover{

                cursor:pointer;

                background:#00BDFF;

                color:blue;

                text-align:center;w

                height: 40px;

                color: white;

                margin-bottom: 15px;



            }



            .info_linkbox {

                border: 1px solid #ccc;

                margin: 7px 5px;

                box-shadow: 0px 0px 1px #ccc;

                overflow: hidden;

                max-height: 80px;

                line-height: 16px;

            }

	</style>
	
	
</head>

<body>
     <?php include ("headerHome.php");
	 
	 $userid = base64_decode($_GET['id']);
	
	 $data = $messages->getuserdata($entityManager, $userid);
	 $img = Profile::getCoverPhoto($entityManager,$userid);
	
	 ?>
          
    <!-- Start cover-section -->
    <section class="cover-section"style="padding-top:60px;">
        <div class="container">
            <div class="row">
			      <?php 
                    if(isset($_GET['id']) && isset($_SESSION["userid"]) ) {

                       $profileid = base64_decode($_GET['id']);

                       $user_id = $_SESSION["userid"];

                    }

                   ?>				



                   <div class="coverphoto">



                     <div class="image-editor">

                         <div id="loading-image"></div>



                         <input type="file" id="fileInput" name="fileInput" style="display:none;" class="cropit-image-input" />



                         <?php if($profileid == $user_id) { ?>



                         <p id="btn-add_cover"  onclick="chooseFile();" class="add_photo">

                             <i class="fa fa-camera"></i><span>Add cover photo</span>

                         </p>

                         <p id="btn-rem_cover" class="remove_photo">

                             <span>Remove cover photo</span>

                         </p>



                         <?php } ?>



                         <div class="cropit-preview">

                             <img width="100%" src="coverpic/images/<?php 
						if( isset($_SESSION["userid"]) && $img = Profile::getCoverPhoto($entityManager,$profileid)) {

                               echo $img;

                               $_SESSION["coverpic"] = $img;

                           }

                           else {

                              echo  'banner.jpg';

                              $_SESSION["coverpic"] = 'banner.jpg';

                          }

                          ?>">



                      </div>



                      <div class="crop-control" style="display:none;">



                        <div class="image-size-label" style="display:none;">

                         <!--  <img src="coverpic/assets/covertext.png" width="100%" height="36px"> -->

                         <span>Reposition & scale header</span><br>

                         <span class="tiny-label">Some areas may be cropped on larger screens</span>

                     </div>



                     <div class="image-action-control">

                        <button

                        type="button"

                        class="button_ttp"

                        onclick="clickcancel();"

                        style="border:0px solid black !important;background: white;color:black">

                        Cancel

                    </button>

                    <button type="submit" class="button_ttp" name="send" id="saveImage">Apply</button>



                </div>

                <div class="image-ranger-control">

                    <img src="coverpic/assets/left-zoom-icon.png" style="width:14px !important; float:left;margin:3px 0 0 -25px;">

                    <img src="coverpic/assets/right-zoom-icon.png"  style="float:right;margin:0px -30px 0 0px;"><input type="range" class="cropit-image-zoom-input" style="display: block;" min="0" max="1" step="0.01">



                    <input type="hidden" name="image-data" class="hidden-image-data">





                </div>







            </div>

        </div>



    </div>
			      <!--<?php if($img == ''){?>
				  <?php $img = 'banner.jpg' ;?>
				   <img class="img-fluid" src="coverpic/images/<?php echo $img?> " alt=""style="width:100%">
				 
				  <?}else{?>
				     <img class="img-fluid" src="coverpic/images/<?php echo $img?> " alt=""style="width:100%">
				  <?php }?>-->
                <div class="cover-bottom d-flex justify-content-between w-100">
                    <div class="user-detials d-flex align-items-center">  
                        <div class="thumb1">
						    <?php if($data[0]['user_id'] == $_SESSION['userid'] ){ ?>
                            <a href="profile.php?profileid=<?php echo base64_encode($_SESSION['userid']); ?>"><img src="<?php echo 'uploads/'.$_SESSION['profile_pic']; ?>" alt="" style="width:42px;height:37px;!important"></a>
							<?php } else { ?>
							<a href="profile.php?profileid=<?php echo base64_encode($data[0]['user_id']); ?>"><img src="<?php echo 'uploads/'.$data[0]['profile_pic']; ?>" alt="" style="width:42px;height:37px;!important"></a>
                            <?php } ?>           
					   </div>
                        <?php if($data[0]['user_id'] == $_SESSION['userid'] ){ ?>
                        <a href="profile.php?profileid=<?php echo base64_encode($_SESSION['userid']); ?>"><h4 class="text-white ml-10"><?php echo $_SESSION['firstname'].'&nbsp;'.$_SESSION['lastname'];?> </h4></a>
                        <?php }else { ?>
						    <a href="profile.php?profileid=<?php echo base64_encode($data[0]['user_id']); ?>"><h4 class="text-white ml-10"><?php echo $data[0]['firstname'].'&nbsp;'.$data[0]['lastname'];?> </h4></a>
						<?php }?>						
				   </div>
                    <div class="search-form d-flex flex-row relative ">
                        <input class="form-control form-control-dark w-100" placeholder="Search for your friends" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search for your friends'"  type="text">
                        <button type="submit" class="search-btn"><i class="fa fa-search"></i></button>                   
                    </div>
                </div>
        </div>    
    </section>
    <!-- End cover-section -->

    <!-- Start friend-list-section -->
	 <?php
	 $userid = base64_decode($_GET['id']);
	 
	 $userid1= $session->getSession("userid");
      if($userid != "")
      {
            $getallfreind = $messages->getFriendsList($entityManager, $userid, 10000);
      }
      else{
            $getallfreind = $messages->getFriendsList($entityManager, $userid1, 10000); 
		} 
		$freindrequestdata = $messages->getFriendsRequest($entityManager, $userid );
		
		?> 
    <section class="friend-list-section">
        <div class="container">
            <div class="row">
                <div class="friend-list-area w-100">
                    <div class="top-bar-wrap d-flex justify-content-between align-items-center">
                        <div class="total-frnd">
                            <h4><i class="fa fa-users mr-10" aria-hidden="true"></i>
                            Friends List (<?php echo count($getallfreind);?>)</h4>
                        </div>
                        <div class="left-wrap d-flex">
                            <a href="friend_request.php" class="buttons " style="height:2%"><i class="fa fa-users" aria-hidden="true"></i> Friend Requests 
							<?php if(count($freindrequestdata) == 0){?>
								<span class=""></span>
							<?php }else{ ?>
							<span class="badges"><?php echo count($freindrequestdata)?></span>
							<?php } ?>
							</a>
                            <a href="#" class="buttons frnd-req-btn"><i class="fa fa-plus" aria-hidden="true" style="margin-top:0px!important"></i> Find Friends</a>
                        </div>
                    </div>                   
                </div>                 
            </div>
            <div class="row d-flex frnd-lists justify-content-start">
                <?php if(count($getallfreind)==0){ ?>
				 <div style="font-size:20px;margin-left:50%;">
				 <?php  echo "No FRIENDS!";?>
				  </div>
				<?php }else{					
                 foreach($getallfreind as $data){ ?>
					<?php //echo'<pre>';print_r($data); ?>					  
                <div class="cols">
                    <div class="inner-wrap">
                        <div class="thumb">
                            
							 <?php if($data['profile_pic'] == '') 
							 {?>
						 
						         <a href="profile.php?profileid=<?php echo base64_encode($data['user_id']); ?>"><img src="uploads/default/Maledefault.png" alt="" style="width:220px;height:240px;!important"></a>
								 
							 <?php }else{								 
							 ?>
							 <a href="profile.php?profileid=<?php echo base64_encode($data['user_id']); ?>"><img class="img-fluid" src="<?php echo "uploads/".$data['profile_pic'] ?>" alt="" style="width:220px;height:240px;!important"></a>
							<?php  } ?>
                        </div>
                        <div class="detials">
                            <a href="profile.php?profileid=<?php echo base64_encode($data['user_id']); ?>"><h4><?php echo $data['firstname'].'&nbsp;'.$data['lastname'];?></h4></a>
                            <p>
                                Doctor, Photographer, Model <br>
                                San Fransisco, California
                            </p>
                            <div class="buttons d-flex justify-content-center">
                                <a href="#"><i class="fa fa-check" aria-hidden="true"></i>Friends</a>
                                <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i>Message</a>
                            </div>                                    
                        </div>                          
                    </div> 
                </div> 
				<?php }
				}?>
            </div>
        </div>    
    </section>
    <!-- End friend-list-section -->
    
    <div class="white-space pt-150"></div>

    

    <script src="friendlist/js/vendor/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="friendlist/js/vendor/bootstrap.min.js"></script>
    <script src="friendlist/js/owl.carousel.min.js"></script>          
    <script src="friendlist/js/wow.min.js"></script>        
    <script src="friendlist/js/main.js"></script>
	<script src="header/js/header.js"></script>


   
	
	<script type="text/javascript" src="coverpic/jquery.cropit.js"></script>
	

	<script type="text/javascript" src="js/profile.js"></script>
	
	

	
	
	
	

    

   
<script>

changeCoverState = false;  



    $(document).ready(function(){

        $(".center_area").click(function(){

            $(".center_area").css( { marginTop : "0px", marginBottom : "0px" } );

            $(this).css( { marginTop : "5px", marginBottom : "5px" } );

        });







    // js added by tech     



    // cover pic upload and crop functions......

    







    







    

                        /*$('.cancel').click(function(){

                            $('.cropit-preview').empty();

                            $('.cropit-image-input').val('');

                        });*/



                        





          // js ended by tech... 





      });

  </script>
  <script>
  
			
			$('#saveImage').click(function() {
                   
                      // Move cropped image data to hidden input

                      var imageData = $('.image-editor').cropit('export');  

                      //console.log(imageData);

                      $('.hidden-image-data').val(imageData);

                      $('#loading-image').show();

                      

                      $(".crop-control").hide();

                      $(".cropit-image-zoom-input").hide();

                      $(".image-size-label").hide();



                      //$(".cropit-preview").attr("style","background-color:gray;");

                      // var coverPreviewImage = $(".cropit-preview-image").attr("src");

                      //coverPreviewStyle = $(".cropit-preview-image").attr("style");



                      // $(".cropit-preview img").attr("style",coverPreviewStyle);

                      // $(".cropit-preview img").attr("src",coverPreviewImage);



                      $.ajax({

                          url: "coverpic/upload.php",

                          method:"POST",

                          data: {"image":imageData}

                      }).done(function(data) {

                            // $.delay(1000);

                            //$(".cropit-preview img").attr("src","coverpic/images/"+data);

                            //alert(data);

                            //$(".cropit-preview img").one("load",function(){

                            	//if(this.complete) $(this).load();

                            	// {

                            	// $(".crop-control").hide();

	                            //  $(".cropit-image-zoom-input").hide();

	                            //  $(".image-size-label").hide();

	//                          $(".profile_pic_overlay").attr("style","opacity:0");

	//                          $("#profile_img").attr("style","opacity:1");



    modifyProfilePicDesign();



    $(".cropit-preview > img").attr("src","");

	                            //alert("Done");

	                          	//$( this ).addClass( "done" );

                               $('#loading-image').hide();



                            // }

                           // });



                           setCoverPic(data);



                           coverPicPath = data;

                       });



                        // save cover image in database 











                          // Print HTTP request params

                          var formValue = $(this).serialize();

                          $('#result-data').text(formValue);



                      });
		
  </script>
  <script>
 
		  $('.remove_photo').click(function() {

                          // Move cropped image data to hidden input

                          var imageData = $('.image-editor').cropit('export');  

                          //console.log(imageData);

                          $('.hidden-image-data').val(imageData);

                          $('#loading-image').show();

                          

                          $.ajax({

                              url: "coverpic/upload.php?del=yes",

                              method:"POST",

                          }).done(function() {

                                //alert("Done");

                              //$( this ).addClass( "done" );

                              $(".cropit-preview-image-container").hide();

                              $(".cropit-preview-image-container > img").attr("src","");

                              $(".cropit-preview > img").attr("src","coverpic/images/banner.jpg");

                              

                              // $(".cropit-preview-image-container").remove();

                              $('#loading-image').hide();

//                              $("#profile_img").attr("style","opacity:1");

modifyProfilePicDesign();

});



                          // Print HTTP request params

                          var formValue = $(this).serialize();

                          $('#result-data').text(formValue);



                          removeCoverPic();



                          coverPicPath = "banner.jpg";



                      });
		
  
  
  </script>
<script>
  $(".coverphoto").mouseover(function() {
           
			if(changeCoverState == false){
                  
				$("#btn-add_cover").show();

				$("#btn-rem_cover").show();

			}
			

		});

		$(".coverphoto").mouseout(function() {

			if(changeCoverState == false){

			  $("#btn-add_cover").hide();

			  $("#btn-rem_cover").hide();

		  }

	  });
</script>
  <script>

    coverPicPath = "<?= $_SESSION["coverpic"]?>";

   //alert(coverPicPath);

    function chooseFile() {

       //alert("choose file clicked");

       $("#fileInput").click();
	   $('.image-editor').cropit(); 
	   
	   
   }

   function clickcancel() {



    $.ajax({

      url: "coverpic/upload.php?can=yes",

      method:"POST",

  }).done(function(data) {

                //$(".cropit-preview img").attr("src","coverpic/images/"+data);

                $(".crop-control").hide();

                $(".cropit-image-zoom-input").hide();

                $(".image-size-label").hide();

                $(".cropit-preview-image-container").hide();

                $(".cropit-preview > img").attr("src","coverpic/images/"+coverPicPath);

                //$(".cropit-preview-image").attr("src","coverpic/images/"+coverPicPath);

                //$(".cropit-preview-image").attr("style",coverPreviewStyle);

               //$(".profile_pic_overlay").attr("style","opacity:0");

               //$("#profile_img").attr("style","opacity:1");



               modifyProfilePicDesign();

           });



       // $(".crop-control").hide();

       // $(".cropit-image-zoom-input").hide();

       // $(".image-size-label").hide();

       //  $(".cropit-preview-image").attr("src","");





       modifyProfilePicDesign();

   }



   function modifyProfilePicDesign(){

        //$(".profile_pic_overlay").attr("style","opacity:0");



        changeCoverState = false;



        //$(".cropit-preview-image-container").hide();

        //$(".cropit-preview-image-container").attr("cursor","default !important");

        $(".cropit-preview-image-container").css("pointer-events","none");



        $("#profile_img").attr("style","opacity:1");

        $("div.profile_pic").attr("border","9px solid black");

        $("#author_name_text").show();



    }




    //console.log(coverPicPath);

</script>  
</body>

</html>