<!-- Bootstrap styles -->
      
	
        <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
        <link rel="stylesheet" href="uploadpl/css/jquery.fileupload.css">
		
		<link rel="stylesheet" href="uploadpl/css/style.css">
<div class="modal fade bs-example-modal-lg" id="modelid" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="outer">
                        <div class="main_box">
                                <div class="col-sm-10">
                                    <div class="right_box">
                                        <div class="mesge_box">
                                            <span><img src="uploadpl/img/info.png" /></span>
                                            <h1>This video currently processing</h1>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                                            <div class="check_box_mian"><input type="checkbox" /> Notify me when my video is done processing</div>
                                        </div>

                                        <div class="box_ttp">

                                            <div class="col-sm-4 pad_left">
                                                <div class="video video-area">
												</div>
                                                <div class="video_text">
                                                    <h1>Upload status:</h1>
                                                    <p> <span class="video-upload-status">Uploading</span> your video.
													<!--<br />Your video will be live at: --> </p>
                                                </div>
                                            </div>

                                            <div class="col-sm-8 pad_right">
												<form action="#" method="POST" id="videoPostfrm">
                                                    <input id='vid' name='vid' value="" type="hidden" />
													 <input id='selectedThumbnail' name='selectedThumbnail' value="" type="hidden" />
                                                    <input  name='owner_id' value="<?php echo isset($_GET['profileid']) ? base64_decode($_GET['profileid']) : $session->getSession("userid");?>" type="hidden" />
                                                  <input  name='author_id' value="<?php echo $session->getSession("userid");?>" type="hidden" />
                                                    <div class="form_main">

														<div id="progress" class="progress ">
															<div class="progress-bar progress-bar-info"></div>
														    <span class="video-upload-progress"></span>
														 </div>
													 <label class="upload-status-label">Your video is still <span class="video-upload-status">Uploading</span>. Please keep this page opne until it's done.</label>
                                                        <input type="text" placeholder="Title" name="title" id="vtitle" />
                                                        <span class="title-error">Video title required</span>
														<textarea placeholder="Description"   name="description" id="description"></textarea>
                                                        <input type="text" placeholder="Tags"  name="tags" id="tags"  >
                                                        <textarea placeholder="Tags (e.g, albert einstein, flying pig, mashup)" name="moretags" id="moretags"></textarea>
														<select name="showTo" id="showTo" >
                                                            <option value="0">Public</option>
                                                            <option value="1">Private</option>
                                                        </select>
														<select  name="category" id="category">
                                                            <option value="1">Beauty & Fashion</option>
                                                            <option value="2">Business</option>
                                                            <option value="3">Cars & Trucks</option>
                                                            <option value="4">Comedy</option>
                                                            <option value="5">Cute / Animals</option>
                                                            <option value="6">Entertainment</option>
                                                            <option value="7">Family</option>
                                                            <option value="8">Food & Health</option>
                                                            <option value="9">Home</option>
                                                            <option value="10">Lifestyle</option>
                                                            <option value="11">Music</option>
                                                            <option value="12">News</option>
                                                            <option value="13">Politics</option>
                                                            <option value="14">Science</option>
                                                            <option value="15">Sports</option>
                                                            <option value="16">Technology</option>
                                                            <option value="17">Video Faming</option>
                                                            <option value="18">Other</option>
                                                        </select>
                                                        <!--<div class="tag es">Category</div>-->
                                                    </div>



                                                    <div class="test_monial">
                                                        <h1>VIDEO THUMBNAILS <img src="uploadpl/img/img.jpg" /></h1>
                                                        <p>Thumbnail selections will appear when the video has finished procesing</p>
                                                        <ul class="video-thumbnail-section" style="display:none;">
                                                           <li><img src="uploadpl/img/1.jpg" /></li>
                                                            <li><img src="uploadpl/img/1.jpg" /></li>
                                                            <li><img src="uploadpl/img/1.jpg" /></li>
                                                        </ul>
                                                        <button type="submit" disabled="disabled" id="video-post-button" class="btn btn-primary disabled"  >Publish</button>
														<button type="button"  class="btn btn-danger" id="video-cancel-button">Cancel</button>
														<button type="button" style="display:none;"  class="btn btn-danger " id="video-colse-button" data-dismiss="modal">Close</button>
                                                    </div>
                                                </form>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                     </div>
                </div>
            </div>
        </div>
		<div id="testy" style="margin: 0 auto;"></div>
		<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
        <script src="uploadpl/js/vendor/jquery.ui.widget.js"></script>
		 <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
        <script src="js/load-image.all.min.js"></script>
        <!-- The Canvas to Blob plugin is included for image resizing functionality -->
        <script src="js/canvas-to-blob.min.js"></script>
		<script src="uploadpl/js/bootstrap.min.js"></script>
		<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
        <script src="uploadpl/js/jquery.iframe-transport.js"></script>
        <!-- The basic File Upload plugin -->
        <script src="uploadpl/js/jquery.fileupload.js"></script>
		<!-- The File Upload processing plugin -->
        <script src="uploadpl/js/jquery.fileupload-process.js"></script>
        <!-- The File Upload image preview & resize plugin -->
        <script src="uploadpl/js/jquery.fileupload-image.js"></script>
		
		<script src="uploadpl/js/jquery.fileupload-validate.js"></script> 
		<script src="uploadpl/js/jquery.toastee.0.1.js"></script>
		<script src="uploadpl/js/videouploadJs.js"></script>
		
		
		