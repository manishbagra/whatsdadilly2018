 <?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once "bootstrap.php";
require_once 'model/Signup.php';
require_once 'model/Twitter.php';
require_once 'classes/Session.class.php';

$session = new Session();
if ($session->getSession("userid") == "" || $session->getSession("userid") == null) {
    
    include 'html/index.php';
} else {
    header("Location:home.php");
}
?> 