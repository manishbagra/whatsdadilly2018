 <?php
error_reporting(-1);
require_once "bootstrap.php";
require_once 'classes/Session.class.php';
require_once "model/Signup.php";

$session = new Session();
$Profile = new Signup();

$userId = "";

if ($session->getSession("userid") != "" || $session->getSession("userid") != null) {
    
    $myValue = array();
    
    parse_str($_POST['formData'], $myValue);
    
    $data['userid'] = $myValue['user_id'];
    
    $data['date'] = $myValue['yy'] . '-' . $myValue['mm'] . '-' . $myValue['dd'];
    
    $data['home'] = $myValue['home_address'];
    
    $data['country']      = $myValue['country'];
    $data['state']        = $myValue['state'];
    $data['city']         = $myValue['city'];
    $data['relationship'] = $myValue['relationship'];
    $data['interested']   = $myValue['interested'];
    $data['political']    = $myValue['political'];
    $data['religion']     = $myValue['religion'];
    $data['language']     = $myValue['language'];
    
    $data['gender'] = $myValue['gender'];
    
    $data['interested'] = $myValue['interested'];
    
    $data['about_you'] = $myValue['about_you'];
    
    $contactInfo = $Profile->contactInfo($data, $entityManager);
    
    if ($contactInfo['email'] != '') {
        
        $indEmail = explode(',', $contactInfo['email']);
        
    } else {
        
        $indEmail = array(
            '',
            ''
        );
    }
    
    
    
    
    switch (true) {
        
        case ($indEmail[0] == '' and $indEmail[1] == ''):
            
            if (@$myValue['add_email1'] != '' && @$myValue['add_email2'] != '') {
                
                $indEmail[0] = $myValue['add_email1'];
                
                $indEmail[1] = $myValue['add_email2'];
                
            } else {
                
                $indEmail = $indEmail;
            }
            
            break;
        
        case ($indEmail[0] != '' and $indEmail[1] == ''):
            
            if (@$myValue['add_email2'] != '') {
                
                $indEmail[1] = $myValue['add_email2'];
                
            } else {
                $indEmail[1] = $indEmail[1];
            }
            
            break;
        
        case ($indEmail[0] == '' and $indEmail[1] != ''):
            
            if (@$myValue['add_email1'] != '') {
                
                $indEmail[0] = $myValue['add_email1'];
                
            } else {
                $indEmail[0] = $indEmail[0];
            }
            
            break;
            
    }
    
    $data['email'] = implode(',', $indEmail);
    
    
    if ($contactInfo['phone'] != '') {
        
        $indPhone = explode(',', $contactInfo['phone']);
        
    } else {
        
        $indPhone = array(
            '',
            ''
        );
    }
    
    
    
    switch (true) {
        
        case ($indPhone[0] == '' and $indPhone[1] == ''):
            
            if (@$myValue['add_phone1'] != '' && @$myValue['add_phone2'] != '') {
                
                $indPhone[0] = $myValue['add_phone1'];
                
                $indPhone[1] = $myValue['add_phone2'];
                
            } else {
                $indPhone = $indPhone;
            }
            
            break;
        
        case ($indPhone[0] != '' and $indPhone[1] == ''):
            
            if (@$myValue['add_phone2'] != '') {
                
                $indPhone[1] = $myValue['add_phone2'];
                
            } else {
                
                $indPhone[1] = $indPhone[1];
            }
            
            break;
        
        case ($indPhone[0] == '' and $indPhone[1] != ''):
            
            if (@$myValue['add_phone1'] != '') {
                
                $indPhone[0] = $myValue['add_phone1'];
                
            } else {
                
                $indPhone[0] = $indPhone[0];
            }
            
            break;
            
    }
    
    $data['phone'] = implode(',', $indPhone);
    
    
    if ($contactInfo['website'] != '') {
        
        $indWeb = explode(',', $contactInfo['website']);
        
    } else {
        
        $indWeb = array(
            '',
            ''
        );
    }
    
    
    
    switch (true) {
        
        case ($indWeb[0] == '' and $indWeb[1] == ''):
            
            if (@$myValue['add_web1'] != '' && @$myValue['add_web2'] != '') {
                
                $indWeb[0] = $myValue['add_web1'];
                
                $indWeb[1] = $myValue['add_web2'];
                
            } else {
                $indWeb = $indWeb;
            }
            
            break;
        
        case ($indWeb[0] != '' and $indWeb[1] == ''):
            
            if (@$myValue['add_web2'] != '') {
                
                $indWeb[1] = $myValue['add_web2'];
                
            } else {
                $indWeb[1] = $indWeb[1];
            }
            
            break;
        
        case ($indWeb[0] == '' and $indWeb[1] != ''):
            
            if (@$myValue['add_web1'] != '') {
                
                $indWeb[0] = $myValue['add_web1'];
                
            } else {
                $indWeb[0] = $indWeb[0];
            }
            
            break;
            
    }
    
    $data['website'] = implode(',', $indWeb);
    
    
    
    //echo '<pre>'; print_r($data);die;
    
    $result = $Profile->updateUserDetails($data, $entityManager);
    
    if ($result === 1) {
        echo json_encode(array(
            'status' => true
        ));
        exit();
    } else {
        echo json_encode(array(
            'status' => false
        ));
        exit();
    }
    
    
} 