 <?php
require_once "bootstrap.php";
include_once("config.php");
require_once 'classes/Session.class.php';
require_once 'classes/Albums.class.php';
require_once 'entities/Album.php';

$session = new Session();
$userid  = $session->getSession('userid');
$img_p   = $session->getSession("profile_pic");



$albums = new Album();

$user_id = $session->getSession('userid');

$output_dir = __DIR__ . "/uploads";



if (isset($_GET['id'])) {
    
    
    $params = array(
        'id_owner' => $userid,
        'id_album' => $_GET['id']
    );
    
    
    
    $album = AlbumUtil::getAlbum($entityManager, $params);
    
    $session->setSession('send_photos', 1);
    
    if ($album == false) {
        $other = true;
        
        
        
        $session->setSession('other', false);
    } else {
        if ($album->getIdOwner() == $session->getSession('userid'))
            $session->setSession('other', true);
        else
            $session->setSession('other', false);
        $session->setSession('id_album', $album->getIdAlbum());
    }
} else {
    
    $params = array(
        'id_owner' => $_SESSION['userid'],
        'title' => 'No title',
        'priv' => '1'
    );
    
    
    $result = Albums::createAlbum($entityManager, $params);
    
    
    
    $repo = $entityManager->getRepository('Album');
    
    $params['datetime'] = $result->getDatetime();
    
    $album = $repo->findOneBy($params);
    
    
    $_SESSION['open_album'] = $album->getIdAlbum();
    
    unset($_SESSION['id_album']);
    
    unset($_SESSION['send_photos']);
    
    
    $session->setSession('send_photos', 0);
}

$session->setSession('other', true);

$session->setSession('id_album', $album->getIdAlbum());


include 'html/imageuploadrer.php';
?> 