 <?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'bootstrap.php';
require_once 'model/SendMail.php';
require_once 'classes/Session.class.php';

$session = new Session();

$sendMailObj = new SendMail();

$allEmails = $sendMailObj->getEmailForInvite($entityManager);

if (count($allEmails) > 0) {
    foreach ($allEmails as $item) {
        $friend['email'] = $item['email'];
        // $friend['id']      = $item[''];
        $friend['name']  = $item['name'];
        
        $sendMailObj->sendInvite($entityManager, 3, $friend);
    }
}
exit(); 