<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once "bootstrap.php";
require_once 'model/Signup.php';
require_once 'model/Twitter.php';
require_once 'classes/Session.class.php';
$session = new Session();
if ($session->getSession("userid") != "" || $session->getSession("userid") != null) {
    if (isset($_POST['log_password']) && $_POST['log_password'] != "" && isset($_POST['old_password']) && $_POST['old_password'] != "") {
        $data = array(
        	"user_id" => $session->getSession("userid"),
        	"log_password" => $_POST['log_password'],
            "old_password" => $_POST['old_password']
        );
     	// print_r($data);
        $result = Signup::ModifyPassword($data, $entityManager);
    }

    if (isset($result) && $result == true) {
    	
    	echo json_encode(['success' => true, 'message' => 'Password Successfully Modified']);
    }
    else {
    	echo json_encode(['success' => false, 'message' => 'Old Password incorrect']);
    }

} else {
    echo json_encode(['success' => false, 'message' => 'Please you are not logged in']);
}
?>