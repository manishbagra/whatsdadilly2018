<?php
error_reporting(0);
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once "bootstrap.php";

require_once 'model/Signup.php';

require_once 'model/Twitter.php';

require_once 'model/Wall.php';

require_once 'model/PhotosModel.php';

require_once 'model/VideosModel.php';

require_once 'model/Audio.php';

require_once 'model/Comments.php';

require_once 'classes/Session.class.php';

include 'html/wall/functions.php';

require_once 'model/Notification.php';

require_once 'model/Friends.php';

require_once 'model/Albums.php';

$params = array();

$params['type'] = $_POST['type'];

if($params['type']=='reply')
{
$params['author_id'] = $_SESSION['userid'];

$params['parent_id'] = (int) str_replace('replyid','',$_POST['reply']);

$params['post_id'] = (int) str_replace('postid_','',$_POST['postid']);


$params['text'] = $_POST['comment'];

$params['date'] = date('Y-m-d H:i:s');


$reply['photos'] = PhotosModel::getPhoto($params['post_id'], $entityManager);
$reply['coverpic'] = PhotosModel::getIsCover($params['post_id'], $entityManager);
$reply['videos'] = VideosModel::getVideos($params['post_id'], $entityManager);

$reply['videosauthor_id'] = WallModel::getAutherIdForReply($entityManager,$params['post_id']);

$reply['audios'] = AudioModel::getAudios($params['post_id'], $entityManager);

$reply['author_id'] = WallModel::getAutherId($entityManager,$params['post_id']);

$reply['album'] = Albums::checkAlbumIsExist($params['post_id'], $entityManager);

$reply['album_author_id'] = WallModel::getAutherId($entityManager,$params['post_id']);

	if(count($reply['photos']) > 0 ) {
			
			$params['post_type'] = 1;
			if($reply['coverpic'][0]['is_cover']==0){
			$src = "uploads/".$reply['photos'][0]['author_id']."/posted/thumb/".$reply['photos'][0][0]['file'];
			}else{
				$src = "coverpic/images/".$reply['photos'][0][0]['file'];
			}
		}else if(count($reply['videos']) > 0 ) {
			
			$params['post_type'] = 2;
			
		    $src = $reply['videos'][0]['thumbnail'];
			
		}else if($reply['audios']['id']) {
			
			$params['post_type'] = 3;
			
			$src = $reply['audios']['thumb'];
			
		}else if ($reply['album']['id_album']) {
			
			$image_path = 'uploads/'.$reply['album']['id_owner'].'/albums/'.$reply['album']['id_album'].'/thumb/'.$reply['album']['cover'];
		
		
			$new_src = glob($image_path."*.{jpg,jpeg,png,gif}", GLOB_BRACE);
			
			$file_new = basename($new_src[0]);
			
			$explode_new = explode(".",$file_new);
			
			if($explode_new[0] == $reply['album']['cover']){
				
				$ext = $explode_new[1];
			}
			$params['post_type'] = 5;
			
			$src = $image_path.'.'.$ext;
			
		} else {
			
			$params['post_type'] = 4;
			/*Youtube video thum*/
			$link = $reply['videosauthor_id']['text'];
			//echo'<pre>';print_r($link);die;
			$video_id = explode("?v=", $link); 
			if (empty($video_id[1]))
				$video_id = explode("/v/", $link); 

			$video_id = explode("&", $video_id[1]);
			$video_id = $video_id[0];
			$src = 'http://i1.ytimg.com/vi/'.$video_id.'/default.jpg';
			
		}
$sql_cmt = "insert into comments (`author_id`,`post_id`,`text`,`parent_id`,`date`,`post_type`) values('".$params['author_id']."','".$params['post_id']."','".$params['text']."','".$params['parent_id']."','".$params['date']."','".$params['post_type']."')";	

$last = CommentsModel::addNewComment($entityManager, $sql_cmt);

//echo'<pre>';print_r($params);die;
//$last = CommentsModel::addComment($entityManager, $params);

$comments = CommentsModel::getLastComments($entityManager, $last);

$cuserdetails = WallModel::getUserDetails($entityManager, $comments[0]['author_id']);

$getallComeneted = WallModel::getUserCommentedIds($entityManager,$params['post_id'],$comments[0]['author_id']);

if(count($getallComeneted) > 0){
			
	$fullname =  ucfirst($cuserdetails[0]['firstname']).' '.$cuserdetails[0]['lastname'];
		
    $notifications = new NotificationModel();
			
	$notifi['id_friend'] = $_SESSION['userid'];
	
	if($params['post_type'] === 1 ){
			
			$noti_txt = 'photo.';
		}else if($params['post_type'] === 2){
			
			$noti_txt = 'video.';
			
		}else if($params['post_type'] === 3){
			$noti_txt = 'audio.';
		}else if($params['post_type'] === 4){
			$noti_txt = 'youtube video.';
		}else if($params['post_type'] === 5){
			$noti_txt = 'album.';
		}
	
	if($reply['coverpic'][0]['is_cover']==0){
		if($reply['photos'][0]['author_id'] == $params['author_id'])
			{
				$notifi['message']    = addslashes("<span>".$fullname." </span>added a reply. ");
			}else{
				
				$notifi['message']    = addslashes("<span>".$fullname." </span>replied on your ".$noti_txt." ");
			}
			  
	}
	if($reply['coverpic'][0]['is_cover']==1){
		if($reply['photos'][0]['author_id'] == $params['author_id'])
			{
				$notifi['message']    = addslashes("<span>".$fullname." </span>added a reply. ");
			}else{
				
				$notifi['message']    = addslashes("<span>".$fullname." </span>replied on your billboard photo. ");
			}
				
		
	}
	if($reply['author_id'] == $params['author_id'])
	{
		$notifi['message']    = addslashes("<span>".$fullname." </span>added a reply. ");
	}else{
		$notifi['message']    = addslashes("<span>".$fullname." </span>replied on your ".$noti_txt." ");
	}
	if($reply['videosauthor_id'] == $params['author_id'] )
	{
		$notifi['message']    = addslashes("<span>".$fullname." </span>added a reply. ");
	}else{
		$notifi['message']    = addslashes("<span>".$fullname." </span>replied on your ".$noti_txt." ");
	}
	if($reply['album_author_id'] == $params['author_id'])
	{
		$notifi['message']    = addslashes("<span>".$fullname." </span>added a reply. ");
	}else{
		$notifi['message']    = addslashes("<span>".$fullname." </span>replied on your ".$noti_txt." ");
	}
	
	
	$notifi['type']      	= 6;
	$notifi['read']      	= 0;    
	$notifi['wall_id']     	= $params['post_id'];  
	$notifi['comment_id']  	= $last;  
	
	//echo'<pre>';print_r($notifi);die;
	
		foreach($getallComeneted as $ids){
		
			$notifi['id_user']   = $ids['author_id'];
			
			 if($notifi['id_user']== $notifi['id_friend']){
		
			}else{
				$sql = "INSERT INTO `notifications` (`id_user`,`id_friend`,`type`,`message`,`thumb`,`comment_id`,`wall_id`,`readed`) values ('".$notifi['id_user']."','".$notifi['id_friend']."','".$notifi['type']."','".$notifi['message']."','".$src."','".$last."','".$params['post_id']."','".$notifi['read']."')";
							  
				 $notifications->addNotificationNEW($entityManager, $sql);
			}
			
		}
	
}else{
	
	$fullname =  ucfirst($cuserdetails[0]['firstname']).' '.$cuserdetails[0]['lastname'];
		
    $notifications = new NotificationModel();
			
	$notifi['id_friend'] = $_SESSION['userid'];
	
	if($params['post_type'] === 1 ){
			
			$noti_txt = 'photo.';
		}else if($params['post_type'] === 2){
			
			$noti_txt = 'video.';
			
		}else if($params['post_type'] === 3){
			$noti_txt = 'audio.';
		}
	
		if($reply['coverpic'][0]['is_cover']==0){echo'aaa';die;
		if($reply['photos'][0]['author_id'] == $params['author_id'])
			{
				$notifi['message']    = addslashes("<span>".$fullname." </span>added a reply. ");
			}else{
				
				$notifi['message']    = addslashes("<span>".$fullname." </span>replied on your ".$noti_txt." ");
			}
			  
	}
	if($reply['coverpic'][0]['is_cover']==1){echo'bbbb';die;
		if($reply['photos'][0]['author_id'] == $params['author_id'])
			{
				$notifi['message']    = addslashes("<span>".$fullname." </span>added a reply. ");
			}else{
				
				$notifi['message']    = addslashes("<span>".$fullname." </span>replied on your billboard photo. ");
			}
				
		
	}
	
	
	$notifi['type']       = 6;
	$notifi['read']       = 0;    
	$notifi['wall_id']    = $params['post_id'];  
	$notifi['comment_id'] = $last;  
	
	$auth_id = WallModel::getAuthorOwnerId($entityManager,$params['post_id']);
	
	$notifi['id_user']   = $auth_id['author_id'];
	$notifi['id_owner']  = $auth_id['owner_id'];
	// echo'<pre>';print_r($notifi['id_user']);
	// echo'<pre>';print_r($notifi['id_owner']);
    if($notifi['id_user']== $notifi['id_friend']){
		
	}else{
		$sql = "INSERT INTO `notifications` (`id_user`,`id_friend`,`type`,`message`,`thumb`,`comment_id`,`wall_id`,`readed`) values ('".$notifi['id_user']."','".$notifi['id_friend']."','".$notifi['type']."','".$notifi['message']."','".$src."','".$last."','".$params['post_id']."','".$notifi['read']."')";
					  
	     $notifications->addNotificationNEW($entityManager, $sql);
	}
	if($notifi['id_user']== $notifi['id_owner'])
	{
	}
	else if($params['author_id']==$notifi['id_owner']){
	}else{
		$sql = "INSERT INTO `notifications` (`id_user`,`id_friend`,`type`,`message`,`thumb`,`comment_id`,`wall_id`,`readed`) values ('".$notifi['id_owner']."','".$notifi['id_friend']."','".$notifi['type']."','".$notifi['message']."','".$src."','".$last."','".$params['post_id']."','".$notifi['read']."')";
	   
	   $notifications->addNotificationNEW($entityManager, $sql);
	}
	
	//echo'<pre>';print_r($notifi);die;
			
	//$last_id =$notifications->addNotification($entityManager, $notifi);
	
	
	
	
}

$text = $comments[0]['text'];

	if($cuserdetails[0]['profile_pic']!=''){
			$cuserdetails[0]['profile_pic']="uploads/".$cuserdetails[0]['profile_pic'];
		  } else {	
			$cuserdetails[0]['profile_pic']="uploads/default/Maledefault.png";
	 }
	$comments[0]['date']=date('F j, Y, g:i a', strtotime($comments[0]['date']));
 
	$comments=$comments[0];
	
	$cuserdetails=$cuserdetails[0];
	
	$finalarray=array('comments'=>$comments,
						'userdata'=>$cuserdetails,
						'lastcommentid'=>$last);
 
 
 echo json_encode( $finalarray); die;
	
	
}
else{
$params['author_id'] = $_SESSION['userid'];

$params['post_id']   = (int) str_replace('postid_','',$_POST['post_id']);

$params['text']      = $_POST['comment'];

$params['date']      = date('Y-m-d H:i:s');

$params['parent_id'] = 0;

        $entries['photos'] = PhotosModel::getPhoto($params['post_id'], $entityManager);
		$entries['coverpic'] = PhotosModel::getIsCover($params['post_id'], $entityManager);
        //echo'<pre>';print_r( $entries['photos']);die;
		$entries['videos'] = VideosModel::getVideos($params['post_id'], $entityManager);
		
		$entries['videosauthor_id'] = WallModel::getAutherIdForReply($entityManager,$params['post_id']);

		$entries['audios'] = AudioModel::getAudios($params['post_id'], $entityManager);
		
		$entries['author_id'] = WallModel::getAutherId($entityManager,$params['post_id']);
		
		$entries['album'] = Albums::checkAlbumIsExist($params['post_id'], $entityManager);
		$entries['album_author_id'] = WallModel::getAutherId($entityManager,$params['post_id']);
		if(count($entries['photos']) > 0 ) {
			
			$params['post_type'] = 1;
			if($entries['coverpic'][0]['is_cover']==0){
			    $src = "uploads/".$entries['photos'][0]['author_id']."/posted/thumb/".$entries['photos'][0][0]['file'];
			}else{
				$src = "coverpic/images/".$entries['photos'][0][0]['file'];
			}
		}else if(count($entries['videos']) > 0 ) {
			
			$params['post_type'] = 2;
			
		$src = $entries['videos'][0]['thumbnail'];
			
		}else if($entries['audios']['id']) {
			
			$params['post_type'] = 3;
			
			$src = $entries['audios']['thumb'];
			
		}else if ($entries['album']['id_album']) {
			
			$image_path = 'uploads/'.$entries['album']['id_owner'].'/albums/'.$entries['album']['id_album'].'/thumb/'.$entries['album']['cover'];
		
		
			$new_src = glob($image_path."*.{jpg,jpeg,png,gif}", GLOB_BRACE);
			
			$file_new = basename($new_src[0]);
			
			$explode_new = explode(".",$file_new);
			
			if($explode_new[0] == $entries['album']['cover']){
				
				$ext = $explode_new[1];
				
			}
			
			$params['post_type'] = 5;
			
			$src = $image_path.'.'.$ext;
			
		} else {
			
			$params['post_type'] = 4;
			/* Youtube vedio thum */
			$link = $entries['videosauthor_id']['text'];
			$video_id = explode("?v=", $link); 
			if (empty($video_id[1]))
				$video_id = explode("/v/", $link); 

			$video_id = explode("&", $video_id[1]);
			$video_id = $video_id[0];
			$src = 'http://i1.ytimg.com/vi/'.$video_id.'/default.jpg';
			
		}

    
	$sql_cmt = "insert into comments (`author_id`,`post_id`,`text`,`parent_id`,`date`,`post_type`) values('".$params['author_id']."','".$params['post_id']."','".$params['text']."','".$params['parent_id']."','".$params['date']."','".$params['post_type']."')";	

	//echo $sql_cmt;die;
	$last = CommentsModel::addNewComment($entityManager, $sql_cmt);
	
   //$last   = CommentsModel::addComment($entityManager, $params);

	$comments     = CommentsModel::getLastComments($entityManager, $last);
	
     //echo'<pre>';print_r($comments);die;
	 
	$cuserdetails = WallModel::getUserDetails($entityManager, $comments[0]['author_id']);
    
	$getallComeneted = WallModel::getUserCommentedIds($entityManager,$params['post_id'],$comments[0]['author_id']);
    
	  // $auth_id = WallModel::getOwnerId($entityManager,$params['post_id']);
	
      // $getallComeneted = array_merge($auth_id,$getallComeneted);
	//echo'<pre>';print_r($getallComeneted);
	
    
		

if(count($getallComeneted) > 0){
			
	$fullname =  ucfirst($cuserdetails[0]['firstname']).' '.$cuserdetails[0]['lastname'];
		
    $notifications = new NotificationModel();
			
	$notifi['id_friend'] = $_SESSION['userid'];
	
	if($params['post_type'] === 1 ){
			
			$noti_txt = 'photo.';
		}else if($params['post_type'] === 2){
			
			$noti_txt = 'video.';
			
		}else if($params['post_type'] === 3){
			$noti_txt = 'audio.';
		}else if($params['post_type'] === 4){
			$noti_txt = 'youtube video.';
		}else if($params['post_type'] === 5){
			$noti_txt = 'album.';
		}
	if($entries['coverpic'][0]['is_cover']==0){
		if($entries['photos'][0]['author_id'] == $params['author_id'])
			{
				$notifi['message']    = addslashes("<span>".$fullname." </span>added a new comment. ");
			}else{
				
				$notifi['message']    = addslashes("<span>".$fullname." </span>commented on your ".$noti_txt." ");
			}
			  
	}
	if($entries['coverpic'][0]['is_cover']==1){
		if($entries['photos'][0]['author_id'] == $params['author_id'])
			{
				$notifi['message']    = addslashes("<span>".$fullname." </span>added a new comment. ");
			}else{
				
				$notifi['message']    = addslashes("<span>".$fullname." </span>commented on your billboard photo. ");
			}
				
		
	}
	
	if($entries['author_id'] == $params['author_id'])
	{
		$notifi['message']    = addslashes("<span>".$fullname." </span>added a new comment. ");
	}else{
		$notifi['message']    = addslashes("<span>".$fullname." </span>commented on your ".$noti_txt." ");
	}
	if($entries['videosauthor_id'] == $params['author_id'] )
	{
		$notifi['message']    = addslashes("<span>".$fullname." </span>added a new comment. ");
	}else{
		$notifi['message']    = addslashes("<span>".$fullname." </span>commented on your ".$noti_txt." ");
	}if($entries['album_author_id'] == $params['author_id'] )
	{
		$notifi['message']    = addslashes("<span>".$fullname." </span>added a new comment. ");
	}else{
		$notifi['message']    = addslashes("<span>".$fullname." </span>commented on your ".$noti_txt." ");
	}
	
	$notifi['type']      	= 6;
	
	$notifi['read']      	= 0;    
	
	$notifi['wall_id']     	= $params['post_id'];  
	
	$notifi['comment_id']  	= $last;  
	
	//echo'<pre>';print_r($notifi);die;
	
		foreach($getallComeneted as $ids){
		
			$notifi['id_user']   = $ids['author_id'];
			
			 if($notifi['id_user']== $notifi['id_friend']){
		
			}else{
				$sql = "INSERT INTO `notifications` (`id_user`,`id_friend`,`type`,`message`,`thumb`,`comment_id`,`wall_id`,`readed`) values ('".$notifi['id_user']."','".$notifi['id_friend']."','".$notifi['type']."','".$notifi['message']."','".$src."','".$last."','".$params['post_id']."','".$notifi['read']."')";
							  
				 $notifications->addNotificationNEW($entityManager, $sql);
			}
			
		}
	
}else{
	
	$fullname =  ucfirst($cuserdetails[0]['firstname']).' '.$cuserdetails[0]['lastname'];
		
    $notifications = new NotificationModel();
			
	$notifi['id_friend'] = $_SESSION['userid'];
	
	if($params['post_type'] === 1 ){
			
			$noti_txt = 'photo.';
		}else if($params['post_type'] === 2){
			
			$noti_txt = 'video.';
			
		}else if($params['post_type'] === 3){
			$noti_txt = 'audio.';
		}
	
	if($entries['coverpic'][0]['is_cover']==0){
		if($entries['photos'][0]['author_id'] == $params['author_id'])
			{
				$notifi['message']    = addslashes("<span>".$fullname." </span>added a new comment. ");
			}else{
				
				$notifi['message']    = addslashes("<span>".$fullname." </span>commented on your ".$noti_txt." ");
			}
			  
	}
	if($entries['coverpic'][0]['is_cover']==1){
		if($entries['photos'][0]['author_id'] == $params['author_id'])
			{
				$notifi['message']    = addslashes("<span>".$fullname." </span>added a new comment. ");
			}else{
				
				$notifi['message']    = addslashes("<span>".$fullname." </span>commented on your billboard photo. ");
			}
				
		
	}
	
	
	
	$notifi['type']       = 6;
	
	$notifi['read']       = 0;    
	
	$notifi['wall_id']    = $params['post_id'];  
	
	$notifi['comment_id'] = $last;  
	
	$auth_id = WallModel::getAuthorOwnerId($entityManager,$params['post_id']);
	
	$notifi['id_user']   = $auth_id['author_id'];
	$notifi['id_owner']  = $auth_id['owner_id'];
	// echo'<pre>';print_r($notifi['id_user']);
	// echo'<pre>';print_r($notifi['id_owner']);
    if($notifi['id_user']== $notifi['id_friend']){
		
	}else{
		$sql = "INSERT INTO `notifications` (`id_user`,`id_friend`,`type`,`message`,`thumb`,`comment_id`,`wall_id`,`readed`) values ('".$notifi['id_user']."','".$notifi['id_friend']."','".$notifi['type']."','".$notifi['message']."','".$src."','".$last."','".$params['post_id']."','".$notifi['read']."')";
					  
	     $notifications->addNotificationNEW($entityManager, $sql);
	}
	if($notifi['id_user']== $notifi['id_owner'])
	{
	}
	else if($params['author_id']==$notifi['id_owner']){
	}else{
		$sql = "INSERT INTO `notifications` (`id_user`,`id_friend`,`type`,`message`,`thumb`,`comment_id`,`wall_id`,`readed`) values ('".$notifi['id_owner']."','".$notifi['id_friend']."','".$notifi['type']."','".$notifi['message']."','".$src."','".$last."','".$params['post_id']."','".$notifi['read']."')";
	   
	   $notifications->addNotificationNEW($entityManager, $sql);
	}
	
	//echo'<pre>';print_r($notifi);die;
			
	//$last_id =$notifications->addNotification($entityManager, $notifi);
	
	
	
	
}

	$text = $comments[0]['text'];

	if($cuserdetails[0]['profile_pic']!=''){
			$cuserdetails[0]['profile_pic']="uploads/".$cuserdetails[0]['profile_pic'];
		  } else {	
			$cuserdetails[0]['profile_pic']="uploads/default/Maledefault.png";
	 }
	$comments[0]['date']=date('F j, Y, g:i a', strtotime($comments[0]['date']));
 
	$comments=$comments[0];
	
	$cuserdetails=$cuserdetails[0];
	
	$finalarray=array('comments'=>$comments,
						'userdata'=>$cuserdetails,
						'lastcommentid'=>$last);
 

 echo json_encode($finalarray); die;
 
}
 
 ?>






