 <?php

//this file its used to upload a photo to a open album
//upload.php

require 'bootstrap.php';
require_once 'entities/Album.php';
require_once 'model/Albums.php';
require_once 'model/Wall.php';
require_once 'classes/Albums.class.php';
require_once 'classes/Session.class.php';

$resp = array();
// echo '<pre>';
// print_r($_POST);die;

// [title] => dfdf
// [album_id] => 2
// [name] => file0001209214386.jpg
// [type] => image/jpeg
// [tmp_name] => D:\xampp\tmp\php70.tmp
// [error] => 0
// [size] => 446716

//echo $_FILES['images']['tmp_name'];die;

$output_dir = __DIR__ . "/uploads";
$session    = new Session();



$userid = $session->getSession('userid');

$params = array();

$params['userid'] = $userid;

$params['id_album'] = $session->getSession('id_album');

$params['title'] = $_POST['title'];


$photo = AlbumUtil::getAlbum($entityManager, $params);


if ($photo == false) {
    die('trying to acess an album that isnt yours');
}

AlbumUtil::userHaveFolder();

$output_dir .= "/" . $userid . "/";
if (!file_exists($output_dir)) {
    $mask = umask(0);
    mkdir($output_dir, 0777);
    umask($mask);
}


$output_dir .= $params['id_album'] . "/";

if (!file_exists($output_dir)) {
    $mask = umask(0);
    mkdir($output_dir, 0777);
    umask($mask);
}


$paramss = array(
    'id_owner' => $userid,
    'id_album' => $params['id_album'],
    'title' => $params['title']
);



$photo = $entityManager->getRepository('PhotoAlbum')->findOneBy($paramss);

// echo '<pre>';
// print_r($photo);die;

$album_obj = new Albums();

$photosss = $album_obj->getLastId($entityManager, $params['id_album']);

$photoId = $photosss['id_photo'];

//echo $photoId;die;

if ($photo instanceof PhotoAlbum) {
    
    $session->setSession('send_photos', 1 + $session->getSession('send_photos'));
    
    $paths = $session->getSession('send_photos_path');
    
    if (is_array($paths)) {
        $paths[] = array(
            'id' => $photoId,
            'path' => $photo->getPath()
        );
        
    } else {
        $paths[] = array(
            array(
                'id' => $photoId,
                'path' => $photo->getPath()
            )
        );
    }
    
    
    $session->setSession('send_photos_path', $paths);
    
    
    $imageName = $photoId . '.jpg';
    
    //echo $imageName ;die;
    if (!file_exists($output_dir . $imageName)) {
        
        move_uploaded_file($_FILES['images']['tmp_name'], $output_dir . $_FILES['images']['name']);
        $photoId = $photoId + 1;
    } else {
        
        $imageName = ($photoId + 1) . '.jpg';
        //echo $imageName ;die;
        
        move_uploaded_file($_FILES['images']['tmp_name'], $output_dir . $imageName);
        
        $photoId = $photoId + 1;
    }
    
    
    $resp[] = array(
        'name' => $params['title'],
        "size" => filesize($output_dir . $imageName),
        "url" => str_replace('\\', '\/', $output_dir . $imageName),
        "thumbnailUrl" => str_replace('\\', '\/', "http://whatsdadilly.com/beta/uploads/" . $userid . "/" . $params['id_album'] . "/" . $imageName),
        "deleteUrl" => str_replace('\\', '\/', $output_dir . $imageName),
        "deleteType" => "DELETE"
    );
}


echo json_encode(array(
    'files' => $resp
));
exit();
?> 