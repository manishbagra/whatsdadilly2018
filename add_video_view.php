 <?php
require_once 'classes/Session.class.php';
require_once 'model/VideosModel.php';
$session = new Session();
$videos  = new VideosModel();
$user_id = $session->getSession("userid");

if ($_SERVER['request_method'] = 'POST') {
    if ($session->getSession("userid") != "" || $session->getSession("userid") != null) {
        $data['video_id'] = $_POST['vid'];
        $data['ip']       = $_SERVER['SERVER_ADDR'];
        $data['uId']      = $user_id;
        
        $check_already_view = $videos->checkAlreadyView($entityManager, $data);
        
        
        
        if ($check_already_view == 0) {
            $last_id = $videos->add_video_view($entityManager, $data);
            
            $jsonarray = array(
                'status' => true,
                'last_id' => $last_id,
                'count' => 1
            );
            
            echo json_encode($jsonarray);
            exit();
        } else {
            $jsonarray = array(
                'status' => false,
                'last_id' => 0,
                'count' => 0
            );
            echo json_encode($jsonarray);
            exit();
        }
        
    }
    
} 