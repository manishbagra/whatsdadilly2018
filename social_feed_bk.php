<?php
error_reporting(-1);

require_once "bootstrap.php";
require_once 'model/Signup.php';
require_once 'classes/Session.class.php';
require_once "model/Profile.php";
require_once 'model/Wall.php';
require_once 'model/PhotosModel.php';
require_once 'model/VideosModel.php';
require_once 'model/Comments.php';
require_once 'model/Comments.php';
require_once 'model/Notification.php';
require_once 'model/Instagram.php';
require_once 'model/Twitter.php';
require_once 'youtube/Google/autoload.php'; 
require_once 'youtube/Google/Client.php';
require_once 'youtube/Google/Service/YouTube.php';

include 'html/wall/functions.php';
include_once("config.php");
include_once("twitteroauth/twitteroauth.php");



$session = new Session();


if ($session->getSession("userid") != "" || $session->getSession("userid") != null) {
	
	$videoObj = new VideosModel();
	
	$userId = $session->getSession("userid");
	
	$data = array("userid" => $session->getSession("userid"));

	$params = array('id_owner' => $session->getSession("userid"));
	
	if (!isset($_POST['limit'])) {

        $limit = 10;

    } else {

        $limit = 3; 

    }

	$entries = array_slice(WallModel::getEntries($entityManager), 0, $limit);
	
	foreach ($entries as $key => $value) {

        $comments = CommentsModel::getCommentsMainPostWithLimit($entries[$key]['id'],0,5, $entityManager);

		$totalComments = count(CommentsModel::getCountCommentsMainPost($entries[$key]['id'], $entityManager));

        $photos = PhotosModel::getPhotos($entries[$key]['id'], $entityManager);

		foreach ($comments as $keyId => $commentVal) {

			$commentId = $commentVal['id'];

			$replies = CommentsModel::getCommentOfReply($commentId, $entityManager);

			$comments[$keyId]['replies'] = $replies;
		}

		$entries[$key]['comments_count'] = $totalComments;

        $entries[$key]['comments'] = array_reverse($comments);

        $entries[$key]['photos'] = $photos;

		$entries[$key]['videos'] = VideosModel::getVideos($entries[$key]['id'], $entityManager);

    }
	
	
	//Tweeter Tab 
	$twitterObj = new Twitter();
	
	$getExitToken = $twitterObj->checkAlreadyExist($entityManager,$userId);
	
	$num_rows = count($getExitToken);
	
	if($num_rows > 0){
		$_SESSION['token_id_twitter']    = $getExitToken[0]['token_id'];
		$_SESSION['networkname_twitter'] = $getExitToken[0]['networkname'];
		$_SESSION['auth_token_twitter']  = $getExitToken[0]['auth_token'];
		$_SESSION['auth_secret_twitter'] = $getExitToken[0]['auth_secret'];
		$_SESSION['screen_name_twitter'] = $getExitToken[0]['screen_name'];
		$_SESSION['screen_id_twitter']   = $getExitToken[0]['screen_id'];
		
		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['auth_token_twitter'], $_SESSION['auth_secret_twitter']);
		
		$live_timeline = $connection->get('https://api.twitter.com/1.1/statuses/home_timeline.json', array('screen_name' => $_SESSION['screen_name_twitter'], 'count' => 50, "contributor_details" => true, "include_entities" => true, "include_my_retweet" => true),'home_timeline');
		
		//$user_info     = $connection->get('https://api.twitter.com/1.1/account/verify_credentials.json', array("include_entities" => true, "include_rts" => true, "contributor_details" => true),'auth');
		
	}else{
		unset($_SESSION['token_id_twitter']);
		unset($_SESSION['networkname_twitter']);
		unset($_SESSION['auth_token_twitter']);
		unset($_SESSION['auth_secret_twitter']);
		unset($_SESSION['screen_name_twitter']);
		unset($_SESSION['screen_id_twitter']);
	}
	  
	
	$client = new Google_Client();
    $client->setClientId(YOUTUBE_CONSUMER_KEY);
    $client->setClientSecret(YOUTUBE_CONSUMER_SECRET);
    $client->setScopes('https://www.googleapis.com/auth/youtube');
    $client->setRedirectUri(YOUTUBE_CONSUMER_CALLBACK);
	
    $youtube = new Google_Service_YouTube($client);
	
	$youtube_auth_url = 'javascript:void(0)';
	
	
	$tokenSessionKey = 'token-' . $client->prepareScopes();
					
		
			if (isset($_GET['code'])) {
				  
				  if (strval($_SESSION['state']) !== strval($_GET['state'])) {
					  
					die('The session state did not match.');
				  
				  }

				  $client->authenticate($_GET['code']);
				  
				 
				  
				  $_SESSION[$tokenSessionKey] = $client->getAccessToken();
				  
				  header('Location: ' . YOUTUBE_CONSUMER_CALLBACK);
			}

				if (isset($_SESSION[$tokenSessionKey])) {
				  $client->setAccessToken($_SESSION[$tokenSessionKey]);
				  
				}

				if ($client->getAccessToken()) {
					
				  try {
					$channelsResponse = $youtube->channels->listChannels('contentDetails', array('mine' => 'true'));
                    
					$htmlBody = array();
					foreach ($channelsResponse['items'] as $channel) {
					  
					  $uploadsListId = $channel['contentDetails']['relatedPlaylists']['uploads'];

					  $playlistItemsResponse = $youtube->playlistItems->listPlaylistItems('snippet', array(
						'playlistId' => $uploadsListId,
						'maxResults' => 50
					  ));

					  foreach ($playlistItemsResponse['items'] as $playlistItem) {
						//$htmlBody[] = $playlistItem['snippet']['resourceId']['videoId'];
					    $htmlBody[] = $playlistItem;
					  }
		
					}
				  } catch (Google_Service_Exception $e) {
					$htmlBody = sprintf('<p>A service error occurred: <code>%s</code></p>',
					  htmlspecialchars($e->getMessage()));
				  } catch (Google_Exception $e) {
					$htmlBody = sprintf('<p>An client error occurred: <code>%s</code></p>',
					  htmlspecialchars($e->getMessage()));
				  }

				  $_SESSION[$tokenSessionKey] = $client->getAccessToken();
				}else {
					
				  $state = mt_rand();
				  $client->setState($state);
				  $_SESSION['state'] = $state;

				  $youtube_auth_url = $client->createAuthUrl();
				 
				}
		
		// echo '<pre>';
		// print_r($htmlBody);die;
		
	include 'html/social/index.php';
}

else{
	
	header("Location:index.php");
}
