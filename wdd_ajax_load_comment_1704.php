 <?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once "bootstrap.php";

require_once 'model/Comments.php';
include 'html/wall/functions.php';
include_once("config.php");

$postId         = $_POST['post_id'];
$start          = $_POST['start'];
$length         = $_POST['length'];
$totalComments  = count(CommentsModel::getCountCommentsMainPost($postId, $entityManager));
$getAllComments = CommentsModel::getCommentsMainPostWithLimit($postId, $start, $length, $entityManager);
if ($totalComments > ($start + $length)):
    $start = $start + $length;
    echo "<div id='loadmore_" . $postId . "' class='loadmorecomment'><a onclick='loadMore(" . $postId . "," . $start . "," . $length . ")'>Load More</a></div>";
endif;
foreach ($getAllComments as $comment):
//print_r($comment);
    
//$commentId = $commentVal['id'];
    $replies            = CommentsModel::getCommentOfReply($comment['id'], $entityManager);
    $comment['replies'] = $replies;
?>


<div class="slider_two_title">
        <img src="uploads/<?php
    echo $comment['profile_pic'];
?>" alt="<?php
    echo $comment['firstname'];
?> <?php
    echo $comment['lastname'];
?>" style="width:37px;height:37px;">
        <div class="slider_title_style 1single-comment">
            <a href="#"><span class="author_slide_top"><?php
    echo $comment['firstname'];
?> <?php
    echo $comment['lastname'];
?></span></a>
            <span class="comment_box_bottom_text"><?php
    echo $comment['text'];
?></span>
            <a href="#"><p class="like_and_reply"><span>Like</span><span class="comment_reply">Reply</span> · <?php
    echo date('m/d/Y H:i:s', strtotime($comment['date']));
?></p></a>                                    
            <!-- Reply Code -->
            
            <?php
    if (count($comment['replies'])):
        $allReplies = $comment['replies'];
        foreach ($allReplies as $replyKey => $replyValue):
?>
           <div class="line"></div>
            <div class="slider_two_title" id="reply_<?= $replyValue['id'] ?>">
                <img src="uploads/<?php
            echo $replyValue['profile_pic'];
?>" alt="<?php
            echo $replyValue['firstname'];
?> <?php
            echo $replyValue['lastname'];
?>" style="width:37px;height:37px;">
                <div class="slider_title_style 1single-comment">
                    <a href="#"><span class="author_slide_top"><?php
            echo $replyValue['firstname'];
?> <?php
            echo $replyValue['lastname'];
?></span></a>
                    <span class="comment_box_bottom_text"><?php
            echo $replyValue['text'];
?></span>
                    <a href="#"><p class="like_and_reply"><?php
            echo date('m/d/Y H:i:s', strtotime($replyValue['date']));
?></p></a>                                    
                </div>
            </div>
            
            <?php
        endforeach;
    endif;
?>
       </div>
        
    </div>
    <div class="line"></div>
    <?php
endforeach;
?> 