<?php

session_start();
//include_once("../config.php");
////include_once("inc/twitteroauth.php");
//require '../inc/tmhOAuth.php';
//require '../inc/tmhUtilities.php';
//$screenname = $_SESSION['request_vars']['screen_name'];
//$twitterid = $_SESSION['request_vars']['user_id'];
include_once("../config.php");
include_once("../twitteroauth/twitteroauth.php");
include_once("codebird.php");

$oauth_token = $_SESSION['auth_token_twitter'];
$oauth_token_secret = $_SESSION['auth_secret_twitter'];

// $tmhOAuth = new tmhOAuth(array(
//                'consumer_key' => CONSUMER_KEY,
//                'consumer_secret' => CONSUMER_SECRET,
//                'user_token' => $oauth_token,
//                'user_secret' => $oauth_token_secret,
//            ));
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $oauth_token, $oauth_token_secret);
// for video upload.. 
\Codebird\Codebird::setConsumerKey(CONSUMER_KEY, CONSUMER_SECRET);
$cb = \Codebird\Codebird::getInstance();

if (! isset($_SESSION['oauth_token'])) {
  // get the request token
  $reply = $cb->oauth_requestToken([
    'oauth_callback' => 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']
  ]);

  // store the token
  $cb->setToken($reply->oauth_token, $reply->oauth_token_secret);
  $_SESSION['oauth_token'] = $reply->oauth_token;
  $_SESSION['oauth_token_secret'] = $reply->oauth_token_secret;
  $_SESSION['oauth_verify'] = true;

  // redirect to auth website
  $auth_url = $cb->oauth_authorize();
  header('Location: ' . $auth_url);
  die();

} elseif (isset($_GET['oauth_verifier']) && isset($_SESSION['oauth_verify'])) {
  // verify the token
  $cb->setToken($_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
  unset($_SESSION['oauth_verify']);

  // get the access token
  $reply = $cb->oauth_accessToken([
    'oauth_verifier' => $_GET['oauth_verifier']
  ]);

  // store the token (which is different from the request token!)
  $_SESSION['oauth_token'] = $reply->oauth_token;
  $_SESSION['oauth_token_secret'] = $reply->oauth_token_secret;

  // send to same URL, without oauth GET parameters
  header('Location: ' . basename(__FILE__));
  die();
}

$cb->setToken($oauth_token, $oauth_token_secret);



// if (isset($_POST["status"]) && !isset($_FILES['photo'])) {

//     $aro = [
//         'withoutmedia' => 1,
//         'success' => 1,
//         //'reply'     => $reply
//     ];
//     //print_r($aro);
//     $s = json_encode($aro);
//     //json_decode($s);
//     echo $s;



//     // if (count($my_update->user) == 1) {
//     //     $success = array("success" => 1);
//     //     echo json_encode($success);
//     // } else {
//     //     $success = array("success" => 0);
//     //     echo json_encode($success);
//     // }

//     // print_r($_FILE);
    
// }
// else 
if(isset($_FILES['video']['tmp_name']) && $_FILES['video']['tmp_name'] != "" ){

    // echo json_encode(['success' => 1,'type' => 'video']);

    $file_tmp  = $_FILES["video"]["tmp_name"];



    // $file       = 'demo-video.mp4';
    $size_bytes = filesize($file_tmp);
    $fp         = fopen($file_tmp, 'r');

    // INIT the upload

    $reply = $cb->media_upload([
      'command'     => 'INIT',
      'media_type'  => 'video/mp4',
      'total_bytes' => $size_bytes
    ]);

    $media_id = $reply->media_id_string;

    // APPEND data to the upload

    $segment_id = 0;

    while (! feof($fp)) {
      $chunk = fread($fp, 1048576); // 1MB per chunk for this sample

      $reply = $cb->media_upload([
        'command'       => 'APPEND',
        'media_id'      => $media_id,
        'segment_index' => $segment_id,
        'media'         => $chunk
      ]);

      $segment_id++;
    }

    fclose($fp);

    // FINALIZE the upload

    $reply = $cb->media_upload([
      'command'       => 'FINALIZE',
      'media_id'      => $media_id
    ]);

    //var_dump($reply);

    if ($reply->httpstatus < 200 || $reply->httpstatus > 299) {
      die();
    }

    // if you have a field `processing_info` in the reply,
    // use the STATUS command to check if the video has finished processing.

    // Now use the media_id in a Tweet

    $message = $_POST["status"];

    $params = [
        'status' => $message,
        'media_ids' => $media_id
    ];

    $reply = $cb->statuses_update($params);

    $aro = [
        'withmedia' => 1,
        'status'    => $reply,
        'video' => implode(',', $file_tmp),
        'success' => 1
    ];

    //print_r($aro); 
    $s = json_encode($aro);
    echo $s;

}

else if(isset($_FILES['photo']['name']) && count($_FILES['photo']['name']) > 0) {

    // echo json_encode(['success' => 1,'type' => 'photo']);

    $photos = [];

    foreach ($_FILES['photo']['name'] as $key => $photo) {
                
        
        //$file_name = $_FILES["photo"]["name"][$key];
        $file_tmp  = $_FILES["photo"]["tmp_name"][$key];

        array_push($photos, $file_tmp);

    }

    foreach ($photos as $key => $file) {
      // upload all media files
      $reply = $cb->media_upload([
        'media' => $file
      ]);
      // and collect their IDs
      $media_ids[] = $reply->media_id_string;
    }

    // convert media ids to string list
    $media_ids = implode(',', $media_ids);

    // $message = urlencode($_POST["status"]);
    $message = $_POST["status"];

    $params = [
        'status' => $message,
        'media_ids' => $media_ids
    ];

    // send Tweet with these medias
    $reply = $cb->statuses_update($params);
    // print_r($reply);


    $aro = [
        'withmedia' => 1,
        'status'    => $reply,
        'photo' => implode(',', $photos),
        'success' => 1
    ];
    //print_r($aro); 
    $s = json_encode($aro);
    echo $s;

    //fclose($handle);
 

    // unset($_SESSION['media_name']);
    // if ($code->id_str != '') {
    //     $success = array("success" => 1);
    //     echo json_encode($success);
    // } else {
    //     $success = array("success" => 0);
    //     echo json_encode($success);
    // }
}

else if (isset($_POST["status"]) && $_POST["status"] != ""){

    $message = $_POST["status"];

    $params = [
        'status' => $message
    ];

    $reply = $cb->statuses_update($params);


    echo json_encode(['success' => 1,'reply' => $reply ]);

} 
else {
   $success = array("success" => 0);
   echo json_encode($success);
}
    
//   $aro[] = [
//     'test' => count($_FILES['photo']['name'])
//   ];

// if(isset($_FILES['photo']['name']) && count($_FILES['photo']['name']) > 0) {


//     $aro[] = [
//         'withmedia' => 1,
//         'status'    => $_FILES['photo']['name'],
//         'photo' => implode(',', $photos),
//         'success' => 1
//     ];
//     //print_r($aro);

// }

// if(isset($_FILES['video']['tmp_name']) && $_FILES['video']['tmp_name'] != "" ){
//   $aro[] = [
//     'test-vid' => $_FILES['video']['name']
//   ];
// }

 
//     $s = json_encode($aro);
//     echo $s;

?>