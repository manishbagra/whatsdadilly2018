<?php
session_start();
include_once("../config.php");
include_once("../inc/twitteroauth.php");

$screenname = $_SESSION['screen_name_twitter'];//$_GET['screenname'];
$oauth_token = $_SESSION['auth_token_twitter'];
$oauth_token_secret = $_SESSION['auth_secret_twitter'];
$twitterid = $_SESSION['screen_id_twitter'];

$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $oauth_token, $oauth_token_secret);

// if (isset($_POST["retweet_id"])) {


//     $id = $_POST["retweet_id"];
//     if ($_POST['action'] == 'delete') {
//         $my_update = $connection->post("statuses/destroy/" . $id);

//     } else if ($_POST['action'] == 'undofav') {
//         $my_update = $connection->post("favorites/destroy", array("id" => $id));
//     } else {
//         $my_update = $connection->post("statuses/destroy/" . $id);
//     }
//     if ($my_update->id_str != '' || $my_update->user->id != '') {
//         $success = array("success" => 1);
//         echo json_encode($success);
//     } else {
//         $success = array("success" => 0);
//         echo json_encode($success);
//     }

// }

if (isset($_POST["retweet_id"])) {


    // update the cache file

    $jsonString = file_get_contents('../cache/twitter-cache-home_timeline');
    $data = json_decode($jsonString, true);

    $id = $_POST["retweet_id"];
 
    if ($_POST['action'] == 'delete') {
        $my_update = $connection->post("statuses/destroy/" . $id);

    } else if ($_POST['action'] == 'undofav') {
        
        $my_update = $connection->post("favorites/destroy", array("id" => $id));

        if (isset($my_update->retweeted_status)) {

            foreach ($data as $key => $entry) {

                if ($entry['id_str'] == $my_update->id_str) {

                    $data[$key]['retweeted_status']['favorite_count'] = $my_update->retweeted_status->favorite_count;

                }
                
            }
        }
        else {

            foreach ($data as $key => $entry) {

                if ($entry['id_str'] == $my_update->id_str) {

                    $data[$key]['favorite_count'] = $my_update->favorite_count;

                }
                
            }

        }

    } else {
        $my_update = $connection->post("statuses/destroy/" . $id);
    }
    if ($my_update->id_str != '' || $my_update->user->id != '') {

        if (isset($my_update->retweeted_status)) {

            $success = array("success" => 1,"favorite_count" => $my_update->retweeted_status->favorite_count,"id_str" => $my_update->id_str);
        }
        else{

            $success = array("success" => 1,"favorite_count" => $my_update->favorite_count,"id_str" => $my_update->id_str);
        }

        echo json_encode($success);
    } else {
        $success = array("success" => 0);
        echo json_encode($success);
    }


    $newJsonString = json_encode($data);
    file_put_contents('../cache/twitter-cache-home_timeline', $newJsonString);
    
    // end of update cache file

}

?>