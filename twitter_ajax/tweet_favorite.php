<?php
session_start();
include_once("../config.php");
include_once("../inc/twitteroauth.php");

$screenname = $_SESSION['screen_name_twitter'];//$_GET['screenname'];
$oauth_token = $_SESSION['auth_token_twitter'];
$oauth_token_secret = $_SESSION['auth_secret_twitter'];
$twitterid = $_SESSION['screen_id_twitter'];

$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $oauth_token, $oauth_token_secret);

// if (isset($_POST["favorite_id"])) {

//     $id = $_POST["favorite_id"];

//     $response = $connection->post('favorites/create', array('id' => $id));
//     if ($response->id_str != '') {
//         $success = array("success" => 1);
//         echo json_encode($success);
//     } else {
//         $success = array("success" => 0);
//         echo json_encode($success);
//     }

// }

if (isset($_POST["favorite_id"])) {

    $id = $_POST["favorite_id"];

    $response = $connection->post('favorites/create', array('id' => $id));
    if ($response->id_str != '') {


        // update the cache file

        $jsonString = file_get_contents('../cache/twitter-cache-home_timeline');
        $data = json_decode($jsonString, true);


        if (isset($response->retweeted_status)) {
            $success = array("success" => 1,"favorite_count" => $response->retweeted_status->favorite_count,"id_str" => $response->id_str );

            foreach ($data as $key => $entry) {
                if ($entry['id_str'] == $response->id_str) {
                    $data[$key]['retweeted_status']['favorite_count'] = $response->retweeted_status->favorite_count;
                }
            }


        }
        else {
            
            $success = array("success" => 1,"favorite_count" => $response->favorite_count,"id_str" => $response->id_str );

            // $data[0]['activity_name'] = "TENNIS";
            // or if you want to change all entries with activity_code "1"
            foreach ($data as $key => $entry) {
                if ($entry['id_str'] == $response->id_str) {
                    $data[$key]['favorite_count'] = $response->favorite_count;
                }
            }


        }


        $newJsonString = json_encode($data);
        file_put_contents('../cache/twitter-cache-home_timeline', $newJsonString);
            
        // end of update cache file        

        echo json_encode($success);
    } else {
        $success = array("success" => 0);
        echo json_encode($success);
    }

}

?>